import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {DialogService} from '@shared/services/confirm-dialog.service';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(private injector: Injector,
              private dialogService: DialogService) {
  }

  handleError(error: Error | HttpErrorResponse): void {
    if (error instanceof HttpErrorResponse) {
      const messages = navigator.onLine ? error.error.message : 'No Internet Connection';
      for (let message of messages) {
      }
    } else {
      const message: string = error.message ? error.message : error.toString();
    }
    this.dialogService.open(JSON.stringify(error));
  }
}
