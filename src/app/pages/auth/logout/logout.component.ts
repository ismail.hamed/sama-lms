import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AuthService} from '@shared/services/auth.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<LogoutComponent>,
    private authService: AuthService) {
  }

  ngOnInit(): void {
  }

  hidePopup(): void {
    this.dialogRef.close();
  }

  logout(): void {
    this.authService.logout();
    this.hidePopup();
  }
}
