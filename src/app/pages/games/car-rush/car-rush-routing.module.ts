import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CarRushComponent} from './car-rush.component';

const routes: Routes = [
  {
    path: '',
    component: CarRushComponent,
    children: [
      {
        path: '',
        component: CarRushComponent,
        // resolve: {games: GamesResolverService},
      },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CarRushRoutingModule {

}
