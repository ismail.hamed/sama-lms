import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {reloadOnce} from '@shared/shared';
import {ActivatedRoute, Router} from '@angular/router';
import {DynamicScriptLoaderService} from '@shared/services/DynamicScriptLoaderService';
import {DialogService} from '@shared/services/confirm-dialog.service';


declare function initial(): any;

declare function stopCarRushAudio(): any;

@Component({
  selector: 'app-car-rush',
  templateUrl: './car-rush.component.html',
  styleUrls: ['./car-rush.component.css'],
  encapsulation: ViewEncapsulation.None

})
export class CarRushComponent implements OnInit, OnDestroy {
  scripts: string[] = [
    'car-rush-1', 'car-rush-2', 'car-rush-3',
  ];
  timeout;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private dialogService: DialogService,
              private dynamicScriptLoader: DynamicScriptLoaderService) {
  }

  ngOnInit(): void {
    reloadOnce();
    this.checkFinishDate();
    this.dynamicScriptLoader.load(this.scripts).then(data => {
      setTimeout(() => {
        initial();
      }, 1);
    });
  }
  private checkFinishDate(): void {
    const finishDate = localStorage.getItem('car-rush');
    const finisTime = new Date(finishDate);
    const now = new Date();
    if (now > finisTime) {
      this.router.navigate(['games']);
    } else {
      const diff = (finisTime.getTime() - now.getTime()) / 1000;
      const diffSeconds = Math.abs(Math.round(diff));
      this.timeout = setTimeout(() => {
        this.dialogService.openFinishTimeDialog();
      }, diffSeconds * 1000);
    }
  }

  ngOnDestroy(): void {
    clearTimeout(this.timeout);
    stopCarRushAudio();
  }

}
