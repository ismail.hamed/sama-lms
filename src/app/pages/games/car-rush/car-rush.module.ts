import {NgModule} from '@angular/core';
import {CarRushComponent} from './car-rush.component';
import {CarRushRoutingModule} from './car-rush-routing.module';

@NgModule({
  declarations: [
    CarRushComponent,
  ],
  imports: [
    CarRushRoutingModule,
  ],
})
export class CarRushModule {
}
