import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GalacticWarComponent} from './galactic-war.component';

const routes: Routes = [
  {
    path: '',
    component: GalacticWarComponent,
    children: [
      {
        path: '',
        component: GalacticWarComponent,
        // resolve: {games: GamesResolverService},
      },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class GalacticWarRoutingModule {

}
