import {Component, OnDestroy, OnInit} from '@angular/core';
import {DynamicScriptLoaderService} from '@shared/services/DynamicScriptLoaderService';
import {ActivatedRoute, Router} from '@angular/router';
import {reloadOnce} from '@shared/shared';
import {DialogService} from '@shared/services/confirm-dialog.service';

declare function initial(): any;

declare function stopGalcticWatAudio(): any;

@Component({
  selector: 'app-galactic-war',
  templateUrl: './galactic-war.component.html',
  styleUrls: ['./galactic-war.component.css']
})
export class GalacticWarComponent implements OnInit, OnDestroy {
  scripts: string[] = ['galactic-war-1', 'galactic-war-2', 'galactic-war-3'];
  timeout;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private dialogService: DialogService,
              private dynamicScriptLoader: DynamicScriptLoaderService) {
  }

  ngOnInit(): void {
    reloadOnce();
    this.checkFinishDate();
    this.dynamicScriptLoader.load(this.scripts).then(data => {
      setTimeout(() => {
        initial();
      }, 1);
    }).catch(error => console.log(error));

  }
  private checkFinishDate(): void {
    const finishDate = localStorage.getItem('galactic-war');
    const finisTime = new Date(finishDate);
    const now = new Date();
    if (now > finisTime) {
      this.router.navigate(['games']);
    } else {
      const diff = (finisTime.getTime() - now.getTime()) / 1000;
      const diffSeconds = Math.abs(Math.round(diff));
      this.timeout = setTimeout(() => {
        this.dialogService.openFinishTimeDialog();
      }, diffSeconds * 1000);
    }
  }

  ngOnDestroy(): void {
    clearTimeout(this.timeout);
    stopGalcticWatAudio();
  }
}
