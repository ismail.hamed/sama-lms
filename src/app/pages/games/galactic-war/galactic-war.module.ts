import {NgModule} from '@angular/core';
import {GalacticWarComponent} from './galactic-war.component';
import {GalacticWarRoutingModule} from './galactic-war-routing.module';

@NgModule({
  declarations: [
    GalacticWarComponent,
  ],
  imports: [
    GalacticWarRoutingModule,
  ],
})
export class GalacticWarModule {
}
