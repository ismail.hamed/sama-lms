import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {AvailableGames, Game, UserGame} from '@shared/models/game.model';
declare function stopAudio(): any;

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css'],
})
export class GameListComponent implements OnInit {
  availableGames: AvailableGames;

  constructor(private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    setTimeout(() => {
      stopAudio();
    }, 501);
    this.route.data.subscribe((data: Data) => {
        this.availableGames = data.games;
      }
    );
  }

  getUserGame(game: Game): UserGame {
    return this.availableGames.usergames.find(x => x.game_id === game.id);
  }

  bugGame(balance): void {
    this.availableGames.balance = balance;
  }
}
