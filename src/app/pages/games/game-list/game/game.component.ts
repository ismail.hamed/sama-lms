import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Game, UserGame} from '@shared/models/game.model';
import {GamesService} from '@shared/services/games.service';
import {DialogService} from '@shared/services/confirm-dialog.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ActivityEnum} from '@shared/shared';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  @Input() game: Game;
  @Input() userGame: UserGame;
  @Input() balance: number;
  @Output() buyGame = new EventEmitter();
  canBuyGame = false;
  canStartGame = false;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private gamesService: GamesService,
              private dialogService: DialogService) {
  }

  ngOnInit(): void {
    if (this.balance >= this.game.price && (!this.userGame || this.userGame?.attempted === 0)) {
      this.canBuyGame = true;
    }
    if (this.userGame?.attempted > 0) {
      this.canStartGame = true;
    }
  }

  play(): void {
    if (this.canStartGame) {
      this.gamesService.updateAttemptedGame(this.game.id).subscribe(res => {
        if (res.message) {
          this.dialogService.open(res.message);
        } else {
          if (res === true) {
            this.userGame.attempted -= 1;
            const gameName = this.getGameName(this.game.id);
            this.router.navigate([gameName], {relativeTo: this.route});
            this.addTimeToFinishGame(gameName);
          }
        }
      });
    }
  }

  buy(): void {
    if (this.canBuyGame) {
      this.gamesService.buyGame(this.game.id).subscribe(res => {
        if (res.message) {
          this.dialogService.open(res.message);
        } else {
          debugger
          this.userGame = res?.usergames;
          this.balance = res?.balance;
          this.buyGame.emit(this.balance);
          this.canStartGame = true;
          this.canBuyGame = false;
          this.dialogService.open(`تم شراء اللعبة بنجاح لديك ${this.userGame.attempted} محاولات`);

        }
      });
    }
  }

  getGameName(id): string {
    let name;
    switch (id) {
      case 1: {
        name = 'monster';
        break;
      }
      case 2: {
        name = 'happy-cups';
        break;
      }
      case 3: {
        name = 'tetris';
        break;
      }
      case 4: {
        name = 'galactic-war';
        break;
      }
      case 5: {
        name = 'soccer';
        break;
      }
      case 6: {
        name = 'sorcerer';
        break;
      }
      case 7: {
        name = 'katana-fruits';
        break;
      }
      case 8: {
        name = 'car-rush';
        break;
      }
      case 9: {
        name = 'star-defense';
        break;
      }
      case 10: {
        name = 'swipe-ball';
        break;
      }
    }
    return name;
  }

  private addTimeToFinishGame(name: string): void {
    const d1 = new Date();
    const d2 = new Date(d1);
    d2.setMinutes(d1.getMinutes() + 5);
    localStorage.setItem(name, d2.toString());
  }
}
