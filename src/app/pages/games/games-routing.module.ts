import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GamesComponent} from './games.component';
import {GameListComponent} from './game-list/game-list.component';
import {GamesResolverService} from '@shared/resolvers/games-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: GamesComponent,
    children: [
      {
        path: '',
        component: GameListComponent,
        resolve: {games: GamesResolverService},
      },
      {
        path: 'monster',
        loadChildren: () => import('./monster/monster.module')
          .then(m => m.MonsterModule),
      },
      {
        path: 'happy-cups',
        loadChildren: () => import('./happy-cups/happy-cups.module')
          .then(m => m.HappyCupsModule),
      },
      {
        path: 'galactic-war',
        loadChildren: () => import('./galactic-war/galactic-war.module')
          .then(m => m.GalacticWarModule),
      },
      {
        path: 'tetris',
        loadChildren: () => import('./tetris/tetris.module')
          .then(m => m.TetrisModule),
      },
      {
        path: 'soccer',
        loadChildren: () => import('./soccer/soccer.module')
          .then(m => m.SoccerModule),
      },
      {
        path: 'sorcerer',
        loadChildren: () => import('./sorcerer/sorcerer.module')
          .then(m => m.SorcererModule),
      },
      {
        path: 'katana-fruits',
        loadChildren: () => import('./katana-fruits/katana-fruits.module')
          .then(m => m.KatanaFruitsModule),
      },
      {
        path: 'car-rush',
        loadChildren: () => import('./car-rush/car-rush-routing.module')
          .then(m => m.CarRushRoutingModule),
      },
      {
        path: 'star-defense',
        loadChildren: () => import('./star-defense/star-defense-routing.module')
          .then(m => m.StarDefenseRoutingModule),
      },
      {
        path: 'swipe-ball',
        loadChildren: () => import('./swipe-ball/swipe-ball-routing.module')
          .then(m => m.SwipeBallRoutingModule),
      },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class GamesRoutingModule {

}
