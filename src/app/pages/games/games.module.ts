import {NgModule} from '@angular/core';
import {SharedModule} from '@shared/shared.module';
import {GamesRoutingModule} from './games-routing.module';
import {GamesComponent} from './games.component';
import {GameListComponent} from './game-list/game-list.component';
import {GameComponent} from './game-list/game/game.component';

@NgModule({
  declarations: [
    GamesComponent,
    GameListComponent,
    GameComponent,
  ],
  imports: [
    GamesRoutingModule,
    SharedModule,
  ],
})
export class GamesModule {
}
