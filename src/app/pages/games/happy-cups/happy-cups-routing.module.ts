import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HappyCupsComponent} from './happy-cups.component';

const routes: Routes = [
  {
    path: '',
    component: HappyCupsComponent,
    children: [
      {
        path: '',
        component: HappyCupsComponent,
        // resolve: {games: GamesResolverService},
      },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class HappyCupsRoutingModule {

}
