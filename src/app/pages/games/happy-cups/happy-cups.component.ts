import {Component, OnDestroy, OnInit} from '@angular/core';
import {DynamicScriptLoaderService} from '@shared/services/DynamicScriptLoaderService';
import {ActivatedRoute, Router} from '@angular/router';
import {reloadOnce} from '@shared/shared';
import {DialogService} from '@shared/services/confirm-dialog.service';

declare function initial(): any;

declare function stopHappyCupsAudio(): any;

@Component({
  selector: 'app-happy-cups',
  templateUrl: './happy-cups.component.html',
  styleUrls: ['./happy-cups.component.css']
})
export class HappyCupsComponent implements OnInit, OnDestroy {
  scripts: string[] = ['happy-cups-1', 'happy-cups-2'];
  timeout;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private dialogService: DialogService,
              private dynamicScriptLoader: DynamicScriptLoaderService) {
  }

  ngOnInit(): void {
    reloadOnce();
    this.checkFinishDate();
    this.dynamicScriptLoader.load(this.scripts).then(data => {
      setTimeout(() => {
        initial();
      }, 1);
    }).catch(error => console.log(error));
  }

  private checkFinishDate(): void {
    const finishDate = localStorage.getItem('happy-cups');
    const finisTime = new Date(finishDate);
    const now = new Date();
    if (now > finisTime) {
      this.router.navigate(['games']);
    } else {
      const diff = (finisTime.getTime() - now.getTime()) / 1000;
      const diffSeconds = Math.abs(Math.round(diff));
      this.timeout = setTimeout(() => {
        this.dialogService.openFinishTimeDialog();
      }, diffSeconds * 1000);
    }
  }

  ngOnDestroy(): void {
    clearTimeout(this.timeout);
    stopHappyCupsAudio();
  }
}
