import {NgModule} from '@angular/core';
import {HappyCupsComponent} from './happy-cups.component';
import {HappyCupsRoutingModule} from './happy-cups-routing.module';

@NgModule({
  declarations: [
    HappyCupsComponent,
  ],
  imports: [
    HappyCupsRoutingModule,
  ],
})
export class HappyCupsModule {
}
