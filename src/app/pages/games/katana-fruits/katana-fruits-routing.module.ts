import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {KatanaFruitsComponent} from './katana-fruits.component';

const routes: Routes = [
  {
    path: '',
    component: KatanaFruitsComponent,
    children: [
      {
        path: '',
        component: KatanaFruitsComponent,
        // resolve: {games: GamesResolverService},
      },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class KatanaFruitsRoutingModule {

}
