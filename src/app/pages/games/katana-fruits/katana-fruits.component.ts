import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DynamicScriptLoaderService} from '@shared/services/DynamicScriptLoaderService';
import {reloadOnce} from '@shared/shared';
import {DialogService} from '@shared/services/confirm-dialog.service';

declare function initial(): any;

declare function stopKatanaFruitsAudio(): any;

@Component({
  selector: 'app-katana-fruits',
  templateUrl: './katana-fruits.component.html',
  styleUrls: ['./katana-fruits.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class KatanaFruitsComponent implements OnInit, OnDestroy {
  scripts: string[] = [
    'katana-fruits-1', 'katana-fruits-2', 'katana-fruits-3', 'katana-fruits-4',
  ];
  timeout;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private dialogService: DialogService,
              private dynamicScriptLoader: DynamicScriptLoaderService) {
  }

  ngOnInit(): void {
    reloadOnce();
    this.checkFinishDate();
    this.dynamicScriptLoader.load(this.scripts).then(data => {
      setTimeout(() => {
        initial();
      }, 1);
    });
  }
  private checkFinishDate(): void {
    const finishDate = localStorage.getItem('katana-fruits');
    const finisTime = new Date(finishDate);
    const now = new Date();
    if (now > finisTime) {
      this.router.navigate(['games']);
    } else {
      const diff = (finisTime.getTime() - now.getTime()) / 1000;
      const diffSeconds = Math.abs(Math.round(diff));
      this.timeout = setTimeout(() => {
        this.dialogService.openFinishTimeDialog();
      }, diffSeconds * 1000);
    }
  }

  ngOnDestroy(): void {
    clearTimeout(this.timeout);
    stopKatanaFruitsAudio();
  }

}
