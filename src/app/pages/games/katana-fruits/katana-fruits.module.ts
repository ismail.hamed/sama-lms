import {NgModule} from '@angular/core';
import {KatanaFruitsComponent} from './katana-fruits.component';
import {KatanaFruitsRoutingModule} from './katana-fruits-routing.module';

@NgModule({
  declarations: [
    KatanaFruitsComponent
  ],
  imports: [
    KatanaFruitsRoutingModule,
  ],
})
export class KatanaFruitsModule {
}
