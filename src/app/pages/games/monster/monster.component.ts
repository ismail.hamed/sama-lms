import {Component, OnDestroy, OnInit} from '@angular/core';
import {DynamicScriptLoaderService} from '@shared/services/DynamicScriptLoaderService';
import {ActivatedRoute, Router} from '@angular/router';
import {reloadOnce} from '@shared/shared';
import {DialogService} from '@shared/services/confirm-dialog.service';

declare function initial(): any;

declare function stopMonsterAudio(): any;

@Component({
  selector: 'app-monster',
  templateUrl: './monster.component.html',
  styleUrls: ['./monster.component.css']
})
export class MonsterComponent implements OnInit, OnDestroy {
  scripts: string[] = ['monster-1', 'monster-2', 'monster-3'];
  timeout;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private dialogService: DialogService,
              private dynamicScriptLoader: DynamicScriptLoaderService) {
  }

  ngOnInit(): void {
    reloadOnce();
    this.checkFinishDate();
    this.dynamicScriptLoader.load(this.scripts).then(data => {
      setTimeout(() => {
        initial();
      }, 1);
    });
  }
  private checkFinishDate(): void {
    const finishDate = localStorage.getItem('monster');
    const finisTime = new Date(finishDate);
    const now = new Date();
    if (now > finisTime) {
      this.router.navigate(['games']);
    } else {
      const diff = (finisTime.getTime() - now.getTime()) / 1000;
      const diffSeconds = Math.abs(Math.round(diff));
      this.timeout = setTimeout(() => {
        this.dialogService.openFinishTimeDialog();
      }, diffSeconds * 1000);
    }
  }

  ngOnDestroy(): void {
    clearTimeout(this.timeout);
    stopMonsterAudio();
  }
}
