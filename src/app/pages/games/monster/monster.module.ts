import {NgModule} from '@angular/core';
import {MonsterComponent} from './monster.component';
import {MonsterRoutingModule} from './monster-routing.module';

@NgModule({
  declarations: [
    MonsterComponent,
  ],
  imports: [
    MonsterRoutingModule,
  ],
})
export class MonsterModule {
}
