import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SoccerComponent} from './soccer.component';

const routes: Routes = [
  {
    path: '',
    component: SoccerComponent,
    children: [
      {
        path: '',
        component: SoccerComponent,
        // resolve: {games: GamesResolverService},
      },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SoccerRoutingModule {

}
