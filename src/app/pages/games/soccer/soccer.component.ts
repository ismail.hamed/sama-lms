import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DynamicScriptLoaderService} from '@shared/services/DynamicScriptLoaderService';
import {reloadOnce} from '@shared/shared';
import {DialogService} from '@shared/services/confirm-dialog.service';

declare function initial(): any;

declare function stopSoccerAudio(): any;

@Component({
  selector: 'app-soccer',
  templateUrl: './soccer.component.html',
  styleUrls: ['./soccer.component.css'],
  encapsulation: ViewEncapsulation.None

})
export class SoccerComponent implements OnInit, OnDestroy {
  scripts: string[] = [
    'soccer-1', 'soccer-2', 'soccer-3', 'soccer-4', 'soccer-5',
    'soccer-6', 'soccer-7', 'soccer-8', 'soccer-9', 'soccer-10',
    'soccer-11', 'soccer-12', 'soccer-13', 'soccer-14', 'soccer-15',
    'soccer-16'
  ];
  timeout;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private dialogService: DialogService,
              private dynamicScriptLoader: DynamicScriptLoaderService) {
  }

  ngOnInit(): void {
    reloadOnce();
    this.checkFinishDate();
    this.dynamicScriptLoader.load(this.scripts).then(data => {
      setTimeout(() => {
        initial();
      }, 1);
    });
  }
  private checkFinishDate(): void {
    const finishDate = localStorage.getItem('soccer');
    const finisTime = new Date(finishDate);
    const now = new Date();
    if (now > finisTime) {
      this.router.navigate(['games']);
    } else {
      const diff = (finisTime.getTime() - now.getTime()) / 1000;
      const diffSeconds = Math.abs(Math.round(diff));
      this.timeout = setTimeout(() => {
        this.dialogService.openFinishTimeDialog();
      }, diffSeconds * 1000);
    }
  }

  ngOnDestroy(): void {
    clearTimeout(this.timeout);
    stopSoccerAudio();
  }
}
