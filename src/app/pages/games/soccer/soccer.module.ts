import {NgModule} from '@angular/core';
import {SoccerComponent} from './soccer.component';
import {SoccerRoutingModule} from './soccer-routing.module';

@NgModule({
  declarations: [
    SoccerComponent
  ],
  imports: [
    SoccerRoutingModule,
  ],
})
export class SoccerModule {
}
