import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SorcererComponent} from './sorcerer.component';

const routes: Routes = [
  {
    path: '',
    component: SorcererComponent,
    children: [
      {
        path: '',
        component: SorcererComponent,
        // resolve: {games: GamesResolverService},
      },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SorcererRoutingModule {

}
