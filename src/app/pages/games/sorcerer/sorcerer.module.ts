import {NgModule} from '@angular/core';
import {SorcererComponent} from './sorcerer.component';
import {SorcererRoutingModule} from './sorcerer-routing.module';

@NgModule({
  declarations: [
    SorcererComponent
  ],
  imports: [
    SorcererRoutingModule,
  ],
})
export class SorcererModule {
}
