import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StarDefenseComponent} from './star-defense.component';

const routes: Routes = [
  {
    path: '',
    component: StarDefenseComponent,
    children: [
      {
        path: '',
        component: StarDefenseComponent,
        // resolve: {games: GamesResolverService},
      },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class StarDefenseRoutingModule {

}
