import {NgModule} from '@angular/core';
import {StarDefenseComponent} from './star-defense.component';
import {StarDefenseRoutingModule} from './star-defense-routing.module';

@NgModule({
  declarations: [
    StarDefenseComponent,
  ],
  imports: [
    StarDefenseRoutingModule,
  ],
})
export class StarDefenseModule {
}
