import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SwipeBallComponent} from './swipe-ball.component';

const routes: Routes = [
  {
    path: '',
    component: SwipeBallComponent,
    children: [
      {
        path: '',
        component: SwipeBallComponent,
        // resolve: {games: GamesResolverService},
      },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SwipeBallRoutingModule {

}
