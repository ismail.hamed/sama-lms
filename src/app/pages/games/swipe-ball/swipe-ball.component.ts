import {Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DynamicScriptLoaderService} from '@shared/services/DynamicScriptLoaderService';
import {reloadOnce} from '@shared/shared';
import {DialogService} from '@shared/services/confirm-dialog.service';

declare function initial(): any;

declare function stopSwipeBallAudio(): any;

@Component({
  selector: 'app-swipe-ball',
  templateUrl: './swipe-ball.component.html',
  styleUrls: ['./swipe-ball.component.css'],
  encapsulation: ViewEncapsulation.None

})
export class SwipeBallComponent implements OnInit, OnDestroy {
  scripts: string[] = ['swipe-ball-1', 'swipe-ball-2', 'swipe-ball-3', 'swipe-ball-4', 'swipe-ball-5'];
  @ViewChild('c2canvasdiv') public c2canvasdiv: ElementRef;
  timeout;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private dialogService: DialogService,
              private dynamicScriptLoader: DynamicScriptLoaderService) {
  }

  ngOnInit(): void {
    reloadOnce();
    this.checkFinishDate();
    this.dynamicScriptLoader.load(this.scripts).then(data => {
      setTimeout(() => {
        const canvas = document.getElementsByTagName('canvas')[0];
        this.c2canvasdiv.nativeElement.appendChild(canvas);
      }, 1);
    });
  }
  private checkFinishDate(): void {
    const finishDate = localStorage.getItem('swipe-ball');
    const finisTime = new Date(finishDate);
    const now = new Date();
    if (now > finisTime) {
      this.router.navigate(['games']);
    } else {
      const diff = (finisTime.getTime() - now.getTime()) / 1000;
      const diffSeconds = Math.abs(Math.round(diff));
      this.timeout = setTimeout(() => {
        this.dialogService.openFinishTimeDialog();
      }, diffSeconds * 1000);
    }
  }

  ngOnDestroy(): void {
    clearTimeout(this.timeout);
    stopSwipeBallAudio();
  }
}
