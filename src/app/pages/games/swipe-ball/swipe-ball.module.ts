import {NgModule} from '@angular/core';
import {SwipeBallComponent} from './swipe-ball.component';
import {SwipeBallRoutingModule} from './swipe-ball-routing.module';

@NgModule({
  declarations: [
    SwipeBallComponent
  ],
  imports: [
    SwipeBallRoutingModule,
  ],
})
export class SwipeBallModule {
}
