import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {DynamicScriptLoaderService} from '@shared/services/DynamicScriptLoaderService';
import {ActivatedRoute, Router} from '@angular/router';
import {reloadOnce} from '@shared/shared';
import {DialogService} from '@shared/services/confirm-dialog.service';

declare function initial(): any;

declare function stopMonsterAudio(): any;

@Component({
  selector: 'app-tetris',
  templateUrl: './tetris.component.html',
  styleUrls: ['./tetris.component.css']
})
export class TetrisComponent implements OnInit, OnDestroy {
  scripts: string[] = ['tetris-1', 'tetris-2', 'tetris-3', 'tetris-4'];
  @ViewChild('c2canvasdiv') public c2canvasdiv: ElementRef;
  timeout;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private dialogService: DialogService,
              private dynamicScriptLoader: DynamicScriptLoaderService) {
  }

  ngOnInit(): void {
    reloadOnce();
    this.checkFinishDate();
    this.dynamicScriptLoader.load(this.scripts).then(data => {
      setTimeout(() => {
        const canvas = document.getElementsByTagName('canvas')[0];
        this.c2canvasdiv.nativeElement.appendChild(canvas);
      }, 1);
    }).catch(error => console.log(error));

  }
  private checkFinishDate(): void {
    const finishDate = localStorage.getItem('tetris');
    const finisTime = new Date(finishDate);
    const now = new Date();
    if (now > finisTime) {
      this.router.navigate(['games']);
    } else {
      const diff = (finisTime.getTime() - now.getTime()) / 1000;
      const diffSeconds = Math.abs(Math.round(diff));
      this.timeout = setTimeout(() => {
        this.dialogService.openFinishTimeDialog();
      }, diffSeconds * 1000);
    }
  }

  ngOnDestroy(): void {
    clearTimeout(this.timeout);
    // stopMonsterAudio();
  }
}
