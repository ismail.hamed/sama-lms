import {NgModule} from '@angular/core';
import {TetrisComponent} from './tetris.component';
import {TetrisRoutingModule} from './tetris-routing.module';

@NgModule({
  declarations: [
    TetrisComponent,
  ],
  imports: [
    TetrisRoutingModule,
  ],
})
export class TetrisModule {
}
