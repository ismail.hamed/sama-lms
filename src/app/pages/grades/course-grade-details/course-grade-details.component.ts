import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {Gradeitem, Usergrade} from '@shared/models/grade-items.model';


interface Transaction {
  item: string;
  cost: number;
}

@Component({
  selector: 'app-course-grade-details',
  templateUrl: './course-grade-details.component.html',
  styleUrls: ['./course-grade-details.component.css']
})
export class CourseGradeDetailsComponent implements OnInit {
  gradeItems: Usergrade;
  categoryItems: Gradeitem[];

  displayedColumns: string[] = ['itemname', 'graderaw'];

  constructor(private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.data.subscribe((data: Data) => {
        this.gradeItems = data.grade?.usergrades[0];
        this.categoryItems = this.gradeItems.gradeitems.filter((item => {
          return item.itemtype === 'category';
        }));
      }
    );
  }

  getActivityItems(category): Gradeitem[] {
    const items = this.gradeItems.gradeitems.filter((item => {
      return item.itemtype !== 'category' && item.categoryid === category.iteminstance;
    }));
    return items;
  }

  getTotalCost(): number {
    return 0;
    // return this.transactions.map(t => t.cost).reduce((acc, value) => acc + value, 0);
  }

}
