import {Component, OnInit} from '@angular/core';
import {Grades} from '@shared/models/grade.model';
import {ActivatedRoute, Data, Router} from '@angular/router';

@Component({
  selector: 'app-grades-bar',
  templateUrl: './grades-bar.component.html',
  styleUrls: ['./grades-bar.component.scss']
})
export class GradesBarComponent implements OnInit {
  courseGrades: Grades;

  constructor(private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.data.subscribe((data: Data) => {
        this.courseGrades = data.grades;
      }
    );
  }
}
