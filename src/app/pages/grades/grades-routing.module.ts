import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GradesComponent} from './grades.component';
import {GradesResolverService} from '@shared/resolvers/grades-resolver.service';
import {CourseGradeDetailsComponent} from './course-grade-details/course-grade-details.component';
import {GradeDetailsResolverService} from '@shared/resolvers/grade-details-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: GradesComponent,
    resolve: {grades: GradesResolverService},
    children: [
      {
        path: ':courseid',
        component: CourseGradeDetailsComponent,
        resolve: {grade: GradeDetailsResolverService},
      },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class GradesRoutingModule {

}
