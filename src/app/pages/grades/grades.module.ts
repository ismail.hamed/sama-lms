import {NgModule} from '@angular/core';
import {SharedModule} from '@shared/shared.module';
import {GradesComponent} from './grades.component';
import {GradesRoutingModule} from './grades-routing.module';
import { CourseGradeDetailsComponent } from './course-grade-details/course-grade-details.component';
import {GradesBarComponent} from './grades-bar/grades-bar.component';
import {MatChipsModule} from '@angular/material/chips';

@NgModule({
  declarations: [
    GradesComponent,
    CourseGradeDetailsComponent,
    GradesBarComponent,
  ],
    imports: [
        GradesRoutingModule,
        SharedModule,
    ],
})
export class GradesModule {
}
