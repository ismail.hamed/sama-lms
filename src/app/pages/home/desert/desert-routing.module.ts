import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DesertComponent} from './desert.component';
import {DesertsComponent} from './deserts/deserts.component';
import {StationsResolverService} from '@shared/resolvers/stations-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: DesertComponent,
    children: [
      {
        path: '',
        component: DesertsComponent,
        resolve: {course: StationsResolverService},
      },
      {
        path: ':courseId/lesson/:lessonId',
        loadChildren: () => import('./deserts/desert-lesson/desert-lesson.module')
          .then(m => m.DesertLessonModule),
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DesertRoutingModule {

}
