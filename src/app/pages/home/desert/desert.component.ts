import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-desert',
  template: `
    <router-outlet>
    </router-outlet>
  `,
})
export class DesertComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}
