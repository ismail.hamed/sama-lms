import {NgModule} from '@angular/core';
import {DesertRoutingModule} from './desert-routing.module';
import {SharedModule} from '@shared/shared.module';
import {DesertComponent} from './desert.component';
import {DesertsComponent} from './deserts/deserts.component';

@NgModule({
  declarations: [
    DesertComponent,
    DesertsComponent,
  ],
  imports: [
    DesertRoutingModule,
    SharedModule,
  ],
})
export class DesertModule {
}
