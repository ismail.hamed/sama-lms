import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-desert-lesson',
  template: `
    <router-outlet>
    </router-outlet>
  `,
})
export class DesertLessonComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}
