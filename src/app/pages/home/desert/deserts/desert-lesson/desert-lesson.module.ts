import {NgModule} from '@angular/core';
import {DesertLessonRoutingModule} from './desert-lesson-routing.module';
import {SharedModule} from '@shared/shared.module';
import {DesertLessonComponent} from './desert-lesson.component';
import {DesertLessonsComponent} from './desert-lessons/desert-lessons.component';

@NgModule({
  declarations: [
    DesertLessonComponent,
    DesertLessonsComponent
  ],
  imports: [
    DesertLessonRoutingModule,
    SharedModule,
  ],
})
export class DesertLessonModule {
}
