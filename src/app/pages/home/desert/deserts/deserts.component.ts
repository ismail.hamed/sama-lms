import {Component, OnDestroy, OnInit} from '@angular/core';
import {SoundsEnum, StageEnum} from '@shared/shared';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {StationsService} from '@shared/services/stations.service';
import {Course, Lesson} from '@shared/models/lessons.model';
import {DynamicScriptLoaderService} from '@shared/services/DynamicScriptLoaderService';
import {DialogService} from '@shared/services/confirm-dialog.service';

declare function initDeserts(): any;

declare var instance;
declare var exportRoot;
declare var createjs;

declare function playAudio(src): any;

@Component({
  selector: 'app-deserts',
  templateUrl: './deserts.component.html',
  styleUrls: ['./deserts.component.css']
})
export class DesertsComponent implements OnInit, OnDestroy {
  course: Course;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private dialogService: DialogService,
              private dynamicScriptLoader: DynamicScriptLoaderService
  ) {
  }

  ngOnInit(): void {
    playAudio(SoundsEnum.Deserts);
    if (typeof exportRoot !== 'undefined') {
      exportRoot = null;
    }

    this.route.data.subscribe((data: Data) => {
        this.course = data.course;
        this.loadScripts();
      }
    );
  }

  private loadScripts(): void {
    this.dynamicScriptLoader.load(['Deserts']).then(data => {
      setTimeout(() => {
        initDeserts().then(data => {
          this.showAvailableStage();
        });
      }, 1);
    });
  }

  private showAvailableStage(): void {
      const stages = exportRoot?.getChildByName('stages');
      if (stages?.visible) {
        const lessons = this.course?.lessons;
        if (lessons?.length > 0) {
          lessons.forEach((lesson, index) => {
            if (lesson.uservisible) {
              const id = setInterval(() => {
                const lessonNumber = index + 1;
                const stage = stages?.getChildByName(lessonNumber);
                if (stage) {
                  this.showStars(lesson, stage);
                  stage.visible = true;
                  clearInterval(id);
                }
              }, 1);
            }
          });
        }
      }
  }

  private showStars(lesson: Lesson, stage): void {
    const stars = stage.instance;
    if (lesson.totalgrade / 3 <= lesson.usergrade) {
      this.showMiddleStar(stars);
      if (lesson.totalgrade * 2 / 3 <= lesson.usergrade) {
        this.showLeftStar(stars);
      }
      if (lesson.totalgrade <= lesson.usergrade) {
        this.showRightStar(stars);
      }

    }
  }

  event(): void {
    if (instance) {
      if (instance === StageEnum.BACK) {
        this.router.navigate(['']);
      } else {
        const lesson: Lesson = this.course?.lessons[instance - 1];
        if (lesson) {
          if (lesson.uservisible) {
            this.router.navigate([this.course.id, 'lesson', lesson.id], {relativeTo: this.route});
          } else {
            this.dialogService.open(lesson.availabilityinfo);
          }
        }
      }

    }
    instance = '';
  }

  showLeftStar(stars: any): void {
    stars.timeline.addTween(createjs.Tween.get(stars.instance_3).wait(5).to({_off: false}, 0).to({
      scaleX: 0.7987,
      scaleY: 0.7987,
      rotation: 36.6297,
      x: 92,
      y: 29.9
    }, 3).to({regX: -0.1, regY: -0.1, rotation: 195.5884, x: 124.15, y: 30.9}, 3).to({
      _off: true,
      regX: 179.1,
      regY: 40.8,
      scaleX: 1,
      scaleY: 1,
      rotation: 360,
      x: 179.1,
      y: 40.8
    }, 2).wait(0).to({
      _off: false,
      regX: -0.1,
      regY: -0.1,
      scaleX: 0.7987,
      scaleY: 0.7987,
      rotation: 195.5884,
      x: 179.15,
      y: 40.9
    }, 0).wait(1));
  }

  showRightStar(stars: any): void {
    stars.timeline.addTween(createjs.Tween.get(stars.instance_5).wait(5).to({_off: false}, 0).to({
      regX: -0.3,
      regY: -0.1,
      scaleX: 0.7989,
      scaleY: 0.7989,
      rotation: -28.602,
      x: -88.15,
      y: 20
    }, 3).to({rotation: -126.6131, x: -125.7, y: 28.9}, 3).to({
      _off: true,
      regX: -165.8,
      regY: 48.6,
      scaleX: 1,
      scaleY: 1,
      rotation: 0,
      x: -165.8,
      y: 48.6
    }, 2).wait(0).to({
      _off: false,
      regX: -0.3,
      regY: -0.1,
      scaleX: 0.7989,
      scaleY: 0.7989,
      rotation: -126.6131,
      x: -165.7,
      y: 48.9
    }, 0).wait(1));

  }

  showMiddleStar(stars: any): void {
    stars.timeline.addTween(createjs.Tween.get(stars.instance_2).wait(1).to({_off: false}, 0).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 2).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).wait(1));
  }

  ngOnDestroy(): void {
    createjs.Ticker.removeAllEventListeners(); // Note the function name
  }
}
