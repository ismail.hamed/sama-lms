import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home.component';
import {StationsComponent} from './stations/stations.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        component: StationsComponent,
      },
      {
        path: 'deserts',
        loadChildren: () => import('./desert/desert.module')
          .then(m => m.DesertModule),
      },
      {
        path: 'seas',
        loadChildren: () => import('./sea/sea.module')
          .then(m => m.SeaModule),
      },
      {
        path: 'mountains',
        loadChildren: () => import('./mountain/mountain.module')
          .then(m => m.MountainModule),
      },
      {
        path: 'plains',
        loadChildren: () => import('./plain/plain.module')
          .then(m => m.PlainModule),
      },
      {
        path: 'spaces',
        loadChildren: () => import('./space/space.module')
          .then(m => m.SpaceModule),
      },
      {
        path: 'rivers',
        loadChildren: () => import('./river/river.module')
          .then(m => m.RiverModule),
      },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRoutingModule {

}
