import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MountainComponent} from './mountain.component';
import {StationsResolverService} from '@shared/resolvers/stations-resolver.service';
import {MountainsComponent} from './mountains/mountains.component';

const routes: Routes = [
  {
    path: '',
    component: MountainComponent,
    children: [
      {
        path: '',
        component: MountainsComponent,
        resolve: {course: StationsResolverService},
      },
      {
        path: ':courseId/lesson/:lessonId',
        loadChildren: () => import('./mountains/mountain-lesson/mountain-lesson.module')
          .then(m => m.MountainLessonModule),
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class MountainRoutingModule {

}
