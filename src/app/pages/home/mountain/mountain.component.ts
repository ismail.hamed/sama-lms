import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-mountain',
  template: `
    <router-outlet>
    </router-outlet>
  `,
})
export class MountainComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}
