import {NgModule} from '@angular/core';
import {MountainRoutingModule} from './mountain-routing.module';
import {SharedModule} from '@shared/shared.module';
import {MountainComponent} from './mountain.component';
import {MountainsComponent} from './mountains/mountains.component';

@NgModule({
  declarations: [
    MountainComponent,
    MountainsComponent,
  ],
  imports: [
    MountainRoutingModule,
    SharedModule,
  ],
})
export class MountainModule {
}
