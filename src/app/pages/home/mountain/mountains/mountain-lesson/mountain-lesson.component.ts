import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-mountain-lesson',
  template: `
    <router-outlet>
    </router-outlet>
  `,
})
export class MountainLessonComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}
