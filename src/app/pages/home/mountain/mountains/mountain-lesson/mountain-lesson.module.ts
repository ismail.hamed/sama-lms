import {NgModule} from '@angular/core';
import {MountainLessonRoutingModule} from './mountain-lesson-routing.module';
import {SharedModule} from '@shared/shared.module';
import {MountainLessonComponent} from './mountain-lesson.component';
import {MountainLessonsComponent} from './mountain-lessons/mountain-lessons.component';

@NgModule({
  declarations: [
    MountainLessonComponent,
    MountainLessonsComponent
  ],
  imports: [
    MountainLessonRoutingModule,
    SharedModule,
  ],
})
export class MountainLessonModule {
}
