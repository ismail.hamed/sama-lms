import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PlainComponent} from './plain.component';
import {StationsResolverService} from '@shared/resolvers/stations-resolver.service';
import {PlainsComponent} from './plains/plains.component';

const routes: Routes = [
  {
    path: '',
    component: PlainComponent,
    children: [
      {
        path: '',
        component: PlainsComponent,
        resolve: {course: StationsResolverService},
      },
      {
        path: ':courseId/lesson/:lessonId',
        loadChildren: () => import('./plains/plain-lesson/plain-lesson.module')
          .then(m => m.PlainLessonModule),
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PlainRoutingModule {

}
