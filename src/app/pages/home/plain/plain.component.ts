import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-plain',
  template: `
    <router-outlet>
    </router-outlet>
  `,
})
export class PlainComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}
