import {NgModule} from '@angular/core';
import {PlainRoutingModule} from './plain-routing.module';
import {SharedModule} from '@shared/shared.module';
import {PlainComponent} from './plain.component';
import {PlainsComponent} from './plains/plains.component';

@NgModule({
  declarations: [
    PlainComponent,
    PlainsComponent,
  ],
  imports: [
    PlainRoutingModule,
    SharedModule,
  ],
})
export class PlainModule {
}
