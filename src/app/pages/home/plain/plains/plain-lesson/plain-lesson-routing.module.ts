import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PlainLessonComponent} from './plain-lesson.component';
import {ActivitiesResolverService} from '@shared/resolvers/activities-resolver.service';
import {HvpComponent} from '@shared/components/hvp/hvp.component';
import {HvpResolverService} from '@shared/resolvers/hvp-resolver.service';
import {PlainLessonsComponent} from './plain-lessons/plain-lessons.component';
import {AssignmentComponent} from '@shared/components/assignment/assignment.component';
import {AssignmentResolverService} from '@shared/resolvers/assignment-resolver.service';
import {ColoringBoardAssignmentComponent} from '@shared/components/coloring-board-assignment/coloring-board-assignment.component';

const routes: Routes = [
  {
    path: '',
    component: PlainLessonComponent,
    children: [
      {
        path: '',
        component: PlainLessonsComponent,
        resolve: {lesson: ActivitiesResolverService},
      },
      {
        path: 'hvp/:id',
        component: HvpComponent,
        resolve: {hvpActivity: HvpResolverService},
      },
      {
        path: 'recording-assign/:id',
        component: AssignmentComponent,
        resolve: {assignmentActivity: AssignmentResolverService},
      },
      {
        path: 'coloring-board-assign/:id',
        component: ColoringBoardAssignmentComponent,
        resolve: {assignmentActivity: AssignmentResolverService},
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PlainLessonRoutingModule {

}
