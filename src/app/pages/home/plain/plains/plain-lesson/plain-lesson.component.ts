import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-plain-lesson',
  template: `
    <router-outlet>
    </router-outlet>
  `,
})
export class PlainLessonComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}
