import {NgModule} from '@angular/core';
import {PlainLessonRoutingModule} from './plain-lesson-routing.module';
import {SharedModule} from '@shared/shared.module';
import {PlainLessonComponent} from './plain-lesson.component';
import {PlainLessonsComponent} from './plain-lessons/plain-lessons.component';

@NgModule({
  declarations: [
    PlainLessonComponent,
    PlainLessonsComponent
  ],
  imports: [
    PlainLessonRoutingModule,
    SharedModule,
  ],
})
export class PlainLessonModule {
}
