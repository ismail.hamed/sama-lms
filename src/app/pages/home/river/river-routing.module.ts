import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RiverComponent} from './river.component';
import {StationsResolverService} from '@shared/resolvers/stations-resolver.service';
import {RiversComponent} from './rivers/rivers.component';

const routes: Routes = [
  {
    path: '',
    component: RiverComponent,
    children: [
      {
        path: '',
        component: RiversComponent,
        resolve: {course: StationsResolverService},
      },
      {
        path: ':courseId/lesson/:lessonId',
        loadChildren: () => import('./rivers/river-lesson/river-lesson.module')
          .then(m => m.RiverLessonModule),
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RiverRoutingModule {

}
