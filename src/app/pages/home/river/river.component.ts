import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-river',
  template: `
    <router-outlet>
    </router-outlet>
  `,
})
export class RiverComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}
