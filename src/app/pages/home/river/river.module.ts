import {NgModule} from '@angular/core';
import {RiverRoutingModule} from './river-routing.module';
import {SharedModule} from '@shared/shared.module';
import {RiverComponent} from './river.component';
import {RiversComponent} from './rivers/rivers.component';

@NgModule({
  declarations: [
    RiverComponent,
    RiversComponent,
  ],
  imports: [
    RiverRoutingModule,
    SharedModule,
  ],
})
export class RiverModule {
}
