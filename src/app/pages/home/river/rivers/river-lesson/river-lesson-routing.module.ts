import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RiverLessonComponent} from './river-lesson.component';
import {ActivitiesResolverService} from '@shared/resolvers/activities-resolver.service';
import {HvpResolverService} from '@shared/resolvers/hvp-resolver.service';
import {RiverLessonsComponent} from './river-lessons/river-lessons.component';
import {HvpComponent} from '@shared/components/hvp/hvp.component';
import {AssignmentComponent} from '@shared/components/assignment/assignment.component';
import {AssignmentResolverService} from '@shared/resolvers/assignment-resolver.service';
import {ColoringBoardAssignmentComponent} from '@shared/components/coloring-board-assignment/coloring-board-assignment.component';

const routes: Routes = [
  {
    path: '',
    component: RiverLessonComponent,
    children: [
      {
        path: '',
        component: RiverLessonsComponent,
        resolve: {lesson: ActivitiesResolverService},
      },
      {
        path: 'hvp/:id',
        component: HvpComponent,
        resolve: {hvpActivity: HvpResolverService},
      },
      {
        path: 'recording-assign/:id',
        component: AssignmentComponent,
        resolve: {assignmentActivity: AssignmentResolverService},
      },
      {
        path: 'coloring-board-assign/:id',
        component: ColoringBoardAssignmentComponent,
        resolve: {assignmentActivity: AssignmentResolverService},
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RiverLessonRoutingModule {

}
