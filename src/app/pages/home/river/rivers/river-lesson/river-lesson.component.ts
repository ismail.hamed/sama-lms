import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-river-lesson',
  template: `
    <router-outlet>
    </router-outlet>
  `,
})
export class RiverLessonComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}
