import {NgModule} from '@angular/core';
import {RiverLessonRoutingModule} from './river-lesson-routing.module';
import {SharedModule} from '@shared/shared.module';
import {RiverLessonComponent} from './river-lesson.component';
import {RiverLessonsComponent} from './river-lessons/river-lessons.component';

@NgModule({
  declarations: [
    RiverLessonComponent,
    RiverLessonsComponent
  ],
  imports: [
    RiverLessonRoutingModule,
    SharedModule,
  ],
})
export class RiverLessonModule {
}
