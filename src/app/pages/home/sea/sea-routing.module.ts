import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SeaComponent} from './sea.component';
import {StationsResolverService} from '@shared/resolvers/stations-resolver.service';
import {SeasComponent} from './seas/seas.component';

const routes: Routes = [
  {
    path: '',
    component: SeaComponent,
    children: [
      {
        path: '',
        component: SeasComponent,
        resolve: {course: StationsResolverService},
      },
      {
        path: ':courseId/lesson/:lessonId',
        loadChildren: () => import('./seas/sea-lesson/sea-lesson.module')
          .then(m => m.SeaLessonModule),
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SeaRoutingModule {

}
