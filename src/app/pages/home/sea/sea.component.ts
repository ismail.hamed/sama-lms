import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-sea',
  template: `
    <router-outlet>
    </router-outlet>
  `,
})
export class SeaComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}
