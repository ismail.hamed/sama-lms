import {NgModule} from '@angular/core';
import {SeaRoutingModule} from './sea-routing.module';
import {SharedModule} from '@shared/shared.module';
import {SeaComponent} from './sea.component';
import {SeasComponent} from './seas/seas.component';

@NgModule({
  declarations: [
    SeaComponent,
    SeasComponent,
  ],
  imports: [
    SeaRoutingModule,
    SharedModule,
  ],
})
export class SeaModule {
}
