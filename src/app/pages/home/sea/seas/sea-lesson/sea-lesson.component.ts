import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-sea-lesson',
  template: `
    <router-outlet>
    </router-outlet>
  `,
})
export class SeaLessonComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}
