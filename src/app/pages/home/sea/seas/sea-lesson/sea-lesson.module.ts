import {NgModule} from '@angular/core';
import {SeaLessonRoutingModule} from './sea-lesson-routing.module';
import {SharedModule} from '@shared/shared.module';
import {SeaLessonComponent} from './sea-lesson.component';
import {SeaLessonsComponent} from './sea-lessons/sea-lessons.component';

@NgModule({
  declarations: [
    SeaLessonComponent,
    SeaLessonsComponent
  ],
  imports: [
    SeaLessonRoutingModule,
    SharedModule,
  ],
})
export class SeaLessonModule {
}
