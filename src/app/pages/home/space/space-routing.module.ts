import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SpaceComponent} from './space.component';
import {StationsResolverService} from '@shared/resolvers/stations-resolver.service';
import {SpacesComponent} from './spaces/spaces.component';

const routes: Routes = [
  {
    path: '',
    component: SpaceComponent,
    children: [
      {
        path: '',
        component: SpacesComponent,
        resolve: {course: StationsResolverService},
      },
      {
        path: ':courseId/lesson/:lessonId',
        loadChildren: () => import('./spaces/space-lesson/space-lesson.module')
          .then(m => m.SpaceLessonModule),
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SpaceRoutingModule {

}
