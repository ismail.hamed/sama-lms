import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-space',
  template: `
    <router-outlet>
    </router-outlet>
  `,
})
export class SpaceComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}
