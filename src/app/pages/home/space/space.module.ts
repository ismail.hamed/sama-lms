import {NgModule} from '@angular/core';
import {SpaceRoutingModule} from './space-routing.module';
import {SharedModule} from '@shared/shared.module';
import {SpaceComponent} from './space.component';
import {SpacesComponent} from './spaces/spaces.component';

@NgModule({
  declarations: [
    SpaceComponent,
    SpacesComponent,
  ],
  imports: [
    SpaceRoutingModule,
    SharedModule,
  ],
})
export class SpaceModule {
}
