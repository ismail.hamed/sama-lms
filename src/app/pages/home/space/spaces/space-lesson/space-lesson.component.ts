import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-space-lesson',
  template: `
    <router-outlet>
    </router-outlet>
  `,
})
export class SpaceLessonComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}
