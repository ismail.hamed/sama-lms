import {NgModule} from '@angular/core';
import {SpaceLessonRoutingModule} from './space-lesson-routing.module';
import {SharedModule} from '@shared/shared.module';
import {SpaceLessonComponent} from './space-lesson.component';
import {SpaceLessonsComponent} from './space-lessons/space-lessons.component';

@NgModule({
  declarations: [
    SpaceLessonComponent,
    SpaceLessonsComponent
  ],
  imports: [
    SpaceLessonRoutingModule,
    SharedModule,
  ],
})
export class SpaceLessonModule {
}
