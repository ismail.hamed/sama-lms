import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {Lesson} from '@shared/models/lessons.model';
import {ActivitiesIcon} from '@shared/components/hvp/ActivitiesIcon';
import {Points} from '@shared/components/hvp/Point';
import {ActivityEnum, ActivityIconEnum, SoundsEnum, stripTashkeel, TypeSubmissionEnum} from '@shared/shared';
import {DynamicScriptLoaderService} from '@shared/services/DynamicScriptLoaderService';
import {DialogService} from '@shared/services/confirm-dialog.service';

declare function initSpaceLessons(): any;

declare var instance;
declare var exportRoot;
declare var createjs;
declare var lib;

declare function playAudio(src): any;

declare function playAudioOnClick(): any;

declare function playAudioOnHover(): any;

@Component({
  selector: 'app-space-lessons',
  templateUrl: './space-lessons.component.html',
  styleUrls: ['./space-lessons.component.css']
})
export class SpaceLessonsComponent implements OnInit, OnDestroy {
  lesson: Lesson;
  waitOne = -3;
  waitTwo = 39;
  activities;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private dialog: DialogService,
              private dynamicScriptLoader: DynamicScriptLoaderService) {
  }

  ngOnInit(): void {
    playAudio(SoundsEnum.SpaceLessons);
    if (typeof exportRoot !== 'undefined') {
      exportRoot = null;
    }
    this.route.data.subscribe((data: Data) => {
      this.lesson = data.lesson;
      this.loadScripts();
    });
  }

  private loadScripts(): void {
    this.dynamicScriptLoader.load(['SpaceLessons']).then(data => {
      setTimeout(() => {
        initSpaceLessons().then(data => {
          const activities = exportRoot?.getChildByName('activities');
          if (activities?.visible) {
            this.activities = activities;
            this.addAnimationToActivities();
          }
          this.addHeaderAndFooterTitle();
          this.addCharacter();
        });
      }, 1);
    });
  }

  private addHeaderAndFooterTitle(): void {
    const lessonTilte = setInterval(() => {
      if (exportRoot && exportRoot?.header && exportRoot?.footer && exportRoot?.num1) {
        clearInterval(lessonTilte);
        exportRoot.header.text = this.lesson.name;
        this.lesson.name = stripTashkeel(this.lesson.name);
        (this.lesson.name?.length < 38) ? exportRoot.header.setTransform(698, 70.95) : null;
        const userGrade = this.lesson?.usergrade ? this.lesson?.usergrade : '';
        exportRoot.footer.text = userGrade;
        exportRoot.num1.text = this.lesson.section + 1;
      }
    }, 100);
  }

  private addAnimationToActivities(): void {
    const activities = this.lesson?.modules;
    if (activities?.length > 0) {
      activities.forEach((activity, index) => {
        if (activity.supportactivity) {
          const prevActivity = activities[index - 1];
          const nextActivity = activities[index + 1];
          if (!activity?.uservisible) {
            if ((prevActivity?.supportactivity && !prevActivity?.uservisible) ||
              (nextActivity?.supportactivity && !nextActivity?.uservisible)) {
              activity.uservisible = true;
              activity.allowClick = false;
            } else if (!prevActivity?.supportactivity && !nextActivity?.supportactivity) {
              activity.uservisible = true;
              activity.allowClick = false;
            }
          }
        }
      });

      const activitiesWithOutSupport = activities.filter(activity =>
        (activity.supportactivity === true && activity.uservisible === true) || activity.supportactivity !== true);
      this.lesson.modules = activitiesWithOutSupport;
      let pointIndex = -1;
      activitiesWithOutSupport.forEach((activity, index) => {
        const id = setInterval(() => {
          const activityIcon = this.getActivityIcon(activity.activityicon);
          if (activityIcon) {
            pointIndex = activity.supportactivity ? pointIndex : pointIndex + 1;
            activityIcon.name = index;
            this.addAnimationToActivity(pointIndex, activityIcon, activity.supportactivity);
            clearInterval(id);
          }
        }, 1);
      });
    }
  }

  private addAnimationToActivity(index: number, activityIcon, isSupportActivity: boolean): void {
    if (index < 12) {
      this.waitOne = this.waitOne + 3;
      this.waitTwo = this.waitTwo - 3;
      const point = isSupportActivity ? Points.allPoints[index][1] : Points.allPoints[index][0];
      if (point) {
        activityIcon.setTransform(point.x, point.y);
        this.activities.timeline.addTween(createjs.Tween.get(activityIcon)
          .wait(this.waitOne).to({_off: false}, 0)
          .wait(this.waitTwo));
      }
    }
  }

  event(): void {
    switch (instance) {
      case ActivityEnum.BACK: {
        this.router.navigate(['stations/spaces']);
        break;
      }
      case ActivityEnum.HOME: {
        this.router.navigate(['']);
        break;
      }
      default:
        if (instance != null) {
          const activity = this.lesson?.modules[instance];
          if (activity) {
            if (activity.uservisible && activity.allowClick === undefined) {
              if (activity.modname === 'assign') {
                let routName = '';
                switch (activity.typesubmission) {
                  case TypeSubmissionEnum.RECORDING: {
                    routName = 'recording-assign';
                    break;
                  }
                  case TypeSubmissionEnum.COLORINGBOARD: {
                    routName = 'coloring-board-assign';
                    break;
                  }
                }
                this.router.navigate([routName, activity.id], {relativeTo: this.route});
              } else {
                this.router.navigate([activity.modname, activity.id], {relativeTo: this.route});
              }
            } else {
              if (!activity.supportactivity) {
                this.dialog.open(activity.availabilityinfo);
              }
            }
          }
        }        break;
    }
    instance = '';
  }

  private addCharacter(): void {
    let userCharacterSelected = localStorage.getItem('userCharacter');
    userCharacterSelected = userCharacterSelected ? userCharacterSelected : '0';
    const characterInterval = setInterval(() => {
      const character = exportRoot?.instance_1.getChildByName(userCharacterSelected);
      if (character?.visible) {
        clearInterval(characterInterval);
        character.alpha = 1;
      }
    }, 500);
  }

  getActivityIcon(icon): any {
    let activityIcon;
    switch (icon) {
      case ActivityIconEnum.ONE: {
        activityIcon = this.activities.instance = new lib.but_mc12();
        break;
      }
      case ActivityIconEnum.TWO: {
        activityIcon = this.activities.instance_2 = new lib.but_mc10();
        break;
      }
      case ActivityIconEnum.THREE: {
        activityIcon = this.activities.instance_1 = new lib.but_mc11();
        break;
      }
      case ActivityIconEnum.FOUR: {
        activityIcon = this.activities.instance_5 = new lib.but_mc7();
        break;
      }
      case ActivityIconEnum.FIVE: {
        activityIcon = this.activities.instance_3 = new lib.but_mc9();
        break;
      }
      case ActivityIconEnum.SIX: {
        activityIcon = this.activities.instance_4 = new lib.but_mc8();
        break;
      }
      case ActivityIconEnum.SEVEN: {
        activityIcon = this.activities.instance_7 = new lib.but_mc5();
        break;
      }
      case ActivityIconEnum.EIGHT: {
        activityIcon = this.activities.instance_13 = new lib.Symbol1copy13();
        break;
      }
      case ActivityIconEnum.NINE: {
        activityIcon = this.activities.instance_9 = new lib.but_mc3();
        break;
      }
      case ActivityIconEnum.TEN: {
        activityIcon = this.activities.instance_17 = new lib.Symbol1copy17();
        break;
      }
      case ActivityIconEnum.ELEVEN: {
        activityIcon = this.activities.instance_16 = new lib.Symbol1copy16();
        break;
      }
      case ActivityIconEnum.TWELVE: {
        activityIcon = this.activities.instance_22 = new lib.Symbol1copy22();
        break;
      }
      case ActivityIconEnum.THIRTEEN: {
        activityIcon = this.activities.instance_23 = new lib.Symbol1copy23();
        break;
      }
      case ActivityIconEnum.FOURTEEN: {
        activityIcon = this.activities.instance_12 = new lib.Symbol1copy12();
        break;
      }
      case ActivityIconEnum.FIFTEEN: {
        activityIcon = this.activities.instance_20 = new lib.Symbol1copy20();
        break;
      }
      case ActivityIconEnum.SIXTEEN: {
        activityIcon = this.activities.instance_19 = new lib.Symbol1copy19();
        break;
      }
      case ActivityIconEnum.SEVENTEEN: {
        activityIcon = this.activities.instance_8 = new lib.but_mc4();
        break;
      }
      case ActivityIconEnum.EIGHTEEN: {
        activityIcon = this.activities.instance_21 = new lib.Symbol1copy21();
        break;
      }
      case ActivityIconEnum.NINETEEN: {
        activityIcon = this.activities.instance_14 = new lib.Symbol1copy14();
        break;
      }
      case ActivityIconEnum.TWENTY: {
        activityIcon = this.activities.instance_18 = new lib.Symbol1copy18();
        break;
      }
      case ActivityIconEnum.TWENTYONE: {
        activityIcon = this.activities.instance_15 = new lib.Symbol1copy15();
        break;
      }
      case ActivityIconEnum.TWENTYTWO: {
        activityIcon = this.activities.instance_6 = new lib.but_mc6();
        break;
      }
      case ActivityIconEnum.TWENTYTHREE: {
        activityIcon = this.activities.instance_10 = new lib.but_mc2();
        break;
      }
      case ActivityIconEnum.TWENTYFOUR: {
        activityIcon = this.activities.instance_11 = new lib.but_mc1();
        break;
      }
    }
    activityIcon._off = true;
    activityIcon.on('click', evt => {
      playAudioOnClick();
      instance = activityIcon.name;
    });
    activityIcon.on('mouseover', evt => {
      playAudioOnHover();
    });
    return activityIcon;
  }

  ngOnDestroy(): void {
    createjs.Ticker.removeAllEventListeners(); // Note the function name
  }
}
