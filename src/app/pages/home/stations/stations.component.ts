import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SoundsEnum, StationEnum} from '@shared/shared';
import {DynamicScriptLoaderService} from '@shared/services/DynamicScriptLoaderService';
import {AuthService} from '@shared/services/auth.service';

declare var createjs;
declare var instance;
declare var exportRoot;

declare function playAudio(src): any;

declare function initStations(): any;

@Component({
  selector: 'app-stations',
  templateUrl: './stations.component.html',
  styleUrls: ['./stations.component.css']
})
export class StationsComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dynamicScriptLoader: DynamicScriptLoaderService,
  ) {
  }

  event(): void {
    switch (instance) {
      case StationEnum.RIVERS: {
        this.router.navigate(['rivers'], {relativeTo: this.route});

        break;
      }
      case StationEnum.MOUNTAINS: {
        this.router.navigate(['mountains'], {relativeTo: this.route});

        break;
      }
      case StationEnum.PLAINS: {
        this.router.navigate(['plains'], {relativeTo: this.route});

        break;
      }
      case StationEnum.SPACES: {
        this.router.navigate(['spaces'], {relativeTo: this.route});

        break;
      }
      case StationEnum.SEAS: {
        this.router.navigate(['seas'], {relativeTo: this.route});
        break;
      }
      case  StationEnum.DESERTS: {
        this.router.navigate(['deserts'], {relativeTo: this.route});
        break;
      }
    }
    instance = '';

  }

  ngOnInit(): void {
    playAudio(SoundsEnum.Stations);
    if (typeof exportRoot !== 'undefined') {
      exportRoot = null;
    }
    this.loadScripts();
  }

  private loadScripts(): void {
    this.dynamicScriptLoader.load(['Stations']).then(data => {
      setTimeout(() => {
        initStations().then(data => {
          this.isLoading = true;
        });
      }, 1);
    }).catch(error => console.log(error));
  }

  ngOnDestroy(): void {
    createjs.Ticker.removeAllEventListeners(); // Note the function name
  }
}
