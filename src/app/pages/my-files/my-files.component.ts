import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {MyFile} from '@shared/models/my-file.model';
import {AuthService} from '@shared/services/auth.service';

@Component({
  selector: 'app-my-files',
  templateUrl: './my-files.component.html',
  styleUrls: ['./my-files.component.css']
})
export class MyFilesComponent implements OnInit {
  myFiles: MyFile[] = [];

  constructor(private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.data.subscribe((data: Data) => {
        this.myFiles = data.myFiles;
      }
    );
  }

  isImage(myFile: MyFile): boolean {
    if (myFile.url.match(/.(jpg|jpeg|png|gif)$/i)) {
      return true;
    }
    return false;
  }

}
