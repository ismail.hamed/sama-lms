import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '@shared/services/auth.service';
import {MyFile} from '@shared/models/my-file.model';

@Component({
  selector: 'app-my-image-file',
  templateUrl: './my-image-file.component.html',
  styleUrls: ['./my-image-file.component.css']
})
export class MyImageFileComponent implements OnInit {
  fileUrl: string;
  myFile: MyFile;

  @Input() set file(myFile: MyFile) {
    this.myFile = myFile;
    this.fileUrl = this.getFileUrl();
  }

  constructor(private auth: AuthService) {
  }

  ngOnInit(): void {
  }

  getFileUrl(): string {
    const token = this.auth.user.value.token;
    const URL = this.myFile.url.split('pluginfile.php');
    const url = URL[0] + 'webservice/pluginfile.php' + URL[1];
    return url + '?token=' + token;
  }
}
