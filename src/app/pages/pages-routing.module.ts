import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {PagesComponent} from './pages.component';
import {AuthGuard} from '@core/guards/auth-guard.service';
import {ProfileComponent} from './profile/profile.component';
import {MyFilesComponent} from './my-files/my-files.component';
import {GamesComponent} from './games/games.component';
import {MyFilesResolverService} from '@shared/resolvers/my-files-resolver.service';


const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'stations',
        pathMatch: 'full',
      },
      {
        path: 'stations',
        loadChildren: () => import('./home/home.module')
          .then(m => m.HomeModule),
      },
      {
        path: 'profile',
        component: ProfileComponent,
      },
      {
        path: 'my-files',
        component: MyFilesComponent,
        resolve: {myFiles: MyFilesResolverService},
      },
      {
        path: 'games',
        loadChildren: () => import('./games/games.module')
          .then(m => m.GamesModule),
      },
      {
        path: 'grades',
        loadChildren: () => import('./grades/grades.module')
          .then(m => m.GradesModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
