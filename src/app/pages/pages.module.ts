import {SharedModule} from '@shared/shared.module';
import {PagesRoutingModule} from './pages-routing.module';
import {NgModule} from '@angular/core';
import {PagesComponent} from './pages.component';
import {ProfileComponent} from './profile/profile.component';
import {CharacterComponent} from './profile/character/character.component';
import { MyFilesComponent } from './my-files/my-files.component';
import {MyImageFileComponent} from './my-files/my-image-file/my-image-file.component';
import { MyAudioFileComponent } from './my-files/my-audio-file/my-audio-file.component';

@NgModule({
  declarations: [
    PagesComponent,
    ProfileComponent,
    CharacterComponent,
    MyFilesComponent,
    MyImageFileComponent,
    MyAudioFileComponent,
  ],
  imports: [
    PagesRoutingModule,
    SharedModule,
  ],
})
export class PagesModule {
}
