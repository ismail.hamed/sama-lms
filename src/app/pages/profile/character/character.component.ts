import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css']
})
export class CharacterComponent implements OnInit {
  @Input() characterName: string;
  @Input() selected = false;

  constructor() {
  }

  ngOnInit(): void {
  }

  select(): void {
    this.selected = true;
  }
}
