import {Component, OnInit} from '@angular/core';
import {SoundsEnum} from '@shared/shared';

declare function playAudio(src): any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  userCharacterSelected;
  characters = [
    {name: 'character-1.png', selected: false},
    {name: 'character-2.png', selected: false},
    {name: 'character-3.png', selected: false},
    {name: 'character-4.png', selected: false},
    {name: 'character-5.png', selected: false},
    {name: 'character-6.png', selected: false},
    {name: 'character-7.png', selected: false},
    {name: 'character-8.png', selected: false},
    {name: 'character-9.png', selected: false},
    {name: 'character-10.png', selected: false},
  ];

  constructor() {
  }

  ngOnInit(): void {
    playAudio(SoundsEnum.Profile);

    this.userCharacterSelected = localStorage.getItem('userCharacter');
    if (this.userCharacterSelected) {
      this.characters[this.userCharacterSelected].selected = true;
    }

  }

  selectItem(index: number): void {
    this.characters.forEach((element) => {
      element.selected = false;
    });
    this.characters[index].selected = true;
    this.userCharacterSelected = index + 1;
    localStorage.setItem('userCharacter', JSON.stringify(index));
  }
}
