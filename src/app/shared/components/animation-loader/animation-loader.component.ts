import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-animation-loader',
  templateUrl: './animation-loader.component.html',
  styleUrls: ['./animation-loader.component.css']
})
export class AnimationLoaderComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
