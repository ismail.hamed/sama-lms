import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements OnInit {
  @Output() clicked = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  event(): void {
    this.clicked.emit(true);
  }
}
