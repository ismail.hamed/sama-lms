import {ActivityIconEnum} from '@shared/shared';

export class ActivitiesIcon {
  activitiesIcon;

  constructor(activitiesIcon) {
    this.activitiesIcon = activitiesIcon;
  }

  getActivityIcon(icon): any {
    let activityIcon;
    switch (icon) {
      case ActivityIconEnum.ONE: {
        activityIcon = this.activitiesIcon.instance_1;
        // activityIcon = activitiesIcon.instance;
        break;
      }
      case ActivityIconEnum.TWO: {
        activityIcon = this.activitiesIcon.instance_7;
        break;
      }
      case ActivityIconEnum.THREE: {
        activityIcon = this.activitiesIcon.instance_3;
        break;
      }
      case ActivityIconEnum.FOUR: {
        activityIcon = this.activitiesIcon.instance_11;
        break;
      }
      case ActivityIconEnum.FIVE: {
        activityIcon = this.activitiesIcon.instance_10;
        break;
      }
      case ActivityIconEnum.SIX: {
        activityIcon = this.activitiesIcon.instance_16;
        break;
      }
      case ActivityIconEnum.SEVEN: {
        activityIcon = this.activitiesIcon.instance_17;
        break;
      }
      case ActivityIconEnum.EIGHT: {
        activityIcon = this.activitiesIcon.instance_6;
        break;
      }
      case ActivityIconEnum.NINE: {
        activityIcon = this.activitiesIcon.instance_14;
        break;
      }
      case ActivityIconEnum.TEN: {
        activityIcon = this.activitiesIcon.instance_13;
        break;
      }
      case ActivityIconEnum.ELEVEN: {
        activityIcon = this.activitiesIcon.instance_2;
        break;
      }
      case ActivityIconEnum.TWELVE: {
        activityIcon = this.activitiesIcon.instance_15;
        break;
      }
      case ActivityIconEnum.THIRTEEN: {
        activityIcon = this.activitiesIcon.instance_8;
        break;
      }
      case ActivityIconEnum.FOURTEEN: {
        activityIcon = this.activitiesIcon.instance_12;
        break;
      }
      case ActivityIconEnum.FIFTEEN: {
        activityIcon = this.activitiesIcon.instance_9;
        break;
      }
      case ActivityIconEnum.SIXTEEN: {
        activityIcon = this.activitiesIcon.instance;
        break;
      }
      case ActivityIconEnum.SEVENTEEN: {
        activityIcon = this.activitiesIcon.instance_4;
        break;
      }
      case ActivityIconEnum.EIGHTEEN: {
        activityIcon = this.activitiesIcon.instance_5;

        break;
      }
    }
    return activityIcon;
  }

}
