import {SetpActivity} from '@shared/models/lessons.model';

export interface File {
  filename: string;
  filepath: string;
  filesize: number;
  fileurl: string;
  timemodified: number;
  mimetype: string;
  isexternalfile: boolean;
}

export interface Filearea {
  area: string;
  files: File[];
}

export interface Plugin {
  type: string;
  name: string;
  fileareas: Filearea[];
}

export interface Submission {
  id: number;
  userid: number;
  attemptnumber: number;
  timecreated: number;
  timemodified: number;
  status: string;
  groupid: number;
  assignment: number;
  latest: number;
  plugins: Plugin[];
}

export interface Lastattempt {
  submission: Submission;
  submissiongroupmemberswhoneedtosubmit: any[];
  submissionsenabled: boolean;
  locked: boolean;
  graded: boolean;
  canedit: boolean;
  caneditowner: boolean;
  cansubmit: boolean;
  extensionduedate?: any;
  blindmarking: boolean;
  gradingstatus: string;
  usergroups: any[];
}

export interface Grade {
  id: number;
  assignment: number;
  userid: number;
  attemptnumber: number;
  timecreated: number;
  timemodified: number;
  grader: number;
  grade: string;
}

export interface Feedback {
  grade: Grade;
  gradefordisplay: string;
  gradeddate: number;
  plugins: any[];
}

export interface Assignment {
  lastattempt: Lastattempt;
  feedback: Feedback;
  warnings: any[];
  introattachments: File[];
  maxgrade: number;
  finalgrade: number;
  intro: string;
  section: string;
  prevactivity: SetpActivity;
  nextactivity: SetpActivity;
}


