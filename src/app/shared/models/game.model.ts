export interface Game {
  id: number;
  name: string;
  price: number;
}
export interface UserGame {
  game_id: number;
  attempted: number;
  payment: string;
}

export interface AvailableGames {
  balance: number;
  games: Game[];
  usergames: UserGame[];
}

