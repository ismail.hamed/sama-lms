export interface Gradeitem {
  id: number;
  itemname: string;
  itemtype: string;
  itemmodule: string;
  iteminstance: number;
  itemnumber?: number;
  categoryid?: number;
  outcomeid?: any;
  scaleid?: any;
  locked?: any;
  weightraw: number;
  weightformatted: string;
  status: string;
  graderaw?: number;
  gradedatesubmitted?: any;
  gradedategraded?: number;
  gradehiddenbydate: boolean;
  gradeneedsupdate: boolean;
  gradeishidden: boolean;
  gradeislocked?: any;
  gradeisoverridden?: any;
  gradeformatted: string;
  grademin: number;
  grademax: number;
  rangeformatted: string;
  percentageformatted: string;
  feedback: string;
  feedbackformat: number;
  cmid?: number;
}

export interface Usergrade {
  courseid: number;
  userid: number;
  userfullname: string;
  maxdepth: number;
  gradeitems: Gradeitem[];
}

export interface GradeItems {
  usergrades: Usergrade[];
  warnings: any[];
}


