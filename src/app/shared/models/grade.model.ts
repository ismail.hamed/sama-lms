export interface Grade {
  courseid: number;
  courseshortname: string;
  coursefullname: string;
  grade: number;
  rawgrade: string;
}

export interface Grades {
  grades: Grade[];
  warnings: any[];
}


