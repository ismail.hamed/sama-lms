export interface Completiondata {
  state: number;
  timecompleted: number;
  overrideby?: any;
  valueused: boolean;
}

export class Acitvity {
  id: number;
  url: string;
  name: string;
  instance: number;
  visible: number;
  uservisible: boolean;
  allowClick: boolean = true;
  visibleoncoursepage: number;
  modicon: string;
  modname: string;
  activityicon: number;
  supportactivity: boolean;
  modplural: string;
  indent: number;
  onclick: string;
  afterlink?: any;
  customdata: string;
  noviewlink: boolean;
  completion: number;
  completiondata: Completiondata;
  availabilityinfo: string;
  typesubmission: number;
}

export interface Lesson {
  id: number;
  name: string;
  visible: number;
  summary: string;
  summaryformat: number;
  section: number;
  hiddenbynumsections: number;
  totalgrade: number;
  usergrade: number;
  uservisible: boolean;
  availabilityinfo: string;
  modules: Acitvity[];
}

export interface Course {
  id: number;
  shortname: string;
  fullname: string;
  displayname: string;
  enrolledusercount: number;
  idnumber: string;
  visible: number;
  summary: string;
  summaryformat: number;
  format: string;
  showgrades: boolean;
  lang: string;
  enablecompletion: boolean;
  completionhascriteria: boolean;
  completionusertracked: boolean;
  category: number;
  progress: number;
  completed: boolean;
  startdate: number;
  enddate: number;
  marker: number;
  lastaccess?: any;
  isfavourite: boolean;
  hidden: boolean;
  overviewfiles: any[];
  lessons: Lesson[];
}

export interface H5PActivity {
  url: string;
  maxgrade: number;
  section: string;
  finalgrade: number;
  prevactivity: SetpActivity;
  nextactivity: SetpActivity;
}

export interface SetpActivity {
  id: number;
  name: string;
  modname: string;
  section: string;
  uservisible: boolean;
  activityicon: number;
  availabilityinfo: string;
  typesubmission: number
}
