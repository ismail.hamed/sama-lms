export interface MyFile {
    contextid: number;
    component: string;
    filearea: string;
    itemid: number;
    filepath: string;
    filename: string;
    isdir: boolean;
    url: string;
    timemodified: number;
    timecreated: number;
    filesize: number;
    author: string;
    license: string;
  }


