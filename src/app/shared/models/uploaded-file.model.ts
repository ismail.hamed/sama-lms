export interface UploadedFile {
  component: string;
  contextid: number;
  userid: string;
  filearea: string;
  filename: string;
  filepath: string;
  itemid: number;
  license: string;
  author: string;
  source: string;
}
