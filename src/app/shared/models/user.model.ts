export interface User {
  token: string;
  userid: number;
  username: string;
  firstname: string;
  lastname: string;
  email: string;
  schoolname: string;
  class: string;
  curriculum: string;
  privatetoken?: string;
}

