import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {GamesService} from '@shared/services/games.service';
import {AvailableGames} from '@shared/models/game.model';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class GamesResolverService implements Resolve<AvailableGames> {

  constructor(private gamesService: GamesService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<AvailableGames> | Promise<AvailableGames> | AvailableGames {
    return this.gamesService.getGames();
  }
}
