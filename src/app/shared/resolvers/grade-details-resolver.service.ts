import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {GamesService} from '@shared/services/games.service';
import {AvailableGames} from '@shared/models/game.model';
import {GradesService} from '@shared/services/grades.service';
import {Grades} from '@shared/models/grade.model';
import {GradeItems} from '@shared/models/grade-items.model';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class GradeDetailsResolverService implements Resolve<GradeItems> {

  constructor(private gradesService: GradesService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<GradeItems> | Promise<GradeItems> | GradeItems {
    const courseid = route.params.courseid;
    return this.gradesService.getCourseGradeDetails(courseid);
  }
}
