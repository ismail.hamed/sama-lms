import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {GamesService} from '@shared/services/games.service';
import {AvailableGames} from '@shared/models/game.model';
import {GradesService} from '@shared/services/grades.service';
import {Grades} from '@shared/models/grade.model';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class GradesResolverService implements Resolve<Grades> {

  constructor(private gradesService: GradesService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<Grades> | Promise<Grades> | Grades {
    return this.gradesService.getCourseGrades();
  }
}
