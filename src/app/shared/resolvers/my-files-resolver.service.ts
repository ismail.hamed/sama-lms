import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '@shared/services/auth.service';
import {MyFile} from '@shared/models/my-file.model';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class MyFilesResolverService implements Resolve<MyFile[]> {

  constructor(private authService: AuthService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<MyFile[]> | Promise<MyFile[]> |  MyFile[] {
    return this.authService.getPrivateFiles();
  }
}
