import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AvailableGames, UserGame} from '@shared/models/game.model';


@Injectable({
  providedIn: 'root'
})
export class GamesService {
  constructor(private http: HttpClient) {
  }

  getGames(): Observable<AvailableGames> {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('wsfunction', 'local_user_get_games');
    const opts = {params: httpParams};
    return this.http.get<AvailableGames>('', opts);
  }

  getGameById(id): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('wsfunction', 'local_user_get_game_by_id');
    httpParams = httpParams.set('id', id);
    const opts = {params: httpParams};
    return this.http.get<any>('', opts);
  }

  buyGame(id): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('wsfunction', 'local_user_bu_game');
    httpParams = httpParams.set('id', id);
    const opts = {params: httpParams};
    return this.http.get<any>('', opts);
  }

  updateAttemptedGame(id): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('wsfunction', 'local_user_update_attempted_game');
    httpParams = httpParams.set('id', id);
    const opts = {params: httpParams};
    return this.http.get<any>('', opts);
  }

}
