import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AvailableGames, UserGame} from '@shared/models/game.model';
import {Grades} from '@shared/models/grade.model';
import {AuthService} from '@shared/services/auth.service';
import {GradeItems} from '@shared/models/grade-items.model';


@Injectable({
  providedIn: 'root'
})
export class GradesService {
  constructor(private auth: AuthService,
              private http: HttpClient) {
  }

  getCourseGrades(): Observable<Grades> {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('wsfunction', 'gradereport_overview_get_course_grades');
    const opts = {params: httpParams};
    return this.http.get<Grades>('', opts).pipe(map(response => {
      response.grades.reverse();
      return response;
    }));
  }

  getCourseGradeDetails(courseid: string): Observable<GradeItems> {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('wsfunction', 'gradereport_user_get_grade_items');
    httpParams = httpParams.set('courseid', courseid);
    const userid = this.auth.currentUser().userid;
    httpParams = httpParams.set('userid', userid.toString());
    const opts = {params: httpParams};
    return this.http.get<GradeItems>('', opts);
  }

}
