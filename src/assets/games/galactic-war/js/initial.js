var oMain
function initial(){
  $(document).ready(function () {
    oMain = new CMain({
      hero_accelleration: 1.8, //HERO ACCELLERATION WHEN MOVING RIGHT/LEFT
      hero_friction: 0.9,      //HERO FRICTION WHEN CHANGE DIRECTION
      max_hero_speed: 20,     //MAX HERO SPEED
      time_occurence_falling_obj: 3000, //STARTING OCCURENCE OF ENEMIES IN MILLISECONDS
      speed_enemy: 5,          //STARTING ENEMY SPEED
      max_speed_enemy: 10,    //MAX ENEMY SPEED
      num_lives: 3,            //PLAYER LIVES
      speed_player_bullet: 30,//PLAYER BULLET SPEED
      score_hit: 10,          //POINTS GAINED WHEN ENEMY IS DESTROYED
      min_shot_occurence: 1000,//MIN OCCURENCE FOR ENEMY SHOT
      max_shot_occurence: 2000, //MIN OCCURENCE FOR ENEMY SHOT
      fullscreen: true,        //SET THIS TO FALSE IF YOU DON'T WANT TO SHOW FULLSCREEN BUTTON
      check_orientation: true //SET TO FALSE IF YOU DON'T WANT TO SHOW ORIENTATION ALERT ON MOBILE DEVICES
    });

    $(oMain).on("start_session", function (evt) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeStartSession();
      }
    });

    $(oMain).on("end_session", function (evt) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeEndSession();
      }
    });

    $(oMain).on("start_level", function (evt, iLevel) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeStartLevel({level: iLevel});
      }
    });

    $(oMain).on("end_level", function (evt, iLevel) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeEndLevel({level: iLevel});
      }
    });

    $(oMain).on("save_score", function (evt, iScore, szMode) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeSaveScore({score: iScore, mode: szMode});
      }
    });

    $(oMain).on("show_interlevel_ad", function (evt) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeShowInterlevelAD();
      }
    });

    $(oMain).on("share_event", function (evt, iScore) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeShareEvent({
          img: TEXT_SHARE_IMAGE,
          title: TEXT_SHARE_TITLE,
          msg: TEXT_SHARE_MSG1 + iScore + TEXT_SHARE_MSG2,
          msg_share: TEXT_SHARE_SHARE1 + iScore + TEXT_SHARE_SHARE1
        });
      }
    });


    if (isIOS()) {
      setTimeout(function () {
        sizeHandler();
      }, 200);
    } else {
      sizeHandler();
    }
  });
}
function stopGalcticWatAudio(){
  try{
    oMain.stopUpdate()
  }catch (e) {
  }

}
