var oMain;

function initial() {
  $(document).ready(function () {
    oMain = new CMain({
      occurence_fruit: 3000,   //FRUIT SHOWING OCCURENCE IN MILLISECONDS
      max_fruit_rot_speed: 2,  //EDIT THIS TO CHANGE FRUIT SPEED
      num_lives: 3,            //NUM OF LIVES
      start_sim_fruit: 1,      //NUM OF SIMULTANEOUS FRUITS WHEN GAME STARTS
      max_sim_fruits: 10,       //MAXIMUM NUMBER OF SIMULTANEOUS FRUITS
      fruits_for_level_up: 6, //FRUITS TO CUT FOR LEVEL UP (NEXT LEVEL INCREASE THE NUMBER OF SIMULTANEOUS FRUITS TO LAUNCH)
      time_for_combo: 500,     //INCREASE THIS VALUE TO GET COMBO MORE EASILY
      combo_points: 10,        //POINTS FOR A COMBO
      fullscreen: true, //SET THIS TO FALSE IF YOU DON'T WANT TO SHOW FULLSCREEN BUTTON
      check_orientation: true,     //SET TO FALSE IF YOU DON'T WANT TO SHOW ORIENTATION ALERT ON MOBILE DEVICES
      audio_enable_on_startup: false,  //ENABLE/DISABLE AUDIO WHEN GAME STARTS
    });


    $(oMain).on("start_session", function (evt) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeStartSession();
      }
      //...ADD YOUR CODE HERE EVENTUALLY
    });

    $(oMain).on("end_session", function (evt) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeEndSession();
      }
      //...ADD YOUR CODE HERE EVENTUALLY
    });

    $(oMain).on("start_level", function (evt, iLevel) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeStartLevel({level: iLevel});
      }
    });

    $(oMain).on("end_level", function (evt, iLevel) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeEndLevel({level: iLevel});
      }
    });

    $(oMain).on("restart_level", function (evt, iLevel) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeRestartLevel({level: iLevel});
      }
    });

    $(oMain).on("save_score", function (evt, iScore) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeSaveScore({score: iScore});
      }
      //...ADD YOUR CODE HERE EVENTUALLY
    });

    $(oMain).on("show_interlevel_ad", function (evt) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeShowInterlevelAD();
      }
      //...ADD YOUR CODE HERE EVENTUALLY
    });

    $(oMain).on("share_event", function (evt, iScore) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeShareEvent({
          img: TEXT_SHARE_IMAGE,
          title: TEXT_SHARE_TITLE,
          msg: TEXT_SHARE_MSG1 + iScore + TEXT_SHARE_MSG2,
          msg_share: TEXT_SHARE_SHARE1 + iScore + TEXT_SHARE_SHARE1
        });
      }
    });

    if (isIOS()) {
      setTimeout(function () {
        sizeHandler();
      }, 200);
    } else {
      sizeHandler();
    }
  });

}

function stopKatanaFruitsAudio() {
  try{
    oMain.stopUpdate();
  }catch (e) {
  }

}
