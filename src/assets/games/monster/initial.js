function initial() {
// Size the canvas to fill the browser viewport.
  jQuery(window).resize(function () {
    cr_sizeCanvas(jQuery(window).width(), jQuery(window).height());
  });

// Start the Construct 2 project running on window load.
  jQuery(document).ready(function () {
    // Create new runtime using the c2canvas
    cr_createRuntime("c2canvas");
  });



  document.addEventListener("visibilitychange", onVisibilityChanged, false);
  document.addEventListener("mozvisibilitychange", onVisibilityChanged, false);
  document.addEventListener("webkitvisibilitychange", onVisibilityChanged, false);
  document.addEventListener("msvisibilitychange", onVisibilityChanged, false);


  // if (navigator.serviceWorker && navigator.serviceWorker.register) {
  //   // Register an empty service worker to trigger web app install banners.
  //   navigator.serviceWorker.register("assets/games/monster/sw.js", {scope: "./"});
  // }
}
// Pause and resume on page becoming visible/invisible
function onVisibilityChanged() {
  if (document.hidden || document.mozHidden || document.webkitHidden || document.msHidden)
    cr_setSuspended(true);
  else
    cr_setSuspended(false);
};
function  stopMonsterAudio(){
  try{
    cr.plugins_.Audio.prototype.acts.StopAll();
    document.removeEventListener("visibilitychange", onVisibilityChanged, false);
    document.removeEventListener("mozvisibilitychange", onVisibilityChanged, false);
    document.removeEventListener("webkitvisibilitychange", onVisibilityChanged, false);
    document.removeEventListener("msvisibilitychange", onVisibilityChanged, false);
  }catch (e) {
  }

}
