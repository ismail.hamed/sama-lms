////////////////////////////////////////////////////////////
// CANVAS LOADER
////////////////////////////////////////////////////////////

 /*!
 *
 * START CANVAS PRELOADER - This is the function that runs to preload canvas asserts
 *
 */
function initPreload(){
	toggleLoader(true);

	checkMobileEvent();

	$(window).resize(function(){
		resizeGameFunc();
	});
	resizeGameFunc();

	loader = new createjs.LoadQueue(false);
	manifest=[
			{src:'assets/games/soccer/assets/background.png', id:'background'},
			{src:'assets/games/soccer/assets/logo.png', id:'logo'},
			{src:'assets/games/soccer/assets/button_start.png', id:'buttonStart'},

			{src:'assets/games/soccer/assets/item_gameplay.png', id:'itemGameplay'},
			{src:'assets/games/soccer/assets/button_quickmatch.png', id:'buttonQuickMatch'},
			{src:'assets/games/soccer/assets/button_championship.png', id:'buttonChampionship'},
			{src:'assets/games/soccer/assets/button_penaltykick.png', id:'buttonPenaltykick'},
			{src:'assets/games/soccer/assets/button_instructions.png', id:'buttonInstructions'},

			{src:'assets/games/soccer/assets/button_side_home.png', id:'buttonSideHome'},
			{src:'assets/games/soccer/assets/button_side_away.png', id:'buttonSideAway'},

			{src:'assets/games/soccer/assets/button_ok.png', id:'buttonOk'},
			{src:'assets/games/soccer/assets/item_instruction.png', id:'itemInstruction'},
			{src:'assets/games/soccer/assets/item_instruction_mobile.png', id:'itemInstructionMobile'},
			{src:'assets/games/soccer/assets/item_instruction_desktop.png', id:'itemInstructionDesktop'},

			{src:'assets/games/soccer/assets/item_ball.png', id:'itemBall'},
			{src:'assets/games/soccer/assets/item_shadow_ball.png', id:'itemBallShadow'},
			{src:'assets/games/soccer/assets/item_field.png', id:'itemField'},
			{src:'assets/games/soccer/assets/item_goalpost.png', id:'itemGoalpost'},
			{src:'assets/games/soccer/assets/item_ball_texture.png', id:'itemBallTexture'},
			{src:'assets/games/soccer/assets/goalkeeperA_Spritesheet6x5.png', id:'itemPlayerAGoalkeeper'},
			{src:'assets/games/soccer/assets/goalkeeperB_Spritesheet6x5.png', id:'itemPlayerBGoalkeeper'},
			{src:'assets/games/soccer/assets/item_guide_direction.png', id:'itemGuideDirection'},
			{src:'assets/games/soccer/assets/item_scoreboard.png', id:'itemScoreboard'},
			{src:'assets/games/soccer/assets/item_scoreboard_penalty.png', id:'itemScoreboardPenalty'},
			{src:'assets/games/soccer/assets/item_statistic.png', id:'itemStatistic'},
			{src:'assets/games/soccer/assets/button_next.png', id:'buttonNext'},
			{src:'assets/games/soccer/assets/button_statistic.png', id:'buttonStatistic'},
			{src:'assets/games/soccer/assets/button_penalty.png', id:'buttonPenalty'},

			{src:'assets/games/soccer/assets/item_status_bar1.png', id:'itemStatusBar1'},
			{src:'assets/games/soccer/assets/item_status_bar2.png', id:'itemStatusBar2'},
			{src:'assets/games/soccer/assets/item_status_kickoff.png', id:'itemStatusKickoff'},
			{src:'assets/games/soccer/assets/item_status_goal.png', id:'itemStatusGoal'},
			{src:'assets/games/soccer/assets/item_status_fulltime.png', id:'itemStatusFulltime'},
			{src:'assets/games/soccer/assets/item_status_misses.png', id:'itemStatusMisses'},
			{src:'assets/games/soccer/assets/item_status_penalty.png', id:'itemStatusPenalty'},
			{src:'assets/games/soccer/assets/item_icon_tackle.png', id:'itemIconTackle'},
			{src:'assets/games/soccer/assets/item_icon_safe.png', id:'itemIconSafe'},

			{src:'assets/games/soccer/assets/item_select.png', id:'itemSelect'},
			{src:'assets/games/soccer/assets/item_team_select.png', id:'itemTeamSelect'},

			{src:'assets/games/soccer/assets/item_championship.png', id:'itemChampionship'},
			{src:'assets/games/soccer/assets/item_bracket.png', id:'itemBracket'},
			{src:'assets/games/soccer/assets/item_bracket_high.png', id:'itemBracketH'},
			{src:'assets/games/soccer/assets/item_bracket_penalty.png', id:'itemBracketP'},
			{src:'assets/games/soccer/assets/button_restart.png', id:'buttonChamRestart'},
			{src:'assets/games/soccer/assets/button_simulate.png', id:'buttonChamSimulate'},
			{src:'assets/games/soccer/assets/button_play.png', id:'buttonChamPlay'},
			{src:'assets/games/soccer/assets/item_championship_player.png', id:'itemChamPlayer'},

			{src:'assets/games/soccer/assets/button_confirm.png', id:'buttonConfirm'},
			{src:'assets/games/soccer/assets/button_cancel.png', id:'buttonCancel'},
			{src:'assets/games/soccer/assets/item_exit.png', id:'itemExit'},

			{src:'assets/games/soccer/assets/item_result.png', id:'itemResult'},
			{src:'assets/games/soccer/assets/item_result_title_game.png', id:'itemResultTitleGame'},
			{src:'assets/games/soccer/assets/item_result_title_penalty.png', id:'itemResultTitlePenalty'},

			{src:'assets/games/soccer/assets/button_continue.png', id:'buttonContinue'},
			{src:'assets/games/soccer/assets/button_facebook.png', id:'buttonFacebook'},
			{src:'assets/games/soccer/assets/button_twitter.png', id:'buttonTwitter'},
			{src:'assets/games/soccer/assets/button_whatsapp.png', id:'buttonWhatsapp'},
			{src:'assets/games/soccer/assets/button_fullscreen.png', id:'buttonFullscreen'},
			{src:'assets/games/soccer/assets/button_sound_on.png', id:'buttonSoundOn'},
			{src:'assets/games/soccer/assets/button_sound_off.png', id:'buttonSoundOff'},
			{src:'assets/games/soccer/assets/button_exit.png', id:'buttonExit'},
			{src:'assets/games/soccer/assets/button_option.png', id:'buttonOption'}];

	for(var n=0; n<team_arr.length; n++){
		manifest.push({src:team_arr[n].icon, id:'team'+n});
		manifest.push({src:team_arr[n].player, id:'player'+n});
	}

	soundOn = true;
	if($.browser.mobile || isTablet){
		if(!enableMobileSound){
			soundOn=false;
		}
	}

	if(soundOn){
		manifest.push({src:'assets/games/soccer/assets/sounds/music1.ogg', id:'musicGame'});
		manifest.push({src:'assets/games/soccer/assets/sounds/music2.ogg', id:'musicGameEnd'});
		manifest.push({src:'assets/games/soccer/assets/sounds/ambience.ogg', id:'soundAmbience'});
		manifest.push({src:'assets/games/soccer/assets/sounds/goal1.ogg', id:'soundGoal1'});
		manifest.push({src:'assets/games/soccer/assets/sounds/goal2.ogg', id:'soundGoal2'});
		manifest.push({src:'assets/games/soccer/assets/sounds/whistle_start.ogg', id:'soundWhistleStart'});
		manifest.push({src:'assets/games/soccer/assets/sounds/whistle_end.ogg', id:'soundWhistleEnd'});
		manifest.push({src:'assets/games/soccer/assets/sounds/tackle.ogg', id:'soundTackle'});
		manifest.push({src:'assets/games/soccer/assets/sounds/kick.ogg', id:'soundKick'});
		manifest.push({src:'assets/games/soccer/assets/sounds/miss.ogg', id:'soundMiss'});
		manifest.push({src:'assets/games/soccer/assets/sounds/hitpost.ogg', id:'soundHitpost'});
		manifest.push({src:'assets/games/soccer/assets/sounds/hitwall.ogg', id:'soundHitwall'});
		manifest.push({src:'assets/games/soccer/assets/sounds/click.ogg', id:'soundClick'});
		manifest.push({src:'assets/games/soccer/assets/sounds/complete.ogg', id:'soundComplete'});

		createjs.Sound.alternateExtensions = ["mp3"];
		loader.installPlugin(createjs.Sound);
	}

	loader.addEventListener("complete", handleComplete);
	loader.addEventListener("fileload", fileComplete);
	loader.addEventListener("error",handleFileError);
	loader.on("progress", handleProgress, this);
	loader.loadManifest(manifest);
}

/*!
 *
 * CANVAS FILE COMPLETE EVENT - This is the function that runs to update when file loaded complete
 *
 */
function fileComplete(evt) {
	var item = evt.item;
	//console.log("Event Callback file loaded ", evt.item.id);
}

/*!
 *
 * CANVAS FILE HANDLE EVENT - This is the function that runs to handle file error
 *
 */
function handleFileError(evt) {
	console.log("error ", evt);
}

/*!
 *
 * CANVAS PRELOADER UPDATE - This is the function that runs to update preloder progress
 *
 */
function handleProgress() {
	$('#mainLoader span').html(Math.round(loader.progress/1*100)+' %');
}

/*!
 *
 * CANVAS PRELOADER COMPLETE - This is the function that runs when preloader is complete
 *
 */
function handleComplete() {
	toggleLoader(false);
	initMain();
};

/*!
 *
 * TOGGLE LOADER - This is the function that runs to display/hide loader
 *
 */
function toggleLoader(con){
	if(con){
		$('#mainLoader').show();
	}else{
		$('#mainLoader').hide();
	}
}
