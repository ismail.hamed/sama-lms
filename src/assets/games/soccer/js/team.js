////////////////////////////////////////////////////////////
// TEAM
////////////////////////////////////////////////////////////
var team_arr = [

				{
					name:'GERMANY',
					icon:'assets/games/soccer/assets/team/icon_germany.png',
					player:'assets/games/soccer/assets/team/players_germany.png'
				},

				{
					name:'BRAZIL',
					icon:'assets/games/soccer/assets/team/icon_brazil.png',
					player:'assets/games/soccer/assets/team/players_brazil.png'
				},

				{
					name:'PORTUGAL',
					icon:'assets/games/soccer/assets/team/icon_portugal.png',
					player:'assets/games/soccer/assets/team/players_portugal.png'
				},

				{
					name:'ARGENTINA',
					icon:'assets/games/soccer/assets/team/icon_argentina.png',
					player:'assets/games/soccer/assets/team/players_argentina.png'
				},

				{
					name:'BELGIUM',
					icon:'assets/games/soccer/assets/team/icon_belgium.png',
					player:'assets/games/soccer/assets/team/players_belgium.png'
				},

				{
					name:'SPAIN',
					icon:'assets/games/soccer/assets/team/icon_spain.png',
					player:'assets/games/soccer/assets/team/players_spain.png'
				},

				{
					name:'POLAND',
					icon:'assets/games/soccer/assets/team/icon_poland.png',
					player:'assets/games/soccer/assets/team/players_poland.png'
				},

				{
					name:'SWITZERLAND',
					icon:'assets/games/soccer/assets/team/icon_switzerland.png',
					player:'assets/games/soccer/assets/team/players_switzerland.png'
				},

				{
					name:'FRANCE',
					icon:'assets/games/soccer/assets/team/icon_france.png',
					player:'assets/games/soccer/assets/team/players_france.png'
				},

				{
					name:'CHILE',
					icon:'assets/games/soccer/assets/team/icon_chile.png',
					player:'assets/games/soccer/assets/team/players_chile.png'
				},

				{
					name:'PERU',
					icon:'assets/games/soccer/assets/team/icon_peru.png',
					player:'assets/games/soccer/assets/team/players_peru.png'
				},

				{
					name:'DENMARK',
					icon:'assets/games/soccer/assets/team/icon_denmark.png',
					player:'assets/games/soccer/assets/team/players_denmark.png'
				},

				{
					name:'COLOMBIA',
					icon:'assets/games/soccer/assets/team/icon_colombia.png',
					player:'assets/games/soccer/assets/team/players_colombia.png'
				},

				{
					name:'ITALY',
					icon:'assets/games/soccer/assets/team/icon_italy.png',
					player:'assets/games/soccer/assets/team/players_italy.png'
				},

				{
					name:'CROATIA',
					icon:'assets/games/soccer/assets/team/icon_croatia.png',
					player:'assets/games/soccer/assets/team/players_croatia.png'
				},

				{
					name:'ENGLAND',
					icon:'assets/games/soccer/assets/team/icon_england.png',
					player:'assets/games/soccer/assets/team/players_england.png'
				},

				];
