var oMain;
function initial() {
  $(document).ready(function () {
    oMain = new CMain({
      fullscreen: true, //SET THIS TO FALSE IF YOU DON'T WANT TO SHOW FULLSCREEN BUTTON
      check_orientation: true,     //SET TO FALSE IF YOU DON'T WANT TO SHOW ORIENTATION ALERT ON MOBILE DEVICES
      audio_enable_on_startup: false,  //ENABLE/DISABLE AUDIO WHEN GAME STARTS
      combo_value: 50,  //amount added to the score for each ball exploded in a combo
      extra_score: 100,  //amount added to the score when level is completely cleared
    });


    $(oMain).on("start_session", function (evt) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeStartSession();
        console.log("start_session");
      }
      //...ADD YOUR CODE HERE EVENTUALLY
    });

    $(oMain).on("end_session", function (evt) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeEndSession();
        console.log("end_session");
      }
      //...ADD YOUR CODE HERE EVENTUALLY
    });

    $(oMain).on("save_score", function (evt, iScore) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeSaveScore({score: iScore});
        console.log("save_score: " + iScore);
      }
      //...ADD YOUR CODE HERE EVENTUALLY
    });

    $(oMain).on("start_level", function (evt, iLevel) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeStartLevel({level: iLevel});
        console.log("start_level " + iLevel);
      }
      //...ADD YOUR CODE HERE EVENTUALLY
    });

    $(oMain).on("end_level", function (evt, iLevel) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeEndLevel({level: iLevel});
        console.log("end_level " + iLevel);
      }
      //...ADD YOUR CODE HERE EVENTUALLY
    });

    $(oMain).on("show_interlevel_ad", function (evt) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeShowInterlevelAD();
        console.log("show_interlevel_ad ");
      }
      //...ADD YOUR CODE HERE EVENTUALLY
    });

    $(oMain).on("share_event", function (evt, iScore) {
      if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeShareEvent({
          img: TEXT_SHARE_IMAGE,
          title: TEXT_SHARE_TITLE,
          msg: TEXT_SHARE_MSG1 + iScore + TEXT_SHARE_MSG2,
          msg_share: TEXT_SHARE_SHARE1 + iScore + TEXT_SHARE_SHARE1
        });
        console.log("share_event " + iScore);
      }
      //...ADD YOUR CODE HERE EVENTUALLY
    });

    if (isIOS()) {
      setTimeout(function () {
        sizeHandler();
      }, 200);
    } else {
      sizeHandler();
    }
  });
}
function stopSorcererAudio(){
  try{
    oMain.stopUpdate();
  }catch (e) {

  }
}
