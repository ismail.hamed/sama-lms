(function (cjs, an) {

  var p; // shortcut to reference prototypes
  var lib = {};
  var ss = {};
  var img = {};
  lib.ssMetadata = [
    {name: "Desert _Stage_atlas_1", frames: [[0, 0, 1930, 1431]]},
    {name: "Desert _Stage_atlas_2", frames: [[0, 0, 967, 1550], [969, 0, 967, 1550]]},
    {name: "Desert _Stage_atlas_3", frames: [[0, 0, 967, 1550], [969, 0, 967, 1550]]},
    {name: "Desert _Stage_atlas_4", frames: [[0, 0, 967, 1550], [969, 0, 967, 1550]]},
    {name: "Desert _Stage_atlas_5", frames: [[0, 0, 1459, 908], [0, 910, 1459, 908]]},
    {name: "Desert _Stage_atlas_6", frames: [[0, 0, 1459, 908], [0, 910, 1459, 908]]},
    {name: "Desert _Stage_atlas_7", frames: [[0, 0, 1459, 908], [0, 910, 1459, 908]]},
    {name: "Desert _Stage_atlas_8", frames: [[0, 0, 1362, 846], [0, 848, 1362, 846]]},
    {name: "Desert _Stage_atlas_9", frames: [[0, 0, 1362, 846], [0, 848, 1362, 846]]},
    {name: "Desert _Stage_atlas_10", frames: [[0, 0, 1362, 846], [0, 848, 1362, 846]]},
    {
      name: "Desert _Stage_atlas_11",
      frames: [[0, 0, 891, 933], [0, 935, 891, 933], [893, 0, 891, 933], [893, 935, 891, 933]]
    },
    {
      name: "Desert _Stage_atlas_12",
      frames: [[893, 0, 859, 899], [0, 0, 891, 933], [0, 935, 891, 933], [893, 901, 859, 899]]
    },
    {
      name: "Desert _Stage_atlas_13",
      frames: [[0, 0, 859, 899], [0, 901, 859, 899], [861, 0, 859, 899], [861, 901, 859, 899]]
    },
    {
      name: "Desert _Stage_atlas_14",
      frames: [[1630, 642, 390, 448], [1330, 642, 298, 686], [0, 0, 663, 640], [1630, 1092, 394, 388], [665, 0, 663, 640], [1330, 0, 663, 640], [0, 642, 663, 640], [0, 1284, 663, 640], [665, 642, 663, 640], [1064, 1330, 394, 388], [1460, 1482, 394, 388], [665, 1284, 397, 454]]
    },
    {
      name: "Desert _Stage_atlas_15",
      frames: [[1169, 768, 153, 379], [790, 672, 377, 204], [1188, 0, 393, 382], [490, 774, 242, 243], [0, 774, 243, 243], [245, 774, 243, 243], [790, 390, 279, 280], [0, 0, 394, 388], [396, 0, 394, 388], [792, 0, 394, 388], [1583, 0, 393, 382], [1188, 384, 393, 382], [1583, 384, 393, 382], [0, 390, 393, 382], [395, 390, 393, 382], [1071, 469, 67, 50], [734, 878, 132, 233], [1071, 390, 83, 77]]
    }
  ];


  (lib.AnMovieClip = function () {
    this.actionFrames = [];
    this.gotoAndPlay = function (positionOrLabel) {
      cjs.MovieClip.prototype.gotoAndPlay.call(this, positionOrLabel);
    }
    this.play = function () {
      cjs.MovieClip.prototype.play.call(this);
    }
    this.gotoAndStop = function (positionOrLabel) {
      cjs.MovieClip.prototype.gotoAndStop.call(this, positionOrLabel);
    }
    this.stop = function () {
      cjs.MovieClip.prototype.stop.call(this);
    }
  }).prototype = p = new cjs.MovieClip();
// symbols:


  (lib.CachedBmp_135 = function () {
    this.initialize(ss["Desert _Stage_atlas_14"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_126 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_134 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_124 = function () {
    this.initialize(ss["Desert _Stage_atlas_14"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_123 = function () {
    this.initialize(img.CachedBmp_123);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 2844, 1667);


  (lib.CachedBmp_122 = function () {
    this.initialize(ss["Desert _Stage_atlas_14"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_121 = function () {
    this.initialize(ss["Desert _Stage_atlas_14"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_120 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_119 = function () {
    this.initialize(ss["Desert _Stage_atlas_5"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_118 = function () {
    this.initialize(ss["Desert _Stage_atlas_2"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_117 = function () {
    this.initialize(ss["Desert _Stage_atlas_8"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_116 = function () {
    this.initialize(ss["Desert _Stage_atlas_12"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_115 = function () {
    this.initialize(img.CachedBmp_115);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 3670, 2839);


  (lib.CachedBmp_133 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_113 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(4);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_132 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(5);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_131 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(6);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_110 = function () {
    this.initialize(ss["Desert _Stage_atlas_5"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_109 = function () {
    this.initialize(ss["Desert _Stage_atlas_6"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_108 = function () {
    this.initialize(ss["Desert _Stage_atlas_6"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_107 = function () {
    this.initialize(ss["Desert _Stage_atlas_7"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_106 = function () {
    this.initialize(ss["Desert _Stage_atlas_7"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_105 = function () {
    this.initialize(ss["Desert _Stage_atlas_2"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_104 = function () {
    this.initialize(ss["Desert _Stage_atlas_3"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_103 = function () {
    this.initialize(ss["Desert _Stage_atlas_3"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_102 = function () {
    this.initialize(ss["Desert _Stage_atlas_4"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_101 = function () {
    this.initialize(ss["Desert _Stage_atlas_4"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_100 = function () {
    this.initialize(ss["Desert _Stage_atlas_8"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_99 = function () {
    this.initialize(ss["Desert _Stage_atlas_9"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_98 = function () {
    this.initialize(ss["Desert _Stage_atlas_9"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_97 = function () {
    this.initialize(ss["Desert _Stage_atlas_10"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_96 = function () {
    this.initialize(ss["Desert _Stage_atlas_10"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_95 = function () {
    this.initialize(ss["Desert _Stage_atlas_11"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_94 = function () {
    this.initialize(ss["Desert _Stage_atlas_11"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_93 = function () {
    this.initialize(ss["Desert _Stage_atlas_11"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_92 = function () {
    this.initialize(ss["Desert _Stage_atlas_11"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_91 = function () {
    this.initialize(ss["Desert _Stage_atlas_12"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_90 = function () {
    this.initialize(ss["Desert _Stage_atlas_12"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_89 = function () {
    this.initialize(ss["Desert _Stage_atlas_12"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_88 = function () {
    this.initialize(ss["Desert _Stage_atlas_13"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_87 = function () {
    this.initialize(ss["Desert _Stage_atlas_13"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_86 = function () {
    this.initialize(ss["Desert _Stage_atlas_13"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_85 = function () {
    this.initialize(ss["Desert _Stage_atlas_13"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_84 = function () {
    this.initialize(ss["Desert _Stage_atlas_14"]);
    this.gotoAndStop(4);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_83 = function () {
    this.initialize(ss["Desert _Stage_atlas_14"]);
    this.gotoAndStop(5);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_82 = function () {
    this.initialize(ss["Desert _Stage_atlas_14"]);
    this.gotoAndStop(6);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_81 = function () {
    this.initialize(ss["Desert _Stage_atlas_14"]);
    this.gotoAndStop(7);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_80 = function () {
    this.initialize(ss["Desert _Stage_atlas_14"]);
    this.gotoAndStop(8);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_79 = function () {
    this.initialize(ss["Desert _Stage_atlas_14"]);
    this.gotoAndStop(9);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_78 = function () {
    this.initialize(ss["Desert _Stage_atlas_14"]);
    this.gotoAndStop(10);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_77 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(7);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_76 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(8);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_75 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(9);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_74 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(10);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_73 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(11);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_72 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(12);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_71 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(13);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_70 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(14);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_69 = function () {
    this.initialize(ss["Desert _Stage_atlas_14"]);
    this.gotoAndStop(11);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_130 = function () {
    this.initialize(img.CachedBmp_130);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 2298, 1364);


  (lib.CachedBmp_129 = function () {
    this.initialize(img.CachedBmp_129);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 2154, 1477);


  (lib.CachedBmp_66 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(15);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_128 = function () {
    this.initialize(ss["Desert _Stage_atlas_1"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_64 = function () {
    this.initialize(img.CachedBmp_64);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 2882, 1770);


  (lib.CachedBmp_63 = function () {
    this.initialize(img.CachedBmp_63);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 2424, 1434);


  (lib.Bitmap1 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(16);
  }).prototype = p = new cjs.Sprite();


  (lib.Blend_0 = function () {
    this.initialize(ss["Desert _Stage_atlas_15"]);
    this.gotoAndStop(17);
  }).prototype = p = new cjs.Sprite();

// helper functions:

  function mc_symbol_clone() {
    var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop, this.reversed));
    clone.gotoAndStop(this.currentFrame);
    clone.paused = this.paused;
    clone.framerate = this.framerate;
    return clone;
  }

  function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
    var prototype = cjs.extend(symbol, cjs.MovieClip);
    prototype.clone = mc_symbol_clone;
    prototype.nominalBounds = nominalBounds;
    prototype.frameBounds = frameBounds;
    return prototype;
  }


  (lib.wood = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(0, 0, 0, 0);


  (lib.Tween12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_135();
    this.instance.setTransform(-97.4, -112.1, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-97.4, -112.1, 195, 224);


  (lib.Tween6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0004, -0.0235, 1.2751, 1.0272, 0, 0, 180);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-177.1, -59.6, 354.29999999999995, 119.2);


  (lib.Tween4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0128, -0.0036);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-138.9, -58, 277.9, 116.1);


  (lib.Tween3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0128, -0.0036);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-138.9, -58, 277.9, 116.1);


  (lib.Tween2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhhMbQg6gRgLgyIAAACQgWAPgYgOQgVgOgMgdIgCABQg3ARg1ggQg5giAVg7QgiAMgKgiQgJgcALgcQgeAdgfgUQgcgTgLgpQgMgqASgZQAWgeA4AOQADADAAACQACgMAIgJQgQgBgRgJQgSgKgHgOQACgEAAgIQAAgHABgEQgYAHgagKIAAADQAOAUgYAYQghAhg3gTQgygSgWgmQgZgqALg0QAMg4AvgPQgugBgcghIAAADQgGAhg3AJQg3AIALgwQgKADgPAAIAFAFQgLA/g3AOQgzANg9ggQg/ggggg5QgkhBAVhFIAAAAIgBABQgVgEgJgUQgJgVASgPQgcgJAAgYQABgVAUgUIgbgMQgFAFgVAQQAeASgQAbQgOAZgfAHIAAABQAMAjglASQggAPgggIQgkgJgbguQgbgvATgaIgDgDQgrAIgGgtQgHgsAjgVQAYgQAeAJQAeAJAFAbQACgVAUgNQASgMAZAAQAZAAARAMQATAOAAAXQARgRAZALIAAgCQgOgPAMgRQALgQAUgCQgqgJgEgfQgEgcAdgdQAxgvBFABQBLABAYA/QARgaAnAGQAmAHAQAbQAWAoggAYQgeAWgmgNQADATgPAQQgOAOgSADQAKAWgSAUIAAAQQATAEAMAKQAOALACARQAdgkBJABQBLABAWArQAQgSASgEIAAgDQhWgNAFhbQACgmATgkQAUglAdgVQAygiBKgEQBMgEA4AgIABgDQgBgRAOgLQANgKASAAQASABALAKQAMAMgEAUQAagEAQAMQgDgPAOgJQAMgIAVgBIABgBQgSgTANgSQANgSAXANQgQgXAbgbQAXgXAdgIQAegJAlAIQAjAHAaAUQAgAWANAtQANAsgMApIAEAAQAXACAQALQATAMABAWIAAgCQATgHAWAGQAVAGARAOQgpguA2g9QAyg5A5gCQg0gIgRguQgRgwAqglQAUgQAggFQgSgPAMgQQAMgQAXAIQgbgWAegJQAdgJAXAOQgggiApgjQAjgfAqgFQAzgEAoAjQArAngYAwIgCAAQAgAFAQAVQAOATgDAZQgEAYgTAQQgVASgfgCQAMAKAAANQAAAOgMAKQAWgFAPADQAVAEgGAVQAZgHAPAKQAOAJABATQADAvg0AOQAUAGAFAUQAFARgJAUQARgOAZgCQAYgCAUAKIACACQAJgRAHgGQgZgDgKgUQgJgRAGgVQAGgVASgLQAUgMAaAIIACAAQhWgdAhhWQANgjAdgeQAegeAfgMQBOgeBDAzQBEA0gWBRQAwgCABAnQABAlgsANQAqgFAnAfQASgoBHAGQgUgIAOgRQANgRATAAQgBgVANgKQALgIAUABQgmhFAvgbQApgXBKASQBKATAfAqQAkAxg2A0QAiAEAMAjQAMAigdAWQAdALAXAgQAaAjgHAdQgGAagnAXQgkAWgdAAQAdAfgbAwQgZAsgoAQIgVAPQAhAwgqAtQgqAtgzgaQAbAcgJAfQgHAagfARQgdAQgegFQgggGgMgfQgSAOgbAKIgEgDQAPAwgcAoQgeArg8gIIhPAaQgWAAgSgHQgXgJgFgSQgWA1hegMQhcgLgZgwIgDAAQgPAvguATQgdAMhAAGQAVBThNAjQggAPglgDQgngEgegWQghArgrgmQgrgmATgsIgCgBQgXALgWgJQAGAOgFAJQgGAIgQgCQAOAig1AWQgrATgcgGQAcAIANAgQAOAkgZAXQAcACgBAbQAAAcgeAGIgDAAQAAAKgHANQA/gFAZAwQAaAyhEAhQgaAOgeAAQgVAAgXgHgA3vk6QgEgOAIgJQAGgHAMgBQAZgDAGANQAFAMgLAPQgKAQgNAAIgCABQgOAAgIgXgAtKmMQgWgNADgZQADgZAdAGQAqAKgLAhQgFANgLAEQgFADgGAAQgIAAgJgGg");
    this.shape.setTransform(0.028, 0.0029);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-152.1, -80.2, 304.29999999999995, 160.4);


  (lib.Tween1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhhMbQg6gRgLgyIAAACQgWAPgYgOQgVgOgMgdIgCABQg3ARg1ggQg5giAVg7QgiAMgKgiQgJgcALgcQgeAdgfgUQgcgTgLgpQgMgqASgZQAWgeA4AOQADADAAACQACgMAIgJQgQgBgRgJQgSgKgHgOQACgEAAgIQAAgHABgEQgYAHgagKIAAADQAOAUgYAYQghAhg3gTQgygSgWgmQgZgqALg0QAMg4AvgPQgugBgcghIAAADQgGAhg3AJQg3AIALgwQgKADgPAAIAFAFQgLA/g3AOQgzANg9ggQg/ggggg5QgkhBAVhFIAAAAIgBABQgVgEgJgUQgJgVASgPQgcgJAAgYQABgVAUgUIgbgMQgFAFgVAQQAeASgQAbQgOAZgfAHIAAABQAMAjglASQggAPgggIQgkgJgbguQgbgvATgaIgDgDQgrAIgGgtQgHgsAjgVQAYgQAeAJQAeAJAFAbQACgVAUgNQASgMAZAAQAZAAARAMQATAOAAAXQARgRAZALIAAgCQgOgPAMgRQALgQAUgCQgqgJgEgfQgEgcAdgdQAxgvBFABQBLABAYA/QARgaAnAGQAmAHAQAbQAWAoggAYQgeAWgmgNQADATgPAQQgOAOgSADQAKAWgSAUIAAAQQATAEAMAKQAOALACARQAdgkBJABQBLABAWArQAQgSASgEIAAgDQhWgNAFhbQACgmATgkQAUglAdgVQAygiBKgEQBMgEA4AgIABgDQgBgRAOgLQANgKASAAQASABALAKQAMAMgEAUQAagEAQAMQgDgPAOgJQAMgIAVgBIABgBQgSgTANgSQANgSAXANQgQgXAbgbQAXgXAdgIQAegJAlAIQAjAHAaAUQAgAWANAtQANAsgMApIAEAAQAXACAQALQATAMABAWIAAgCQATgHAWAGQAVAGARAOQgpguA2g9QAyg5A5gCQg0gIgRguQgRgwAqglQAUgQAggFQgSgPAMgQQAMgQAXAIQgbgWAegJQAdgJAXAOQgggiApgjQAjgfAqgFQAzgEAoAjQArAngYAwIgCAAQAgAFAQAVQAOATgDAZQgEAYgTAQQgVASgfgCQAMAKAAANQAAAOgMAKQAWgFAPADQAVAEgGAVQAZgHAPAKQAOAJABATQADAvg0AOQAUAGAFAUQAFARgJAUQARgOAZgCQAYgCAUAKIACACQAJgRAHgGQgZgDgKgUQgJgRAGgVQAGgVASgLQAUgMAaAIIACAAQhWgdAhhWQANgjAdgeQAegeAfgMQBOgeBDAzQBEA0gWBRQAwgCABAnQABAlgsANQAqgFAnAfQASgoBHAGQgUgIAOgRQANgRATAAQgBgVANgKQALgIAUABQgmhFAvgbQApgXBKASQBKATAfAqQAkAxg2A0QAiAEAMAjQAMAigdAWQAdALAXAgQAaAjgHAdQgGAagnAXQgkAWgdAAQAdAfgbAwQgZAsgoAQIgVAPQAhAwgqAtQgqAtgzgaQAbAcgJAfQgHAagfARQgdAQgegFQgggGgMgfQgSAOgbAKIgEgDQAPAwgcAoQgeArg8gIIhPAaQgWAAgSgHQgXgJgFgSQgWA1hegMQhcgLgZgwIgDAAQgPAvguATQgdAMhAAGQAVBThNAjQggAPglgDQgngEgegWQghArgrgmQgrgmATgsIgCgBQgXALgWgJQAGAOgFAJQgGAIgQgCQAOAig1AWQgrATgcgGQAcAIANAgQAOAkgZAXQAcACgBAbQAAAcgeAGIgDAAQAAAKgHANQA/gFAZAwQAaAyhEAhQgaAOgeAAQgVAAgXgHgA3vk6QgEgOAIgJQAGgHAMgBQAZgDAGANQAFAMgLAPQgKAQgNAAIgCABQgOAAgIgXgAtKmMQgWgNADgZQADgZAdAGQAqAKgLAhQgFANgLAEQgFADgGAAQgIAAgJgGg");
    this.shape.setTransform(-0.022, 0.0029);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-152.1, -80.2, 304.2, 160.4);


  (lib.Symbol11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_126();
    this.instance.setTransform(-38.4, -94.65, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(105,112,89,0.659)").s().p("AhPCxQg6gCglgGQg0gJglgWQgrgZgYgrQgZguALgrQAThSB/gqQB0gnBwAGQB7AHBfA6QA+AmAYAxQAPAcAAAfQAAAhgRAZQgRAbgkAQQgaALgrAJQhzAViDAAIgrAAg");
    this.shape.setTransform(-8.9053, 85.3621);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(-43.7, -94.6, 81.80000000000001, 197.7), null);


  (lib.Symbol10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_134();
    this.instance.setTransform(-94, -51.45, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(105,112,89,0.659)").s().p("AjPFFQgmgHgiggIgVgTIgdgOQgWgLgOgTQgOgUgCgWIABgeQgBgJgHgeQgHgfAEgTQAIgsA9gfQA5geBBgGQBCgGA8AUQA0ASAYAhQAQAXACAcIADAdIAKAXQAIASgFAUQgEAUgOAOQgMALgCAFQgCAEgBAMQgEAegfASQgVAMglADQgNABgGADQgFADgNAMQgPANgVAGQgMADgLAAQgKAAgJgCgAGVDFQhdgEhggUQgpgJgSgMQgRgMgLgTQgKgTgBgVQgBgVAJgUQAIgUAQgNQAUgQAggHQAWgFAkgBQBcgCBZAPQAjAFAUAKQAWALAPAWQAPAWACAZIACAdQABASgBAGQgEAUgVAPQgYARgiAFQgQACgVAAIgbgBgAsPBaQgmgBgWgSQgOgNgGgTQgFgTAGgTQAFgRAQgMQAQgNATgBIAVgDQAFgBAMgKQAWgSAqgLQAtgLAqgBQAggBAUAFQAcAIAQATQANAPACAVQABAVgJARQgOAYgpAUQg3AbguAEQgQABggABIggADIgcACIgFAAgAlrhuQgggIgTggQgPgZgBgmQgBgdAHgXQAJgcAUgPQASgOAegDQASgCAjADQCFALBUALQCHATBEAIQB2APBWgBQBCgBATACIALACQAEgIAGgGQASgUAwgIQBygSBuAdQATAFAJABQAOAAAagFQATgBARAIQARAJAKAPQAKAPAAATQAAAUgLAPQgPAWghAIQgTAEgngBIilgEQhBgBgjgHIgTgFQgNAGgOAEQgYAIgnAEQiIARifgHQiBgGilgYQgTgDgLADQgJADgKAIIgPAQQgSASgWAKQgQAGgQAAQgHAAgHgBg");
    this.shape.setTransform(-1.8565, 18.8739);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(-94, -51.4, 188.5, 103), null);


  (lib.Symbol8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_124();
    this.instance.setTransform(-74.4, -171.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-74.4, -171.5, 149, 343), null);


  (lib.Symbol1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_123();
    this.instance.setTransform(-711, -416.6, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-711, -416.6, 1422, 833.5);


  (lib._3_Stars = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_122();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Star_R = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_121();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_stae_L = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_120();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib.Stageon = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_119();
    this.instance.setTransform(-364.6, -227, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-364.6, -227, 729.5, 454);


  (lib.Locked_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_118();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Less_1_off = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Steb_1
    this.instance = new lib.CachedBmp_117();
    this.instance.setTransform(-1676.9, -18.35, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1676.9, -18.3, 681.0000000000001, 423);


  (lib.C_1_ON = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("1", "bold 200px 'Calibri'", "#49386B");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_116();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.Blend = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Blend_0();

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Blend, new cjs.Rectangle(0, 0, 83, 77), null);


  (lib.Path_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(0, 0, 0, 0);


  (lib.Path = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.rf(["#2F0202", "#3B0101", "#3B0303", "#4A0101", "#600101", "#790306", "#A50003", "#820506", "#C00004"], [0.604, 0.635, 0.663, 0.698, 0.729, 0.776, 0.808, 0.816, 0.839], 2.7, -3.2, 0, 2.7, -3.2, 26.8).s().p("AiKCLQg6g5AAhSQAAhQA6g6QA6g6BQAAQBSAAA6A6QA5A5AABRQAABSg5A5Qg6A6hSAAQhQAAg6g6g");
    this.shape.setTransform(19.7, 19.725);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path, new cjs.Rectangle(0, 0, 39.4, 39.5), null);


  (lib.Tween5copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0004, -0.0235, 1.2751, 1.0272, 0, 0, 180);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-177.1, -59.6, 354.29999999999995, 119.2);


  (lib.Symbol12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_115();
    this.instance.setTransform(-696.7, -540.3, 0.3801, 0.3801);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true}, 1).wait(184));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-696.7, -540.3, 1394.8000000000002, 1079);


  (lib.Symbol7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Bitmap1();

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(0, 0, 132, 233);


  (lib.Stageoncopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_110();
    this.instance.setTransform(-364.6, -227, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-364.6, -227, 729.5, 454);


  (lib.Stageoncopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_109();
    this.instance.setTransform(-364.6, -227, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-364.6, -227, 729.5, 454);


  (lib.Stageoncopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_108();
    this.instance.setTransform(-364.6, -227, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-364.6, -227, 729.5, 454);


  (lib.Stageoncopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_107();
    this.instance.setTransform(-364.6, -227, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-364.6, -227, 729.5, 454);


  (lib.Stageon_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance_1 = new lib.CachedBmp_106();
    this.instance_1.setTransform(-364.6, -227, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-364.6, -227, 729.5, 454);


  (lib.Locked_1copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_105();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Locked_1copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_104();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Locked_1copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_103();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Locked_1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_102();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Locked_1_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance_1 = new lib.CachedBmp_101();
    this.instance_1.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Less_1_offcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Steb_1
    this.instance = new lib.CachedBmp_100();
    this.instance.setTransform(-1676.9, -18.4, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1676.9, -18.4, 681.0000000000001, 423);


  (lib.Less_1_offcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Steb_1
    this.instance = new lib.CachedBmp_99();
    this.instance.setTransform(-1676.9, -18.4, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1676.9, -18.4, 681.0000000000001, 423);


  (lib.Less_1_offcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Steb_1
    this.instance = new lib.CachedBmp_98();
    this.instance.setTransform(-1676.9, -18.4, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1676.9, -18.4, 681.0000000000001, 423);


  (lib.Less_1_offcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Steb_1
    this.instance = new lib.CachedBmp_97();
    this.instance.setTransform(-1676.9, -18.4, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1676.9, -18.4, 681.0000000000001, 423);


  (lib.Less_1_off_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Steb_1
    this.instance_1 = new lib.CachedBmp_96();
    this.instance_1.setTransform(-1676.9, -18.4, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1676.9, -18.4, 681.0000000000001, 423);


  (lib.C_ONcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, 2.275);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_95();
    this.instance.setTransform(-214.65, -220.3, 0.4819, 0.4819);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -224.5, 449.1, 453.9);


  (lib.C_ONcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, 2.275);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_94();
    this.instance.setTransform(-214.65, -220.3, 0.4819, 0.4819);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -224.5, 449.1, 453.9);


  (lib.C_ONcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, 2.275);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_93();
    this.instance.setTransform(-214.65, -220.3, 0.4819, 0.4819);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -224.5, 449.1, 453.9);


  (lib.C_ONcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, 2.275);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_92();
    this.instance.setTransform(-214.65, -220.3, 0.4819, 0.4819);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -224.5, 449.1, 453.9);


  (lib.C_ONcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, 2.275);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_91();
    this.instance.setTransform(-214.65, -220.3, 0.4819, 0.4819);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -224.5, 449.1, 453.9);


  (lib.C_ON = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, 2.275);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_90();
    this.instance.setTransform(-214.65, -220.3, 0.4819, 0.4819);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -224.5, 449.1, 453.9);


  (lib.C_1_ONcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("6", "bold 200px 'Calibri'", "#49386B");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_89();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("5", "bold 200px 'Calibri'", "#49386B");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_88();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("4", "bold 200px 'Calibri'", "#49386B");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_87();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("3", "bold 200px 'Calibri'", "#49386B");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_86();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ON_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text_1 = new cjs.Text("2", "bold 200px 'Calibri'", "#49386B");
    this.text_1.textAlign = "center";
    this.text_1.lineHeight = 225;
    this.text_1.lineWidth = 172;
    this.text_1.alpha = 0.98823529;
    this.text_1.parent = this;
    this.text_1.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text_1).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance_1 = new lib.CachedBmp_85();
    this.instance_1.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib._3_Starscopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_84();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_83();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_82();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_81();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Stars_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance_1 = new lib.CachedBmp_80();
    this.instance_1.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Star_Rcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_79();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_78();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_77();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_76();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_R_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance_1 = new lib.CachedBmp_75();
    this.instance_1.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_stae_Lcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_74();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_73();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_72();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_71();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_L_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance_1 = new lib.CachedBmp_70();
    this.instance_1.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib.Camel = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_69();
    this.instance.setTransform(-96.55, -110.45, 0.487, 0.487);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(109,126,109,0.329)").s().p("ALfHNQgYgEgpgKIiMghQg4gOgdgLQgtgRgcgcQiIAeiIgcQg2gLgegUQgQgLgZgaQgbgcgOgKQg6gthpgDQgYAAg+ACQg3ACgggCQhAgFg9gYQg9gXgzgoQgYgTgFgRQgEgTANgSQANgRAUgFQARgFAWACQANACAbAGQgSgZgZhGQgWhAgZgcQgZgcgtgVQgOgHhFgZQhugpgXhCQgLgjAOgaQAVglBNgCQBPgDAtATQA/AbBABnQBIB1AsAgQAbATAnAPQAWAIAxAOQAyANAaAEQArAHAhgHIAxgOQAdgIAUAFQAQADAZAQQAbARAMAFQAWAIAsgBIBQAAQAzAAAYAIQAUAIAbAWIAsAjQANAJAqAVQAiARARAPQAZAWAFAgQAGAjgYASQA/AgBFAPQBGAOBGgGIAFgkQAugEAqALQAtAMAfAcQAiAfAJAtQAJAvgWAlQgRAbggAQQgdAOgjADIgTABQgVAAgZgEg");
    this.shape.setTransform(-2.9883, 66.9941);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-96.5, -110.4, 193.3, 223.9);


  (lib.Symbol328 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_R("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol327 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_L("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol326 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy4("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol325 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy4("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol324 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy3("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol323 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy3("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol322 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy2("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol321 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy2("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol320 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol319 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol318 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_R_1("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol317 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_L_1("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol21 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Tween5copy6("synched", 0);

    this.instance_1 = new lib.Tween6("synched", 0);
    this.instance_1.setTransform(75.8, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance_1}]}, 779).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true, x: 75.8}, 779).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-177.1, -59.6, 430.1, 119.2);


  (lib.Symbol18 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Tween3("synched", 0);

    this.instance_1 = new lib.Tween4("synched", 0);
    this.instance_1.setTransform(43.6, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance_1}]}, 739).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true, x: 43.6}, 739).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-138.9, -58, 321.5, 116.1);


  (lib.Symbol17 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Tween1("synched", 0);

    this.instance_1 = new lib.Tween2("synched", 0);
    this.instance_1.setTransform(117.35, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance_1}]}, 699).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true, x: 117.35}, 699).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-152.1, -80.2, 421.6, 160.4);


  (lib.Symbol9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_43 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(43).call(this.frame_43).wait(1));

    // Layer_2
    this.instance = new lib.Symbol7("single", 0);
    this.instance.setTransform(65.85, 703.7, 1, 1, 14.9983, 0, 0, 65.9, 233.3);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      x: -161.3,
      y: 378.9
    }, 6).to({startPosition: 0}, 5).to({
      regX: 65.8,
      scaleX: 0.8992,
      scaleY: 0.8992,
      rotation: -15.0106,
      x: -161.4,
      y: 378.85
    }, 7).to({
      scaleX: 0.9999,
      scaleY: 0.9999,
      rotation: -0.0009,
      y: 378.9
    }, 5).to({rotation: -0.0009}, 4).to({startPosition: 0}, 5).to({alpha: 0}, 11).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-272.9, 136.5, 463, 584);


  (lib.Symbol7_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    this.isSingleFrame = false;
    // timeline functions:
    this.frame_0 = function () {
      if (this.isSingleFrame) {
        return;
      }
      if (this.totalFrames == 1) {
        this.isSingleFrame = true;
      }
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

    // Layer_1
    this.instance_1 = new lib.Tween12("synched", 0);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    // Layer_3
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(105,112,89,0.659)").s().p("AB1F1IjeAAQhvgBg4gDQhcgHhHgTIhBgSQgmgLgbgCQgSgChjAHQhGAFgpgTQgagMgZgaQgSgSgXghQgXgfgKgUQgQgdgEgbQgGgsAYgqQAXgoAqgQQATgIAlgFQAqgGAQgFQAhgLA4goQA5goAggKQAygQA2ASQA1ASAdAsQAeAsgCA3QgCA4giApQFZA5FXg3QBPgMA1gRQBGgXAwgnQgigdAKg0QAJgyAogbQAkgZAygCQArgCAxAOQBfAaAoA9QAaApADA6QACAqgLBAQgIAygKAeQgOAqgYAcQhBBIiUgIIhpgJQg/gGgqAEQgjADg8ANQhDAQgcADQgnAGhAAAIgWgBgAQhhJIgzAAQgeAAgUgEQgbgGgUgOQgXgQgJgXIgBgDQgTADgVgLQgbgOgCgbQhEAChUgHQgygEhlgMQjbgbhrgWIhVgTQgygKgkgFQhTgLiKAIQhhAFjGASIm8AoQgEA1gLAfQgQAtgiAQQgPAIgVACQgMACgZAAQhtgBgPg4QgGgXANgYQAMgWAXgNQATgKAbgGQARgDAggCQgVgOgCgcQgBgcAQgUQAdglA9gBQAUAAAfAEIAyAGQArAEBagHQBYgHAtAFQAUACAnAIQAmAIAUACQAfAEArgBIBKgDQBIgDCbADQCUAEBOgEQBhgFDEgWQCtgNB1AYIAyAJQAcADAVgFQAQgEAfgPQAdgLATAHQASAHASAcIAOAVQAwASAdAjIAOAQQAJAKAIAEQAMAFAfgDQBAgGAlARQAZALAOAWQAPAYgFAZQgFAWgTAQQgSAPgYAGQgUAFgbAAIgvgBg");
    this.shape.setTransform(7.8928, 86.9579);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol7_1, new cjs.Rectangle(-113.9, -112.1, 243.6, 236.39999999999998), null);


  (lib.Symbol1_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance_1 = new lib._3_Stars("synched", 0);
    this.instance_1.setTransform(2.95, 30.95);

    // this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1_1, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Light = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_29 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(29).call(this.frame_29).wait(1));

    // Layer_1
    this.instance = new lib.Stageon("synched", 0);
    this.instance.setTransform(6.05, 821);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(29).to({startPosition: 0}, 0).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AC5QaQg2g8AAh3IAA1DQl2EhiCAAQg+AAgxgxQgxgxABhDQgBhKAxglQAugjB6g4QCyhUBrhdQBqhdBTh1QBUhzAZgbQAZgbBFAAQBPABAvA9QAvA9gBBrIAAadQABEpjKABQhagBg5g8g");
    this.shape.setTransform(-22.65, 353.8);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(30));

    // Layer_3
    this.instance_1 = new lib.C_ON("synched", 0);
    this.instance_1.setTransform(0, 350.85, 1.0374, 1.0374);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(29).to({startPosition: 0}, 0).wait(1));

    // Layer_5
    this.instance_2 = new lib._3_Stars("synched", 0);
    this.instance_2.setTransform(2.95, 50.95, 0.2172, 0.2172);
    this.instance_2._off = true;

    this.movieClip_1 = new lib.Symbol1_1();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.movieClip_1}]}, 21).wait(1));

    // Layer_6
    this.instance_3 = new lib._3_stae_L("synched", 0);
    this.instance_3.setTransform(0, -0.05);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol327("synched", 0);
    this.instance_4.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 16).wait(1));

    // Layer_7
    this.instance_5 = new lib._3_Star_R("synched", 0);
    this.instance_5.setTransform(-0.05, 0);
    this.instance_5._off = true;

    this.instance_6 = new lib.Symbol328("synched", 0);
    this.instance_6.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_5}]}, 5).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_5}]}, 16).wait(1));

    // Layer_4
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.lf(["rgba(180,180,153,0.439)", "rgba(50,50,32,0.439)", "rgba(6,6,3,0.769)"], [0, 0.306, 1], 6.5, 77.6, 18.1, -167.4).s().p("EgpQAIOQxFjaAAk0QAAkyRFjaQRHjaYJAAQYKAARGDaQRGDaAAEyQAAE0xGDaQxGDZ4KAAQ4JAAxHjZg");
    this.shape_1.setTransform(10.875, 1004.225);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(30));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-362.5, -173.4, 746.8, 1252);


  (lib.Tween6copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjHEXABQEYgBDGDHQDGDGAAEXQAAEYjGDGQjGDGkYAAQkXAAjGjGg");
    this.shape.setTransform(0.125, -0.5);

    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjHEXABQEYgBDGDHQDGDGAAEXQAAEYjGDGQjGDGkYAAQkXAAjGjGg");
    this.shape.setTransform(0.125, -0.5);

    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjHEXABQEYgBDGDHQDGDGAAEXQAAEYjGDGQjGDGkYAAQkXAAjGjGg");
    this.shape.setTransform(0.125, -0.5);

    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjHEXABQEYgBDGDHQDGDGAAEXQAAEYjGDGQjGDGkYAAQkXAAjGjGg");
    this.shape.setTransform(0.125, -0.5);

    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjHEXABQEYgBDGDHQDGDGAAEXQAAEYjGDGQjGDGkYAAQkXAAjGjGg");
    this.shape.setTransform(0.125, -0.5);

    this.instance = new lib.C_1_ON_1("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ON_1("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(162,134,0,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjHEXABQEYgBDGDHQDGDGAAEXQAAEYjGDGQjGDGkYAAQkXAAjGjGg");
    this.shape_1.setTransform(0.125, -0.5);

    this.instance = new lib.C_1_ON("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ON("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape_1}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween5copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("ApTG0Qj3i0gBkAQABj+D3i1QD2i1FdAAQFeAAD2C1QD4C1AAD+QAAEAj4C0Qj2C0leABQldgBj2i0g");
    this.shape.setTransform(0.15, -0.45);

    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("ApTG0Qj3i0gBkAQABj+D3i1QD2i1FdAAQFeAAD2C1QD4C1AAD+QAAEAj4C0Qj2C0leABQldgBj2i0g");
    this.shape.setTransform(0.15, -0.45);

    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("ApTG0Qj3i0gBkAQABj+D3i1QD2i1FdAAQFeAAD2C1QD4C1AAD+QAAEAj4C0Qj2C0leABQldgBj2i0g");
    this.shape.setTransform(0.15, -0.45);

    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("ApTG0Qj3i0gBkAQABj+D3i1QD2i1FdAAQFeAAD2C1QD4C1AAD+QAAEAj4C0Qj2C0leABQldgBj2i0g");
    this.shape.setTransform(0.15, -0.45);

    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("ApTG0Qj3i0gBkAQABj+D3i1QD2i1FdAAQFeAAD2C1QD4C1AAD+QAAEAj4C0Qj2C0leABQldgBj2i0g");
    this.shape.setTransform(0.15, -0.45);

    this.instance = new lib.C_1_ON_1("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ON_1("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("ApTG0Qj3i0gBkAQABj+D3i1QD2i1FdAAQFeAAD2C1QD4C1AAD+QAAEAj4C0Qj2C0leABQldgBj2i0g");
    this.shape.setTransform(0.15, -0.45);

    this.instance = new lib.C_1_ON("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ON("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween4copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AiYJFQg/jxAAlUQAAlTA/jxQA/jxBZAAQBZAABADxQBADxAAFTQAAFUhADxQhADxhZAAQhZAAg/jxg");
    this.shape.setTransform(0.1, -0.65);

    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AiYJFQg/jxAAlUQAAlTA/jxQA/jxBZAAQBZAABADxQBADxAAFTQAAFUhADxQhADxhZAAQhZAAg/jxg");
    this.shape.setTransform(0.1, -0.65);

    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AiYJFQg/jxAAlUQAAlTA/jxQA/jxBZAAQBZAABADxQBADxAAFTQAAFUhADxQhADxhZAAQhZAAg/jxg");
    this.shape.setTransform(0.1, -0.65);

    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AiYJFQg/jxAAlUQAAlTA/jxQA/jxBZAAQBZAABADxQBADxAAFTQAAFUhADxQhADxhZAAQhZAAg/jxg");
    this.shape.setTransform(0.1, -0.65);

    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AiYJFQg/jxAAlUQAAlTA/jxQA/jxBZAAQBZAABADxQBADxAAFTQAAFUhADxQhADxhZAAQhZAAg/jxg");
    this.shape.setTransform(0.1, -0.65);

    this.instance = new lib.C_1_ON_1("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ON_1("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(162,134,0,0.659)").s().p("AiYJFQg/jxAAlUQAAlTA/jxQA/jxBZAAQBZAABADxQBADxAAFTQAAFUhADxQhADxhZAAQhZAAg/jxg");
    this.shape_1.setTransform(0.1, -0.65);

    this.instance = new lib.C_1_ON("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ON("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape_1}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween3copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AvrD5QmghmAAiTQAAiSGghnQGghnJLgBQJNABGgBnQGfBnAACSQAACTmfBmQmgBopNAAQpLAAmghog");
    this.shape.setTransform(0.275, -0.25);

    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AvrD5QmghmAAiTQAAiSGghnQGghnJLgBQJNABGgBnQGfBnAACSQAACTmfBmQmgBopNAAQpLAAmghog");
    this.shape.setTransform(0.275, -0.25);

    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AvrD5QmghmAAiTQAAiSGghnQGghnJLgBQJNABGgBnQGfBnAACSQAACTmfBmQmgBopNAAQpLAAmghog");
    this.shape.setTransform(0.275, -0.25);

    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AvrD5QmghmAAiTQAAiSGghnQGghnJLgBQJNABGgBnQGfBnAACSQAACTmfBmQmgBopNAAQpLAAmghog");
    this.shape.setTransform(0.275, -0.25);

    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AvrD5QmghmAAiTQAAiSGghnQGghnJLgBQJNABGgBnQGfBnAACSQAACTmfBmQmgBopNAAQpLAAmghog");
    this.shape.setTransform(0.275, -0.25);

    this.instance = new lib.C_1_ON_1("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ON_1("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(162,134,0,0.659)").s().p("AvrD5QmghmAAiTQAAiSGghnQGghnJLgBQJNABGgBnQGfBnAACSQAACTmfBmQmgBopNAAQpLAAmghog");
    this.shape_1.setTransform(0.275, -0.25);

    this.instance = new lib.C_1_ON("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ON("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape_1}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween2copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AjNNUQhWlgAAn0QAAnyBWliQBVliB4AAQB5AABVFiQBVFiABHyQgBH0hVFgQhVFih5AAQh4AAhVlig");
    this.shape.setTransform(0.1, -1);

    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AjNNUQhWlgAAn0QAAnyBWliQBVliB4AAQB5AABVFiQBVFiABHyQgBH0hVFgQhVFih5AAQh4AAhVlig");
    this.shape.setTransform(0.1, -1);

    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AjNNUQhWlgAAn0QAAnyBWliQBVliB4AAQB5AABVFiQBVFiABHyQgBH0hVFgQhVFih5AAQh4AAhVlig");
    this.shape.setTransform(0.1, -1);

    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AjNNUQhWlgAAn0QAAnyBWliQBVliB4AAQB5AABVFiQBVFiABHyQgBH0hVFgQhVFih5AAQh4AAhVlig");
    this.shape.setTransform(0.1, -1);

    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AjNNUQhWlgAAn0QAAnyBWliQBVliB4AAQB5AABVFiQBVFiABHyQgBH0hVFgQhVFih5AAQh4AAhVlig");
    this.shape.setTransform(0.1, -1);

    this.instance = new lib.C_1_ON_1("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ON_1("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(162,134,0,0.659)").s().p("AjNNUQhWlgAAn0QAAnyBWliQBVliB4AAQB5AABVFiQBVFiABHyQgBH0hVFgQhVFih5AAQh4AAhVlig");
    this.shape_1.setTransform(0.1, -1);

    this.instance = new lib.C_1_ON("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ON("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape_1}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween1copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(109,36,135,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjGEXAAQEYAADGDGQDGDGAAEXQAAEYjGDGQjGDHkYAAQkXAAjGjHg");
    this.shape.setTransform(0.1015, -0.45, 0.5056, 1);

    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(109,36,135,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjGEXAAQEYAADGDGQDGDGAAEXQAAEYjGDGQjGDHkYAAQkXAAjGjHg");
    this.shape.setTransform(0.1015, -0.45, 0.5056, 1);

    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(109,36,135,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjGEXAAQEYAADGDGQDGDGAAEXQAAEYjGDGQjGDHkYAAQkXAAjGjHg");
    this.shape.setTransform(0.1015, -0.45, 0.5056, 1);

    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(109,36,135,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjGEXAAQEYAADGDGQDGDGAAEXQAAEYjGDGQjGDHkYAAQkXAAjGjHg");
    this.shape.setTransform(0.1015, -0.45, 0.5056, 1);

    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AjxHeQhkjGAAkYQAAkXBkjHQBljFCMgBQCOABBkDFQBkDHAAEXQAAEYhkDGQhkDHiOAAQiMAAhljHg");
    this.shape.setTransform(0.1, -0.45);

    this.instance = new lib.C_1_ON_1("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ON_1("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(109,36,135,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjGEXAAQEYAADGDGQDGDGAAEXQAAEYjGDGQjGDHkYAAQkXAAjGjHg");
    this.shape_1.setTransform(0.1015, -0.45, 0.5056, 1);

    this.instance = new lib.C_1_ON("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ON("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}, {t: this.shape_1}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Symbol15 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Symbol12("synched", 0);
    this.instance.setTransform(0, 0.05, 1, 1, 0, 0, 0, 0.7, -0.8);

    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("Ehv7BXZMAAAiuxMDf2AAAMAAACuxgEhhSBJQMDBegDoMAAAiSVMjD9AAAg");
    this.shape.setTransform(-2.1, 1.525);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-718.4, -557.7, 1432.6999999999998, 1118.5);


  (lib.Symbol9_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance_1 = new lib.Symbol21();
    this.instance_1.setTransform(-1228.45, -301.5, 1.5356, 1.5356, 0, 0, 0, 0.1, -0.1);

    this.instance_2 = new lib.Symbol21();
    this.instance_2.setTransform(321.65, -272.85, 1.5356, 1.5356, 0, 0, 0, 0.1, -0.1);

    this.instance_3 = new lib.Symbol18();
    this.instance_3.setTransform(-274.2, -303.8, 1.5356, 1.5356);

    this.instance_4 = new lib.Symbol17();
    this.instance_4.setTransform(-822.55, -304.5, 1.5356, 1.5356, 0, 0, 0, -0.1, -0.1);

    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["#FFFFFF", "#00CCFF"], [0, 1], 9.5, -128.9, 9.7, -733.2).s().p("EirDBwTMAAAjglMFWHAAAMAAADglg");
    this.shape.setTransform(-428.775, 148.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape}, {t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol9_1, new cjs.Rectangle(-1523.5, -569.9, 2189.5, 1437.4), null);


  (lib.Symbol7copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFC000").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACIgEgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape.setTransform(5.8, -5.3833);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#542000").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBAAAAgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape_1.setTransform(5.8, -3.8833);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f().s("#F0EDED").ss(0.1, 1, 1).p("AEbABQAAB0hUBTQhSBTh1AAQh0AAhUhTQhShTAAh0QAAh1BShTQBUhTB0AAQB1AABSBTQBUBTAAB1g");
    this.shape_2.setTransform(6.125, -3.425);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("rgba(0,0,0,0.329)").s().p("AjIDIQhShTAAh0QAAh1BShTQBUhTB0AAQB1AABSBTQBUBTAAB1QAAB0hUBTQhSBTh1AAQh0AAhUhTg");
    this.shape_3.setTransform(6.125, -3.425);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#F3F3E8").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACIgEgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape_4.setTransform(5.8, -5.3833);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("rgba(52,100,196,0.769)").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBAAAAgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape_5.setTransform(5.8, -3.8833);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).to({state: [{t: this.shape_3}, {t: this.shape_2}]}, 1).to({state: [{t: this.shape_5}, {t: this.shape_4}]}, 1).wait(2));

    // Layer 1
    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#FFFFFF").s().p("AgbBfQgEgEAAgFQAAgJALgFQApgTAMgZQgWAAgKgGQgMgGAAgUQAAgPAJgOQAJgPAOAAQARAAALAOQAKANACATIAoAAQgHgQAAgKQAAgOAMgNQANgPAUgBQALAAALAHQAOAGAAALQAAALgJAAQgEAAgIgEQgIgDgFAAQgVABAAAQQAAAKAHAGQAHAFAKAAQAKAAAVgFIAGAAQAOAAAAAKQAAALgMAFQgfANg1gBIgoAAQgNAvg5AXIgHABQgGgBgDgDgAAMgRQgDADAAAEQAAAHAKACIAPABQAAgIgEgFQgEgHgHAAQgEAAgDADgAh0A3QABgMATgCIAegDQAWgFAAgGQACgOgggaQgLgKABgHQAAgGAFgEQAEgDAGAAQAPACARAaQARAZAAAQQAAATgOAKQgIAGgUAGQgNADgPABQgcgBACgPgAilAoQgKgIAAgPQAAgLAFgMQAEgJAFgGQgLgFAAgJQAAgGAFgFQAEgGAGAAQAUABAVARQAVATAAATQAAAVgPALQgMAMgSgBQgQAAgJgHgAiXAHQAAALAMAAQAFgBAFgCQAFgDAAgEQAAgLgTgIQgIAKAAAIgAiQhAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGgAi8hAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGg");
    this.shape_6.setTransform(5.6825, -3.8402, 0.8692, 0.8692);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#330000").s().p("AgbBfQgEgEAAgFQAAgJALgFQApgTAMgZQgWAAgKgGQgMgGAAgUQAAgPAJgOQAJgPAOAAQARAAALAOQAKANACATIAoAAQgHgQAAgKQAAgOAMgNQANgPAUgBQALAAALAHQAOAGAAALQAAALgJAAQgEAAgIgEQgIgDgFAAQgVABAAAQQAAAKAHAGQAHAFAKAAQAKAAAVgFIAGAAQAOAAAAAKQAAALgMAFQgfANg1gBIgoAAQgNAvg5AXIgHABQgGgBgDgDgAAMgRQgDADAAAEQAAAHAKACIAPABQAAgIgEgFQgEgHgHAAQgEAAgDADgAh0A3QABgMATgCIAegDQAWgFAAgGQACgOgggaQgLgKABgHQAAgGAFgEQAEgDAGAAQAPACARAaQARAZAAAQQAAATgOAKQgIAGgUAGQgNADgPABQgcgBACgPgAilAoQgKgIAAgPQAAgLAFgMQAEgJAFgGQgLgFAAgJQAAgGAFgFQAEgGAGAAQAUABAVARQAVATAAATQAAAVgPALQgMAMgSgBQgQAAgJgHgAiXAHQAAALAMAAQAFgBAFgCQAFgDAAgEQAAgLgTgIQgIAKAAAIgAiQhAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGgAi8hAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGg");
    this.shape_7.setTransform(5.6825, -2.8902, 0.8692, 0.8692);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape_7}, {t: this.shape_6}]}, 1).to({state: []}, 1).wait(2));

    // Layer 1
    this.instance = new lib.CachedBmp_133();
    this.instance.setTransform(-25.1, -35.05, 0.2582, 0.2582);

    this.instance_1 = new lib.Path();
    this.instance_1.setTransform(6.2, -3.4, 1.3907, 1.3907, 0, 0, 0, 19.7, 19.8);
    this.instance_1.compositeOperation = "screen";

    this.instance_2 = new lib.CachedBmp_113();
    this.instance_2.setTransform(-25.4, -35.1, 0.2582, 0.2582);

    this.instance_3 = new lib.Path_1();
    this.instance_3.setTransform(6.1, -3.8, 1.4695, 1.4695, 0, 0, 0, 19.7, 19.7);
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_132();
    this.instance_4.setTransform(-25.3, -35.2, 0.2582, 0.2582);

    this.instance_5 = new lib.CachedBmp_131();
    this.instance_5.setTransform(-30.05, -39.9, 0.2582, 0.2582);

    this.instance_6 = new lib.Blend();
    this.instance_6.setTransform(-0.45, 1.55, 1, 1, 0, 0, 0, 41.5, 38.5);
    this.instance_6.compositeOperation = "multiply";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_6}, {t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-41.9, -39.9, 83.9, 80);


  (lib.Symbol3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    //ss
    // Layer_1
    this.instance = new lib.Light();
    this.instance.setTransform(39.45, 50.3, 0.1078, 0.1078, 0, 0, 0, 7.4, 584.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-0.4, 0, 80.5, 103.6);


  (lib.Symbol1copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy4("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));
    //
    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy10, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy3("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy9, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy2("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy8, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy7, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Stars_1("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy6, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy4("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy4("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjGEXAAQEYAADGDGQDGDGAAEXQAAEYjGDGQjGDGkYAAQkXAAjGjGg");
    this.shape.setTransform(-0.175, -91.05);

    this.instance_2 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.shape}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy5, new cjs.Rectangle(-129.6, -172.2, 259.29999999999995, 344.6), null);


  (lib.Symbol1copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy3("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy3("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjGEXAAQEYAADGDGQDGDGAAEXQAAEYjGDGQjGDGkYAAQkXAAjGjGg");
    this.shape.setTransform(-0.175, -91.05);

    this.instance_2 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.shape}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy4, new cjs.Rectangle(-129.6, -172.2, 259.29999999999995, 344.6), null);


  (lib.Symbol1copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy2("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy2("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjGEXAAQEYAADGDGQDGDGAAEXQAAEYjGDGQjGDGkYAAQkXAAjGjGg");
    this.shape.setTransform(-0.175, -91.05);

    this.instance_2 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.shape}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy3, new cjs.Rectangle(-129.6, -172.2, 259.29999999999995, 344.6), null);


  (lib.Symbol1copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjGEXAAQEYAADGDGQDGDGAAEXQAAEYjGDGQjGDGkYAAQkXAAjGjGg");
    this.shape.setTransform(-0.175, -91.05);

    this.instance_2 = new lib.C_1_ONcopy("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.shape}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy2, new cjs.Rectangle(-129.6, -172.2, 259.29999999999995, 344.6), null);


  (lib.Symbol1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1_1("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_off_1("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjGEXAAQEYAADGDGQDGDGAAEXQAAEYjGDGQjGDGkYAAQkXAAjGjGg");
    this.shape.setTransform(-0.175, -91.05);

    this.instance_2 = new lib.C_1_ON_1("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ON_1("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.shape}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy, new cjs.Rectangle(-129.6, -172.2, 259.29999999999995, 344.6), null);


  (lib.Symbol1_2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance_2 = new lib.Locked_1("synched", 0);
    this.instance_2.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_3 = new lib.Less_1_off("synched", 0);
    this.instance_3.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(162,134,0,0.659)").s().p("AndHeQjGjGAAkYQAAkXDGjGQDGjGEXAAQEYAADGDGQDGDGAAEXQAAEYjGDGQjGDGkYAAQkXAAjGjGg");
    this.shape.setTransform(-0.175, -91.05);

    this.instance_4 = new lib.C_1_ON("synched", 0);
    this.instance_4.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_5 = new lib.C_1_ON("synched", 0);
    this.instance_5.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_5}, {t: this.instance_4}, {t: this.shape}, {t: this.instance_3}, {t: this.instance_2}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1_2, new cjs.Rectangle(-129.6, -172.2, 259.29999999999995, 344.6), null);


  (lib.OFFcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy4("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy5();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy4("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy5("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy5("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy5("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy5("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy5("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy5("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({
      _off: true,
      y: -24.7
    }, 2).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(2).to({_off: false}, 2).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["rgba(180,180,153,0.439)", "rgba(50,50,32,0.439)", "rgba(6,6,3,0.769)"], [0, 0.306, 1], 2.4, 31.5, 6.5, -67.9).s().p("AuvDVQmGhYAAh9QAAh8GGhYQGHhYIoAAQIoAAGHBYQGHBYAAB8QAAB9mHBYQmHBZooAAQooAAmHhZg");
    this.shape.setTransform(75.225, 146.5);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 418.20000000000005);


  (lib.OFFcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy3("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy4();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy3("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy4("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy4("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy4("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy4("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy4("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy4("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({
      _off: true,
      y: -24.7
    }, 2).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(2).to({_off: false}, 2).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["rgba(180,180,153,0.439)", "rgba(50,50,32,0.439)", "rgba(6,6,3,0.769)"], [0, 0.306, 1], 2.4, 31.5, 6.5, -67.9).s().p("AuvDWQmGhZAAh8QAAh9GGhYQGHhZIoAAQIoAAGHBZQGHBYAAB9QAAB8mHBZQmHBXooAAQooAAmHhXg");
    this.shape.setTransform(75.225, 146.55);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 418.20000000000005);


  (lib.OFF = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy2("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy3();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy2("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy3("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy3("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy3("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy3("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy3("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy3("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({
      _off: true,
      y: -24.7
    }, 2).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(2).to({_off: false}, 2).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["rgba(180,180,153,0.439)", "rgba(50,50,32,0.439)", "rgba(6,6,3,0.769)"], [0, 0.306, 1], 2.4, 31.5, 6.5, -67.9).s().p("AuvDVQmGhYAAh9QAAh8GGhYQGHhYIoAAQIoAAGHBYQGHBYAAB8QAAB9mHBYQmHBZooAAQooAAmHhZg");
    this.shape.setTransform(75.225, 146.5);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 418.20000000000005);


  (lib.Lightcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_29 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(29).call(this.frame_29).wait(1));

    // Layer_1
    this.instance = new lib.Stageoncopy4("synched", 0);
    this.instance.setTransform(6.05, 821);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(29).to({startPosition: 0}, 0).wait(1));

    // Layer_2
    this.text = new cjs.Text("6", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.parent = this;
    this.text.setTransform(-1.3, 235.9);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(30));

    // Layer_3
    this.instance_1 = new lib.C_ONcopy5("synched", 0);
    this.instance_1.setTransform(0, 350.85, 1.0374, 1.0374);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(29).to({startPosition: 0}, 0).wait(1));

    // Layer_5
    this.instance_2 = new lib._3_Starscopy4("synched", 0);
    this.instance_2.setTransform(2.95, 50.95, 0.2172, 0.2172);
    this.instance_2._off = true;

    this.movieClip_1 = new lib.Symbol1copy10();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.movieClip_1}]}, 21).wait(1));

    // Layer_6
    this.instance_3 = new lib._3_stae_Lcopy4("synched", 0);
    this.instance_3.setTransform(0, -0.05);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol325("synched", 0);
    this.instance_4.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 16).wait(1));

    // Layer_7
    this.instance_5 = new lib._3_Star_Rcopy4("synched", 0);
    this.instance_5.setTransform(-0.05, 0);
    this.instance_5._off = true;

    this.instance_6 = new lib.Symbol326("synched", 0);
    this.instance_6.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_5}]}, 5).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_5}]}, 16).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["rgba(180,180,153,0.439)", "rgba(50,50,32,0.439)", "rgba(6,6,3,0.769)"], [0, 0.306, 1], 6.5, 77.6, 18.1, -167.4).s().p("EgpQAIOQxFjaAAk0QAAkyRFjaQRHjaYJAAQYKAARGDaQRGDaAAEyQAAE0xGDaQxGDZ4KAAQ4JAAxHjZg");
    this.shape.setTransform(10.875, 1004.225);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(30));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-362.5, -173.4, 746.8, 1252);


  (lib.Lightcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_29 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(29).call(this.frame_29).wait(1));

    // Layer_1
    this.instance = new lib.Stageoncopy3("synched", 0);
    this.instance.setTransform(6.05, 821);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(29).to({startPosition: 0}, 0).wait(1));

    // Layer_2
    this.text = new cjs.Text("5", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.parent = this;
    this.text.setTransform(-1.3, 235.9);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(30));

    // Layer_3
    this.instance_1 = new lib.C_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 350.85, 1.0374, 1.0374);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(29).to({startPosition: 0}, 0).wait(1));

    // Layer_5
    this.instance_2 = new lib._3_Starscopy3("synched", 0);
    this.instance_2.setTransform(2.95, 50.95, 0.2172, 0.2172);
    this.instance_2._off = true;

    this.movieClip_1 = new lib.Symbol1copy9();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.movieClip_1}]}, 21).wait(1));

    // Layer_6
    this.instance_3 = new lib._3_stae_Lcopy3("synched", 0);
    this.instance_3.setTransform(0, -0.05);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol323("synched", 0);
    this.instance_4.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 16).wait(1));

    // Layer_7
    this.instance_5 = new lib._3_Star_Rcopy3("synched", 0);
    this.instance_5.setTransform(-0.05, 0);
    this.instance_5._off = true;

    this.instance_6 = new lib.Symbol324("synched", 0);
    this.instance_6.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_5}]}, 5).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_5}]}, 16).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["rgba(180,180,153,0.439)", "rgba(50,50,32,0.439)", "rgba(6,6,3,0.769)"], [0, 0.306, 1], 6.5, 77.6, 18.1, -167.4).s().p("EgpQAIOQxFjaAAk0QAAkyRFjaQRHjaYJAAQYKAARGDaQRGDaAAEyQAAE0xGDaQxGDZ4KAAQ4JAAxHjZg");
    this.shape.setTransform(10.875, 1004.225);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(30));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-362.5, -173.4, 746.8, 1252);


  (lib.Lightcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_29 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(29).call(this.frame_29).wait(1));

    // Layer_1
    this.instance = new lib.Stageoncopy2("synched", 0);
    this.instance.setTransform(6.05, 821);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(29).to({startPosition: 0}, 0).wait(1));

    // Layer_2
    this.text = new cjs.Text("4", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.parent = this;
    this.text.setTransform(-1.3, 235.9);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(30));

    // Layer_3
    this.instance_1 = new lib.C_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 350.85, 1.0374, 1.0374);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(29).to({startPosition: 0}, 0).wait(1));

    // Layer_5
    this.instance_2 = new lib._3_Starscopy2("synched", 0);
    this.instance_2.setTransform(2.95, 50.95, 0.2172, 0.2172);
    this.instance_2._off = true;

    this.movieClip_1 = new lib.Symbol1copy8();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.movieClip_1}]}, 21).wait(1));

    // Layer_6
    this.instance_3 = new lib._3_stae_Lcopy2("synched", 0);
    this.instance_3.setTransform(0, -0.05);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol321("synched", 0);
    this.instance_4.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 16).wait(1));

    // Layer_7
    this.instance_5 = new lib._3_Star_Rcopy2("synched", 0);
    this.instance_5.setTransform(-0.05, 0);
    this.instance_5._off = true;

    this.instance_6 = new lib.Symbol322("synched", 0);
    this.instance_6.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_5}]}, 5).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_5}]}, 16).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["rgba(180,180,153,0.439)", "rgba(50,50,32,0.439)", "rgba(6,6,3,0.769)"], [0, 0.306, 1], 6.5, 77.6, 18.1, -167.4).s().p("EgpQAIOQxFjaAAk0QAAkyRFjaQRHjaYJAAQYKAARGDaQRGDaAAEyQAAE0xGDaQxGDZ4KAAQ4JAAxHjZg");
    this.shape.setTransform(10.875, 1004.225);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(30));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-362.5, -173.4, 746.8, 1252);


  (lib.Lightcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_29 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(29).call(this.frame_29).wait(1));

    // Layer_1
    this.instance = new lib.Stageoncopy("synched", 0);
    this.instance.setTransform(6.05, 821);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(29).to({startPosition: 0}, 0).wait(1));

    // Layer_2
    this.text = new cjs.Text("3", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.parent = this;
    this.text.setTransform(-1.3, 235.9);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(30));

    // Layer_3
    this.instance_1 = new lib.C_ONcopy2("synched", 0);
    this.instance_1.setTransform(0, 350.85, 1.0374, 1.0374);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(29).to({startPosition: 0}, 0).wait(1));

    // Layer_5
    this.instance_2 = new lib._3_Starscopy("synched", 0);
    this.instance_2.setTransform(2.95, 50.95, 0.2172, 0.2172);
    this.instance_2._off = true;

    this.movieClip_1 = new lib.Symbol1copy7();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.movieClip_1}]}, 21).wait(1));

    // Layer_6
    this.instance_3 = new lib._3_stae_Lcopy("synched", 0);
    this.instance_3.setTransform(0, -0.05);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol319("synched", 0);
    this.instance_4.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 16).wait(1));

    // Layer_7
    this.instance_5 = new lib._3_Star_Rcopy("synched", 0);
    this.instance_5.setTransform(-0.05, 0);
    this.instance_5._off = true;

    this.instance_6 = new lib.Symbol320("synched", 0);
    this.instance_6.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_5}]}, 5).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_5}]}, 16).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["rgba(180,180,153,0.439)", "rgba(50,50,32,0.439)", "rgba(6,6,3,0.769)"], [0, 0.306, 1], 6.5, 77.6, 18.1, -167.4).s().p("EgpQAIOQxFjaAAk0QAAkyRFjaQRHjaYJAAQYKAARGDaQRGDaAAEyQAAE0xGDaQxGDZ4KAAQ4JAAxHjZg");
    this.shape.setTransform(10.875, 1004.225);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(30));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-362.5, -173.4, 746.8, 1252);


  (lib.Light_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_29 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(29).call(this.frame_29).wait(1));

    // Layer_1
    this.instance_7 = new lib.Stageon_1("synched", 0);
    this.instance_7.setTransform(6.05, 821);

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(29).to({startPosition: 0}, 0).wait(1));

    // Layer_2
    this.text = new cjs.Text("2", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.parent = this;
    this.text.setTransform(-1.3, 235.9);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(30));

    // Layer_3
    this.instance_8 = new lib.C_ONcopy("synched", 0);
    this.instance_8.setTransform(0, 350.85, 1.0374, 1.0374);

    this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(29).to({startPosition: 0}, 0).wait(1));

    // Layer_5
    this.instance_2 = new lib._3_Stars_1("synched", 0);
    this.instance_2.setTransform(2.95, 50.95, 0.2172, 0.2172);
    this.instance_2._off = true;

    this.movieClip_1_1 = new lib.Symbol1copy6();
    this.movieClip_1_1.name = "movieClip_1_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.movieClip_1_1}]}, 21).wait(1));

    // Layer_6
    this.instance_3 = new lib._3_stae_L_1("synched", 0);
    this.instance_3.setTransform(0, -0.05);
    this.instance_3._off = true;

    this.instance_11 = new lib.Symbol317("synched", 0);
    this.instance_11.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_11}]}, 2).to({state: [{t: this.instance_3}]}, 16).wait(1));

    // Layer_7
    this.instance_5 = new lib._3_Star_R_1("synched", 0);
    this.instance_5.setTransform(-0.05, 0);
    this.instance_5._off = true;

    this.instance_13 = new lib.Symbol318("synched", 0);
    this.instance_13.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_5}]}, 5).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_13}]}, 2).to({state: [{t: this.instance_5}]}, 16).wait(1));

    // Layer_4
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.lf(["rgba(180,180,153,0.439)", "rgba(50,50,32,0.439)", "rgba(6,6,3,0.769)"], [0, 0.306, 1], 6.5, 77.6, 18.1, -167.4).s().p("EgpQAIOQxFjaAAk0QAAkyRFjaQRHjaYJAAQYKAARGDaQRGDaAAEyQAAE0xGDaQxGDZ4KAAQ4JAAxHjZg");
    this.shape_2.setTransform(10.875, 1004.225);

    this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(30));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-362.5, -173.4, 746.8, 1252);


  (lib.B_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy2();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy2("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy2("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy2("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy2("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy2("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy2("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({
      _off: true,
      y: -24.7
    }, 2).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(2).to({_off: false}, 2).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["rgba(180,180,153,0.439)", "rgba(50,50,32,0.439)", "rgba(6,6,3,0.769)"], [0, 0.306, 1], 2.4, 31.5, 6.5, -67.9).s().p("AuvDWQmGhZAAh9QAAh8GGhYQGHhZIoAAQIoAAGHBZQGHBYAAB8QAAB9mHBZQmHBXooAAQooAAmHhXg");
    this.shape.setTransform(80.975, 147.55);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 418.20000000000005);


  (lib._6copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib.OFFcopy2();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off: false}, 0).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AAcIQQgBAAAAAAQAAgBgBAAQAAAAAAABQAAAAgBAAIAAAAQgVgEgRgGIgBAAQgNgCgLgGIgBAAIgigMIgBAAIgggLIgDAAIgBAAQgDABgCgDIAAAAIgFAAIgBABQgDABgCgCIgBAAIgDAAIgBAAQgEABgCgCIgBAAIgBAAIgCAAQgEAAgCgBIgBAAIgCAAQgEAAgCgBIAAAAIgCAAIAAAAQgEAAgCgCIgBAAIgBAAQgEACgCgDIgBAAIgBAAQgDABgCgCIgBgBIgVgDIgBAAQgggGgegIIgBgBQghgHgbgPIgBAAQg2gggJhLIgBgBIAAgHIAAAAIAAgCIgBAAIAAgHIABAAIAAgBIAAgBQAOgQAVgIIACAAIgBgBIgCgKIgBAAIgMgcIAAgBQgGgNgDgPIgBAAIgIgeIAAgBQgEgMgBgPIgBAAQgBgDABgFQgCgCAAgDIAAgCIAAgBIgBAAIAAgGIAAgCIAAgEIAAgBIgBgGIAAgBIAAgNIAAgCIAAgFIABAAIAAgBIAAgBIAAgCIAAgEIABAAIAAgCIAAgEQAAAAABAAQAAgBAAAAQABgBAAAAQAAgBAAgBIAAgBIAAgBIAAgIIABAAIAAgBIAAgCQASgVAfgJIACAAIAAgBIAMgDIABAAIAAgCIAxgNIABAAQAIAAAHgCIABAAIAqgIIAAAAIAAgBIAGAAIACAAIABAAIAAgBIAEAAIACAAIABAAIAAgCIAFAAIABAAIACAAIABAAIAAgBIAFAAIABAAIADAAIABAAIAAgBIAFAAIABAAIABAAIABAAIAAgBIAFAAIABAAIAEAAIABAAIAAgBIAGAAIABAAIADAAIAAAAIAAgBIAGAAIACAAIADAAIACAAIAAgBIAFAAIABAAIACAAIAAAAIAAgBIAGAAIABAAIAGAAIABAAIABAAIACAAIAAgCIAGAAIAAAAIAGAAIABAAIACAAIABAAIAAgBIAGAAIABAAIAFAAIADgCIAAAAIAAAAQANgLARgHIABAAIAAAAQAOgEAQgDIABAAIAAgBIAKAAIABAAIACAAIABAAIAGAAQhkgChGhHQhJhKAAhnQAAhpBJhIQApgqAzgSQAngOAsAAQArAAAoAOQAyASAqAqQBIBIAABpQAABnhIBKQhHBHhkACIAAABQATABANAHIABAAIAEACIAAAAIAGADIAAABQANAGALAHIAGABIAAAAIABAAIACAAIAGAAIAAAAIAHACIAAAAIAEAAIABAAIAGAAIABABIAGAAIABAAIAGABIAAAAIAEAAIABAAIAHABIAAAAIABAAIABAAIAHAAIAAABIADAAIABAAIAHABIABAAIACAAIAGAAIAAABIACAAIABAAIAFABIACAAIAGABIAAABIACAAIAHABIAAAAIAHAAIAAABIABAAIAHAAIAAABQBgAMBMAeQAVAIADAWIAAAFIAAABIAAAFIAAACIAAAAQABAEgCACIAAADIAAACIAAABQABADgCACIAAAEIAAABIAAABQABADgCACIAAACIAAACIAAAAQABAEgCACIAAABIAAABIAAAFIgBACQgBAJgCAJIAAABQgEAcgKAYIAAACIAAABQgDAOgGAMIAAABIgEAKIAAAAQgFAOgJAMIAAABIAAAAIAAABIAAABIAAABQAhAIASAVIABABIAAAFIAAABIgBAAIAAAAQgHAdgKAWQghBMhbARIAAABQgVAHgYACIAAABIgIAAIAAABQgCACgFgBIAAABIgGAAIgBAAIgDAAIAAAAIgFABIAAAAIgGABIgBAAIgBAAIAAAAIgHAAIgBAAIgGAAIgBAAIAAABIgFABIAAABIgHAAIAAAAIgEAAIAAAAIgGABIgCAAIgFAAIAAABIgHABIgBAAIgFAAIgBAAIgGAAIgCAAIgFAAIgBAAIgHAAIgBAAIAAABIgHABIAAAAIgGAAIgCAAIgEAAIgBAAIgDAEIAAABQAAAAAAABQgBAAAAABQAAAAgBABQAAAAAAABIAAAAIgCADIAAACQgBAAAAABQgBAAAAABQgBAAAAAAQAAAAgBABIgBAAQgGAKgNAEIAAACIgBAAIgDABIgBAAIgJACgADekwIAAABIABAAIgBgDgADZlPIAAABIAAAAIABAEIgDgNIACAIgADRloIABACIgBgDg");
    this.shape.setTransform(0, -25.05);
    this.shape._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1).to({_off: false}, 0).to({_off: true}, 1).wait(2));

    // Layer_1
    this.instance_1 = new lib.OFFcopy2();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-42, -77.9, 81.8, 106.9);


  (lib._6copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.247)").s().p("AAcIQQgBAAAAAAQAAgBgBAAQAAAAAAABQAAAAgBAAIAAAAQgVgEgRgGIgBAAQgNgCgLgGIgBAAIgigMIgBAAIgggLIgDAAIgBAAQgDABgCgDIAAAAIgFAAIgBABQgDABgCgCIgBAAIgDAAIgBAAQgEABgCgDIgBAAIgBAAIgCAAQgEABgCgBIgBAAIgCAAQgEAAgCgBIAAAAIgCAAIAAAAQgEAAgCgCIgBAAIgBAAQgEABgCgBIgBAAIgBAAQgDAAgCgCIgBgBIgVgDIgBAAQgggGgegIIgBgBQghgHgbgPIgBAAQg2gfgJhMIgBgBIAAgHIAAAAIAAgCIgBAAIAAgHIABAAIAAgBIAAgBQAOgQAVgIIACAAIgBgBIgCgKIgBAAIgMgcIAAgBQgGgNgDgPIgBAAIgIgeIAAgBQgEgMgBgPIgBAAQgBgDABgFQgCgCAAgDIAAgBIAAgCIgBAAIAAgGIAAgBIAAgGIAAAAIgBgGIAAgBIAAgNIAAgBIAAgGIABAAIAAgBIAAgBIAAgBIAAgGIABAAIAAgBIAAgEQAAAAABAAQAAgBAAAAQABgBAAAAQAAgBAAgBIAAgBIAAgBIAAgIIABAAIAAgBIAAgBQASgWAfgJIACAAIAAgBIAMgDIABAAIAAgBIAxgOIABAAQAIAAAHgCIABAAIAqgIIAAAAIAAgBIAGAAIACAAIABAAIAAgBIAEAAIACAAIABAAIAAgBIAFAAIABAAIACAAIABAAIAAgCIAFAAIABAAIADAAIABAAIAAgBIAFAAIABAAIABAAIABAAIAAgBIAFAAIABAAIAEAAIABAAIAAgBIAGAAIABAAIADAAIAAAAIAAgBIAGAAIACAAIADAAIACAAIAAgBIAFAAIABAAIACAAIAAAAIAAgBIAGAAIABAAIAGAAIABAAIABAAIACAAIAAgBIAGAAIAAAAIAGAAIABAAIACAAIABAAIAAgCIAGAAIABAAIAFAAIADgCIAAAAIAAgBQANgKARgHIABAAIAAgBQAOgEAQgCIABAAIAAgBIAKAAIABAAIACAAIABAAIAGAAQhkgChGhHQhJhKAAhnQAAhpBJhIQApgqAzgSQAngNAsAAQArAAAoANQAyASAqAqQBIBIAABpQAABnhIBKQhHBHhkACIAAABQATABANAHIABAAIAEACIAAAAIAGADIAAABQANAGALAHIAGABIAAABIABAAIACAAIAGAAIAAAAIAHABIAAAAIAEAAIABAAIAGAAIABABIAGAAIABAAIAGABIAAAAIAEAAIABAAIAHAAIAAABIABAAIABAAIAHAAIAAABIADAAIABAAIAHABIABAAIACAAIAGAAIAAABIACAAIABAAIAFACIACAAIAGAAIAAABIACAAIAHABIAAAAIAHAAIAAABIABAAIAHAAIAAABQBgAMBMAeQAVAIADAWIAAAFIAAABIAAAFIAAACIAAABQABAEgCABIAAAEIAAABIAAABQABAEgCABIAAAEIAAABIAAAAQABAEgCACIAAACIAAABIAAABQABAEgCACIAAABIAAABIAAAGIgBABQgBAJgCAJIAAABQgEAcgKAYIAAACIAAABQgDAOgGAMIAAABIgEAJIAAABQgFAOgJAMIAAABIAAAAIAAABIAAABIAAABQAhAIASAVIABABIAAAFIAAABIgBAAIAAAAQgHAdgKAWQghBMhbARIAAACQgVAFgYAEIAAAAIgIAAIAAABQgCACgFgBIAAABIgGAAIgBAAIgDAAIAAAAIgFAAIAAABIgGABIgBAAIgBAAIAAAAIgHAAIgBAAIgGAAIgBAAIAAABIgFABIAAABIgHAAIAAAAIgEAAIAAABIgGAAIgCAAIgFAAIAAABIgHABIgBAAIgFAAIgBAAIgGAAIgCAAIgFAAIgBAAIgHAAIgBAAIAAABIgHAAIAAAAIgGAAIgCAAIgEAAIgBAAIgDAFIAAABQAAAAAAABQgBAAAAABQAAAAgBABQAAAAAAABIAAAAIgCADIAAACQgBAAAAABQgBAAAAABQgBAAAAAAQAAAAgBABIgBAAQgGAKgNAFIAAABIgBAAIgDABIgBAAIgJABgADekwIAAABIABAAIgBgDgADZlPIAAAAIAAABIACAEIgEgNIACAIgADRloIABACIgBgDg");
    this.shape.setTransform(0, -24.05);

    this.instance = new lib.OFFcopy();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_1
    this.instance_1 = new lib.OFFcopy();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-42, -76.9, 81.8, 105.9);


  (lib._6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AAZIOIAAAAQgVgEgRgGIgBAAQgNgCgLgGIgBAAIghgMIgCAAIgggKIgDAAIgBAAQgCAAgCgBIgBAAIgEAAIgBAAQgEABgCgCIAAAAIgEAAIgBAAQgDABgCgDIgBAAIgCAAIgCAAQgDABgDgCIAAAAIgCAAQgEABgCgCIgBAAIgBAAIgBAAQgDABgCgCIgCAAIAAAAQgEABgCgCIgBAAIgBAAQgEAAgBgBIgCAAIgVgFIAAAAQgggGgegHIgBgBQghgHgbgQIgBAAQg2gegJhNIAAAAIgBgGIAAgBIAAgCIgBAAIAAgHIABAAIAAgBIAAgBQAPgPAVgJIABAAIgBAAIgCgLIgBAAIgLgcIgBgBQgFgNgEgPIgBAAIgIgdIAAgBQgEgMgBgOIAAgBQgCgDABgEIAAAAIgBgHIAAgBIAAgBIgBAAIgBgGIAAgCIAAgFIAAAAIAAgGIAAgBIAAgOIAAgBIAAgFIAAAAIAAgCIAAgBIAAgBIAAgEIACAAIAAgCIAAgEQAAAAAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBIAAgCIAAgBIAAgHIABAAIAAgBIAAgCQATgVAfgIIABAAIAAgCIAMgDIABAAIAAgCIAxgMIABAAQAIgBAHgCIABgBQAVgDAUgEIABgBIAAAAIAGAAIABAAIACAAIAAgBIAEAAIACAAIABAAIAAgCIAFAAIABAAIABAAIABAAIAAAAIAFAAIABAAIADAAIABAAIAAgBIAFAAIACAAIABAAIABAAIAAgBIAFAAIABAAIAEAAIABAAIAAgBIAGAAIAAAAIADAAIABAAIAAgBIAGAAIACAAIADAAIABAAIAAgCIAGAAIABAAIABAAIABAAIAAgBIAFAAIABAAIAGAAIABAAIACAAIABAAIAAgBIAGAAIABAAIAFAAIABAAIADAAIAAAAIAAgCIAGAAIABAAIAFAAIADgBIABAAIAAgBQANgKAQgHIABAAIAAgBQAOgEAQgCIABAAIgBgBIALAAIABAAIACAAIABAAIAGAAQhjgDhGhGQhJhKAAhnQAAhoBJhJQApgpAygRQAmgPAtAAQArAAAoAPQAyARApApQBIBJAABoQAABnhIBKQhHBGhiADIAAABQARABANAHIABAAIAFABIAAABIAFACIAAABQAOAGALAHIAGABIAAABIABAAIACAAIAFAAIABAAIAHAAIAAABIAEAAIABAAIAGAAIAAABIAGAAIABAAIAGABIAAABIAFAAIABAAIAHAAIAAABIABAAIABAAIAHAAIAAABIACAAIABAAIAHAAIAAABIACAAIABAAIAHAAIAAABIABAAIABAAIAGAAIABAAIAHABIAAABIABAAIAHABIABAAIAGAAIAAAAIABAAIAHABIAAACQBgALBLAeQAVAJAEAWIAAAEIAAABIAAAGIAAABIAAAAQABAEgCACIAAADIAAABIAAACQABADgCACIAAADIAAABIAAACQAAAEgBABIAAACIAAABIAAACQAAADgBACIAAAAIAAACIgBAFIAAABQgBAKgDAJIAAAAQgEAdgIAXIAAACIAAABQgFAOgFAMIAAABIgFAKIAAABQgFAOgHAKIAAADIgBAAIAAAAIAAACIABABQAfAHASAVIABAAIAAAGIAAABIgBAAIAAABQgGAbgLAXQggBMhbARIAAABQgVAGgYADIAAAAIgHABIAAAAQgDACgFgBIAAABIgGAAIgBAAIgCAAIAAABIgGAAIAAABIgGABIgBAAIgBAAIgBAAIgGABIAAAAIgGAAIgBAAIgGABIAAABIgGAAIgBAAIgEAAIAAAAIgHAAIAAAAIgFAAIgBABIgGABIgBAAIgGAAIgBAAIgGABIgCAAIgEAAIgBAAIgHAAIgBAAIgHABIgBAAIgFAAIgCAAIgEAAIgCAAIgDAFIAAAAQAAABAAAAQAAABAAAAQAAABgBAAQAAABgBAAIgCAEIAAACQAAAAgBAAQAAABAAAAQgBAAAAABQgBAAgBAAIAAAAQgGALgNAEIAAABIgBAAIgDABIgCAAIgJABQAAAAAAAAQAAAAgBAAQAAAAAAAAQAAAAgBAAgADdkvIAAABIABABIgBgDgADYlOIAAACIAAAAIACAEIgDgLIABAFg");
    this.shape.setTransform(-0.1, -24.45);

    this.instance = new lib.OFF();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_1
    this.instance_1 = new lib.OFF();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-42, -77.1, 81.8, 106.1);


  (lib.B_1_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance_9 = new lib.Locked_1_1("synched", 0);
    this.instance_9.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance_9._off = true;

    this.instance_10 = new lib.Symbol1copy();
    this.instance_10.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_9}]}, 11).to({state: [{t: this.instance_9}]}, 3).to({state: [{t: this.instance_9}]}, 1).to({state: [{t: this.instance_9}]}, 1).to({state: [{t: this.instance_9}]}, 1).to({state: [{t: this.instance_9}]}, 1).to({state: [{t: this.instance_10}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_3
    this.instance_11 = new lib.Less_1_off_1("synched", 0);
    this.instance_11.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_11).to({_off: true}, 19).wait(1));

    // C
    this.instance_12 = new lib.Tween1copy("synched", 0);
    this.instance_12.setTransform(78.7, 76.2);
    this.instance_12._off = true;

    this.instance_13 = new lib.Tween2copy("synched", 0);
    this.instance_13.setTransform(78.7, -24.7);
    this.instance_13._off = true;

    this.instance_14 = new lib.Tween3copy("synched", 0);
    this.instance_14.setTransform(78.7, -127.7);
    this.instance_14._off = true;

    this.instance_15 = new lib.Tween4copy("synched", 0);
    this.instance_15.setTransform(78.6, -71.2);
    this.instance_15._off = true;

    this.instance_16 = new lib.Tween5copy("synched", 0);
    this.instance_16.setTransform(78.7, -96);
    this.instance_16._off = true;

    this.instance_17 = new lib.Tween6copy("synched", 0);
    this.instance_17.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_12}]}, 2).to({state: [{t: this.instance_13}]}, 2).to({state: [{t: this.instance_14}]}, 2).to({state: [{t: this.instance_15}]}, 2).to({state: [{t: this.instance_16}]}, 2).to({state: [{t: this.instance_17}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(2).to({_off: false}, 0).to({
      _off: true,
      y: -24.7
    }, 2).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(2).to({_off: false}, 2).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.lf(["rgba(180,180,153,0.439)", "rgba(50,50,32,0.439)", "rgba(6,6,3,0.769)"], [0, 0.306, 1], 2.4, 31.5, 6.5, -67.9).s().p("AuvDVQmGhYAAh8QAAh9GGhYQGHhYIogBQIoABGHBYQGHBYAAB9QAAB8mHBYQmHBZoogBQooABmHhZg");
    this.shape_1.setTransform(75.225, 148.15);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(20));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 418.20000000000005);


  (lib.All_Space = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_22
    this.instance = new lib.Symbol7copy();
    this.instance.setTransform(-0.05, 615.3, 1.9367, 1.9367);
    new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib.Symbol7copy(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_18
    this.instance_1 = new lib.Symbol15("synched", 0);
    this.instance_1.setTransform(1.75, -4.85, 1.2652, 1.3156, 0, 0, 0, -0.1, -0.2);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.All_Space, new cjs.Rectangle(-907.1, -738.3, 1812.7, 1471.5), null);


  (lib.Symbol15_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(0,0,0,0.239)").s().p("AAgJzQAAAAAAgBQAAAAgBAAQAAAAAAAAQAAABgBAAIgBAAQgYgFgVgHIgBAAQgPgDgNgGIgBAAQgVgHgTgIIgCAAIgmgNIgEAAIgBABQgDAAgCgCIgCAAIgEAAIgBAAQgFABgCgCIgBAAIgEAAIgBAAQgEABgCgDIgBAAIgDAAIgCAAQgFABgCgCIgBAAIAAAAIgCAAQgFABgCgCIgBAAIgBAAIgBAAQgFABgCgDIgBAAIgBAAQgFACgCgDIgBAAIgBAAQgFABgCgCIgBgBIgZgEIgBAAQgngHgigKIgBgBQgogIgfgSIgBAAQhBglgLhbIgBAAIAAgIIAAgBIAAgCIgBAAIgBgIIACAAIAAgCIAAgBQARgSAZgKIACAAIgBgBIgDgMIgBAAIgOgiIAAgBQgHgPgDgSIgCAAIgJgjIAAgCQgFgNgCgSIAAgBQgCgDABgFIAAAAQgCgEAAgEIAAgBIAAgBIAAAAIgBgIIAAgBIAAgHIgBAAIAAgHIAAgBIAAgQIAAgCIAAgGIABAAIAAgCIAAgBIAAgBIAAgGIABAAIAAgCIAAgEQACgBAAgFIAAgBIAAgBIAAgKIABAAIAAAAIAAgCQAXgaAlgKIABAAIAAgCIAOgEIABAAIAAgBIA7gPIABAAQAJgBAJgDIABAAIAxgJIACgBIAAgBIAGAAIACAAIABAAIAAAAIAFAAIADAAIABAAIAAgCIAGAAIACAAIABAAIABAAIAAgBIAGAAIACAAIACAAIABAAIAAgCIAHAAIABAAIABAAIABAAIAAgBIAHAAIABAAIAFAAIABAAIAAgBIAHAAIABAAIADAAIABAAIAAgCIAHAAIACAAIAFAAIABAAIAAgBIAHAAIABAAIACAAIAAAAIAAgBIAHAAIABAAIAHAAIABAAIACAAIABAAIAAgCIAHAAIABAAIAHAAIABAAIACAAIACAAIAAgBIAGAAIABAAIAHAAIADgCIAAAAIAAgCQAQgMAUgIIABAAIAAgBQARgFATgCIABgBIgBAAIANAAIABAAIACAAIABAAIAHAAQh1gDhUhVQhWhXgBh7QABh8BWhWQAxgyA8gVQAugQA1AAQAzAAAvAQQA9AVAxAyQBVBWAAB8QAAB7hVBXQhUBVh3ADIAAAAQAWACAQAIIABAAIAEACIAAABIAHADIAAAAQAQAIANAJIAHABIAAAAIACAAIACAAIAGAAIABAAIAIABIAAABIAGAAIABAAIAHAAIABABIAGAAIACAAIAHABIAAAAIAFAAIABAAIAIABIAAABIABAAIABAAIAJAAIAAABIAEAAIABAAIAIABIAAAAIACAAIABAAIAIABIAAABIACAAIABAAIAHABIACAAIAHABIAAABIACAAIAHAAIACAAIAHABIAAAAIABAAIAJABIAAABQByAOBaAkQAZAKAFAaIAAAGIAAABIAAAGIAAACIAAABQAAAFgCABIAAAEIAAACIAAABQABAFgCABIAAAEIAAACIAAABQAAAFgBABIAAADIAAABIAAACQABAEgDACIAAABIAAACIgBAGIAAABQAAAMgDAKIAAABQgGAigLAcIAAACIAAABQgEARgHAOIAAABIgFAMIAAABQgHAQgIAOIAAACIgBAAIgBABIABABIABABQAlAKAWAZIABAAIAAAHIAAABIgCAAIAAABQgGAhgNAbQgnBahsAUIAAACQgaAHgcADIAAABIgIAAIgBABQgCACgGgBIAAABIgHAAIgCAAIgDAAIAAABIgGAAIAAABIgHABIgCAAIgBAAIAAAAIgIABIgBAAIgHAAIgBAAIAAAAIgHABIAAABIgIABIgBAAIgDAAIAAAAIgIABIgBAAIgHAAIAAABIgIAAIgBAAIgHAAIgBAAIgIABIgBAAIgGAAIgBAAIgJAAIgBAAIAAABIgIABIgBAAIgGAAIgCAAIgFAAIgCAAIgDAFIAAABQgBADgCACIAAAAIgDAEIAAACQAAAAAAABQgBAAAAABQgBAAAAAAQgBABAAAAIgBABQgIALgPAGIAAABIgBAAIgEABIgBAAIgMACgAEIlpIAAABIABABIgBgDgAEBmOIAAABIAAABIADAHIgEgRIABAIgAD4mrIACACIgCgDg");
    this.shape_1.setTransform(0.3, -29.225);

    this.instance_1 = new lib.B_1_1();
    this.instance_1.setTransform(69.55, 0.3, 0.3655, 0.3655, 0, 0, 0, 269.3, 83);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape_1}]}, 1).to({state: [{t: this.instance_1}]}, 2).wait(1));

    // Layer_3
    this.instance_2 = new lib.B_1_1();
    this.instance_2.setTransform(69.55, 0.3, 0.3655, 0.3655, 0, 0, 0, 269.3, 83);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 3).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-50.1, -91.9, 97.6, 127);


  (lib.Symbol13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Lightcopy4();
    this.instance.setTransform(39.45, 50.3, 0.1078, 0.1078, 0, 0, 0, 7.4, 584.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-0.4, 0, 80.5, 103.6);


  (lib.Symbol12_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Lightcopy3();
    this.instance.setTransform(39.45, 50.3, 0.1078, 0.1078, 0, 0, 0, 7.4, 584.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-0.4, 0, 80.5, 103.6);


  (lib.Symbol6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Light();
    this.instance.setTransform(39.45, 50.3, 0.1078, 0.1078, 0, 0, 0, 7.4, 584.1);
    // new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib.Symbol3(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(22));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-39.8, -50.1, 80.6, 103.6);


  (lib.Symbol4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Lightcopy2();
    this.instance.setTransform(39.45, 50.3, 0.1078, 0.1078, 0, 0, 0, 7.4, 584.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-0.4, 0, 80.5, 103.6);


  (lib.Symbol3_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Lightcopy();
    this.instance.setTransform(39.45, 50.3, 0.1078, 0.1078, 0, 0, 0, 7.4, 584.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-0.4, 0, 80.5, 103.6);


  (lib.Symbol2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Light_1();
    this.instance.setTransform(39.45, 50.3, 0.1078, 0.1078, 0, 0, 0, 7.4, 584.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-0.4, 0, 80.5, 103.6);


  (lib.OFF_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance_9 = new lib.Locked_1("synched", 0);
    this.instance_9.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance_9._off = true;

    this.instance_10 = new lib.Symbol1_2();
    this.instance_10.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_9}]}, 11).to({state: [{t: this.instance_9}]}, 3).to({state: [{t: this.instance_9}]}, 1).to({state: [{t: this.instance_9}]}, 1).to({state: [{t: this.instance_9}]}, 1).to({state: [{t: this.instance_9}]}, 1).to({state: [{t: this.instance_10}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_3
    this.instance_11 = new lib.Less_1_off("synched", 0);
    this.instance_11.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_11).to({_off: true}, 19).wait(1));

    // C
    this.instance_12 = new lib.Tween1_1("synched", 0);
    this.instance_12.setTransform(78.7, 76.2);
    this.instance_12._off = true;

    this.instance_13 = new lib.Tween2_1("synched", 0);
    this.instance_13.setTransform(78.7, -24.7);
    this.instance_13._off = true;

    this.instance_14 = new lib.Tween3_1("synched", 0);
    this.instance_14.setTransform(78.7, -127.7);
    this.instance_14._off = true;

    this.instance_15 = new lib.Tween4_1("synched", 0);
    this.instance_15.setTransform(78.6, -71.2);
    this.instance_15._off = true;

    this.instance_16 = new lib.Tween5("synched", 0);
    this.instance_16.setTransform(78.7, -96);
    this.instance_16._off = true;

    this.instance_17 = new lib.Tween6_1("synched", 0);
    this.instance_17.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_12}]}, 2).to({state: [{t: this.instance_13}]}, 2).to({state: [{t: this.instance_14}]}, 2).to({state: [{t: this.instance_15}]}, 2).to({state: [{t: this.instance_16}]}, 2).to({state: [{t: this.instance_17}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(2).to({_off: false}, 0).to({
      _off: true,
      y: -24.7
    }, 2).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(2).to({_off: false}, 2).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.lf(["rgba(180,180,153,0.439)", "rgba(50,50,32,0.439)", "rgba(6,6,3,0.769)"], [0, 0.306, 1], 2.4, 31.5, 6.5, -67.9).s().p("AuvDWQmGhZAAh8QAAh9GGhYQGHhZIoAAQIoAAGHBZQGHBYAAB9QAAB8mHBZQmHBXooAAQooAAmHhXg");
    this.shape_1.setTransform(75.225, 146.55);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(20));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 418.20000000000005);


  (lib.N3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.B_1();
    this.instance.setTransform(128.7, 10.1, 1, 1, 0, 0, 0, 268.7, 82.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.N3, new cjs.Rectangle(-192.4, -70.6, 266.8, 176), null);


  (lib.Symbol5copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_10_copy_copy_copy_copy_copy
    this.instance = new lib._6copy2();
    this.instance.setTransform(-396.45, -283.85, 1, 1, 0, 0, 0, 58.2, 0);
    new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib._6copy2(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(32));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-496.7, -361.7, 81.89999999999998, 106.79999999999998);


  (lib.Symbol5copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_10_copy_copy_copy_copy_copy
    this.instance = new lib._6copy();
    this.instance.setTransform(-396.45, -283.85, 1, 1, 0, 0, 0, 58.2, 0);
    new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib._6copy(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(32));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-496.7, -360.7, 81.89999999999998, 105.79999999999998);


  (lib.Symbol5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_10_copy_copy_copy_copy_copy
    this.instance = new lib._6();
    this.instance.setTransform(-396.45, -283.85, 1, 1, 0, 0, 0, 58.2, 0);
    new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib._6(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(32));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-496.7, -360.9, 81.89999999999998, 105.99999999999997);


  (lib.N4copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3_copy_copy
    this.instance = new lib.Symbol5copy2();
    this.instance.setTransform(303.2, -235.75, 1.2, 1.2, 0, 0, 0, -396.4, -283.9);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.N4copy, new cjs.Rectangle(182.8, -265.3, 98.19999999999999, 64.4), null);


  (lib.N4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3_copy_copy
    this.instance = new lib.Symbol5copy();
    this.instance.setTransform(303.2, -235.75, 1.2, 1.2, 0, 0, 0, -396.4, -283.9);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.N4, new cjs.Rectangle(182.8, -265.3, 98.19999999999999, 64.4), null);


  (lib._6_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(0,0,0,0.239)").s().p("AAbISIgBAAIgBAAQgUgEgSgGIgBAAQgNgCgLgGIgBAAQgSgFgQgHIAAAAIgigLIgCAAIgBAAQgDABgCgCIgBAAIgDAAIgCAAQgDABgCgCIgBAAIgDAAIgCAAQgEABAAgCIgCAAIgCAAIgCAAQgEAAgBgCIgBAAIAAAAIgCABQgEAAgBgCIgBAAIgCAAIgBABQgEAAgBgCIgBAAIgBAAQgEABgCgCIgBAAIgBAAQgEABgCgCIgBAAIgUgEIgBAAQghgGgdgIIgBgBQgigHgagPIgBAAQg3gggJhMIAAgBIgBgGIAAgBIAAgBIgBAAIgBgIIACAAIAAgBIAAgBQAOgPAWgJIABAAIAAgBIgEgKIgBAAIgLgcIAAgBQgFgNgEgPIgBAAIgIgeIAAgBQgEgMgCgOIAAgBQgBgDAAgEIAAAAIgBgHIAAgBIAAgBIgBAAIAAgGIAAgBIAAgGIAAAAIgBgGIAAgBIAAgOIAAgBIAAgFIABAAIAAgCIAAgBIAAgBIAAgFIABAAIAAgBIAAgEQACgBgBgEIAAgBIAAgBIAAgIIABAAIAAAAIAAgCQAUgVAfgJIABAAIAAgCIAMgDIABAAIAAgBIAxgNIABAAQAIAAAIgDIABAAIApgIIABAAIAAgBIAFAAIACAAIABAAIAAgBIAFAAIABAAIABAAIAAgBIAGAAIABAAIABAAIABAAIAAgBIAGAAIAAAAIADAAIABAAIAAgBIAFAAIABAAIABAAIABAAIAAgBIAGAAIABAAIADAAIACAAIAAgBIAFAAIABAAIAEAAIABAAIAAgCIAFAAIACAAIADAAIABAAIAAgBIAGAAIACAAIAAAAIABAAIAAgBIAGAAIABAAIAFAAIABAAIADAAIABAAIAAgBIAFAAIACAAIAEAAIABAAIADAAIABAAIAAgBIAFAAIABAAIAGAAIACgCIABAAIAAgBQANgLARgGIABAAIAAgBQAOgEAQgCIABgBIgBAAIALAAIABAAIACAAIABAAIAFAAQhigDhHhHQhKhKAAhoQAAhoBKhKQApgpAzgSQAngOAsAAQArAAAoAOQAzASAqApQBIBKAABoQAABohIBKQhIBHhjADIAAAAQASACANAGIABAAIAEACIAAABIAFACIAAABQAOAGALAIIAHAAIAAABIABAAIABAAIAGAAIABAAIAHABIAAAAIAEAAIABAAIAGABIABAAIAFAAIABAAIAGABIAAAAIAFAAIABAAIAHABIAAABIABAAIABAAIAHAAIAAABIACAAIABAAIAIAAIAAABIABAAIABAAIAHAAIAAABIABAAIABAAIAHABIABAAIAHABIAAAAIABAAIAGABIACAAIAFAAIAAABIACAAIAGAAIAAABQBgAMBNAfQAVAIAEAWIAAAFIAAABIAAAFIAAACIAAAAQABAEgCACIAAADIAAABIAAACQABADgDACIAAADIAAABIABABQABAEgCACIAAACIAAABIAAABQAAAEgBACIAAAAIAAACIgBAFIAAABQgBAKgDAJIAAABQgEAcgJAYIAAABIAAACQgEAOgFAMIAAABIgFAKIAAABQgGANgHAMIAAABIAAABIgBAAIABABIAAABQAgAIASAVIABABIAAAFIAAABIgBAAIAAABQgGAcgKAXQgiBMhbARIAAABQgWAGgXADIAAABIgHAAIgBABQgCABgFgBIAAABIgGAAIgCAAIgCAAIAAABIgGABIAAAAIgFABIgBAAIgCAAIAAAAIgGABIgBAAIgGAAIgBAAIAAAAIgGABIAAABIgGAAIgBAAIgEAAIAAABIgGAAIgBAAIgFAAIgBABIgGAAIgBAAIgGAAIgBAAIgGABIgCAAIgEAAIgCAAIgGAAIgBAAIAAABIgHAAIgBAAIgGAAIgBAAIgFAAIgBAAIgDAFIAAABQAAAAAAABQAAAAAAABQgBAAAAABQgBAAAAABIAAAAIgDAEIAAABQAAAAAAABQAAAAgBAAQAAABgBAAQAAAAgBAAIAAABQgHAKgMAFIAAABIgBAAIgEABIgBAAIgKABgADgkxIAAABIAAABIAAgDgADZlQIAAABIAAABIADAHIgEgQIABAHgADSlpIACACIgCgCg");
    this.shape_1.setTransform(0.15, -24.675);

    this.instance_2 = new lib.OFF_1();
    this.instance_2.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape_1}]}, 1).to({state: []}, 1).to({state: [{t: this.instance_2}]}, 1).wait(1));

    // Layer_1
    this.instance_3 = new lib.OFF_1();
    this.instance_3.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-42, -77.7, 82.1, 106.7);


  (lib.Symbol16 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AAgJzQAAgBAAAAQAAAAgBAAQAAAAAAAAQAAAAgBABIgBAAQgYgFgVgHIgBAAQgPgDgNgGIgCAAQgUgHgTgIIgBAAIgngNIgEAAIgBABQgDAAgDgCIgBAAIgEAAIgBAAQgFABgCgCIAAAAIgFAAIgBAAQgFABgBgDIgCAAIgCAAIgCAAQgFABgCgCIgBAAIAAAAIgCAAQgFABgCgCIgBAAIgBAAIgBAAQgFABgCgDIgBAAIgBAAQgFACgCgDIgBAAIgBAAQgFABgCgCIgBgBIgZgEIgBAAQgmgHgjgKIgBgBQgogIgfgSIgBAAQhBglgLhbIgBAAIAAgIIAAgBIAAgCIgBAAIgBgIIACAAIAAgCIAAgBQARgSAZgKIACAAIgBgBIgDgMIgCAAIgNgiIAAgBQgGgPgEgSIgCAAIgJgjIAAgCQgFgNgCgSIAAgBQgCgDABgFIAAAAQgCgEAAgEIAAgBIAAgBIgBAAIAAgIIAAgBIAAgHIgBAAIAAgHIAAgBIAAgQIAAgCIAAgGIABAAIAAgCIAAgBIAAgBIAAgGIABAAIAAgCIAAgEQACgBAAgFIAAgBIAAgBIAAgKIABAAIAAAAIAAgCQAXgaAkgKIACAAIAAgCIAOgEIABAAIAAgBIA7gPIABAAQAKgBAIgDIABAAIAxgJIACgBIAAgBIAGAAIACAAIABAAIAAAAIAFAAIADAAIABAAIAAgCIAGAAIACAAIABAAIABAAIAAgBIAGAAIACAAIACAAIABAAIAAgCIAHAAIABAAIABAAIABAAIAAgBIAHAAIABAAIAFAAIABAAIAAgBIAGAAIACAAIADAAIABAAIAAgCIAHAAIACAAIAFAAIABAAIAAgBIAGAAIACAAIACAAIAAAAIAAgBIAHAAIABAAIAGAAIACAAIACAAIABAAIAAgCIAHAAIACAAIAGAAIABAAIACAAIACAAIAAgBIAGAAIABAAIAHAAIADgCIAAAAIAAgCQAQgMAUgIIABAAIAAgBQARgFATgCIABgBIgBAAIANAAIABAAIACAAIABAAIAHAAQh1gDhUhVQhWhXAAh7QAAh8BWhWQAxgyA8gVQAugQA1AAQAzAAAvAQQA9AVAxAyQBVBWAAB8QAAB7hVBXQhUBVh3ADIAAAAQAWACAPAIIACAAIAEACIAAABIAHADIAAAAQAQAIANAJIAIABIAAAAIABAAIACAAIAGAAIABAAIAJABIAAABIAFAAIABAAIAHAAIABABIAGAAIABAAIAIABIAAAAIAGAAIAAAAIAIABIAAABIACAAIAAAAIAJAAIAAABIADAAIACAAIAIABIAAAAIACAAIACAAIAHABIAAABIACAAIABAAIAHABIACAAIAIABIAAABIABAAIAIAAIABAAIAIABIAAAAIAAAAIAJABIAAABQBxAOBbAkQAZAKAFAaIAAAGIAAABIAAAGIAAACIAAABQAAAFgCABIAAAEIAAACIAAABQABAFgCABIAAAEIAAACIAAABQABAFgDABIAAADIAAABIAAACQACAEgDACIAAABIAAACIgBAGIAAABQAAAMgDAKIAAABQgGAigLAcIAAACIAAABQgEARgHAOIAAABIgFAMIAAABQgHAQgJAOIAAACIAAAAIgBABIABABIAAABQAmAKAWAZIABAAIAAAHIAAABIgBAAIAAABQgIAhgMAbQgoBahrAUIAAACQgaAHgcADIAAABIgIAAIgBABQgCACgGgBIAAABIgHAAIgCAAIgDAAIAAABIgGAAIAAABIgHABIgCAAIgBAAIgBAAIgHABIgBAAIgHAAIgBAAIAAAAIgGABIAAABIgJABIAAAAIgFAAIAAAAIgHABIgCAAIgGAAIAAABIgHAAIgCAAIgHAAIAAAAIgIABIgCAAIgGAAIgBAAIgJAAIAAAAIAAABIgJABIgBAAIgGAAIgCAAIgGAAIgBAAIgDAFIAAABQgBADgCACIAAAAIgDAEIAAACQgBACgCABIgBABQgIALgPAGIAAABIgBAAIgEABIgBAAIgMACgAEIlpIAAABIABABIgBgDgAEBmOIAAABIAAABIADAGIgEgQIABAIgAD4mrIACACIgCgDg");
    this.shape.setTransform(0.2, -29.275);

    this.instance = new lib.N3();
    this.instance.setTransform(69.35, 0.05, 0.3654, 0.3654, 0, 0, 0, 128.8, 10.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_3
    this.instance_1 = new lib.N3();
    this.instance_1.setTransform(69.35, 0.05, 0.3654, 0.3654, 0, 0, 0, 128.8, 10.1);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-48, -92, 97.5, 126.9);


  (lib.Symbol5_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_10_copy_copy_copy_copy_copy
    this.instance_1 = new lib._6_1();
    this.instance_1.setTransform(-396.45, -283.85, 1, 1, 0, 0, 0, 58.2, 0);
    new cjs.ButtonHelper(this.instance_1, 0, 1, 2, false, new lib._6_1(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(32));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-496.7, -361.5, 82.09999999999997, 106.6);


  (lib.N4_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3_copy_copy
    this.instance_1 = new lib.Symbol5();
    this.instance_1.setTransform(303.2, -235.75, 1.2, 1.2, 0, 0, 0, -396.4, -283.9);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.N4_1, new cjs.Rectangle(182.8, -265.3, 98.19999999999999, 64.4), null);


  (lib.Aii_Desert = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_179 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(179).call(this.frame_179).wait(1));

    // Layer_3
    this.instance = new lib.Symbol9();
    this.instance.setTransform(-315.6, 517.5, 0.3322, 0.3322, 0, 0, 0, 96.5, 591.3);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(118).to({_off: false}, 0).wait(62));

    // Layer_17
    this.instance_1 = new lib.Symbol13();
    this.instance_1.setTransform(-567.65, -225, 1.228, 1.228, 0, 0, 0, -0.1, -0.1);
    this.instance_1._off = true;
    this.instance_1.name = '6';
    this.instance_1.visible = false;
    new cjs.ButtonHelper(this.instance_1, 0, 1, 2, false, new lib.Symbol13(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(179).to({_off: false}, 0).wait(1));

    // Layer_16
    this.instance_2 = new lib.Symbol12_1();
    this.instance_2.setTransform(-160.55, -165.65, 1.243, 1.243, 0, 0, 0, -0.3, -0.2);
    this.instance_2._off = true;
    this.instance_2.name = '5';
    this.instance_2.visible = false;
    new cjs.ButtonHelper(this.instance_2, 0, 1, 2, false, new lib.Symbol12_1(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(174).to({_off: false}, 0).wait(6));

    // Layer_15
    this.instance_3 = new lib.Symbol4();
    this.instance_3.setTransform(162.5, -293.9, 1.2451, 1.2451, 0, 0, 0, -1.1, -0.8);
    this.instance_3._off = true;
    this.instance_3.name = '4';
    this.instance_3.visible = false;
    new cjs.ButtonHelper(this.instance_3, 0, 1, 2, false, new lib.Symbol4(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(167).to({_off: false}, 0).wait(13));

    // Layer_14
    this.instance_4 = new lib.Symbol3_1();
    this.instance_4.setTransform(217, -82.75, 1.197, 1.197);
    this.instance_4._off = true;
    this.instance_4.name = '3';
    this.instance_4.visible = false;
    new cjs.ButtonHelper(this.instance_4, 0, 1, 2, false, new lib.Symbol3_1(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(158).to({_off: false}, 0).wait(22));

    // Layer_8
    this.instance_5 = new lib.Symbol2();
    this.instance_5.setTransform(-166.75, 91.9, 1.1942, 1.1942, 0, 0, 0, -0.1, 0);
    this.instance_5._off = true;
    this.instance_5.name = '2';
    this.instance_5.visible = false;
    new cjs.ButtonHelper(this.instance_5, 0, 1, 2, false, new lib.Symbol2(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(150).to({_off: false}, 0).wait(30));

    // Layer_4
    this.instance_6 = new lib.Symbol6();
    this.instance_6.setTransform(-460, 303, 1.2, 1.2);
    this.instance_6._off = true;
    this.instance_6.name = '1';
    this.instance_6.visible = false;
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(141).to({_off: false}, 0).wait(39));

    // Layer_5
    this.instance_7 = new lib.N4copy();
    this.instance_7.setTransform(-448.9, -131.35, 1, 1, 0, 0, 0, 303.1, -235.8);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(102).to({_off: false}, 0).wait(39));

    // Layer_6
    this.instance_8 = new lib.N4();
    this.instance_8.setTransform(-40.7, -70.05, 1, 1, 0, 0, 0, 303.1, -235.8);
    this.instance_8._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(83).to({_off: false}, 0).wait(39));

    // Layer_7
    this.instance_9 = new lib.N4_1();
    this.instance_9.setTransform(283.45, -198.2, 1, 1, 0, 0, 0, 303.1, -235.8);
    this.instance_9._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(63).to({_off: false}, 0).wait(39));

    // Layer_3_copy
    this.instance_10 = new lib.Symbol16();
    this.instance_10.setTransform(333.3, 7.85, 1, 1, 0, 0, 0, 69.4, 0);
    this.instance_10._off = true;
    new cjs.ButtonHelper(this.instance_10, 0, 1, 2, false, new lib.Symbol16(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(44).to({_off: false}, 0).wait(39));

    // Layer_9
    this.instance_11 = new lib.Symbol15_1();
    this.instance_11.setTransform(-50.45, 183.5, 1, 1, 0, 0, 0, 69.4, 0);
    this.instance_11._off = true;
    new cjs.ButtonHelper(this.instance_11, 0, 1, 2, false, new lib.Symbol15_1(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(25).to({_off: false}, 0).wait(39));

    // Layer_10
    this.instance_12 = new lib.Symbol5_1();
    this.instance_12.setTransform(-343.45, 395.35, 1.2, 1.2, 0, 0, 0, -396.4, -283.9);
    this.instance_12._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(5).to({_off: false}, 0).wait(39));

    // Layer_11
    this.instance_13 = new lib.wood("synched", 0);
    this.instance_13.setTransform(-10.85, 26.35, 7.3773, 6.9843, 0, 177.7259, 0, 20, 0.6);

    this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(180));

    // Layer_12
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EhwhBYnMAAAixNMDhDAAAMAAACxNgEhoBBKUMDJ4AAAMAAAialMjJ4AAAg");
    this.shape.setTransform(4.025, 23.0375);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(180));

    // Layer_13
    this.instance_14 = new lib.Camel("synched", 0);
    this.instance_14.setTransform(281.2, 332.6, 1.0267, 1.0267, 0, 0, 0, 0.3, 0.1);

    this.instance_15 = new lib.Camel("synched", 0);
    this.instance_15.setTransform(-425.5, 1.85, 0.638, 0.638, 0, 0, 180, -0.1, 0.1);

    this.instance_16 = new lib.Symbol11();
    this.instance_16.setTransform(-461.7, -47.35, 0.5827, 0.5827, 0, 0, 0, 0.1, 0.1);

    this.instance_17 = new lib.Symbol10();
    this.instance_17.setTransform(89.25, -51.55, 0.8364, 0.8364, 0, 0, 0, 0.3, -0.1);

    this.instance_18 = new lib.Symbol7_1();
    this.instance_18.setTransform(450.4, 323.95, 0.6575, 0.6575, 0, 0, 0, 0, 0.1);

    this.instance_19 = new lib.CachedBmp_130();
    this.instance_19.setTransform(-602.4, -304.4, 0.5, 0.5);

    this.instance_20 = new lib.Symbol8();
    this.instance_20.setTransform(-457.85, 236.75, 0.8365, 0.8365, 0, 0, 0, 0.1, 0.1);

    this.instance_21 = new lib.CachedBmp_129();
    this.instance_21.setTransform(-512.25, -322, 0.5, 0.5);

    this.instance_22 = new lib.CachedBmp_66();
    this.instance_22.setTransform(65.05, 69.95, 0.5, 0.5);

    this.instance_23 = new lib.CachedBmp_128();
    this.instance_23.setTransform(-567.15, -319.35, 0.5, 0.5);

    this.instance_24 = new lib.Symbol1("synched", 0);
    this.instance_24.setTransform(4.6, 104.3, 0.8524, 0.8602);

    this.instance_25 = new lib.CachedBmp_64();
    this.instance_25.setTransform(-719.3, -318.65, 0.5, 0.5);

    this.instance_26 = new lib.CachedBmp_63();
    this.instance_26.setTransform(-603.7, -250.9, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_26}, {t: this.instance_25}, {t: this.instance_24}, {t: this.instance_23}, {t: this.instance_22}, {t: this.instance_21}, {t: this.instance_20}, {t: this.instance_19}, {t: this.instance_18}, {t: this.instance_17}, {t: this.instance_16}, {t: this.instance_15}, {t: this.instance_14}]}).wait(180));

    // Layer_2
    this.instance_27 = new lib.Symbol9_1();
    this.instance_27.setTransform(-15.65, -231.05, 0.5637, 0.4263, 0, 0, 0, -417.1, -86.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(180));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-719.3, -544.1, 1443.5, 1134.3000000000002);


// stage content:
  (lib.Desert = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.All_Space();
    this.instance.setTransform(675.3, 585.85, 0.7944, 0.7654, 0, 0, 0, -57.5, 20.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_1
    this.instance_1 = new lib.Aii_Desert();
    this.instance_1.setTransform(721.7, 556.5, 1, 1, 0, 0, 0, 4.1, 12.6);
    this.instance_1.name = 'stages';
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    // stageBackground
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("rgba(0,0,0,0)").ss(1, 1, 1, 3, true).p("EhyDhaEMDkHAAAMAAAC0JMjkHAAAg");
    this.shape.setTransform(720, 566.5);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EhyDBaFMAAAi0JMDkHAAAMAAAC0Jg");
    this.shape_1.setTransform(720, 566.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new lib.AnMovieClip();
  p.nominalBounds = new cjs.Rectangle(709, 555.5, 742, 588.5);
// library properties:
  lib.properties = {
    id: '8463AC777CA0C54192671A88D06A6704',
    width: 1440,
    height: 1133,
    fps: 24,
    color: "#FFFFFF",
    opacity: 1.00,
    manifest: [
      {src: "assets/images/deserts/CachedBmp_123.png", id: "CachedBmp_123"},
      {src: "assets/images/shared/1.png", id: "CachedBmp_115"},
      {src: "assets/images/deserts/CachedBmp_130.png", id: "CachedBmp_130"},
      {src: "assets/images/deserts/CachedBmp_129.png", id: "CachedBmp_129"},
      {src: "assets/images/deserts/CachedBmp_64.png", id: "CachedBmp_64"},
      {src: "assets/images/deserts/CachedBmp_63.png", id: "CachedBmp_63"},
      {src: "assets/images/deserts/Desert _Stage_atlas_1.png", id: "Desert _Stage_atlas_1"},
      {src: "assets/images/shared/2.png", id: "Desert _Stage_atlas_2"},
      {src: "assets/images/shared/2.png", id: "Desert _Stage_atlas_3"},
      {src: "assets/images/shared/2.png", id: "Desert _Stage_atlas_4"},
      {src: "assets/images/deserts/Desert _Stage_atlas_5.png", id: "Desert _Stage_atlas_5"},
      {src: "assets/images/deserts/Desert _Stage_atlas_5.png", id: "Desert _Stage_atlas_6"},
      {src: "assets/images/deserts/Desert _Stage_atlas_5.png", id: "Desert _Stage_atlas_7"},
      {src: "assets/images/deserts/Desert _Stage_atlas_8.png", id: "Desert _Stage_atlas_8"},
      {src: "assets/images/deserts/Desert _Stage_atlas_8.png", id: "Desert _Stage_atlas_9"},
      {src: "assets/images/deserts/Desert _Stage_atlas_8.png", id: "Desert _Stage_atlas_10"},
      {src: "assets/images/shared/3.png", id: "Desert _Stage_atlas_11"},
      {src: "assets/images/shared/3.png", id: "Desert _Stage_atlas_12"},
      {src: "assets/images/shared/3.png", id: "Desert _Stage_atlas_13"},
      {src: "assets/images/deserts/Desert _Stage_atlas_14.png", id: "Desert _Stage_atlas_14"},
      {src: "assets/images/deserts/Desert _Stage_atlas_15.png", id: "Desert _Stage_atlas_15"}
    ],
    preloads: []
  };


// bootstrap callback support:

  (lib.Stage = function (canvas) {
    createjs.Stage.call(this, canvas);
  }).prototype = p = new createjs.Stage();

  p.setAutoPlay = function (autoPlay) {
    this.tickEnabled = autoPlay;
  }
  p.play = function () {
    this.tickEnabled = true;
    this.getChildAt(0).gotoAndPlay(this.getTimelinePosition())
  }
  p.stop = function (ms) {
    if (ms) this.seek(ms);
    this.tickEnabled = false;
  }
  p.seek = function (ms) {
    this.tickEnabled = true;
    this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000);
  }
  p.getDuration = function () {
    return this.getChildAt(0).totalFrames / lib.properties.fps * 1000;
  }

  p.getTimelinePosition = function () {
    return this.getChildAt(0).currentFrame / lib.properties.fps * 1000;
  }

  an.bootcompsLoaded = an.bootcompsLoaded || [];
  if (!an.bootstrapListeners) {
    an.bootstrapListeners = [];
  }

  an.bootstrapCallback = function (fnCallback) {
    an.bootstrapListeners.push(fnCallback);
    if (an.bootcompsLoaded.length > 0) {
      for (var i = 0; i < an.bootcompsLoaded.length; ++i) {
        fnCallback(an.bootcompsLoaded[i]);
      }
    }
  };

  an.compositions = an.compositions || {};
  an.compositions['8463AC777CA0C54192671A88D06A6704'] = {
    getStage: function () {
      return exportRoot.stage;
    },
    getLibrary: function () {
      return lib;
    },
    getSpriteSheet: function () {
      return ss;
    },
    getImages: function () {
      return img;
    }
  };

  an.compositionLoaded = function (id) {
    an.bootcompsLoaded.push(id);
    for (var j = 0; j < an.bootstrapListeners.length; j++) {
      an.bootstrapListeners[j](id);
    }
  }

  an.getComposition = function (id) {
    return an.compositions[id];
  }


  an.makeResponsive = function (isResp, respDim, isScale, scaleType, domContainers) {
    var lastW, lastH, lastS = 1;
    window.addEventListener('resize', resizeCanvas);
    resizeCanvas();

    function resizeCanvas() {
      var w = lib.properties.width, h = lib.properties.height;
      var iw = window.innerWidth, ih = window.innerHeight;
      var pRatio = window.devicePixelRatio || 1, xRatio = iw / w, yRatio = ih / h, sRatio = 1;
      if (isResp) {
        if ((respDim == 'width' && lastW == iw) || (respDim == 'height' && lastH == ih)) {
          sRatio = lastS;
        } else if (!isScale) {
          if (iw < w || ih < h)
            sRatio = Math.min(xRatio, yRatio);
        } else if (scaleType == 1) {
          sRatio = Math.min(xRatio, yRatio);
        } else if (scaleType == 2) {
          sRatio = Math.max(xRatio, yRatio);
        }
      }
      domContainers[0].width = w * pRatio * sRatio;
      domContainers[0].height = h * pRatio * sRatio;
      domContainers.forEach(function (container) {
        container.style.width = w * sRatio + 'px';
        container.style.height = h * sRatio + 'px';
      });
      stage.scaleX = pRatio * sRatio;
      stage.scaleY = pRatio * sRatio;
      lastW = iw;
      lastH = ih;
      lastS = sRatio;
      stage.tickOnUpdate = false;
      stage.update();
      stage.tickOnUpdate = true;
    }
  }
  an.handleSoundStreamOnTick = function (event) {
    if (!event.paused) {
      var stageChild = stage.getChildAt(0);
      if (!stageChild.paused) {
        stageChild.syncStreamSounds();
      }
    }
  }


})(createjs = createjs || {}, AdobeAn = AdobeAn || {});
var createjs, AdobeAn, instance;
var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;

function initDeserts() {
  return new Promise((resolve, reject) => {
    canvas = document.getElementById("canvas");
    anim_container = document.getElementById("animation_container");
    dom_overlay_container = document.getElementById("dom_overlay_container");
    var comp = AdobeAn.getComposition("8463AC777CA0C54192671A88D06A6704");
    var lib = comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function (evt) {
      handleFileLoadDeserts(evt, comp)
    });
    loader.addEventListener("complete", function (evt) {
      handleCompleteDeserts(evt, comp).then(data => {
        resolve(true)
      });
    });
    var lib = comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  })
}

function handleFileLoadDeserts(evt, comp) {
  var images = comp.getImages();
  if (evt && (evt.item.type == "image")) {
    images[evt.item.id] = evt.result;
  }
}

function handleCompleteDeserts(evt, comp) {
  return new Promise((resolve, reject) => {
    var lib = comp.getLibrary();
    var ss = comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for (i = 0; i < ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet({
        "images": [queue.getResult(ssMetadata[i].name)],
        "frames": ssMetadata[i].frames
      })
    }
    var preloaderDiv = document.getElementById("_preload_div_");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.Desert();
    stage = new lib.Stage(canvas);
    stage.enableMouseOver();
    createjs.Ticker.removeAllEventListeners(); // Note the function name
    //Registers the "tick" event listener.
    fnStartAnimation = function () {
      stage.addChild(exportRoot);
      createjs.Ticker.framerate = lib.properties.fps;
      createjs.Ticker.addEventListener("tick", stage);
    }
    //Code to support hidpi screens and responsive scaling.
    AdobeAn.makeResponsive(true, 'both', false, 1, [canvas, preloaderDiv, anim_container, dom_overlay_container]);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
    desertClickEvents();
    desertHoverEvents();
    resolve(true);
  })
}

function desertClickEvents() {
  //stage 1
  exportRoot.instance_1.instance_6.on("click", function (evt) {
    playAudioOnClick();
    instance = 1;
  });
  //stage 2
  exportRoot.instance_1.instance_5.on("click", function (evt) {
    playAudioOnClick();
    instance = 2;
  });
  //stage 3
  exportRoot.instance_1.instance_4.on("click", function (evt) {
    instance = 3;
  });
  //stage 4
  exportRoot.instance_1.instance_3.on("click", function (evt) {
    playAudioOnClick();
    instance = 4;
  });
  //stage 5
  exportRoot.instance_1.instance_2.on("click", function (evt) {
    playAudioOnClick();
    instance = 5;
  });
  //stage 6
  exportRoot.instance_1.instance_1.on("click", function (evt) {
    playAudioOnClick();
    instance = 6;
  });
  //back
  exportRoot.instance.instance.on("click", function (evt) {
    playAudioOnClick();
    instance = 7;
  });
}

function desertHoverEvents() {
  //stage 1
  exportRoot.instance_1.instance_6.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 2
  exportRoot.instance_1.instance_5.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 3
  exportRoot.instance_1.instance_4.on("mouseover", function (evt) {
  });
  //stage 4
  exportRoot.instance_1.instance_3.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 5
  exportRoot.instance_1.instance_2.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 6
  exportRoot.instance_1.instance_1.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //back
  exportRoot.instance.instance.on("mouseover", function (evt) {
    playAudioOnHover();
  });
}
