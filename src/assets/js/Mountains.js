(function (cjs, an) {

  var p; // shortcut to reference prototypes
  var lib = {};
  var ss = {};
  var img = {};
  lib.ssMetadata = [
    {name: "Maotens_atlas_1", frames: [[0, 0, 967, 1550], [969, 0, 967, 1550]]},
    {name: "Maotens_atlas_2", frames: [[0, 0, 967, 1550], [969, 0, 967, 1550]]},
    {name: "Maotens_atlas_3", frames: [[0, 0, 967, 1550], [969, 0, 967, 1550]]},
    {
      name: "Maotens_atlas_4",
      frames: [[0, 0, 1128, 998], [1130, 0, 859, 899], [1130, 901, 859, 899], [0, 1000, 859, 899]]
    },
    {name: "Maotens_atlas_5", frames: [[0, 0, 859, 899], [0, 901, 859, 899], [861, 0, 859, 899], [861, 901, 859, 899]]},
    {name: "Maotens_atlas_6", frames: [[0, 0, 859, 899], [0, 901, 859, 899], [861, 0, 859, 899], [861, 901, 859, 899]]},
    {
      name: "Maotens_atlas_7",
      frames: [[0, 0, 859, 899], [0, 901, 663, 640], [665, 901, 663, 640], [1330, 0, 663, 640], [1330, 642, 663, 640], [1330, 1284, 663, 640]]
    },
    {
      name: "Maotens_atlas_8",
      frames: [[1556, 774, 323, 327], [1432, 1385, 242, 243], [0, 1788, 243, 243], [1187, 1158, 243, 243], [1556, 1103, 279, 280], [0, 642, 367, 754], [665, 0, 365, 786], [0, 0, 663, 640], [0, 1398, 394, 388], [369, 788, 394, 388], [396, 1178, 394, 388], [765, 788, 394, 388], [396, 1568, 394, 388], [1032, 0, 394, 388], [1032, 390, 393, 382], [1427, 390, 393, 382], [1428, 0, 393, 382], [792, 1178, 393, 382], [1161, 774, 393, 382], [792, 1562, 393, 382], [1432, 1158, 83, 77]]
    }
  ];


  (lib.AnMovieClip = function () {
    this.actionFrames = [];
    this.gotoAndPlay = function (positionOrLabel) {
      cjs.MovieClip.prototype.gotoAndPlay.call(this, positionOrLabel);
    }
    this.play = function () {
      cjs.MovieClip.prototype.play.call(this);
    }
    this.gotoAndStop = function (positionOrLabel) {
      cjs.MovieClip.prototype.gotoAndStop.call(this, positionOrLabel);
    }
    this.stop = function () {
      cjs.MovieClip.prototype.stop.call(this);
    }
  }).prototype = p = new cjs.MovieClip();
// symbols:


  (lib.CachedBmp_62 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_47 = function () {
    this.initialize(img.CachedBmp_47);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 3670, 2839);


  (lib.CachedBmp_61 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_45 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_60 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_59 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(4);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_42 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(5);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_41 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(6);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_58 = function () {
    this.initialize(ss["Maotens_atlas_4"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_39 = function () {
    this.initialize(ss["Maotens_atlas_1"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_38 = function () {
    this.initialize(ss["Maotens_atlas_1"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_37 = function () {
    this.initialize(ss["Maotens_atlas_2"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_36 = function () {
    this.initialize(ss["Maotens_atlas_2"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_35 = function () {
    this.initialize(ss["Maotens_atlas_3"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_34 = function () {
    this.initialize(ss["Maotens_atlas_3"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_33 = function () {
    this.initialize(ss["Maotens_atlas_4"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_32 = function () {
    this.initialize(ss["Maotens_atlas_4"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_31 = function () {
    this.initialize(ss["Maotens_atlas_4"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_30 = function () {
    this.initialize(ss["Maotens_atlas_5"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_29 = function () {
    this.initialize(ss["Maotens_atlas_5"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_28 = function () {
    this.initialize(ss["Maotens_atlas_5"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_27 = function () {
    this.initialize(ss["Maotens_atlas_5"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_26 = function () {
    this.initialize(ss["Maotens_atlas_6"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_25 = function () {
    this.initialize(ss["Maotens_atlas_6"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_24 = function () {
    this.initialize(ss["Maotens_atlas_6"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_23 = function () {
    this.initialize(ss["Maotens_atlas_6"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_22 = function () {
    this.initialize(ss["Maotens_atlas_7"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_21 = function () {
    this.initialize(ss["Maotens_atlas_7"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_20 = function () {
    this.initialize(ss["Maotens_atlas_7"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_19 = function () {
    this.initialize(ss["Maotens_atlas_7"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_18 = function () {
    this.initialize(ss["Maotens_atlas_7"]);
    this.gotoAndStop(4);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_17 = function () {
    this.initialize(ss["Maotens_atlas_7"]);
    this.gotoAndStop(5);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_16 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(7);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_15 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(8);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_14 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(9);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_13 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(10);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_12 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(11);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_11 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(12);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_10 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(13);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_9 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(14);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_8 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(15);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_7 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(16);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_6 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(17);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_5 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(18);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_4 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(19);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_57 = function () {
    this.initialize(img.CachedBmp_57);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 5365, 3799);


  (lib.CachedBmp_56 = function () {
    this.initialize(img.CachedBmp_56);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 5148, 1671);


  (lib.CachedBmp_1 = function () {
    this.initialize(img.CachedBmp_1);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 3132, 1355);


  (lib.Blend_0 = function () {
    this.initialize(ss["Maotens_atlas_8"]);
    this.gotoAndStop(20);
  }).prototype = p = new cjs.Sprite();

// helper functions:

  function mc_symbol_clone() {
    var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop, this.reversed));
    clone.gotoAndStop(this.currentFrame);
    clone.paused = this.paused;
    clone.framerate = this.framerate;
    return clone;
  }

  function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
    var prototype = cjs.extend(symbol, cjs.MovieClip);
    prototype.clone = mc_symbol_clone;
    prototype.nominalBounds = nominalBounds;
    prototype.frameBounds = frameBounds;
    return prototype;
  }


  (lib.Tween6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0004, -0.0235, 1.2751, 1.0272, 0, 0, 180);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-177.1, -59.6, 354.29999999999995, 119.2);


  (lib.Tween4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0128, -0.0036);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-138.9, -58, 277.9, 116.1);


  (lib.Tween3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0128, -0.0036);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-138.9, -58, 277.9, 116.1);


  (lib.Tween2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhhMbQg6gRgLgyIAAACQgWAPgYgOQgVgOgMgdIgCABQg3ARg1ggQg5giAVg7QgiAMgKgiQgJgcALgcQgeAdgfgUQgcgTgLgpQgMgqASgZQAWgeA4AOQADADAAACQACgMAIgJQgQgBgRgJQgSgKgHgOQACgEAAgIQAAgHABgEQgYAHgagKIAAADQAOAUgYAYQghAhg3gTQgygSgWgmQgZgqALg0QAMg4AvgPQgugBgcghIAAADQgGAhg3AJQg3AIALgwQgKADgPAAIAFAFQgLA/g3AOQgzANg9ggQg/ggggg5QgkhBAVhFIAAAAIgBABQgVgEgJgUQgJgVASgPQgcgJAAgYQABgVAUgUIgbgMQgFAFgVAQQAeASgQAbQgOAZgfAHIAAABQAMAjglASQggAPgggIQgkgJgbguQgbgvATgaIgDgDQgrAIgGgtQgHgsAjgVQAYgQAeAJQAeAJAFAbQACgVAUgNQASgMAZAAQAZAAARAMQATAOAAAXQARgRAZALIAAgCQgOgPAMgRQALgQAUgCQgqgJgEgfQgEgcAdgdQAxgvBFABQBLABAYA/QARgaAnAGQAmAHAQAbQAWAoggAYQgeAWgmgNQADATgPAQQgOAOgSADQAKAWgSAUIAAAQQATAEAMAKQAOALACARQAdgkBJABQBLABAWArQAQgSASgEIAAgDQhWgNAFhbQACgmATgkQAUglAdgVQAygiBKgEQBMgEA4AgIABgDQgBgRAOgLQANgKASAAQASABALAKQAMAMgEAUQAagEAQAMQgDgPAOgJQAMgIAVgBIABgBQgSgTANgSQANgSAXANQgQgXAbgbQAXgXAdgIQAegJAlAIQAjAHAaAUQAgAWANAtQANAsgMApIAEAAQAXACAQALQATAMABAWIAAgCQATgHAWAGQAVAGARAOQgpguA2g9QAyg5A5gCQg0gIgRguQgRgwAqglQAUgQAggFQgSgPAMgQQAMgQAXAIQgbgWAegJQAdgJAXAOQgggiApgjQAjgfAqgFQAzgEAoAjQArAngYAwIgCAAQAgAFAQAVQAOATgDAZQgEAYgTAQQgVASgfgCQAMAKAAANQAAAOgMAKQAWgFAPADQAVAEgGAVQAZgHAPAKQAOAJABATQADAvg0AOQAUAGAFAUQAFARgJAUQARgOAZgCQAYgCAUAKIACACQAJgRAHgGQgZgDgKgUQgJgRAGgVQAGgVASgLQAUgMAaAIIACAAQhWgdAhhWQANgjAdgeQAegeAfgMQBOgeBDAzQBEA0gWBRQAwgCABAnQABAlgsANQAqgFAnAfQASgoBHAGQgUgIAOgRQANgRATAAQgBgVANgKQALgIAUABQgmhFAvgbQApgXBKASQBKATAfAqQAkAxg2A0QAiAEAMAjQAMAigdAWQAdALAXAgQAaAjgHAdQgGAagnAXQgkAWgdAAQAdAfgbAwQgZAsgoAQIgVAPQAhAwgqAtQgqAtgzgaQAbAcgJAfQgHAagfARQgdAQgegFQgggGgMgfQgSAOgbAKIgEgDQAPAwgcAoQgeArg8gIIhPAaQgWAAgSgHQgXgJgFgSQgWA1hegMQhcgLgZgwIgDAAQgPAvguATQgdAMhAAGQAVBThNAjQggAPglgDQgngEgegWQghArgrgmQgrgmATgsIgCgBQgXALgWgJQAGAOgFAJQgGAIgQgCQAOAig1AWQgrATgcgGQAcAIANAgQAOAkgZAXQAcACgBAbQAAAcgeAGIgDAAQAAAKgHANQA/gFAZAwQAaAyhEAhQgaAOgeAAQgVAAgXgHgA3vk6QgEgOAIgJQAGgHAMgBQAZgDAGANQAFAMgLAPQgKAQgNAAIgCABQgOAAgIgXgAtKmMQgWgNADgZQADgZAdAGQAqAKgLAhQgFANgLAEQgFADgGAAQgIAAgJgGg");
    this.shape.setTransform(0.028, 0.0029);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-152.1, -80.2, 304.29999999999995, 160.4);


  (lib.Tween1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhhMbQg6gRgLgyIAAACQgWAPgYgOQgVgOgMgdIgCABQg3ARg1ggQg5giAVg7QgiAMgKgiQgJgcALgcQgeAdgfgUQgcgTgLgpQgMgqASgZQAWgeA4AOQADADAAACQACgMAIgJQgQgBgRgJQgSgKgHgOQACgEAAgIQAAgHABgEQgYAHgagKIAAADQAOAUgYAYQghAhg3gTQgygSgWgmQgZgqALg0QAMg4AvgPQgugBgcghIAAADQgGAhg3AJQg3AIALgwQgKADgPAAIAFAFQgLA/g3AOQgzANg9ggQg/ggggg5QgkhBAVhFIAAAAIgBABQgVgEgJgUQgJgVASgPQgcgJAAgYQABgVAUgUIgbgMQgFAFgVAQQAeASgQAbQgOAZgfAHIAAABQAMAjglASQggAPgggIQgkgJgbguQgbgvATgaIgDgDQgrAIgGgtQgHgsAjgVQAYgQAeAJQAeAJAFAbQACgVAUgNQASgMAZAAQAZAAARAMQATAOAAAXQARgRAZALIAAgCQgOgPAMgRQALgQAUgCQgqgJgEgfQgEgcAdgdQAxgvBFABQBLABAYA/QARgaAnAGQAmAHAQAbQAWAoggAYQgeAWgmgNQADATgPAQQgOAOgSADQAKAWgSAUIAAAQQATAEAMAKQAOALACARQAdgkBJABQBLABAWArQAQgSASgEIAAgDQhWgNAFhbQACgmATgkQAUglAdgVQAygiBKgEQBMgEA4AgIABgDQgBgRAOgLQANgKASAAQASABALAKQAMAMgEAUQAagEAQAMQgDgPAOgJQAMgIAVgBIABgBQgSgTANgSQANgSAXANQgQgXAbgbQAXgXAdgIQAegJAlAIQAjAHAaAUQAgAWANAtQANAsgMApIAEAAQAXACAQALQATAMABAWIAAgCQATgHAWAGQAVAGARAOQgpguA2g9QAyg5A5gCQg0gIgRguQgRgwAqglQAUgQAggFQgSgPAMgQQAMgQAXAIQgbgWAegJQAdgJAXAOQgggiApgjQAjgfAqgFQAzgEAoAjQArAngYAwIgCAAQAgAFAQAVQAOATgDAZQgEAYgTAQQgVASgfgCQAMAKAAANQAAAOgMAKQAWgFAPADQAVAEgGAVQAZgHAPAKQAOAJABATQADAvg0AOQAUAGAFAUQAFARgJAUQARgOAZgCQAYgCAUAKIACACQAJgRAHgGQgZgDgKgUQgJgRAGgVQAGgVASgLQAUgMAaAIIACAAQhWgdAhhWQANgjAdgeQAegeAfgMQBOgeBDAzQBEA0gWBRQAwgCABAnQABAlgsANQAqgFAnAfQASgoBHAGQgUgIAOgRQANgRATAAQgBgVANgKQALgIAUABQgmhFAvgbQApgXBKASQBKATAfAqQAkAxg2A0QAiAEAMAjQAMAigdAWQAdALAXAgQAaAjgHAdQgGAagnAXQgkAWgdAAQAdAfgbAwQgZAsgoAQIgVAPQAhAwgqAtQgqAtgzgaQAbAcgJAfQgHAagfARQgdAQgegFQgggGgMgfQgSAOgbAKIgEgDQAPAwgcAoQgeArg8gIIhPAaQgWAAgSgHQgXgJgFgSQgWA1hegMQhcgLgZgwIgDAAQgPAvguATQgdAMhAAGQAVBThNAjQggAPglgDQgngEgegWQghArgrgmQgrgmATgsIgCgBQgXALgWgJQAGAOgFAJQgGAIgQgCQAOAig1AWQgrATgcgGQAcAIANAgQAOAkgZAXQAcACgBAbQAAAcgeAGIgDAAQAAAKgHANQA/gFAZAwQAaAyhEAhQgaAOgeAAQgVAAgXgHgA3vk6QgEgOAIgJQAGgHAMgBQAZgDAGANQAFAMgLAPQgKAQgNAAIgCABQgOAAgIgXgAtKmMQgWgNADgZQADgZAdAGQAqAKgLAhQgFANgLAEQgFADgGAAQgIAAgJgGg");
    this.shape.setTransform(-0.022, 0.0029);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-152.1, -80.2, 304.2, 160.4);


  (lib.Symbol10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#6B0F0F").ss(0.5, 1, 1).p("ADxihQgEgCgFgCQhGghhVAEQgdACgcAGQiaAdgzAOQguALgUAQIgBABQgBABgBABQgOAMAAAOQAAACABADQAAABABAAIAAABQAAAAAAAAIAEAHQABACACACQABAAABABIAAABQABAAABABIAEADAE0hRQAAgDAAgDQAAgEAAgEQgCgJgFgJQgCgDgDgEIgigdQgCgBgCgBQgGgEgGgDADwgIIAAAAQADgBACgCQAHgEAGgEQAIgGAHgGIABAAQACgCACgDIAFgEQAAgBABgBQACgCACgBQABgCABgBQAHgIAEgIQAFgJACgIQAEAUgHAhQgCAOgEAOQgEAKgEAMQgFANAAAUQAAAMABAZAC6AOQgCgCgBgCIgBAAQgBgDgBgEQgCgOAJgGIABgBQADgFADgDQAFgEAHgBQACAAAJABQAIAAAGAGQABACADABIABABQABABAAABQAAABACACQABACAAAFQgCApgGAxQgBAMgBALQgEAcgFAXQgBAIgCAHQABgBAAgBQANgRABgBQABgBADgBQACAAACgBQABAAACAAQgBgGACgEQAEgIAPgRQACgCACgCQAJgLAGgEQABgBACgBQAAAAABAAIABAAQAGABATARQAWATgDAHQgPAUgKAGQgRALgRAEQgRAEgEgEQgBgBgDgHQAAAAgBgIQgDgIAAgFQANgCALgDQAOgDAHAAQAJAAAJAEQAJAEALgHQACgCADgDABsAaQABAAAAAAQABAAABAAQAAAAABAAQALAAALgBIAFgBQACAAACAAIABAAIAPgEIAXgEQACgBACgBQgEAaAMA6QAKAxgDAaQAAAEgBAEQAHgBAFgDQADgDADgDAgGADIAAAAQADABADABIAQAFIAAAAQABAAABAAQABABABAAQAHADAIACQAMADAMADIABAAQABAAACAAIAAAAQAWAEAWAAABmC4IAFgEQADgCADgCQAJgHAJgCQACgBACAAQAIAAAPAFQAEABAFACQAWAHALAAAgSC2QADgCAGgDQABgBACgBQABAAAAAAQAMgHAJAAQAHgBAKAFQAGADAIAGIACABIABABQACABACABQAPAKAMgBQAHgBALgHAh7gYQAFABAFABQAEABAFABQAEABAFABQACABACAAQAFABAEABIAaAHQAVAEAPAEAjBgoQACABADABQAIABAIACQAOADAMADIAFABIAOADAjBgoQgEAagBAvQgBAmgHAlQAJAKAIAHQAKAJATACQAGABAGABQANACAEABIABAAQAMAFAWAMQAGADAGAEQABAAACABQAFADAFADQAAgCAAgCQACgPAVgBQAHAAAHAIQAIAHABAHQAAACgCAJQgJgHgKgBQgNgBgJgCQgCgBgBgBAgSAAQAAAhABASIABAXQAAAEgCAoQgBAVABArAhSgPQgBAZAAArQABAtgCBDAj5g+QAJAHAPAFQADABAEACQALAEAOADAkLhSQABAPgCAeQgCAkgEANQAAABgBABAkbAoQgCgCgEgLQgDgIgHAAQgBAAgBAAQgBADAEATIABABQAAABAAABQAFAXAAADQAQABAUAAQAVACAAAAQAAAAABABQABABABABQACACACADQAGAHAJALQAEAFADAEAkTARIgCADQAAABAAABQgBACgBACQgDAKgBAEAktATQgEACgFAHQgCAFgFAIQgBABgEAEQgCACgCACQgIAIADABQALAIAcADAjrBJIgCgCAjuBFQgIgHgNgHQgRgJgHgGAgbC6QACgBAHgDAgbC5IAAABQgWANgJgGQgNgKAAgL");
    this.shape.setTransform(0.0114, 7.5036);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#744331").s().p("AhIBOIgBgKIABgXQAFgygCgwQAAgVABgGIABgEIABAAIADAAIAAAAQAWAEAWAAQgCAEgBAGQgFAVgBAMIABAzQACAlgCAYIAAAEQgLAHgHABIgDAAQgKAAgOgJgAAoBDIgJgDQgDgjgBgmIgChKIgIABIAPgEQACAQACAIIAGATQAJAYACAKIAFAgQAEAUAFANIAFAJIACABIgBAIIgCAAQgLAAgUgHgAANhSIgBAAIAEAAIgCAAg");
    this.shape_1.setTransform(12.7438, 18.2295);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#6A4031").s().p("AgjAUIgEgIIgBgIQgDgHAAgGIAYgFQAOgDAGAAQAJABAJAEQAJADALgHIAFgEQgPAUgKAFQgRALgQAEIgOABQgFAAgCgBg");
    this.shape_2.setTransform(28.825, 23.2875);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#7B4D39").s().p("AgYA2IAIgyIAPgKQAOgIADgKQACgFAAgKIABgPQABgEAHgKIAAAIIABAOQAAAGgHAOQgEAKAAAIQAAAJAFAGIgFAEQgPASgDAHQgBAFAAAFIgCAAIgFABIgDACIgOATIgBABIADgPg");
    this.shape_3.setTransform(24.6813, 17.7);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#573325").s().p("ABqBWQgNgKAAgLIADACQAJACANABQAKABAJAHIAAABQgPAJgJAAQgEAAgDgCgAhbgjQgUAAgQgBIgFgaIAAgCIgBgBQgEgTABgDIACAAQAHAAADAIQAEALACACQAHAGARAJQANAHAIAHIABACIACACIgBAAIgUgCg");
    this.shape_4.setTransform(-16.4687, 18.2056);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#5C2C1B").s().p("AACAOQgRgKgHgFIAEgOIACgEIABgCIABgDIABgDIAEAEIAJAHIANAKQAGAEADAEIAAAaQgIgHgMgHg");
    this.shape_5.setTransform(-26.1, 11.7);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#BE9787").s().p("ADwAbIAWgQIAFgEIAAACIgBAAIgPAMIgNAIIACgCgAkJgbIgBgBIAAAAIACABg");
    this.shape_6.setTransform(0.725, 3.425);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#794531").s().p("AgrA+QgNgBgJgCIgDgBIAAgFQACgPAVAAQAHgBAHAIQAIAHABAHIgCALQgJgGgKgCgAD4AXQAEgHAPgRIAEgEQAJgLAGgEIADgCIABAAIABAAIAAABQAAAFAEAFIAFAGIABAHQAAAIADAKQABAEAEABIABAEQAAAAABAAQAAABAAAAQABAAAAAAQABAAAAAAIAHgDQAFgEAFADQgLAHgJgDQgJgEgJgBQgHAAgOADIgYAFQgBgFACgFgAlIg3QgDgBAIgIIAEgEIABAAQAGADAJgCIAJgCIAFAZQgcgCgLgJg");
    this.shape_7.setTransform(-0.2812, 19);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#633320").s().p("AABBGIgOgTIgFgFIgBgCIAAgBIAKhBIAEgtIACgKIAXAHQgDAagBAwQgBAlgHAlIgHgIg");
    this.shape_8.setTransform(-21.45, 10.5);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.f("#94614C").s().p("AjJBYQgTgDgKgIQgIgHgJgLQAHgkABglQABgwAEgaIAFABIAQAEIAaAFQgGArAAAvQAAAjAEAqIgMgBgAC/AiQAGgxACgpIAAAAIAIgBQAQgEAZgZIgIAdQgHAaAAAQQgHAKgBAEIgBAOQAAAKgCAFQgEAKgOAJIgPAKIACgXg");
    this.shape_9.setTransform(4.125, 12.375);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.f("#542716").s().p("AihBiQABhDAAgsQgBgrACgaIAaAGIAkAJQgFAFgDANQgFAbgCAZIgDA9QgMACgLAGQgLAHgGALIgEAJIgCgBgACNA7IgEgYQgBgLgCgtQgBgigHgUQgDgMgGgGQAFgEAHgBIAKABQAIAAAGAGIAFADIAAABIABACIACADIABAHQgCApgFAxIgDAXIgIAzIgDgeg");
    this.shape_10.setTransform(7.775, 14.25);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.f("#462215").s().p("AhgBqIABgLQgBgIgHgHQgIgIgHABQgUAAgCAPIAAAFIgLgGIAEgJQAGgLALgHQALgHAMgBIADg8QACgaAFgbQADgNAFgFIABAAQAAAiACARIAAAYIgBAqQgBAVAAArIgIAEgACEBeQADgagLgyQgMg4AEgaIgDgEIAAgBIgCgHQgCgOAJgHIAAAAQADgFAEgDQAGAGADAMQAHAUABAiQACAtABAKIAEAZIADAeIgDAOIgGAGQgGAEgHAAIACgHg");
    this.shape_11.setTransform(6.975, 15.475);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.f("#683B2A").s().p("AgXBYIAEADIAAAAIgEgDgAgXBYIgBAAIgCgCIgOgIQgKgFgHAAQgJABgNAHIgBAAIgDACIAAgGQADgWACg+IAAgdQgDgiABgRIAAgDIAGACIARAEIAAAAIACABIACABIAPAEIAYAHIgBAEQgBAGAAAVQACAugFA0IgBAXIABAKIgEgDgAgXBYIAAAAgAAggBQgBgeADgkIABAAIACAAIABAAIAWgCIAFAAIABAAIABAAIACgBIABAAIAIAAIACBIQABAoADAiQgPgFgIABIgEABQgJACgJAHQgHg2AAgdg");
    this.shape_12.setTransform(7.425, 16.95);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.f("#9D7B6E").s().p("AAuASIgBAAIgCAAIAAAAQgXAAgUgEIAAAAQAOABAdgBIAgACIgFAAIgWACIgCAAgAB2ACIAAAAIADAEIgEABQACgCgBgDgAhAgDIgGgBIgBgBIgHgCIgFgBIgBAAIgkgJIAIACQAcAFAWAHIAOAEg");
    this.shape_13.setTransform(6.475, 8.375);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.f("#825441").s().p("AgGBOIAAAAIgSgEQgDgqgBgiQABgwAGgrIAFABIAOADIADABIAJACIAJADIAJACIAAAGIgCAvQAAAoABBSQgVgMgMgEg");
    this.shape_14.setTransform(-12.45, 13.9);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.f("#82655B").s().p("ACQAvQgdABgPgBIAAAAIgBAAIgCgBIgBAAIgYgGIgQgFIgBAAIgCgBIgBAAIgOgFQgWgHgbgFIgIgCIgagGIgKgCIgEgBIgJgCIAAgDIgEgBQgOgGACAAIAGABIA0AJIAGABIAeAGIACAAIBjATQAYAEASACIAAAAIAFABIAAgBIAPABQARAAAUgEQAHgCACADIAAABIAAAAQABADgCACIgWAFIgPADIgBAAIgEABIgggCgAi/ghQgJgDgNgBIgBgBIgFgDIAEgEIACgBIADgCIAAAAQAOAAALAEQAIACANAIIgKAIIgRgHg");
    this.shape_15.setTransform(-3.6333, 5.075);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.f("#EED2C6").s().p("AADAGIgDgFIAEAFgAgDgEIAAgBIABABg");
    this.shape_16.setTransform(-26.325, -0.05);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.f("#543C32").s().p("AhAAwIAEABIALgIQAIgFABgFIAAgGIAEgGQADgFABgFQAHAGAEAHQgDABAAAFIAAAHQgBACgDABIgFAEQgBACAAAGIABAEgAiZAmIgagGIgRgEIgFgBIgZgHIgHgDQgPgGgJgGQANACAJADIARAGIABABIgCACIABABIAFgBIgBAAIAQgGIASgGIAEAAQADAAAKAJIAFADQABAAAAABQABAAAAAAQAAAAAAABQAAAAAAAAQABAAAAAAQABAAAAABQAAAAABABQAAABAAABQABADgQAAIgDAAQgSABAoAJgADHgiQADgGABgKQAaADAOgFIACgBQAGAFADAFIAEAKQgLAAgKACQgKACgTAHIgBABQgDgHgFgGg");
    this.shape_17.setTransform(0.775, 0.75);

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.f("#9D6753").s().p("ABeC7QACgYgCglIgBg0QABgMAFgVQABgGACgEIABAAQgDAkABAeQAAAeAHA2IgGAEIgFAEIgDACIAAgEgAgXB7IACgsIgBgXQgBgSAAghIAMADIAAAAIAAADQgBARADAiIAAAdQgCBAgDAVIAAAGIgJAFQgBgrABgVgADCCxIgFgJQgFgNgEgUIgFghQgCgKgJgYIgGgTQgCgIgCgQIAXgEIAEgCQgEAaAMA6QAKAxgDAaIgCgBgAhlCjQgChTAAgoIACguIABgHIAEABIAJACQgBAZAAArQABAtgCBDIgMgHgAEuCUIgBgDQgEgBgBgFQgDgJAAgJIgBgIIgFgFQgEgFAAgFIAAgBQAGABATARQAWATgDAHIgFAFQgFgDgFADIgHAEIAAAAQgBAAAAAAQgBgBAAAAQAAAAgBAAQAAgBAAAAgAEDBiQAAgIAEgKQAGgOABgGIgBgOIAAgIQAAgRAHgYIAIgdQgZAYgQAEIgIABIAFgDIANgIIAPgMIABgBIAEgEIAFgEIABgCIAEgDIACgDIALgQQAFgJACgIQAEAUgHAhQgCAOgEANIgIAXQgFANAAAUIABAlIgBAAIgDACQgGAEgJALQgEgHAAgJgAjwBOIgCgCIgBgCIAAgaQgDgEgGgEIgNgKIgKgIIgFgDIABgCQAEgOACgjQACgegBgPIABABIAAABIAAAAIAAAAIAEAHIADAEIABABIABAAIAAABIACABIAEADIACABQAJAHAPAFIAHADIgCAKIgEArIgKBCIAAACIgBgBgAlGAzIgBAAIAFgFIAHgNQAFgHAEgCQgBADAEATIABABIAAACIgJADIgGABQgFAAgEgCgAkQhNIAAAAIAAAAgAkRhSIABAFIgBgFgAkRhSQAAgOAOgMIACgCIABgBQAUgQAugLQAzgOCagdQAcgGAdgCQgdACgcAGQiaAdgzAOQguALgUAQIgBABIgCACQgOAMAAAOIAAAAgADjigIAJAEIgJgEgADaikIAJAEIgJgEgADaikIAAAAIAAAAgABdi+QBEABA5AZQg5gZhEgBgABMi9IgEAAIAEAAgABci+IgQABIAQgBgABci+IABAAIgBAAgABci+IAAAAIAAAAg");
    this.shape_18.setTransform(0.5176, 7.048);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.f("#A87E6D").s().p("ABmBkIAAgBIgBgCIAAgOQgBgJgIgMQgKgPgCgEQgCgGACgDQACgDAKAAQAIAAAKgFIAQgJQATgIAhAFIAMAAQAHgCABgFQABgEgFgGQgGgGAAgEIADgJIACgKQADgEALACQAOACAIgDIAGgCIgNgJIgZgOQgVAKgaADQgTAEgQAAIgPAAQgJAAgGADIgNAJQgHAEgLAAIgUgBQgVAAgUAFQgJACgDADIgHAHQgHAIgIADQgGABgBAEQgCADAFAGIANAOQAIAHABAFQABAEgCAKIgEAHIgGAEQgCADABAFIABACIgDAAIgDgBIgBgDQAAgHACgCIAEgDQADgCABgCIABgHQAAgFADgBQgFgHgHgGQAAAFgEAFIgDAGIgBAGQgBAGgIAFIgKAHIgFAAIgFgBIADgDIAIgFQAFgDADgEQADgDADgJQADgKADgDQgCgBAAgGQAAgCgDgEIgDgGQgCgIAJgEIAOgIIAJgJIgigEQgVgBgQADIgOABQgKgBgCgGQgGgBgJACIgPADQgTAFgJgJIgFgFIgBAAQgLAFgKAGIgZATQgQAKgLAGIgDACIgBAAIgDABIgCABIgEACIgBAAIAAABIAAgBIgCgBIACgBQgGgPAHgNQAJgSAlgLIAGgCIADgBIALgDIADgBIABAAIAMgEIBmgXQBQgSApgEQBEgIA1AMQAfAHAUALIACACIABAAIABABIACABIABABIAKAGQAIAEAGACIAOAMIABAAIAEAEIAFAEQADADADAGQAGALAAAKIAAABIAAABQAAAJgEAMQgEAMgHAJQgGAIgMAKIgSAOIgBgCIgBgBIgEgDQgGgGgIAAIgLgBQgHABgFAEQgDADgDAFIgBABQgJAGACAPIACAHQgGgLABgLIACgIIADgIIAEgFQAJgLANgBIANAAIAOAEQAEABAEAEIAGgJQAMgPAEgRQACgKgBgHIgTABIgQADQgJACgEADIgCACQAAAHgCAGIgogBQgVgBgLAFIgPAIQgOAHgRAAQABAHAIALQAJANACAFQABAEACAOIACAKIAAAAIgFAAgADBgNQAFAFACAIIACgBQATgHAKgCQAJgCALAAIgEgKQgDgGgFgFIgDABQgNAGgbgDQgBAJgCAHgAgugcIARADIAHADQAFgEAMgCQAZgFANABIATABQALAAAHgDIAOgKQAFgCAHgBQgNgCgHABQgPAAgaAJQABgCAFgCQgCAAgDgBIgSgKIgJgDQgGgCgNABIgKgCIgKgBQgFAAgEgCQgDgBgDgFIgBgBQg5ALg6ASIgQAGIAFAFQAJAGASgGQATgFAIAEIAGAEQAHADAMgDQAJgCAOAAIAYABgAgfhIIgKACIACADQADACAIAAIALABIALACIAOABQAGABALAGIATAJIgBABQAOgFAKgBQALgCARABIAdACQANAAARgCQAJgDASgIIAHgCQgggPglgHIgMAAQhFABhFANgACug3QAPgDAHgDIAAAAIgEgCIgSAIgAiJA/IgEgBIgOgDQgogKASAAIADAAQAPgBAAgDQAAgBgBgBQAAgBAAAAQgBgBAAAAQgBAAAAAAQAAAAAAAAQAAgBAAAAQgBAAAAAAQAAgBgBAAIgGgDQgJgIgEgBIgDAAIgTAHIgPAFIAAAAIgEABIgBgBIACgCIgCgBIASgKIAQgCQAKgCAEADIAFADQAFAFAGADIAEACQACABAEAGQACAEADACIAHADIAEADQgBAAANAGQgGgCgIACg");
    this.shape_19.setTransform(1.404, -1.273);

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.f("#B39082").s().p("ABgBdIgCgKQgCgOgBgEQgCgFgJgNQgIgLgBgHQARAAAOgHIAPgIQALgFAVABIAoABQACgGAAgGIACgCQAEgEAJgCIAQgDIATgBQABAIgCAJQgEARgMAPIgGAJQgEgEgEgBIgOgEIgNAAQgNABgJALIgEAFIgDAIIgCAIQgBALAGALQgCgDgGACQgUAEgSAAIgPgBgAAxBWIhhgSIADAAIgBgCQgBgFACgDIAGgEIAEgHQACgKgBgEQgBgFgIgHIgNgOQgFgGACgCQABgEAGgCQAIgDAHgIIAHgHQADgDAJgCQAUgFAVAAIAUABQALAAAHgEIANgJQAGgDAJAAIAPAAQAQAAATgEQAagDAVgKIAZAOIANAJIgGACQgIADgOgCQgLgCgDAEIgCAKIgDAJQAAAEAGAGQAFAHgBADQgBAFgHACIgMAAQghgFgTAIIgQAJQgKAFgIAAQgKAAgCADQgCADACAGQACAEAKAPQAIAMABAJIAAAOIABADQgSgCgYgFgAgrBRIAFAAIAHADIgMgDgAiMAzIgGgBIgEgDIgHgDQgDgCgCgEQgEgGgCgBIgEgCQgGgDgFgFIgFgDQgEgDgKACIgQACIgSAKIALgIQgNgHgJgDQgLgDgNAAIADgCQALgGAQgKIAZgTQAKgGALgFIABAAIAFAFQAJAJATgFIAPgDQAJgCAGABQACAGAKABIAOgBQAQgDAVABIAiAEIgJAJIgOAIQgJAFACAHIADAGQADAEAAACQAAAGACABQgDADgDAKQgDAJgDADQgDAEgFADIgIAFIgDADgAiKA6IgKgCQAIgCAGACIAEABIABADIgJgCgAEWALIACgKIADgPIAAAIIAAAGQgCAHgFAJIACgFgAkaANIAAgBIABAAIAEgCIgDAEIgCgBgAgoggIgRgDQgfgCgQADQgMADgHgDIgGgEQgIgEgTAFQgSAGgJgGIgFgFIAQgGQA6gSA5gLIABABQADAFADABQAEACAFAAIAKABIAKACQAOgBAGACIAIADIASAKQADABACAAQgFACgBACQAagJAPAAQAHgBANACQgHABgFACIgOAKQgHADgLAAIgTgBQgNgBgZAFQgMACgFAEIgHgDgAAhg0IgTgJQgLgGgFgBIgPgBIgLgCIgLgBQgIAAgDgCIgCgDIAKgCQBFgNBFgBIAMAAQAlAHAgAPIgHACQgSAIgJADQgRACgNAAIgdgCQgRgBgLACQgKABgOAFgAC1hGIAEACIAAAAQgHADgPADIASgIg");
    this.shape_20.setTransform(2.525, -0.575);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_20}, {t: this.shape_19}, {t: this.shape_18}, {t: this.shape_17}, {t: this.shape_16}, {t: this.shape_15}, {t: this.shape_14}, {t: this.shape_13}, {t: this.shape_12}, {t: this.shape_11}, {t: this.shape_10}, {t: this.shape_9}, {t: this.shape_8}, {t: this.shape_7}, {t: this.shape_6}, {t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    // Layer_2
    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.f("rgba(124,113,80,0.529)").s().p("Ag1BeIgpgZQgJgFgKgBQgHAAgWACQgnACgTgZIgJgOQgFgJgFgEQgIgHgQgDQgNgCgcgBQgbAAgOgDQgRgDgLgIQgOgKgDgNQgEgNAGgNQAGgMAMgJQAQgMAigGQBegQBJAQIA6AQIBGAPQArAHAZAIIAgALQATAFANACQAUADAngCIBSgEIAggBQARACAOAEQAPAGALAMQAMAMABAOIAAAHIABAEQACAGAAAFIgBAOIgFAMQgIAMgGAFQgLAKgcAIIgSACIgHgBQgLADgKgCQgOgCgFABQgEAAgLAIQgPAJgRgGIgUgKQgMgFgJAAQgGAAgSAKQgVAKgYgCQgYgCgSgOQgJgFgCAAQgDAAgIAGQgSAPgVABQgTAAgXgPgAgUAnQAGAEAEgBIAGgCIgEgFQgGgEgMgCIgPgDgAiugjIACgCIgFgBIADADg");
    this.shape_21.setTransform(-1.4145, 18);

    this.timeline.addTween(cjs.Tween.get(this.shape_21).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-38.7, -13, 74.6, 41.9);


  (lib.Tween5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0004, -0.0235, 1.2751, 1.0272, 0, 0, 180);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-177.1, -59.6, 354.29999999999995, 119.2);


  (lib.Symbol16copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#391200").s().p("AoCKjIgJgOIAKgIIAXgMQAVgLAPgTQAPgSALgYQAHgPAGgQIAAgDQgDgEgGADQgRAJgQAKIgQAKQgPgFABgPQAAgJAFgHQALgRAPgMQAVgSAXgPIAggSQAMgGADgNQADgLAAgJIADgTIAAgEQgEgLgMAEQgNAEgMAGQgeAPggAJIggAJQgtAJgsAFQgrADgpgPQgagKgZgMQgagLgXgPQgxgdgngtQgOgRgJgSQgIgRgCgTIgCgaIADgCQBAARA/AOQAeAGAfAEQB6ANB5gKIBUgMQARACAMgIIADgGQgVgrgOgqQgJgegFgdIgLg1QgHgZgIgZIgOgpQgLglgCgnIgCg/QgCg3ALg1QADgOAMgHQAKgEAEgJIgFgLQgCgFgEgEQgJgHgMgCQgQAIgKASQgHAMgJAKQggAhgmAYQgMACgHgKQgGgHgDgHQgDgHABgHQAFgVAHgUQAWg4AhguIgDgIIgIAAQgTANgTARQgXAWgaARQgZASgdANQgEADgFgDQgHgDgDgGQgEgHACgGQAFgRAKgPQAhgwAugmQBPhABbgpQCLg/CWgPQBHgHBGAFIBCAGQASABARAEQAKAEAKAHQAUAQANAWQAFAJACALQADATACATIACAoQABATgCAUIgGA7IgKBeIANAHQAog3AvgwQA5g7BKgjQAtgXAzgDQAigCAjgGIAygGQAcgGAegDIBTABQA7AHAuAkQAWARAGAcQAWBZgSBbIgLAzQgJAogVAiQgUAhgdAVQgQgBgIgPQgFgJgCgKQgJgqAFgqQADgZgBgZQAAgSgCgRQgHAMgEANIgWA/IgJAVQgXAugnAiQgKAEgIgHQgFgFgDgFQgKgRACgUQABgIADgHQAKgdABgeIgMAEQgMAhgYAXQgDADgEABQgXgFgMgUQgiBNgbBOQgXBAgaBAQgGANAMAGQAPAJARAGQBIAcBFAiQASAJAUADIBJASQARAEAQAHQAQAIAOALIARAOQALAMAMAKIAZAXQAFAFABAHQABAIgCAIQgCAKgFAJIgWAmQgDAFgHAAQgFACgFgCQgGgEgFgFIgMgMQgEAAgEACQgpAOgpAQIigBDQgtATguARQgoAPgpAFQg7AIg9AFIgtADQhNgIhJgbQgkgNgkgIQgJgCgJACQgKACgJAGQgeASglADQgKgBgKACQgNACgLAKQgGAFgBAHQgEATgBATQgDAkgcAXIgcAVQgmAcgrANQgWAIgXAFQgGABgFgCQgGgCgFgFQgHgGAEgIIAKgRIAEgHIgsAXQggAVgnAEQgOgCgGgJgAjxHlIgRAPQgDAOAIAJQADAEABAEQAAAEgBAEQgMAhgaAWQgRAPgNAUIAFAJQAjgMAbgXQAWgVATgXQAFgGgDgIIgXgLQgDgKAEgIQAEgIAFgGIAPgPIAEgFQAAgFgFgCIgMgBQgLACgLAJgAlDI1IgZAfIgeApQAPgJAOgKQAOgNAOgOQAXgVALgfIABgFQgWALgPAUgAmUHmIgBAIQABAIAIAEQAFADACAGQADAIAAAJQAAAIgEAIQgNAegPAdIgOAaQAggQAYgaQAbgdgCgoQgBgJgGgHQgGgIgJgGQgLgFgLgEgAgLGnQATALAWAJQAwAVA2AJQAkAFAkAAQAxgBAxgDQgXgLgQgSQgWgZgQgfQgGgLgDgKQgNgsABgwQABhaA2hIQAEgFADgGIgJgNIgHAAIgHAEQgLADgHAJIgDAVIgCADQgJADgIgEQgKgGgCgLQgFgggLgdQgJgYgGgYQgShVgGhYQgKiTAOiTQAEgoAAgrQgBgwgYgoIgJgPIgFABIhTAvQAAAAgBAAQAAABAAAAQAAAAgBABQAAAAAAAAQAAAFADADQAEAFAGAFIAaAVIABAKIgLAKQAAAAgBAAQAAAAAAAAQgBAAAAAAQgBgBAAAAQgJgDgIgFQgNgHgQAAQgHADgFAJQgVAmgFAsQgCAUAIARIAKAUIgEANIgNAGIgEgCQgMgIgFgQQgEgPgPgGQgLABgJAHQgMAHgKAJQgjAhgPAtQgRAwgBA0QgCA7ASA4IALAdIAcBIIAFAOIgMAGQgHgBgEgGQgHgPgGgPQgIgWgQgQQgGgFgHgEQgHACgCAHQgMAzAOAzIAOA0QAYBVA6BBQAYAbAcAZQAFADAEAGIgMAOQgGACgHgDIg5gYQgVgIgQgOQgpASAIACQAEACADAEQAAAGgFADQgPAOgTgFQgLgDgLgBQgSgFgNgPQgTgWgcgDIg2ALQhbAUhdgEQgygFgwgDQhLgDhIgSIgVgFQgHAAgGAHIgCAHQADAFAEAEQAbAaAdAYQAnAiAuAUIAwATQAlANAmABQBegKBUgpIAEgKIAFgJIAHgIIAXADQAHACAIgCQAHgBAHgFIAPgKIALgIQAJABADAGQAIAMgFANIgUApIgJAPQgFAIgEAJQgCAIADAIQAEAKAJAJQAUAYAdgHQAKgBAKgEQAPgGAHgOIgGgGIgSgLIgVgMIAEgSQAJgGAJAHQAUAOAXAGQAhAIAkADQAegBAeAEQADACAEgBQAEgCACgEQADgGABgHIABgKIAGgGIAuAbgAExBqIgHADQgLAagFAdQgBAFgFADIgQANQgHAHgFAJQgLAZAAAdQAAAZACAZQADAYAGAZIAMArIACAKIA2AmQAeAVAkgEQAagCAbgHQAsgMApgTQAxgVAugbIAEgGIgJgYQgGgWAMgSIAQgXQgHgRgOgLIgagQIgQgIQgCgKAIgEIAOgHQArgBAmARQAWAKAZAHQAKADAMABIAEgDIAGgVQgQgGgTgFQgVgCgVgGQgugJgtgKQgEgCgDgEQgNgTgWgHQgegKgggJQgfgLgggIIhBgMIgsAFgALPEXQgSAEgTABQgcAIgNAYIgDAHIAIAJQAfgVAjgKQASgDAUgCQAiAFAiALIAGAAQAIgKgGgKQgGgLgLgDQgPgFgOAFIgUgBQgVAAgUACgAmfjoQgFAlACAmQAEA6ATA4QAJAFAHAHQAQAOgBATIAAArQADBIAfA+QAUAnAdAgQAKALAOAGQALAGBGgQIgEgEQgKgJgDgOIAGgKIAGgBIAtAcQgBgKgGgIIgVgeQhFhhgLh2QgDggAOggQAEgJAGgIQALgNAAgRIgCg6QgDgxAKgyQAKgxAYguQATglAbgeQAEgGAHgCQALgDAMgCQAPAAAHgLQAEgGACgGQAHgjASgfQALgSAHgTQgiAZghAYQiKBehZCNQgWAjgkAWQgKAFgKgDQgMABgJAJgAFVBHIArAHIAJgEQAFgEACgEQAKgUAIgUIATgzIAdhNIATg0QAYhGAmhAQA3hbBMhLQAQgPASgOQASgMAUgHQARgHATgDIAaAIIAAARIgGAEQgPgBgOABQgGACgGADQgKAHgJAJQAOAKAGAPQAIAVAEAWQAGAfADAhQADAhABAhQAAAagCAbIgCAeIAAAJIAMAFQAMgHAFgMQAKgZAHgaIADgUIAHg7QAEgsACgsQACgZgEgZQgCgRgFgRQgFgSgOgIQgOgIgQgEQgogLgoABIgPABIhNADQg2AHg3AQQgZAIgZAKQgUAHgSAKQgWANgVAPIgzAlQgDgCgCgDQgDgFgBgFIAAgEQAFgKAKgIQAfgXAhgVIAegVQg5Ajg3AlQgOAJgLALIgVAWQgPAPgPAQQgRAUgQAVIghAwQgLAOABASIAFA6QAFBAAJA/IAFAiIANBDQAEATASAGIAigBQAqAAAqAGgALKnCQgMAIgJAKQgKAMgCARQgCAPABAPQABAWgDAVQgFAigHAiIgJArIgHAkQAhgXAMgmIAQg1QAHgYAFgZIANg7QAEgSABgSQABgGgEgEQgEgEgGgCQgIACgGAFgAJylsQgQAdgPAeQgOAcgDAhQAegbANglQAMglAYgfIAEgGIgKgHQgQAKgJAPgAgtqNIg5ABQgSAAgRADQgGAAgEAFQgEAFgCAFQgFALgIAGQgmAbgfAiQgYAbgWAdQgWAdgTAgQgSAfgPAhIgbA/IgGAPIAAADQABADADACIAHAGQAKgBAEgHIAmg3QA6hSBKhEQAjggAlgYIBCgpQAZgQAbgNQAbgNAdgLIgJgHIgsAAIgtAAgAlyo0QgCAUgRAMQgbAVgYAXQgXAXgUAZQgLAOgKAQQgbAugRAvIAGAFQAJgBAFgGQARgVASgUIAygzIAzgvQAagXAbgWIArglQAegZAdgbQAWgVAagRIgCgKQgUgEgSAHQgXAKgZAGQgdAIgcAKQgdAKgbAMQgaAKgZALQhMAkhAA4QghAcgfAeIAHADQAJgBAHgGQAkgcAlgaQAbgTAdgRIAvgbIAngYIAYgNQADgCAEgBgAmZmPQgCAEABAEIAEAJIAJgBQATgXAFgdIABgMQgWAXgPAZgABZDHQgGgFgDgHIgMgsIgShOQgJgnABgoIAAg9IgBgfIACg4IAFhCQADgjAGgiQAGgnACgnQABgOAEgLQACgGAEgFIAGAEQADADACAEQAEAJAAAJQABAXgDAYQgQBygBB0QAABtATBsQACAKADAKIAHATIAFAPIAEAKIACAKIAAAMIgOAJQgHgDgEgFg");
    this.shape.setTransform(0.0101, 0.004);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#E2E0D9").s().p("AivCkIg2gmIgDgLIgMgrQgGgYgDgZQgCgXAAgZQAAgdALgaQAFgJAHgGIAQgOQAFgDABgFQAFgdALgaIAHgDIAsgFIBCANQAfAIAfAKQAfAJAeALQAWAGANATQADAEAEACQAtALAuAJQAVAFAWADQASAEAQAHIgGAVIgEADQgMgCgKgDQgZgGgWgKQgmgRgqAAIgPAHQgIAFACAJIAQAJIAaAQQAPAKAGARIgQAWQgMASAGAXIAJAYIgDAGQgvAagxAWQgnASgtANQgbAHgaABIgMABQgdAAgYgRg");
    this.shape_1.setTransform(49.3, 28.1827);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#FFC600").s().p("AoECsQANgUARgPQAagWAMghQABgEAAgEQgBgEgDgEQgIgJADgOIARgPQALgJALgCIAMABQAFACAAAFIgEAFIgPAPQgFAGgEAIQgEAIADAKIAXALQADAIgFAGQgTAXgWAVQgbAXgjAMgApoCWQAPgdANgeQAEgIAAgIQAAgJgDgIQgCgGgFgDQgIgEgBgIIABgIIAJgDQALAEALAFQAJAGAGAIQAGAHABAJQACAogbAdQgYAaggAQIAOgagAG4iFIADgHQANgYAcgIQATgBASgEQAegDAfACQAOgFAPAFQALADAGALQAGAKgIAKIgGAAQgigLgigFQgUACgSADQgjAKgfAVg");
    this.shape_2.setTransform(19.8036, 45.7125);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#592626").s().p("AilEtQg7gJg7AEQgSgHgEgTIgNhEIgFghQgJg/gFhAIgFg6QgBgSALgOIAhgvQAQgWARgTQAPgRAPgPIAVgVQALgMAOgIQA3glA5gjIgeAVQghAVgfAXQgKAHgFAKIAAAFQABAFADAEQACADADADIAzgmQAVgOAWgNQASgKAUgIQAZgJAYgIQA3gQA2gIIBNgCIAPgBQAogCAoAMQAQADAOAIQAOAIAFATQAFAQACARQAEAZgCAaQgCArgEAsIgHA7IgDAUQgHAZgKAZQgFANgMAGIgMgEIAAgKIACgeQACgZAAgaQgBgigDghQgDgggGggQgEgWgIgUQgGgQgOgKQAJgJAKgHQAGgDAGgBQAOgBAPAAIAGgDIAAgSIgagHQgTACgRAHQgUAHgSANQgSANgQAQQhMBKg3BcQgmA+gXBGIgTA0IgdBOIgTA0QgIAUgKATQgCAFgFADIgJAFIgrgHgACWAKIAJgrQAHghAFgiQADgWgBgVQgBgQACgPQACgQAKgMQAJgLAMgIQAGgEAIgDQAGACAEAEQAEAEgBAGQgBASgEATIgNA7QgFAYgHAYIgQA1QgMAmghAWIAHgjgABYhLQAPgeAQgdQAJgQAQgKIAKAHIgEAGQgYAggMAkQgNAlgeAcQADghAOgcg");
    this.shape_3.setTransform(50.7182, -22.9087);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#874F4F").s().p("AGZIvQg2gIgwgVQgWgJgUgMIgugbIgGAGIgBAKQgBAHgDAHQgCADgEACQgEACgDgCQgegFgeABQgkgDghgIQgXgGgUgOQgJgGgJAGIgEARIAVAMIASAMIAGAFQgHAPgPAGQgKADgKABQgcAHgUgXQgJgJgEgLQgDgHACgIQAEgJAFgIIAJgQIAUgpQAFgMgIgMQgDgGgJgBIgLAHIgPALQgHAEgHABQgIADgHgDIgXgDIgHAJIgFAJIgEAJQhUApheAKQgmAAglgNIgwgUQgugUgnghQgdgZgbgaQgEgEgDgFIACgHQAGgGAHAAIAVAFQBIASBLACQAwADAyAGQBdAEBbgVIA2gKQAcADATAWQAMAPASAEQALABALAEQATAEAPgNQAFgEAAgGQgDgEgEgCQgIgCApgRQAQANAVAIIA5AZQAHADAGgCIAMgPQgEgFgFgEQgcgYgYgcQg6hBgYhUIgOg0QgOg0AMgzQACgHAHgDQAHAEAGAGQAQAQAIAWQAGAPAHAOQAEAHAHABIAMgHIgFgOIgchHIgLgdQgSg4ACg7QABg0ARgvQAPgtAjghQAKgKAMgHQAJgGALgCQAPAHAEAPQAFAPAMAIIAEACIANgGIAEgMIgKgUQgIgSACgTQAFgtAVgmQAFgJAIgCQAQgBANAIQAIAEAJADQAAABABAAQAAAAAAABQABAAAAAAQABAAAAAAIALgLIgBgJIgagWQgGgEgEgGQgDgDAAgEQAAAAAAgBQAAAAABAAQAAgBAAAAQAAAAABgBIBTguIAFgCIAJAPQAYApABAvQAAAsgEAoQgOCTAKCSQAGBYASBVQAGAZAJAXQALAeAFAgQACALAKAFQAIAEAJgDIACgCIADgVQAHgKALgDIAHgDIAHgBIAJANQgDAGgEAFQg2BJgBBZQgBAwANAsQADALAGAKQAQAfAWAZQAQASAXAMQgxADgxAAIgGABQghAAghgGgAFbk6QgEAMgBANQgCAngGAoQgGAigDAjIgFBBIgCA4IABAeIAAA9QgBApAJAnIASBOIAMAsQADAHAGAFQAEAFAHADIAOgIIAAgMIgCgLIgEgKIgFgOIgHgTQgDgKgCgLQgThrAAhtQABh1AQhyQADgXgBgYQAAgJgEgIQgCgFgDgCIgGgEQgEAEgCAGg");
    this.shape_4.setTransform(-27.675, -8.6479);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#A6654D").s().p("AgjGqQgOgGgLgLQgdgggUgnQgfg/gChHIAAgsQAAgUgPgOQgHgHgJgFQgTg4gEg6QgDgkAGgmQAJgIALgBQALADAJgGQAkgVAWgjQBZiOCJheQAigXAhgaQgGAUgMASQgSAegHAjQgBAHgEAFQgIALgOABQgMABgMADQgGADgFAFQgbAfgTAkQgXAvgKAxQgKAxADAyIABA4QAAARgJANQgGAIgEAJQgOAgADAhQALB2BDBhIAWAeQAGAIAAALIgsgdIgGABIgHAKQAEAOAKAKIAEAEQgyAMgUAAQgHAAgDgCgAi9joQgBgFACgDQAQgaAWgXIgBAMQgGAdgTAXIgJACIgEgJg");
    this.shape_5.setTransform(-22.1141, -15.8432);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#884B4B").s().p("AhXCzQgDgCgBgDIAAgDIAGgPIAbg/QAPghASgfQASgfAVgdQAWgdAZgbQAegiAmgbQAJgGAFgLQACgFADgFQAEgFAGAAQASgDASAAIA4gBIBaAAIAJAHQgcALgcANQgbANgaAQIhBApQgmAYgiAgQhKBDg6BSIglA3QgFAHgKABIgGgGgAj8CXQARgvAcguQAJgQAMgOQAUgZAXgWQAYgXAagVQASgMABgUIgGgHQgEABgDACIgXANIgoAYIgvAbQgdARgbASQglAagkAcQgGAGgJABIgHgDQAegeAhgcQBBg3BMgkQAZgLAZgKQAcgMAdgKQAcgKAcgIQAYgGAXgKQATgHAUAEIACAKQgaARgXAVQgdAbgdAZIgrAlQgbAWgaAWIgzAvIgxAzQgTAUgRAVQgFAGgIABg");
    this.shape_6.setTransform(-29.625, -46.8625);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_6}, {t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-89.1, -68.6, 178.2, 137.2);


  (lib.Symbol15 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#391200").s().p("AgvItIgPgRQgTgXgIgdQgHgXgCgWIgIhJQgBgVAAgUIABgQIgBgCIgJgEQgZATgeAHQgJADgKABIgGAFIgPA/IgDAGQgpAxg0AlQgNALgRAFQgMADgNABQgYgEAAgXQAAgGAGgFQAGgGAIgEQAYgNAMgXQAJgRAAgTIABgQQgZASgTAZQgOATgRAPQgbAZgdAXQgSAOgXgHQgSgGgHgRQgFgNAFgPQAIgUAUgFQAbgHAXgQQAKgGAIgIQAKgKAHgLQAHgJAEgKIAEgJQgCgFgCgDQgFgEgFgCIgHgDIgYARQgHAFgIAEIgEACQgKgCgDgIQgCgGABgFQADgMAHgKQAUgdAigNQASgHAUAAQANACABgNQABgLgDgLQgKgfAQgdQAHgMADgNIADgPIgwBIIh9AcIgwAJQhEAHg5gkQg5gkg1gpIgygoQgHgFgBgLQgBgOAGgNIAGgMQAFgCAFADQAaAAAZAFQB0ASB0gEQBHgCBDgRQAYg/Afg9QAphSA5hIQgPAEgNAJIgqAhIhCA2QgWASgVAUQgHAIgGAJQgFAHgIADQgSADgEgPQgCgLABgKQAEgpAVgiQAMgTAPgSIAzg9IApgzQAgglAkggQAQgOARgMQARgMATgKQAzgdA5gWQAqgQAqgNQApgOArgNQAegJAfgHQAfgHAhgFQBFgFBFAKIA0AFQAJACAJAIQAFAGADAHQACAHAAAIQACAVgHATQgRA0gcArQgYAmgOArIgWBBIgLAkIAbgkQAIgKALgIQASgNASgJQBrguB0gJQAygCAyAHQAPAAANAHQANAGAMAKQAMAKAKANQAGAHgFAIIgKAPQgWAjgYAiQglA1gcA6IgaA2IACAFQApAPAuAHQAqAGAaAgIAPATQADAEAFADQAUAMALAWQAJAUgFAVQgFAWgSAOQgKAIgKAAIgFgBQgJgJgKgGQgHgEgIACQgpANgnARQgZAKgbACQgZAEgPAXQgNAUAAAXIgBAzQAABDAMBDQADASgHAQQgFAKgNADQgGABgFgCQgIgEgHgGQgUgTgLgYQgLgagIgZQgIgZgCgYQgDgbgBgaIAAgTQgPgNgRANQgGAFgBAIQgJBMAMBMQABAIgEAFQgGAIgJADQgPgCgIgNIgQgcQgSgggIgkIgGgjIgFgfIgJAGQgXAeACAnQAAAQgFALQgCAEgEACIgFAEQgMACgGgKQgLgSgIgTQgEgIgCgJIgBgHQgTgMgWADIg4AEIgrgGIgzgMIgogHQgGAOAAAQQAAA6gEA4QgCAbACAbIABAPQACAXgMAWIgGAHIgIAFQgZAAgRgVgAA7jcQgBAHgCAGIgHAPQgmBIgYBQQgcBdgPBiQgIAygFA0IgCAZIgBAaIAAAZIABAlQABATACASIAFAlIAGAjIADAMQAEALAFAKQAHANAIAMIAIAJIAEgDQADgDABgDQABgJgBgJQgIhCAIhBQANhrAThrQAFgfAHgeIAIgdIAJgeIANgsIANglIAQguIANgiIAOgjIAPghQAQgfARgfIAhg8QAYgpAdgoQARgWAUgUQASgTAUgSQAEAGABAGQABAFgDAFQgSAbgTAZQhcB9g7COQgYA5gUA8IgYBKIgHAZIgGAYIgFAaIgFAZIgFAXIgFAYQgDAKgCAKQgBALACAKQABAGAFAEQAMAIANAFQAaAIAcADQAkAGAlAAIA1gCIANgDIBJgLIAMAAQgBgHgCgFQgCgGgEgEIgcgbQgagbgOgkQgMgjgEglQgDgaAIgaQAGgSAEgTIABgKIAAgEIgCgFIgKgXIgFgLIgDgNQgDgOAAgOQAAgjAFgjQAFgmAJgmQADgLAFgLIAag/QAUgwAXgsIAOgZQAPgXANgZQAEgGAAgIQACgQgBgRIgCgLIgUgBQgngDgngBQgngBgnABIgxAIIgNAVQgEAFgFADQgKAHgKACIgwANQgYAEgYAHQhrAkhjA6QgmAWglAYIglAaIgkAbIgPANIgOANIgcAgIgNARIgMASIgNAXQgHANgDAPQgBADABAFIBEg7QAbgYAcgVQAdgVAegSQAcgRAdgPIA4gfQATgJARgQQAMgKAMgGIAMgDIAJAMQgCAGgDAFQgEAFgFAEQg+AigwAyQhDBHgyBUQgbAugWAxIgOAgIgLAhIgKAiIgJAjIgGAjIAAAHIACAFIAKAFIAHgKIAUghQAeg4Aig3QAjg5Ang4QAZgkAdgfIAbgeIAcgeIAdgdIAdgcIAVgVIAWgTIAOgMIANgMQADgDAFgBQAFgCAFABQAHABADAHIABADIgFAFIgsAqQg7A3guBDQgqA/ggBEQgRAjgKAmIgNAvIgHAdIgGAbIgGAlIgGAjIgEAmQgBAXAAAYQAAASACASQADAUAFAVIADAKIANAAIABgNIAIhCQAJhLAfhFIAVguIAshbQAUglAWglIAUggIAUgfIASgYIAQgaIASgaIARgbIADgEIAFgDIAKgCgAlsFOQgBATgFARQgEARgOAOQgJAKgLAHQgPAMgRAKIgcASQgIAFgGAIIAAAGIAEAGQANgBAJgGQAngaAhghQAbgcAPgjIgBgQIgEgEQABgHgBgHIgCgBQgNAAgCAPgAlBHcQAdgLATgXIAOgTQADgLgGgIQgFgHgIgDgAILD/QgIADgCAIQgDAQABASQACAfAFAfQAJAxAcAqQACgjgJgjQgEgSgBgRQgCgeABgdIACgZIAAgFQgGgGgHAAQgDAAgFACgAGFFtQAEARAHASIAFALQAJgEAAgMQgBgXgCgWQgCgNACgNIAEgRIgFgLIgigBIANBGgAlDEbQgGADgDAFQgFAHgDAKQAKAIAMgJQAJgHACgLQgEgHgGAAIgGABgAi2ETIgFAHQgCAMALAGQAHAEAHgGQAJgHALgEIgegPgAFag9QgFAGgBAHQgDAKgCALQgKAGgIAJQgJAIgEALQgIAXgHAZQgEAQgCAQQgBANADAOQAEAQAHAQQAJAUAHATQAFANAIANQASAeAdAUQAKAGAMgBIBGgVIA+gTIA4gVIAYgKIACgEIgGgTIgEgQQgBgHABgGQACgGAEgGIASgZIAAgHIgNgLQgJgIgHgJQgGgJgCgKQgEgOAAgPIAAgIIgRgHQgEgIgFgIQgGgHgHgEQgSgHgTgFIiIglIgGgBQgKAAgHAIgAkwCEQAAAogOAlQgDAHgFAHIgFAHQADAHAEAGQAFAIAIAIIAFAFIANgDQADgKACgLIALhKQAOhhAohYQAGgLABgNQABgHgBgHIggA2IgWAkQgKAQgBAUIAAAaIgWgDgAhVAaIghBIIgUAsIgTAsQgHARgGASIgDAMQAWAUAbAIQAFABAGAAQAHgFAAgGIAKhFIAGgkIAIgkIAHglQAOg4ATgzIgHgDQgUAegQAhgAstBLQgDALAJAFIAaARIAyAkIAcATIAfAVQAKAHAMAFQAWAJAYAFQAXADAXAAQA5gHA3gKQAKgDADgLQAJgfgCggIABgaIgDgEIgOAAQgdADgeAAQgeAAgegCIhegBIgrgDIgrgGIgagEIgigHIgMAGgALgB2QAgADAeALIAKADIANgMQABgLgIgGQgHgGgJABQghgFghAEQgdAFgcAJQgPAEgOAGIALAEQAfgKAgAAIAQAAgAJmA3QAGAQAQAGIAKADIAEgHQAAgJgJgGQgJgFgJAAIgJACgAKDAZQAaAYAiAJIAcAHIACgCIAEgCIAEgEIACgGIACgFQgLgNgQgDQgRgCgRgEIgOAAIgbABgAKxkYQAEADADAEQAEAFABAHIABADIgFAEQg0AagxAdQgZAPgYASQgoAhghAnQgFAFgCAHIAMAHIBFAcIArAQQAFgDADgFQAKgNAIgQQAbg0Aeg0IAkg6IAYgjIAKgRQgGgOgOgEQgcgGgbgCQgbgFgbgBQhHAGhEAVQgjALghAPQgiAPgeAUQgPAKgOANIgYAUIgHArQgDAOANAJQAEAEAGABIBMAPIAsAHIADgBQAfgbAcgdQAZgZAbgWQAZgUAbgRIA+gnQAQgKASAAIADAAgAEag1IACADQAGABADgEIAMgOQAEgGgEgGIgLgJQgUANAIAWg");
    this.shape.setTransform(0.0212, 0.0167);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("AAEAKQgPgGgGgPQAOgEAMAHQAJAGAAAIIgEAHIgKgDg");
    this.shape_1.setTransform(63.225, 6.7054);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#FFC600").s().p("AqLC6IAAgGQAGgIAIgFIAcgSQARgKAPgMQALgHAJgKQAOgOAEgRQAFgRABgTQACgPANAAIACABQABAHgBAHIAEAEIABAQQgPAjgbAcQghAhgnAaQgJAGgNABgAm8BnQAIADAFAHQAGAIgDALIgOATQgTAXgdALgAn9ARQADgKAFgHQADgEAGgDQALgEAFAKQgCAKgJAHQgGAFgGAAQgFAAgFgEgAJ1ieQgegLgggDQgogDgnANIgLgEQAOgGAPgEQAcgJAdgFQAhgEAhAFQAJgBAHAGQAIAGgBALIgNAMIgKgDg");
    this.shape_2.setTransform(16.9118, 29.1111);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#E2E0D9").s().p("AiNCjQgdgUgSgeQgIgNgFgNQgHgTgJgUQgHgQgEgQQgDgOABgMQACgQAEgQQAHgZAIgXQAEgMAJgIQAIgJAKgGQACgLADgKQABgHAFgGQAJgKAOADICIAlQASAFASAHQAHAEAGAHQAFAIAEAJIARAHIAAAIQAAAPAEAOQACAKAGAJQAHAJAJAIIANAKIAAAHIgSAZQgEAGgCAGQgBAGABAHIAEAQIAGATIgCAEIgYAKIg4AVIg9ATIhGAVIgFAAQgJAAgIgFgAC9gnQgigJgagYQAUgCAVABQARAEARACQAQADALANIgCAFIgCAGIgEAEIgEACIgCACIgcgHg");
    this.shape_3.setTransform(51.4025, 9.8317);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#592626").s().p("ADwElQgFgfgCgfQgBgSADgQQACgIAIgDQAMgGAJAKIAAAFIgCAZQgBAdACAeQABARAEASQAJAjgCAjQgcgqgJgxgAB6FLQgHgSgEgRIgNhGIAiABIAFALIgEARQgCANACANQACAWABAXQAAAMgJAEIgFgLgAnIDnQgLgGACgMIAFgHIAIgDIAeAPQgLAEgJAHQgEAEgFAAIgFgCgAEOh0IhFgcIgMgHQACgHAFgFQAhgnAoghQAYgSAZgPQAxgdA0gaIAFgEIgBgDQgBgHgEgFQgDgEgEgDQgUgBgRALIg+AnQgbARgZAUQgbAWgZAZQgcAdgfAbIgDABIgsgHIhMgPQgGgBgEgEQgNgJADgOIAHgrIAYgUQAOgNAPgKQAegUAigPQAhgPAjgLQBEgVBHgGQAbABAbAFQAbACAcAGQAOAEAGAOIgKARIgYAjIgkA6QgeA0gbA0QgIAQgKANQgDAFgFADIgrgQgAAGh3IgCgDQgHgWATgNIALAJQAEAGgEAGIgMAOQgCADgEAAIgDAAg");
    this.shape_4.setTransform(27.8269, 6.925);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#884B4B").s().p("AgQCVQgagIgWgUIACgMQAGgSAIgRIATgsIAUgrIAghIQAQgiATgeIAHADQgSA0gOA4IgHAlIgIAjIgGAkIgKBFQgBAGgFAFIgDAAIgJgBg");
    this.shape_5.setTransform(-10.85, 11.2313);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#874F4F").s().p("AC3IXQgIgMgHgNQgFgLgEgLIgDgLIgGgkIgFglQgCgSgBgSIgBgmIAAgZIABgZIACgZQAFg1AIgyQAPhiAchdQAZhPAmhJIAHgPQACgGABgGIgGgEIgKABIgFAEIgDADIgRAbIgSAbIgRAZIgSAZIgUAeIgUAgQgWAlgUAmIgsBbIgVAtQgfBGgIBLIgIBBIgBAOIgNAAIgDgLQgFgUgDgVQgCgSAAgSQAAgXABgYIAEglIAGgjIAGglIAGgcIAHgcIAMgwQAKgmARgiQAghEAqhAQAuhDA7g2IAtgrIAFgFIgBgDQgDgGgHgCQgFAAgFACQgFAAgDADIgOAMIgOANIgWATIgVAUIgdAdIgdAdIgcAdIgbAeQgdAggYAkQgnA4gjA4QgiA3geA4IgUAiIgHAJIgKgEIgCgFIAAgHIAGgjIAJgjIAKgiIALgiIAOgfQAWgyAbgtQAyhVBChHQAwgyA+giQAFgDAEgFQADgGACgGIgJgMIgMADQgMAHgMAKQgRAPgTAJIg4AfQgcAQgcARQgeASgdAVQgcAVgbAXIhEA8QgBgFABgEQADgOAHgNIANgXIAMgSIANgRIAcggIAOgOIAPgMIAkgcIAlgZQAlgZAlgWQBjg5BsglQAYgHAYgEIAwgMQAKgDAKgGQAFgDAEgGIANgUIAxgIQAngBAnAAQAnABAnADIAUACIACALQABAQgCARQAAAHgEAHQgNAYgPAYIgOAZQgXArgUAwIgaA/QgFALgDAMQgJAlgFAmQgFAjAAAjQAAAOADAOIADANIAFAMIAKAWIACAGIAAADIgBAKQgEATgGATQgIAZADAbQAEAkAMAjQAOAlAaAbIAcAaQAEAFACAFQACAGABAGIgMAAIhJAMIgNADIg1ABQglAAgkgFQgcgDgagJQgNgEgMgIQgFgEgBgHQgCgKABgKQACgLADgKIAFgXIAFgYIAFgZIAFgZIAGgZIAHgYIAYhKQAUg8AYg6QA7iOBch8QATgaASgaQADgFgBgGQgBgGgEgFQgUARgSATQgUAUgRAXQgdAngYAqIghA7QgRAfgQAgIgPAgIgOAjIgNAjIgQAtIgNAlIgNAsIgJAeIgIAeQgHAegFAfQgUBrgNBqQgIBBAIBCQABAJgBAJQgBAEgDACIgEADIgIgIgAhhEFQgIgIgFgJQgEgGgDgGIAFgHQAFgHADgIQAOglAAgnIABgoIAWACIAAgZQABgUAKgQIAWgkIAgg3QABAIgBAHQgBAMgGAMQgoBXgOBhIgLBLQgCAKgDALIgNACIgFgEgAmGDTQgYgEgWgKQgMgFgKgHIgfgUIgcgUIgygjIgagRQgJgGADgKIAMgGIAiAHIAaADIArAGIArAEIBeABQAeACAegBQAeAAAdgCIAOAAIADAEIgBAZQACAhgJAfQgDALgKACQg3ALg5AHQgXgBgXgDg");
    this.shape_6.setTransform(-21.366, -0.1083);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_6}, {t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-84.9, -57.8, 169.8, 115.69999999999999);


  (lib.Symbol14 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#391200").s().p("AMaKcQgJgEgHgFQgKgIgIgLQgSgWgPgXQgPgYgMgZQgUgogSgqQgQglgNgmQgPgsgLgsQgLgtgJgtQgFgYgHgXQgCgHgEgHQgFAKgDAMQgEAKAAAMIgDBWIgCBVQgCBGAJBHQAEAhgQAaQgFAIgIAHIgNAAQgJgHgHgIQgIgIgFgKQgohIgNhSQgIg1gDg4QgFhYAVhXQABgDgBgEIgNAAIgHARIgLAmQgLAogMAoQgTA+gIBAQgCAOgEAOQgGAQgIAOQgFAAgDgBQgFAAgEgDQgLgNgGgQQgUgzABg1QABgmAEglQAAgJgIgGIgbBZIgPAEQgHgGgEgGQgKgQgEgSQgEgSgBgUIgBgcQAAgLgJgHIgagXQgUgSAGgcIAKgqQAGgUgFgWQgFgXgIgXIgNgiIgHA0QgIBAAFBCQAHBjATBgQASBbAdBZQAKAdAMAcIAYA7QAEALACALQABAGgBAGQgCAKgLAHQgOAIgKgHQgOgHgLgLIgRgQIgPgSIgOgTQgOgXgNgWQgOgZgMgZIgag4QgMgdgKgdQgMgggKghQgKgjgIghQgIghgDghQgDgdgBgeIgCg1QgEgtABgsQgLAtgHAwQgHAvgDAwQgFBEADBFQACAcADAeIARB8IACAhIgNAJQgOAFgOgIQgRgLgJgSIgSgmIgTgwQgOgpgJgrQgWhwAChzIAAhiIgLAJQgHAHgBAKIgVCmQgEAhgCAjQgCAggEAhQgDAUgGAWQgBAGgFAEIgIACQgIAAgGgIQgYghgNgnQgLgkgDgnQgCgdAAgdIAAgtIgBgRIgGgBQgEAFgDAFQgDAJgCAJIgIAmIgDAPQAAAEgDAEQgGAIgLgEQgMgEgFgLQgEgKgCgMQgEgegBgfQgEhOAUhMIANg4QgMAOgJAPQgJAPgHARIgBADIgIAHIgIgBQgFgCgEgEQgGgFgEgHIgWgrIgSAFIgGgEIgEgEQgFgIgEgHIgBgGIAdgWQATgOAOgUQAVggAPgkIAUgvIgCgEQgBgDgDgCIgZgOIg0AUQgRAHgRABQgOgDgOADQgMgBgMACQgNADgMABQgXgEgUgKIg8gfQhwg6hShgIgFgGQAAgBAAAAQAAAAAAgBQAAAAAAgBQAAAAABgBQABgDADgEQAMgKAOgFIA3AMQBKATBNAEQAdAEAdgBQBIgEBIACQAogGApgIQApgHAogKQBagWBYggQAtgQAtgOQAsgPAsgMQBJgSBLgJQAvgGAugCIATABIATAEIATAEIA6ARIBaAcQAqANAqAPIAgAKIAxAMQAcAHAcAIQAIADAHAFQAGAGAFAHIAUAZQAHAJALAHQAWAMAHAZQAJAkgZAYIgLALIgLgFQgJgEgIgGQgJgFgIABQgbAFgZANQgeAQggAKIgsARQgGABgGAEIADANQAHATANAVQASAdAPAeQAeA3AAA/IgBCDQAAAlABAlQAGCCAcB+QAVBcAjBVQAXA3AVA3QACAGgDAIQgCAEgFADIgGACIgJACQgKgBgJgBgAJPgJIAGBiQAGBCAQBCIAgCAQAPA9AbA5IAsBfQAMAZARATIAcAgIAHgTIgFgLQgVgsgTgsQgOgigLghQgNgpgJgqQgYhugJhyQgDgpgCgpIgDhsQAAgSgCgSQgYAGgYAIQgJACgIgDQgOgEgMgGQgQgKgOgMQgQgNgNgPIgagaQgMgEgKAJQgFAFgGADQgGADgHABQgVgDgSgOQgdgXgSgfQgIgOgLgJQgFgEgGgCQgQgBgPAHQgKAFgLgBQgVAFgWACIgwAAQgLADgIAHQgHAGgCAKQgFAYgBAXIgBAiQAWAPASAVQAGALAKAFIAGADIAMgOIAOgTQAFgGAIgDQAJgBAGAGIABAFQghA1gSA7QgIAXgGAYIgGAhIAMAGQANgMAGgPQANgjARghQAUgmAYglQAOgWATgLIAGAGQABANgGAMQgpBGgcBNQgWA/ABBBQALgGAEgLQAYhDAkhAIA/hqQADgGAGgBIAGgCQADAFACAGQACAFgBAHQgCANgGALQggA3gOA9QgRBCgIBCQgHAvAIAuQADANAIANQAJhxAnhrQAmhqBAheQALAQgEARQgEAPgFAOQgJAdgDAcQgMB6ACB5QAAAaADAaQAEAsAKArIASBIQADAMALAGIANgEQAEgDgBgHIgFgyQgJhGAHhFIAHhCQAFg9AKg9IAShsQAEgZAJgXIAGgLQAKAGABAJgAAhjGQAGAFgCAHIgFAXQgCAQgBARQgDAgAAAhQAAApACAoIADA9IADAoQAEApAGAqQASCEA5B3QASAjASAhQAIARALAPQAPAWARAUQAKAKALAKIALgHQggg5gVg+QgOgpgMgoIgPg5IgJgnIgKgzQgFgigCgiIgEg8QgCg4gBg2QgBhqAThpQAQhRARhQQAHgbADgcQACgOgEgOQgEgNgHgMQgHgMgEgMQgFgNgBgMQgFgnAUghQADgFAIgDIANABQAFAHgDAIIgHASQgFAMgCAOQgCANADANQADAMAIAMQACAEAFACIAJACQAGgJAAgLQAAgaAGgZQAEgVAKgTQAFgLAIgLQADgEABgGQABgEAAgGIgBgDIgHgEQgNgBgPAFIgoAKIg/ANIg+AQIg9ASIhHAZQgjAMgkAKIgyAOQgVAEgSANIgKAJQgIAKgKAJIg1AxQgHAHgGAJQgIAOgHAPQgEAKAAALQAAAFADAFIAIAGQALAAAFgKQAMgVAQgVQAQgXAUgVQAVgVAXgUQAJgIALgHQACgCADAAIAGALQgSASgRAVQgSAWgQAXIgjAxQgQAXgOAaQgUAigPAlQgMAbgIAdQgEANgBAOIgDAdIAJgCQATgdAPgcQAWgpAcgiIA3hDIAhgnQAJgKALgGIAIgGQACAPgIALIgYAjQgTAcgRAeQgRAdgNAfQgNAcgKAeQgMAlgKAnIgPBDQgIAlAAAnIAAAbIAKAEIAEgCQAKgLACgQQACgOAFgOIAVg5IAUg2IAUg3IAUg0QATgvAUguQAEgIAGgIIAGgBQAPAHgGAQIgDAJIgVAuIgSAvIgQAzIgMAkQgOAsgDAtIgIB9QgEBAAQA/QAEANAHANQADAGAGAFIAFADIADgEQgMhnAShmIAKg2QAFgbAHgaQAKgiALggQAMglAOgjIAZg+QAIgSAKgPQACgCAEgBQAAAAAAAAQABAAAAAAQABAAAAAAQABABAAAAIAEACIABADQgMAegFAjQgFAcgCAcQgDAogBAoIgBBCIABBJIADBDQACAzAIAxQAPBmAvBYQAGALAHALQAGgOgEgPIgMgoQgFgUgCgTQgFgjgDgjQgCgmAAgmQgBgpABgpIADg1QAEgvALguQANg5AQg6IAThGQAGgUAUgJgAHzi+QAFAEADAEIAUAgQATAgAhAQQAiASAmgHQATgCAFgSQADgNgEgPQgDgOgFgNQgMgfgQgeQgPgagQgaQgNgVgUgNQgdAJgdANQgSAJgSAFQgmAMgqAJQgPADgQADQAAASAIATQAHAOAKAIQATAPAWAKQAHAEAIgCQAHgCAGgFIAQgRQAEgFAGAAQAFAAAFADgADCmEQgHAcgEAeQgCATgGAUIgHAZIAAAFQARANAZgBIA9gLIAggFQglgSgVgiQgVgfgQgkIgFgGgAEhp2QgNAIgGAPIgPAgIgKAUQgHAQgEAQQgFAagDAaQgBALgEAKQgDAHgBAJQAAAEACAEIAMAbIAQAnQARAnAjAYQAaAQAeAIIBVgZIAugOIAXgGIBLgeQARgGAHgPQADgHgBgHIgCgUQAAgHADgGIANgbQAHgMgGgOQgFgMgJgNIgQgWIgKgOQAbgIAcAJQATAHATAIQAhARAkgEQANgEANgEQghgWgngJQglgGglgFQgcgDgWgNQgKgEgGgJIgmgOIgugNIgugJQg7gNg+gHQgMAAgJAGgAsAnGQAHAIAJAIQAJAFAIAGQAKAHAJAJQAQAPAUAJIAbANIApATQAVAIAWAGQAYAIAaAAQASAEAUgDIA2gLIAbgDIAFgQIAlg4QAPgWASgUQAFgGAEgGIAGgNIg5APQgfAJgigBIiEAAIhRAAIgcgBQgggFgfgIQgegFgcgIQAMASAOARgAKznLQgoAKgPAoIAPAAQAlgYAtgDIAngFQANACAPAEIAQAIQAHAEAIgEQAFgHgCgHIgBgEQgPgVgZgDIgWgBQgpAAgnALg");
    this.shape.setTransform(-0.0025, 0);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFC600").s().p("AhiAeQAOgnAogKQAxgNA0ADQAaADAOAWIACAEQABAFgFAHQgHAEgIgEIgQgHQgPgFgMgBIgmAEQguADgkAZg");
    this.shape_1.setTransform(73.5173, -44.011);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#592626").s().p("AEiG8QgRgTgLgZIgthfQgbg5gPg9IgfiAQgRhBgGhCIgGhjQgBgJgKgGIgFALQgKAYgEAZIgSBsQgKA8gFA9IgGBCQgIBFAJBGIAGAyQAAAHgEADIgMAEQgLgGgEgMIgShIQgJgrgEgsQgCgagBgaQgBh5AMh5QABgcAKgdQAEgPAEgPQAEgRgLgQQg/BfgmBqQgmBqgKBxQgIgNgDgNQgHguAGgvQAIhCARhBQAPg9Afg4QAGgLACgNQABgHgBgFQgCgGgEgFIgGACQgGABgDAGIg+BrQglBAgYBCQgEALgLAGQgBhAAXg/QAbhNAphHQAGgMgBgNIgGgGQgTALgOAWQgXAlgUAnQgSAhgNAjQgGAPgNAMIgLgGIAFghQAHgYAHgYQATg7Agg1IAAgFQgGgGgJABQgJADgFAGIgNATIgMAOIgHgDQgJgFgHgLQgSgVgVgPIABgiQABgXAEgYQACgKAHgGQAIgHALgDIAxAAQAVgCAWgFQAKABAKgFQAQgHAPABQAGACAFAEQALAJAJAOQARAfAdAXQASAOAVADQAHgBAHgDQAFgDAFgFQAJgJAMAEIAaAaQAOAPAPANQAPAMAQAKQAMAGANAEQAIADAJgCQAZgIAXgGQACASAAASIAEBtQABApADApQAJBxAYBuQAJAqANApQALAhAOAiQATAsAVAsIAFALIgHATIgcgggABnkIQghgQgTggIgUggQgDgEgFgEQgLgIgJAKIgPARQgGAFgHACQgIACgHgEQgWgKgTgPQgKgIgGgOQgJgTAAgSQAQgDAPgDQAqgJAlgMQASgFASgJQAdgNAdgJQAVANAMAVQARAaAOAaQAQAeAMAfQAFANAEAOQADAPgDANQgEASgUACQgKACgLAAQgbAAgYgNg");
    this.shape_2.setTransform(47.55, 16.2);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#884B4B").s().p("AhCA6IAAgFIAHgZQAGgTACgTQAEgeAHgbIAJgDIAFAGQAQAkAUAfQAVAhAlASIggAFIg8ALIgEABQgWAAgQgNg");
    this.shape_3.setTransform(23.4875, -32.0464);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#E2E0D9").s().p("AjDCeQgjgYgRgnIgQgnIgMgbQgCgEAAgEQABgJADgHQAEgJABgLQADgbAFgZQAEgRAHgQIAKgUIAPggQAGgPANgHQAJgHAMAAQA+AIA7ANIAuAIIAtAOIAmANQAGAJAKAFQAWAMAcAEQAlAEAlAHQAnAIAhAWQgNAFgNADQgkAEghgQQgTgJgTgHQgcgIgbAHIAKAPIAQAVQAJANAFANQAGANgHAMIgNAbQgDAFAAAHIACAUQABAIgDAHQgHAOgRAGIhLAfIgWAFIguAPIhVAZQgegIgagQg");
    this.shape_4.setTransform(49.495, -45.55);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#874F4F").s().p("AHjJaQgRgUgPgWQgLgQgIgQQgSghgSgkQg5h2gSiEQgGgqgEgpIgDgpIgDg8QgCgoAAgpQAAghADghQABgQACgQIAFgYQACgHgGgEIgEgCQgUAIgGAVIgUBGQgQA6gNA5QgLAugEAvIgDA1QgBApABApQAAAmACAmQADAiAFAjQACAUAFATIAMApQAEAPgGAOQgHgLgGgLQgvhZgPhlQgIgxgCgzIgDhDIgBhJIABhCQABgoADgoQACgdAFgcQAFgiAMgeIgBgDIgEgCQAAAAAAgBQgBAAAAAAQgBAAAAAAQgBgBAAAAQgEABgCADQgKAPgIASIgZA+QgOAjgMAkQgLAhgKAiQgHAagFAbIgKA2QgSBmAMBmIgDAFIgFgEQgGgEgDgGQgHgNgEgOQgQg+AEhBIAIh8QADgtAOgsIAMglIAQgyIASgvIAVgvIADgIQAGgQgPgHIgGABQgGAHgEAJQgUAugTAvIgUA0IgTA3IgUA2IgVA5QgFAOgCAOQgCAQgKALIgEACIgKgEIAAgcQAAgmAIglIAPhDQAKgnAMglQAKgeANgdQAMgeARgdQARgeATgcIAYgjQAIgLgCgPIgIAFQgLAHgJAKIghAmIg2BEQgcAigWAoQgPAdgTAdIgJACIADgeQABgNAEgNQAIgdAMgcQAPgkAUgiQAOgaAQgXIAigxQAQgXASgWQARgVASgTIgGgKQgDAAgCACQgLAGgJAJQgXATgVAWQgTAVgQAXQgQAUgMAWQgFAKgLAAIgIgHQgDgEAAgFQAAgLAEgKQAHgPAIgOQAGgJAHgHIA1gxQAJgJAIgKIAKgJQASgOAVgDIAygOQAkgKAjgMIBHgZIA+gSIA+gQIA/gNIAogLQAPgEANABIAHAEIABADQAAAFgBAFQgBAFgDAFQgIAKgFALQgKAUgEAVQgGAZAAAaQAAALgGAJIgJgCQgFgCgCgEQgIgMgDgNQgDgNACgNQACgNAFgMIAHgSQADgJgFgGIgNgBQgIACgDAGQgUAhAFAmQABANAFANQAEAMAHAMQAHAMAEANQAEANgCAPQgDAcgHAbQgRBQgQBRQgTBpABBqQABA2ACA4IAEA8QACAiAFAiIAKAzIAJAnIAPA5QAMAoAOApQAVA+AgA5IgLAHQgLgKgKgKgAjylEQgagBgYgHQgWgGgVgIIgpgTIgbgNQgUgJgQgQQgJgJgKgGQgIgHgJgFQgJgHgHgIQgOgRgMgSQAcAIAeAFQAfAHAgAFIAcACIBRAAICEAAQAiAAAfgIIA5gPIgGANQgEAGgFAGQgSAUgPAWIglA4IgFAQIgbADIg2ALIgSABQgKAAgKgCg");
    this.shape_5.setTransform(-27.7417, -0.8937);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-86, -67, 172, 134);


  (lib.Symbol13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#391200").s().p("AlyM6IgMgFQgMgIgFgKIgBgDQAAgGADgDQAKgKALgHIAXgSQAQgMANgTIAWghIgEgHIgFgBIgOANIgOASQgNAQgPAOQgTAQgUAOQgTANgWAKQgVAKgXAGQgMABgLgFQgKgFgKgHQABgHAEgFQAKgMALgJIAagSQAXgSARgXIAOgUQAGgMgIgJIgrASIgHgCIgBgBIgCgCIgHgGIgDgFIAAgGQAOgMAQgLQAVgPAWgLIAvgbQAIgEAEgIQAGgGACgIIABgDIgEgFIgOgGQgKABgKAEIg5AMQgrAKgsAEQgfAGgeACQgTgDgTgFQgYgHgWgKQgfgPgegRQgWgPgVgQQgjgcgZgiQgHgKgGgLQgFgMgDgNQgDgRgBgSQgBgFABgEIAEgEQAIAAAIADQAJABAIAEIAZAKIA7ASIAnALIAOAEIAFAAIADgKIgEgHIgHgFIgcgMQgUgIgMgOIgUgcQgTgegdgVIgugjQgRgPgMgSQgNgVgJgXQgDgGADgHQADgGAGgEQAGgDAGgBIASADIADABIACAAIAEgJQgPgOgPgQQgQgSgQgTQgNgSgMgSQgKgQgGgRQgIgSgDgTQgEgOACgOQABgJADgHQAHgPAQAAQARADALALQAVAUAWATIAbAXIggg2IgnhEQgNgYgLgYQgSglgHgkQgHgmAAglQAAgZAGgZIAAAAQAEgSAPgKQAGABAGACQAFACAEAEIAFAFQAKAMAKANQAKAPAKARIAbAsQATAfAWAdQATAbAWAaQAeAkAjAdIgCgLQgBgGgDgGIgzhPQgRgbgMghQgHgYgGgZQgMgxgDgzQgBgmACgmQAGhCAcg+QANgdAZgNQAGgDAIAAIAFAPQAHAaAEAbQAEASABAUIADAfIAHAhIAJAmQAJAhAKAgQARAyAZAwQATAjAYAgIADABIAGAAIAFgDIgQgkQgIgTgFgUQgHgdgEgdQgKg3gCg6IAAgDQgCgvAFgvQADgiAIgiQAIghANggQAHgUAKgSQAOgaAagPQADgCAEABQAHADAGAGQAFAGACAJQADAMABANIAFA5IADA+QABAwAKAuIAFAWIAOA+IAGAZIARAyQAQAzAaAwQAOAbAQAcIAQAZIAVAdIAUAaQAIALAKAKQAZAaAaAZQAbAbAeAaQAwArAxAmQAOAMAMAPQALAOAIASQAMAaAIAcIAUBGIALAjIAGAEIAkgJQAegIAfgEQAcgHAcgDIBogNQAQgBAPgKQAhgUAbgYQAWgTAYgRIAegUQAEgEACgFQACgIgBgJIgEgoQgCgPAEgPQAGgTALgRQAFgHAIgCQAIgDAHABIAaAKIAMAFIgJgtQgHgigFgjQgCgIAFgGQAIgMALgHQAHgDAGACQAMAEALAGQAlAXAWAlQAEAHAHAGIgEgeIgIgrIgLg5QgFgjAAgkQABgIAEgGQAGgGAHgEQAOgGANAHQAHAEAGAGQAWAUANAbIAbA4IALATIADgKIACgLIgCgjQgEguAEgwIAHg3IAEghQACgQANgKQAMgJAPACQASACAIAOQAKAPAJARQAHAQAGARIARA6IAJAgIAHgJQACgEAAgFIAFg5QAEg4ALg1QAIgdAMgcQAGgPAMgMQACgDAEgCQAJAHAJAIIAMAMIAEAGQAJAOAGAQQAHAYAEAZQAGAlABAlIABAfIgBASQgGBOgRBMQgOBFgTBCQgMAqgQAqQgKAggOAfQgOAfgRAdQgOAZgQAYQgEAIgIAFQgIAHgJAEIg0AdIgpAXIgXANIABAFIAdAKQARAFARAGQARAIAPAMQAJAIAHALIAVAYIATAWQAOAPAMARQAIAMgJANQgSAagVAZIgCAEIgHgCIgLgNQgFgFgHgCQgGgCgHAAIhEASIg7AUQgeAMgcANIg+AcQgoAQgqAKIhRARQgrAKgqAEQgkAEglAAQgrgHgpgJQgogFgngJIg/gRIgsgLQghgHggAJQgLACgJAFQgZAMgQAWIgGAIQAEAMADAMQACAJgBAJQgBAJgEAIQgGAJgHAIIgcAcQgQAOgRALQgTANgVALQgVAKgUAIQgEACgEAAQgGAAgFgDgAkTKeQAEATgJAQQgGAMgIALQgVAbgZAVQgKAJgFALQAhgHAagTIAdgWQAOgLAMgNIAMgOQALgRgHgRQgfgIgbgUIgJgHQANANAEAQgAmNKMIgDAGQAHANgFAQQgCAHgDAGQgKAQgOAOQgKAJgLAIIggAYIgBAFIAEAHQAegKAagSQAigYAcgdQALgNACgQIgDgIQgCgEgEgCQgQgLgTAAgAlBJrIgHAJIAAAFIANAGIAFgFIANgRIAFgGIgDgKQgRAEgJAOgAgbI8IAtASQAfANAgAFIBWAPQAxAGAxAAIAkAAIAFgCIAEgLQgcgMgWgWQgUgVgNgZQgKgTgIgTQgGgMAAgOQgBgbABgcQACguAQgrQAJgUALgSIAHgMQAFgIgEgIIgFgFQgUgBgRAEQgQADgQABQhDACg+ANIgwALIgEADIABAJQALArAZAnQARAaAdALIAQgEQATAFgJAUQgKASgTgEQgNgDgMgHQgPgJgMgNQgNgNgJgQQgKgQgHgRQgMgggLghQgPgygLg2QgIgjgKghQgFgPgGgOQgMgWgTgRIglgfIg1gpIgsglQgOgMgQgLQgEgCgEgBQgdAEgOAbQgOAagCAeQgDAiANAhQADALAHAJIAMARQANAQADATIACAOQgYgCgOgWQgFgJgIgFQgWgOgcgBQgpgEgfAaQgWASgGAbQgHAcAIAbQALAmAZAfQAWAdAfAWQAPALAVAFIAPAFQAGAAAEAEQAEADACAFQACAFAAAFIAAADIgiAJIgEADQACAKAGADQAPAJARAIQArAVAtAOQALADAMACQAmgFAoAHIBAAEIAgAHIASgCQgsgkgYg1QglhRgQhXIAJgLQARACADARIAOA4QAMAtATApQAKATALASQASAhAdAXQAgAbAqAEQA0AEAqgdQAMADgCAMQgBAHgFAGQgKAPgRAKQgUANgZACQgaAAgbgGQgggFghgHQhFgRhGgJQgvgCgsAAQgMAAgLgEQgJgBgHgCQgZANgLAdIAiAJQgBAVgPAQIAKgBIAOgDIAUgHIAUACIAFgDIAEgdQAZgIAaAFQAQAFARAAIAagEQAQAKAMAMQAMAMARAAQAOgCAOgEQAIgDAHgBQAJACAJAEgAE2ECQgKAKgHAMIgBADIgWAMQgFAWgIAUIgLAgQgEAKABAKIADAeQAAAIgCAIIgIASQgEAJAEAJIAJASQALAYAOAXQAMATAPASQAQAUAaALQALAGALgBQAdAAAdgIIBHgWQAigNAjgOQAZgJAUgOIgGgYQgDgLABgLQAAgLAEgKQAIgQAJgOIgBgFQgSgQgQgTQgIgKgFgLQgFgJgCgLQgDgLAAgMQAAgIACgIQglgRgkgLQgkgLglgKIgygKIgjgGQgNACgHAJgAtFGFQAJASAPARQAnAqAwAjQAYARAbANQAPAIARAGIAZAHQARABARgEQALABAJgFQAMgEALgKIALgJQALAEACAMIABAFQAJAHAKgFQAZgOAdgDQAKgBAJgDQAFgCADgFIAEgGQAAgHgGgCIgZgDIg5gIQgngFglgNQgfgMgdgRQgTgLgVgHQgSgGgUgEQglgHgngLIgqgQgAm1GdQACAOAIAMQADAGAGAEQAcAYAkAJIAIgJQgEgJgIgEQgbgHgSgVQgTgYgDghQgfgZgageQgfgigOgsQgEgNgCgNQgGgnATgkQAGgNAJgLQANgQATgMQAqgZAsAPIANgEIABgEQgGgYgDgaQgCgTADgRQAEgSAIgQQAJgRANgPQAKgNAMgMIAAgFQgSgWgVgbQgSgXgQgZQgyhPgkhWQgPglgKglQgMgngHgoIgGgeQgShogBhqIAAgMIgJgJIgHACQghBPgLBWQgGAtAEAtIAAAJIAGA0IAGA4QAFAkAKAjQANApAQAoQAPAkATAkIATAkIgJAHQgGABgFgFQg2gzgzg3Qg0g5gdhHQgVgzgMg2QgOg1gEg2QgCgcgEgcQAAgEgCgCIgDgCQgOACgEAPQgRBFAABJIABAgQAFBxA1BlQApBOBAA7IAXAZQADADAAAFQAAAKgHAHIgFAAQgggagngRQgWgJgTgNQg+gpgug5QghgogagrQgOgYgKgXIgNgcIgOABIgFAHIAIAuQAFAcAFAcQAKAvAVAsQANAbAQAZQANAVAQAUQAbAhAeAdQATASAWAOIAhAUIAKAHQgBATgRAGIgEACIgLgBQgrgNgngYQghgTgggYQghgYgbgcQgEAXAJAWQANAgAWAbQAaAhAcAeQAZAZAdAVQAbAUAeAMQARAHgCAQIgEACQgYAIgZgEQgJgBgIgEIgngTQgRgKgUAAQAjAvAzAbQATAJAVAGIAoANIADARIgBADQgEACgEAAIgOAGQgFABgFADQACAFADAEIATAVQAuArA9AMIAoADQAOABALAGIAFAOIgGALIgVAGIgCAJQAVAUAdAGIBMALQADgKgJgJQgSgQAKgTQADgEAEgDIAKgDQAIAEACAHgALfHEQAuAFApAXIACABIAFgQQgZgjgrABIhBAAQgVAEgSANQgJAGgHAIQgDADgCAEIAHAFQArgWAvAAIACAAgAK9F/IAOAFIAIgDIADgNQgNgHgNgFIgegHIgpgBIBIAfgAGRDgIA7AVQAjALAlAOQAcAJAcALQATAHAVAFQAGABAHgDQANgEANgIIAtgbIAwgdQAigUAVgiQAUggAQgkQALgXAIgYQALgfAJggQALgkAKgmIARhKIAKg0IAJg3QAEgdACgdIADgbIACgzQACgigFghQgDgQgFgQQgFgRgRgCQgIALgDAOIgHA9IgHA8IgHBAQgNBhgZBfQgHAbgKAbQgEAKgGAJIgNgBQgJgKADgRQAGgiAFgjQACgYAAgYQgBgngEgnQgFgwgPgtQgJgggNgfIgJgCIgHAGQgLBAAABBIgBA6IgDA2IgDBFIgEA2QgCAWgKATQgHAEgGgDQgFgCgBgEQgEgGgCgHQgBgLAAgLQAAgggDggQgDgVgFgUQgFgNgGgNIgXgxQgFgNgGgMIgSgbIgJgDIgFAFIAEAbQAIA/ALBAQADANgBANIgCAjQgBAIABAJIAJBPQABAHgCAHQgCAGgEAFIgDADQgPgBgDgPQgJghgKggQgIgWgKgUQgOgagRgXIgWgeIgNgDIgFAJIAMAxQAHAXADAZIAHAdQAEANAGAOIAVAxIAEALIgLAKQgLAAgEgKQgMgUgNgTQgNgRgPgQQgGgGgIgEQgLgGgMACQgHAFAAAHQgBAbAFAaQADARAHASQADAJABAKQACAOAAAPIgGAHIgWgNQgMgGgNAGQgTAHgRAMIgrAcQgOAKgPAHQgRAKgRAIIA5gGIAMgBQAeAAAdALg");
    this.shape.setTransform(0, -0.0016);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#A6654D").s().p("ADjJbQgGgFgDgFQgIgMgCgOQgCgIgIgDIgKADQgEACgDAFQgKASASAQQAJAJgDALIhMgMQgdgFgVgVIACgJIAVgGIAGgKIgFgPQgLgGgOgBIgogDQg8gLgugsIgTgUQgDgFgCgEQAFgEAFgBIAOgFQAEgBAEgCIABgDIgDgRIgogMQgVgHgTgJQgzgbgjguQAUAAARAJIAnATQAIAFAJAAQAZAEAYgIIAEgCQACgQgRgHQgegMgbgTQgdgVgZgZQgcgfgaghQgWgcgNggQgJgVAEgYQAbAcAhAZQAgAYAhATQAnAYArAOIALABIAEgCQARgHABgSIgKgIIghgUQgWgPgTgSQgegcgbgiQgQgTgNgWQgQgYgNgbQgVgsgKgvQgFgcgFgbIgIguIAFgIIAOgBIANAcQAKAYAOAXQAaArAhAoQAuA5A+AoQATAOAWAJQAnAQAfAaIAFAAQAHgHAAgKQAAgEgDgDIgXgZQg/g7gphNQg1hmgFhwIgBghQAAhIARhGQAEgPAOgCIADACQACACAAAEQAEAcACAcQAEA3AOA1QAMA1AVAzQAdBHAzA5QAzA3A2AyQAFAGAGgCIAJgGIgTgkQgTgjgPgkQgQgpgNgpQgKgjgFgkIgGg4IgGg0IAAgJQgEgtAGgtQALhWAhhPIAHgCIAJAJIAAAMQABBqASBoIAGAeQAHAoAMAnQAKAlAPAlQAkBWAyBQQAQAYASAXQAVAaASAWIAAAFQgMANgKANQgNAOgJARQgIARgEARQgDATACASQADAaAGAYIgBAEIgNAEQgsgPgqAaQgTALgNAQQgJAMgGANQgTAkAGAmQACANAEANQAOAtAfAiQAaAeAfAZQADAgATAYQASAWAbAGQAIAEAEAKIgIAIQgkgJgcgXg");
    this.shape_1.setTransform(-64.6, -15.35);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#874F4F").s().p("AGvFcIhWgPQgggFgfgNIgugSQgJgEgJgCQgIABgHADQgOAEgOACQgRAAgMgMQgMgMgQgKIgaAEQgRAAgRgFQgZgFgZAIIgEAdIgEADIgVgCIgUAHIgNADIgKABQAOgQACgVIgjgJQAMgdAYgNQAIACAJABQALAEAMAAQArAAAvACQBGAJBFARQAhAHAhAFQAbAGAZAAQAZgCAVgNQARgKAKgPQAFgGABgHQACgMgNgDQgpAdg0gEQgqgEghgbQgdgXgSghQgLgSgKgTQgUgpgMgtIgNg3QgEgRgQgCIgJALQAQBWAkBRQAYA1AtAkIgSACIgggHIhAgEQgogHgmAFQgMgCgLgDQgsgOgsgVQgQgIgPgJQgGgDgCgKIAEgDIAhgJIABgDQAAgFgCgFQgCgFgEgDQgFgEgGAAIgPgFQgUgFgQgLQgegWgWgdQgZgegLgmQgIgbAGgcQAHgbAWgSQAfgaApAEQAcABAVAOQAJAFAFAJQAOAWAYACIgCgOQgDgTgNgQIgMgRQgHgJgEgLQgMghACgiQADgfAOgaQAOgbAdgEQAEABAEACQAPALAOAMIAsAlIA1AqIAlAfQATARAMAWQAGAOAFAPQAKAhAIAjQALA2APAxQAKAhANAgQAHARAKAQQAKAQANANQAMANAPAJQALAHANADQAUAEAJgSQAKgUgUgFIgPAEQgdgLgRgaQgZgngMgrIgCgJIAFgDIAwgLQA/gMBCgCQARgBAQgDQARgEATABIAGAFQAEAIgFAHIgHAMQgMASgIAUQgRArgCAuQgBAcABAbQABAOAFAMQAJATAKATQANAZAUAVQAWAWAcAMIgEALIgFACIglAAQgwAAgxgGgAljEVIgYgHQgRgGgPgIQgbgNgYgRQgwgjgngqQgPgRgJgSIAFgHIAqAQQAmALAmAHQATAEATAGQAVAHASALQAdARAgAMQAlANAmAFIA6AIIAYADQAHACAAAHIgEAGQgDAFgFACQgJADgKABQgdADgZAOQgLAFgIgHIgBgFQgDgMgKgEIgLAJQgLAKgMAEQgKAFgKgBQgPADgPAAIgFAAg");
    this.shape_2.setTransform(-26.375, 27.475);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#E2E0D9").s().p("AiMCwQgagLgQgUQgPgSgMgTQgOgXgMgYIgIgSQgEgJAEgJIAHgSQADgHAAgIIgDgeQgBgKADgKIAMggQAIgUAFgWIAVgMIACgDQAHgMAJgKQAIgJANgCIAjAGIAyAKQAkAKAjALQAlALAlARQgDAIAAAIQABAMACALQADALAEAJQAGALAIAKQAQATASAQIABAEQgJAOgIAQQgEAKgBALQAAALACALIAHAYQgUAOgaAJQgiAOgiANIhGAWQgdAIgeAAIgDAAQgJAAgJgFgADdgsIhIgfIApABIAeAHQANAFANAHIgEANIgHADIgOgFg");
    this.shape_3.setTransform(48.025, 42.7821);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#FFC600").s().p("AoBCiQAZgUAUgbQAJgLAGgMQAJgQgFgTQgDgRgOgMIAKAHQAbAUAfAIQAHAQgMASIgMAOQgMANgOALIgcAWQgaATghAHQAFgMAKgJgAqPCiIACgEIAggYQALgIAJgKQAOgNAKgQQAEgHACgHQAEgPgGgNIADgHIAHgEQATAAAQAMQADABADAEIADAIQgCARgLAMQgdAeghAXQgaATgeAJgAKIiEQgpgXgtgFQgxgBgrAWIgHgFQACgEACgCQAIgIAJgHQASgMAVgEIBBgBQArAAAZAiIgFAQIgDAAg");
    this.shape_4.setTransform(17.325, 61.3246);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#592626").s().p("AAMGWQgTgFgUgHQgcgLgcgJQgkgOgkgLIg7gVQgigNgkADIg6AGQARgIARgKQAQgHANgKIArgcQARgMATgHQAOgGALAGIAWANIAGgHQAAgPgCgOQgBgKgDgJQgHgSgDgRQgFgaABgbQAAgHAHgGQAMgCALAGQAIAFAHAGQAPAQAMARQAOATALAUQAFAKAKAAIALgKIgEgLIgUgxQgHgOgEgOIgGgdQgEgZgGgXIgNgwIAGgJIAMADIAXAeQAQAWAOAaQALAUAHAWQALAhAIAhQACAPAPABIADgDQAEgFACgGQADgHgBgHIgKhQQgBgJABgIIACgjQABgMgDgNQgLhAgHg/IgDgbIADgFIAJADIASAbQAGAMAGANIAWAxQAGANAFANQAGAUACAVQADAfAAAgQAAALACALQABAHAEAGQACAEAEACQAGAEAIgFQAJgTACgWIAEg2IAEhEIACg2IABg6QAAhBALhAIAHgGIAJACQANAfAKAgQAPAtAEAwQAEAnABAnQAAAYgCAYQgEAigGAiQgDARAJAKIAMABQAGgJAEgKQAKgbAHgaQAZhfANhhIAHhAIAHg8IAIg9QACgOAJgLQAQACAFARQAFAQADAQQAGAhgCAiIgDAzIgDAbQgCAdgEAdIgJA3IgKA0IgRBJQgKAmgLAlQgJAggLAfQgIAYgKAXQgRAkgUAgQgVAigiAUIgwAdIgtAbQgNAIgNAEQgEACgEAAIgFAAg");
    this.shape_5.setTransform(61.575, -10.425);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-99.2, -82.8, 198.5, 165.7);


  (lib.Symbol12copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_47();
    this.instance.setTransform(-696.65, -540.25, 0.3801, 0.3801);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true}, 1).wait(184));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-696.6, -540.2, 1394.8000000000002, 1078.9);


  (lib.Symbol12copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#391200").s().p("AnGPDQgKgEgCgOQgBgLAHgLQAHgLAKgKIAjgiQAJgHAEgMIAFgOIgCgEIgEgDIggAPIgIAAQgNgHAAgQQAAgLAGgJQAJgPAOgMQAWgSAeAIQASgGAIgRIAMgVQAEgKgDgIIgQgpQgwAfg0AUQghAMgjAAQgfAAgggDIguAAQgZgDgXgIQgtgOgrgTIgbgOQgcgRgVgWQgNgOgJgQQgIgMgCgNQgCgMACgMQADgLAEgLIAkAFQAiAHAkADQAWAEAXABQCUAICUgDQgpgugUg6QgLghgKgiQgFgQgPgOIgfgeQgLgKgGgOQgIgOgDgPQgEgQgBgQIgBghIAAggQAAgnAbgYIACgGQgDgPgPACQgXgFgVgLQgogUgWgqQgOgagOgdIAPgHQAyABAyAJQAaAEAbAAIAFgJQgFgFgFgCIgdgOQgUgJgSgLQg6gigag8QgFgMAGgJQAJgMAPgHQAMgEAMACQBOAUBLAgIAHgCQgSgYgagSQhohHghh6QgIgfAOgcQAGgNALgIQAMgBAKAGQA3AmAuAxQAUAUAWAUQAiAfAfAkQAEAEAFADIAKgIIgFgMIgegzQgUghgNglQgchMgPhSQgGgigDgjQgGg+AZg5QAEgIAHgFQAHgGAJgDQAHAGAEAIQAMAWAJAXIA5CTIAYA7QAGAOAHAOQgChAgHg9QgFgqABgqQAAgrALgoQAKgkAFgkQAEgiAUgfQAJgPAMgMQADgDAEgCIAMAEQAFADAEAFQAPAUAEAXQADAUgBAVQgDBUAMBUIAOBZQAOBUAXBQQAcBhArBbIA7B6IAYAwQAGANAAAOQgBBUAUBSIAcBqQADAKAJAGQAYgqAhgjIASgRQAHgIAAgIQABghAYgUQBJg5BVgiIAJgDQACgIgIgEIgOgGQgbgNgNgbQgCgFADgFQAEgJAGgHQAGgGAIgFQAYgRAdAEQAaAFAXAQIANgDQg0g9gnhHQgGgKADgJQAEgNALgJQAOgNATAFQAKADAKAFQAQAHAOAJIAsAdIAcAQQgkhTgghXQgGgOgBgOQgBgJADgJQAKgaAdgCQAQADANAJQAgAXAYAeQAbAhAXAlQAHgZgIgYQgNglgEgnQgIhKAAhKQAAgTAFgTQAFgWAOgRIAGgCQALAAAHAJQApAzAVA/QAOAoAJAqQACAJAIAFQAEh2AchyQAHgZAJgYQAGgQALgPQALgOAOgMQAEgEAGgBQARAHAIATQAEAKACALQAEARABARQACAlAAAlQgBCNgSCNIgJBJQgMBngWBnQgKAygSAvQgGAPgKAPQgRAZgSAYQgkAtgeAxIgtBFQgiA0glAzIgfAuQgCADAAAEQAFAEAFACIAvANQAWAHAWALQAeAOAgALQAfANAgAGIAyAOQAVAGAUAKQAPAJAMAMQAGAGADAJIADAPQACAIAIAGIASAQQAHAHgBALIgBAgQgBANgIAKIgHADQgXgDgVADQgTAAgRAGIhcAiQgvAPgtAaQgoAYgtAMQgeAHgeAEQgnAHgoAEQg9AEg6AJIggAEQgkgFgjgMQgbgKgbgFQgTgGgTgCIghABQgZgDgXgKQgQgGgQABQgWACgVAKIhgAqIgDAGIAJAeQABAFgBAFQgCALgFAHIgJAMQgWAbgdAWQgKAIgLAFQgcAOgggIQgEgBgCgDIgNgaQgVAIgTARQgOAMgTAAQgIAAgJgCgAk3M4QgGAMAAARQgBAIgDAIQgJAQgKAQIgQAZQAOAFANgJQAfgXAOgjQADgJgBgJQgBgIgGgGQgFgFgIgBQgDgCgEAAIgCAAgAl2NiIg3A7QANAAALgGQAVgLARgQQASgRAGgWIgHgLIgIAAQgHAOgJAKgAj4MYIgJAFIADAOQAFASgFATQgEAPgHAPIgKAYIAFAGIAGABQAXgTAGgdQAJgnAFgmQgQABgLAHgAkqKFIgJATQgEAMAKAJQAKAKANAIIACAIQgTAXgYAXQgUATgSAVIgIAMIACALQAQADALgMQAQgUATgRQAOgNAPgMQAHgFAIADQAJADAIAGQATgEAOgLQAdgYAZgaQAFgGAIABQARALASAHQAUAJAXAGQAcAIAeAHIAJgHIgKgZQAKABAKAFQANAEAMAIIAUAOQALAIALACIBEAWQAtAOAvgFIAtgBQgZgVgVgaQgIgJgEgLQgHgYgFgXQgHghABgkQADg6AVg0QAJgVAOgRIAggjIiTAOIgIAHQgBAMAHAIIAUAdQAMAUAQAPQAHAGgDAIQgBAEgEACQgFAEgHACQgYgUgRgcIg8hhQggg2gUg7QgZhJgShMQgLgvgFgwIgHhAQgCgTgFgSQgGgUgIgTQgIgVgKgTIgTgnIgagyQgCgEgDgCIgFgBQgaASgMAaQgSAogBAtIgBAqIAAA9IAAATQgPgJgBgSQgBgFgCgEIgJgEQhBAIgbA5QgIARgCATQgEAgADAhQADAkAHAkQAGAkAQAhQAHAPAJAOQAKAOAMANQAMAOAOAMIASAQIAAAGIgJAMQgQACgKgOQgNgRgPgPIgNgNQgOgNgKAOQgEAFAAAHQgBAUADAUQADATAGATQAFAOAIAOQARAeAYAYQAdAfAnAPQAWAIAWgBIAHAQIgWALIgBAHQA6AmBFgJQAPgEAOAAQAQAIgKAQIgEAGIgfAHQgIADgIgCQgPgCgOgEIgtAEIgxAJQgsABgqgRIgggNIgGgBQgEADgDAEgAFQGnQgSAGgQAIQgLAFgGALQgJARgGATQgEAPgHAOQgHARgEATQgIAsACAsQABARAKAPQANAVAPAUQAUAbAaAXQAFAEAGABQAHAEAIABQBJgHBHgWQAigMAhgOQAhgPAfgSQAKgFAIgHIADgDQABgMgIgMQgGgJABgMQABgMAEgLQADgJAEgJQgGgTgQgNQgPgKgRgJQgIgDgBgJIAAgGQAIgJAMABQASADANgNIgygKQgEgNgGgMQgDgGgGgBQgigMgkgGQgjgIgkgGIhGgGQgLABgKADgAiaLeIgeATIgSAMQAJAFAJgDQAUgFAUgGIAUgGQAGgJgJgGQgGgFgIAAQgGAAgHAEgAtMI1IAGARQAhAYAgAZQAUAPAWALQA4AcA8AGIAxACIAZADIArACQAngFAkgRQAVgJAUgMIAcgRIAbgIIACgJIhEg2QgSAGgSABQg1gDg0AHIg1AHQgvgGgtgDIg9gBQgjgEgkgFQgPgEgQgBgAKuJTQgPAIgKANQgGAIABAKIAIAFQAigTApgGQAfgHAgAHIAeADIAFgIQgBgNgLgGQgOgHgRABIgjgFQgnABgiAPgAisJ1QgRgPgTgPQgDgDgBgDQgEgJADgMQgPgQgUgOQgNgHgKgKQgtgtgQg8QgIgeAAghQAAgLAEgLQAGgMAHgLQAFgIgBgJQgHgmgLgkQgNgpgHgrIgEgjQgEg0AfgrQALgOASgLQAQgIAQgHQAMgGAOgCQALgDANABIALgEIAAgHIgEghQgCgOABgNQADgZAHgXQAHgYAMgVQAOgYAQgXIAHgLQAEgIgCgLQgDgQgGgPQgTgwgQg0Qglh1gSh7QgGgtgDgtIgDgzQgBg/ABhAIgEgEIgKAAQgFAHgDAJQgJAagGAbQgGAcgEAdQgIA7ABA9QAABWANBUIAJA7QALBBAXA/QACAHAAAJIgKAEQgMgEgFgOIgRgmQgZg6geg6QgPgdgNgdIgag7IgfhNQgLgbgHgbIgDgLIgDgCQAAgBgBAAQAAAAAAgBQgBAAAAAAQgBAAAAAAIgFgBIgFACQgBAPAAAPQAAAbABAbIAFAxQAJBOAdBKQAVA4AbA2QAWAsAeApQAGAIAEAKIACAKIgPAIIgjgaQgTgOgTgMQgPgKgNgLIgrglQg1gwg2gvQgNgKgOgJQgEgCgEACQgFABgDADQgBAOAJANQAHAIAEALIAHAbQAKAkAZAdIAjAnQAaAcAiAWQAWAOAXANIAeARQAMAHgCAQQgCAJgLACQhRgGhMgbQgpgOgpgLQgGgDgGACQgBAHACAGQADAMAIAKQAGAKAJAIQAxArBAAQQASAFATAEIAxAFIADABIAJAGIACARQgEAEgEABQgSAJgSAGQgkAKglAGQgbAFgcACIg2gHIgbgGIAUAWQAYAdAiAMIAyATIAggSIAKAFQAFALgHALQgMATgKATQgLAZgHAcQgGAagBAcQAAATAFASQAFASAJARQAEAHAHAFQADADAEABQAHABAEgEQAJgIALgBIAGAIQAAAiAJAgIANArQAPAtAcAlQAfAoApAgIAhAYQAWAOAZAJQAoAOArABIAAAAgAKAH8IBrAkIASgPQgEgFgFgDQgMgHgNgFQgXgIgXAAQgXAAgWAHgADwBQIAGAIIAAAIQgDAJgLAEQhCAXgyAwQgPAPgNASQgeAoAOAuQAFATAJATQAMAZARAYQADAFAHADQAngFAngHQAmgGAmAIQAQAFARACQAEACAFgCIAcgHQAKgEALgBIAyAJQAKACAJgFQAIgFAEgGIAXggQAwhDAuhFQAyhLA3hJQAOgSAMgUQAHgKAFgKQALgdAHgdQAKgmAIgmIAMg+QAJg0AGg3IAKhVIAGgzQADgdACgcQACgdAAgcIAAhNIAAgnIAAguQAAgagEgaQAAgFgEgEIgHAEQgYA8gJBCIgLBLQgQBfgKBgIgDAyIgBAVQABALACALIAHAZIACAGIgKAEQgZgKgFgbQgEgWACgWQAFgigJgiQgLgrgMgoIgQgzQgMgsgOgqQgEgLgKgEIgNACIgFAIQAKAiAAAjIgCAfQgBAaAHAaQAQA8AJA+IAKBAIAGAsIAAADIgPAGIgJgDQgagpgYgqQgeg3gvgoQgPgNgQgJQgEgDgEAAIgKAHIACAMQARA3AXAzQAVAsASAuQALAbAJAdQADAKgBALIgCAKIgEACQgZgUgUgZQgPgUgVgOQgWgQgXgOIgygZQgNADADAOQABAHAEAGIA0BKIAdAoIggAoQgEAAgDgCQgKgCgKgEQgYgJgdAAQgMAAgJAJQgDAKAKAGIAhATQAQAJAMAMIAQAQQAGAKgLAHQgJAGgKABQgjgCghAPQgjAQghATQgWAMgTARQgKAJgIANQAUgBAPgLQAOgKAQAAQAHAAAHACgAA/IgQgWgdgRggQgNgagLgaQgTgrgRgrQgJgYgIgYQgKgfgIgfQgHgbADgeQAAgHAEgGQAIACADAJQALAZAGAYQAJAiALAiQAKAfAMAgQALAgAPAgIAbA7IATAlQAGANALAIQADADACAEQACADAAAFIgLAKQgLgEgKgOg");
    this.shape.setTransform(-0.0178, -0.0019);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#E2E0D9").s().p("AilCtQgGgCgEgEQgbgWgUgbQgPgUgNgVQgJgPgBgRQgCgsAIgrQADgUAIgRQAGgOAFgOQAFgTAKgRQAGgLAKgGQARgHARgGQAKgDALgBIBGAGQAkAGAkAHQAiAHAjAMQAFABADAFQAGANAFANIAxAKQgMAMgSgDQgNAAgIAJIAAAGQACAJAHADQASAJAOAKQARAMAGAUQgFAJgDAIQgEALgBAMQgBAMAHAJQAIALgCAMIgDADQgIAIgJAFQggASggAPQgiAOghALQhGAXhKAHQgIgBgHgEgACJhYQAtgPAtAPQAOAFAMAIQAFADAEAFIgTAPg");
    this.shape_1.setTransform(50.33, 59.7);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#FFC600").s().p("ApCByQAJgLAHgNIAIAAIAIALQgHAWgSARQgRAPgUAMQgMAGgNAAIA3g7gAnZCcIgGgGIAKgZQAHgOAFgPQAEgUgFgRIgDgOIAJgGQAMgGAPgBQgFAmgIAmQgHAegWATgAHEh1QgBgKAGgIQAKgNAPgIQAigQAnAAIAjAFQARgBAPAHQAKAGABAMIgFAJIgegDQgggHgfAHQgoAGgjATg");
    this.shape_2.setTransform(20.35, 75.2);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#874F4F").s().p("AhVISIgDgLIAIgMQATgVATgTQAYgXASgXIgBgIQgMgIgLgKQgKgJAFgMIAJgTQACgEAFgDIAFABIAgANQApARAsgBIAxgJIAugEQANAEAPACQAIACAJgDIAfgHIAFgGQAJgQgQgIQgPAAgPAEQhFAJg6gmIACgHIAVgLIgHgQQgVABgXgIQgngPgdgfQgXgYgRgeQgIgOgFgOQgGgTgDgTQgCgUAAgUQAAgHAEgFQAKgOAOANIAMANQAPAPANARQAKAOAQgCIAJgMIAAgGIgSgQQgOgMgMgOQgMgNgJgOQgJgOgHgOQgQghgGgkQgGgkgDgkQgEghAEggQACgTAIgRQAbg6BAgIIAJAEQACAEABAFQABATAPAJIAAgUIAAg9IABgqQACgtARgoQAMgaAagSIAFABQAEACACAEIAZAyIAUAnQAJATAIAVQAIATAGAUQAFASACATIAIBBQAEAwAMAvQARBMAaBJQAVA6AfA2IA8BhQARAcAZAUQAGgCAGgEQADgCABgEQADgIgHgGQgQgPgMgUIgUgdQgGgIAAgMIAIgHICTgOIgfAjQgPARgJAVQgVA0gDA6QgBAkAHAhQAFAXAHAYQAEALAIAJQAVAaAaAVIguABQguAFgtgOIhFgWQgLgCgKgIIgVgOQgLgIgNgEQgLgFgLgBIALAZIgKAHQgdgHgdgIQgWgGgVgJQgSgHgRgLQgIgBgFAGQgZAagdAYQgOALgTAEQgIgGgJgDQgIgDgHAFQgOAMgOANQgTARgQAUQgIAJgMAAIgGAAgADJhvQgDAeAHAbQAJAfAJAeQAIAYAJAYQARArAUArQALAaAOAaQARAgAVAdQAKAOALAEIAMgKQgBgFgCgDQgCgEgDgDQgLgIgGgNIgTglIgbg7QgPgggMggQgMgggKgfQgKghgKgiQgGgYgLgZQgDgJgIgCQgEAGAAAHgAkHGWIgagDIgxgCQg8gGg3gcQgXgLgUgPQgfgZgigYIgGgRIACgEQAQABAQAEQAjAFAkAEIA8ABQAuADAuAGIA1gHQA1gHA0ADQASgBASgGIBEA2IgBAJIgcAIIgcARQgUAMgVAJQgkARgmAFIgrgCg");
    this.shape_3.setTransform(-27.75, 28.9883);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#A6654D").s().p("ACZL8QgZgJgWgPIghgYQgqgfgegoQgcgmgPgsIgNgsQgIgggBgiIgFgHQgMABgJAIQgDADgHAAQgFgCgDgCQgHgFgEgIQgJgQgEgSQgFgTAAgSQAAgcAHgbQAGgbAMgZQAJgUAMgSQAIgMgGgLIgKgFIgfASIgygSQgjgMgYgeIgTgXIAaAGIA3AIQAbgCAcgFQAkgHAkgJQATgHARgIQAFgCAEgDIgDgSIgIgGIgEAAIgxgGQgSgDgSgGQhAgQgxgrQgJgHgHgJQgHgLgEgLQgCgHABgHQAHgBAFACQApAMApAOQBNAaBPAGQAMgDABgJQADgOgNgIIgdgQQgXgNgWgPQgigVgZgdIgkgnQgYgcgKglIgIgaQgDgLgHgJQgKgMABgOQAEgDAEgCQAEgBAEACQAOAIANALQA2AuA1AwIArAmQANALAPAJQATAMASAPIAjAZIAPgIIgCgJQgDgKgGgJQgfgogWgsQgZg2gWg5QgchJgKhOIgEgyQgCgbAAgbQAAgPACgPIAFgBIAEABQABAAAAAAQABAAAAAAQAAAAABABQAAAAABAAIACACIAEALQAHAbALAcIAfBMIAZA7QANAeAPAdQAdA5AaA6IAQAmQAGAOALAFIALgEQAAgJgDgIQgWg+gLhCIgKg7QgMhUgBhWQAAg8AIg7QAEgdAGgcQAGgbAJgbQADgIAFgIIAKAAIADAFQAABAABA+IACA0QADAtAHAtQASB6AkB2QAQAzATAwQAGAQAEAQQACALgEAIIgIALQgQAXgOAYQgMAVgHAXQgHAXgCAYQgCAOACAOIAEAhIAAAGIgLAEQgMAAgMACQgNACgMAGQgRAHgPAJQgSAKgLAQQgfAqADA1IAFAjQAHAqAMApQALAlAHAlQACAKgFAHQgIALgFAMQgFALAAAMQAAAgAJAfQAQA8AsAsQALAKAMAIQAUAOAQAQQgDALADAJQACAEADACQATAPARAQQgsgBgngOg");
    this.shape_4.setTransform(-40.8667, -14.975);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#592626").s().p("AnzMBIASgLIAegUQAPgIAMAJQAJAHgGAIIgUAGQgUAHgUAEIgHABQgFAAgGgDgAi2GGQgQgXgMgaQgJgSgGgUQgNguAdgoQAOgRAPgPQAygwBCgYQALgEADgIIAAgIIgHgJQgXgGgVAPQgOAKgVABQAJgMAKgKQASgQAXgMQAhgTAjgRQAggQAjADQAJgCAKgFQAKgHgFgJIgQgQQgMgNgQgJIghgTQgJgFADgLQAIgIAMAAQAcgBAZAKQAKAEAKACQADACAEAAIAfgoIgcgoIg0hKQgEgHgBgHQgEgNAOgDIAxAZQAYANAWAQQAUAPAQATQAUAZAYAUIAFgBIACgLQAAgLgCgKQgJgcgLgcQgSgtgVgsQgYgzgQg3IgCgMIAKgIQAEABAEACQAQAKAPAMQAvApAeA2QAYAqAaApIAIAEIAQgHIAAgCIgHgsIgJhAQgJg+gRg9QgGgaABgaIACgeQAAgjgKgiIAEgIIAOgCQAKADADALQAPArAMArIAQAzQAMApALArQAJAhgFAjQgCAWADAWQAFAaAZALIAKgEIgBgHIgHgZQgCgKgBgLIAAgWIAEgyQAKhfAPhfIAMhLQAJhCAYg9IAHgDQADAEABAEQADAaABAbIAAAuIAAAnIAABNQgBAcgBAcQgCAdgEAcIgFAzIgKBVQgGA4gJA0IgMA+QgIAmgKAlQgIAegLAbQgEAMgHAKQgNATgOASQg2BKgyBLQgvBEgvBEIgXAfQgFAGgHAFQgJAGgKgDIgygJQgLACgLAEIgbAGQgFACgFgBQgPgDgQgEQgmgIgnAGQgmAHgnAEQgHgDgEgFg");
    this.shape_5.setTransform(29.7, -0.4687);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-86.8, -96.5, 173.6, 193);


  (lib.Symbol10copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#6B0F0F").ss(0.5, 1, 1).p("ADzigIgCgBQgEgCgFgCQhGghhVAEQgdACgcAGQiaAdgzAOQgtAMgVAPIgBABQgBABgBABQgOAMAAAOQAAACABADQABABAAAAIAAABQAAAAAAAAAC6AOQgCgBgBgDQgBAAAAAAQgBgDgBgEQgCgNAJgHQABAAAAgBQADgEADgEQAFgEAHgBQACAAAJABQAIAAAGAGQACACACABQABABAAAAQABABAAABAE0hRQAAgDAAgDQAAgEAAgEQgCgJgFgJQgCgDgDgEIgigdQgCgBgCgBQgGgEgGgDADtgSQAAABACACQABACAAAFQAAAAAAAAQADgBACgCQAHgEAGgEQALgIAJgJIAFgEQABgBAAgBQACgBACgCQABgBABgCQAHgIAEgIQAGgIABgJQAEAUgHAhQgCAOgEAOQgEALgEALQgFANAAAUQAAAMABAZIABAAQAGABATARQAWATgDAHQgPAUgKAGQgRALgRAEQgRAEgEgEQgBgBgDgHQAAAAgBgIQgDgIAAgFQgBgGACgEQAEgIAPgRQACgCACgCQAKgLAFgEQACgBABgBQAAAAABAAADwgIQgCApgGAxQgBAMgBALQgEAcgFAXQgBAIgCAHQABgBAAgBQANgRABgBQACgBACgBQACAAACgBQABAAACAAQAOgCAKgDQAOgDAHAAQAJAAAJAEQAJAEALgHQADgCACgDADIC1QAHAAAFgEQAEgCACgEABsAaQABAAAAAAQABAAABAAQABAAAAAAQALAAALgBIAFgBQADAAACAAIAPgEIAXgEQACgBACgBQgEAaAMA6QAKAygDAZQAAAFgBADAgGADQAAAAAAAAQAEABACABIAQAFQAAAAAAAAQABAAABAAIAAAAQABABABAAQAIADAHACQAMADAMADIABAAQACAAABAAQAWAEAWAAAgSC2QADgCAGgDQABgBACgBQABAAAAAAQAMgHAJAAQAHAAAKAEQAGAEAIAFIACABQADACACABQAQAKALgBQAHgBALgHABmC4IAFgEQADgCADgCQAJgHAJgCQACAAACgBQAIAAAPAFQAEABAFACQAWAHALAAAkJhPIADAGQABACACACQABABABAAIAAABQABABABAAIAEADAj5g+QAJAHAPAFQADACAEABQALAEAOADQACABADABQAIACAIABQAOADAMADIAKACIAJACAkLhSQABAPgCAeQgCAkgEANQAAABgBABAh7gYQAFABAFABQAEABAFABQAEABAFABQACABACAAQAFABAEABIAaAHQAVAFAPADAgSAAQAAAhABASIABAXQAAAEgCAoQAAAVAAArAjOBsQAJALAIAGQAKAJATACQAHABAFABQANACAEABQABAAAAAAQAMAFAWAMQAGADAGAEQACAAABABQAFADAFADAgbC5QgJgHgKgBQgNgBgJgCQgCgBgBgBQAAgCAAgCQACgPAVgBQAHAAAHAIQAIAHABAHQAAACgCAJIAAABQgWANgJgGQgNgKAAgLAgbC6QACgBAHgDAhSgPQgBAZAAArQABAugCBCAkTARIgCADQAAABAAABQgBACgBACQgDAKgBAEQgCgCgEgLQgDgIgHAAQgBAAgBAAQgBADAEATIABABQAFAZAAADQAQABAUAAQAVACAAAAQAAAAABABQABABABABQACACACADQAHAHAIALQAEAFADAEAjuBFQgIgHgNgHQgRgJgHgGAjrBJIgCgCAktATQgEACgFAHQgCAFgFAIQgBABgEAEQgCACgCACQgIAIADABQALAIAcADAjBgoQgEAagBAvQgBAngHAk");
    this.shape.setTransform(0.0114, 7.5015);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#B18E81").s().p("AABAAIgBAAIABAAg");
    this.shape_1.setTransform(6.1875, 9.725);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#744331").s().p("AhJBOIAAgKIABgXQAFgygCgwQAAgVABgGIABgFIABABIACAAIABAAQAWAEAWAAIgDAKQgFAVgBAMIABAzQACAlgCAYIAAAEQgLAHgHABIgCAAQgLAAgPgJgAAoBDIgJgDIgEhJIgDhLIgMACIAFgBIAPgDIAEAYIAGATQAJAYACAKIAFAgQAEAUAFANIAFAJIACABIgBAIIgCAAQgLAAgUgHg");
    this.shape_2.setTransform(12.7313, 18.2283);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#6A4031").s().p("AgjAUIgEgIIgBgIQgDgHAAgGIAYgFQAOgDAGAAQAJABAJAEQAJADALgHIAFgEQgPAUgKAFQgRALgQAEIgOABQgFAAgCgBg");
    this.shape_3.setTransform(28.825, 23.2875);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#7B4D39").s().p("AgYA3QAEgYAEgcIAPgIQAOgJADgKQACgFAAgKIABgPQABgEAHgKIAAAJIABANQAAAGgHAPQgEAJAAAJQAAAIAEAGIgEAFQgPARgDAIQgBAEAAAFIgDABIgEAAIgDACIgOATIgBABIADgOg");
    this.shape_4.setTransform(24.6813, 17.6875);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#5C2C1B").s().p("AACAOQgRgKgHgEIAEgPIACgDIABgDIABgDIABgDIAEAEIAJAHIANAKQAGAEADAEIAAAaQgIgIgMgGg");
    this.shape_5.setTransform(-26.1, 11.6875);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#794531").s().p("AgrA/QgNgBgJgCIgDgBIAAgFQACgPAVAAQAHgBAHAIQAIAHABAIIgCALQgJgHgKgCgAD4AZQAEgIAPgRIAEgEQAKgLAFgEIADgBIABAAIABAAIAAAAQAAAFAEAFIAFAGIABAHQAAAJADAJQABAFAEABIABADQAAAAABABQAAAAAAAAQABAAAAAAQABAAAAAAIAHgDQAFgDAFACQgLAHgJgDQgJgEgJgBQgHAAgOADIgYAFQgBgFACgEgAlIg1QgDgCAIgHIAEgEIABAAQAGADAJgCIAJgDIAAgDIAFAcQgcgCgLgIg");
    this.shape_6.setTransform(-0.2812, 18.875);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#573325").s().p("ABqBWQgNgKAAgLIADACQAJACANABQAKABAJAHIAAABQgPAJgJAAQgEAAgDgCgAhbgjQgUAAgQgBIgGgcIAAgBQgEgTABgDIACAAQAHAAADAIQAEALACACQAHAGARAJQANAHAHAHIACACIACACIgBAAIgUgCg");
    this.shape_7.setTransform(-16.4687, 18.2056);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#683B2A").s().p("AgYBYIAFADIAAAAIgFgDgAgYBYIgBgBIgPgJQgKgFgHABQgJAAgMAHIgBAAIgEACIAAgGQADgVACg/IAAgdQgCgiABgRIAAgDIAFACIARAEIAAABIACAAIABAAIABABIAPAFIAYAGIgBAEQgBAGAAAVQACAvgEAzIgCAXIABAKIgFgDgAgYBYIAAAAgAAggBQgBgeADgkIACAAIABAAIACAAIAVgBIAFgBIANgBIACBJIAEBKQgPgFgHAAIgFABQgIACgKAHQgHg2AAgdg");
    this.shape_8.setTransform(7.4, 16.925);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.f("#9D7B6E").s().p("AAuASIgBAAIgCAAIAAAAQgXAAgUgEIAAAAQAOABAdgBIAgACIAFgBIgGABIgEAAIgWACIgCAAgAB2ACIAAAAIADAEIgEABQABAAAAgBQAAgBABAAQAAgBAAAAQAAgBgBgBgAhAgDIgGgBIgBgBIgHgCIgFgBIgBAAIgkgJIAIACQAcAFAWAHIAOAEg");
    this.shape_9.setTransform(6.475, 8.3625);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.f("#825441").s().p("AgFBOIgBgBIgRgDQgEgqAAgiQAAgwAGgrIAKACIAJACIADABIAJACIAKACIAIACIAAAHIgCAvQAAAoACBSQgWgMgLgEg");
    this.shape_10.setTransform(-12.4625, 13.8875);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.f("#633320").s().p("AABBGIgOgTIgEgFIgDgCIABgBIAKhCIAEgsIABgKIAZAHQgEAbgBAuQgBAngHAkIgHgIg");
    this.shape_11.setTransform(-21.4625, 10.5);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.f("#7B452F").s().p("AADA3IgMgKIgKgIIgFgDIABgCQAEgPACgiQACgegBgPIABABIAAAAIAAABIAAAAIAAAAIABABIADAGIADADIABACIABAAIAAAAIACACIAEADIAAABQAKAGAPAGIAAAAQgNA9AAAcIAAAEQgDgEgGgEg");
    this.shape_12.setTransform(-25.075, 5.55);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.f("#82655B").s().p("ACQAvQgdABgPgBIAAAAIgDgBIgBAAIgYgGIgQgFIgBAAIAAAAIgCgBIgBAAIgOgFQgWgHgbgFIgIgCIgagGIgKgCIgEgBIgJgCIAAgDIgEgBQgPgGADAAIAAAAIAGABIA0AJIAGABIAeAGIACAAIBjATQAmAHAYAAQARAAAUgEQAHgCACADIAAABIAAAAQABABAAABQAAAAAAABQAAAAgBABQAAABgBAAIgWAFIgPADIgEABIghgCgAiugaQgKgFgHgCQgJgDgNgBIgBgBIgFgDIAEgEIAAAAIACgBIADgCIAAAAQAOAAALAEQAIACANAIIgKAIg");
    this.shape_13.setTransform(-3.6283, 5.0625);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.f("#9D6753").s().p("ABeCCQACgYgCglIgBg0QABgMAFgUIADgKIABAAQgDAjABAeQAAAeAHA2IgGAEIgFAEIgDACIAAgEgAgXBCIACgsIgBgWQgBgSAAghIAMADIAAAAIAAADQgBARADAhIAAAdQgCBAgDAVIAAAGIgJAFIAAhAgADCB4IgFgJQgFgNgEgUIgFghQgCgKgJgYIgGgSIgEgYIAXgFIAEgBQgEAaAMA5QAKAxgDAaIgCgBgAhlBqQgChTAAgnIACgvIABgHIAEABIAJACQgBAaAAAqQABAtgCBDIgMgHgAEuBbIgBgDQgEgBgBgFQgDgJAAgJIgBgIIgFgFQgEgFAAgFIAAgBQAGABATARQAWATgDAHIgFAFQgFgDgFADIgHAEIAAAAQgBAAAAAAQAAAAgBgBQAAAAAAAAQgBAAAAgBgAEDApQAAgIAEgKQAGgOABgGIgBgNIAAgIQAAgRAHgZIAIgdQgZAYgQAEQgDABgFAAIAFgDIANgJQALgHAJgJIAFgEIABgCIAEgDIACgDQAHgIAEgIQAGgJABgIQAEAUgHAhIgGAbIgIAYQgFANAAATIABAlIgBAAIgDACQgFAEgKAKQgEgGAAgJgAjwAVIgCgCIgBgCIAAgZIAAgEQAAgcANg9IAAgBIAHADIgCAKIgEAsIgKBBIAAABIgBAAgAlGgFIgBAAIAFgFIAHgNQAFgHAEgCQgBADAEATIABABIAAACIgJADIgGABQgFAAgEgCg");
    this.shape_14.setTransform(0.5051, 12.725);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.f("#94614C").s().p("AjJBYQgTgDgKgIQgIgHgJgLQAHgkABglQABgwAEgaIAFABIAQAEIAaAFQgGArAAAvQAAAjAEAqIgMgBgAC/AiQAGgxACgpIAAAAQAEAAAEgBQAQgEAZgZIgIAdQgHAaAAAQQgHAKgBAEIgBAOQAAAKgCAFQgEAKgOAJIgPAJIACgWg");
    this.shape_15.setTransform(4.125, 12.375);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.f("#542716").s().p("AihBhQABhCAAgsQgBgrACgaIAaAGIAkAJQgFAGgDANQgFAagCAZIgDA+QgMABgLAGQgLAHgGALIgEAJIgCgCgACNA8IgEgZQgBgLgCgsQgBgigHgVQgDgMgGgGQAFgEAHgBIAKABQAIAAAGAGIAFADIAAABIABACIAAAAIACADIABAHQgCAqgFAwIgDAXQgEAcgEAXIgDgdg");
    this.shape_16.setTransform(7.775, 14.2375);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.f("#BE9787").s().p("ADfAoIAWgQIAFgEIgBACIAFgFQgJAJgLAIIgNAIIACgCgAEXgPIACgKQACgIAAgIIABAIIAAAGQgCAIgFAJIACgFgAkagOIgBgBIAAAAIACABg");
    this.shape_17.setTransform(2.425, 2.1125);

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.f("#543C32").s().p("AhAAwIAEABIALgHQAIgFABgGIAAgGIAEgGQADgFABgFQAHAGAEAHQgDABAAAFIAAAHQgBACgDACIgFADQgBACAAAHIABADgAiZAmIgagGIgRgEIgFgBIgZgHIgHgDQgOgGgKgGQANABAJAEQAHACAKAEIABABIgCACIABABIAFgBIgBgBIAQgEIASgHIAEAAQADABAKAIIAFADQABAAAAABQABAAAAAAQAAAAAAABQAAAAAAAAQABAAAAAAQABAAAAABQAAAAABABQAAAAAAABQABAEgQAAIgDAAQgTABAuALgADHghQADgHABgJQAaADAOgGIACgBQAGAFADAGIAEAJQgLABgKACQgKACgTAHIgBABQgCgIgGgFg");
    this.shape_18.setTransform(0.7625, 0.7375);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.f("#462215").s().p("AhgBqIABgLQgBgIgHgHQgIgIgHABQgUAAgCAPIAAAFIgLgHIAEgIQAGgLALgHQALgHAMgBIADg8QACgaAFgbQADgNAFgFIABAAQAAAiACARIAAAYIgBAqIgBBAIgIAEgACDBeQAEgagLgyQgMg4AEgaIgDgEIAAgBIgCgHQgCgOAJgHIAAAAQADgFAEgDQAGAGADAMQAHAUABAiQACAtABAKIAEAZIADAeIgDAOIgHAGQgFAEgHAAIABgHg");
    this.shape_19.setTransform(6.975, 15.475);

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.f("#EED2C6").s().p("ADbBWIgCgDIAAAAIASgOQANgKAGgIQAHgJADgMQAFgMAAgKIAAgBIAAgBQgBgJgGgLQgDgGgDgDIgEgEIgFgEIgBAAIgNgMQgHgCgHgEIgLgGIAAgBIgBAAIgCgBIAAAAIAAgBIgBAAIgDgCQgTgLgfgHQg2gMhEAIQgpAEhPASIhmAXIgNAEIAAAAIgEABIgKADIgDABIgGACQglALgJARQgHAOAFAPIgBABIgEgEIgDgGIAAAAIAAgBIgBAAIAAgBIAAgBIgBgFQAAgOANgLIACgCIACgBQAUgQAtgLQA0gOCZgdQAcgGAdgCQBWgEBFAhIAKAEIABABIAEACIALAHIAEACIAjAdIAEAHQAGAJABAIQAAAIgCAIIgDAKIgBAFQgEAIgIAIIgCADIgDADIgCACIgEAEIgFAFIABgCIgGAEIgVAQIgDACIgEADIgBAAIgBgHgAkXAgIgDgEIAEAEgAkeAVIAAAAIABAAgAEZgLIgEgHIAEAHIAAAAIAAAAg");
    this.shape_20.setTransform(1.9625, -2.6791);

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.f("#A87E6D").s().p("ABmBkIgBgDIAAgOQgBgJgIgMQgKgPgCgEQgCgGACgDQACgDAKAAQAIAAAKgFIAQgJQATgIAhAFIAMAAQAHgCABgFQABgEgFgGQgGgGAAgEIADgJIACgKQADgEALACQAOACAIgDIAGgCIgNgJQgMgIgNgGQgVAKgaADQgTAEgQAAIgPAAQgJAAgGADIgNAJQgHAEgLAAIgUgBQgVAAgUAFQgJACgDADIgHAHQgHAIgIADQgGABgBAEQgCADAFAGIANAOQAIAHABAFQABAEgCAKIgEAHIgGAEQgCADABAFIABACIgDAAIgDgBIgBgDQAAgHACgCIAEgDQADgCABgCIABgHQAAgFADgBQgFgHgHgGQAAAFgEAFIgDAGIgBAGQgBAGgIAFIgKAHIgFAAIgFgBIADgDIAIgFQAFgDADgEQADgDADgJQADgKADgDQgCgBAAgGQAAgCgDgEIgDgGQgCgIAJgEIAOgIIAJgJIgigEQgVgBgQADIgOABQgKgBgCgGQgGgBgJACIgPADQgTAFgJgJIgFgFIgBAAIgVALIgZATQgQAKgLAGIgDACIgBAAIAAAAIgDABIgCABIAAAAIgEACIgBAAIAAABIAAgBIgCgBIACgBQgGgPAHgNQAJgSAlgLIAGgCIADgBIALgDIADgBIABAAIAMgEIBmgXQBQgSApgEQBEgIA1AMQAfAHAUALIACACIABAAIABABIAAAAIACABIAAAAIABABIAKAGQAIAEAGACIAOAMIABAAIAEAEIAFAEQADADADAGQAGALAAAKIAAABIAAABQAAAJgEAMQgEAMgHAJQgGAIgMAKIgSAOIgBgCIgBgBIgEgDQgGgGgIAAIgLgBQgGABgGAEQgDADgDAFIgBABQgIAGABAPIACAHQgGgLABgLIACgIIADgIIAEgFQAJgLANgBIANAAIAOAEQAEACAEADIAGgJQAMgPAEgRQACgKgBgHIgTABIgQADQgJACgEADIgCACQAAAHgCAGIgogBQgVgBgLAFIgPAIQgOAHgRAAQABAHAIALQAJANACAFQABAEACAOIACAKIgFAAgADBgNQAFAFACAIIACgBQATgHAKgCQAKgCAKAAIgEgKQgCgGgGgFIgDABQgNAGgbgDQgBAJgCAHgAgugcIARADIAHADQAFgEAMgCQAZgFANABIATABQALAAAHgDIAOgKQAFgCAHgBQgMgCgIABQgPAAgaAJQABgCAFgCQgCAAgDgBIgSgKIgJgDQgGgCgNABIgKgCIgKgBQgFAAgEgCQgDgBgDgFIgBgBQg4ALg7ASIgQAGIAFAFQAJAGASgGQATgFAIAEIAGAEQAHADAMgDQAJgCAOAAIAYABgABrhWQhEABhGANIgKACIACADQADACAIAAIALABIALACIAOABQAGABALAGIATAJIgBABQAOgFAKgBQALgCARABIAdACQANAAARgCQAJgDASgIIAHgCQgggPglgHIgGAAIgGAAgACug3QAPgDAHgDIAAAAIgEgCIgSAIgAiNA+IgJgCQgugLATAAIADAAQAPgBAAgDQAAgBgBgBQAAgBAAAAQgBAAAAgBQgBAAAAAAQAAAAAAAAQAAAAAAgBQgBAAAAAAQAAAAgBgBIgGgDQgJgIgEgBIgDAAIgTAHIgPAFIAAAAIgEABIgBgBIACgCIgCgBIAAAAQAPgJADgBIAQgCQAKgCAEADIAFADQAFAFAGADIAEACQACABAEAGQACAEADACIAHADIAEADIABAAQgEgBAPAHQgFgCgJACIgEgBg");
    this.shape_21.setTransform(1.404, -1.2855);

    this.shape_22 = new cjs.Shape();
    this.shape_22.graphics.f("#B39082").s().p("ABGBWIhhgSIADAAIgBgCQgBgFACgDIAGgEIAEgHQACgKgBgEQgBgFgIgHIgNgOQgFgGACgCQABgEAGgCQAIgDAHgIIAHgHQADgDAIgCQAVgFAVAAIAUABQALAAAHgEIANgJQAGgDAJAAIAPAAQAQAAATgEQAagDAVgKQANAGAMAIIANAJIgGACQgIADgOgCQgLgCgDAEIgCAKIgDAJQAAAEAGAGQAFAHgBADQgBAFgHACIgMAAQghgFgTAIIgQAJQgKAFgIAAQgKAAgCADQgCADACAGQACAEAKAPQAIAMABAJIAAAOIABADIAFAAIgCgKQgCgOgBgEQgCgFgJgNQgIgLgBgHQARAAAOgHIAPgIQALgFAVABIAoABQACgGgBgGIADgCQAEgEAJgCIAQgDIATgBQABAIgCAJQgEARgMAPIgGAJQgEgDgEgCIgOgEIgNAAQgNABgJALIgEAFIgDAIIgCAIQgBALAGALQgCgDgGACQgUAEgSAAQgXAAgngIgAgWBRIAFAAIAGADIgLgDgAh3AzIgGgBIgEgDIgHgDQgDgCgCgEQgEgGgCgBIgEgCQgGgDgFgFIgFgDQgEgDgKACIgQACQgEABgOAJIALgIQgNgHgJgDQgLgDgOAAIABAAIADgCQALgGAQgKIAZgTIAVgLIABAAIAFAFQAJAJATgFIAPgDQAJgCAGABQACAGAKABIAOgBQAQgDAVABIAiAEIgJAJIgOAIQgJAFACAHIADAGQADAEAAACQAAAGACABQgDADgDAKQgDAJgDADQgDAEgFADIgIAFIgDADgAh2A6IgJgCQAIgCAGACIAEABIABADIgKgCgAkFANIAAgBIABAAIAEgCIgDAEIgCgBgAgTggIgRgDQgfgCgQADQgMADgHgDIgGgEQgIgEgTAFQgSAGgJgGIgGgFIARgGQA6gSA5gLIABABQADAFADABQAEACAFAAIAKABIAKACQANgBAGACIAJADIASAKQADABACAAQgFACgBACQAagJAPAAQAHgBANACQgHABgFACIgOAKQgHADgLAAIgTgBQgNgBgaAFQgLACgFAEIgHgDgAA2g0IgTgJQgLgGgGgBIgPgBIgKgCIgLgBQgIAAgDgCIgCgDIAKgCQBFgNBFgBIAMAAQAlAHAgAPIgHACQgSAIgJADQgRACgNAAIgdgCQgRgBgLACQgKABgOAFIABgBgADKhGIAEACIAAAAQgHADgPADIASgIg");
    this.shape_22.setTransform(0.4409, -0.5783);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_22}, {t: this.shape_21}, {t: this.shape_20}, {t: this.shape_19}, {t: this.shape_18}, {t: this.shape_17}, {t: this.shape_16}, {t: this.shape_15}, {t: this.shape_14}, {t: this.shape_13}, {t: this.shape_12}, {t: this.shape_11}, {t: this.shape_10}, {t: this.shape_9}, {t: this.shape_8}, {t: this.shape_7}, {t: this.shape_6}, {t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    // Layer_2
    this.shape_23 = new cjs.Shape();
    this.shape_23.graphics.f("rgba(124,113,80,0.529)").s().p("Ag1BeIgpgZQgJgFgKgBQgHAAgWACQgnACgTgZIgJgOQgFgJgFgEQgIgHgQgDQgNgCgcgBQgbAAgOgDQgRgDgLgIQgOgKgDgNQgEgNAGgNQAGgMAMgJQAQgMAigGQBegQBJAQIA6AQIBGAPQArAHAZAIIAgALQATAFANACQAUADAngCIBSgEIAggBQARACAOAEQAPAGALAMQAMAMABAOIAAAHIABAEQACAGAAAFIgBAOIgFAMQgIAMgGAFQgLAKgcAIIgSACIgHgBQgLADgKgCQgOgCgFABQgEAAgLAIQgPAJgRgGIgUgKQgMgFgJAAQgGAAgSAKQgVAKgYgCQgYgCgSgOQgJgFgCAAQgDAAgIAGQgSAPgVABQgTAAgXgPgAgUAnQAGAEAEgBIAGgCIgEgFQgGgEgMgCIgPgDgAiugjIACgCIgFgBIADADg");
    this.shape_23.setTransform(-1.4145, 18);

    this.timeline.addTween(cjs.Tween.get(this.shape_23).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-38.7, -13, 74.6, 41.9);


  (lib.Symbol10copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#6B0F0F").ss(0.5, 1, 1).p("ADzigIgCgBQgEgCgFgCQhGghhVAEQgdACgcAGQiaAdgzAOQgtAMgVAPIgBABQgBABgBABQgOAMAAAOQAAACABADQABABAAAAIAAABQAAAAAAAAAC6AOQgCgBgBgDQgBAAAAAAQgBgDgBgEQgCgNAJgHQABAAAAgBQADgEADgEQAFgEAHgBQACAAAJABQAIAAAGAGQACACACABQABABAAAAQABABAAABAE0hRQAAgDAAgDQAAgEAAgEQgCgJgFgJQgCgDgDgEIgigdQgCgBgCgBQgGgEgGgDADtgSQAAABACACQABACAAAFQAAAAAAAAQADgBACgCQAHgEAGgEQALgIAJgJIAFgEQABgBAAgBQACgBACgCQABgBABgCQAHgIAEgIQAGgIABgJQAEAUgHAhQgCAOgEAOQgEALgEALQgFANAAAUQAAAMABAZIABAAQAGABATARQAWATgDAHQgPAUgKAGQgRALgRAEQgRAEgEgEQgBgBgDgHQAAAAgBgIQgDgIAAgFQgBgGACgEQAEgIAPgRQACgCACgCQAKgLAFgEQACgBABgBQAAAAABAAADwgIQgCApgGAxQgBAMgBALQgEAcgFAXQgBAIgCAHQABgBAAgBQANgRABgBQACgBACgBQACAAACgBQABAAACAAQAOgCAKgDQAOgDAHAAQAJAAAJAEQAJAEALgHQADgCACgDADIC1QAHAAAFgEQAEgCACgEABsAaQABAAAAAAQABAAABAAQABAAAAAAQALAAALgBIAFgBQADAAACAAIAPgEIAXgEQACgBACgBQgEAaAMA6QAKAygDAZQAAAFgBADAgGADQAAAAAAAAQAEABACABIAQAFQAAAAAAAAQABAAABAAIAAAAQABABABAAQAIADAHACQAMADAMADIABAAQACAAABAAQAWAEAWAAAgSC2QADgCAGgDQABgBACgBQABAAAAAAQAMgHAJAAQAHAAAKAEQAGAEAIAFIACABQADACACABQAQAKALgBQAHgBALgHABmC4IAFgEQADgCADgCQAJgHAJgCQACAAACgBQAIAAAPAFQAEABAFACQAWAHALAAAkJhPIADAGQABACACACQABABABAAIAAABQABABABAAIAEADAj5g+QAJAHAPAFQADACAEABQALAEAOADQACABADABQAIACAIABQAOADAMADIAKACIAJACAkLhSQABAPgCAeQgCAkgEANQAAABgBABAh7gYQAFABAFABQAEABAFABQAEABAFABQACABACAAQAFABAEABIAaAHQAVAFAPADAgSAAQAAAhABASIABAXQAAAEgCAoQAAAVAAArAjOBsQAJALAIAGQAKAJATACQAHABAFABQANACAEABQABAAAAAAQAMAFAWAMQAGADAGAEQACAAABABQAFADAFADAgbC5QgJgHgKgBQgNgBgJgCQgCgBgBgBQAAgCAAgCQACgPAVgBQAHAAAHAIQAIAHABAHQAAACgCAJIAAABQgWANgJgGQgNgKAAgLAgbC6QACgBAHgDAhSgPQgBAZAAArQABAugCBCAkTARIgCADQAAABAAABQgBACgBACQgDAKgBAEQgCgCgEgLQgDgIgHAAQgBAAgBAAQgBADAEATIABABQAFAZAAADQAQABAUAAQAVACAAAAQAAAAABABQABABABABQACACACADQAHAHAIALQAEAFADAEAjuBFQgIgHgNgHQgRgJgHgGAjrBJIgCgCAktATQgEACgFAHQgCAFgFAIQgBABgEAEQgCACgCACQgIAIADABQALAIAcADAjBgoQgEAagBAvQgBAngHAk");
    this.shape.setTransform(0.0114, 7.5015);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#B18E81").s().p("AABAAIgBAAIABAAg");
    this.shape_1.setTransform(6.1875, 9.725);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#744331").s().p("AhJBOIAAgKIABgXQAFgygCgwQAAgVABgGIABgFIABABIACAAIABAAQAWAEAWAAIgDAKQgFAVgBAMIABAzQACAlgCAYIAAAEQgLAHgHABIgCAAQgLAAgPgJgAAoBDIgJgDIgEhJIgDhLIgMACIAFgBIAPgDIAEAYIAGATQAJAYACAKIAFAgQAEAUAFANIAFAJIACABIgBAIIgCAAQgLAAgUgHg");
    this.shape_2.setTransform(12.7313, 18.2283);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#6A4031").s().p("AgjAUIgEgIIgBgIQgDgHAAgGIAYgFQAOgDAGAAQAJABAJAEQAJADALgHIAFgEQgPAUgKAFQgRALgQAEIgOABQgFAAgCgBg");
    this.shape_3.setTransform(28.825, 23.2875);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#7B4D39").s().p("AgYA3QAEgYAEgcIAPgIQAOgJADgKQACgFAAgKIABgPQABgEAHgKIAAAJIABANQAAAGgHAPQgEAJAAAJQAAAIAEAGIgEAFQgPARgDAIQgBAEAAAFIgDABIgEAAIgDACIgOATIgBABIADgOg");
    this.shape_4.setTransform(24.6813, 17.6875);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#5C2C1B").s().p("AACAOQgRgKgHgEIAEgPIACgDIABgDIABgDIABgDIAEAEIAJAHIANAKQAGAEADAEIAAAaQgIgIgMgGg");
    this.shape_5.setTransform(-26.1, 11.6875);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#794531").s().p("AgrA/QgNgBgJgCIgDgBIAAgFQACgPAVAAQAHgBAHAIQAIAHABAIIgCALQgJgHgKgCgAD4AZQAEgIAPgRIAEgEQAKgLAFgEIADgBIABAAIABAAIAAAAQAAAFAEAFIAFAGIABAHQAAAJADAJQABAFAEABIABADQAAAAABABQAAAAAAAAQABAAAAAAQABAAAAAAIAHgDQAFgDAFACQgLAHgJgDQgJgEgJgBQgHAAgOADIgYAFQgBgFACgEgAlIg1QgDgCAIgHIAEgEIABAAQAGADAJgCIAJgDIAAgDIAFAcQgcgCgLgIg");
    this.shape_6.setTransform(-0.2812, 18.875);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#573325").s().p("ABqBWQgNgKAAgLIADACQAJACANABQAKABAJAHIAAABQgPAJgJAAQgEAAgDgCgAhbgjQgUAAgQgBIgGgcIAAgBQgEgTABgDIACAAQAHAAADAIQAEALACACQAHAGARAJQANAHAHAHIACACIACACIgBAAIgUgCg");
    this.shape_7.setTransform(-16.4687, 18.2056);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#683B2A").s().p("AgYBYIAFADIAAAAIgFgDgAgYBYIgBgBIgPgJQgKgFgHABQgJAAgMAHIgBAAIgEACIAAgGQADgVACg/IAAgdQgCgiABgRIAAgDIAFACIARAEIAAABIACAAIABAAIABABIAPAFIAYAGIgBAEQgBAGAAAVQACAvgEAzIgCAXIABAKIgFgDgAgYBYIAAAAgAAggBQgBgeADgkIACAAIABAAIACAAIAVgBIAFgBIANgBIACBJIAEBKQgPgFgHAAIgFABQgIACgKAHQgHg2AAgdg");
    this.shape_8.setTransform(7.4, 16.925);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.f("#9D7B6E").s().p("AAuASIgBAAIgCAAIAAAAQgXAAgUgEIAAAAQAOABAdgBIAgACIAFgBIgGABIgEAAIgWACIgCAAgAB2ACIAAAAIADAEIgEABQABAAAAgBQAAgBABAAQAAgBAAAAQAAgBgBgBgAhAgDIgGgBIgBgBIgHgCIgFgBIgBAAIgkgJIAIACQAcAFAWAHIAOAEg");
    this.shape_9.setTransform(6.475, 8.3625);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.f("#825441").s().p("AgFBOIgBgBIgRgDQgEgqAAgiQAAgwAGgrIAKACIAJACIADABIAJACIAKACIAIACIAAAHIgCAvQAAAoACBSQgWgMgLgEg");
    this.shape_10.setTransform(-12.4625, 13.8875);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.f("#633320").s().p("AABBGIgOgTIgEgFIgDgCIABgBIAKhCIAEgsIABgKIAZAHQgEAbgBAuQgBAngHAkIgHgIg");
    this.shape_11.setTransform(-21.4625, 10.5);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.f("#7B452F").s().p("AADA3IgMgKIgKgIIgFgDIABgCQAEgPACgiQACgegBgPIABABIAAAAIAAABIAAAAIAAAAIABABIADAGIADADIABACIABAAIAAAAIACACIAEADIAAABQAKAGAPAGIAAAAQgNA9AAAcIAAAEQgDgEgGgEg");
    this.shape_12.setTransform(-25.075, 5.55);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.f("#82655B").s().p("ACQAvQgdABgPgBIAAAAIgDgBIgBAAIgYgGIgQgFIgBAAIAAAAIgCgBIgBAAIgOgFQgWgHgbgFIgIgCIgagGIgKgCIgEgBIgJgCIAAgDIgEgBQgPgGADAAIAAAAIAGABIA0AJIAGABIAeAGIACAAIBjATQAmAHAYAAQARAAAUgEQAHgCACADIAAABIAAAAQABABAAABQAAAAAAABQAAAAgBABQAAABgBAAIgWAFIgPADIgEABIghgCgAiugaQgKgFgHgCQgJgDgNgBIgBgBIgFgDIAEgEIAAAAIACgBIADgCIAAAAQAOAAALAEQAIACANAIIgKAIg");
    this.shape_13.setTransform(-3.6283, 5.0625);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.f("#9D6753").s().p("ABeCCQACgYgCglIgBg0QABgMAFgUIADgKIABAAQgDAjABAeQAAAeAHA2IgGAEIgFAEIgDACIAAgEgAgXBCIACgsIgBgWQgBgSAAghIAMADIAAAAIAAADQgBARADAhIAAAdQgCBAgDAVIAAAGIgJAFIAAhAgADCB4IgFgJQgFgNgEgUIgFghQgCgKgJgYIgGgSIgEgYIAXgFIAEgBQgEAaAMA5QAKAxgDAaIgCgBgAhlBqQgChTAAgnIACgvIABgHIAEABIAJACQgBAaAAAqQABAtgCBDIgMgHgAEuBbIgBgDQgEgBgBgFQgDgJAAgJIgBgIIgFgFQgEgFAAgFIAAgBQAGABATARQAWATgDAHIgFAFQgFgDgFADIgHAEIAAAAQgBAAAAAAQAAAAgBgBQAAAAAAAAQgBAAAAgBgAEDApQAAgIAEgKQAGgOABgGIgBgNIAAgIQAAgRAHgZIAIgdQgZAYgQAEQgDABgFAAIAFgDIANgJQALgHAJgJIAFgEIABgCIAEgDIACgDQAHgIAEgIQAGgJABgIQAEAUgHAhIgGAbIgIAYQgFANAAATIABAlIgBAAIgDACQgFAEgKAKQgEgGAAgJgAjwAVIgCgCIgBgCIAAgZIAAgEQAAgcANg9IAAgBIAHADIgCAKIgEAsIgKBBIAAABIgBAAgAlGgFIgBAAIAFgFIAHgNQAFgHAEgCQgBADAEATIABABIAAACIgJADIgGABQgFAAgEgCg");
    this.shape_14.setTransform(0.5051, 12.725);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.f("#94614C").s().p("AjJBYQgTgDgKgIQgIgHgJgLQAHgkABglQABgwAEgaIAFABIAQAEIAaAFQgGArAAAvQAAAjAEAqIgMgBgAC/AiQAGgxACgpIAAAAQAEAAAEgBQAQgEAZgZIgIAdQgHAaAAAQQgHAKgBAEIgBAOQAAAKgCAFQgEAKgOAJIgPAJIACgWg");
    this.shape_15.setTransform(4.125, 12.375);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.f("#542716").s().p("AihBhQABhCAAgsQgBgrACgaIAaAGIAkAJQgFAGgDANQgFAagCAZIgDA+QgMABgLAGQgLAHgGALIgEAJIgCgCgACNA8IgEgZQgBgLgCgsQgBgigHgVQgDgMgGgGQAFgEAHgBIAKABQAIAAAGAGIAFADIAAABIABACIAAAAIACADIABAHQgCAqgFAwIgDAXQgEAcgEAXIgDgdg");
    this.shape_16.setTransform(7.775, 14.2375);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.f("#BE9787").s().p("ADfAoIAWgQIAFgEIgBACIAFgFQgJAJgLAIIgNAIIACgCgAEXgPIACgKQACgIAAgIIABAIIAAAGQgCAIgFAJIACgFgAkagOIgBgBIAAAAIACABg");
    this.shape_17.setTransform(2.425, 2.1125);

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.f("#543C32").s().p("AhAAwIAEABIALgHQAIgFABgGIAAgGIAEgGQADgFABgFQAHAGAEAHQgDABAAAFIAAAHQgBACgDACIgFADQgBACAAAHIABADgAiZAmIgagGIgRgEIgFgBIgZgHIgHgDQgOgGgKgGQANABAJAEQAHACAKAEIABABIgCACIABABIAFgBIgBgBIAQgEIASgHIAEAAQADABAKAIIAFADQABAAAAABQABAAAAAAQAAAAAAABQAAAAAAAAQABAAAAAAQABAAAAABQAAAAABABQAAAAAAABQABAEgQAAIgDAAQgTABAuALgADHghQADgHABgJQAaADAOgGIACgBQAGAFADAGIAEAJQgLABgKACQgKACgTAHIgBABQgCgIgGgFg");
    this.shape_18.setTransform(0.7625, 0.7375);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.f("#462215").s().p("AhgBqIABgLQgBgIgHgHQgIgIgHABQgUAAgCAPIAAAFIgLgHIAEgIQAGgLALgHQALgHAMgBIADg8QACgaAFgbQADgNAFgFIABAAQAAAiACARIAAAYIgBAqIgBBAIgIAEgACDBeQAEgagLgyQgMg4AEgaIgDgEIAAgBIgCgHQgCgOAJgHIAAAAQADgFAEgDQAGAGADAMQAHAUABAiQACAtABAKIAEAZIADAeIgDAOIgHAGQgFAEgHAAIABgHg");
    this.shape_19.setTransform(6.975, 15.475);

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.f("#EED2C6").s().p("ADbBWIgCgDIAAAAIASgOQANgKAGgIQAHgJADgMQAFgMAAgKIAAgBIAAgBQgBgJgGgLQgDgGgDgDIgEgEIgFgEIgBAAIgNgMQgHgCgHgEIgLgGIAAgBIgBAAIgCgBIAAAAIAAgBIgBAAIgDgCQgTgLgfgHQg2gMhEAIQgpAEhPASIhmAXIgNAEIAAAAIgEABIgKADIgDABIgGACQglALgJARQgHAOAFAPIgBABIgEgEIgDgGIAAAAIAAgBIgBAAIAAgBIAAgBIgBgFQAAgOANgLIACgCIACgBQAUgQAtgLQA0gOCZgdQAcgGAdgCQBWgEBFAhIAKAEIABABIAEACIALAHIAEACIAjAdIAEAHQAGAJABAIQAAAIgCAIIgDAKIgBAFQgEAIgIAIIgCADIgDADIgCACIgEAEIgFAFIABgCIgGAEIgVAQIgDACIgEADIgBAAIgBgHgAkXAgIgDgEIAEAEgAkeAVIAAAAIABAAgAEZgLIgEgHIAEAHIAAAAIAAAAg");
    this.shape_20.setTransform(1.9625, -2.6791);

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.f("#A87E6D").s().p("ABmBkIgBgDIAAgOQgBgJgIgMQgKgPgCgEQgCgGACgDQACgDAKAAQAIAAAKgFIAQgJQATgIAhAFIAMAAQAHgCABgFQABgEgFgGQgGgGAAgEIADgJIACgKQADgEALACQAOACAIgDIAGgCIgNgJQgMgIgNgGQgVAKgaADQgTAEgQAAIgPAAQgJAAgGADIgNAJQgHAEgLAAIgUgBQgVAAgUAFQgJACgDADIgHAHQgHAIgIADQgGABgBAEQgCADAFAGIANAOQAIAHABAFQABAEgCAKIgEAHIgGAEQgCADABAFIABACIgDAAIgDgBIgBgDQAAgHACgCIAEgDQADgCABgCIABgHQAAgFADgBQgFgHgHgGQAAAFgEAFIgDAGIgBAGQgBAGgIAFIgKAHIgFAAIgFgBIADgDIAIgFQAFgDADgEQADgDADgJQADgKADgDQgCgBAAgGQAAgCgDgEIgDgGQgCgIAJgEIAOgIIAJgJIgigEQgVgBgQADIgOABQgKgBgCgGQgGgBgJACIgPADQgTAFgJgJIgFgFIgBAAIgVALIgZATQgQAKgLAGIgDACIgBAAIAAAAIgDABIgCABIAAAAIgEACIgBAAIAAABIAAgBIgCgBIACgBQgGgPAHgNQAJgSAlgLIAGgCIADgBIALgDIADgBIABAAIAMgEIBmgXQBQgSApgEQBEgIA1AMQAfAHAUALIACACIABAAIABABIAAAAIACABIAAAAIABABIAKAGQAIAEAGACIAOAMIABAAIAEAEIAFAEQADADADAGQAGALAAAKIAAABIAAABQAAAJgEAMQgEAMgHAJQgGAIgMAKIgSAOIgBgCIgBgBIgEgDQgGgGgIAAIgLgBQgGABgGAEQgDADgDAFIgBABQgIAGABAPIACAHQgGgLABgLIACgIIADgIIAEgFQAJgLANgBIANAAIAOAEQAEACAEADIAGgJQAMgPAEgRQACgKgBgHIgTABIgQADQgJACgEADIgCACQAAAHgCAGIgogBQgVgBgLAFIgPAIQgOAHgRAAQABAHAIALQAJANACAFQABAEACAOIACAKIgFAAgADBgNQAFAFACAIIACgBQATgHAKgCQAKgCAKAAIgEgKQgCgGgGgFIgDABQgNAGgbgDQgBAJgCAHgAgugcIARADIAHADQAFgEAMgCQAZgFANABIATABQALAAAHgDIAOgKQAFgCAHgBQgMgCgIABQgPAAgaAJQABgCAFgCQgCAAgDgBIgSgKIgJgDQgGgCgNABIgKgCIgKgBQgFAAgEgCQgDgBgDgFIgBgBQg4ALg7ASIgQAGIAFAFQAJAGASgGQATgFAIAEIAGAEQAHADAMgDQAJgCAOAAIAYABgABrhWQhEABhGANIgKACIACADQADACAIAAIALABIALACIAOABQAGABALAGIATAJIgBABQAOgFAKgBQALgCARABIAdACQANAAARgCQAJgDASgIIAHgCQgggPglgHIgGAAIgGAAgACug3QAPgDAHgDIAAAAIgEgCIgSAIgAiNA+IgJgCQgugLATAAIADAAQAPgBAAgDQAAgBgBgBQAAgBAAAAQgBAAAAgBQgBAAAAAAQAAAAAAAAQAAAAAAgBQgBAAAAAAQAAAAgBgBIgGgDQgJgIgEgBIgDAAIgTAHIgPAFIAAAAIgEABIgBgBIACgCIgCgBIAAAAQAPgJADgBIAQgCQAKgCAEADIAFADQAFAFAGADIAEACQACABAEAGQACAEADACIAHADIAEADIABAAQgEgBAPAHQgFgCgJACIgEgBg");
    this.shape_21.setTransform(1.404, -1.2855);

    this.shape_22 = new cjs.Shape();
    this.shape_22.graphics.f("#B39082").s().p("ABGBWIhhgSIADAAIgBgCQgBgFACgDIAGgEIAEgHQACgKgBgEQgBgFgIgHIgNgOQgFgGACgCQABgEAGgCQAIgDAHgIIAHgHQADgDAIgCQAVgFAVAAIAUABQALAAAHgEIANgJQAGgDAJAAIAPAAQAQAAATgEQAagDAVgKQANAGAMAIIANAJIgGACQgIADgOgCQgLgCgDAEIgCAKIgDAJQAAAEAGAGQAFAHgBADQgBAFgHACIgMAAQghgFgTAIIgQAJQgKAFgIAAQgKAAgCADQgCADACAGQACAEAKAPQAIAMABAJIAAAOIABADIAFAAIgCgKQgCgOgBgEQgCgFgJgNQgIgLgBgHQARAAAOgHIAPgIQALgFAVABIAoABQACgGgBgGIADgCQAEgEAJgCIAQgDIATgBQABAIgCAJQgEARgMAPIgGAJQgEgDgEgCIgOgEIgNAAQgNABgJALIgEAFIgDAIIgCAIQgBALAGALQgCgDgGACQgUAEgSAAQgXAAgngIgAgWBRIAFAAIAGADIgLgDgAh3AzIgGgBIgEgDIgHgDQgDgCgCgEQgEgGgCgBIgEgCQgGgDgFgFIgFgDQgEgDgKACIgQACQgEABgOAJIALgIQgNgHgJgDQgLgDgOAAIABAAIADgCQALgGAQgKIAZgTIAVgLIABAAIAFAFQAJAJATgFIAPgDQAJgCAGABQACAGAKABIAOgBQAQgDAVABIAiAEIgJAJIgOAIQgJAFACAHIADAGQADAEAAACQAAAGACABQgDADgDAKQgDAJgDADQgDAEgFADIgIAFIgDADgAh2A6IgJgCQAIgCAGACIAEABIABADIgKgCgAkFANIAAgBIABAAIAEgCIgDAEIgCgBgAgTggIgRgDQgfgCgQADQgMADgHgDIgGgEQgIgEgTAFQgSAGgJgGIgGgFIARgGQA6gSA5gLIABABQADAFADABQAEACAFAAIAKABIAKACQANgBAGACIAJADIASAKQADABACAAQgFACgBACQAagJAPAAQAHgBANACQgHABgFACIgOAKQgHADgLAAIgTgBQgNgBgaAFQgLACgFAEIgHgDgAA2g0IgTgJQgLgGgGgBIgPgBIgKgCIgLgBQgIAAgDgCIgCgDIAKgCQBFgNBFgBIAMAAQAlAHAgAPIgHACQgSAIgJADQgRACgNAAIgdgCQgRgBgLACQgKABgOAFIABgBgADKhGIAEACIAAAAQgHADgPADIASgIg");
    this.shape_22.setTransform(0.4409, -0.5783);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_22}, {t: this.shape_21}, {t: this.shape_20}, {t: this.shape_19}, {t: this.shape_18}, {t: this.shape_17}, {t: this.shape_16}, {t: this.shape_15}, {t: this.shape_14}, {t: this.shape_13}, {t: this.shape_12}, {t: this.shape_11}, {t: this.shape_10}, {t: this.shape_9}, {t: this.shape_8}, {t: this.shape_7}, {t: this.shape_6}, {t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    // Layer_2
    this.shape_23 = new cjs.Shape();
    this.shape_23.graphics.f("rgba(124,113,80,0.529)").s().p("Ag1BeIgpgZQgJgFgKgBQgHAAgWACQgnACgTgZIgJgOQgFgJgFgEQgIgHgQgDQgNgCgcgBQgbAAgOgDQgRgDgLgIQgOgKgDgNQgEgNAGgNQAGgMAMgJQAQgMAigGQBegQBJAQIA6AQIBGAPQArAHAZAIIAgALQATAFANACQAUADAngCIBSgEIAggBQARACAOAEQAPAGALAMQAMAMABAOIAAAHIABAEQACAGAAAFIgBAOIgFAMQgIAMgGAFQgLAKgcAIIgSACIgHgBQgLADgKgCQgOgCgFABQgEAAgLAIQgPAJgRgGIgUgKQgMgFgJAAQgGAAgSAKQgVAKgYgCQgYgCgSgOQgJgFgCAAQgDAAgIAGQgSAPgVABQgTAAgXgPgAgUAnQAGAEAEgBIAGgCIgEgFQgGgEgMgCIgPgDgAiugjIACgCIgFgBIADADg");
    this.shape_23.setTransform(-1.4145, 18);

    this.timeline.addTween(cjs.Tween.get(this.shape_23).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-38.7, -13, 74.6, 41.9);


  (lib.Symbol10copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#6B0F0F").ss(0.5, 1, 1).p("ADzigIgCgBQgEgCgFgCQhGghhVAEQgdACgcAGQiaAdgzAOQgtAMgVAPIgBABQgBABgBABQgOAMAAAOQAAACABADQABABAAAAIAAABQAAAAAAAAAC6AOQgCgBgBgDQgBAAAAAAQgBgDgBgEQgCgNAJgHQABAAAAgBQADgEADgEQAFgEAHgBQACAAAJABQAIAAAGAGQACACACABQABABAAAAQABABAAABAE0hRQAAgDAAgDQAAgEAAgEQgCgJgFgJQgCgDgDgEIgigdQgCgBgCgBQgGgEgGgDADtgSQAAABACACQABACAAAFQAAAAAAAAQADgBACgCQAHgEAGgEQALgIAJgJIAFgEQABgBAAgBQACgBACgCQABgBABgCQAHgIAEgIQAGgIABgJQAEAUgHAhQgCAOgEAOQgEALgEALQgFANAAAUQAAAMABAZIABAAQAGABATARQAWATgDAHQgPAUgKAGQgRALgRAEQgRAEgEgEQgBgBgDgHQAAAAgBgIQgDgIAAgFQgBgGACgEQAEgIAPgRQACgCACgCQAKgLAFgEQACgBABgBQAAAAABAAADwgIQgCApgGAxQgBAMgBALQgEAcgFAXQgBAIgCAHQABgBAAgBQANgRABgBQACgBACgBQACAAACgBQABAAACAAQAOgCAKgDQAOgDAHAAQAJAAAJAEQAJAEALgHQADgCACgDADIC1QAHAAAFgEQAEgCACgEABsAaQABAAAAAAQABAAABAAQABAAAAAAQALAAALgBIAFgBQADAAACAAIAPgEIAXgEQACgBACgBQgEAaAMA6QAKAygDAZQAAAFgBADAgGADQAAAAAAAAQAEABACABIAQAFQAAAAAAAAQABAAABAAIAAAAQABABABAAQAIADAHACQAMADAMADIABAAQACAAABAAQAWAEAWAAAgSC2QADgCAGgDQABgBACgBQABAAAAAAQAMgHAJAAQAHAAAKAEQAGAEAIAFIACABQADACACABQAQAKALgBQAHgBALgHABmC4IAFgEQADgCADgCQAJgHAJgCQACAAACgBQAIAAAPAFQAEABAFACQAWAHALAAAkJhPIADAGQABACACACQABABABAAIAAABQABABABAAIAEADAj5g+QAJAHAPAFQADACAEABQALAEAOADQACABADABQAIACAIABQAOADAMADIAKACIAJACAkLhSQABAPgCAeQgCAkgEANQAAABgBABAh7gYQAFABAFABQAEABAFABQAEABAFABQACABACAAQAFABAEABIAaAHQAVAFAPADAgSAAQAAAhABASIABAXQAAAEgCAoQAAAVAAArAjOBsQAJALAIAGQAKAJATACQAHABAFABQANACAEABQABAAAAAAQAMAFAWAMQAGADAGAEQACAAABABQAFADAFADAgbC5QgJgHgKgBQgNgBgJgCQgCgBgBgBQAAgCAAgCQACgPAVgBQAHAAAHAIQAIAHABAHQAAACgCAJIAAABQgWANgJgGQgNgKAAgLAgbC6QACgBAHgDAhSgPQgBAZAAArQABAugCBCAkTARIgCADQAAABAAABQgBACgBACQgDAKgBAEQgCgCgEgLQgDgIgHAAQgBAAgBAAQgBADAEATIABABQAFAZAAADQAQABAUAAQAVACAAAAQAAAAABABQABABABABQACACACADQAHAHAIALQAEAFADAEAjuBFQgIgHgNgHQgRgJgHgGAjrBJIgCgCAktATQgEACgFAHQgCAFgFAIQgBABgEAEQgCACgCACQgIAIADABQALAIAcADAjBgoQgEAagBAvQgBAngHAk");
    this.shape.setTransform(0.0114, 7.5015);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#B18E81").s().p("AABAAIgBAAIABAAg");
    this.shape_1.setTransform(6.1875, 9.725);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#744331").s().p("AhJBOIAAgKIABgXQAFgygCgwQAAgVABgGIABgFIABABIACAAIABAAQAWAEAWAAIgDAKQgFAVgBAMIABAzQACAlgCAYIAAAEQgLAHgHABIgCAAQgLAAgPgJgAAoBDIgJgDIgEhJIgDhLIgMACIAFgBIAPgDIAEAYIAGATQAJAYACAKIAFAgQAEAUAFANIAFAJIACABIgBAIIgCAAQgLAAgUgHg");
    this.shape_2.setTransform(12.7313, 18.2283);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#6A4031").s().p("AgjAUIgEgIIgBgIQgDgHAAgGIAYgFQAOgDAGAAQAJABAJAEQAJADALgHIAFgEQgPAUgKAFQgRALgQAEIgOABQgFAAgCgBg");
    this.shape_3.setTransform(28.825, 23.2875);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#7B4D39").s().p("AgYA3QAEgYAEgcIAPgIQAOgJADgKQACgFAAgKIABgPQABgEAHgKIAAAJIABANQAAAGgHAPQgEAJAAAJQAAAIAEAGIgEAFQgPARgDAIQgBAEAAAFIgDABIgEAAIgDACIgOATIgBABIADgOg");
    this.shape_4.setTransform(24.6813, 17.6875);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#5C2C1B").s().p("AACAOQgRgKgHgEIAEgPIACgDIABgDIABgDIABgDIAEAEIAJAHIANAKQAGAEADAEIAAAaQgIgIgMgGg");
    this.shape_5.setTransform(-26.1, 11.6875);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#794531").s().p("AgrA/QgNgBgJgCIgDgBIAAgFQACgPAVAAQAHgBAHAIQAIAHABAIIgCALQgJgHgKgCgAD4AZQAEgIAPgRIAEgEQAKgLAFgEIADgBIABAAIABAAIAAAAQAAAFAEAFIAFAGIABAHQAAAJADAJQABAFAEABIABADQAAAAABABQAAAAAAAAQABAAAAAAQABAAAAAAIAHgDQAFgDAFACQgLAHgJgDQgJgEgJgBQgHAAgOADIgYAFQgBgFACgEgAlIg1QgDgCAIgHIAEgEIABAAQAGADAJgCIAJgDIAAgDIAFAcQgcgCgLgIg");
    this.shape_6.setTransform(-0.2812, 18.875);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#573325").s().p("ABqBWQgNgKAAgLIADACQAJACANABQAKABAJAHIAAABQgPAJgJAAQgEAAgDgCgAhbgjQgUAAgQgBIgGgcIAAgBQgEgTABgDIACAAQAHAAADAIQAEALACACQAHAGARAJQANAHAHAHIACACIACACIgBAAIgUgCg");
    this.shape_7.setTransform(-16.4687, 18.2056);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#683B2A").s().p("AgYBYIAFADIAAAAIgFgDgAgYBYIgBgBIgPgJQgKgFgHABQgJAAgMAHIgBAAIgEACIAAgGQADgVACg/IAAgdQgCgiABgRIAAgDIAFACIARAEIAAABIACAAIABAAIABABIAPAFIAYAGIgBAEQgBAGAAAVQACAvgEAzIgCAXIABAKIgFgDgAgYBYIAAAAgAAggBQgBgeADgkIACAAIABAAIACAAIAVgBIAFgBIANgBIACBJIAEBKQgPgFgHAAIgFABQgIACgKAHQgHg2AAgdg");
    this.shape_8.setTransform(7.4, 16.925);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.f("#9D7B6E").s().p("AAuASIgBAAIgCAAIAAAAQgXAAgUgEIAAAAQAOABAdgBIAgACIAFgBIgGABIgEAAIgWACIgCAAgAB2ACIAAAAIADAEIgEABQABAAAAgBQAAgBABAAQAAgBAAAAQAAgBgBgBgAhAgDIgGgBIgBgBIgHgCIgFgBIgBAAIgkgJIAIACQAcAFAWAHIAOAEg");
    this.shape_9.setTransform(6.475, 8.3625);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.f("#825441").s().p("AgFBOIgBgBIgRgDQgEgqAAgiQAAgwAGgrIAKACIAJACIADABIAJACIAKACIAIACIAAAHIgCAvQAAAoACBSQgWgMgLgEg");
    this.shape_10.setTransform(-12.4625, 13.8875);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.f("#633320").s().p("AABBGIgOgTIgEgFIgDgCIABgBIAKhCIAEgsIABgKIAZAHQgEAbgBAuQgBAngHAkIgHgIg");
    this.shape_11.setTransform(-21.4625, 10.5);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.f("#7B452F").s().p("AADA3IgMgKIgKgIIgFgDIABgCQAEgPACgiQACgegBgPIABABIAAAAIAAABIAAAAIAAAAIABABIADAGIADADIABACIABAAIAAAAIACACIAEADIAAABQAKAGAPAGIAAAAQgNA9AAAcIAAAEQgDgEgGgEg");
    this.shape_12.setTransform(-25.075, 5.55);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.f("#82655B").s().p("ACQAvQgdABgPgBIAAAAIgDgBIgBAAIgYgGIgQgFIgBAAIAAAAIgCgBIgBAAIgOgFQgWgHgbgFIgIgCIgagGIgKgCIgEgBIgJgCIAAgDIgEgBQgPgGADAAIAAAAIAGABIA0AJIAGABIAeAGIACAAIBjATQAmAHAYAAQARAAAUgEQAHgCACADIAAABIAAAAQABABAAABQAAAAAAABQAAAAgBABQAAABgBAAIgWAFIgPADIgEABIghgCgAiugaQgKgFgHgCQgJgDgNgBIgBgBIgFgDIAEgEIAAAAIACgBIADgCIAAAAQAOAAALAEQAIACANAIIgKAIg");
    this.shape_13.setTransform(-3.6283, 5.0625);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.f("#9D6753").s().p("ABeCCQACgYgCglIgBg0QABgMAFgUIADgKIABAAQgDAjABAeQAAAeAHA2IgGAEIgFAEIgDACIAAgEgAgXBCIACgsIgBgWQgBgSAAghIAMADIAAAAIAAADQgBARADAhIAAAdQgCBAgDAVIAAAGIgJAFIAAhAgADCB4IgFgJQgFgNgEgUIgFghQgCgKgJgYIgGgSIgEgYIAXgFIAEgBQgEAaAMA5QAKAxgDAaIgCgBgAhlBqQgChTAAgnIACgvIABgHIAEABIAJACQgBAaAAAqQABAtgCBDIgMgHgAEuBbIgBgDQgEgBgBgFQgDgJAAgJIgBgIIgFgFQgEgFAAgFIAAgBQAGABATARQAWATgDAHIgFAFQgFgDgFADIgHAEIAAAAQgBAAAAAAQAAAAgBgBQAAAAAAAAQgBAAAAgBgAEDApQAAgIAEgKQAGgOABgGIgBgNIAAgIQAAgRAHgZIAIgdQgZAYgQAEQgDABgFAAIAFgDIANgJQALgHAJgJIAFgEIABgCIAEgDIACgDQAHgIAEgIQAGgJABgIQAEAUgHAhIgGAbIgIAYQgFANAAATIABAlIgBAAIgDACQgFAEgKAKQgEgGAAgJgAjwAVIgCgCIgBgCIAAgZIAAgEQAAgcANg9IAAgBIAHADIgCAKIgEAsIgKBBIAAABIgBAAgAlGgFIgBAAIAFgFIAHgNQAFgHAEgCQgBADAEATIABABIAAACIgJADIgGABQgFAAgEgCg");
    this.shape_14.setTransform(0.5051, 12.725);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.f("#94614C").s().p("AjJBYQgTgDgKgIQgIgHgJgLQAHgkABglQABgwAEgaIAFABIAQAEIAaAFQgGArAAAvQAAAjAEAqIgMgBgAC/AiQAGgxACgpIAAAAQAEAAAEgBQAQgEAZgZIgIAdQgHAaAAAQQgHAKgBAEIgBAOQAAAKgCAFQgEAKgOAJIgPAJIACgWg");
    this.shape_15.setTransform(4.125, 12.375);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.f("#542716").s().p("AihBhQABhCAAgsQgBgrACgaIAaAGIAkAJQgFAGgDANQgFAagCAZIgDA+QgMABgLAGQgLAHgGALIgEAJIgCgCgACNA8IgEgZQgBgLgCgsQgBgigHgVQgDgMgGgGQAFgEAHgBIAKABQAIAAAGAGIAFADIAAABIABACIAAAAIACADIABAHQgCAqgFAwIgDAXQgEAcgEAXIgDgdg");
    this.shape_16.setTransform(7.775, 14.2375);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.f("#BE9787").s().p("ADfAoIAWgQIAFgEIgBACIAFgFQgJAJgLAIIgNAIIACgCgAEXgPIACgKQACgIAAgIIABAIIAAAGQgCAIgFAJIACgFgAkagOIgBgBIAAAAIACABg");
    this.shape_17.setTransform(2.425, 2.1125);

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.f("#543C32").s().p("AhAAwIAEABIALgHQAIgFABgGIAAgGIAEgGQADgFABgFQAHAGAEAHQgDABAAAFIAAAHQgBACgDACIgFADQgBACAAAHIABADgAiZAmIgagGIgRgEIgFgBIgZgHIgHgDQgOgGgKgGQANABAJAEQAHACAKAEIABABIgCACIABABIAFgBIgBgBIAQgEIASgHIAEAAQADABAKAIIAFADQABAAAAABQABAAAAAAQAAAAAAABQAAAAAAAAQABAAAAAAQABAAAAABQAAAAABABQAAAAAAABQABAEgQAAIgDAAQgTABAuALgADHghQADgHABgJQAaADAOgGIACgBQAGAFADAGIAEAJQgLABgKACQgKACgTAHIgBABQgCgIgGgFg");
    this.shape_18.setTransform(0.7625, 0.7375);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.f("#462215").s().p("AhgBqIABgLQgBgIgHgHQgIgIgHABQgUAAgCAPIAAAFIgLgHIAEgIQAGgLALgHQALgHAMgBIADg8QACgaAFgbQADgNAFgFIABAAQAAAiACARIAAAYIgBAqIgBBAIgIAEgACDBeQAEgagLgyQgMg4AEgaIgDgEIAAgBIgCgHQgCgOAJgHIAAAAQADgFAEgDQAGAGADAMQAHAUABAiQACAtABAKIAEAZIADAeIgDAOIgHAGQgFAEgHAAIABgHg");
    this.shape_19.setTransform(6.975, 15.475);

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.f("#EED2C6").s().p("ADbBWIgCgDIAAAAIASgOQANgKAGgIQAHgJADgMQAFgMAAgKIAAgBIAAgBQgBgJgGgLQgDgGgDgDIgEgEIgFgEIgBAAIgNgMQgHgCgHgEIgLgGIAAgBIgBAAIgCgBIAAAAIAAgBIgBAAIgDgCQgTgLgfgHQg2gMhEAIQgpAEhPASIhmAXIgNAEIAAAAIgEABIgKADIgDABIgGACQglALgJARQgHAOAFAPIgBABIgEgEIgDgGIAAAAIAAgBIgBAAIAAgBIAAgBIgBgFQAAgOANgLIACgCIACgBQAUgQAtgLQA0gOCZgdQAcgGAdgCQBWgEBFAhIAKAEIABABIAEACIALAHIAEACIAjAdIAEAHQAGAJABAIQAAAIgCAIIgDAKIgBAFQgEAIgIAIIgCADIgDADIgCACIgEAEIgFAFIABgCIgGAEIgVAQIgDACIgEADIgBAAIgBgHgAkXAgIgDgEIAEAEgAkeAVIAAAAIABAAgAEZgLIgEgHIAEAHIAAAAIAAAAg");
    this.shape_20.setTransform(1.9625, -2.6791);

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.f("#A87E6D").s().p("ABmBkIgBgDIAAgOQgBgJgIgMQgKgPgCgEQgCgGACgDQACgDAKAAQAIAAAKgFIAQgJQATgIAhAFIAMAAQAHgCABgFQABgEgFgGQgGgGAAgEIADgJIACgKQADgEALACQAOACAIgDIAGgCIgNgJQgMgIgNgGQgVAKgaADQgTAEgQAAIgPAAQgJAAgGADIgNAJQgHAEgLAAIgUgBQgVAAgUAFQgJACgDADIgHAHQgHAIgIADQgGABgBAEQgCADAFAGIANAOQAIAHABAFQABAEgCAKIgEAHIgGAEQgCADABAFIABACIgDAAIgDgBIgBgDQAAgHACgCIAEgDQADgCABgCIABgHQAAgFADgBQgFgHgHgGQAAAFgEAFIgDAGIgBAGQgBAGgIAFIgKAHIgFAAIgFgBIADgDIAIgFQAFgDADgEQADgDADgJQADgKADgDQgCgBAAgGQAAgCgDgEIgDgGQgCgIAJgEIAOgIIAJgJIgigEQgVgBgQADIgOABQgKgBgCgGQgGgBgJACIgPADQgTAFgJgJIgFgFIgBAAIgVALIgZATQgQAKgLAGIgDACIgBAAIAAAAIgDABIgCABIAAAAIgEACIgBAAIAAABIAAgBIgCgBIACgBQgGgPAHgNQAJgSAlgLIAGgCIADgBIALgDIADgBIABAAIAMgEIBmgXQBQgSApgEQBEgIA1AMQAfAHAUALIACACIABAAIABABIAAAAIACABIAAAAIABABIAKAGQAIAEAGACIAOAMIABAAIAEAEIAFAEQADADADAGQAGALAAAKIAAABIAAABQAAAJgEAMQgEAMgHAJQgGAIgMAKIgSAOIgBgCIgBgBIgEgDQgGgGgIAAIgLgBQgGABgGAEQgDADgDAFIgBABQgIAGABAPIACAHQgGgLABgLIACgIIADgIIAEgFQAJgLANgBIANAAIAOAEQAEACAEADIAGgJQAMgPAEgRQACgKgBgHIgTABIgQADQgJACgEADIgCACQAAAHgCAGIgogBQgVgBgLAFIgPAIQgOAHgRAAQABAHAIALQAJANACAFQABAEACAOIACAKIgFAAgADBgNQAFAFACAIIACgBQATgHAKgCQAKgCAKAAIgEgKQgCgGgGgFIgDABQgNAGgbgDQgBAJgCAHgAgugcIARADIAHADQAFgEAMgCQAZgFANABIATABQALAAAHgDIAOgKQAFgCAHgBQgMgCgIABQgPAAgaAJQABgCAFgCQgCAAgDgBIgSgKIgJgDQgGgCgNABIgKgCIgKgBQgFAAgEgCQgDgBgDgFIgBgBQg4ALg7ASIgQAGIAFAFQAJAGASgGQATgFAIAEIAGAEQAHADAMgDQAJgCAOAAIAYABgABrhWQhEABhGANIgKACIACADQADACAIAAIALABIALACIAOABQAGABALAGIATAJIgBABQAOgFAKgBQALgCARABIAdACQANAAARgCQAJgDASgIIAHgCQgggPglgHIgGAAIgGAAgACug3QAPgDAHgDIAAAAIgEgCIgSAIgAiNA+IgJgCQgugLATAAIADAAQAPgBAAgDQAAgBgBgBQAAgBAAAAQgBAAAAgBQgBAAAAAAQAAAAAAAAQAAAAAAgBQgBAAAAAAQAAAAgBgBIgGgDQgJgIgEgBIgDAAIgTAHIgPAFIAAAAIgEABIgBgBIACgCIgCgBIAAAAQAPgJADgBIAQgCQAKgCAEADIAFADQAFAFAGADIAEACQACABAEAGQACAEADACIAHADIAEADIABAAQgEgBAPAHQgFgCgJACIgEgBg");
    this.shape_21.setTransform(1.404, -1.2855);

    this.shape_22 = new cjs.Shape();
    this.shape_22.graphics.f("#B39082").s().p("ABGBWIhhgSIADAAIgBgCQgBgFACgDIAGgEIAEgHQACgKgBgEQgBgFgIgHIgNgOQgFgGACgCQABgEAGgCQAIgDAHgIIAHgHQADgDAIgCQAVgFAVAAIAUABQALAAAHgEIANgJQAGgDAJAAIAPAAQAQAAATgEQAagDAVgKQANAGAMAIIANAJIgGACQgIADgOgCQgLgCgDAEIgCAKIgDAJQAAAEAGAGQAFAHgBADQgBAFgHACIgMAAQghgFgTAIIgQAJQgKAFgIAAQgKAAgCADQgCADACAGQACAEAKAPQAIAMABAJIAAAOIABADIAFAAIgCgKQgCgOgBgEQgCgFgJgNQgIgLgBgHQARAAAOgHIAPgIQALgFAVABIAoABQACgGgBgGIADgCQAEgEAJgCIAQgDIATgBQABAIgCAJQgEARgMAPIgGAJQgEgDgEgCIgOgEIgNAAQgNABgJALIgEAFIgDAIIgCAIQgBALAGALQgCgDgGACQgUAEgSAAQgXAAgngIgAgWBRIAFAAIAGADIgLgDgAh3AzIgGgBIgEgDIgHgDQgDgCgCgEQgEgGgCgBIgEgCQgGgDgFgFIgFgDQgEgDgKACIgQACQgEABgOAJIALgIQgNgHgJgDQgLgDgOAAIABAAIADgCQALgGAQgKIAZgTIAVgLIABAAIAFAFQAJAJATgFIAPgDQAJgCAGABQACAGAKABIAOgBQAQgDAVABIAiAEIgJAJIgOAIQgJAFACAHIADAGQADAEAAACQAAAGACABQgDADgDAKQgDAJgDADQgDAEgFADIgIAFIgDADgAh2A6IgJgCQAIgCAGACIAEABIABADIgKgCgAkFANIAAgBIABAAIAEgCIgDAEIgCgBgAgTggIgRgDQgfgCgQADQgMADgHgDIgGgEQgIgEgTAFQgSAGgJgGIgGgFIARgGQA6gSA5gLIABABQADAFADABQAEACAFAAIAKABIAKACQANgBAGACIAJADIASAKQADABACAAQgFACgBACQAagJAPAAQAHgBANACQgHABgFACIgOAKQgHADgLAAIgTgBQgNgBgaAFQgLACgFAEIgHgDgAA2g0IgTgJQgLgGgGgBIgPgBIgKgCIgLgBQgIAAgDgCIgCgDIAKgCQBFgNBFgBIAMAAQAlAHAgAPIgHACQgSAIgJADQgRACgNAAIgdgCQgRgBgLACQgKABgOAFIABgBgADKhGIAEACIAAAAQgHADgPADIASgIg");
    this.shape_22.setTransform(0.4409, -0.5783);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_22}, {t: this.shape_21}, {t: this.shape_20}, {t: this.shape_19}, {t: this.shape_18}, {t: this.shape_17}, {t: this.shape_16}, {t: this.shape_15}, {t: this.shape_14}, {t: this.shape_13}, {t: this.shape_12}, {t: this.shape_11}, {t: this.shape_10}, {t: this.shape_9}, {t: this.shape_8}, {t: this.shape_7}, {t: this.shape_6}, {t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    // Layer_2
    this.shape_23 = new cjs.Shape();
    this.shape_23.graphics.f("rgba(124,113,80,0.529)").s().p("Ag1BeIgpgZQgJgFgKgBQgHAAgWACQgnACgTgZIgJgOQgFgJgFgEQgIgHgQgDQgNgCgcgBQgbAAgOgDQgRgDgLgIQgOgKgDgNQgEgNAGgNQAGgMAMgJQAQgMAigGQBegQBJAQIA6AQIBGAPQArAHAZAIIAgALQATAFANACQAUADAngCIBSgEIAggBQARACAOAEQAPAGALAMQAMAMABAOIAAAHIABAEQACAGAAAFIgBAOIgFAMQgIAMgGAFQgLAKgcAIIgSACIgHgBQgLADgKgCQgOgCgFABQgEAAgLAIQgPAJgRgGIgUgKQgMgFgJAAQgGAAgSAKQgVAKgYgCQgYgCgSgOQgJgFgCAAQgDAAgIAGQgSAPgVABQgTAAgXgPgAgUAnQAGAEAEgBIAGgCIgEgFQgGgEgMgCIgPgDgAiugjIACgCIgFgBIADADg");
    this.shape_23.setTransform(-1.4145, 18);

    this.timeline.addTween(cjs.Tween.get(this.shape_23).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-38.7, -13, 74.6, 41.9);


  (lib.Symbol10copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#6B0F0F").ss(0.5, 1, 1).p("ADzigIgCgBQgEgCgFgCQhGghhVAEQgdACgcAGQiaAdgzAOQgtAMgVAPIgBABQgBABgBABQgOAMAAAOQAAACABADQABABAAAAIAAABQAAAAAAAAAC6AOQgCgBgBgDQgBAAAAAAQgBgDgBgEQgCgNAJgHQABAAAAgBQADgEADgEQAFgEAHgBQACAAAJABQAIAAAGAGQACACACABQABABAAAAQABABAAABAE0hRQAAgDAAgDQAAgEAAgEQgCgJgFgJQgCgDgDgEIgigdQgCgBgCgBQgGgEgGgDADtgSQAAABACACQABACAAAFQAAAAAAAAQADgBACgCQAHgEAGgEQALgIAJgJIAFgEQABgBAAgBQACgBACgCQABgBABgCQAHgIAEgIQAGgIABgJQAEAUgHAhQgCAOgEAOQgEALgEALQgFANAAAUQAAAMABAZIABAAQAGABATARQAWATgDAHQgPAUgKAGQgRALgRAEQgRAEgEgEQgBgBgDgHQAAAAgBgIQgDgIAAgFQgBgGACgEQAEgIAPgRQACgCACgCQAKgLAFgEQACgBABgBQAAAAABAAADwgIQgCApgGAxQgBAMgBALQgEAcgFAXQgBAIgCAHQABgBAAgBQANgRABgBQACgBACgBQACAAACgBQABAAACAAQAOgCAKgDQAOgDAHAAQAJAAAJAEQAJAEALgHQADgCACgDADIC1QAHAAAFgEQAEgCACgEABsAaQABAAAAAAQABAAABAAQABAAAAAAQALAAALgBIAFgBQADAAACAAIAPgEIAXgEQACgBACgBQgEAaAMA6QAKAygDAZQAAAFgBADAgGADQAAAAAAAAQAEABACABIAQAFQAAAAAAAAQABAAABAAIAAAAQABABABAAQAIADAHACQAMADAMADIABAAQACAAABAAQAWAEAWAAAgSC2QADgCAGgDQABgBACgBQABAAAAAAQAMgHAJAAQAHAAAKAEQAGAEAIAFIACABQADACACABQAQAKALgBQAHgBALgHABmC4IAFgEQADgCADgCQAJgHAJgCQACAAACgBQAIAAAPAFQAEABAFACQAWAHALAAAkJhPIADAGQABACACACQABABABAAIAAABQABABABAAIAEADAj5g+QAJAHAPAFQADACAEABQALAEAOADQACABADABQAIACAIABQAOADAMADIAKACIAJACAkLhSQABAPgCAeQgCAkgEANQAAABgBABAh7gYQAFABAFABQAEABAFABQAEABAFABQACABACAAQAFABAEABIAaAHQAVAFAPADAgSAAQAAAhABASIABAXQAAAEgCAoQAAAVAAArAjOBsQAJALAIAGQAKAJATACQAHABAFABQANACAEABQABAAAAAAQAMAFAWAMQAGADAGAEQACAAABABQAFADAFADAgbC5QgJgHgKgBQgNgBgJgCQgCgBgBgBQAAgCAAgCQACgPAVgBQAHAAAHAIQAIAHABAHQAAACgCAJIAAABQgWANgJgGQgNgKAAgLAgbC6QACgBAHgDAhSgPQgBAZAAArQABAugCBCAkTARIgCADQAAABAAABQgBACgBACQgDAKgBAEQgCgCgEgLQgDgIgHAAQgBAAgBAAQgBADAEATIABABQAFAZAAADQAQABAUAAQAVACAAAAQAAAAABABQABABABABQACACACADQAHAHAIALQAEAFADAEAjuBFQgIgHgNgHQgRgJgHgGAjrBJIgCgCAktATQgEACgFAHQgCAFgFAIQgBABgEAEQgCACgCACQgIAIADABQALAIAcADAjBgoQgEAagBAvQgBAngHAk");
    this.shape.setTransform(0.0114, 7.5015);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#B18E81").s().p("AABAAIgBAAIABAAg");
    this.shape_1.setTransform(6.1875, 9.725);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#744331").s().p("AhJBOIAAgKIABgXQAFgygCgwQAAgVABgGIABgFIABABIACAAIABAAQAWAEAWAAIgDAKQgFAVgBAMIABAzQACAlgCAYIAAAEQgLAHgHABIgCAAQgLAAgPgJgAAoBDIgJgDIgEhJIgDhLIgMACIAFgBIAPgDIAEAYIAGATQAJAYACAKIAFAgQAEAUAFANIAFAJIACABIgBAIIgCAAQgLAAgUgHg");
    this.shape_2.setTransform(12.7313, 18.2283);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#6A4031").s().p("AgjAUIgEgIIgBgIQgDgHAAgGIAYgFQAOgDAGAAQAJABAJAEQAJADALgHIAFgEQgPAUgKAFQgRALgQAEIgOABQgFAAgCgBg");
    this.shape_3.setTransform(28.825, 23.2875);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#7B4D39").s().p("AgYA3QAEgYAEgcIAPgIQAOgJADgKQACgFAAgKIABgPQABgEAHgKIAAAJIABANQAAAGgHAPQgEAJAAAJQAAAIAEAGIgEAFQgPARgDAIQgBAEAAAFIgDABIgEAAIgDACIgOATIgBABIADgOg");
    this.shape_4.setTransform(24.6813, 17.6875);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#5C2C1B").s().p("AACAOQgRgKgHgEIAEgPIACgDIABgDIABgDIABgDIAEAEIAJAHIANAKQAGAEADAEIAAAaQgIgIgMgGg");
    this.shape_5.setTransform(-26.1, 11.6875);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#794531").s().p("AgrA/QgNgBgJgCIgDgBIAAgFQACgPAVAAQAHgBAHAIQAIAHABAIIgCALQgJgHgKgCgAD4AZQAEgIAPgRIAEgEQAKgLAFgEIADgBIABAAIABAAIAAAAQAAAFAEAFIAFAGIABAHQAAAJADAJQABAFAEABIABADQAAAAABABQAAAAAAAAQABAAAAAAQABAAAAAAIAHgDQAFgDAFACQgLAHgJgDQgJgEgJgBQgHAAgOADIgYAFQgBgFACgEgAlIg1QgDgCAIgHIAEgEIABAAQAGADAJgCIAJgDIAAgDIAFAcQgcgCgLgIg");
    this.shape_6.setTransform(-0.2812, 18.875);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#573325").s().p("ABqBWQgNgKAAgLIADACQAJACANABQAKABAJAHIAAABQgPAJgJAAQgEAAgDgCgAhbgjQgUAAgQgBIgGgcIAAgBQgEgTABgDIACAAQAHAAADAIQAEALACACQAHAGARAJQANAHAHAHIACACIACACIgBAAIgUgCg");
    this.shape_7.setTransform(-16.4687, 18.2056);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#683B2A").s().p("AgYBYIAFADIAAAAIgFgDgAgYBYIgBgBIgPgJQgKgFgHABQgJAAgMAHIgBAAIgEACIAAgGQADgVACg/IAAgdQgCgiABgRIAAgDIAFACIARAEIAAABIACAAIABAAIABABIAPAFIAYAGIgBAEQgBAGAAAVQACAvgEAzIgCAXIABAKIgFgDgAgYBYIAAAAgAAggBQgBgeADgkIACAAIABAAIACAAIAVgBIAFgBIANgBIACBJIAEBKQgPgFgHAAIgFABQgIACgKAHQgHg2AAgdg");
    this.shape_8.setTransform(7.4, 16.925);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.f("#9D7B6E").s().p("AAuASIgBAAIgCAAIAAAAQgXAAgUgEIAAAAQAOABAdgBIAgACIAFgBIgGABIgEAAIgWACIgCAAgAB2ACIAAAAIADAEIgEABQABAAAAgBQAAgBABAAQAAgBAAAAQAAgBgBgBgAhAgDIgGgBIgBgBIgHgCIgFgBIgBAAIgkgJIAIACQAcAFAWAHIAOAEg");
    this.shape_9.setTransform(6.475, 8.3625);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.f("#825441").s().p("AgFBOIgBgBIgRgDQgEgqAAgiQAAgwAGgrIAKACIAJACIADABIAJACIAKACIAIACIAAAHIgCAvQAAAoACBSQgWgMgLgEg");
    this.shape_10.setTransform(-12.4625, 13.8875);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.f("#633320").s().p("AABBGIgOgTIgEgFIgDgCIABgBIAKhCIAEgsIABgKIAZAHQgEAbgBAuQgBAngHAkIgHgIg");
    this.shape_11.setTransform(-21.4625, 10.5);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.f("#7B452F").s().p("AADA3IgMgKIgKgIIgFgDIABgCQAEgPACgiQACgegBgPIABABIAAAAIAAABIAAAAIAAAAIABABIADAGIADADIABACIABAAIAAAAIACACIAEADIAAABQAKAGAPAGIAAAAQgNA9AAAcIAAAEQgDgEgGgEg");
    this.shape_12.setTransform(-25.075, 5.55);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.f("#82655B").s().p("ACQAvQgdABgPgBIAAAAIgDgBIgBAAIgYgGIgQgFIgBAAIAAAAIgCgBIgBAAIgOgFQgWgHgbgFIgIgCIgagGIgKgCIgEgBIgJgCIAAgDIgEgBQgPgGADAAIAAAAIAGABIA0AJIAGABIAeAGIACAAIBjATQAmAHAYAAQARAAAUgEQAHgCACADIAAABIAAAAQABABAAABQAAAAAAABQAAAAgBABQAAABgBAAIgWAFIgPADIgEABIghgCgAiugaQgKgFgHgCQgJgDgNgBIgBgBIgFgDIAEgEIAAAAIACgBIADgCIAAAAQAOAAALAEQAIACANAIIgKAIg");
    this.shape_13.setTransform(-3.6283, 5.0625);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.f("#9D6753").s().p("ABeCCQACgYgCglIgBg0QABgMAFgUIADgKIABAAQgDAjABAeQAAAeAHA2IgGAEIgFAEIgDACIAAgEgAgXBCIACgsIgBgWQgBgSAAghIAMADIAAAAIAAADQgBARADAhIAAAdQgCBAgDAVIAAAGIgJAFIAAhAgADCB4IgFgJQgFgNgEgUIgFghQgCgKgJgYIgGgSIgEgYIAXgFIAEgBQgEAaAMA5QAKAxgDAaIgCgBgAhlBqQgChTAAgnIACgvIABgHIAEABIAJACQgBAaAAAqQABAtgCBDIgMgHgAEuBbIgBgDQgEgBgBgFQgDgJAAgJIgBgIIgFgFQgEgFAAgFIAAgBQAGABATARQAWATgDAHIgFAFQgFgDgFADIgHAEIAAAAQgBAAAAAAQAAAAgBgBQAAAAAAAAQgBAAAAgBgAEDApQAAgIAEgKQAGgOABgGIgBgNIAAgIQAAgRAHgZIAIgdQgZAYgQAEQgDABgFAAIAFgDIANgJQALgHAJgJIAFgEIABgCIAEgDIACgDQAHgIAEgIQAGgJABgIQAEAUgHAhIgGAbIgIAYQgFANAAATIABAlIgBAAIgDACQgFAEgKAKQgEgGAAgJgAjwAVIgCgCIgBgCIAAgZIAAgEQAAgcANg9IAAgBIAHADIgCAKIgEAsIgKBBIAAABIgBAAgAlGgFIgBAAIAFgFIAHgNQAFgHAEgCQgBADAEATIABABIAAACIgJADIgGABQgFAAgEgCg");
    this.shape_14.setTransform(0.5051, 12.725);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.f("#94614C").s().p("AjJBYQgTgDgKgIQgIgHgJgLQAHgkABglQABgwAEgaIAFABIAQAEIAaAFQgGArAAAvQAAAjAEAqIgMgBgAC/AiQAGgxACgpIAAAAQAEAAAEgBQAQgEAZgZIgIAdQgHAaAAAQQgHAKgBAEIgBAOQAAAKgCAFQgEAKgOAJIgPAJIACgWg");
    this.shape_15.setTransform(4.125, 12.375);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.f("#542716").s().p("AihBhQABhCAAgsQgBgrACgaIAaAGIAkAJQgFAGgDANQgFAagCAZIgDA+QgMABgLAGQgLAHgGALIgEAJIgCgCgACNA8IgEgZQgBgLgCgsQgBgigHgVQgDgMgGgGQAFgEAHgBIAKABQAIAAAGAGIAFADIAAABIABACIAAAAIACADIABAHQgCAqgFAwIgDAXQgEAcgEAXIgDgdg");
    this.shape_16.setTransform(7.775, 14.2375);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.f("#BE9787").s().p("ADfAoIAWgQIAFgEIgBACIAFgFQgJAJgLAIIgNAIIACgCgAEXgPIACgKQACgIAAgIIABAIIAAAGQgCAIgFAJIACgFgAkagOIgBgBIAAAAIACABg");
    this.shape_17.setTransform(2.425, 2.1125);

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.f("#543C32").s().p("AhAAwIAEABIALgHQAIgFABgGIAAgGIAEgGQADgFABgFQAHAGAEAHQgDABAAAFIAAAHQgBACgDACIgFADQgBACAAAHIABADgAiZAmIgagGIgRgEIgFgBIgZgHIgHgDQgOgGgKgGQANABAJAEQAHACAKAEIABABIgCACIABABIAFgBIgBgBIAQgEIASgHIAEAAQADABAKAIIAFADQABAAAAABQABAAAAAAQAAAAAAABQAAAAAAAAQABAAAAAAQABAAAAABQAAAAABABQAAAAAAABQABAEgQAAIgDAAQgTABAuALgADHghQADgHABgJQAaADAOgGIACgBQAGAFADAGIAEAJQgLABgKACQgKACgTAHIgBABQgCgIgGgFg");
    this.shape_18.setTransform(0.7625, 0.7375);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.f("#462215").s().p("AhgBqIABgLQgBgIgHgHQgIgIgHABQgUAAgCAPIAAAFIgLgHIAEgIQAGgLALgHQALgHAMgBIADg8QACgaAFgbQADgNAFgFIABAAQAAAiACARIAAAYIgBAqIgBBAIgIAEgACDBeQAEgagLgyQgMg4AEgaIgDgEIAAgBIgCgHQgCgOAJgHIAAAAQADgFAEgDQAGAGADAMQAHAUABAiQACAtABAKIAEAZIADAeIgDAOIgHAGQgFAEgHAAIABgHg");
    this.shape_19.setTransform(6.975, 15.475);

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.f("#EED2C6").s().p("ADbBWIgCgDIAAAAIASgOQANgKAGgIQAHgJADgMQAFgMAAgKIAAgBIAAgBQgBgJgGgLQgDgGgDgDIgEgEIgFgEIgBAAIgNgMQgHgCgHgEIgLgGIAAgBIgBAAIgCgBIAAAAIAAgBIgBAAIgDgCQgTgLgfgHQg2gMhEAIQgpAEhPASIhmAXIgNAEIAAAAIgEABIgKADIgDABIgGACQglALgJARQgHAOAFAPIgBABIgEgEIgDgGIAAAAIAAgBIgBAAIAAgBIAAgBIgBgFQAAgOANgLIACgCIACgBQAUgQAtgLQA0gOCZgdQAcgGAdgCQBWgEBFAhIAKAEIABABIAEACIALAHIAEACIAjAdIAEAHQAGAJABAIQAAAIgCAIIgDAKIgBAFQgEAIgIAIIgCADIgDADIgCACIgEAEIgFAFIABgCIgGAEIgVAQIgDACIgEADIgBAAIgBgHgAkXAgIgDgEIAEAEgAkeAVIAAAAIABAAgAEZgLIgEgHIAEAHIAAAAIAAAAg");
    this.shape_20.setTransform(1.9625, -2.6791);

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.f("#A87E6D").s().p("ABmBkIgBgDIAAgOQgBgJgIgMQgKgPgCgEQgCgGACgDQACgDAKAAQAIAAAKgFIAQgJQATgIAhAFIAMAAQAHgCABgFQABgEgFgGQgGgGAAgEIADgJIACgKQADgEALACQAOACAIgDIAGgCIgNgJQgMgIgNgGQgVAKgaADQgTAEgQAAIgPAAQgJAAgGADIgNAJQgHAEgLAAIgUgBQgVAAgUAFQgJACgDADIgHAHQgHAIgIADQgGABgBAEQgCADAFAGIANAOQAIAHABAFQABAEgCAKIgEAHIgGAEQgCADABAFIABACIgDAAIgDgBIgBgDQAAgHACgCIAEgDQADgCABgCIABgHQAAgFADgBQgFgHgHgGQAAAFgEAFIgDAGIgBAGQgBAGgIAFIgKAHIgFAAIgFgBIADgDIAIgFQAFgDADgEQADgDADgJQADgKADgDQgCgBAAgGQAAgCgDgEIgDgGQgCgIAJgEIAOgIIAJgJIgigEQgVgBgQADIgOABQgKgBgCgGQgGgBgJACIgPADQgTAFgJgJIgFgFIgBAAIgVALIgZATQgQAKgLAGIgDACIgBAAIAAAAIgDABIgCABIAAAAIgEACIgBAAIAAABIAAgBIgCgBIACgBQgGgPAHgNQAJgSAlgLIAGgCIADgBIALgDIADgBIABAAIAMgEIBmgXQBQgSApgEQBEgIA1AMQAfAHAUALIACACIABAAIABABIAAAAIACABIAAAAIABABIAKAGQAIAEAGACIAOAMIABAAIAEAEIAFAEQADADADAGQAGALAAAKIAAABIAAABQAAAJgEAMQgEAMgHAJQgGAIgMAKIgSAOIgBgCIgBgBIgEgDQgGgGgIAAIgLgBQgGABgGAEQgDADgDAFIgBABQgIAGABAPIACAHQgGgLABgLIACgIIADgIIAEgFQAJgLANgBIANAAIAOAEQAEACAEADIAGgJQAMgPAEgRQACgKgBgHIgTABIgQADQgJACgEADIgCACQAAAHgCAGIgogBQgVgBgLAFIgPAIQgOAHgRAAQABAHAIALQAJANACAFQABAEACAOIACAKIgFAAgADBgNQAFAFACAIIACgBQATgHAKgCQAKgCAKAAIgEgKQgCgGgGgFIgDABQgNAGgbgDQgBAJgCAHgAgugcIARADIAHADQAFgEAMgCQAZgFANABIATABQALAAAHgDIAOgKQAFgCAHgBQgMgCgIABQgPAAgaAJQABgCAFgCQgCAAgDgBIgSgKIgJgDQgGgCgNABIgKgCIgKgBQgFAAgEgCQgDgBgDgFIgBgBQg4ALg7ASIgQAGIAFAFQAJAGASgGQATgFAIAEIAGAEQAHADAMgDQAJgCAOAAIAYABgABrhWQhEABhGANIgKACIACADQADACAIAAIALABIALACIAOABQAGABALAGIATAJIgBABQAOgFAKgBQALgCARABIAdACQANAAARgCQAJgDASgIIAHgCQgggPglgHIgGAAIgGAAgACug3QAPgDAHgDIAAAAIgEgCIgSAIgAiNA+IgJgCQgugLATAAIADAAQAPgBAAgDQAAgBgBgBQAAgBAAAAQgBAAAAgBQgBAAAAAAQAAAAAAAAQAAAAAAgBQgBAAAAAAQAAAAgBgBIgGgDQgJgIgEgBIgDAAIgTAHIgPAFIAAAAIgEABIgBgBIACgCIgCgBIAAAAQAPgJADgBIAQgCQAKgCAEADIAFADQAFAFAGADIAEACQACABAEAGQACAEADACIAHADIAEADIABAAQgEgBAPAHQgFgCgJACIgEgBg");
    this.shape_21.setTransform(1.404, -1.2855);

    this.shape_22 = new cjs.Shape();
    this.shape_22.graphics.f("#B39082").s().p("ABGBWIhhgSIADAAIgBgCQgBgFACgDIAGgEIAEgHQACgKgBgEQgBgFgIgHIgNgOQgFgGACgCQABgEAGgCQAIgDAHgIIAHgHQADgDAIgCQAVgFAVAAIAUABQALAAAHgEIANgJQAGgDAJAAIAPAAQAQAAATgEQAagDAVgKQANAGAMAIIANAJIgGACQgIADgOgCQgLgCgDAEIgCAKIgDAJQAAAEAGAGQAFAHgBADQgBAFgHACIgMAAQghgFgTAIIgQAJQgKAFgIAAQgKAAgCADQgCADACAGQACAEAKAPQAIAMABAJIAAAOIABADIAFAAIgCgKQgCgOgBgEQgCgFgJgNQgIgLgBgHQARAAAOgHIAPgIQALgFAVABIAoABQACgGgBgGIADgCQAEgEAJgCIAQgDIATgBQABAIgCAJQgEARgMAPIgGAJQgEgDgEgCIgOgEIgNAAQgNABgJALIgEAFIgDAIIgCAIQgBALAGALQgCgDgGACQgUAEgSAAQgXAAgngIgAgWBRIAFAAIAGADIgLgDgAh3AzIgGgBIgEgDIgHgDQgDgCgCgEQgEgGgCgBIgEgCQgGgDgFgFIgFgDQgEgDgKACIgQACQgEABgOAJIALgIQgNgHgJgDQgLgDgOAAIABAAIADgCQALgGAQgKIAZgTIAVgLIABAAIAFAFQAJAJATgFIAPgDQAJgCAGABQACAGAKABIAOgBQAQgDAVABIAiAEIgJAJIgOAIQgJAFACAHIADAGQADAEAAACQAAAGACABQgDADgDAKQgDAJgDADQgDAEgFADIgIAFIgDADgAh2A6IgJgCQAIgCAGACIAEABIABADIgKgCgAkFANIAAgBIABAAIAEgCIgDAEIgCgBgAgTggIgRgDQgfgCgQADQgMADgHgDIgGgEQgIgEgTAFQgSAGgJgGIgGgFIARgGQA6gSA5gLIABABQADAFADABQAEACAFAAIAKABIAKACQANgBAGACIAJADIASAKQADABACAAQgFACgBACQAagJAPAAQAHgBANACQgHABgFACIgOAKQgHADgLAAIgTgBQgNgBgaAFQgLACgFAEIgHgDgAA2g0IgTgJQgLgGgGgBIgPgBIgKgCIgLgBQgIAAgDgCIgCgDIAKgCQBFgNBFgBIAMAAQAlAHAgAPIgHACQgSAIgJADQgRACgNAAIgdgCQgRgBgLACQgKABgOAFIABgBgADKhGIAEACIAAAAQgHADgPADIASgIg");
    this.shape_22.setTransform(0.4409, -0.5783);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_22}, {t: this.shape_21}, {t: this.shape_20}, {t: this.shape_19}, {t: this.shape_18}, {t: this.shape_17}, {t: this.shape_16}, {t: this.shape_15}, {t: this.shape_14}, {t: this.shape_13}, {t: this.shape_12}, {t: this.shape_11}, {t: this.shape_10}, {t: this.shape_9}, {t: this.shape_8}, {t: this.shape_7}, {t: this.shape_6}, {t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    // Layer_2
    this.shape_23 = new cjs.Shape();
    this.shape_23.graphics.f("rgba(124,113,80,0.529)").s().p("Ag1BeIgpgZQgJgFgKgBQgHAAgWACQgnACgTgZIgJgOQgFgJgFgEQgIgHgQgDQgNgCgcgBQgbAAgOgDQgRgDgLgIQgOgKgDgNQgEgNAGgNQAGgMAMgJQAQgMAigGQBegQBJAQIA6AQIBGAPQArAHAZAIIAgALQATAFANACQAUADAngCIBSgEIAggBQARACAOAEQAPAGALAMQAMAMABAOIAAAHIABAEQACAGAAAFIgBAOIgFAMQgIAMgGAFQgLAKgcAIIgSACIgHgBQgLADgKgCQgOgCgFABQgEAAgLAIQgPAJgRgGIgUgKQgMgFgJAAQgGAAgSAKQgVAKgYgCQgYgCgSgOQgJgFgCAAQgDAAgIAGQgSAPgVABQgTAAgXgPgAgUAnQAGAEAEgBIAGgCIgEgFQgGgEgMgCIgPgDgAiugjIACgCIgFgBIADADg");
    this.shape_23.setTransform(-1.4145, 18);

    this.timeline.addTween(cjs.Tween.get(this.shape_23).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-38.7, -13, 74.6, 41.9);


  (lib.Symbol10copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#6B0F0F").ss(0.5, 1, 1).p("ADzigIgCgBQgEgCgFgCQhGghhVAEQgdACgcAGQiaAdgzAOQgtAMgVAPIgBABQgBABgBABQgOAMAAAOQAAACABADQABABAAAAIAAABQAAAAAAAAAC6AOQgCgBgBgDQgBAAAAAAQgBgDgBgEQgCgNAJgHQABAAAAgBQADgEADgEQAFgEAHgBQACAAAJABQAIAAAGAGQACACACABQABABAAAAQABABAAABAE0hRQAAgDAAgDQAAgEAAgEQgCgJgFgJQgCgDgDgEIgigdQgCgBgCgBQgGgEgGgDADtgSQAAABACACQABACAAAFQAAAAAAAAQADgBACgCQAHgEAGgEQALgIAJgJIAFgEQABgBAAgBQACgBACgCQABgBABgCQAHgIAEgIQAGgIABgJQAEAUgHAhQgCAOgEAOQgEALgEALQgFANAAAUQAAAMABAZIABAAQAGABATARQAWATgDAHQgPAUgKAGQgRALgRAEQgRAEgEgEQgBgBgDgHQAAAAgBgIQgDgIAAgFQgBgGACgEQAEgIAPgRQACgCACgCQAKgLAFgEQACgBABgBQAAAAABAAADwgIQgCApgGAxQgBAMgBALQgEAcgFAXQgBAIgCAHQABgBAAgBQANgRABgBQACgBACgBQACAAACgBQABAAACAAQAOgCAKgDQAOgDAHAAQAJAAAJAEQAJAEALgHQADgCACgDADIC1QAHAAAFgEQAEgCACgEABsAaQABAAAAAAQABAAABAAQABAAAAAAQALAAALgBIAFgBQADAAACAAIAPgEIAXgEQACgBACgBQgEAaAMA6QAKAygDAZQAAAFgBADAgGADQAAAAAAAAQAEABACABIAQAFQAAAAAAAAQABAAABAAIAAAAQABABABAAQAIADAHACQAMADAMADIABAAQACAAABAAQAWAEAWAAAgSC2QADgCAGgDQABgBACgBQABAAAAAAQAMgHAJAAQAHAAAKAEQAGAEAIAFIACABQADACACABQAQAKALgBQAHgBALgHABmC4IAFgEQADgCADgCQAJgHAJgCQACAAACgBQAIAAAPAFQAEABAFACQAWAHALAAAkJhPIADAGQABACACACQABABABAAIAAABQABABABAAIAEADAj5g+QAJAHAPAFQADACAEABQALAEAOADQACABADABQAIACAIABQAOADAMADIAKACIAJACAkLhSQABAPgCAeQgCAkgEANQAAABgBABAh7gYQAFABAFABQAEABAFABQAEABAFABQACABACAAQAFABAEABIAaAHQAVAFAPADAgSAAQAAAhABASIABAXQAAAEgCAoQAAAVAAArAjOBsQAJALAIAGQAKAJATACQAHABAFABQANACAEABQABAAAAAAQAMAFAWAMQAGADAGAEQACAAABABQAFADAFADAgbC5QgJgHgKgBQgNgBgJgCQgCgBgBgBQAAgCAAgCQACgPAVgBQAHAAAHAIQAIAHABAHQAAACgCAJIAAABQgWANgJgGQgNgKAAgLAgbC6QACgBAHgDAhSgPQgBAZAAArQABAugCBCAkTARIgCADQAAABAAABQgBACgBACQgDAKgBAEQgCgCgEgLQgDgIgHAAQgBAAgBAAQgBADAEATIABABQAFAZAAADQAQABAUAAQAVACAAAAQAAAAABABQABABABABQACACACADQAHAHAIALQAEAFADAEAjuBFQgIgHgNgHQgRgJgHgGAjrBJIgCgCAktATQgEACgFAHQgCAFgFAIQgBABgEAEQgCACgCACQgIAIADABQALAIAcADAjBgoQgEAagBAvQgBAngHAk");
    this.shape.setTransform(0.0114, 7.5015);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#B18E81").s().p("AABAAIgBAAIABAAg");
    this.shape_1.setTransform(6.1875, 9.725);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#744331").s().p("AhJBOIAAgKIABgXQAFgygCgwQAAgVABgGIABgFIABABIACAAIABAAQAWAEAWAAIgDAKQgFAVgBAMIABAzQACAlgCAYIAAAEQgLAHgHABIgCAAQgLAAgPgJgAAoBDIgJgDIgEhJIgDhLIgMACIAFgBIAPgDIAEAYIAGATQAJAYACAKIAFAgQAEAUAFANIAFAJIACABIgBAIIgCAAQgLAAgUgHg");
    this.shape_2.setTransform(12.7313, 18.2283);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#6A4031").s().p("AgjAUIgEgIIgBgIQgDgHAAgGIAYgFQAOgDAGAAQAJABAJAEQAJADALgHIAFgEQgPAUgKAFQgRALgQAEIgOABQgFAAgCgBg");
    this.shape_3.setTransform(28.825, 23.2875);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#7B4D39").s().p("AgYA3QAEgYAEgcIAPgIQAOgJADgKQACgFAAgKIABgPQABgEAHgKIAAAJIABANQAAAGgHAPQgEAJAAAJQAAAIAEAGIgEAFQgPARgDAIQgBAEAAAFIgDABIgEAAIgDACIgOATIgBABIADgOg");
    this.shape_4.setTransform(24.6813, 17.6875);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#5C2C1B").s().p("AACAOQgRgKgHgEIAEgPIACgDIABgDIABgDIABgDIAEAEIAJAHIANAKQAGAEADAEIAAAaQgIgIgMgGg");
    this.shape_5.setTransform(-26.1, 11.6875);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#794531").s().p("AgrA/QgNgBgJgCIgDgBIAAgFQACgPAVAAQAHgBAHAIQAIAHABAIIgCALQgJgHgKgCgAD4AZQAEgIAPgRIAEgEQAKgLAFgEIADgBIABAAIABAAIAAAAQAAAFAEAFIAFAGIABAHQAAAJADAJQABAFAEABIABADQAAAAABABQAAAAAAAAQABAAAAAAQABAAAAAAIAHgDQAFgDAFACQgLAHgJgDQgJgEgJgBQgHAAgOADIgYAFQgBgFACgEgAlIg1QgDgCAIgHIAEgEIABAAQAGADAJgCIAJgDIAAgDIAFAcQgcgCgLgIg");
    this.shape_6.setTransform(-0.2812, 18.875);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#573325").s().p("ABqBWQgNgKAAgLIADACQAJACANABQAKABAJAHIAAABQgPAJgJAAQgEAAgDgCgAhbgjQgUAAgQgBIgGgcIAAgBQgEgTABgDIACAAQAHAAADAIQAEALACACQAHAGARAJQANAHAHAHIACACIACACIgBAAIgUgCg");
    this.shape_7.setTransform(-16.4687, 18.2056);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#683B2A").s().p("AgYBYIAFADIAAAAIgFgDgAgYBYIgBgBIgPgJQgKgFgHABQgJAAgMAHIgBAAIgEACIAAgGQADgVACg/IAAgdQgCgiABgRIAAgDIAFACIARAEIAAABIACAAIABAAIABABIAPAFIAYAGIgBAEQgBAGAAAVQACAvgEAzIgCAXIABAKIgFgDgAgYBYIAAAAgAAggBQgBgeADgkIACAAIABAAIACAAIAVgBIAFgBIANgBIACBJIAEBKQgPgFgHAAIgFABQgIACgKAHQgHg2AAgdg");
    this.shape_8.setTransform(7.4, 16.925);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.f("#9D7B6E").s().p("AAuASIgBAAIgCAAIAAAAQgXAAgUgEIAAAAQAOABAdgBIAgACIAFgBIgGABIgEAAIgWACIgCAAgAB2ACIAAAAIADAEIgEABQABAAAAgBQAAgBABAAQAAgBAAAAQAAgBgBgBgAhAgDIgGgBIgBgBIgHgCIgFgBIgBAAIgkgJIAIACQAcAFAWAHIAOAEg");
    this.shape_9.setTransform(6.475, 8.3625);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.f("#825441").s().p("AgFBOIgBgBIgRgDQgEgqAAgiQAAgwAGgrIAKACIAJACIADABIAJACIAKACIAIACIAAAHIgCAvQAAAoACBSQgWgMgLgEg");
    this.shape_10.setTransform(-12.4625, 13.8875);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.f("#633320").s().p("AABBGIgOgTIgEgFIgDgCIABgBIAKhCIAEgsIABgKIAZAHQgEAbgBAuQgBAngHAkIgHgIg");
    this.shape_11.setTransform(-21.4625, 10.5);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.f("#7B452F").s().p("AADA3IgMgKIgKgIIgFgDIABgCQAEgPACgiQACgegBgPIABABIAAAAIAAABIAAAAIAAAAIABABIADAGIADADIABACIABAAIAAAAIACACIAEADIAAABQAKAGAPAGIAAAAQgNA9AAAcIAAAEQgDgEgGgEg");
    this.shape_12.setTransform(-25.075, 5.55);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.f("#82655B").s().p("ACQAvQgdABgPgBIAAAAIgDgBIgBAAIgYgGIgQgFIgBAAIAAAAIgCgBIgBAAIgOgFQgWgHgbgFIgIgCIgagGIgKgCIgEgBIgJgCIAAgDIgEgBQgPgGADAAIAAAAIAGABIA0AJIAGABIAeAGIACAAIBjATQAmAHAYAAQARAAAUgEQAHgCACADIAAABIAAAAQABABAAABQAAAAAAABQAAAAgBABQAAABgBAAIgWAFIgPADIgEABIghgCgAiugaQgKgFgHgCQgJgDgNgBIgBgBIgFgDIAEgEIAAAAIACgBIADgCIAAAAQAOAAALAEQAIACANAIIgKAIg");
    this.shape_13.setTransform(-3.6283, 5.0625);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.f("#9D6753").s().p("ABeCCQACgYgCglIgBg0QABgMAFgUIADgKIABAAQgDAjABAeQAAAeAHA2IgGAEIgFAEIgDACIAAgEgAgXBCIACgsIgBgWQgBgSAAghIAMADIAAAAIAAADQgBARADAhIAAAdQgCBAgDAVIAAAGIgJAFIAAhAgADCB4IgFgJQgFgNgEgUIgFghQgCgKgJgYIgGgSIgEgYIAXgFIAEgBQgEAaAMA5QAKAxgDAaIgCgBgAhlBqQgChTAAgnIACgvIABgHIAEABIAJACQgBAaAAAqQABAtgCBDIgMgHgAEuBbIgBgDQgEgBgBgFQgDgJAAgJIgBgIIgFgFQgEgFAAgFIAAgBQAGABATARQAWATgDAHIgFAFQgFgDgFADIgHAEIAAAAQgBAAAAAAQAAAAgBgBQAAAAAAAAQgBAAAAgBgAEDApQAAgIAEgKQAGgOABgGIgBgNIAAgIQAAgRAHgZIAIgdQgZAYgQAEQgDABgFAAIAFgDIANgJQALgHAJgJIAFgEIABgCIAEgDIACgDQAHgIAEgIQAGgJABgIQAEAUgHAhIgGAbIgIAYQgFANAAATIABAlIgBAAIgDACQgFAEgKAKQgEgGAAgJgAjwAVIgCgCIgBgCIAAgZIAAgEQAAgcANg9IAAgBIAHADIgCAKIgEAsIgKBBIAAABIgBAAgAlGgFIgBAAIAFgFIAHgNQAFgHAEgCQgBADAEATIABABIAAACIgJADIgGABQgFAAgEgCg");
    this.shape_14.setTransform(0.5051, 12.725);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.f("#94614C").s().p("AjJBYQgTgDgKgIQgIgHgJgLQAHgkABglQABgwAEgaIAFABIAQAEIAaAFQgGArAAAvQAAAjAEAqIgMgBgAC/AiQAGgxACgpIAAAAQAEAAAEgBQAQgEAZgZIgIAdQgHAaAAAQQgHAKgBAEIgBAOQAAAKgCAFQgEAKgOAJIgPAJIACgWg");
    this.shape_15.setTransform(4.125, 12.375);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.f("#542716").s().p("AihBhQABhCAAgsQgBgrACgaIAaAGIAkAJQgFAGgDANQgFAagCAZIgDA+QgMABgLAGQgLAHgGALIgEAJIgCgCgACNA8IgEgZQgBgLgCgsQgBgigHgVQgDgMgGgGQAFgEAHgBIAKABQAIAAAGAGIAFADIAAABIABACIAAAAIACADIABAHQgCAqgFAwIgDAXQgEAcgEAXIgDgdg");
    this.shape_16.setTransform(7.775, 14.2375);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.f("#BE9787").s().p("ADfAoIAWgQIAFgEIgBACIAFgFQgJAJgLAIIgNAIIACgCgAEXgPIACgKQACgIAAgIIABAIIAAAGQgCAIgFAJIACgFgAkagOIgBgBIAAAAIACABg");
    this.shape_17.setTransform(2.425, 2.1125);

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.f("#543C32").s().p("AhAAwIAEABIALgHQAIgFABgGIAAgGIAEgGQADgFABgFQAHAGAEAHQgDABAAAFIAAAHQgBACgDACIgFADQgBACAAAHIABADgAiZAmIgagGIgRgEIgFgBIgZgHIgHgDQgOgGgKgGQANABAJAEQAHACAKAEIABABIgCACIABABIAFgBIgBgBIAQgEIASgHIAEAAQADABAKAIIAFADQABAAAAABQABAAAAAAQAAAAAAABQAAAAAAAAQABAAAAAAQABAAAAABQAAAAABABQAAAAAAABQABAEgQAAIgDAAQgTABAuALgADHghQADgHABgJQAaADAOgGIACgBQAGAFADAGIAEAJQgLABgKACQgKACgTAHIgBABQgCgIgGgFg");
    this.shape_18.setTransform(0.7625, 0.7375);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.f("#462215").s().p("AhgBqIABgLQgBgIgHgHQgIgIgHABQgUAAgCAPIAAAFIgLgHIAEgIQAGgLALgHQALgHAMgBIADg8QACgaAFgbQADgNAFgFIABAAQAAAiACARIAAAYIgBAqIgBBAIgIAEgACDBeQAEgagLgyQgMg4AEgaIgDgEIAAgBIgCgHQgCgOAJgHIAAAAQADgFAEgDQAGAGADAMQAHAUABAiQACAtABAKIAEAZIADAeIgDAOIgHAGQgFAEgHAAIABgHg");
    this.shape_19.setTransform(6.975, 15.475);

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.f("#EED2C6").s().p("ADbBWIgCgDIAAAAIASgOQANgKAGgIQAHgJADgMQAFgMAAgKIAAgBIAAgBQgBgJgGgLQgDgGgDgDIgEgEIgFgEIgBAAIgNgMQgHgCgHgEIgLgGIAAgBIgBAAIgCgBIAAAAIAAgBIgBAAIgDgCQgTgLgfgHQg2gMhEAIQgpAEhPASIhmAXIgNAEIAAAAIgEABIgKADIgDABIgGACQglALgJARQgHAOAFAPIgBABIgEgEIgDgGIAAAAIAAgBIgBAAIAAgBIAAgBIgBgFQAAgOANgLIACgCIACgBQAUgQAtgLQA0gOCZgdQAcgGAdgCQBWgEBFAhIAKAEIABABIAEACIALAHIAEACIAjAdIAEAHQAGAJABAIQAAAIgCAIIgDAKIgBAFQgEAIgIAIIgCADIgDADIgCACIgEAEIgFAFIABgCIgGAEIgVAQIgDACIgEADIgBAAIgBgHgAkXAgIgDgEIAEAEgAkeAVIAAAAIABAAgAEZgLIgEgHIAEAHIAAAAIAAAAg");
    this.shape_20.setTransform(1.9625, -2.6791);

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.f("#A87E6D").s().p("ABmBkIgBgDIAAgOQgBgJgIgMQgKgPgCgEQgCgGACgDQACgDAKAAQAIAAAKgFIAQgJQATgIAhAFIAMAAQAHgCABgFQABgEgFgGQgGgGAAgEIADgJIACgKQADgEALACQAOACAIgDIAGgCIgNgJQgMgIgNgGQgVAKgaADQgTAEgQAAIgPAAQgJAAgGADIgNAJQgHAEgLAAIgUgBQgVAAgUAFQgJACgDADIgHAHQgHAIgIADQgGABgBAEQgCADAFAGIANAOQAIAHABAFQABAEgCAKIgEAHIgGAEQgCADABAFIABACIgDAAIgDgBIgBgDQAAgHACgCIAEgDQADgCABgCIABgHQAAgFADgBQgFgHgHgGQAAAFgEAFIgDAGIgBAGQgBAGgIAFIgKAHIgFAAIgFgBIADgDIAIgFQAFgDADgEQADgDADgJQADgKADgDQgCgBAAgGQAAgCgDgEIgDgGQgCgIAJgEIAOgIIAJgJIgigEQgVgBgQADIgOABQgKgBgCgGQgGgBgJACIgPADQgTAFgJgJIgFgFIgBAAIgVALIgZATQgQAKgLAGIgDACIgBAAIAAAAIgDABIgCABIAAAAIgEACIgBAAIAAABIAAgBIgCgBIACgBQgGgPAHgNQAJgSAlgLIAGgCIADgBIALgDIADgBIABAAIAMgEIBmgXQBQgSApgEQBEgIA1AMQAfAHAUALIACACIABAAIABABIAAAAIACABIAAAAIABABIAKAGQAIAEAGACIAOAMIABAAIAEAEIAFAEQADADADAGQAGALAAAKIAAABIAAABQAAAJgEAMQgEAMgHAJQgGAIgMAKIgSAOIgBgCIgBgBIgEgDQgGgGgIAAIgLgBQgGABgGAEQgDADgDAFIgBABQgIAGABAPIACAHQgGgLABgLIACgIIADgIIAEgFQAJgLANgBIANAAIAOAEQAEACAEADIAGgJQAMgPAEgRQACgKgBgHIgTABIgQADQgJACgEADIgCACQAAAHgCAGIgogBQgVgBgLAFIgPAIQgOAHgRAAQABAHAIALQAJANACAFQABAEACAOIACAKIgFAAgADBgNQAFAFACAIIACgBQATgHAKgCQAKgCAKAAIgEgKQgCgGgGgFIgDABQgNAGgbgDQgBAJgCAHgAgugcIARADIAHADQAFgEAMgCQAZgFANABIATABQALAAAHgDIAOgKQAFgCAHgBQgMgCgIABQgPAAgaAJQABgCAFgCQgCAAgDgBIgSgKIgJgDQgGgCgNABIgKgCIgKgBQgFAAgEgCQgDgBgDgFIgBgBQg4ALg7ASIgQAGIAFAFQAJAGASgGQATgFAIAEIAGAEQAHADAMgDQAJgCAOAAIAYABgABrhWQhEABhGANIgKACIACADQADACAIAAIALABIALACIAOABQAGABALAGIATAJIgBABQAOgFAKgBQALgCARABIAdACQANAAARgCQAJgDASgIIAHgCQgggPglgHIgGAAIgGAAgACug3QAPgDAHgDIAAAAIgEgCIgSAIgAiNA+IgJgCQgugLATAAIADAAQAPgBAAgDQAAgBgBgBQAAgBAAAAQgBAAAAgBQgBAAAAAAQAAAAAAAAQAAAAAAgBQgBAAAAAAQAAAAgBgBIgGgDQgJgIgEgBIgDAAIgTAHIgPAFIAAAAIgEABIgBgBIACgCIgCgBIAAAAQAPgJADgBIAQgCQAKgCAEADIAFADQAFAFAGADIAEACQACABAEAGQACAEADACIAHADIAEADIABAAQgEgBAPAHQgFgCgJACIgEgBg");
    this.shape_21.setTransform(1.404, -1.2855);

    this.shape_22 = new cjs.Shape();
    this.shape_22.graphics.f("#B39082").s().p("ABGBWIhhgSIADAAIgBgCQgBgFACgDIAGgEIAEgHQACgKgBgEQgBgFgIgHIgNgOQgFgGACgCQABgEAGgCQAIgDAHgIIAHgHQADgDAIgCQAVgFAVAAIAUABQALAAAHgEIANgJQAGgDAJAAIAPAAQAQAAATgEQAagDAVgKQANAGAMAIIANAJIgGACQgIADgOgCQgLgCgDAEIgCAKIgDAJQAAAEAGAGQAFAHgBADQgBAFgHACIgMAAQghgFgTAIIgQAJQgKAFgIAAQgKAAgCADQgCADACAGQACAEAKAPQAIAMABAJIAAAOIABADIAFAAIgCgKQgCgOgBgEQgCgFgJgNQgIgLgBgHQARAAAOgHIAPgIQALgFAVABIAoABQACgGgBgGIADgCQAEgEAJgCIAQgDIATgBQABAIgCAJQgEARgMAPIgGAJQgEgDgEgCIgOgEIgNAAQgNABgJALIgEAFIgDAIIgCAIQgBALAGALQgCgDgGACQgUAEgSAAQgXAAgngIgAgWBRIAFAAIAGADIgLgDgAh3AzIgGgBIgEgDIgHgDQgDgCgCgEQgEgGgCgBIgEgCQgGgDgFgFIgFgDQgEgDgKACIgQACQgEABgOAJIALgIQgNgHgJgDQgLgDgOAAIABAAIADgCQALgGAQgKIAZgTIAVgLIABAAIAFAFQAJAJATgFIAPgDQAJgCAGABQACAGAKABIAOgBQAQgDAVABIAiAEIgJAJIgOAIQgJAFACAHIADAGQADAEAAACQAAAGACABQgDADgDAKQgDAJgDADQgDAEgFADIgIAFIgDADgAh2A6IgJgCQAIgCAGACIAEABIABADIgKgCgAkFANIAAgBIABAAIAEgCIgDAEIgCgBgAgTggIgRgDQgfgCgQADQgMADgHgDIgGgEQgIgEgTAFQgSAGgJgGIgGgFIARgGQA6gSA5gLIABABQADAFADABQAEACAFAAIAKABIAKACQANgBAGACIAJADIASAKQADABACAAQgFACgBACQAagJAPAAQAHgBANACQgHABgFACIgOAKQgHADgLAAIgTgBQgNgBgaAFQgLACgFAEIgHgDgAA2g0IgTgJQgLgGgGgBIgPgBIgKgCIgLgBQgIAAgDgCIgCgDIAKgCQBFgNBFgBIAMAAQAlAHAgAPIgHACQgSAIgJADQgRACgNAAIgdgCQgRgBgLACQgKABgOAFIABgBgADKhGIAEACIAAAAQgHADgPADIASgIg");
    this.shape_22.setTransform(0.4409, -0.5783);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_22}, {t: this.shape_21}, {t: this.shape_20}, {t: this.shape_19}, {t: this.shape_18}, {t: this.shape_17}, {t: this.shape_16}, {t: this.shape_15}, {t: this.shape_14}, {t: this.shape_13}, {t: this.shape_12}, {t: this.shape_11}, {t: this.shape_10}, {t: this.shape_9}, {t: this.shape_8}, {t: this.shape_7}, {t: this.shape_6}, {t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    // Layer_2
    this.shape_23 = new cjs.Shape();
    this.shape_23.graphics.f("rgba(124,113,80,0.529)").s().p("Ag1BeIgpgZQgJgFgKgBQgHAAgWACQgnACgTgZIgJgOQgFgJgFgEQgIgHgQgDQgNgCgcgBQgbAAgOgDQgRgDgLgIQgOgKgDgNQgEgNAGgNQAGgMAMgJQAQgMAigGQBegQBJAQIA6AQIBGAPQArAHAZAIIAgALQATAFANACQAUADAngCIBSgEIAggBQARACAOAEQAPAGALAMQAMAMABAOIAAAHIABAEQACAGAAAFIgBAOIgFAMQgIAMgGAFQgLAKgcAIIgSACIgHgBQgLADgKgCQgOgCgFABQgEAAgLAIQgPAJgRgGIgUgKQgMgFgJAAQgGAAgSAKQgVAKgYgCQgYgCgSgOQgJgFgCAAQgDAAgIAGQgSAPgVABQgTAAgXgPgAgUAnQAGAEAEgBIAGgCIgEgFQgGgEgMgCIgPgDgAiugjIACgCIgFgBIADADg");
    this.shape_23.setTransform(-1.4145, 18);

    this.timeline.addTween(cjs.Tween.get(this.shape_23).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-38.7, -13, 74.6, 41.9);


  (lib.Symbol10copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#6B0F0F").ss(0.5, 1, 1).p("ADzigIgCgBQgEgCgFgCQhGghhVAEQgdACgcAGQiaAdgzAOQgtAMgVAPIgBABQgBABgBABQgOAMAAAOQAAACABADQABABAAAAIAAABQAAAAAAAAAC6AOQgCgBgBgDQgBAAAAAAQgBgDgBgEQgCgNAJgHQABAAAAgBQADgEADgEQAFgEAHgBQACAAAJABQAIAAAGAGQACACACABQABABAAAAQABABAAABAE0hRQAAgDAAgDQAAgEAAgEQgCgJgFgJQgCgDgDgEIgigdQgCgBgCgBQgGgEgGgDADtgSQAAABACACQABACAAAFQAAAAAAAAQADgBACgCQAHgEAGgEQALgIAJgJIAFgEQABgBAAgBQACgBACgCQABgBABgCQAHgIAEgIQAGgIABgJQAEAUgHAhQgCAOgEAOQgEALgEALQgFANAAAUQAAAMABAZIABAAQAGABATARQAWATgDAHQgPAUgKAGQgRALgRAEQgRAEgEgEQgBgBgDgHQAAAAgBgIQgDgIAAgFQgBgGACgEQAEgIAPgRQACgCACgCQAKgLAFgEQACgBABgBQAAAAABAAADwgIQgCApgGAxQgBAMgBALQgEAcgFAXQgBAIgCAHQABgBAAgBQANgRABgBQACgBACgBQACAAACgBQABAAACAAQAOgCAKgDQAOgDAHAAQAJAAAJAEQAJAEALgHQADgCACgDADIC1QAHAAAFgEQAEgCACgEABsAaQABAAAAAAQABAAABAAQABAAAAAAQALAAALgBIAFgBQADAAACAAIAPgEIAXgEQACgBACgBQgEAaAMA6QAKAygDAZQAAAFgBADAgGADQAAAAAAAAQAEABACABIAQAFQAAAAAAAAQABAAABAAIAAAAQABABABAAQAIADAHACQAMADAMADIABAAQACAAABAAQAWAEAWAAAgSC2QADgCAGgDQABgBACgBQABAAAAAAQAMgHAJAAQAHAAAKAEQAGAEAIAFIACABQADACACABQAQAKALgBQAHgBALgHABmC4IAFgEQADgCADgCQAJgHAJgCQACAAACgBQAIAAAPAFQAEABAFACQAWAHALAAAkJhPIADAGQABACACACQABABABAAIAAABQABABABAAIAEADAj5g+QAJAHAPAFQADACAEABQALAEAOADQACABADABQAIACAIABQAOADAMADIAKACIAJACAkLhSQABAPgCAeQgCAkgEANQAAABgBABAh7gYQAFABAFABQAEABAFABQAEABAFABQACABACAAQAFABAEABIAaAHQAVAFAPADAgSAAQAAAhABASIABAXQAAAEgCAoQAAAVAAArAjOBsQAJALAIAGQAKAJATACQAHABAFABQANACAEABQABAAAAAAQAMAFAWAMQAGADAGAEQACAAABABQAFADAFADAgbC5QgJgHgKgBQgNgBgJgCQgCgBgBgBQAAgCAAgCQACgPAVgBQAHAAAHAIQAIAHABAHQAAACgCAJIAAABQgWANgJgGQgNgKAAgLAgbC6QACgBAHgDAhSgPQgBAZAAArQABAugCBCAkTARIgCADQAAABAAABQgBACgBACQgDAKgBAEQgCgCgEgLQgDgIgHAAQgBAAgBAAQgBADAEATIABABQAFAZAAADQAQABAUAAQAVACAAAAQAAAAABABQABABABABQACACACADQAHAHAIALQAEAFADAEAjuBFQgIgHgNgHQgRgJgHgGAjrBJIgCgCAktATQgEACgFAHQgCAFgFAIQgBABgEAEQgCACgCACQgIAIADABQALAIAcADAjBgoQgEAagBAvQgBAngHAk");
    this.shape.setTransform(0.0114, 7.5015);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#B18E81").s().p("AABAAIgBAAIABAAg");
    this.shape_1.setTransform(6.1875, 9.725);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#744331").s().p("AhJBOIAAgKIABgXQAFgygCgwQAAgVABgGIABgFIABABIACAAIABAAQAWAEAWAAIgDAKQgFAVgBAMIABAzQACAlgCAYIAAAEQgLAHgHABIgCAAQgLAAgPgJgAAoBDIgJgDIgEhJIgDhLIgMACIAFgBIAPgDIAEAYIAGATQAJAYACAKIAFAgQAEAUAFANIAFAJIACABIgBAIIgCAAQgLAAgUgHg");
    this.shape_2.setTransform(12.7313, 18.2283);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#6A4031").s().p("AgjAUIgEgIIgBgIQgDgHAAgGIAYgFQAOgDAGAAQAJABAJAEQAJADALgHIAFgEQgPAUgKAFQgRALgQAEIgOABQgFAAgCgBg");
    this.shape_3.setTransform(28.825, 23.2875);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#7B4D39").s().p("AgYA3QAEgYAEgcIAPgIQAOgJADgKQACgFAAgKIABgPQABgEAHgKIAAAJIABANQAAAGgHAPQgEAJAAAJQAAAIAEAGIgEAFQgPARgDAIQgBAEAAAFIgDABIgEAAIgDACIgOATIgBABIADgOg");
    this.shape_4.setTransform(24.6813, 17.6875);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#5C2C1B").s().p("AACAOQgRgKgHgEIAEgPIACgDIABgDIABgDIABgDIAEAEIAJAHIANAKQAGAEADAEIAAAaQgIgIgMgGg");
    this.shape_5.setTransform(-26.1, 11.6875);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#794531").s().p("AgrA/QgNgBgJgCIgDgBIAAgFQACgPAVAAQAHgBAHAIQAIAHABAIIgCALQgJgHgKgCgAD4AZQAEgIAPgRIAEgEQAKgLAFgEIADgBIABAAIABAAIAAAAQAAAFAEAFIAFAGIABAHQAAAJADAJQABAFAEABIABADQAAAAABABQAAAAAAAAQABAAAAAAQABAAAAAAIAHgDQAFgDAFACQgLAHgJgDQgJgEgJgBQgHAAgOADIgYAFQgBgFACgEgAlIg1QgDgCAIgHIAEgEIABAAQAGADAJgCIAJgDIAAgDIAFAcQgcgCgLgIg");
    this.shape_6.setTransform(-0.2812, 18.875);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#573325").s().p("ABqBWQgNgKAAgLIADACQAJACANABQAKABAJAHIAAABQgPAJgJAAQgEAAgDgCgAhbgjQgUAAgQgBIgGgcIAAgBQgEgTABgDIACAAQAHAAADAIQAEALACACQAHAGARAJQANAHAHAHIACACIACACIgBAAIgUgCg");
    this.shape_7.setTransform(-16.4687, 18.2056);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#683B2A").s().p("AgYBYIAFADIAAAAIgFgDgAgYBYIgBgBIgPgJQgKgFgHABQgJAAgMAHIgBAAIgEACIAAgGQADgVACg/IAAgdQgCgiABgRIAAgDIAFACIARAEIAAABIACAAIABAAIABABIAPAFIAYAGIgBAEQgBAGAAAVQACAvgEAzIgCAXIABAKIgFgDgAgYBYIAAAAgAAggBQgBgeADgkIACAAIABAAIACAAIAVgBIAFgBIANgBIACBJIAEBKQgPgFgHAAIgFABQgIACgKAHQgHg2AAgdg");
    this.shape_8.setTransform(7.4, 16.925);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.f("#9D7B6E").s().p("AAuASIgBAAIgCAAIAAAAQgXAAgUgEIAAAAQAOABAdgBIAgACIAFgBIgGABIgEAAIgWACIgCAAgAB2ACIAAAAIADAEIgEABQABAAAAgBQAAgBABAAQAAgBAAAAQAAgBgBgBgAhAgDIgGgBIgBgBIgHgCIgFgBIgBAAIgkgJIAIACQAcAFAWAHIAOAEg");
    this.shape_9.setTransform(6.475, 8.3625);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.f("#825441").s().p("AgFBOIgBgBIgRgDQgEgqAAgiQAAgwAGgrIAKACIAJACIADABIAJACIAKACIAIACIAAAHIgCAvQAAAoACBSQgWgMgLgEg");
    this.shape_10.setTransform(-12.4625, 13.8875);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.f("#633320").s().p("AABBGIgOgTIgEgFIgDgCIABgBIAKhCIAEgsIABgKIAZAHQgEAbgBAuQgBAngHAkIgHgIg");
    this.shape_11.setTransform(-21.4625, 10.5);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.f("#7B452F").s().p("AADA3IgMgKIgKgIIgFgDIABgCQAEgPACgiQACgegBgPIABABIAAAAIAAABIAAAAIAAAAIABABIADAGIADADIABACIABAAIAAAAIACACIAEADIAAABQAKAGAPAGIAAAAQgNA9AAAcIAAAEQgDgEgGgEg");
    this.shape_12.setTransform(-25.075, 5.55);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.f("#82655B").s().p("ACQAvQgdABgPgBIAAAAIgDgBIgBAAIgYgGIgQgFIgBAAIAAAAIgCgBIgBAAIgOgFQgWgHgbgFIgIgCIgagGIgKgCIgEgBIgJgCIAAgDIgEgBQgPgGADAAIAAAAIAGABIA0AJIAGABIAeAGIACAAIBjATQAmAHAYAAQARAAAUgEQAHgCACADIAAABIAAAAQABABAAABQAAAAAAABQAAAAgBABQAAABgBAAIgWAFIgPADIgEABIghgCgAiugaQgKgFgHgCQgJgDgNgBIgBgBIgFgDIAEgEIAAAAIACgBIADgCIAAAAQAOAAALAEQAIACANAIIgKAIg");
    this.shape_13.setTransform(-3.6283, 5.0625);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.f("#94614C").s().p("AjJBYQgTgDgKgIQgIgHgJgLQAHgkABglQABgwAEgaIAFABIAQAEIAaAFQgGArAAAvQAAAjAEAqIgMgBgAC/AiQAGgxACgpIAAAAQAEAAAEgBQAQgEAZgZIgIAdQgHAaAAAQQgHAKgBAEIgBAOQAAAKgCAFQgEAKgOAJIgPAJIACgWg");
    this.shape_14.setTransform(4.125, 12.375);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.f("#9D6753").s().p("ABeCCQACgYgCglIgBg0QABgMAFgUIADgKIABAAQgDAjABAeQAAAeAHA2IgGAEIgFAEIgDACIAAgEgAgXBCIACgsIgBgWQgBgSAAghIAMADIAAAAIAAADQgBARADAhIAAAdQgCBAgDAVIAAAGIgJAFIAAhAgADCB4IgFgJQgFgNgEgUIgFghQgCgKgJgYIgGgSIgEgYIAXgFIAEgBQgEAaAMA5QAKAxgDAaIgCgBgAhlBqQgChTAAgnIACgvIABgHIAEABIAJACQgBAaAAAqQABAtgCBDIgMgHgAEuBbIgBgDQgEgBgBgFQgDgJAAgJIgBgIIgFgFQgEgFAAgFIAAgBQAGABATARQAWATgDAHIgFAFQgFgDgFADIgHAEIAAAAQgBAAAAAAQAAAAgBgBQAAAAAAAAQgBAAAAgBgAEDApQAAgIAEgKQAGgOABgGIgBgNIAAgIQAAgRAHgZIAIgdQgZAYgQAEQgDABgFAAIAFgDIANgJQALgHAJgJIAFgEIABgCIAEgDIACgDQAHgIAEgIQAGgJABgIQAEAUgHAhIgGAbIgIAYQgFANAAATIABAlIgBAAIgDACQgFAEgKAKQgEgGAAgJgAjwAVIgCgCIgBgCIAAgZIAAgEQAAgcANg9IAAgBIAHADIgCAKIgEAsIgKBBIAAABIgBAAgAlGgFIgBAAIAFgFIAHgNQAFgHAEgCQgBADAEATIABABIAAACIgJADIgGABQgFAAgEgCg");
    this.shape_15.setTransform(0.5051, 12.725);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.f("#542716").s().p("AihBhQABhCAAgsQgBgrACgaIAaAGIAkAJQgFAGgDANQgFAagCAZIgDA+QgMABgLAGQgLAHgGALIgEAJIgCgCgACNA8IgEgZQgBgLgCgsQgBgigHgVQgDgMgGgGQAFgEAHgBIAKABQAIAAAGAGIAFADIAAABIABACIAAAAIACADIABAHQgCAqgFAwIgDAXQgEAcgEAXIgDgdg");
    this.shape_16.setTransform(7.775, 14.2375);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.f("#BE9787").s().p("ADfAoIAWgQIAFgEIgBACIAFgFQgJAJgLAIIgNAIIACgCgAEXgPIACgKQACgIAAgIIABAIIAAAGQgCAIgFAJIACgFgAkagOIgBgBIAAAAIACABg");
    this.shape_17.setTransform(2.425, 2.1125);

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.f("#543C32").s().p("AhAAwIAEABIALgHQAIgFABgGIAAgGIAEgGQADgFABgFQAHAGAEAHQgDABAAAFIAAAHQgBACgDACIgFADQgBACAAAHIABADgAiZAmIgagGIgRgEIgFgBIgZgHIgHgDQgOgGgKgGQANABAJAEQAHACAKAEIABABIgCACIABABIAFgBIgBgBIAQgEIASgHIAEAAQADABAKAIIAFADQABAAAAABQABAAAAAAQAAAAAAABQAAAAAAAAQABAAAAAAQABAAAAABQAAAAABABQAAAAAAABQABAEgQAAIgDAAQgTABAuALgADHghQADgHABgJQAaADAOgGIACgBQAGAFADAGIAEAJQgLABgKACQgKACgTAHIgBABQgCgIgGgFg");
    this.shape_18.setTransform(0.7625, 0.7375);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.f("#462215").s().p("AhgBqIABgLQgBgIgHgHQgIgIgHABQgUAAgCAPIAAAFIgLgHIAEgIQAGgLALgHQALgHAMgBIADg8QACgaAFgbQADgNAFgFIABAAQAAAiACARIAAAYIgBAqIgBBAIgIAEgACDBeQAEgagLgyQgMg4AEgaIgDgEIAAgBIgCgHQgCgOAJgHIAAAAQADgFAEgDQAGAGADAMQAHAUABAiQACAtABAKIAEAZIADAeIgDAOIgHAGQgFAEgHAAIABgHg");
    this.shape_19.setTransform(6.975, 15.475);

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.f("#EED2C6").s().p("ADbBWIgCgDIAAAAIASgOQANgKAGgIQAHgJADgMQAFgMAAgKIAAgBIAAgBQgBgJgGgLQgDgGgDgDIgEgEIgFgEIgBAAIgNgMQgHgCgHgEIgLgGIAAgBIgBAAIgCgBIAAAAIAAgBIgBAAIgDgCQgTgLgfgHQg2gMhEAIQgpAEhPASIhmAXIgNAEIAAAAIgEABIgKADIgDABIgGACQglALgJARQgHAOAFAPIgBABIgEgEIgDgGIAAAAIAAgBIgBAAIAAgBIAAgBIgBgFQAAgOANgLIACgCIACgBQAUgQAtgLQA0gOCZgdQAcgGAdgCQBWgEBFAhIAKAEIABABIAEACIALAHIAEACIAjAdIAEAHQAGAJABAIQAAAIgCAIIgDAKIgBAFQgEAIgIAIIgCADIgDADIgCACIgEAEIgFAFIABgCIgGAEIgVAQIgDACIgEADIgBAAIgBgHgAkXAgIgDgEIAEAEgAkeAVIAAAAIABAAgAEZgLIgEgHIAEAHIAAAAIAAAAg");
    this.shape_20.setTransform(1.9625, -2.6791);

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.f("#A87E6D").s().p("ABmBkIgBgDIAAgOQgBgJgIgMQgKgPgCgEQgCgGACgDQACgDAKAAQAIAAAKgFIAQgJQATgIAhAFIAMAAQAHgCABgFQABgEgFgGQgGgGAAgEIADgJIACgKQADgEALACQAOACAIgDIAGgCIgNgJQgMgIgNgGQgVAKgaADQgTAEgQAAIgPAAQgJAAgGADIgNAJQgHAEgLAAIgUgBQgVAAgUAFQgJACgDADIgHAHQgHAIgIADQgGABgBAEQgCADAFAGIANAOQAIAHABAFQABAEgCAKIgEAHIgGAEQgCADABAFIABACIgDAAIgDgBIgBgDQAAgHACgCIAEgDQADgCABgCIABgHQAAgFADgBQgFgHgHgGQAAAFgEAFIgDAGIgBAGQgBAGgIAFIgKAHIgFAAIgFgBIADgDIAIgFQAFgDADgEQADgDADgJQADgKADgDQgCgBAAgGQAAgCgDgEIgDgGQgCgIAJgEIAOgIIAJgJIgigEQgVgBgQADIgOABQgKgBgCgGQgGgBgJACIgPADQgTAFgJgJIgFgFIgBAAIgVALIgZATQgQAKgLAGIgDACIgBAAIAAAAIgDABIgCABIAAAAIgEACIgBAAIAAABIAAgBIgCgBIACgBQgGgPAHgNQAJgSAlgLIAGgCIADgBIALgDIADgBIABAAIAMgEIBmgXQBQgSApgEQBEgIA1AMQAfAHAUALIACACIABAAIABABIAAAAIACABIAAAAIABABIAKAGQAIAEAGACIAOAMIABAAIAEAEIAFAEQADADADAGQAGALAAAKIAAABIAAABQAAAJgEAMQgEAMgHAJQgGAIgMAKIgSAOIgBgCIgBgBIgEgDQgGgGgIAAIgLgBQgGABgGAEQgDADgDAFIgBABQgIAGABAPIACAHQgGgLABgLIACgIIADgIIAEgFQAJgLANgBIANAAIAOAEQAEACAEADIAGgJQAMgPAEgRQACgKgBgHIgTABIgQADQgJACgEADIgCACQAAAHgCAGIgogBQgVgBgLAFIgPAIQgOAHgRAAQABAHAIALQAJANACAFQABAEACAOIACAKIgFAAgADBgNQAFAFACAIIACgBQATgHAKgCQAKgCAKAAIgEgKQgCgGgGgFIgDABQgNAGgbgDQgBAJgCAHgAgugcIARADIAHADQAFgEAMgCQAZgFANABIATABQALAAAHgDIAOgKQAFgCAHgBQgMgCgIABQgPAAgaAJQABgCAFgCQgCAAgDgBIgSgKIgJgDQgGgCgNABIgKgCIgKgBQgFAAgEgCQgDgBgDgFIgBgBQg4ALg7ASIgQAGIAFAFQAJAGASgGQATgFAIAEIAGAEQAHADAMgDQAJgCAOAAIAYABgABrhWQhEABhGANIgKACIACADQADACAIAAIALABIALACIAOABQAGABALAGIATAJIgBABQAOgFAKgBQALgCARABIAdACQANAAARgCQAJgDASgIIAHgCQgggPglgHIgGAAIgGAAgACug3QAPgDAHgDIAAAAIgEgCIgSAIgAiNA+IgJgCQgugLATAAIADAAQAPgBAAgDQAAgBgBgBQAAgBAAAAQgBAAAAgBQgBAAAAAAQAAAAAAAAQAAAAAAgBQgBAAAAAAQAAAAgBgBIgGgDQgJgIgEgBIgDAAIgTAHIgPAFIAAAAIgEABIgBgBIACgCIgCgBIAAAAQAPgJADgBIAQgCQAKgCAEADIAFADQAFAFAGADIAEACQACABAEAGQACAEADACIAHADIAEADIABAAQgEgBAPAHQgFgCgJACIgEgBg");
    this.shape_21.setTransform(1.404, -1.2855);

    this.shape_22 = new cjs.Shape();
    this.shape_22.graphics.f("#B39082").s().p("ABGBWIhhgSIADAAIgBgCQgBgFACgDIAGgEIAEgHQACgKgBgEQgBgFgIgHIgNgOQgFgGACgCQABgEAGgCQAIgDAHgIIAHgHQADgDAIgCQAVgFAVAAIAUABQALAAAHgEIANgJQAGgDAJAAIAPAAQAQAAATgEQAagDAVgKQANAGAMAIIANAJIgGACQgIADgOgCQgLgCgDAEIgCAKIgDAJQAAAEAGAGQAFAHgBADQgBAFgHACIgMAAQghgFgTAIIgQAJQgKAFgIAAQgKAAgCADQgCADACAGQACAEAKAPQAIAMABAJIAAAOIABADIAFAAIgCgKQgCgOgBgEQgCgFgJgNQgIgLgBgHQARAAAOgHIAPgIQALgFAVABIAoABQACgGgBgGIADgCQAEgEAJgCIAQgDIATgBQABAIgCAJQgEARgMAPIgGAJQgEgDgEgCIgOgEIgNAAQgNABgJALIgEAFIgDAIIgCAIQgBALAGALQgCgDgGACQgUAEgSAAQgXAAgngIgAgWBRIAFAAIAGADIgLgDgAh3AzIgGgBIgEgDIgHgDQgDgCgCgEQgEgGgCgBIgEgCQgGgDgFgFIgFgDQgEgDgKACIgQACQgEABgOAJIALgIQgNgHgJgDQgLgDgOAAIABAAIADgCQALgGAQgKIAZgTIAVgLIABAAIAFAFQAJAJATgFIAPgDQAJgCAGABQACAGAKABIAOgBQAQgDAVABIAiAEIgJAJIgOAIQgJAFACAHIADAGQADAEAAACQAAAGACABQgDADgDAKQgDAJgDADQgDAEgFADIgIAFIgDADgAh2A6IgJgCQAIgCAGACIAEABIABADIgKgCgAkFANIAAgBIABAAIAEgCIgDAEIgCgBgAgTggIgRgDQgfgCgQADQgMADgHgDIgGgEQgIgEgTAFQgSAGgJgGIgGgFIARgGQA6gSA5gLIABABQADAFADABQAEACAFAAIAKABIAKACQANgBAGACIAJADIASAKQADABACAAQgFACgBACQAagJAPAAQAHgBANACQgHABgFACIgOAKQgHADgLAAIgTgBQgNgBgaAFQgLACgFAEIgHgDgAA2g0IgTgJQgLgGgGgBIgPgBIgKgCIgLgBQgIAAgDgCIgCgDIAKgCQBFgNBFgBIAMAAQAlAHAgAPIgHACQgSAIgJADQgRACgNAAIgdgCQgRgBgLACQgKABgOAFIABgBgADKhGIAEACIAAAAQgHADgPADIASgIg");
    this.shape_22.setTransform(0.4409, -0.5783);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_22}, {t: this.shape_21}, {t: this.shape_20}, {t: this.shape_19}, {t: this.shape_18}, {t: this.shape_17}, {t: this.shape_16}, {t: this.shape_15}, {t: this.shape_14}, {t: this.shape_13}, {t: this.shape_12}, {t: this.shape_11}, {t: this.shape_10}, {t: this.shape_9}, {t: this.shape_8}, {t: this.shape_7}, {t: this.shape_6}, {t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    // Layer_2
    this.shape_23 = new cjs.Shape();
    this.shape_23.graphics.f("rgba(124,113,80,0.529)").s().p("Ag1BeIgpgZQgJgFgKgBQgHAAgWACQgnACgTgZIgJgOQgFgJgFgEQgIgHgQgDQgNgCgcgBQgbAAgOgDQgRgDgLgIQgOgKgDgNQgEgNAGgNQAGgMAMgJQAQgMAigGQBegQBJAQIA6AQIBGAPQArAHAZAIIAgALQATAFANACQAUADAngCIBSgEIAggBQARACAOAEQAPAGALAMQAMAMABAOIAAAHIABAEQACAGAAAFIgBAOIgFAMQgIAMgGAFQgLAKgcAIIgSACIgHgBQgLADgKgCQgOgCgFABQgEAAgLAIQgPAJgRgGIgUgKQgMgFgJAAQgGAAgSAKQgVAKgYgCQgYgCgSgOQgJgFgCAAQgDAAgIAGQgSAPgVABQgTAAgXgPgAgUAnQAGAEAEgBIAGgCIgEgFQgGgEgMgCIgPgDgAiugjIACgCIgFgBIADADg");
    this.shape_23.setTransform(-1.4145, 18);

    this.timeline.addTween(cjs.Tween.get(this.shape_23).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-38.7, -13, 74.6, 41.9);


  (lib.Symbol10_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape_22 = new cjs.Shape();
    this.shape_22.graphics.f("#391200").s().p("AukFsQgJgBgHgGQgJgLgGgOIAdgCIAdAHQAKACAKAAQAKAAAKgCIgQgKIgTgJQgogHgfgZQgLgJgJgJQgKgKgHgLIgPgYQgNgVALgVQAGgKAMgCQAHgCAHACIA4AXIA2AaIAyAWIgFgLQgDgFgFgEQgvgigdgzQgKgSgHgSQgIgUgFgTQgFgUAKgQQAFgJAIgHQAIgIAMgBIANAIQAbAYAaAbIAqAnQAIAIAKAEQAKAEAKACIAPgBIgahBQgMgfgRgaQgIgOgLgLQgMgLgNgKIg8grQgTgQgSgRQgVgVgLgaQgDgGABgHQACgQAFgQIACgFIAFgDIAvAKQBBALBDADQA+ABA9gFIAsgEQAlgHAjgIIAZggIAHAEIAFAFIAGAGIAMASQAbgCAagFQAmgEAlgIQA8gOA7gTQAcgKAcgIIBBgUIAygOQBBgSBDgGQBIgCBFAFIAHgEQAIgEAJgDQAIgDAJACQAtAJAsAMIBXAZIBbAfQAqAOArAMIA6ASQAXAGAMAUIATAgIAJANIAUAYIAEAIQACAFAAAFIAAAFQgBAGgEAGIgWAfQgGAHgDAIQgGANgCAOQgBAMADALIAIAbQAEALABAMIACAMIADAFIAEgBQADgDACgEQAIgTAGgUIAXhHQAIgZALgXQAGgPAKgMQAHgIAIgFQAHgFAHAEQAWAMAJAXQAEAOgBAOIgEAkQgEAdgIAcQgFAUgJAUIgJAVIAIAFQAIgFAHgHQAHgGAGgIQAwg/AphEQARgdAUgbQAFgHAJgDQAGgCAIAAQAJACAFAGQAHAJABAKIADAgQAAAPgDAQIgDAPIgMAiIgcBFQgLAYgNAXQgLATgMASQgSAagZAWIgiAcQg1ArhFABQgeAAgegFQghgCgggHIghgHIgqgJIhIgNIg4gGIhEgGQgkgEgkgBQgiAAgiACIgzAGQgsgGgmgQQgggMgZgXIgXA0QgXAwggAsQgQAVgXAQQgOAIgPAGQgSAIgTAHIgKgIIgKgIQgIgEgIgDQg0gCg2gFIhEgCQgggBgegCQhGgDhGAIIghAHQgyAEgyAJIhCAMQgRAEgRAAQgdAGgdACIg7gCgAvmDHIgEACIgBAEQADARAOALIAUASQAOAMAQAIQATAKAUAHIAYAIIASAEQAmAJAoAEIAcAEIA6gJIABgCIAAgJQg9gQg8gMQgjgIgigNQglgPgkgRIglgRIgFAAIgDAAgAoZjMQgEAfgCAfQgEA+AKA7QAUB3BEBkQANAUAQATQATAWAXAWQAFAEAGABQARAEARACIA6AIIAzAIIAoADQAmADAegWQANgJAKgMQAPgSALgTQAdgwAXgyQh0B0gJAGQgKAFgLAAQgYAAgWgHQgVgHgUgLQg+gkgsg6Qg6hMgjhXQgQgngNgpIgSg9QgGgUgHgUQgDgHgGgFQgFgEgFgDQgJAFgCAKgAudAsQAAAGACAGQAGAWAKAVQAaA2AuAmQASAPAUAMQAWAOAYALQAlAQAnAPQAUAHAUAKQAKAGAMACIAigJIgBgHQgfgSgegVQgVgQgYgKQgogRgjgaIgvgnIgugrIgfggQgNgOgRgIgAr5hbIgGAUQgGASACASQACAUAGASQAMAqAXApQANAXAPAXQATAcAXAXQAfAeAiAcQAaAUAcARQAKAGAMAEQAbgDAaAAQASAAASACIAGgLQgcgagggWQgVgPgTgRQgbgcgagdQgWgZgVgbQgVgcgTgcIguhEIgXghQgKgNgLgLgAlricIAEAHQgFAJgJAEQgkAQghAXQAAAGACAHQAEAQAGAQQAiBbA9BOIAhApQAQASAVASQAbAXAiALQARAGAQAAQAMAAAJgFQALgEAJgIIAWgRIAEACQAAAEAEABQACABBYhTIAEgKQAfhMAWhOQAHgZAJgXQAFgMAHgLQARgWAFgcIAHglQAJg4AagyIAHgQIABgFIgEgHQhIgDhFARQgeAJggAHQh3Ach3AkQgsANgtAMIg0ALQgVADgUgGIALAbIAJAaIAIARIAJAEIAHgEQAZgUAegMIAIgBQAHAEADAHgAFNCkIBJADIBMAHQAhAFAhAIIA9AQQAiAJAkADQAZAEAbAAIA2gLQARgEAQgIQAQgHAOgKQAfgVAXgcQAXgdATghQATgiAMgkIATg+QAGgVAAgXQgBgJgFgGIgFAAQglA9glBBQgMAUgRARIgmAkIgVATIgXAQQgSAMgTAKQgLAHgMADQgIABgIAAIABgHQAbgcAVggQAIgNAGgPQALgcAHgcQAHgeACgeQACgYgCgYIgBgGIgJgCIgFADQgIAkgKAhQgTA+glA2QgTAcgUAaIgLAOIgGABQgQgFADgRIAKgrQADgQgBgPQgDgfgGgdIgHghIgGgEIgKAFQgDAkgNAfIgPAiQgFAOgDAQIgGAEQgLADgGgJQgNgUgSgRIgPgQIgHgJIgpggIgEADQABAFADAEQAaAkAcAlQAjAuAxAbQATAKAVAIQAXAHAZAEQAQADARAAIAZgHIASgGIAFAAIADACIADADIACALIgBAHQgHAIgIACQgYAFgYAEQgbACgZgFIgrgOQgNgFgMgHQgYgPgVgSQgRgOgPgQQgcghgagjIgagkQgCgEgEgBIgIgEQgeAJgcARIgIAGQALAVASARQAGAFAEAIQgGADgIgCQgLgFgKgHIgcgVIgJgFQgRABgQAEIgtAGQgRAFgSABIgwADQgZAFgZACIg8ANQgPADgNAIQgHAEgEAGIgPAXIAGADQAkANAgAUQALAHANAEIBGgDIBKgFIAvAAIAYAAgArxByIAAAAQgFAfAYASIAoAgQgKgggYgYIgZgZIAAgBIAAABgApcAQQgGAIgDAJQgBAEADAEQAMAUAOAUQAcApAfAoIANAOQAEAEAHADIAGABIABgLQgXglgPgoQgOgigLgjIgGgYIgHgHQgUAFgNAPgABXheIgiCHIACADQACACADABIAHADIABABIADABIAGgBIBKgMIARABIAJgMIgVgZQggglgOgwQgDgJgEgFIgHgFgAJCg/QgZAMgZALIggAPIgEACQA0AcAIAFIAOAOQAKALANADIAGAAQAAgfAKgeIALgkIACgIIgGgIIgFgCQgPAGgOAIgADQlQQgHAKgGAOQgEALgHAKQgNAPgLATQgMAVgHAXQgJAcgFAgQgBAMgEAMIgFAMQAYBKA7AyQALAKAPAGQAYAIAagGIAugOQAZgHAZgJQAbgLAagOIAcgNIgBgCIACABIARgHQAAgGgBgFIgDgMQgCgIABgHQADgVAMgPQAHgKAbgQQgIgEgIgHQgXgUgNgdIgXgIIgCgMQAEgJAKgCQAbgEAbAGQAYAHAWAOQAcARAhAJQAHABAHgCIAXgIQgzgfg5gNQgvgJguAAQgcgBgbABIAIgeIABgFQgcgPgggGQgXgGgXgEIhVgRIgEACgArdiAQAHAFAFAHQAYAiAWAjIAdAvIAFAHIALAAIAEgHIAAgLIAAgEIACgFIACgDIARgGIAUgFQAEgCACgDQAEgGABgHQAAgTgBgUQgCgbAGgbQAHgeABgfIgkALQggAHggADQgkAHglAEIhIABQgigEghgGQgjgEgjgHIgIABQgBAHADAGQAEALAJAJQAGAEAHADIASAIQAEAKAGAKQAFAIAJAGIAVAOIAiAXIAHAGIAKAAQAFgCADgFIAFgGIAOgYQALgTAWgKQAJACAJAEgAHlhdIgBABIAEAMQACAFAEAFQAFABAEgBQAagHAYgJQAKgEAGgIIgtACQgEgEgDgEQgDgFgBgFQgTADgJASgAJyiIQATACASAFQAGACAEAFIAOAQIAFgBQAKgOgBgPQgBgTgQgJQgKgFgLABQgwAHgvAMIgWAEQgLAAgJgCIgNAhIALABIAjgNQAagLAcAAIANABgAH0jMIgDADQACANAKAIQAPAMASAIIAMgNQgCgOgKgJQgOgKgOAAQgHAAgHACg");
    this.shape_22.setTransform(-0.0324, 0.0056);

    this.shape_23 = new cjs.Shape();
    this.shape_23.graphics.f("#A6654D").s().p("AkKEJQgogEgngJIgRgEIgZgIQgUgIgTgJQgQgJgNgLIgVgSQgOgLgDgRIACgEIADgDQAEAAAEABIAlAQQAkASAlAOQAiANAjAJQA9AMA8AQIAAAIIgBACIg6AKIgbgEgAFFEHIgogEIgzgHIg5gIQgSgCgRgEQgGgBgEgEQgXgWgUgXQgQgTgNgUQhChjgVh3QgKg8AEg9QACgfAEgfQACgKAJgGQAFADAGAFQAFAFACAHQAIATAFAVIASA9QANApAQAmQAjBXA7BNQAsA5A9AlQAUAKAWAIQAWAHAXAAQALAAAKgGQAJgFB0h1QgXAzgdAvQgLAUgOARQgLANgNAJQgbATghAAIgIAAgAh9DzQgUgKgUgIQgngOglgRQgYgLgWgNQgUgMgSgPQgugmgag3QgKgVgGgVQgCgGAAgGIAKgEQASAIANANIAeAfIAuArIAvAnQAjAbApARQAXAKAWAPQAdAVAfASIABAHIghAKQgMgDgLgFgAgdDlQgbgRgbgUQgigcgfgeQgXgWgTgdQgPgWgNgXQgXgpgMgqQgGgTgCgTQgCgSAGgSIAGgVIAOgDQAKAMAKANIAYAhIAtBEQAUAcAUAbQAVAbAWAaQAaAdAcAbQARASAVAPQAgAVAcAaIgGAMQgSgCgSAAQgaAAgaACQgLgDgLgHg");
    this.shape_23.setTransform(-51.05, 4.875);

    this.shape_24 = new cjs.Shape();
    this.shape_24.graphics.f("#592626").s().p("ACpCoQgkgDgigJIg9gQQghgIgggFIhMgHIhJgEQgkAAgjABIhKAFIhGADQgNgEgLgHQgggUgkgOIgGgCIAPgXQAEgGAHgFQANgHAPgEIA8gMQAZgDAZgEIAwgDQASgBARgFIAtgFQAQgEARgBIAJAFIAcATQAKAHALAGQAIACAGgDQgEgIgGgFQgSgSgLgTIAIgHQAcgQAegKIAIAEQAEACACADIAaAkQAZAjAcAhQAPAPARAOQAVASAYAQQAMAHANAFIArAOQAZAFAbgDQAYgDAYgGQAIgBAHgJIABgHIgCgKIgDgDIgDgCIgFAAIgSAGIgZAHQgRgBgQgCQgZgEgXgIQgVgHgTgLQgxgagjgvQgcgjgZgmQgDgEgBgFIAEgCIAoAgIAHAJIAPAQQASARANATQAGAKALgDIAGgEQADgQAFgOIAPghQANggADgkIAKgGIAGAFIAHAgQAGAfADAeQABAPgDAQIgKArQgDARAQAFIAGgBIALgPQAUgaATgbQAlg1ATg/QAKgiAIgjIAFgDIAJACIABAGQACAYgCAYQgCAegHAdQgHAdgLAcQgGAPgIANQgVAggbAcIgBAGQAIABAIgBQAMgDALgHQATgKASgNIAXgPIAVgTIAmgkQARgQAMgVQAlhCAlg9IAFAAQAFAHABAJQAAAWgGAWIgTA+QgMAlgTAhQgTAggXAdQgXAdgfAVQgOAKgQAHQgQAIgRAEIg2AKQgbAAgZgDg");
    this.shape_24.setTransform(50.9768, 4.75);

    this.shape_25 = new cjs.Shape();
    this.shape_25.graphics.f("#FFC600").s().p("AhiAxQgEgFgCgGIgEgLIABgBQAJgSATgDQABAFADAEQADAFAEAEIAtgCQgGAIgKADQgYAKgaAGIgGABIgDAAgABQgCQgEgFgGgCQgSgFgTgCQgigDggANIgjAMIgLgCIANgfQAJACALAAIAWgFQAugLAwgHQALgBAKAFQAQAIABAUQABAPgKAMIgFABIgOgOg");
    this.shape_25.setTransform(59.2512, -11.95);

    this.shape_26 = new cjs.Shape();
    this.shape_26.graphics.f("#FFFFFF").s().p("AgQADQgKgHgCgMIADgEQAWgHATAQQAKAIADANIgNANQgRgIgPgMg");
    this.shape_26.setTransform(52.55, -18.4197);

    this.shape_27 = new cjs.Shape();
    this.shape_27.graphics.f("#884B4B").s().p("AguBMIgBgBIgHgDQgDAAgCgDIgCgDIAiiHIAJgHIAHAFQAEAGADAIQANAwAgAlIAVAaIgJALIgRAAIhJALIgGABIgDgBg");
    this.shape_27.setTransform(11.475, -2.5);

    this.shape_28 = new cjs.Shape();
    this.shape_28.graphics.f("#874F4F").s().p("ADHEWQgjgMgbgXQgUgRgQgTIgigpQg8hNgihcQgGgQgEgQQgCgGAAgHQAigWAigRQAJgEAFgIIgDgIQgEgHgHgDIgHABQgeAMgZATIgGAEIgKgDIgHgRIgJgbIgLgaQAUAFAUgCIAzgMQAugMArgNQB3gkB4gcQAfgHAfgIQBFgRBIACIAFAIIgBAEIgHARQgaAygKA3IgHAlQgFAcgQAWQgIALgEAMQgKAYgHAXQgWBPggBMIgEAKQhXBUgDgBQgDgCgBgDIgDgDIgWASQgJAHgLAFQgJAEgMAAQgRAAgQgFgAlLDVQgYgTAGgfIAYAZQAZAZAKAgIgpgggAhjDoQgGgCgFgFIgNgOQgegogdgpQgOgTgMgUQgCgEABgFQADgJAGgHQANgPATgGIAIAHIAGAZQALAjANAiQAQAnAWAlIgBAMIgGgCgAjyAyIgegwQgWgjgXghQgGgIgHgEQgIgEgKgDQgVALgMATIgOAXIgEAHQgEAFgFACIgJgBIgHgGIgigXIgWgNQgIgHgGgIQgGgKgEgKIgRgHQgIgDgFgFQgJgIgFgLQgDgGABgHIAJgBQAiAGAjAEQAiAHAiADIBIgBQAkgEAlgHQAggDAggGIAjgLQgBAegHAfQgGAbACAbQACATgBATQAAAGgEAGQgDAEgEABIgTAFIgRAHIgDADIgBAEIgBAEIABAMIgFAHIgLABIgEgHg");
    this.shape_28.setTransform(-40.3875, -4.916);

    this.shape_29 = new cjs.Shape();
    this.shape_29.graphics.f("#E2E0D9").s().p("AidCzQgPgHgLgKQg7gzgYhKIAFgLQAEgMABgMQAFgfAJgdQAHgXAMgUQALgTANgQQAHgKAFgLQAFgOAHgKIAEgBIBVAQQAXAEAXAGQAgAGAcAPIgBAGIgIAeQAagBAcAAQAuABAvAIQA5AOAzAeIgWAJQgIACgHgCQghgIgcgSQgWgOgYgGQgagHgcAFQgKACgEAJIADAMIAWAHQAOAdAWAVQAIAGAIADQgbARgHAJQgMAPgCAVQgBAIACAHIACAMQABAGAAAGIgRAHIgCgBIABABIgcANQgaAOgaALQgYAKgZAIIguANQgMADgMAAQgOAAgNgEg");
    this.shape_29.setTransform(38.35, -15.5293);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_29}, {t: this.shape_28}, {t: this.shape_27}, {t: this.shape_26}, {t: this.shape_25}, {t: this.shape_24}, {t: this.shape_23}, {t: this.shape_22}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-103.1, -36.6, 206.2, 73.30000000000001);


  (lib.Symbol9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#391200").s().p("AABMXQgJgKgIgLQgEgHAAgIQABgUAEgUQAFgUAGgUIAZhFIAfhVQgKAIgIAKQgOAQgMARQgdApglAjIgmAiQgMALgOAKQgIAFgIACIgSAFQgFABgFgBQgIgCgEgGQgIgKgCgNQgBgHACgIQAGgRAIgQQAOgbAPgaIAdguQAWgiATgkIAig+QALgTAIgTIAHgRQgdAXgWAdQgSAYgXAUIgrAmIgwAmQgbATgfASQgUALgXABQgYgEgJgWQgEgIgBgJQgBgNAHgLQARgaAVgZIAsg2IAsg1IAigmQAVgYASgcQgkARghAbQgIAHgKAGQgWAMgXAKQgYAMgYAKIgTAIIgaAMQgMAFgNgGIgfgQQgIgFACgMQACgPAKgLQAqgsAtgnIATgTIAKgMQgwAOgxAFQgUgCgRgNQgHgFgEgGQgEgHgCgIQgCgIAEgIQAEgIAGgFQAagXAagVQAOgLgCgSQgBgRgJgRQgbg0gVg5IgRgzIgJgmQgGggABggQACglAJgjQAFgVgHgVQgIgYgGgZQgFgUAEgVIAOhBIADgTQgRASgVAJIgpAQIg0ARIg3ASIgqAMQgqgVgrgRQgRgGgQgIIgjgTQgOgHgNgJIgegWIgSgOQgVgSgYgLQgSgIAHgSQAHgTASgEQAQgDARAAIAUAAIAmAAQAqgFApABIBQgBIBFgFIBFgIQAzgGAzgKQAtgJAugLIA5gOQA6gPA3gRIBQgYQA5gSA6gMQAmgHAngFIAngHQAhgDAhACQBPADBMAXQAdAIAdALQAmAOAmANQATAIATAGIAnAJQAxAKAvATQAQAHANALQALAJAJALQASAUAXAOQAGADAFAGQAJAMgCAPQgDAYgQASQgLANgNALIgRAMIgVggQggAFgdAJIg0ATQgOAGgNAHIgeASIgwAVIg4AZQAAAGADAGIAMAcQAMAYALAbQAIATAKAUQAPAeARAeIAkA8IA3BXQAlA5AaA8QAMAbAJAbQAEAPADAPIAOBNQAQBQAhBNIAjBNIAhBAQAWApAZAqQAcAvAjAsIAXAfQALAQgIATQgCAEgDAEQgJAJgNACQgTAAgRgLIgpgfQgVgRgUgSQgkgjgdgoIgHAGQAWAsATAtQAHARgDATIgCANIgKAGQgYgCgWgLQgRgJgPgLQgNgJgLgMQgOgOgMgPIgfgoQgcgkgSgpQgGgPgIgOQgFgIgGgHQgFAfALAdQAGARAAARQAAAYgDAYQgCAMgEAMIgFAKIgJACQgPgBgHgLQgNgUgOgTQgWgdgUgeQgKgPgJgPQgIAGgCAJQgBAOgGAMQgCAFgFAFQgFABgFgCQgHAAgGgEQgJgIgIgKQgKgPgJgQQgNgagKgaQgJgYgHgYIgDgIQgOADgMgBQgGACgFgEQgEgEgCgFQgGgSgEgSIgCgQQgVAOgEAXQgMA+gZA8QgRAngTAlQgSAmgWAkQgPAZgRAZQgVAfgiAQIgFACQgEAAgEgEgACAF/QgbBKgXBMQgKAigLAgIgSA5QgGASgFASQgEATgIATQgDAHAAAJIAGAEQAHgBAFgFQAOgOANgPQAUgaARgdQAbgyAUg2QAahKAXhMQAIgbAGgcQAah3APh5IAMhsQADgZgJgaQgchVgghVIgmhoQgZhHgThJQgKgogEgoQgCgbABgaQAAgGAEgEIAFgEQALgBAHAIIAEAIIAHAUQAGgSgDgSQgCgSgEgSQgEgMAFgLIAKgXQAKgbAHgcQACgFgBgGQgJgDgNABIhIAOQhOAJhOAZQiFAriFAgQgfAIggAHQhQAPhUAGQhIAEhIAMQhDAIhCgGIgPABQgOABgMAFQANAFAMAGQAVALASAQIAgAeQATATAZAJIAzASQA2ARA2gQIAugQQAcgJAagQIAdgUIAAgBQgOgIgLgNQgDgDAAgFQABgMALgFIA9AfQANgGAGgNQAIgTARgMQAKgGAMAAQAKAKgEAMQgCAHgFAGIgXAeQgWAfgJAkQgFASgCATIgFA1QgBAHABAIQABAIAEAIIAfgOQAIgBAEAIQADAJgFAHQgZAkgKArQgJAqAHAvQAEAcAIAcQAHAcALAbQACAGAFAHIAeAAQAFAFAAAFQABAOgEARQgIAqAiAZIASgBQAIABAEAIQAEAHgFAFIgGAFIgoAeQgbAUgYAVQgEAEgCAFIgBAIQAIAPASgFQAagJAagCQAmgFAigRQAcgOAXgVIAJgIIAFgDIAJADIAMAFQABAJgFAHIgVAWQg7A6g9A2IhEA8IAGARIANgBQAegIAcgLQAwgSArgcIA2gkQAZgRAWgWQAOgNAMgPIAOACIAFAIIgDAHQgyBlhJBVQgTAVgRAWQgmAwgqAxIAFAOQANADANgDQAUgFASgKQAOgIANgJQB7hcBXh7IAagiIANAEQAFAMgIAKIgPASQgOASgCAXQgCAWgFAWQgIAdgNAbQgQAggRAgQgsBNgxBKQgIANAIAMIAIADQAvgeAogrQAagcAYgeQAhgrAfgtQAWggAQgeQAHgMANgEgAKGGLIAOAcQAgBFArA/QBABcBYBEIAbASIAHACIAGgHQACgHgHgHQgsgxghg2QgohAgkhEQgkhFgbhLIgchSQgFgOgCgPQgDgMAAgMIgBgQIgIgDQgMAFgDAMQgCAIgHAIQgIALgNAEQgYAGgZgKIgNgCQgSACgNALQgGAFgHABQgIADgJAAQgRgEgPgLQhGgzg1hEIgGgKQgJgRgGgSQgFgOgCgOIAAgEIAFgGQAVgEAFAVQAIAeAQAcQAHALAJAKIAfAiIAMAOQALAOAQALQAMAHAMAGIAdAMIALgFIALgJIAEgFQgBgFgCgDIgIgQIgFgLIAGgKIAHgCQATAKAPASQAHAHAJAHQAKAGAMgEIASgJQAJgFAIgGQAbgUgGghQgGgdgLgcQgOglgSgjQgQgjgYgiQhNhxg+h4IgPgfQgMgXgMgWQgEgGgFgFQgZABgYAMQgFACgFAEIAFAOQAIARAKARQANAWAOAWQAIAOAHAOQACAEgBAGQgBAIgJABQgjgrgegxQgKgRgMgPQgFgGgHgCQgWgEgVAEQgMAAgLAFIgRAHQgGACABAHQACAUAGATIAYBGQAfBVAXBVIAMAuQAFAagGAcIgFAgIgHBGIgEAgIgNBKQgIAtAQAqIABAEQgGARAEATIAIAhQAOgiAIgkQACgLAGgHQAEgFAHgCIAHAAIAFAIQgHAmAEAlQAHBAAcA6QAIATALARIAGAIIAIgDIgBgSIgFgbQgKg9ABg/IACgoQAMgJALAHQAEACAAAEQAEAYADAZQADAfAKAfQAXBFAhA/QANAXATAVIADAEIAKgCQACgHgBgHQgQhRgChUIgDhdIAEgCIALAHQAOAyAVAyQAYA7AfA4QATAiAYAeQAeAkAlAZIARAMIAMgIQABgNgJgQQgMgUgHgVQgfhXgjhUIgZg7QgFgLgCgLQgBgDABgDIALgEQAJADAEAJgACfmSIANAVIADAAIAIAAIAIgEIAHgEIACgKIg8hEQADAiAQAfgADWrlQgHAEgHAGQgFAEgDAHIgGANIgGARQgDAMgCANIgRAVIAJANQAEAHABAIQAEAYgIAWQAGARADAUIAAAFIgCADIgKAKQAIAhATAbQATAaAaAUQAEAEAGADQAGADAGgCQAcgEAbgGQAQgEARgGQANgEALgJQAYgTAcAMIAYgLIAqgRQATgIASgLQAGgDAFgGQgCgNgKgKQgIgIADgMQACgHAEgGIASgXIAEgGQgFgJgKgFQgNgHgLgKQgJgIgGgJQgGgIgEgJIAEgHIAIgDIAaAAQAcABAaAKIA6AWIAJACIADgEIABgEIABgEIgBgGQgqgRgsgGIhFgJIg8ACQgHgHABgKIABgLIABgGQgFgEgGgCIhNgeQgZgJgZgEIghgCQgVAFgWAJgAJcpBQgkAKgRAfQAOAFANgHIASgKQAcgRAjgEQAWgCATALIAaAOIADABIAOgMQgGgNgNgIQgOgJgPgCIgRgBQglAAglANg");
    this.shape.setTransform(0.0141, 0.016);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#592626").s().p("AF8JXIgbgSQhYhEhAhcQgrg/gghFIgOgcQgEgJgJgDIgLAEQgBADABADQACALAFALIAZA7QAjBUAfBXQAHAVAMAUQAJAQgBANIgMAIIgRgLQglgagegjQgYgegTgiQgfg4gYg8QgUgxgOgzIgLgGIgEABIADBeQACBTAQBSQABAGgCAHIgKACIgDgEQgTgUgNgYQghg/gXhFQgKgfgDgfQgDgZgEgYQAAgDgEgCQgLgIgMAKIgCAnQgBA/AKA9IAFAbIABASIgIADIgGgHQgLgSgIgSQgcg7gHhAQgEglAHgmIgFgHIgHgBQgHACgEAFQgGAIgCAKQgIAlgOAhIgIghQgEgTAGgRIgBgEQgQgqAIgsIANhLIAEgfIAHhFIAFghQAGgcgFgbIgMguQgXhVgfhVIgYhFQgGgTgCgVQgBgGAGgDIARgHQALgEAMAAQAVgEAWADQAHACAFAHQAMAPAKAQQAeAxAjAsQAJgCABgIQABgFgCgFQgHgOgIgOQgOgVgNgXQgKgQgIgRIgFgPQAFgDAFgDQAYgLAZgCQAFAFAEAGQAMAWAMAXIAPAfQA+B4BMByQAYAhAQAjQASAlAOAkQALAdAGAdQAGAggbATQgIAGgJAFIgSAJQgMAFgKgHQgJgGgHgIQgPgRgSgKIgHABIgGAKIAFALIAIAQQACADABAFIgEAFIgLAKIgLAEIgdgMQgMgFgMgIQgQgKgLgPIgMgOIgfghQgJgKgHgLQgQgcgIgeQgFgVgVAFIgFAFIAAAEQACAOAFAPQAGARAJARIAGAKQA1BDBGAzQAPALARAFQAJgBAIgDQAHgBAGgFQAMgLASgCIANADQAZAJAYgGQANgEAIgLQAHgHACgJQADgMAMgFIAIADIABARQAAAMADALQACAPAFAOIAcBSQAbBMAkBFQAkBDAoBAQAhA2AsAyQAHAGgCAHIgGAHIgHgCgAlmoDIgDAAIgNgVQgQgfgDghIA8BDIgCAKIgHAFIgIADIgIAAg");
    this.shape_1.setTransform(53.4141, 13.4);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#874F4F").s().p("AFPLyQAAgJADgIQAIgSAEgTQAFgTAGgRIASg5QALggAKgjQAXhLAbhKIgKgDQgNAFgHAMQgQAegWAfQgfAugiAqQgYAegaAdQgoAqgvAeIgIgCQgIgNAIgMQAxhKAshNQARggAQghQANgbAIgdQAGgVACgXQACgXAOgRIAPgTQAIgKgFgLIgNgEIgaAiQhYB6h7BcQgNAKgOAHQgSAKgUAGQgNADgNgDIgFgPQAqgwAmgxQARgWATgVQBJhUAyhlIADgIIgFgHIgOgCQgMAOgOAOQgWAVgZASIg2AkQgrAbgwATQgbAKgeAIIgNACIgGgRIBDg8QA9g3A7g5IAVgXQAFgGgBgJIgMgGIgJgCIgFADIgJAIQgXAUgcAOQgiARglAGQgaABgaAJQgSAFgIgOIABgIQACgGAEgDQAYgWAbgUIAngdIAGgFQAFgGgEgHQgEgHgIgCIgRACQgigaAIgqQAEgQgBgOQAAgGgFgEIgegBQgFgGgCgHQgLgbgHgbQgIgcgEgdQgHguAJgrQAKgrAZgjQAFgHgDgJQgEgIgIAAIgfAOQgEgIgBgIQgBgHABgIIAFg1QACgTAFgSQAJgjAWgfIAXgfQAFgGACgHQAEgMgKgJQgMgBgKAHQgRALgIATQgGAOgNAGIg9ggQgLAFgBANQAAAEADAEQALAMAOAJIAAAAIgdAVQgaAQgcAJIguAQQg2AQg2gRIgzgSQgZgKgTgSIgggeQgSgRgVgKQgMgHgNgEQAMgFAOgCIAPgBQBCAHBDgJQBIgLBIgEQBUgGBQgQQAggGAfgJQCEggCFgrQBOgYBPgJIBIgOQANgCAJAEQABAFgCAFQgHAcgKAcIgKAXQgFAKAEANQAEARACASQADATgGARIgHgUIgEgHQgHgJgLABIgFAEQgEAFAAAGQgBAZACAcQAEAoAKAnQATBKAZBGIAmBoQAgBWAcBVQAJAZgDAaIgMBsQgPB5gaB3QgGAbgIAbQgXBMgaBKQgUA2gbAyQgRAegUAZQgNAQgOAOQgFAEgHABg");
    this.shape_2.setTransform(-32.1966, -0.7475);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#FFC600").s().p("AhfAaQASgeAkgKQArgQAvAEQAPACANAJQANAIAGAMIgNAMIgEgBIgagOQgTgKgVACQgjAEgcAQIgSAKQgHAEgIAAQgGAAgGgCg");
    this.shape_3.setTransform(64.7, -56.2735);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#E2E0D9").s().p("AibCwQgGgDgEgEQgagUgTgaQgTgcgIghIAKgJIACgEIAAgEQgDgUgGgRQAIgWgEgYQgBgHgEgHIgJgOIARgVQACgMADgMIAGgSIAGgNQADgGAFgFQAHgFAHgEQAWgKAVgEIAhACQAZAEAZAIIBMAfQAGACAFAEIgBAFIgBALQgBALAHAGIA8gBIBFAIQAsAHAqAQIABAGIgBAFIgBADIgDAEIgJgCIg6gWQgagJgcgCIgaAAIgIAEIgEAHQAEAIAGAIQAGAKAJAIQALAJANAIQAKAFAFAJIgEAEIgSAYQgEAGgCAGQgDANAIAIQAKAJACANQgFAHgGADQgSALgTAIIgqARIgYALQgbgMgYATQgLAIgNAEQgRAHgQADQgbAHgcAEIgEAAQgEAAgEgBg");
    this.shape_4.setTransform(40.325, -57.8219);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-95.7, -79.4, 191.5, 158.9);


  (lib.Symbol8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#391200").s().p("AlqJzQgUgEgVgDQg5gKg6AEIhlAGIgoAHIgrAEQgRAAgNgJIgRgMIgbgNQgNgGgMgHQgYgQgRgXQgUgZgHgfQgDgQAPgIQAJgFAKAAIAMAFQATAKAMARQAGAIALACQAJADAGgBQgOgQgLgRQgDgFABgFIACgHIAFgDIA7AaQAHgLgCgLQgDgSAIgOQAKgRAMgPQAGgJALACQAIABAHAFIAjAbIAKAGQANgDAIgLQAXgjAMgpIAQgzQAFgPgGgQIgMgoQgFgQgCgQQgEgZABgYQAEg3AHg1QACgQAIgMIAHgNIgMgKIggARQgZgEgSgSQgMgMADgTQADgUARgJQANALAOgHQAGgDAEgFIAOgSIAOgRQASgWADgcIgJgMIgaAXIgMAFIgNgJQgFgJAHgKQAIgNAKgMQAWgZAdgSQAGgEABgIQADgKABgLQABgNgIgLQgFgGgIACQgLAEgJAFQgjARgkAOQg0AVg2AJQgbACgZgLQg2gXg0gbQgcgOgZgPQgLgfgXgWQgIgHgFgJQgFgIgEgKQgFgOgMgLQgLgKAFgMQADgHAFgFQAGgGAHgDQALgDAKADQAOgBAOAFQAlANAqAGQBWAGBWgEQBHgBBHgKICUgYQAugKAugNIA/gRIEFhHQBmgbBoAIQA9AGA8ANQAhAHAgAJQAfAJAeAMIA5AWQAiAPAnAGQAgAGAaAXQAWATAUAWQAPAQANASQAEAGgCAHIgFAWIgCAGQgGATgMANQgKAKgMACQgJgCgEgHIgCgFQgEgGgHgBQguAHgsATQgkAQgjASQgoAVgrATIgVAKQgGACgEAEQAAAYALAUIALASQASAgARAiQAVAqAYApIA1BZIAgA3QAUAlAPApQAPArAWAqQAdA2AoAyQAZAfAiAQQATAJAWgFQAZgFAUgSQALgKAJgLIAEgDQAaAFABAaQAAAJgDAJIgHAWIAHAJQAdgGAXgVQAKgLAHgNQAJgRAHgRQADgIAHgDQANgFAFAOQADAHgBAIQgFAmgHAlQgCAJgFAHQgFAIgHAGIgsAjQgUAPgVAJIgKACIgngHQgMAAgMACIgzAOQgPAFgQAAQgQAAgQgDQgXgGgWgJQgOgGgNgIIgbgQIgPgIQAAAGADAFIANAXQADAEAAAHQACAPgPAFQgNAGgPAEIgSgJQgKgGgIgHQgbgagagbQgbgdgPgjQgCgFgEgEQgFgEgFgCQgIAZAGAZQADAKgBAMQgBAKgKAFIgMAAQgfgdgTgjQgIgQgFgQQgLglgJglQgQATAEAaQABAIgBAHQgDAVgWAFQgGABgGgGQgLgJgIgLQgXgegLggIgLgoQgEgEgHAEQgRAKgWgDQgbgGgTgWIgXgdQgRgXgOgYIgMgUIgGgCIgGAEIgIAbQgiB1hKBiQgpA1gtAwQgRASgTARIgzAsIggAaIgJAGQgigFgigIgAn5FlQgEAJgCAKQgCALgBALIAAANQACBFAjA9QAIAOAJANQAJALAPAEQA4AIA4AMIAHABQAPADAKgKIACgCIgFgMQgkAEgfgMQgfgLgVgbQgVgagOgeQgYgwgOgwIgBgDIgHgagAsZInIAVAOQARAKASAIQAlAQAmgKIAKgCQg1gJgwgZIgPgJIgqgbIgYgRQAOAfAbAUgAqoGlQgIASADAQQACANAGAMQAEAJAFAJIAMATQAMASAPAQQASATAaAHQAOAEAPADIAagBIAEgOQgegRgYgaQgYgagXgcIgQgVQgNgQgPgOQgCgCgDAAgAnmE5QALAFAHAQIAKAcIAJAdQAJAgAMAgQANAgAWAXQAKAMAOAIQAaAOAbAMQAcALAgACIAbgUQATgQARgSIAngrQAmgtAfgwQATgeARgfQAWgnARgnQALgbAGgbQANg1AHg3IAEggQAFgzACg0IADhIIAFhOIAEg6IAIhZIAEgjQADgWAFgWQABgGAEgFQAHACADAJQAFARgBASIgDBQIgBA7QAAAKAEAJQADAEAEAEQAagEAXABIARADIAFgDIADgDIgDgDQgOgOgIgSQgTgxgLgzQgIgjgFgjQgCgSABgSQABgSAFgSQAHgZAKgZIAPghIABgFIgMgDQhpAKhlAfIg4ASQgeAIgdAGIhDASIheAbQguAMgtAJQg3AKg5AFQgqACgsABIgcAAQgtAEgtABIg9AAQglgEgkgHQgZgFgYgHIgWgGIgNAEIgBAMQAWAMAQAVIAQAWQALAQAQAKQgCAQANAJQApAeAxAOIARAHQANAEAOADQANAEANACQAdAAAbgIQAzgPAvgWQAMgFAFgMIAIgOIAJgEIAPAHQAJAEAKAAQAFACAEgCIAJgHQAIgMAHgMIARgcQAIgNAOgIIAMAGQAEALgIAKIgPATQgcAkgMApIgIAZIAMAEIALgLIAIgCIAOAOQgJAdgOAdQgQAigMAkQgHAVgEAXQAAAFACAFIADAFIAbgCQAPANgGASQgJAbgYARQgIAHgEAJQgIANgEAQQgPA1gEA4QgCAYACAYQAAAKAEAKQAKAeASAZQAGAJgDAJIgHAUQgDAKgBAJIABAIQAJgJAKAAQAFAAAGADgArQH+IAEAJIAEAHQALAUAXAIIAFgKIgkgogApLGzIgHAKQgCADgBAEQgDAKAIAJQAPASARARQAUAUAaANQgSgjgHgnIgHguIgDgeQgVAWgRAYgAHFEkQAEAAADADIACAEIgGAVQgJAaAGAaQAFAWAKAVQAJAWANAVQAQAaAWAXIAQAPIAMALIAHAEIAJgGQACgLgGgKQgkg1gVg8QgDgJAAgJQAAgBAAAAQAAgBABAAQAAgBAAAAQAAgBABAAIAPgBIAhAuQAKANALAMQAiAhAsAWQAaANAcALIARAGIAQABIAigFIgugYQgigPgcgYQgRgOgPgRQgPgPgNgQQgIgMgHgMQgDgFAAgFQAAgEABgCQADgFAFgEIAFgDQAIACAEAIIAdAqQAJAMAMAJQASAOAUAMIAUAOIAJAFIAUAJQASAHATADQAPADAQABIAQgDQgFgKgLgDQgRgBgRgGQgRgHgPgIQgjgVgageIgbghQgNgRgLgSIgjg5IgSghQgOABgFAKQgHANgPAAQgbAAgWgMIgDgBIgEgCIgkgYIgDgCQgEgNALgMIAFAAQAdAZAlAJQAIAAAIgCQAJgDABgJQADgKAAgKQABgKgBgKIgCgUQgCgLgFgJQgOghgSgeQgYgrgagoQguhLgmhPIgJgTIgYgzQgIgQgQADIgeALIgMADIgDABQgFALAGAMQAQAfASAgIAJAQQAeAzAiAyIARAXIATAWIAeAoIADAHIgDAIQgLAKgKgJQgLgLgIgMIgog8IhEhpIgLgRIgKgQIgVgjIgVgiQgHgNgNAHQgIADgHAFQgPALgUgBQgwgBgxAEQgKAfgBAiIgCA8QgBAygGAxIgBAIQgEAlAAAlQAAAMADAMQACANAGAMQANAbARAaQAQAZATAXQAFAGAGAEIAFADIBGgWQARAOADAXQAGArAQAoQAEAJAFAHIAKgBQADgjASgiQAEgIAIgHIAKgKIAJgHIAFgDQAQAFACARIACAcQACAzASAvIADAIIAFAIQACAEAEACIAOAJQgGgzAMgyQAGgaAKgaQAEgIAFgIIADAAQAHAAAHACgAoehoQgCAMgJAIIgkAgIAAAFIAFAGQAMgCAMgGQAQgHAQAIIAHgFQADgegOgZgAC8pVIgXAXQgZAXgDAkQgCAaAAAbQAAAbACAaQAAALADAMIAKApQACAFgCAGIgBAGQAHALAGALIASAqIAFANQAHAOANgDIATgFIBRgWQAbgGAbgIQBEgXA9gjQAHgDAFgFIAAgFQgPgRgHgWQgCgGABgHQACgJAEgJIAGgVQACgHgCgHQgCgIgFgHIgUgeIAIgLQAYgFAWAJQAUAHAWAFIAyAKQABAAAAAAQAAABABAAQAAAAAAAAQABAAAAAAIADgCIABgCIABgIQgEgIgHgBIgigHQgzgLg0gCQgFgDgDgEIgIgMQgGgMgOgEQhogjhtgMQgSAAgNAMgAILl+IgDAHIACAFQAIAFALgBQAegEAXgSIgUgCQgaADgZAFgAJTm8IgmALQgRAFgQAJQgLAFgIAKIgHALQAigQAmgKQATgFATgCQASADATAFQAQAGAPAIIAIADQAHACAEgHQAEgHgEgKQgDgIgIgDQgfgMghAAQgNAAgMACgAFJDDQgZgTgWgVQgRgQgPgRIgwg6QgRgUgUgQQgRgPgUgMIgOgIQgHgGgGgHQgMgNgBgSIAIgIQAWAQARAXIAEAHIANgHIgEgbQAEgNAPAAIAKADIAEAEIABADIACAbQABAWAMAVQArBOBHA2QAQAMASAJQATALAXABQAGACAGgDQAIgEAFgHQAGgIAEgJQAFgOABgPQASAKACAVQAAAKgDAKQgDAIgFAHQgFAIgHAFQgHAFgJACQgNADgOAAQgegDgXgSg");
    this.shape.setTransform(0.0235, -0.0065);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#592626").s().p("Ak/GnIgHgBQg4gMg4gIQgPgEgJgLQgJgNgIgOQgjg9gChFIAAgNQABgLACgLQACgKAEgJIAKgBIAHAaIABADQAOAwAYAwQAOAeAVAaQAVAbAfALQAfAMAkgEIAFAMIgCACQgIAIgKAAIgHgBgArjGSQgSgIgRgKIgVgOQgbgUgOgfIAYARIAqAbIAPAJQAwAZA1AJIgKACQgOAEgOAAQgYAAgXgKgApLGMQgagHgSgTQgPgQgMgSIgMgTQgFgJgEgJQgGgMgCgNQgDgQAIgSIAEgCQADAAACACQAPAOANAQIAQAVQAXAcAYAaQAYAaAeARIgEAOIgaABQgPgDgOgEgArKFZIgEgHIgEgJIALgGIAkAoIgFAKQgXgIgLgUgAIqFgIgMgLIgQgPQgWgXgQgaQgNgVgJgWQgKgVgFgWQgGgaAJgaIAGgVIgCgEQgDgDgEAAQgIgCgJAAQgFAIgEAIQgKAagGAaQgMAyAGAzIgOgJQgEgCgCgEIgFgIIgDgIQgSgvgCgzIgCgcQgCgRgQgFIgFADIgJAHIgKAKQgIAHgEAIQgSAigDAjIgKABQgFgHgEgJQgQgogGgrQgDgXgRgOIhGAWIgFgDQgGgEgFgGQgTgXgQgZQgRgagNgaQgGgMgCgNQgDgMAAgMQAAglAEglIABgIQAGgyABgyIACg8QABgiAKgfQAxgEAwABQAUABAPgLQAHgFAIgDQANgHAHANIAVAiIAVAjIAKAQIALARIBEBpIAoA9QAIAMALALQAKAJALgKIADgIIgDgHIgegoIgTgXIgRgXQgigygegzIgJgQQgSgggQgfQgGgMAFgLIADgBIAMgDIAegLQAQgDAIAQIAYAzIAJATQAmBPAuBLQAaApAYArQASAeAOAhQAFAJACALIACATQABAKgBAKQAAAKgDAKQgBAJgJADQgIACgIAAQglgJgdgZIgFAAQgLAMAEANIADACIAkAYIAEACIADABQAWAMAbAAQAPAAAHgNQAFgKAOgBIASAhIAjA5QALASANARIAbAhQAaAeAjAVQAPAIARAHQARAGARABQALADAFAKIgQADQgQgBgPgDQgTgDgSgHIgUgJIgJgFIgUgOQgUgMgSgOQgMgJgJgMIgdgqQgEgIgIgCIgFADQgFAEgDAFQgBACAAAEQAAAFADAFQAHAMAIAMQANAQAPAPQAPARARAOQAcAYAiAPIAuAYIgiAFIgQgBIgRgGQgcgLgagNQgsgWgighQgLgMgKgNIghguIgPABQgBABAAAAQAAABAAAAQgBABAAAAQAAAAAAABQAAAJADAJQAVA8AkA1QAGAKgCALIgJAGIgHgEgABWjoQABASAMANQAGAHAHAGIAOAJQAUAMARAPQAUAQARAUIAwA6QAPARARAQQAWAVAZASQAXASAeADQAOAAANgDQAJgCAHgFQAHgFAFgIQAFgHADgHQADgKAAgKQgCgVgSgKQgBAPgFAOQgEAJgGAIQgFAHgIADQgGADgGgCQgXgBgTgKQgSgJgQgMQhHg2grhOQgMgWgBgWIgCgbIgBgDIgEgEIgKgDQgPAAgEANIAEAbIgNAHIgEgHQgRgXgWgQg");
    this.shape_1.setTransform(0.225, 18.0532);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#E2E0D9").s().p("Ai9CtIgGgNIgSgqQgFgLgIgLIACgGQABgGgBgFIgKgpQgDgMgBgLQgCgZAAgbQAAgbACgaQADgkAagXIAWgXQANgMASAAQBuAMBmAjQAOAEAHAMIAIAMQADAEAFADQA0ACAzALIAhAHQAHABAFAIIgBAIIgCACIgCACQgBAAAAAAQgBAAAAAAQAAAAgBAAQAAgBAAAAIgzgKQgVgFgUgHQgWgJgZAFIgIALIAVAeQAEAHACAIQACAHgBAHIgHAUQgEAJgCAJQgBAHACAGQAHAWAQARIAAAFQgGAFgGADQg+AjhCAXQgbAIgcAGIhQAWIgTAFIgFABQgJAAgGgMg");
    this.shape_2.setTransform(37.575, -42.5493);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#FFC600").s().p("AqDDEIABgEIAjggQAKgJACgMIAKgDQANAZgCAdIgIAFQgPgHgRAHQgMAGgLABgAHUh9IgCgFIADgIQAZgFAagDIAVACQgYASgdAFIgEAAQgJAAgHgEgAHEifQAIgKAKgGQAQgIASgGIAlgKQAugGAsAQQAHADAEAIQAEAJgFAIQgEAGgGgBIgIgDQgQgIgQgGQgSgFgTgDQgTACgSAEQgnAKghARIAHgLg");
    this.shape_3.setTransform(5.3691, -24.4568);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#874F4F").s().p("ABZI/QgbgMgagOQgOgIgLgLQgUgYgNgfQgMghgJggIgJgdIgKgcQgHgQgLgFQgQgHgOANIgBgHQABgKADgKIAHgUQADgJgHgJQgRgZgLgeQgDgKAAgKQgCgYACgYQAEg4APg1QAEgPAHgPQAFgJAIgGQAYgQAIgcQAHgRgQgOIgaACIgDgEQgCgGAAgFQADgXAIgVQAMgkAPgiQAOgdAKgdIgPgNIgHABIgLALIgMgDIAIgaQAMgpAcgjIAPgUQAIgKgEgLIgMgGQgPAIgHANIgRAcQgHAMgIAMIgJAHQgEACgFgCQgLAAgIgDIgPgIIgJAEIgIAOQgFAMgNAFQguAWg0APQgaAIgdAAQgNgCgOgEQgNgDgNgEIgSgGQgwgOgqgfQgNgJACgQQgPgKgMgQIgPgVQgQgWgXgMIACgMIANgEIAWAGQAYAIAZAEQAkAHAlAFIA8gBQAugBAtgEIAcAAQArgBAqgBQA5gGA3gKQAtgJAugMIBdgbIBDgRQAegHAdgIIA5gSQBmgfBpgKIAMADIgBAFIgPAhQgKAZgHAaQgFARgBATQgBARACASQAEAjAJAjQALAzATAxQAIASAOAOIADADIgDAEIgGACIgQgCQgXgCgaAEQgEgEgDgEQgEgJAAgKIABg7IADhQQABgRgGgSQgCgJgIgCQgDAFgBAGQgFAWgDAXIgEAjIgIBYIgFA6IgEBOIgEBIQgBAzgGA0IgDAhQgHA2gNA2QgGAagMAbQgRAngWAnQgRAggUAdQgeAxgmAsIgoArQgQASgUAQIgaAUQgggCgcgLgAiaINQgRgRgPgSQgIgJADgKQAAgEADgDIAHgKQARgXAVgXIADAfIAGAuQAHAmATAkQgagOgUgUg");
    this.shape_4.setTransform(-40.55, -1.95);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-101.5, -64, 203.1, 128);


  (lib.Symbol7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("ABeFCQgHgEgFgNQgRgjgRgsQgKAYgUAXQgKALgHAAQgFABgGgEQgFgDgDgGQgEgIgBgRQgJAJgNADQgNADgMgFQgMgFgHgMQgHgLABgNQgGAEgIAMQgIANgGADQgHADgIgCQgIgCgFgHQgIgKgCgTQgDgjAVgfQAGgIAOgPIAPgRIAGgaQAchqAUhpQADhCAcg4QAKgVABgGIABgLQABgGACgEQAEgJASgIQAMgEAFACQAHACAFAMQAGATACAYQABAPAAAdQgBBBAFAqQAGA6ATAtQAIATAKALQARAQAHAJQALAOABAUQAAATgLAPIgFAHQgDAEAAADQgBAIAJAKQAPASAKAUQARAigFAZQgEAPgMALQgNAKgPgCQAFAVgDALQgDAIgHAEQgFADgEAAIgGgBg");
    this.shape.setTransform(-2.6964, -104.3208);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#99FFFF").s().p("ABfFqQgQgBgMgKQgRgOgNgnQgMAXgKAMQgPARgRACQgYADgUgWQgNgOgLgcQgHAGgKANIgQAVQgKAKgMAEQgOAEgKgGQgNgIgDgXIgBgXQgBgOgCgJIgHgYQgGgXAGgaQAGgZAbgpQAdgsAHgWQAGgRADgYIAFgqQAEgoANgyQAGgaATg+IAjhxQAOgvAagBQAQgBAMAQQAJAMAGAUQAMAsAGBHQAIBkADARQAIBBAbAdIAPAPQAJAJAEAHQAGAMAEAbQAGAtgVALQAbAeAPAmQAPAngBAoQgBAWgIAIQgLANgTgFQgPgDgMgPQAEAigLANQgKAKgPAAIgCAAgABIExQAFANAHADQAHADAIgEQAHgFADgIQADgKgFgWQAPADANgLQAMgKAEgQQAFgYgRgiQgKgUgPgTQgJgKABgHQAAgEADgEIAFgGQALgPAAgUQgBgTgLgPQgHgJgRgPQgKgMgIgTQgTgsgGg7QgFgpABhCQAAgdgBgPQgCgYgGgSQgFgMgHgDQgFgCgMAFQgSAHgEAKQgCAEgBAGIgBAKQgBAGgKAWQgcA4gDBBQgVBpgbBrIgHAZIgOARQgOAPgGAJQgVAeADAkQACATAIAKQAFAGAIACQAIADAHgEQAGgDAIgMQAIgNAGgDQgBANAHALQAHALAMAFQAMAFANgDQANgCAJgKQABARAEAIQADAGAFAEQAGADAGAAQAHgBAJgKQAUgYAKgYQARAsARAkg");
    this.shape_1.setTransform(-1.6594, -104.2747);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_42();
    this.instance.setTransform(-66.85, -139.05, 0.3662, 0.3662);

    this.instance_1 = new lib.CachedBmp_41();
    this.instance_1.setTransform(-66.9, -138.5, 0.3662, 0.3662);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-66.9, -140.5, 134.5, 289.8);


  (lib.Symbol5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_58();
    this.instance.setTransform(-42.85, -38, 0.0761, 0.0761);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-42.8, -38, 85.9, 76);


  (lib.Locked_1copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_39();
    this.instance.setTransform(-162.8, -223.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-162.8, -223.5, 483.5, 775);


  (lib.Locked_1copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_38();
    this.instance.setTransform(-109.15, -230.55, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-109.1, -230.5, 483.5, 775);


  (lib.Locked_1copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_37();
    this.instance.setTransform(-189.95, -225.95, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-189.9, -225.9, 483.5, 775);


  (lib.Locked_1copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_36();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Locked_1copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_35();
    this.instance.setTransform(-241.8, -258.1, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -258.1, 483.5, 775);


  (lib.Locked_1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_34();
    this.instance.setTransform(-241.8, -155.1, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -155.1, 483.5, 775);


  (lib.C_1_ONcopy13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("6", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_33();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("5", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_32();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("4", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_31();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("3", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_30();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("2", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_29();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("4", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(51,204,102,0.549)").s().p("A0pVaQpRoXAxsqQAxsqIzopQIzooLvABQLwACIEIsQIEIrAYNEQAWNEoKHqQoKHpsUAOIgqABQr5AApBoIg");
    this.shape.setTransform(-2.6405, -24.3306);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_28();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("2", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(51,204,102,0.549)").s().p("A0wVlQodoQANssQANsrIqo3QIro3LsAEQLuADIYJJQIYJKAUM7QAVM9o5HiQo5Hhr7AIIgXAAQrsAAoVoIg");
    this.shape.setTransform(-0.3177, -24.3699);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_27();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("1", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(51,204,102,0.549)").s().p("AgGd1QscgJoaoYQoZoYAcsYQAdsYJDpcQJDpcLhA+QLjA+H7JMQH8JNAVLzQAVLzocIXQoVIPsMAAIgYAAg");
    this.shape.setTransform(-0.1577, -25.9536);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_26();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("1", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_25();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("6", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(51,204,102,0.549)").s().p("A0qVgQogoRAPs1QAPszIWorQIWoqL0AAQL0AAIWIqQIWIrAmMrQAmMtovIXQovIXsGACIgHAAQsCAAodoPg");
    this.shape.setTransform(0.9752, -22.4118);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_24();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("5", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(51,204,102,0.549)").s().p("A09VVQoqohApsHQAosGHwpBQHxpBMNgJQMNgIIjIrQIjIsAWMsQAXMsoQIHQoQIHsmATIg1ABQsDAAoXoQg");
    this.shape.setTransform(0.7834, -24.3999);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_23();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("3", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(51,204,102,0.549)").s().p("A0YWAQoboAAFtOQAFtNIoozQIno0L1AQQL2AQIDIsQIDIqAXNCQAXNDomH3QonH3r6AMIgkAAQrkAAoOnzg");
    this.shape.setTransform(0.4414, -23.1574);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_22();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.Blendcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Blend_0();

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Blendcopy, new cjs.Rectangle(0, 0, 83, 77), null);


  (lib.Pathcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.rf(["#2F0202", "#3B0101", "#3B0303", "#4A0101", "#600101", "#790306", "#A50003", "#820506", "#C00004"], [0.604, 0.635, 0.663, 0.698, 0.729, 0.776, 0.808, 0.816, 0.839], 2.7, -3.2, 0, 2.7, -3.2, 26.8).s().p("AiKCLQg6g5AAhSQAAhQA6g6QA6g6BQAAQBSAAA6A6QA5A5AABRQAABSg5A5Qg6A6hSAAQhQAAg6g6g");
    this.shape.setTransform(19.7, 19.725);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Pathcopy, new cjs.Rectangle(0, 0, 39.4, 39.5), null);


  (lib.Path_1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(0, 0, 0, 0);


  (lib._3_Starscopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_21();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_20();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_19();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_18();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_17();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Stars = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_16();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Star_Rcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_15();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_14();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_13();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_12();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_11();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_R = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_10();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_stae_Lcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_9();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_8();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_7();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_6();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_5();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_L = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_4();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib.Symbol460 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy5("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol459 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy5("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol458 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy4("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol457 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy4("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol456 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy3("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol455 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy3("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol454 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy2("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol453 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy2("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol452 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol451 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol450 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_R("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol449 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_L("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol21 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Tween5("synched", 0);

    this.instance_1 = new lib.Tween6("synched", 0);
    this.instance_1.setTransform(75.8, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance_1}]}, 779).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true, x: 75.8}, 779).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-177.1, -59.6, 430.1, 119.2);


  (lib.Symbol18 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Tween3("synched", 0);

    this.instance_1 = new lib.Tween4("synched", 0);
    this.instance_1.setTransform(43.6, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance_1}]}, 739).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true, x: 43.6}, 739).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-138.9, -58, 321.5, 116.1);


  (lib.Symbol17 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Tween1("synched", 0);

    this.instance_1 = new lib.Tween2("synched", 0);
    this.instance_1.setTransform(117.35, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance_1}]}, 699).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true, x: 117.35}, 699).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-152.1, -80.2, 421.6, 160.4);


  (lib.Symbol16 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol12copy("synched", 0);

    this.instance_1 = new lib.Symbol13("synched", 0);
    this.instance_1.setTransform(3.25, 13.65);

    this.instance_2 = new lib.Symbol14("synched", 0);
    this.instance_2.setTransform(5.5, 104.05);

    this.instance_3 = new lib.Symbol9("synched", 0);
    this.instance_3.setTransform(15.25, 116.5);

    this.instance_4 = new lib.Symbol8("synched", 0);
    this.instance_4.setTransform(21.05, 101.05);

    this.instance_5 = new lib.Symbol10_1("synched", 0);
    this.instance_5.setTransform(19.55, 73.7);

    this.instance_6 = new lib.Symbol15("synched", 0);
    this.instance_6.setTransform(-6.1, 43.5, 1.1556, 1.1556);

    this.instance_7 = new lib.Symbol16copy("synched", 0);
    this.instance_7.setTransform(2.9, 26.3);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_2}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 3).to({state: [{t: this.instance_7}]}, 3).wait(3));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-104.2, -96.5, 226.8, 292.5);


  (lib.Symbol14_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Symbol5("synched", 0);
    this.instance.setTransform(25.55, -0.05, 0.8096, 1.8762, 0, -8.1926, 171.8116);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-19.1, -75.5, 89.1, 151);


  (lib.Symbol11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Symbol7("synched", 0);
    this.instance.setTransform(25.55, -6.05, 1.3654, 1.1411, 0, 0, 180);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-66.7, -166.4, 183.60000000000002, 330.8);


  (lib.Symbol9_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Symbol21();
    this.instance.setTransform(-852.55, -352.75, 1.231, 1.231, 0, 0, 0, 0.1, -0.1);

    this.instance_1 = new lib.Symbol21();
    this.instance_1.setTransform(390.05, -329.75, 1.231, 1.231, 0, 0, 0, 0.1, -0.1);

    this.instance_2 = new lib.Symbol18();
    this.instance_2.setTransform(-87.65, -354.7, 1.231, 1.231, 0, 0, 0, -0.1, -0.1);

    this.instance_3 = new lib.Symbol17();
    this.instance_3.setTransform(-527.1, -355.15, 1.231, 1.231, 0, 0, 0, -0.1, -0.1);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.lf(["#FFFFFF", "#00CCFF"], [0, 1], 7.6, -67.3, 7.7, -382.8).s().p("EiIUA6oMAAAh1PMEQpAAAMAAAB1Pg");
    this.shape_5.setTransform(-206.575, -118.025);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_5}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol9_1, new cjs.Rectangle(-1079.1, -493.2, 1745.1, 750.4), null);


  (lib.Tween6copy13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy13("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy13("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy12("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy12("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("4", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-3.95, -40.25, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(51,204,102,0.549)").s().p("AnXHuQjBjEAAkkQAAkiDJjKQDKjJEPAAQERABC7DJQC7DJAIEuQAIEvjIC3QjHC2kUACIgEAAQkRAAjAjCg");
    this.shape.setTransform(-0.0207, -0.312);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_62();
    this.instance.setTransform(-80.85, -81.7, 0.5, 0.5);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.5);


  (lib.Tween6copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween5copy13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy13("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy13("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy12("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy12("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy8("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween4copy13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy13("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy13("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy12("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy12("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy8("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween3copy13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy13("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy13("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy12("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy12("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy8("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween2copy13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy13("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy13("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy12("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy12("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy8("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween1copy13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy13("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy13("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy12("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy12("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy8("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Symbol15copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Symbol12copy2("synched", 0);
    this.instance.setTransform(0, 0.05, 1, 1, 0, 0, 0, 0.7, -0.8);

    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("Ehv7BXiIAAomMDf2AAAIAAImgEhv7hXhMDf2AAAMAAACmcMjf2AAAgEhjTBJgMDDfgEBMAAAiSVMjD9AAAg");
    this.shape.setTransform(-2.1, 2.425);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-718.4, -557.7, 1432.6999999999998, 1120.3000000000002);


  (lib.Symbol7copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFC000").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACIgEgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape.setTransform(5.8, -5.3833);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#542000").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBAAAAgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape_1.setTransform(5.8, -3.8833);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f().s("#F0EDED").ss(0.1, 1, 1).p("AEbABQAAB0hUBTQhSBTh1AAQh0AAhUhTQhShTAAh0QAAh1BShTQBUhTB0AAQB1AABSBTQBUBTAAB1g");
    this.shape_2.setTransform(6.125, -3.425);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("rgba(0,0,0,0.329)").s().p("AjIDIQhShTAAh0QAAh1BShTQBUhTB0AAQB1AABSBTQBUBTAAB1QAAB0hUBTQhSBTh1AAQh0AAhUhTg");
    this.shape_3.setTransform(6.125, -3.425);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#F3F3E8").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACIgEgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape_4.setTransform(5.8, -5.3833);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("rgba(52,100,196,0.769)").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBAAAAgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape_5.setTransform(5.8, -3.8833);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).to({state: [{t: this.shape_3}, {t: this.shape_2}]}, 1).to({state: [{t: this.shape_5}, {t: this.shape_4}]}, 1).wait(2));

    // Layer 1
    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#FFFFFF").s().p("AgbBfQgEgEAAgFQAAgJALgFQApgTAMgZQgWAAgKgGQgMgGAAgUQAAgPAJgOQAJgPAOAAQARAAALAOQAKANACATIAoAAQgHgQAAgKQAAgOAMgNQANgPAUgBQALAAALAHQAOAGAAALQAAALgJAAQgEAAgIgEQgIgDgFAAQgVABAAAQQAAAKAHAGQAHAFAKAAQAKAAAVgFIAGAAQAOAAAAAKQAAALgMAFQgfANg1gBIgoAAQgNAvg5AXIgHABQgGgBgDgDgAAMgRQgDADAAAEQAAAHAKACIAPABQAAgIgEgFQgEgHgHAAQgEAAgDADgAh0A3QABgMATgCIAegDQAWgFAAgGQACgOgggaQgLgKABgHQAAgGAFgEQAEgDAGAAQAPACARAaQARAZAAAQQAAATgOAKQgIAGgUAGQgNADgPABQgcgBACgPgAilAoQgKgIAAgPQAAgLAFgMQAEgJAFgGQgLgFAAgJQAAgGAFgFQAEgGAGAAQAUABAVARQAVATAAATQAAAVgPALQgMAMgSgBQgQAAgJgHgAiXAHQAAALAMAAQAFgBAFgCQAFgDAAgEQAAgLgTgIQgIAKAAAIgAiQhAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGgAi8hAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGg");
    this.shape_6.setTransform(5.6822, -3.8401, 0.8691, 0.8691);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#330000").s().p("AgbBfQgEgEAAgFQAAgJALgFQApgTAMgZQgWAAgKgGQgMgGAAgUQAAgPAJgOQAJgPAOAAQARAAALAOQAKANACATIAoAAQgHgQAAgKQAAgOAMgNQANgPAUgBQALAAALAHQAOAGAAALQAAALgJAAQgEAAgIgEQgIgDgFAAQgVABAAAQQAAAKAHAGQAHAFAKAAQAKAAAVgFIAGAAQAOAAAAAKQAAALgMAFQgfANg1gBIgoAAQgNAvg5AXIgHABQgGgBgDgDgAAMgRQgDADAAAEQAAAHAKACIAPABQAAgIgEgFQgEgHgHAAQgEAAgDADgAh0A3QABgMATgCIAegDQAWgFAAgGQACgOgggaQgLgKABgHQAAgGAFgEQAEgDAGAAQAPACARAaQARAZAAAQQAAATgOAKQgIAGgUAGQgNADgPABQgcgBACgPgAilAoQgKgIAAgPQAAgLAFgMQAEgJAFgGQgLgFAAgJQAAgGAFgFQAEgGAGAAQAUABAVARQAVATAAATQAAAVgPALQgMAMgSgBQgQAAgJgHgAiXAHQAAALAMAAQAFgBAFgCQAFgDAAgEQAAgLgTgIQgIAKAAAIgAiQhAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGgAi8hAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGg");
    this.shape_7.setTransform(5.6822, -2.8901, 0.8691, 0.8691);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape_7}, {t: this.shape_6}]}, 1).to({state: []}, 1).wait(2));

    // Layer 1
    this.instance = new lib.CachedBmp_61();
    this.instance.setTransform(-25.1, -35.05, 0.2582, 0.2582);

    this.instance_1 = new lib.Pathcopy();
    this.instance_1.setTransform(6.2, -3.4, 1.3907, 1.3907, 0, 0, 0, 19.7, 19.8);
    this.instance_1.compositeOperation = "screen";

    this.instance_2 = new lib.CachedBmp_45();
    this.instance_2.setTransform(-25.4, -35.1, 0.2582, 0.2582);

    this.instance_3 = new lib.Path_1copy();
    this.instance_3.setTransform(6.1, -3.8, 1.4695, 1.4695, 0, 0, 0, 19.7, 19.7);
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_60();
    this.instance_4.setTransform(-25.3, -35.2, 0.2582, 0.2582);

    this.instance_5 = new lib.CachedBmp_59();
    this.instance_5.setTransform(-30.05, -39.9, 0.2582, 0.2582);

    this.instance_6 = new lib.Blendcopy();
    this.instance_6.setTransform(-0.45, 1.55, 1, 1, 0, 0, 0, 41.5, 38.5);
    this.instance_6.compositeOperation = "multiply";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_6}, {t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-41.9, -39.9, 83.9, 80);


  (lib.Symbol1copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy5("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy5, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy4("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy4, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy3("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy3, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy2("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy2, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Stars("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Less_1_offcopy12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol10copy6("synched", 0);
    this.instance.setTransform(-1336.55, 174.05, 14.9975, 14.9975, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1895.6, -21.7, 1117.6999999999998, 616.5);


  (lib.Less_1_offcopy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol10copy5("synched", 0);
    this.instance.setTransform(-1336.55, 174.05, 14.9975, 14.9975, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1895.6, -21.7, 1117.6999999999998, 616.5);


  (lib.Less_1_offcopy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol10copy4("synched", 0);
    this.instance.setTransform(-1336.55, 174.05, 14.9975, 14.9975, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1895.6, -21.7, 1117.6999999999998, 616.5);


  (lib.Less_1_offcopy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol10copy3("synched", 0);
    this.instance.setTransform(-1336.55, 174.05, 14.9975, 14.9975, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1895.6, -21.7, 1117.6999999999998, 616.5);


  (lib.Less_1_offcopy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib.Symbol10("synched", 0);
    this.instance.setTransform(-1343.3, 160.05, 15, 15, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1902.5, -35.7, 1118, 616.5);


  (lib.Less_1_offcopy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol10("synched", 0);
    this.instance.setTransform(-1319.45, 172.85, 15, 15, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1878.6, -22.9, 1117.8999999999999, 616.5);


  (lib.Less_1_offcopy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol10("synched", 0);
    this.instance.setTransform(-1336.55, 174.05, 14.9975, 14.9975, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1895.6, -21.7, 1117.6999999999998, 616.5);


  (lib.Less_1_offcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol10copy2("synched", 0);
    this.instance.setTransform(-1336.55, 174.05, 14.9975, 14.9975, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1895.6, -21.7, 1117.6999999999998, 616.5);


  (lib.Less_1_offcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib.Symbol10("synched", 0);
    this.instance.setTransform(-1315.6, 216.75, 15, 15, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1874.8, 21, 1118, 616.5);


  (lib.Less_1_offcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib.Symbol10("synched", 0);
    this.instance.setTransform(-1335.95, 282, 15, 15, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1895.1, 86.2, 1117.8999999999999, 616.5999999999999);


  (lib.Less_1_offcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol10copy("synched", 0);
    this.instance.setTransform(-1336.55, 174.05, 14.9975, 14.9975, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1895.6, -21.7, 1117.6999999999998, 616.5);


  (lib.Less_1_offcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib.Symbol10("synched", 0);
    this.instance.setTransform(-1384.75, 219.65, 15, 15, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1943.9, 23.9, 1117.9, 616.5);


  (lib.All_Spacecopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_22
    this.instance = new lib.Symbol7copy2();
    this.instance.setTransform(-0.05, 615.3, 1.9367, 1.9367);
    new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib.Symbol7copy2(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_18
    this.instance_1 = new lib.Symbol15copy("synched", 0);
    this.instance_1.setTransform(1.75, -4.85, 1.2652, 1.3156, 0, 0, 0, -0.1, -0.2);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.All_Spacecopy, new cjs.Rectangle(-907.1, -738.3, 1812.7, 1473.9), null);


  (lib.AllSpace = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_6
    this.instance = new lib.Symbol11("synched", 0);
    this.instance.setTransform(-1120.3, -402.8, 0.7089, 0.7089, 0, 0, 0, 25.3, -0.6);

    this.instance_1 = new lib.Symbol11("synched", 0);
    this.instance_1.setTransform(-1172.45, -430.2, 0.7089, 0.7089, 0, 0, 0, 25.3, -0.6);

    this.instance_2 = new lib.Symbol11("synched", 0);
    this.instance_2.setTransform(-1094.25, -471.35, 0.7089, 0.7089, 0, 0, 0, 25.3, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    // Layer_8
    this.instance_3 = new lib.CachedBmp_57();
    this.instance_3.setTransform(-1312.95, -749.9, 0.5, 0.5);

    this.instance_4 = new lib.Symbol11("synched", 0);
    this.instance_4.setTransform(392.15, -357.6, 0.563, 0.563, 0, 0, 0, 25.4, -0.5);

    this.instance_5 = new lib.Symbol11("synched", 0);
    this.instance_5.setTransform(275.15, -426.5, 0.563, 0.563, 0, 0, 0, 25.4, -0.5);

    this.instance_6 = new lib.Symbol11("synched", 0);
    this.instance_6.setTransform(329.45, -393.1, 0.563, 0.563, 0, 0, 0, 25.4, -0.5);

    this.instance_7 = new lib.Symbol11("synched", 0);
    this.instance_7.setTransform(385.9, -397.85, 0.563, 0.563, 0, 0, 0, 25.4, -0.5);

    this.instance_8 = new lib.Symbol11("synched", 0);
    this.instance_8.setTransform(429.25, -421.6, 0.4179, 0.4179, 0, 0, 0, 25.3, -0.4);

    this.instance_9 = new lib.Symbol11("synched", 0);
    this.instance_9.setTransform(379.3, -455.05, 0.4179, 0.4179, 0, 0, 0, 25.3, -0.4);

    this.instance_10 = new lib.Symbol11("synched", 0);
    this.instance_10.setTransform(307.65, -459.8, 0.4179, 0.4179, 0, 0, 0, 25.3, -0.4);

    this.instance_11 = new lib.Symbol11("synched", 0);
    this.instance_11.setTransform(262.05, -476.5, 0.4179, 0.4179, 0, 0, 0, 25.3, -0.4);

    this.instance_12 = new lib.Symbol11("synched", 0);
    this.instance_12.setTransform(212.1, -490.85, 0.4179, 0.4179, 0, 0, 0, 25.3, -0.4);

    this.instance_13 = new lib.Symbol11("synched", 0);
    this.instance_13.setTransform(338.05, -426.4, 0.4179, 0.4179, 0, 0, 0, 25.3, -0.4);

    this.instance_14 = new lib.CachedBmp_56();
    this.instance_14.setTransform(-1226, -723.35, 0.5, 0.5);

    this.instance_15 = new lib.CachedBmp_1();
    this.instance_15.setTransform(-217.8, -719.85, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_15}, {t: this.instance_14}, {t: this.instance_13}, {t: this.instance_12}, {t: this.instance_11}, {t: this.instance_10}, {t: this.instance_9}, {t: this.instance_8}, {t: this.instance_7}, {t: this.instance_6}, {t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}]}).wait(1));

    // Layer_15
    this.instance_16 = new lib.Symbol14_1("synched", 0);
    this.instance_16.setTransform(-1078.55, 862.75, 3.4995, 1.653, 0, 17.7465, 8.4941, 25.7, -0.1);

    this.instance_17 = new lib.Symbol14_1("synched", 0);
    this.instance_17.setTransform(546.65, -159.85, 1.2718, 0.6007, 0, 17.7464, 8.4943, 25.6, -0.3);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_17}, {t: this.instance_16}]}).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#8C9193").s().p("A0JQJQoXmsAApeQAApeIXmqQIWmsLzAAQL0AAIWGsQIXGqAAJeQAAJeoXGsQoWGtr0AAQrzAAoWmtg");
    this.shape.setTransform(182.6552, -17.0964, 3.0797, 3.0797);

    this.instance_18 = new lib.Symbol9_1();
    this.instance_18.setTransform(-292.05, -488.3, 1.4018, 1.3175, 0, 0, 0, -417.2, -86.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_18}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.AllSpace, new cjs.Rectangle(-1312.9, -1024, 2682.5, 2173.6), null);


  (lib.Symbol2copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Starscopy5("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1copy5();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_Lcopy5("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol459("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));

    // Layer_3
    this.instance_3 = new lib._3_Star_Rcopy5("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol460("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5).to({_off: false}, 0).to({
      regX: -0.3,
      regY: -0.1,
      scaleX: 0.7989,
      scaleY: 0.7989,
      rotation: -28.602,
      x: -88.15,
      y: 20
    }, 3).to({rotation: -126.6131, x: -125.7, y: 28.9}, 3).to({
      _off: true,
      regX: -165.8,
      regY: 48.6,
      scaleX: 1,
      scaleY: 1,
      rotation: 0,
      x: -165.8,
      y: 48.6
    }, 2).wait(6).to({
      _off: false,
      regX: -0.3,
      regY: -0.1,
      scaleX: 0.7989,
      scaleY: 0.7989,
      rotation: -126.6131,
      x: -165.7,
      y: 48.9
    }, 0).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol2copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Starscopy4("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1copy4();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_Lcopy4("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol457("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));

    // Layer_3
    this.instance_3 = new lib._3_Star_Rcopy4("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol458("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5).to({_off: false}, 0).to({
      regX: -0.3,
      regY: -0.1,
      scaleX: 0.7989,
      scaleY: 0.7989,
      rotation: -28.602,
      x: -88.15,
      y: 20
    }, 3).to({rotation: -126.6131, x: -125.7, y: 28.9}, 3).to({
      _off: true,
      regX: -165.8,
      regY: 48.6,
      scaleX: 1,
      scaleY: 1,
      rotation: 0,
      x: -165.8,
      y: 48.6
    }, 2).wait(6).to({
      _off: false,
      regX: -0.3,
      regY: -0.1,
      scaleX: 0.7989,
      scaleY: 0.7989,
      rotation: -126.6131,
      x: -165.7,
      y: 48.9
    }, 0).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol2copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Starscopy3("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1copy3();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_Lcopy3("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol455("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));

    // Layer_3
    this.instance_3 = new lib._3_Star_Rcopy3("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol456("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5).to({_off: false}, 0).to({
      regX: -0.3,
      regY: -0.1,
      scaleX: 0.7989,
      scaleY: 0.7989,
      rotation: -28.602,
      x: -88.15,
      y: 20
    }, 3).to({rotation: -126.6131, x: -125.7, y: 28.9}, 3).to({
      _off: true,
      regX: -165.8,
      regY: 48.6,
      scaleX: 1,
      scaleY: 1,
      rotation: 0,
      x: -165.8,
      y: 48.6
    }, 2).wait(6).to({
      _off: false,
      regX: -0.3,
      regY: -0.1,
      scaleX: 0.7989,
      scaleY: 0.7989,
      rotation: -126.6131,
      x: -165.7,
      y: 48.9
    }, 0).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol2copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Starscopy2("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1copy2();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_Lcopy2("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol453("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));

    // Layer_3
    this.instance_3 = new lib._3_Star_Rcopy2("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol454("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5).to({_off: false}, 0).to({
      regX: -0.3,
      regY: -0.1,
      scaleX: 0.7989,
      scaleY: 0.7989,
      rotation: -28.602,
      x: -88.15,
      y: 20
    }, 3).to({rotation: -126.6131, x: -125.7, y: 28.9}, 3).to({
      _off: true,
      regX: -165.8,
      regY: 48.6,
      scaleX: 1,
      scaleY: 1,
      rotation: 0,
      x: -165.8,
      y: 48.6
    }, 2).wait(6).to({
      _off: false,
      regX: -0.3,
      regY: -0.1,
      scaleX: 0.7989,
      scaleY: 0.7989,
      rotation: -126.6131,
      x: -165.7,
      y: 48.9
    }, 0).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol2copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Starscopy("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1copy();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_Lcopy("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol451("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));

    // Layer_3
    this.instance_3 = new lib._3_Star_Rcopy("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol452("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol2copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Stars("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_L("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol449("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));

    // Layer_3
    this.instance_3 = new lib._3_Star_R("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol450("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol1copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy3("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy3("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.instance_2 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy9, new cjs.Rectangle(-212.7, -172.2, 425.7, 446.7), null);


  (lib.Symbol1copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.instance_2 = new lib.C_1_ONcopy("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy8, new cjs.Rectangle(-231.3, -172.2, 425.70000000000005, 423), null);


  (lib.Symbol1copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy7("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy7("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.instance_2 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy7, new cjs.Rectangle(-206.4, -172.2, 425.70000000000005, 405.2), null);


  (lib.Symbol1copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy6("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy6("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.instance_2 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy6, new cjs.Rectangle(-212.9, -172.2, 425.6, 405.6), null);


  (lib.OFFcopy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy12("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy13("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy13("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy13("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy13("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy13("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy13("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2copy6();
    this.instance_7.setTransform(77.75, -212.7, 0.2774, 0.2774);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(7));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-133.9, -221.9, 425.6, 457.1);


  (lib.OFFcopy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy11("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy12("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy12("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy12("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy12("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy12("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy12("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2copy5();
    this.instance_7.setTransform(77.75, -212.7, 0.2774, 0.2774);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(7));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-133.9, -221.9, 425.6, 457.1);


  (lib.OFFcopy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy10("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy11("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy11("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy11("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy11("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy11("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy5("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2copy4();
    this.instance_7.setTransform(77.75, -212.7, 0.2774, 0.2774);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(7));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-133.9, -221.9, 425.6, 457.1);


  (lib.OFFcopy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy9("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy10("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy10("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy10("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy10("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy10("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy4("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2copy3();
    this.instance_7.setTransform(77.75, -212.7, 0.2774, 0.2774);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(7));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-133.9, -221.9, 425.6, 457.1);


  (lib.OFFcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy5("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy6("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy6("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy6("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy6("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy6("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy3("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2copy2();
    this.instance_7.setTransform(77.75, -212.7, 0.2774, 0.2774);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(7));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-133.9, -221.9, 425.6, 457.1);


  (lib.OFFcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy6("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy6();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy6("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy7("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy7("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy7("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy7("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy7("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy7("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_4}]}, 1).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-134, -221.9, 425.7, 461.1);


  (lib.OFFcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy2("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy3("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy3("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy3("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy3("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy3("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy2("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2copy();
    this.instance_7.setTransform(77.75, -212.7, 0.2774, 0.2774);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(7));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-133.9, -221.9, 425.6, 457.1);


  (lib.OFFcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy4("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).wait(2));

    // Layer_3
    this.instance_1 = new lib.Less_1_offcopy4("synched", 0);
    this.instance_1.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(20));

    // C
    this.instance_2 = new lib.Tween1copy5("synched", 0);
    this.instance_2.setTransform(78.7, 76.2);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween2copy5("synched", 0);
    this.instance_3.setTransform(78.7, -24.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween3copy5("synched", 0);
    this.instance_4.setTransform(78.7, -127.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween4copy5("synched", 0);
    this.instance_5.setTransform(78.6, -71.2);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween5copy5("synched", 0);
    this.instance_6.setTransform(78.7, -96);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween6copy11("synched", 0);
    this.instance_7.setTransform(78.7, -120.65);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).wait(8));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -120.65
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-126, -221.9, 425.7, 473.4);


  (lib.OFFcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy3("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy9();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy3("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy4("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy4("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy4("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy4("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy4("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy10("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({
      _off: true,
      y: -24.7
    }, 2).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(2).to({_off: false}, 2).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-133.8, -221.9, 425.8, 498.20000000000005);


  (lib.OFF = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy8("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).wait(2));

    // Layer_3
    this.instance_1 = new lib.Less_1_offcopy8("synched", 0);
    this.instance_1.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(20));

    // C
    this.instance_2 = new lib.Tween1copy9("synched", 0);
    this.instance_2.setTransform(78.7, 76.2);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween2copy9("synched", 0);
    this.instance_3.setTransform(78.7, -24.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween3copy9("synched", 0);
    this.instance_4.setTransform(78.7, -127.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween4copy9("synched", 0);
    this.instance_5.setTransform(78.6, -71.2);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween5copy9("synched", 0);
    this.instance_6.setTransform(78.7, -96);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween6copy9("synched", 0);
    this.instance_7.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_4}]}, 1).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).wait(8));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(3).to({_off: false}, 1).to({y: -76.2}, 1).to({
      _off: true,
      y: -127.7
    }, 1).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(5).to({_off: false}, 1).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-136.5, -221.9, 425.7, 461.1);


  (lib.B_1copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy7("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy7();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy7("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy8("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy8("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy8("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy8("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy8("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy8("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({
      _off: true,
      y: -24.7
    }, 2).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(2).to({_off: false}, 2).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-127.5, -221.9, 425.8, 457.70000000000005);


  (lib.B_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy8();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy2("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy2("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy2("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy2("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy2("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy6("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_4}]}, 1).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_5}]}, 1).to({state: [{t: this.instance_6}]}, 1).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      x: 78.65,
      y: -99.45
    }, 1).to({_off: true, x: 78.6, y: -71.2}, 1).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(7).to({_off: false}, 1).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-152.4, -221.9, 425.79999999999995, 482);


  (lib.aa1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.OFFcopy5();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-65.3, -25, 130.6, 72);


  (lib._6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.OFFcopy2();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off: false}, 0).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AhrFKQgMgHgGgOQgGgOAEgNQgYgEgcgNQhogThUguQg+gigkgoQgHABggAHQgYAGgPgGQgOgFgIgPQgHgOADgPQAEgQAbgXIAbgXIAAgJQgChPA+g5IABgGQgBgJgHgPQgEgPAHgQQAHgPAOgLQALgIASgHIAfgJQBcgfAwgKQBCgLAggHQATgFAzgQQArgNAagFQArgHBNAEIAWABQARgEAOAAQANAAAUAFQAVAAAOgCQArgEAJABQAdAAASAMIAXATQAKAHASAGIAfAKQAkANAfAiIADADIAQAMQAUARAHAQQAEAJABAPQAQAbACASQADATgIAZIgRArQgcBFACBNQACAiAKARQAFAJAQARQAPAQAFALQAIARgFAUQgEAUgOAOQgNANgUAFQgTAFgTgDQglgHgcgfQgRAXgcAKQgdAKgbgJIgkgNQgWgIgOAGQgHADgMAJQgMAKgHADQgKAFgUAAQgWgBgJADQgSALgLABQgNABgRgRQgVgSgKgDQgMgDgQAHIgaAQQgWAOgdAGQgLABgJAAQgVAAgOgIg");
    this.shape.setTransform(4.1753, 16.1337);
    this.shape._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1).to({_off: false}, 0).to({_off: true}, 1).wait(2));

    // Layer_4
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(41,88,161,0.329)").s().p("AhqFYQgXgRgBgUQgIgGgIgDIgBgBIgDgBQgMgIgKgFQgmgVgVgIIgCAAQgHgDgXgEIgVgCQgjgEgRgQQgPgMgQgTIgNgQIgaggIAAgBIgHgHIgEgFIgIAAIgcgDQgjAAgcgCQg0gFgTgOQgEgCAAgEQAAgDACgCQACgEAHgIIAJgHIAAgBIAIgIIAMgWQAIgOAJgDQACgDADAAQAPAAAHATIAAgBIAGAOQABgHAEgIIgBAAIACgGQAAgEACgCIAAAAIADgGQAAAAAAAAQABgBAAAAQABAAAAAAQAAgBABAAIgBgBQgBAAAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAIgZADg/QAEgzgCgaIAAgBQgCgFAAgFQAAgaAagXIAEgDIADgBIgBAAQAlgeBQgUQBcgYEPgzQAxgLA0gDQCYgIB8A7QAJADAJAFIAYAUIAkAeQASAJAnA7QAGA+gNA6QgFAZgHAYIgOAoQgIAXAAAkIACA9IACAAQAMAAAjAgQAoAjgFAPIAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAgBgBIgCgBIgBAFQgbAjgSALQgdATgfAHQgdAGgHgGQgDgDgFgLQABAAgCgOQgFgPgBgJIgEABIACACIACABIgCABIgBAAIgIACIgEABIgZAgIgBABIAAADQgFAGgHAFQgKAGgNACIgCAAQgUAAgngNIgPgFIABAAQgagJgNABIgBAAQgEAAgDACIAAAAQgOADgQAMIgCABIgIAGIgBAAIgHAGIgEABIgBAAIgBACQgVAOgOABQgVACgdgSIgBAAIgEgCIgCgBIgFgFQgNgJgMgEQgRgJgJABQgRAAgTAMIgCAAIgBABIgFADIgBABQgKADgFAEIgCAAIgMAGIgBABQgcARgRAAQgKAAgGgFg");
    this.shape_1.setTransform(5.0079, 14.5612);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(4));

    // Layer_3
    this.instance_1 = new lib.OFFcopy2();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-62.9, -20.3, 130.6, 72.2);


  (lib._5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("ABoFMQgHgDgLgIIgRgLQgagNgqAMIggAMQgUAIgOACQgqAJgXgYIgMgOQgHgJgHgDQgFgDgTgCQgUgDgfgUQgigVgRgFIgtgHQgcgEgNgLQgJgHgJgQIgQgZQgOgSgYgKQgWgJgagBIgngCQgVgEgLgMQgLgNAFgTQAFgSAQgHIAPgEQAKgCAFgEQAKgGAEgXQADgRABgcIADgvQADg5AWggQAcgrBCgWQAWgIA8gOICfgkIBQgRQA2gKBbgLQArgFAZgBQAlgDAeAEQApAFA5ATQA2ATAeATQAOAJAaAUIAaARQAPALAJAJQAbAbgFAeQAGAAAFAEQAFAFABAGQADAIgCAQIgHA8QgJBMgNAoIgIAbQgFAQgBAMQgDAjAVARIANAIQAJAEAEAEQAJAIAKAVQAJASgBAMQgCARgWARQgXASgcAOQgkASgVgMQgLgHgGgQQgEgLgDgTQggAdgRAKQgfASgcgEQgKgBgUgHQgUgGgKgBQgQgBgTAGIgiAOQgbALgVAAQgMAAgKgEg");
    this.shape.setTransform(3.8571, 22.7815);

    this.instance = new lib.OFFcopy();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_3
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(41,88,161,0.329)").s().p("AhqFXQgXgRAAgUQgIgFgIgEIgBgBIgDgBIgXgNQgmgVgVgIIgBAAQgIgCgWgEIgVgCQgjgFgRgQQgPgMgQgTIgNgPQgPgVgLgLIAAgBIgHgIIgEgEIAAAAIgIAAIgbgDQgkAAgbgDQg0gEgTgOQgEgDAAgDQAAgDACgCQACgFAHgHIAIgHIAAgBIAIgIIANgWQAIgOAIgDQACgDAEAAQAPAAAHATIAAgBIAFANIAFgOIAAAAIACgGQAAgEACgCIAAAAIACgGQABAAAAgBQAAAAABAAQAAAAAAgBQABAAAAAAIgBgBQAAAAAAAAQgBgBAAAAQAAgBAAAAQAAgBAAAAQAIgZADg+QAFgzgCgaIAAgCQgDgFAAgEQAAgbAagWIAEgDIADgCIgBAAQAlgdBQgUQBbgYEPgzQAxgLA0gDQCYgIB7A7QAJADAJAFIAYAUIAkAeQASAJAnA7QAFA+gNA5QgEAZgHAYQgGATgIAVQgIAXAAAkIACA8IABAAQANABAiAgIAAgBQApAkgGAPIAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBAAAAAAIgCgBIgBAEQgbAjgSALQgdATgfAHQgdAGgHgGQgDgDgFgLQABAAgCgOQgFgOgBgKIgEABIACADIACABIgCABIgBAAIgIACIgEABIgZAgIgBABIAAACQgFAGgHAFQgKAGgNACIgCAAQgUABgmgOIgQgFIABAAQgagJgNABIgBAAQgDAAgDACIgBAAQgOAEgQALIgBABIgJAGIAAAAIgHAGIgEABIgBAAQAAAAAAABQAAAAAAABQAAAAAAAAQgBAAAAAAQgVAPgOABQgWACgcgTIgBAAQAAAAgBAAQAAgBgBAAQAAAAgBAAQAAgBgBAAIgCgBIgEgDIgBgBQgNgKgMgEQgRgJgJABQgRABgTALIgCAAIgBABIgFADIgBABQgKADgFAFIgCAAIgMAFQAAAAAAAAQAAABAAAAQAAAAgBAAQAAAAAAABQgbAQgSAAQgJAAgHgFg");
    this.shape_1.setTransform(2.7579, 22.3143);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(4));

    // Layer_2
    this.instance_1 = new lib.OFFcopy();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-65.3, -12.5, 130.6, 72);


  (lib._4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AhvFLQgMgGgSgRQgUgSgLgGQgPgIgcgFIgugJQgdgIgxgiIhthNQgOgKgIgCQgGgCgWABQgvADgSgYQgIgJgBgNQgCgMAEgLQAFgLAJgHQAKgIALgCQAJgBARACQASADAIgBQAHgZgBgjQAAgUgDgqQgDhOAlgkQAQgPAZgKQAQgGAegHICdglQCNghBGgPQBDgOAogEQAigDBDABQA+ABAdAJQAUAGAhARIA7AcQAvAXAUARQAkAbAJAjQAGATgCAXQgBAPgFAcIgUBuQgIApADAWIAFAZQADAQgCAKIgEAYQgCAOAGAIQAGAHAOACQATADAEACQANAGAEAOQAFAOgEAOQgGAVgdAVQgYATgTAAQgSAAgQgNQgOgNgIgSQgbAcgTAKQggAQgZgOIgTgMQgLgIgIgBQgMAAgRAMIgiAZQgUAOgSAFQgSAFgTgDQgTgEgQgLIgNgKQgIgFgGgCQgJgBgSAFIhDAVQgkALgWACIgNABQgYAAgTgKgAhMECQANAFAagIIADgBQgcgBgbgCQAHAFAGACg");
    this.shape.setTransform(2.4258, 9.1675);

    this.instance = new lib.OFF();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_3
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(41,88,161,0.329)").s().p("AhpFXQgXgSgBgTQgIgGgIgDIAAgBIgDgCIgBAAIgWgMQgmgWgVgHIgBAAQgIgDgWgEIgVgCQgjgEgSgQQgOgMgQgTIgNgQIgagfIAAgBIgHgIIgEgEIAAgBIgIAAIgbgCQgkAAgbgDQg0gEgTgOQgEgDAAgEQAAgCACgDQACgEAHgHQAEgFAFgCIAAgBIAIgIIAMgWQAIgOAIgEQADgCADAAQAPAAAHATIAAgBQACAIADAFQACgGAEgIIgBAAIACgHQAAgDACgCIAAgBIACgFQAAAAABgBQAAAAAAgBQABAAAAAAQABAAAAAAIgBgBQAAAAgBgBQAAAAAAgBQAAAAAAAAQAAgBAAAAQAIgZAEg/QAEgygCgaIAAgCQgCgFAAgEQAAgbAZgWIAEgDIADgBIgBAAQAlgeBQgUQBbgYEOgzQAygLA0gDQCXgIB7A8QAJACAJAFIAYAUIAkAeQASAJAnA7QAFA9gNA6QgEAZgHAYQgGATgIAVQgIAXAAAjIACA9IABAAQANABAiAfIAAAAQAoAjgFAPIAAAAIgBgEIgCgBIgBAFQgbAjgSALQgdASgfAIQgdAGgHgGQgDgDgFgLQABAAgCgPQgFgOgBgJIgEAAIACADIACABIgCABIgBAAIgIACIgDABIgaAgIAAABIgBADQgFAGgHAEQgKAHgNABIgCAAQgUABgmgNIgQgGIABAAQgZgIgNABIgCAAQgDAAgDACIgBAAQgOADgPAMIgCAAQgEACgEAFIgBAAIgHAGIgEAAIgBAAIgBADQgVAOgOABQgVACgdgSIgBAAQAAgBAAAAQgBAAAAAAQgBgBAAAAQgBAAgBAAQAAAAAAAAQgBAAAAAAQAAgBAAAAQAAAAAAAAIgFgEIgBgBQgNgJgMgEQgQgJgKAAQgRABgTAMIgCAAIgBAAIgFADIgBABQgJAEgGAEIgBAAIgMAGQgBAAAAAAQAAAAAAAAQAAAAAAABQAAAAAAAAQgcARgRAAQgKAAgGgFg");
    this.shape_1.setTransform(2.6832, 8.0143);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(4));

    // Layer_2
    this.instance_1 = new lib.OFF();
    this.instance_1.setTransform(59.1, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("rgba(0,0,0,0.239)").s().p("ABrFVQgNgDgagRQgXgQgQgCQgNgBgQAGIgbANQgpATgggGQgQgDgggTQgqgagggRQgkgTgbgGIgTgEQgMgCgIgDQgUgHgcgdIgvgxQgWgYgTgFQgLgDgTADIgeAFQgNABgMgFQgNgFgHgKQgIgOAGgTQAGgRAPgKQAVgNAugCQgFgWAIgmQAJgsAAgRIgCgXQgCgOABgJQABgUANgTQAMgRASgNQAPgLAXgIIAogOICDgqQBHgXAqgFQATgCAkAAQAlgBARgCQAVgCAjgJIA4gMQBJgNBPAPQBKAPBFAnQAzAdAZAgQANASAVAvQAIARADANQADAOgCAbQgDAtgFAWQgKAmgDAUQgFAcAEA5QABAVAHAJQAFAFAJAGIAPAJQAOAKAGASQAEASgEASQgIAdgiAbQgkAdghgDQgUgCgOgNQgQgPABgSQgQgBgWASQgbAVgLADQgRAFgcgIQgjgKgMAAQgVAAgvAXQggAPgWAAIgMgBgAGkDJQASACAPAQQALALAKAVQAPgHAMgQQgRgNgPgSQgMgPgCgNQgNAPgWARg");
    this.shape_2.setTransform(2.9467, 9.9964);

    this.timeline.addTween(cjs.Tween.get({}).to({
      state: [{
        t: this.instance_1,
        p: {x: 59.1, y: 0.1}
      }]
    }).to({state: [{t: this.shape_2}, {t: this.instance_1, p: {x: 58.3, y: 1.8}}]}, 1).wait(3));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-66.1, -26.7, 131.39999999999998, 73.7);


  (lib._2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AiEGPQgZgJgMgVIgHgOQgFgHgFgEQgEgDgIgCIgNgCQgOgDgRgJIgegPIhbgjQg2gUgagcQgGgHgQgWQgOgTgKgJQgYgVgogFQgVgCg0ABQgnABgNgRQgJgNAEgSQADgRAMgPQAJgNANgHQAOgHAMAGQALALAHAAQAKABAHgPQAOgcACgoQAAgMgCg9QgFhgAqgnQASgRApgQQCgg+CmgaQApgHBTgKICegTQA6gHAegCQAxgDAnAFQA0AFAvATQAYAJAvAXIAsATQAZALAQAMQAgAXATAiQAUAiAEAmQADAcgGAsQgFAlgQBVQgPBQgFAsQgDAbAHAMQAEAIAKAIIARAMQAdAVARAeQANAWgFAOQgEANgUANQgoAagwASQgYAIgPgBQgKgBgIgGQgJgHgCgJQgBgFACgQQABgNgDgGQgcAJgdgGQgOAhgkAPQgkAOghgPIgPgIQgIgFgHgCQghgMg3AhQgfASgQAGQgcAKgVgIQgHgCgLgHIgSgKQgOgGgSgCIghgBQgiABgRAGQgIADgPAKQgPAJgIADQgMAFgLAAQgMAAgMgFgAh6FBQAHAMAKABQAGAAAGgEIALgJIAEgDIgwgGIAEAJgAIeFNQAygQAqgdQgGgNgNgKQgIgHgRgLQACALgEAMQgFALgIAHQgJAHgMADQgLACgLgEgAB7E9QAKAFAFAAQAGgBAKgGIAFgCIgkAEg");
    this.shape.setTransform(7.0253, 11.0025);
    this.shape._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1).to({_off: false}, 0).to({_off: true}, 1).wait(2));

    // Layer_4
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(41,88,161,0.329)").s().p("Ah/GbQgbgVgBgXQgKgHgJgEIgBgBIgEgCIAAAAIgbgPQgtgZgagKIgBAAQgJgDgbgEQgMgBgNgCQgqgFgVgTQgSgPgTgWIgPgTQgSgYgNgOIAAgBIgIgJIgFgFIgBgBIgJAAIghgDQgqAAghgDQg+gFgXgRQgFgDAAgFQAAgDADgDQACgFAJgJQAFgFAFgDIAAgBIAJgKIAPgaQAJgRALgEQACgDAEAAQASAAAJAXIAAgBQADAJADAGQACgIAEgJIgBAAIADgIQAAgEADgCIAAgBIADgGQAAgBAAAAQABgBAAAAQABgBAAAAQABAAAAAAIgBgBQgBAAAAgBQAAAAAAgBQgBAAABgBQAAAAAAgBQAJgdAEhLQAFg9gCggIAAgBQgDgHAAgFQAAggAfgaQACgDADgBIADgCIgBAAQAsgjBggYQBtgdFFg9QA7gNA+gEQC2gJCTBHQALADAKAGIAeAYIArAjQAVAMAuBHQAHBJgPBFQgGAegIAdIgRAwQgJAcAAAqQAAAXACAyIACAAQAOABAqAlIAAAAQAwArgGASIAAgBQAAgBAAgBQAAAAgBgBQAAgBAAAAQgBAAAAgBIgCgBQAAADgCADQggAqgVANQgjAXglAIQgjAHgIgHQgDgDgHgOQABAAgBgRQgHgRgBgLIgFABIACADIADABQAAAAgBAAQAAAAAAAAQgBAAAAABQAAAAgBAAIgBAAIgJACIgFACIgdAmIgCABIAAADQgHAIgHAFQgNAIgPACIgDAAQgYABgtgQIgTgGIABAAQgfgLgPABIgCAAQgEAAgDACIgBAAQgRAFgTANIgBABQgFADgGAFIAAAAIgJAHQgBAAAAAAQAAABgBAAQAAAAgBAAQgBAAAAAAIgBAAQAAABAAAAQAAAAgBABQAAAAAAAAQgBABAAAAQgZARgRABQgaACgigWIgBAAIgFgCIgCgBIgFgEIgBgCQgQgLgOgFQgUgLgMABQgUABgXAOIgCAAIgBABIgGADIgBACQgMAEgGAFIgCAAIgPAHQAAAAAAAAQAAAAAAAAQgBAAAAABQAAAAAAAAQghAUgVAAQgLAAgIgGg");
    this.shape_1.setTransform(5.8638, 11.3336);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(4));

    // Layer_2
    this.instance = new lib.B_1copy2();
    this.instance.setTransform(69.55, 0.3, 0.3655, 0.3655, 0, 0, 0, 269.3, 83);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off: false}, 0).wait(1));

    // Layer_3
    this.instance_1 = new lib.B_1copy2();
    this.instance_1.setTransform(69.55, 0.3, 0.3655, 0.3655, 0, 0, 0, 269.3, 83);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-75.5, -30.3, 155.7, 86.1);


  (lib._1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("ABbFSQgHgDgMgIQgMgJgHgDQgYgLgfAJQgSAFgkANQgXAHgQgDQgXgEgIgRQgFgOgEgGQgEgHgNgGQgWgKg9gLQg2gKgagSQgJgGgLgKIgSgSIgogkQgWgVgJgUQghAKgSADQgdAEgWgHQgXgGgLgSQgJgPADgTQACgMAIgKQAJgLAMgCQAMgCALAJQAKAJgCAMQAJgCADgLIgBgTQgCgZAKgtQALgxAAgWIgBgcQAAgRADgMQAIghAjgUQAWgMAsgMIBEgSQBfgaAsgJQAhgHBBgLIBAgLQA7gKAcgDQBcgKBdAOQBEALAqAWQAZAOAoAgQAjAbARAVQAVAaAJAiQAKAigFAiQgDASgJAcIgNAqQgCATgGASQgFAdgDApQgCAZAEALQAEAMAMAMIAXAVQAfAagEAZQgCAKgJALIgRAQQgdAXgLAGQgbANgVgJQgMgEgHgLQgHgMACgMIgPgFQgKARgUAKQgSAJgWACQgcACg1gNQgRgFgNAAQgUABgkATQgZAPgQAFQgNADgLAAQgKAAgJgDgAhPETQAPgCAYgIIAVgHQgmgCgigFQAKALACANg");
    this.shape.setTransform(3.316, 9.3478);

    this.instance = new lib.OFFcopy4();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_3
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(41,88,161,0.329)").s().p("AqOFfQgXgSgBgUQgIgGgIgDIgBgBIgDgCIgBAAIgWgNQgngVgVgIIgBAAQgJgDgWgEIgVgCQglgFgRgQQgPgMgRgTIgNgRQgPgUgMgMIAAgBIgHgIIgDgEIgBAAIgHAAIgdgDQgkAAgcgDQg1gEgUgPQgDgCgBgEQAAgDACgCQACgFAIgHQAEgFAEgCIAAgBIAIgIIANgXQAIgOAJgEQACgCADAAQAQAAAHATIAAgBQACAJADAFQACgHAEgIIgBAAIACgHQAAgDACgCIAAgBIADgFQAAgBABAAQAAAAAAgBQABAAAAAAQAAAAABAAIgBgBQgBgBAAAAQAAAAAAgBQAAAAAAgBQAAAAAAgBQAIgZAEhAQADg0gCgbIAAgBQgBgFAAgFQAAgbAagWIAEgEIADgBIgBAAQAlgeBTgVQBdgYEVg0QAzgMA0gDQCbgIB/A9QAIACAKAGIAYAUIAmAeQASAKAmA8QAGA/gOA7QgDAagHAYIgPApQgHAYgBAkIADA+IABAAQAMABAjAgIAAgBQApAlgFAPIAAgBQgBAAAAgBQAAAAAAgBQAAAAAAgBQgBAAAAAAIgCgBIgBAEQgbAkgTALQgdAUgfAHQgeAGgGgGQgDgDgGgLQABAAgBgPQgGgOgBgKIgFABIADADIACAAIgCABIgCAAIgHACIgEACIgaAgIgBABIAAADQgGAGgGAFQgKAGgOACIgCAAQgUABgogOIgPgFIABAAQgagJgOABIgBAAQgEAAgDACIgBAAQgNADgRAMIgBABQgEACgFAEIAAAAIgIAGIgDABIgCAAQAAABAAAAQAAABAAAAQAAAAAAAAQgBABAAAAQgVAOgPABQgVACgdgTIgBAAIgFgCIgBgBIgFgDIgBgBQgOgKgMgEQgRgJgKAAQgRABgUAMIgCAAIgBABIgFADIgBABQgKADgFAFIgCAAIgNAGIAAABQgdARgSAAQgJAAgHgFgAR8ApIAAgBIAAABg");
    this.shape_1.setTransform(58.25, 9.6733);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(4));

    // Layer_2
    this.instance_1 = new lib.OFFcopy4();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-65.3, -25.9, 238.39999999999998, 72.9);


  (lib.Symbol6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.OFFcopy9();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-65.3, -25, 130.6, 72);


  (lib.Symbol5_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.OFFcopy8();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-65.3, -25, 130.6, 72);


  (lib.Symbol4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.OFFcopy7();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-65.3, -25, 130.6, 72);


  (lib.Symbol3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.OFFcopy6();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-65.3, -25, 130.6, 72);


  (lib.Symbol1_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.OFFcopy3();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-65.3, -25, 130.6, 72);


  (lib.N3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.B_1();
    this.instance.setTransform(128.7, 10.1, 1, 1, 0, 0, 0, 268.7, 82.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.N3, new cjs.Rectangle(-292.4, -54.5, 425.79999999999995, 234.8), null);


  (lib._3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AChGMQgSgFgSgRIgeggQgMgPgLgEQgRgGgXALQgOAHgZAUQgZATgQAHQgsATgsgUQgMgFgQgLIgbgTQgcgSgogNIg3gOQgigJgUgJQgmgSgcgfIgjgpQgVgXgVgIQgTgGgkACIg7ACIgVAAQgLgBgHgFQgRgMACgYQAAgQAOgUQAQgXAUgHQAMgEAMAEQAMADAHAJQAkhQgNh9QgEgpAIgSQAHgQASgNQAMgIAYgLIAkgQQAVgHARgBIAIgBQCLgtE1hMQHOgUBmCOQBaB7g4BzQADAJgBAMQAAAJgEARIgnDEQArAOATAPQAPALAHAPQAJAQgCAQQgDAcgdATQgKAIgPAHIgbALIgTAGQgKADgIgBQgLgCgIgHQgIgHgEgLQgIgTAJgXQggAJgbAUIgVARQgMAKgLADQgZAHgjgSIgcgPQgRgIgNgCQgMAHgWARQgWASgMAHQgfAUgcAAQgKAAgKgDgAibEOQAQAKAJAJQARAPAIADQASAGAXgPQADgCAAgCQAAgDgGgDIgGgHQgEgEgDgBQgEgCgFABQgWACgUgFIgMgDIgFAAQgEAAgDABg");
    this.shape.setTransform(-3.8131, 16.8424);

    this.instance = new lib.N3();
    this.instance.setTransform(69.35, 0.05, 0.3654, 0.3654, 0, 0, 0, 128.8, 10.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_3
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(41,88,161,0.329)").s().p("Ah+GaQgbgVgBgXQgKgHgJgEIgBgBIgEgCIAAAAQgOgJgNgGQgtgZgZgJIgCAAQgJgDgagFIgZgDQgqgFgVgTQgSgOgTgXIgPgTQgTgYgMgNIAAgCIgJgJIgEgFIgBAAIgJAAIghgEQgqAAghgDQg+gFgXgRQgEgDAAgEQAAgDACgDQACgFAJgJQAFgFAFgDIAAgCIAKgJIAOgaQAKgRAKgEQADgDAEAAQASAAAIAXIAAgCQADAKADAGQACgIAFgJIgBAAIACgIQAAgEADgCIAAgBIADgGQAAgBAAgBQABAAAAAAQAAgBABAAQAAAAABAAIgBgBQgBAAAAgBQAAAAgBgBQAAAAAAgBQAAAAABgBQAJgdAEhLQAFg9gCgfIAAgCQgDgGAAgFQAAggAfgaIAFgEIADgCIgBAAQAsgjBggYQBtgcFDg9QA7gOA+gDQC1gKCTBHQALADAKAHIAdAXIArAkQAVALAvBGQAGBKgPBFQgFAegJAcQgHAXgKAZQgJAcAAAqIACBIIACAAQAPABApAmIAAgBQAwArgGASIAAgBQAAgBAAgBQgBAAAAgBQAAAAAAgBQgBAAAAAAIgCgBIgBAFQghAqgVANQgjAWgkAJQgjAHgIgHQgEgDgGgOQABAAgCgRQgGgRgBgLIgFABIACADIADABIgDABIgBAAIgKADIgEABIgeAmIgBABIAAADQgHAIgHAFQgNAIgPACIgDAAQgYABgtgQIgTgHIABAAQgegKgQABIgBAAQgEAAgEACIgBAAQgQAFgTANIgCABQgFADgFAFIgBAAIgIAHIgFABIgBAAQAAAAAAABQAAAAAAABQAAAAgBAAQAAAAAAABQgZARgRABQgaACgigWIgBAAIgFgCIgCgBIgFgEIgBgCQgQgLgOgFQgUgLgLABQgUABgXAOIgDAAIgBABIgGADIgBACQgLAEgHAFIgCAAIgOAHQgBAAAAAAQAAAAAAAAQAAAAAAAAQAAABAAAAQgiAUgVAAQgLAAgHgGg");
    this.shape_1.setTransform(-3.4612, 17.6972);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(4));

    // Layer_2
    this.instance_1 = new lib.N3();
    this.instance_1.setTransform(69.35, 0.05, 0.3654, 0.3654, 0, 0, 0, 128.8, 10.1);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-84.5, -23.8, 155.6, 86);


  (lib.All_M = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_249 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(249).call(this.frame_249).wait(1));

    // Layer_3
    this.instance = new lib.Symbol16();
    this.instance.setTransform(-975.75, -204.25);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({x: 919.65, y: -429.35}, 149).wait(35).to({
      regX: -0.1,
      skewX: -17.4872,
      skewY: 162.5128,
      x: 975.9,
      y: -120.9
    }, 0).to({regX: 0, skewX: 0, skewY: 180, x: -606.2, y: 729.7}, 65).wait(1));

    // Layer_16
    this.instance_1 = new lib.Symbol6();
    this.instance_1.setTransform(246.85, -192);
    this.instance_1._off = true;
    this.instance_1.name = '6';
    this.instance_1.visible = false;
    new cjs.ButtonHelper(this.instance_1, 0, 1, 2, false, new lib.Symbol6(), 3);
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(249).to({_off: false}, 0).wait(1));

    // Layer_15
    this.instance_2 = new lib.Symbol5_1();
    this.instance_2.setTransform(-228.1, -85);
    this.instance_2._off = true;
    this.instance_2.name = '5';
    this.instance_2.visible = false;
    new cjs.ButtonHelper(this.instance_2, 0, 1, 2, false, new lib.Symbol5_1(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(246).to({_off: false}, 0).wait(4));

    // Layer_14
    this.instance_3 = new lib.Symbol4();
    this.instance_3.setTransform(-657.1, -88.4);
    this.instance_3._off = true;
    this.instance_3.name = '4';
    this.instance_3.visible = false;
    new cjs.ButtonHelper(this.instance_3, 0, 1, 2, false, new lib.Symbol4(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(240).to({_off: false}, 0).wait(10));

    // Layer_13
    this.instance_4 = new lib.Symbol3();
    this.instance_4.setTransform(-484, 244, 1.1897, 1.1897, 0, 0, 0, -0.1, 0.1);
    this.instance_4._off = true;
    this.instance_4.name = '3';
    this.instance_4.visible = false;
    new cjs.ButtonHelper(this.instance_4, 0, 1, 2, false, new lib.Symbol3(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(232).to({_off: false}, 0).wait(18));

    // Layer_12
    this.instance_5 = new lib.aa1();
    this.instance_5.setTransform(133.2, 189.1, 1.2007, 1.2007, 0, 0, 0, 0.1, 0.1);
    this.instance_5._off = true;
    this.instance_5.name = '2';
    this.instance_5.visible = false;
    new cjs.ButtonHelper(this.instance_5, 0, 1, 2, false, new lib.aa1(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(224).to({_off: false}, 0).wait(26));

    // Layer_2
    this.instance_6 = new lib.Symbol1_1();
    this.instance_6.setTransform(540.3, 327.4, 1.2386, 1.2386, 0, 0, 0, 0.1, 0.1);
    this.instance_6._off = true;
    this.instance_6.name = '1';
    this.instance_6.visible = false;
    new cjs.ButtonHelper(this.instance_6, 0, 1, 2, false, new lib.Symbol1_1(), 3);
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(213).to({_off: false}, 0).wait(37));

    // Layer_5
    this.instance_7 = new lib._6();
    this.instance_7.setTransform(244.45, -186.3);
    this.instance_7._off = true;
    new cjs.ButtonHelper(this.instance_7, 0, 1, 2, false, new lib._6(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(132).to({_off: false}, 0).wait(36));

    // Layer_6
    this.instance_8 = new lib._5();
    this.instance_8.setTransform(-228.7, -87.9);
    this.instance_8._off = true;
    new cjs.ButtonHelper(this.instance_8, 0, 1, 2, false, new lib._5(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(107).to({_off: false}, 0).wait(36));

    // Layer_7
    this.instance_9 = new lib._4();
    this.instance_9.setTransform(-656.85, -87.35);
    this.instance_9._off = true;
    new cjs.ButtonHelper(this.instance_9, 0, 1, 2, false, new lib._4(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(82).to({_off: false}, 0).wait(36));

    // Layer_8
    this.instance_10 = new lib._3();
    this.instance_10.setTransform(-488.55, 249.05, 1, 1, 0, 0, 0, -5.2, 5.9);
    this.instance_10._off = true;
    new cjs.ButtonHelper(this.instance_10, 0, 1, 2, false, new lib._3(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(55).to({_off: false}, 0).wait(36));

    // Layer_9
    this.instance_11 = new lib._2();
    this.instance_11.setTransform(130.55, 189.4);
    this.instance_11._off = true;
    new cjs.ButtonHelper(this.instance_11, 0, 1, 2, false, new lib._2(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(35).to({_off: false}, 0).wait(36));

    // Layer_10
    this.instance_12 = new lib._1();
    this.instance_12.setTransform(540.05, 327.15, 1.2358, 1.2358);
    this.instance_12._off = true;
    new cjs.ButtonHelper(this.instance_12, 0, 1, 2, false, new lib._1(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(13).to({_off: false}, 0).wait(36));

    // Layer_11
    this.instance_13 = new lib.AllSpace();
    this.instance_13.setTransform(-47.55, -4.1, 0.6743, 0.6426, 0, 0, 0, -62.8, 3.9);

    this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(250));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1062.6, -664.7, 2150.3, 1490.9);


// stage content:
  (lib.Maotens = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.All_Spacecopy();
    this.instance.setTransform(675.3, 585.85, 0.7944, 0.7654, 0, 0, 0, -57.5, 20.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_1
    this.instance_1 = new lib.All_M();
    this.instance_1.setTransform(628.55, 584.5, 0.7326, 0.7309, 0, 0, 0, -109.6, 4.1);
    this.instance_1.name = 'stages';
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#6B0F0F").ss(0.5, 1, 1).p("EgoFgHtQBLoILEl6QMYmoRfAAQN2AAKpEJQCeA+CSBMQAUAKAUALQMYGnAAJXQAAJWsYGoQhCAjhDAgEgoFgHtQCpjRETizQLGnPPsAAQPrAALGHPQLGHPAAKNQAAEBhtDkQiqFgmvEZQrGHPvrAAQvsAArGnPQrGnPAAqPQAAmPEKlJg");
    this.shape.setTransform(738.775, 542.275);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#8C9193").s().p("EghJAVJQrGnPAAqPQAAmPEKlJQCpjRETizQLGnPPsAAQPrAALGHPQLGHPAAKNQAAEBhtDkQBtjkAAkBQAAqNrGnPQrGnPvrAAQvsAArGHPQkTCzipDRQBLoILEl6QMYmoRfAAQN2AAKpEJQCeA+CSBMIAoAVQMYGnAAJXQAAJWsYGoQhCAjhDAgQiqFgmvEZQrGHPvrAAQvsAArGnPg");
    this.shape_1.setTransform(738.775, 542.275);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}, {t: this.instance_1}]}).wait(1));

    // stageBackground
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f().s("rgba(0,0,0,0)").ss(1, 1, 1, 3, true).p("EhyDhaEMDkHAAAMAAAC0JMjkHAAAg");
    this.shape_2.setTransform(720, 566.5);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#FFFFFF").s().p("EhyDBaFMAAAi0JMDkHAAAMAAAC0Jg");
    this.shape_3.setTransform(720, 566.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_3}, {t: this.shape_2}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new lib.AnMovieClip();
  p.nominalBounds = new cjs.Rectangle(650.5, 555.5, 800.5, 588.5);
// library properties:
  lib.properties = {
    id: 'E57964F4E1D8D746ADE74AF9EC3FBC2C',
    width: 1440,
    height: 1133,
    fps: 24,
    color: "#FFFFFF",
    opacity: 1.00,
    manifest: [
      {src: "assets/images/shared/1.png", id: "CachedBmp_47"},
      {src: "assets/images/mountains/CachedBmp_57.png", id: "CachedBmp_57"},
      {src: "assets/images/mountains/CachedBmp_56.png", id: "CachedBmp_56"},
      {src: "assets/images/mountains/CachedBmp_1.png", id: "CachedBmp_1"},
      {src: "assets/images/shared/2.png", id: "Maotens_atlas_1"},
      {src: "assets/images/shared/2.png", id: "Maotens_atlas_2"},
      {src: "assets/images/shared/2.png", id: "Maotens_atlas_3"},
      {src: "assets/images/mountains/Maotens_atlas_4.png", id: "Maotens_atlas_4"},
      {src: "assets/images/shared/3.png", id: "Maotens_atlas_5"},
      {src: "assets/images/shared/3.png", id: "Maotens_atlas_6"},
      {src: "assets/images/mountains/Maotens_atlas_7.png", id: "Maotens_atlas_7"},
      {src: "assets/images/mountains/Maotens_atlas_8.png", id: "Maotens_atlas_8"}
    ],
    preloads: []
  };


// bootstrap callback support:

  (lib.Stage = function (canvas) {
    createjs.Stage.call(this, canvas);
  }).prototype = p = new createjs.Stage();

  p.setAutoPlay = function (autoPlay) {
    this.tickEnabled = autoPlay;
  }
  p.play = function () {
    this.tickEnabled = true;
    this.getChildAt(0).gotoAndPlay(this.getTimelinePosition())
  }
  p.stop = function (ms) {
    if (ms) this.seek(ms);
    this.tickEnabled = false;
  }
  p.seek = function (ms) {
    this.tickEnabled = true;
    this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000);
  }
  p.getDuration = function () {
    return this.getChildAt(0).totalFrames / lib.properties.fps * 1000;
  }

  p.getTimelinePosition = function () {
    return this.getChildAt(0).currentFrame / lib.properties.fps * 1000;
  }

  an.bootcompsLoaded = an.bootcompsLoaded || [];
  if (!an.bootstrapListeners) {
    an.bootstrapListeners = [];
  }

  an.bootstrapCallback = function (fnCallback) {
    an.bootstrapListeners.push(fnCallback);
    if (an.bootcompsLoaded.length > 0) {
      for (var i = 0; i < an.bootcompsLoaded.length; ++i) {
        fnCallback(an.bootcompsLoaded[i]);
      }
    }
  };

  an.compositions = an.compositions || {};
  an.compositions['E57964F4E1D8D746ADE74AF9EC3FBC2C'] = {
    getStage: function () {
      return exportRoot.stage;
    },
    getLibrary: function () {
      return lib;
    },
    getSpriteSheet: function () {
      return ss;
    },
    getImages: function () {
      return img;
    }
  };

  an.compositionLoaded = function (id) {
    an.bootcompsLoaded.push(id);
    for (var j = 0; j < an.bootstrapListeners.length; j++) {
      an.bootstrapListeners[j](id);
    }
  }

  an.getComposition = function (id) {
    return an.compositions[id];
  }


  an.makeResponsive = function (isResp, respDim, isScale, scaleType, domContainers) {
    var lastW, lastH, lastS = 1;
    window.addEventListener('resize', resizeCanvas);
    resizeCanvas();

    function resizeCanvas() {
      var w = lib.properties.width, h = lib.properties.height;
      var iw = window.innerWidth, ih = window.innerHeight;
      var pRatio = window.devicePixelRatio || 1, xRatio = iw / w, yRatio = ih / h, sRatio = 1;
      if (isResp) {
        if ((respDim == 'width' && lastW == iw) || (respDim == 'height' && lastH == ih)) {
          sRatio = lastS;
        } else if (!isScale) {
          if (iw < w || ih < h)
            sRatio = Math.min(xRatio, yRatio);
        } else if (scaleType == 1) {
          sRatio = Math.min(xRatio, yRatio);
        } else if (scaleType == 2) {
          sRatio = Math.max(xRatio, yRatio);
        }
      }
      domContainers[0].width = w * pRatio * sRatio;
      domContainers[0].height = h * pRatio * sRatio;
      domContainers.forEach(function (container) {
        container.style.width = w * sRatio + 'px';
        container.style.height = h * sRatio + 'px';
      });
      stage.scaleX = pRatio * sRatio;
      stage.scaleY = pRatio * sRatio;
      lastW = iw;
      lastH = ih;
      lastS = sRatio;
      stage.tickOnUpdate = false;
      stage.update();
      stage.tickOnUpdate = true;
    }
  }
  an.handleSoundStreamOnTick = function (event) {
    if (!event.paused) {
      var stageChild = stage.getChildAt(0);
      if (!stageChild.paused) {
        stageChild.syncStreamSounds();
      }
    }
  }


})(createjs = createjs || {}, AdobeAn = AdobeAn || {});
var createjs, AdobeAn, instance;
var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;

function initMountains() {
  return new Promise((resolve, reject) => {
    canvas = document.getElementById("canvas");
    anim_container = document.getElementById("animation_container");
    dom_overlay_container = document.getElementById("dom_overlay_container");
    var comp = AdobeAn.getComposition("E57964F4E1D8D746ADE74AF9EC3FBC2C");
    var lib = comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function (evt) {
      handleFileLoadMountains(evt, comp)
    });
    loader.addEventListener("complete", function (evt) {
      handleCompleteMountains(evt, comp).then(data => {
        resolve(true)
      });
    });
    var lib = comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  })
}

function handleFileLoadMountains(evt, comp) {
  var images = comp.getImages();
  if (evt && (evt.item.type == "image")) {
    images[evt.item.id] = evt.result;
  }
}

function handleCompleteMountains(evt, comp) {
  return new Promise((resolve, reject) => {
    var lib = comp.getLibrary();
    var ss = comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for (i = 0; i < ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet({
        "images": [queue.getResult(ssMetadata[i].name)],
        "frames": ssMetadata[i].frames
      })
    }
    var preloaderDiv = document.getElementById("_preload_div_");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.Maotens();
    stage = new lib.Stage(canvas);
    stage.enableMouseOver();
    createjs.Ticker.removeAllEventListeners(); // Note the function name
    //Registers the "tick" event listener.
    fnStartAnimation = function () {
      stage.addChild(exportRoot);
      createjs.Ticker.framerate = lib.properties.fps;
      createjs.Ticker.addEventListener("tick", stage);
    }
    //Code to support hidpi screens and responsive scaling.
    AdobeAn.makeResponsive(true, 'both', false, 1, [canvas, preloaderDiv, anim_container, dom_overlay_container]);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
    MountainClickEvents();
    MountainHoverEvents();
    resolve(true);
  })
}

function MountainClickEvents() {
  //stage 1
  exportRoot.instance_1.instance_6.on("click", function (evt) {
    playAudioOnClick();
    instance = 1;
  });
  //stage 2
  exportRoot.instance_1.instance_5.on("click", function (evt) {
    playAudioOnClick();
    instance = 2;
  });
  //stage 3
  exportRoot.instance_1.instance_4.on("click", function (evt) {
    playAudioOnClick();
    instance = 3;
  });
  //stage 4
  exportRoot.instance_1.instance_3.on("click", function (evt) {
    playAudioOnClick();
    instance = 4;
  });
  //stage 5
  exportRoot.instance_1.instance_2.on("click", function (evt) {
    playAudioOnClick();
    instance = 5;
  });
  //stage 6
  exportRoot.instance_1.instance_1.on("click", function (evt) {
    playAudioOnClick();
    instance = 6;
  });
  //back
  exportRoot.instance.instance.on("click", function (evt) {
    playAudioOnClick();
    instance = 7;
  });
}

function MountainHoverEvents() {
  //stage 1
  exportRoot.instance_1.instance_6.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 2
  exportRoot.instance_1.instance_5.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 3
  exportRoot.instance_1.instance_4.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 4
  exportRoot.instance_1.instance_3.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 5
  exportRoot.instance_1.instance_2.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 6
  exportRoot.instance_1.instance_1.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //back
  exportRoot.instance.instance.on("mouseover", function (evt) {
    playAudioOnHover();
  });
}
