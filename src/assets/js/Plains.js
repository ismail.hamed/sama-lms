(function (cjs, an) {

  var p; // shortcut to reference prototypes
  var lib = {};
  var ss = {};
  var img = {};
  lib.ssMetadata = [
    {name: "Plains_atlas_1", frames: [[0, 0, 1717, 927]]},
    {name: "Plains_atlas_2", frames: [[0, 0, 967, 1550], [969, 0, 967, 1550]]},
    {name: "Plains_atlas_3", frames: [[0, 0, 967, 1550], [969, 0, 967, 1550]]},
    {name: "Plains_atlas_4", frames: [[0, 0, 967, 1550], [969, 0, 967, 1550]]},
    {name: "Plains_atlas_5", frames: [[0, 0, 1482, 759], [0, 761, 1683, 664]]},
    {name: "Plains_atlas_6", frames: [[0, 0, 1683, 664], [0, 666, 1683, 664]]},
    {name: "Plains_atlas_7", frames: [[0, 0, 1459, 748], [0, 1393, 1623, 640], [0, 750, 1623, 641]]},
    {name: "Plains_atlas_8", frames: [[0, 0, 1482, 624], [0, 626, 1682, 546], [0, 1174, 1682, 546]]},
    {name: "Plains_atlas_9", frames: [[0, 0, 1682, 546], [0, 548, 1682, 546], [0, 1096, 1682, 546]]},
    {name: "Plains_atlas_10", frames: [[0, 1096, 1459, 614], [0, 0, 1682, 546], [0, 548, 1682, 546]]},
    {
      name: "Plains_atlas_11",
      frames: [[0, 529, 1622, 526], [0, 0, 1622, 527], [0, 1057, 859, 899], [861, 1057, 859, 899]]
    },
    {name: "Plains_atlas_12", frames: [[0, 0, 859, 899], [0, 901, 859, 899], [861, 0, 859, 899], [861, 901, 859, 899]]},
    {name: "Plains_atlas_13", frames: [[0, 0, 859, 899], [0, 901, 859, 899], [861, 0, 859, 899], [861, 901, 859, 899]]},
    {
      name: "Plains_atlas_14",
      frames: [[0, 1446, 1401, 447], [0, 901, 1402, 543], [0, 0, 859, 899], [861, 0, 859, 899]]
    },
    {
      name: "Plains_atlas_15",
      frames: [[0, 0, 792, 539], [1330, 1284, 629, 386], [1459, 0, 412, 593], [794, 0, 663, 640], [0, 541, 663, 640], [0, 1183, 663, 640], [665, 642, 663, 640], [1330, 642, 663, 640], [665, 1284, 663, 640]]
    },
    {
      name: "Plains_atlas_16",
      frames: [[2017, 0, 13, 15], [721, 0, 429, 371], [2032, 0, 3, 3], [0, 0, 307, 634], [734, 1548, 166, 44], [1515, 1472, 300, 216], [1515, 1164, 346, 306], [0, 636, 216, 132], [395, 1089, 228, 45], [309, 0, 410, 415], [1863, 1164, 159, 208], [1944, 0, 71, 194], [1185, 1164, 328, 438], [1817, 1521, 200, 47], [1890, 780, 144, 145], [1890, 927, 144, 145], [1152, 0, 394, 388], [705, 763, 393, 382], [0, 1191, 323, 327], [1863, 1374, 144, 145], [490, 1531, 242, 243], [0, 1520, 243, 243], [245, 1531, 243, 243], [395, 807, 279, 280], [1548, 0, 394, 388], [721, 373, 394, 388], [1117, 390, 394, 388], [1513, 390, 394, 388], [309, 417, 394, 388], [1100, 780, 393, 382], [1495, 780, 393, 382], [0, 807, 393, 382], [395, 1147, 393, 382], [790, 1164, 393, 382], [1944, 196, 83, 77]]
    }
  ];


  (lib.AnMovieClip = function () {
    this.actionFrames = [];
    this.gotoAndPlay = function (positionOrLabel) {
      cjs.MovieClip.prototype.gotoAndPlay.call(this, positionOrLabel);
    }
    this.play = function () {
      cjs.MovieClip.prototype.play.call(this);
    }
    this.gotoAndStop = function (positionOrLabel) {
      cjs.MovieClip.prototype.gotoAndStop.call(this, positionOrLabel);
    }
    this.stop = function () {
      cjs.MovieClip.prototype.stop.call(this);
    }
  }).prototype = p = new cjs.MovieClip();
// symbols:


  (lib.CachedBmp_225 = function () {
    this.initialize(img.CachedBmp_225);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 2707, 1897);


  (lib.CachedBmp_224 = function () {
    this.initialize(img.CachedBmp_224);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 2131, 1257);


  (lib.CachedBmp_249 = function () {
    this.initialize(ss["Plains_atlas_1"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_248 = function () {
    this.initialize(ss["Plains_atlas_15"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_221 = function () {
    this.initialize(ss["Plains_atlas_15"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_220 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_219 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_218 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_217 = function () {
    this.initialize(img.CachedBmp_217);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 2612, 1630);


  (lib.CachedBmp_216 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_215 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(4);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_214 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(5);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_247 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(6);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_246 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(7);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_211 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(8);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_210 = function () {
    this.initialize(ss["Plains_atlas_15"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_245 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(9);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_208 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(10);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_207 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(11);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_206 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(12);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_205 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(13);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_244 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(14);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_243 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(15);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_202 = function () {
    this.initialize(ss["Plains_atlas_15"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_201 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(16);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_200 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(17);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_242 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(18);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_198 = function () {
    this.initialize(img.CachedBmp_198);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 3670, 2839);


  (lib.CachedBmp_241 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(19);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_240 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(20);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_195 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(21);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_239 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(22);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_238 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(23);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_237 = function () {
    this.initialize(ss["Plains_atlas_8"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_191 = function () {
    this.initialize(ss["Plains_atlas_5"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_236 = function () {
    this.initialize(ss["Plains_atlas_10"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_189 = function () {
    this.initialize(ss["Plains_atlas_7"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_188 = function () {
    this.initialize(ss["Plains_atlas_2"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_187 = function () {
    this.initialize(ss["Plains_atlas_2"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_186 = function () {
    this.initialize(ss["Plains_atlas_3"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_185 = function () {
    this.initialize(ss["Plains_atlas_3"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_184 = function () {
    this.initialize(ss["Plains_atlas_4"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_183 = function () {
    this.initialize(ss["Plains_atlas_4"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_235 = function () {
    this.initialize(ss["Plains_atlas_8"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_234 = function () {
    this.initialize(ss["Plains_atlas_8"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_233 = function () {
    this.initialize(ss["Plains_atlas_9"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_232 = function () {
    this.initialize(ss["Plains_atlas_9"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_231 = function () {
    this.initialize(ss["Plains_atlas_9"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_173 = function () {
    this.initialize(ss["Plains_atlas_5"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_230 = function () {
    this.initialize(ss["Plains_atlas_10"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_181 = function () {
    this.initialize(ss["Plains_atlas_6"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_229 = function () {
    this.initialize(ss["Plains_atlas_11"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_169 = function () {
    this.initialize(ss["Plains_atlas_7"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_228 = function () {
    this.initialize(ss["Plains_atlas_11"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_167 = function () {
    this.initialize(ss["Plains_atlas_7"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_227 = function () {
    this.initialize(ss["Plains_atlas_10"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_165 = function () {
    this.initialize(ss["Plains_atlas_6"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_226 = function () {
    this.initialize(ss["Plains_atlas_14"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_163 = function () {
    this.initialize(ss["Plains_atlas_14"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_162 = function () {
    this.initialize(ss["Plains_atlas_11"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_161 = function () {
    this.initialize(ss["Plains_atlas_11"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_160 = function () {
    this.initialize(ss["Plains_atlas_12"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_159 = function () {
    this.initialize(ss["Plains_atlas_12"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_158 = function () {
    this.initialize(ss["Plains_atlas_12"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_157 = function () {
    this.initialize(ss["Plains_atlas_12"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_156 = function () {
    this.initialize(ss["Plains_atlas_13"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_155 = function () {
    this.initialize(ss["Plains_atlas_13"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_154 = function () {
    this.initialize(ss["Plains_atlas_13"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_153 = function () {
    this.initialize(ss["Plains_atlas_13"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_152 = function () {
    this.initialize(ss["Plains_atlas_14"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_151 = function () {
    this.initialize(ss["Plains_atlas_14"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_150 = function () {
    this.initialize(ss["Plains_atlas_15"]);
    this.gotoAndStop(4);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_149 = function () {
    this.initialize(ss["Plains_atlas_15"]);
    this.gotoAndStop(5);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_148 = function () {
    this.initialize(ss["Plains_atlas_15"]);
    this.gotoAndStop(6);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_147 = function () {
    this.initialize(ss["Plains_atlas_15"]);
    this.gotoAndStop(7);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_146 = function () {
    this.initialize(ss["Plains_atlas_15"]);
    this.gotoAndStop(8);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_145 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(24);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_144 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(25);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_143 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(26);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_142 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(27);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_141 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(28);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_140 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(29);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_139 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(30);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_138 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(31);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_137 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(32);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_136 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(33);
  }).prototype = p = new cjs.Sprite();


  (lib.Blend_0 = function () {
    this.initialize(ss["Plains_atlas_16"]);
    this.gotoAndStop(34);
  }).prototype = p = new cjs.Sprite();

// helper functions:

  function mc_symbol_clone() {
    var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop, this.reversed));
    clone.gotoAndStop(this.currentFrame);
    clone.paused = this.paused;
    clone.framerate = this.framerate;
    return clone;
  }

  function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
    var prototype = cjs.extend(symbol, cjs.MovieClip);
    prototype.clone = mc_symbol_clone;
    prototype.nominalBounds = nominalBounds;
    prototype.frameBounds = frameBounds;
    return prototype;
  }


  (lib.Tween6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0004, -0.0235, 1.2751, 1.0272, 0, 0, 180);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-177.1, -59.6, 354.29999999999995, 119.2);


  (lib.Tween5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0004, -0.0235, 1.2751, 1.0272, 0, 0, 180);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-177.1, -59.6, 354.29999999999995, 119.2);


  (lib.Tween4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0128, -0.0036);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-138.9, -58, 277.9, 116.1);


  (lib.Tween3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0128, -0.0036);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-138.9, -58, 277.9, 116.1);


  (lib.Tween2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhhMbQg6gRgLgyIAAACQgWAPgYgOQgVgOgMgdIgCABQg3ARg1ggQg5giAVg7QgiAMgKgiQgJgcALgcQgeAdgfgUQgcgTgLgpQgMgqASgZQAWgeA4AOQADADAAACQACgMAIgJQgQgBgRgJQgSgKgHgOQACgEAAgIQAAgHABgEQgYAHgagKIAAADQAOAUgYAYQghAhg3gTQgygSgWgmQgZgqALg0QAMg4AvgPQgugBgcghIAAADQgGAhg3AJQg3AIALgwQgKADgPAAIAFAFQgLA/g3AOQgzANg9ggQg/ggggg5QgkhBAVhFIAAAAIgBABQgVgEgJgUQgJgVASgPQgcgJAAgYQABgVAUgUIgbgMQgFAFgVAQQAeASgQAbQgOAZgfAHIAAABQAMAjglASQggAPgggIQgkgJgbguQgbgvATgaIgDgDQgrAIgGgtQgHgsAjgVQAYgQAeAJQAeAJAFAbQACgVAUgNQASgMAZAAQAZAAARAMQATAOAAAXQARgRAZALIAAgCQgOgPAMgRQALgQAUgCQgqgJgEgfQgEgcAdgdQAxgvBFABQBLABAYA/QARgaAnAGQAmAHAQAbQAWAoggAYQgeAWgmgNQADATgPAQQgOAOgSADQAKAWgSAUIAAAQQATAEAMAKQAOALACARQAdgkBJABQBLABAWArQAQgSASgEIAAgDQhWgNAFhbQACgmATgkQAUglAdgVQAygiBKgEQBMgEA4AgIABgDQgBgRAOgLQANgKASAAQASABALAKQAMAMgEAUQAagEAQAMQgDgPAOgJQAMgIAVgBIABgBQgSgTANgSQANgSAXANQgQgXAbgbQAXgXAdgIQAegJAlAIQAjAHAaAUQAgAWANAtQANAsgMApIAEAAQAXACAQALQATAMABAWIAAgCQATgHAWAGQAVAGARAOQgpguA2g9QAyg5A5gCQg0gIgRguQgRgwAqglQAUgQAggFQgSgPAMgQQAMgQAXAIQgbgWAegJQAdgJAXAOQgggiApgjQAjgfAqgFQAzgEAoAjQArAngYAwIgCAAQAgAFAQAVQAOATgDAZQgEAYgTAQQgVASgfgCQAMAKAAANQAAAOgMAKQAWgFAPADQAVAEgGAVQAZgHAPAKQAOAJABATQADAvg0AOQAUAGAFAUQAFARgJAUQARgOAZgCQAYgCAUAKIACACQAJgRAHgGQgZgDgKgUQgJgRAGgVQAGgVASgLQAUgMAaAIIACAAQhWgdAhhWQANgjAdgeQAegeAfgMQBOgeBDAzQBEA0gWBRQAwgCABAnQABAlgsANQAqgFAnAfQASgoBHAGQgUgIAOgRQANgRATAAQgBgVANgKQALgIAUABQgmhFAvgbQApgXBKASQBKATAfAqQAkAxg2A0QAiAEAMAjQAMAigdAWQAdALAXAgQAaAjgHAdQgGAagnAXQgkAWgdAAQAdAfgbAwQgZAsgoAQIgVAPQAhAwgqAtQgqAtgzgaQAbAcgJAfQgHAagfARQgdAQgegFQgggGgMgfQgSAOgbAKIgEgDQAPAwgcAoQgeArg8gIIhPAaQgWAAgSgHQgXgJgFgSQgWA1hegMQhcgLgZgwIgDAAQgPAvguATQgdAMhAAGQAVBThNAjQggAPglgDQgngEgegWQghArgrgmQgrgmATgsIgCgBQgXALgWgJQAGAOgFAJQgGAIgQgCQAOAig1AWQgrATgcgGQAcAIANAgQAOAkgZAXQAcACgBAbQAAAcgeAGIgDAAQAAAKgHANQA/gFAZAwQAaAyhEAhQgaAOgeAAQgVAAgXgHgA3vk6QgEgOAIgJQAGgHAMgBQAZgDAGANQAFAMgLAPQgKAQgNAAIgCABQgOAAgIgXgAtKmMQgWgNADgZQADgZAdAGQAqAKgLAhQgFANgLAEQgFADgGAAQgIAAgJgGg");
    this.shape.setTransform(0.028, 0.0029);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-152.1, -80.2, 304.29999999999995, 160.4);


  (lib.Tween1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhhMbQg6gRgLgyIAAACQgWAPgYgOQgVgOgMgdIgCABQg3ARg1ggQg5giAVg7QgiAMgKgiQgJgcALgcQgeAdgfgUQgcgTgLgpQgMgqASgZQAWgeA4AOQADADAAACQACgMAIgJQgQgBgRgJQgSgKgHgOQACgEAAgIQAAgHABgEQgYAHgagKIAAADQAOAUgYAYQghAhg3gTQgygSgWgmQgZgqALg0QAMg4AvgPQgugBgcghIAAADQgGAhg3AJQg3AIALgwQgKADgPAAIAFAFQgLA/g3AOQgzANg9ggQg/ggggg5QgkhBAVhFIAAAAIgBABQgVgEgJgUQgJgVASgPQgcgJAAgYQABgVAUgUIgbgMQgFAFgVAQQAeASgQAbQgOAZgfAHIAAABQAMAjglASQggAPgggIQgkgJgbguQgbgvATgaIgDgDQgrAIgGgtQgHgsAjgVQAYgQAeAJQAeAJAFAbQACgVAUgNQASgMAZAAQAZAAARAMQATAOAAAXQARgRAZALIAAgCQgOgPAMgRQALgQAUgCQgqgJgEgfQgEgcAdgdQAxgvBFABQBLABAYA/QARgaAnAGQAmAHAQAbQAWAoggAYQgeAWgmgNQADATgPAQQgOAOgSADQAKAWgSAUIAAAQQATAEAMAKQAOALACARQAdgkBJABQBLABAWArQAQgSASgEIAAgDQhWgNAFhbQACgmATgkQAUglAdgVQAygiBKgEQBMgEA4AgIABgDQgBgRAOgLQANgKASAAQASABALAKQAMAMgEAUQAagEAQAMQgDgPAOgJQAMgIAVgBIABgBQgSgTANgSQANgSAXANQgQgXAbgbQAXgXAdgIQAegJAlAIQAjAHAaAUQAgAWANAtQANAsgMApIAEAAQAXACAQALQATAMABAWIAAgCQATgHAWAGQAVAGARAOQgpguA2g9QAyg5A5gCQg0gIgRguQgRgwAqglQAUgQAggFQgSgPAMgQQAMgQAXAIQgbgWAegJQAdgJAXAOQgggiApgjQAjgfAqgFQAzgEAoAjQArAngYAwIgCAAQAgAFAQAVQAOATgDAZQgEAYgTAQQgVASgfgCQAMAKAAANQAAAOgMAKQAWgFAPADQAVAEgGAVQAZgHAPAKQAOAJABATQADAvg0AOQAUAGAFAUQAFARgJAUQARgOAZgCQAYgCAUAKIACACQAJgRAHgGQgZgDgKgUQgJgRAGgVQAGgVASgLQAUgMAaAIIACAAQhWgdAhhWQANgjAdgeQAegeAfgMQBOgeBDAzQBEA0gWBRQAwgCABAnQABAlgsANQAqgFAnAfQASgoBHAGQgUgIAOgRQANgRATAAQgBgVANgKQALgIAUABQgmhFAvgbQApgXBKASQBKATAfAqQAkAxg2A0QAiAEAMAjQAMAigdAWQAdALAXAgQAaAjgHAdQgGAagnAXQgkAWgdAAQAdAfgbAwQgZAsgoAQIgVAPQAhAwgqAtQgqAtgzgaQAbAcgJAfQgHAagfARQgdAQgegFQgggGgMgfQgSAOgbAKIgEgDQAPAwgcAoQgeArg8gIIhPAaQgWAAgSgHQgXgJgFgSQgWA1hegMQhcgLgZgwIgDAAQgPAvguATQgdAMhAAGQAVBThNAjQggAPglgDQgngEgegWQghArgrgmQgrgmATgsIgCgBQgXALgWgJQAGAOgFAJQgGAIgQgCQAOAig1AWQgrATgcgGQAcAIANAgQAOAkgZAXQAcACgBAbQAAAcgeAGIgDAAQAAAKgHANQA/gFAZAwQAaAyhEAhQgaAOgeAAQgVAAgXgHgA3vk6QgEgOAIgJQAGgHAMgBQAZgDAGANQAFAMgLAPQgKAQgNAAIgCABQgOAAgIgXgAtKmMQgWgNADgZQADgZAdAGQAqAKgLAhQgFANgLAEQgFADgGAAQgIAAgJgGg");
    this.shape.setTransform(-0.022, 0.0029);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-152.1, -80.2, 304.2, 160.4);


  (lib.Symbol24 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#A7745D").s().p("AisgHIAogMIAOgEIANgEQAOAPAdAYQAfAbANAOIADAEIgUAGIAAAAIg1ARQgmgqgugtgABnAAQgmgcgmgjIAsgKIAFgBIAcgFIAcAWIApAgIhAAeIgGgFg");
    this.shape.setTransform(37.425, -5.0875);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#5C301B").s().p("AhDBBQgagbgNgNIgRgOQgJgJgGgGIgGgJIAxgPIAAAAIgpAMQAuAtAnAqIgIACIgIgIgAA1hGIAKgCIAAAAQAmAjAnAdIAFAFIgIADIhUhGg");
    this.shape_1.setTransform(33.7875, -4.1875);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#7B5848").s().p("AlXBMIAUgFIApgMIDZhIIBKgVIDng1IAeAAQAmgBAkAKIgIAFIgSgBQgtAEgrAHIhTAMIg0AKQhFAOhDAUIhgAbIijA6IgQAEQgEAAgCACQgBAAAAABQgBAAAAABQgBAAAAABQAAAAAAABIgBAAg");
    this.shape_2.setTransform(34.3, -9.1262);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#CF7F59").s().p("Aj+CRIgHgDQgygfgvgkIgqglIgDgLIgBAAQABgEACgEIAWgKQAIgBAHgEQAegMAhgJIEohQIEJguICRgFIgKAQIg4A2IhMA4Qg4Apg/AdIiEA0Ih0AhIgUAFIgyAIIgaADgADtiEIh9AbIjqBAIiGAuQgyAPgsAQIACADIAQAQIA8AtIARARIACABQARAOAWgFIAvgGIAVgEQA7gKA5gRIBngjIBZgmIA8ggIB0hNQAOgGAJgQIABgCIgCgFIgpgLIgigBIgwABgAmUAbIAAAAIAAAAg");
    this.shape_3.setTransform(33.525, -5.45);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#AA6A4C").s().p("Ak1DBIgBAAQgPgWAAgEIgHglIANgCIAegBIBKgKIA3gMIBMgYICthCIAUgLQAqgYAogdIBohTIAkgnIgBAJQgCAXgLAVIgDAIIgHADIgFAJQgKATgRALIgQAMQgbAVgcARIh1A/IjlBWIgpAIQglALglgGIgGAFIACABQALAJAPgBIA/gIIBpgiICVg5ICAhFIATgNIAuggIAagYIgFAJIgEAGQgqA6g2AuQgyAqhAASIglABIgGAAQgSADgKARIgHAFQgYATgcAEIgLACQgVAHgVAEQhIAThKANIgPABQgJACgKAAQgOAAgOgEgAlHBdIAtADIA3gGIAKAAIA1gOIBmgdICVg9IBTgvIBKg1IACgBIBVhQIABgBIABABIgCAWIgMAAIgCADIgZAaIhlBSIhMA1IgVAKIiwBFIgbAKIg9ASIgsAJIh2ANg");
    this.shape_4.setTransform(41.275, 0.855);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#6F361C").s().p("AAvBYIgVgIQgzgXgbg0IgBgFIABABIARALIAfASIAEACIABgCIAAgCQgZgVgcgRIgHgEIgNhGIAAgOIANALIAIAIQAYAVAbAUQAKAHAMAGQAZAOAPARIAAAkIAHAuQABAFADAGg");
    this.shape_5.setTransform(0.85, 9.5);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#894A2C").s().p("AkBBpIgRgNQgagSgWgXICqg5IAGAJQAGAGAKAKIAQAOQAOANAZAbIAJAIIgIADIhzAXgAAKA0QgMgOgfgbQgdgYgOgPIARgFQAlgNAmgJIAngKIgKADIBUBGIg6AbIAAAAIgPAHIgrAOIgDgEgAhngVIANgDIgNAEgAChg6IgcgXIAJgBICDgVIAygDIh2BOIgDACIgpggg");
    this.shape_6.setTransform(34.55, -4.95);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#6B0F0F").s().p("AjuDRIAAAAIgCgBIgBAAQhFgPgsgvQg7g/gChYIgBgYQgBgFAEgCQAQgLARgIIB/gsID4g/IDHgmIAHgBIANgCIAYgDQBOgJBNACIAUAAIABACIABAFQACAJAAAKQABAxgTAtQgMAcgQAZIACABIgGAJIgRARIgRARQg6BChRAlQglASgrAGQgKgEgIAHQgyAohBANQg6ANg7AKQgYAEgYAAQgbAAgbgFgAFzh8IhnBTQgoAegqAYIgVALIisBCIhNAYIg3AMIhJAKIgfABIgMACIAGAlQABADAPAXIAAAAQAYAGAYgEIAPgCQBKgNBHgSQAWgFAUgGIALgDQAcgEAYgSIAHgGQAKgQASgDIAGgBIAmAAQA/gSAygrQA3guApg5IAFgHIAEgIIgZAYIgvAgIgSAMIiBBFIiWA5IhnAiIg/AIQgPABgMgIIgBgBIAFgFQAlAGAmgLIApgIIDlhXIB1g/QAcgQAbgVIAQgNQAQgKALgUIAFgJIAGgDIAEgIQALgUABgXIABgKgAj9DBQgEgFgBgFIgHguIAAglQgPgRgZgOQgMgGgKgIQgbgTgZgVIgIgIIgNgKIAAAMIANBHIAIADQAbASAbAWIAAACIgCACIgFgCIgfgSIgQgLIgBgBIABAFQAbA0AzAWIAWAJIAaAKIAAAAgAj/CBIB2gNIAsgKIA9gSIAbgKICwhEIAUgKIBNg2IBlhRIAZgbIACgCIALgBIACgWIAAgBIgCABIhVBRIgCABIhJA1IhTAuIiXA+IhkAcIg1AOIgKABIg3AGIgtgDgAmUgaIADAKIAqAkQAvAkAyAfIAHAEIA2ADIAagCIAygJIAUgFIB0ghICEgzQA/gdA4gpIBMg4IA4g3IAKgQIiRAFIkJAvIkoBQQghAIgeAOQgHADgIACIgWAKQgCADgBAFIAAAAIAAAAIAAAAIABAAgAj+BAIgCgCIgRgRIg8gtIgQgOIgCgEQAsgPAygQICGgvIDqhAIB9gbIAwgBIAiABIApALIACAFIgBADQgJAPgOAGIh0BNIg8AhIhZAmIhnAiQg5ARg7AKIgVAEIgvAHIgLABQgPAAgNgKgAAZhpQgmAKglAMIgRAFIgNAEIgOAEIgxAPIiqA5QAXAWAZASIARANIBEACIBzgXIAIgDIAHgCIA0gRIAAAAIAVgGIAqgNIAPgGIABgBIA5gbIAJgEIBAgfIADgBIB2hPIgyAEIiDAUIgJACIgcAFIgFABIgsAKIAAAAIgoAJgAEUizIgdABIjoA0IhJAVIjZBKIgqAMIgTAFIARAMIABgBQAAAAAAgBQAAAAAAgBQABAAAAgBQABAAAAgBQADgBAEAAIAQgFICjg6IBfgbQBDgUBGgPIA0gJIBTgNQAqgHAugDIASAAIAHgEQghgKglAAIgEAAg");
    this.shape_7.setTransform(33.6247, 0.0234);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_7}, {t: this.shape_6}, {t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-8.1, -21.4, 83.5, 42.9);


  (lib.Symbol23 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#561C00").ss(0.5, 1, 1).p("Akzi2QAMgdAWg3QAYhAAMgWQAVgtAjgdQAoggAsABQApAAAlAeQAgAbAVArQARAfAMAsQAGAWANA6QAKAxAAAdQAAAsgUAdQgZAlg4AMQgVADgaABAjKj+QAAAPgDAKQgEALgFAAQgFAAgDgLQgEgKAAgPQAAgPAEgLQADgLAFAAQAFAAAEALQADALAAAPgAhDj0QAAAMgDAIQgDAJgFAAQgFAAgEgJQgDgIAAgMQAAgMADgJQAEgIAFAAQAFAAADAIQADAJAAAMgAhmlaQAfgSAMAlAhhi4QAFgHAJgDQANgEANAGQALAFAHANQAHALABAOQABAUgLAgQgTAtgRAXQgeAvgigEQgEAAgEgBQgNgDgLgKQgBgBgBgBQgNgOgHgUQgFgMgJgZQgJgVgJgOQABAAABABQAcAMArADQAeACASgGQAZgGALgUQAFgLgBgMQgBgNgIgIAhVibQABgHAIgIIAFgBQACAAAFAFAhlixQACgEACgDAjzihQAHAEAIAFQAuAeAXAGQAWAGAFgIAjzihQAQgDAXgQQAbgUANgEQASgFASAFQASAFANAPAiwgyQACAMAIAHQAHAGAMgCQABAAAGgDQAEAAABgBQACAAACgDAgkBuQgJgTAAgUQgBgVAKgVQAKgUARgLACgCNQgJgpAXggQAFgKAIgCQAEgBAEACQAFADAAAEQANgeALgLQAKgIANgCQANgCAJAHQAJAHAFASQAEAagJAYQAIgNALgFQAOgHAJAKQAJAHgCATQgGAXgLAPQgBABgBABAE2COQADgDAFACQAIACACAHQACAKgMAOQgSAUgXAPQgBABgBACQgEAGgIAMQgCgEAAgCQACAagJAaACkCPQAKgIANAAQAHgCAGAGQAEAEAAAMIAAADQAEgLAHgEQAFgFAHgCQAIgBAFAEQAEAAADAFQACACAAAJIAAAIQAGgIAJAAQAJAAAEAJQADAFAAAMQgDAZAAADAD5CjIAAANQgDAIgCACQAAgBgDAAADVC5QAHgIAHgCQAGAAAFAGQACABABADQACADABAFQABADABAEQAFgBAEAEQADAEABAHQABAFABAFADJCnQABAAABABQADACACADQAEAFABAGQAAAAAAABQABAJgEAKQgCAFgGAOAD2DIQABAOgHAQQgBAEgJAOQgGAGgBAAADJCnQACgFABgEIgBAKACRCqQAQAHAHANQAAAAAFgKQADgGADgDQALgIAJADQABABABAAAjbEUQARA9AvAvQAwAwA+ARQA+ASBCgRQBBgQAvguQAegbAQgiQALgaACgZQgDAHgEAHQgSAlgiAVQggAVgpACQgoACglgRQgUgIgMgOQgOgQAAgSACRCqQACgJANgQQACgBACgBACgCNQAAAAAAAAQACAAABABQABAAAAABAARDYQAHgZAagNQATgLAhgDQAZAAAMAEQADABADABAgkBuQAmgRA+AHQBFAKAbAfAjbEUQAAgCABgBQAEgWAJgcQACApATAnIAAhNQAVAiAJAmQAJgggEgmQAjA4AIALQAJgXgGgZQgFgbgSgQQAOADAMAKQAMAKAEAOQACAGADAEQAEAEAEgBQAFgDAAgKQgCgYgGgYIAlASQAFADADgDQAEgBgEgJQgTgmgEgIQAOAMAVAEQgGgPgQgUQAQAQASgEQgSgOgLgUQgBgCgBgCAgYErQgGAUAJAQAgFEgQgDACgDAPQgCAOADAKAiVANQgCAsgtBDQgxBHgGAmQAHgOARgXQgOAvAUAuQABgHABgGAkzi2QAMgFAMACQAHACAhAWAknhuQgNgEgJgKQgJgKgBgNQgCgSAOgMQAEgDAEgCAj0h0QgaAOgZgIQAAAAAAAAQAHAgApAiQAkAfAmANAj0h0QAKAFAHADAkYh9QAMgBAQAHQAEACAEABAjeldQAOgWAfAI");
    this.shape.setTransform(0.007, -0.0319);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#EAFBFF").s().p("AhjDWQg+gRgwgwQgvgvgRg+IAAgCQAEgXAKgaQACAoATAnIAAhMQAVAhAJAmQAJgggEglIArBCQAJgXgGgZQgFgagSgQQAOADAMAKQAMAKAEANQACAGADAEQAEAEAEgBQAFgDAAgKQgCgXgGgYIAlASQAFADADgDQAEgBgEgJIgXguQAOAMAVAEQgGgPgQgUQAQAQASgEQgSgOgLgUIgCgEQAngRA9AHQBFAKAbAfIAAAAIADABIABAAIgBAAIgDgBIAAAAQgJgpAXggQAFgKAIgCQAEgBAEACQAFADAAAEQANgeALgLQAKgIANgCQANgCAJAHQAJAHAFASQAEAagJAYQAIgNALgFQAOgHAJAKQAJAHgCATQgGAXgMAOIgBADIABgDQAEgCAFACQAIACACAHQACAKgMAOQgTAUgWAPIADgcQAAgMgDgFQgEgJgJAAQgJAAgGAIIAAgIQAAgJgCgCQgDgFgEAAQgFgEgIABQgHACgFAFQgHAEgEALIAAgDQAAgMgEgEQgGgGgHACQgNAAgKAHIgEADQgNAQgCAJQAPAGAIAOIAFgKQADgGADgDQALgIAJADIACABIACABQADABACAEQAEAFABAGIAAABQAGgJAIgBQAGAAAFAGIADAEIADAIIABAHQAFgBAFAEQADAEABAHIABAKQADAZgKAZIgGAPQgSAlgiAVQggAVgpACIgJAAIAAAAIAAAAQgiAAgdgOIgBAAIgBAAIgCgBQgUgIgMgOQgPgQAAgSQAAASAPAQQAMAOAUAIIACABIABAAIABAAQAdAOAiAAIAAAAIAAAAIAJAAQApgCAggVQAigVASglIAGgPQgBAagLAaQgQAigeAbQgvAuhBAQQgfAIgfAAQghAAghgJgAhLBkQgFgKAAgLQAAgHACgIQgCAIAAAHQAAALAFAKgAhABeQgCgHAAgIIABgJQADgPADgCQgDACgDAPIgBAJQAAAIACAHgAC6gEIgKARQgGAGgBAAQABAAAGgGIAKgRQAGgOAAgLIgBgFIABAFQAAALgGAOgACcgeIgIATIAIgTQADgJAAgHIAAgDIAAADQAAAHgDAJgAgDg4QgaANgHAZQAHgZAagNQASgLAhgDQAZAAAMAEIAGACIgGgCQgMgEgZAAQghADgSALgADHAoIAAAAg");
    this.shape_1.setTransform(5.4268, 23.4973);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#FFA100").s().p("AAmBvIgIgBQgNgEgLgLIgCgBQgMgOgHgVIgOgkQgJgWgJgNIACABQAcALAqADIADAAIAMABIAAAAIAAAAQATAAAMgEIACAAQAZgGALgUQAFgJAAgKIgBgEQgBgNgIgIQAIAIABANIABAEQAAAKgFAJQgLAUgZAGIgCAAQgMAEgTAAIAAAAIAAAAIgMgBIgDAAQgqgDgcgLIgCgBIgRgHIgIgEIgDgBQgMgEgJAAIAAAAIAAAAIgCAAIgCAAIACAAIACAAIAAAAIAAAAQAJAAAMAEIADABIAIAEQgaANgZgHIAAAAQgNgEgJgKQgJgLgBgNQgCgRAOgNIAIgEQAMgGAMADQAHACAhAVQAQgDAXgQQAagTANgEQASgGASAFQASAFANAPQAGgGAIgDQANgEANAFQALAGAHAMQAHAMABAOQABAUgLAfQgTAsgRAYQgbAsggAAIgFAAgAAgBEIAHgCQAEAAABgCQABAAAAAAQABAAAAAAQABgBAAAAQABgBAAAAQAAAAgBABQAAAAgBABQAAAAgBAAQAAAAgBAAQgBACgEAAIgHACIgGABQgHAAgGgFQgIgGgCgMQACAMAIAGQAGAFAHAAIAGgBgAAkgRQAIAAADgEIAAAAIAAAAQgDAEgIAAIAAAAIgBAAIgLgCIgBAAIgCAAIgBAAQgVgGgvgeIgPgKIAPAKQAvAeAVAGIABAAIACAAIABAAIALACIABAAIAAAAgABnhJQgIAHgBAIQABgIAIgHIAFgCQACAAAFAFQgFgFgCAAgABOhRIAEgHIgEAHgAhAgTIAAAAg");
    this.shape_2.setTransform(-17.9986, -9.682);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("rgba(0,0,0,0.659)").s().p("AA8AdQgDgIgBgMQABgLADgJQADgIAFAAQAFAAADAIQADAJABALQgBAMgDAIQgDAJgFAAQgFAAgDgJgAhLAYQgEgKAAgOQAAgPAEgLQADgLAGAAQAFAAADALQADALAAAPQAAAOgDAKQgDALgFAAQgGAAgDgLg");
    this.shape_3.setTransform(-14.7, -25.425);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#A5B4B8").s().p("Ai/EtQgKAbgEAXIAAACIgCAOQgUguAOgvQgRAXgHAOQAGgmAxhHQAthDACgsIAIABQAiAEAegwQARgXATgsQALgggBgUQgBgOgHgLQgHgNgLgFQgNgGgNAEQgJADgFAGQgNgOgSgFQgSgFgSAFQgNAEgbAUQgXAPgQADQghgVgHgCQgMgDgMAGIAihUQAYhAAMgWQAVgtAjgdQAoggAsABQApAAAlAeQAgAbAVArQARAfAMAsQAGAWANA6QAKAxAAAdQAAAsgUAcQgaAkg4AMQgUAFgaABQAagBAUgFQgQANgKAUQgKAVABAVQAAAUAJATIACAEQALAUARAOQgRAEgQgQQAQAUAGAPQgVgEgOgMIAXAuQAEAJgEABQgDADgFgDIglgSQAGAYACAYQAAAKgFADQgEABgEgEQgDgEgCgGQgEgOgMgKQgMgKgOgDQASAQAFAbQAGAZgJAXIgrhDQAEAmgJAgQgJgmgVgiIAABNQgTgngCgpgAhJi7QgDAJAAAMQAAAMADAIQAEAJAFAAQAFAAADgJQADgIAAgMQAAgMgDgJQgDgIgFAAQgFAAgEAIgAjQjKQgEALAAAPQAAAPAEAKQADALAFAAQAFAAAEgLQADgKAAgPQAAgPgDgLQgEgLgFAAQgFAAgDALgAgtj5IgBgDQgIgWgQAAIAAAAIAAAAQgIAAgKAGQAKgGAIAAIAAAAIAAAAQAQAAAIAWIABADgAjQkPIABgBIAAAAIAAAAIABgBQAJgOASAAIAAAAIABAAQAGAAAJACQgJgCgGAAIgBAAIAAAAQgSAAgJAOIgBABIAAAAIAAAAIgBABgAESEuIgBgKQgBgHgDgEQgFgEgFABIgBgHIgDgIQABAAAAAAQABAAAAAAQABAAAAABQAAAAAAAAQACgCADgIIAAgNIAAANQgDAIgCACQAAAAAAAAQAAgBgBAAQAAAAgBAAQAAAAgBAAIgDgEQgFgGgGAAQgIABgGAJIAAgBQgBgGgEgFQgCgEgDgBIABgKIgDAJIgCgBQgJgDgLAIQgDADgDAGIgFAKQgIgOgPgGQACgJANgQIAEgDQAKgHANAAQAHgCAGAGQAEAEAAAMIAAADQAEgLAHgEQAFgFAHgCQAIgBAFAEQAEAAADAFQACACAAAJIAAAIQAGgIAJAAQAJAAAEAJQADAFAAAMIgDAcIgDADIgLASIgDgGgAD/EHIAAAAgAjpAhQgpghgIggIABAAQAZAIAagOIARAIQAJAOAJAUIAOAlQAHAUANAOIACACQgmgNgkgfg");
    this.shape_4.setTransform(-1.3625, -7.8015);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    // Layer_1_copy
    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.lf(["rgba(63,98,133,0)", "rgba(255,255,255,0.439)"], [0, 1], -8, 22.7, 4.4, -7.2).s().p("AiWCTQgegJgSgQQgKgIgVgVIgbgcIgGgCQgPgFACgIQABgFAJgFIABAAQAHgDAKgBQAHgMAigLQAdgKAhgEIAAAAQAJgDAJgBQgEgOgigTQgpgZgFgOIgBgBIABgCIABAAIACAAIANAJQgFgOAOgOIACAAIABAAIABACIAAAAQAPgTAlgPQAogQA1gGQAzgGA4AFQA3AGAnAQIAAgBQAZAKAOAMQAKAJACAJQAHAIgBAJIAJAFIABAAIABgBIACABIAAACQASAFAOAGQAKAFgBADQgCADgGABQgFAAgDgBQAKAFAFAIQABAHgHACQgIADgLgCQgJgCgHgEQAHAIgDAJQgEAGgHACQgIADgLgBQgLgBgIgCQgJgEgLgKIgEACIgHAAQgHAAgEgEQgRgJAEgMQgXAJg4AEIAAAAQgzACgggFQgGAFAAAFIAAABQAAAFAHAGQAIAHANAEQAvADAVANQASAKABAQQAAALgJASIgBAAIgPAbIAAgBQgLAQgOALQgSAPgaAJQggAKgjAAIgDAAQgkAAgfgLg");
    this.shape_5.setTransform(-3.2273, 59.0946);

    this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol23, new cjs.Rectangle(-33.8, -46.8, 67.69999999999999, 121.7), null);


  (lib.Symbol7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_216();
    this.instance.setTransform(-66.9, -139.05, 0.4382, 0.4382);

    this.instance_1 = new lib.CachedBmp_215();
    this.instance_1.setTransform(-36.15, 129.95, 0.4382, 0.4382);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-66.9, -139, 134.5, 288.3);


  (lib.Symbol6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_214();
    this.instance.setTransform(-75, -54, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#4D7E02").s().p("ADvEJQjpg1jmg6Qjmg5jKh5IhDgoIgBgFQD9hSDxhtQDlgxDcBSQDCBIC6BVQDWBjiVCAQiPB6inAAQg4AAg7gOg");
    this.shape.setTransform(-9.7549, 28.7274);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-82.2, -54, 157.2, 110.7);


  (lib.Symbol5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_247();
    this.instance.setTransform(-42.85, -38, 0.2484, 0.2484);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-42.8, -38, 85.9, 76);


  (lib.Symbol4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_246();
    this.instance.setTransform(-47.25, -18.35, 0.4243, 0.4243);

    this.instance_1 = new lib.CachedBmp_211();
    this.instance_1.setTransform(-49.8, 20.3, 0.4243, 0.4243);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-49.8, -18.3, 96.8, 57.7);


  (lib.Symbol2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_206();
    this.instance.setTransform(-52.8, -159.9, 0.4053, 0.4053);

    this.instance_1 = new lib.CachedBmp_205();
    this.instance_1.setTransform(-25.25, 6.2, 0.4053, 0.4053);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-52.8, -159.9, 133, 185.20000000000002);


  (lib._3_Stars = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_202();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Star_R = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_201();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_stae_L = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_200();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib.Group_1_4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#542F1D").s().p("ABEH/QgYgYgPgKIgqgWQgUgLACgVQAAgLALgkQAFAoAXAEQAXAEAbg1IAXg2IABBKQALBLAuABQAuABAMg7IACg7IAUAwQAaApAhgjQAGAeAAALQAAAMgQAJIggAQQgKAGgZAVQgcARgoACIgIAAQgjAAgWgRgAhTlIIgqAOQglgJgaALQgUgVgIgNQgjgzgghXIBQgrIBdApQASAqAsAvQAnAqAlAXQgQAGgKALIgFALQgfgfgxAHg");
    this.shape.setTransform(28.425, 52.8345);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Group_1_4, new cjs.Rectangle(0, 0, 56.9, 105.7), null);


  (lib.Blend = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Blend_0();

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Blend, new cjs.Rectangle(0, 0, 83, 77), null);


  (lib.Path_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.rf(["#FFFFFF", "#FCFDFE", "#F1F8FC", "#E0EFF7", "#C7E2F1", "#A8D1EA", "#81BDE0", "#53A4D5", "#1F89C8", "#0079C1"], [0.604, 0.647, 0.682, 0.714, 0.741, 0.773, 0.8, 0.827, 0.855, 0.871], -1.8, 2.4, 0, -1.8, 2.4, 26.8).s().p("AiKCLQg6g5AAhSQAAhQA6g6QA6g6BQAAQBSAAA6A6QA5A5AABRQAABSg5A5Qg6A6hSAAQhQAAg6g6g");
    this.shape.setTransform(19.7, 19.725);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_1, new cjs.Rectangle(0, 0, 39.4, 39.5), null);


  (lib.Path = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.rf(["#000000", "#000303", "#000B0E", "#001A1F", "#002E38", "#004857", "#00687E", "#008EAC", "#00B8E0", "#00C2EC"], [0.604, 0.647, 0.682, 0.718, 0.745, 0.776, 0.808, 0.835, 0.863, 0.871], 2.7, -3.2, 0, 2.7, -3.2, 26.8).s().p("AiKCLQg6g5AAhSQAAhQA6g6QA6g6BQAAQBSAAA6A6QA5A5AABRQAABSg5A5Qg6A6hSAAQhQAAg6g6g");
    this.shape.setTransform(19.7, 19.725);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path, new cjs.Rectangle(0, 0, 39.4, 39.5), null);


  (lib.Path_26 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#CDAD9A").s().p("AgZApQAVg7APhjIAmAAQgIAkAEA0QACAdAGA8IAEAjQg4APg5AIQAOgbARgyg");
    this.shape.setTransform(5.675, 11.775);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_26, new cjs.Rectangle(0, 0, 11.4, 23.6), null);


  (lib.Path_25 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#CDAD9A").s().p("ACWCJQhGgEgxgIQg9gLgygWIgvgWQgbgOgRgGQgTgIgPgUQgOgTgCgUIgEg3QgCgfACgWQA/gPBUAAQAzAABlAHQAIAxAWA4QAUAtAaAqQAcAvAxAjIhNgEg");
    this.shape.setTransform(22.725, 14.1);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_25, new cjs.Rectangle(0, 0, 45.5, 28.2), null);


  (lib.Path_24 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#996E4E").s().p("AhkC2QhOgHgogGQhDgLg+gfIg4gYQgZgLgUgSQgQgOgFhZQgCgxgBhTIgBggIAJAYQAGAPAFACQAFADAJgJIANgOQAFAwAKAfQAKAfATAZQANAQAQAJQAQAJAOgCQAKgCAOgOQARgRAGgDQAZBSAQAdQAaAyAhgNQARgGAZgBIAqABQALAAAGgBQAJgDAHgHIAPgUQAKgNAGgEQAAApAbAbQAUAUAXAEQAPACAUgHIAigOQAmgQAngEQAKgBAIgOIAOgeIAOgdQAJgRAHACQAEBCAwAAIARgBQAJgBAHgEQAFgDAIgIQALgNAUgNIAPgKQAJgHAGgCQAIgCAFgFQAEgDAJgLQAIgKAHgnQAHglAJgKQgDASACAEIAHAMQACAKAAALQAAAUgFARQgFAQgIAIQgsAwgMAKQhtBeiIASQggAEgrAAQhKAAhqgMg");
    this.shape.setTransform(47.425, 19.3883);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_24, new cjs.Rectangle(0, 0, 94.9, 38.8), null);


  (lib.Path_26_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#CDAD9A").s().p("AgZApQAVg7APhjIAmAAQgIAkAEA0QACAdAGA8IAEAjQg4APg5AIQAOgbARgyg");
    this.shape_1.setTransform(5.675, 11.775);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_26_1, new cjs.Rectangle(0, 0, 11.4, 23.6), null);


  (lib.Path_25_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#CDAD9A").s().p("ACWCJQhGgEgxgIQg9gLgygWIgvgWQgbgOgRgGQgTgIgPgUQgOgTgCgUIgEg3QgCgfACgWQA/gPBUAAQAzAABlAHQAIAxAWA4QAUAtAaAqQAcAvAxAjIhNgEg");
    this.shape_1.setTransform(22.725, 14.1);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_25_1, new cjs.Rectangle(0, 0, 45.5, 28.2), null);


  (lib.Path_24_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#996E4E").s().p("AhkC2QhOgHgogGQhDgLg+gfIg4gYQgZgLgUgSQgQgOgFhZQgCgxgBhTIgBggIAJAYQAGAPAFACQAFADAJgJIANgOQAFAwAKAfQAKAfATAZQANAQAQAJQAQAJAOgCQAKgCAOgOQARgRAGgDQAZBSAQAdQAaAyAhgNQARgGAZgBIAqABQALAAAGgBQAJgDAHgHIAPgUQAKgNAGgEQAAApAbAbQAUAUAXAEQAPACAUgHIAigOQAmgQAngEQAKgBAIgOIAOgeIAOgdQAJgRAHACQAEBCAwAAIARgBQAJgBAHgEQAFgDAIgIQALgNAUgNIAPgKQAJgHAGgCQAIgCAFgFQAEgDAJgLQAIgKAHgnQAHglAJgKQgDASACAEIAHAMQACAKAAALQAAAUgFARQgFAQgIAIQgsAwgMAKQhtBeiIASQggAEgrAAQhKAAhqgMg");
    this.shape_1.setTransform(47.425, 19.3883);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_24_1, new cjs.Rectangle(0, 0, 94.9, 38.8), null);


  (lib.Symbol12copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_198();
    this.instance.setTransform(-696.65, -540.25, 0.3801, 0.3801);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true}, 1).wait(184));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-696.6, -540.2, 1394.8000000000002, 1078.9);


  (lib.Locked_1copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_188();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Locked_1copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_187();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Locked_1copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_186();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Locked_1copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_185();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Locked_1copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_184();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Locked_1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_183();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.C_1_ONcopy12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("6", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_162();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("5", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_161();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("4", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_160();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("3", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_159();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("4", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#C36C19").s().p("A0pVaQpRoXAxsqQAxsqIzopQIzooLvABQLwACIEIsQIEIrAYNEQAWNEoKHqQoKHpsUAOIgqABQr5AApBoIg");
    this.shape.setTransform(-2.6405, -24.3306);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_158();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("2", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(195,108,25,0.659)").s().p("A0wVlQodoQANssQANsrIqo3QIro3LsAEQLuADIYJJQIYJKAUM7QAVM9o5HiQo5Hhr7AIIgXAAQrsAAoVoIg");
    this.shape.setTransform(-0.3177, -24.3699);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_157();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("1", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(195,108,25,0.659)").s().p("AgGd1QscgJoaoYQoZoYAcsYQAdsYJDpcQJDpcLhA+QLjA+H7JMQH8JNAVLzQAVLzocIXQoVIPsMAAIgYAAg");
    this.shape.setTransform(-0.1577, -25.9536);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_156();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("2", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_155();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("6", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(195,108,25,0.659)").s().p("A0qVgQogoRAPs1QAPszIWorQIWoqL0AAQL0AAIWIqQIWIrAmMrQAmMtovIXQovIXsGACIgHAAQsCAAodoPg");
    this.shape.setTransform(0.9752, -22.4118);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_154();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("5", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(195,108,25,0.659)").s().p("A09VVQoqohApsHQAosGHwpBQHxpBMNgJQMNgIIjIrQIjIsAWMsQAXMsoQIHQoQIHsmATIg1ABQsDAAoXoQg");
    this.shape.setTransform(0.7834, -24.3999);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_153();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("1", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_152();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("3", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(195,108,25,0.659)").s().p("A0YWAQoboAAFtOQAFtNIoozQIno0L1AQQL2AQIDIsQIDIqAXNCQAXNDomH3QonH3r6AMIgkAAQrkAAoOnzg");
    this.shape.setTransform(0.4414, -23.1574);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_151();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.Blendcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Blend_0();

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Blendcopy, new cjs.Rectangle(0, 0, 83, 77), null);


  (lib.Pathcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.rf(["#2F0202", "#3B0101", "#3B0303", "#4A0101", "#600101", "#790306", "#A50003", "#820506", "#C00004"], [0.604, 0.635, 0.663, 0.698, 0.729, 0.776, 0.808, 0.816, 0.839], 2.7, -3.2, 0, 2.7, -3.2, 26.8).s().p("AiKCLQg6g5AAhSQAAhQA6g6QA6g6BQAAQBSAAA6A6QA5A5AABRQAABSg5A5Qg6A6hSAAQhQAAg6g6g");
    this.shape.setTransform(19.7, 19.725);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Pathcopy, new cjs.Rectangle(0, 0, 39.4, 39.5), null);


  (lib.Path_26copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#CDAD9A").s().p("AgZApQAVg7APhjIAmAAQgIAkAEA0QACAdAGA8IAEAjQg4APg5AIQAOgbARgyg");
    this.shape.setTransform(5.675, 11.775);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_26copy6, new cjs.Rectangle(0, 0, 11.4, 23.6), null);


  (lib.Path_26copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#CDAD9A").s().p("AgZApQAVg7APhjIAmAAQgIAkAEA0QACAdAGA8IAEAjQg4APg5AIQAOgbARgyg");
    this.shape.setTransform(5.675, 11.775);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_26copy5, new cjs.Rectangle(0, 0, 11.4, 23.6), null);


  (lib.Path_26copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#CDAD9A").s().p("AgZApQAVg7APhjIAmAAQgIAkAEA0QACAdAGA8IAEAjQg4APg5AIQAOgbARgyg");
    this.shape.setTransform(5.675, 11.775);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_26copy4, new cjs.Rectangle(0, 0, 11.4, 23.6), null);


  (lib.Path_26copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#CDAD9A").s().p("AgZApQAVg7APhjIAmAAQgIAkAEA0QACAdAGA8IAEAjQg4APg5AIQAOgbARgyg");
    this.shape.setTransform(5.675, 11.775);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_26copy3, new cjs.Rectangle(0, 0, 11.4, 23.6), null);


  (lib.Path_26copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#CDAD9A").s().p("AgZApQAVg7APhjIAmAAQgIAkAEA0QACAdAGA8IAEAjQg4APg5AIQAOgbARgyg");
    this.shape.setTransform(5.675, 11.775);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_26copy2, new cjs.Rectangle(0, 0, 11.4, 23.6), null);


  (lib.Path_26copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#CDAD9A").s().p("AgZApQAVg7APhjIAmAAQgIAkAEA0QACAdAGA8IAEAjQg4APg5AIQAOgbARgyg");
    this.shape.setTransform(5.675, 11.775);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_26copy, new cjs.Rectangle(0, 0, 11.4, 23.6), null);


  (lib.Path_26_2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#CDAD9A").s().p("AgZApQAVg7APhjIAmAAQgIAkAEA0QACAdAGA8IAEAjQg4APg5AIQAOgbARgyg");
    this.shape_2.setTransform(5.675, 11.775);

    this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_26_2, new cjs.Rectangle(0, 0, 11.4, 23.6), null);


  (lib.Path_25copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#CDAD9A").s().p("ACWCJQhGgEgxgIQg9gLgygWIgvgWQgbgOgRgGQgTgIgPgUQgOgTgCgUIgEg3QgCgfACgWQA/gPBUAAQAzAABlAHQAIAxAWA4QAUAtAaAqQAcAvAxAjIhNgEg");
    this.shape.setTransform(22.725, 14.1);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_25copy6, new cjs.Rectangle(0, 0, 45.5, 28.2), null);


  (lib.Path_25copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#CDAD9A").s().p("ACWCJQhGgEgxgIQg9gLgygWIgvgWQgbgOgRgGQgTgIgPgUQgOgTgCgUIgEg3QgCgfACgWQA/gPBUAAQAzAABlAHQAIAxAWA4QAUAtAaAqQAcAvAxAjIhNgEg");
    this.shape.setTransform(22.725, 14.1);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_25copy5, new cjs.Rectangle(0, 0, 45.5, 28.2), null);


  (lib.Path_25copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#CDAD9A").s().p("ACWCJQhGgEgxgIQg9gLgygWIgvgWQgbgOgRgGQgTgIgPgUQgOgTgCgUIgEg3QgCgfACgWQA/gPBUAAQAzAABlAHQAIAxAWA4QAUAtAaAqQAcAvAxAjIhNgEg");
    this.shape.setTransform(22.725, 14.1);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_25copy4, new cjs.Rectangle(0, 0, 45.5, 28.2), null);


  (lib.Path_25copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#CDAD9A").s().p("ACWCJQhGgEgxgIQg9gLgygWIgvgWQgbgOgRgGQgTgIgPgUQgOgTgCgUIgEg3QgCgfACgWQA/gPBUAAQAzAABlAHQAIAxAWA4QAUAtAaAqQAcAvAxAjIhNgEg");
    this.shape.setTransform(22.725, 14.1);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_25copy3, new cjs.Rectangle(0, 0, 45.5, 28.2), null);


  (lib.Path_25copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#CDAD9A").s().p("ACWCJQhGgEgxgIQg9gLgygWIgvgWQgbgOgRgGQgTgIgPgUQgOgTgCgUIgEg3QgCgfACgWQA/gPBUAAQAzAABlAHQAIAxAWA4QAUAtAaAqQAcAvAxAjIhNgEg");
    this.shape.setTransform(22.725, 14.1);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_25copy2, new cjs.Rectangle(0, 0, 45.5, 28.2), null);


  (lib.Path_25copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#CDAD9A").s().p("ACWCJQhGgEgxgIQg9gLgygWIgvgWQgbgOgRgGQgTgIgPgUQgOgTgCgUIgEg3QgCgfACgWQA/gPBUAAQAzAABlAHQAIAxAWA4QAUAtAaAqQAcAvAxAjIhNgEg");
    this.shape.setTransform(22.725, 14.1);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_25copy, new cjs.Rectangle(0, 0, 45.5, 28.2), null);


  (lib.Path_25_2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#CDAD9A").s().p("ACWCJQhGgEgxgIQg9gLgygWIgvgWQgbgOgRgGQgTgIgPgUQgOgTgCgUIgEg3QgCgfACgWQA/gPBUAAQAzAABlAHQAIAxAWA4QAUAtAaAqQAcAvAxAjIhNgEg");
    this.shape_2.setTransform(22.725, 14.1);

    this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_25_2, new cjs.Rectangle(0, 0, 45.5, 28.2), null);


  (lib.Path_24copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#996E4E").s().p("AhkC2QhOgHgogGQhDgLg+gfIg4gYQgZgLgUgSQgQgOgFhZQgCgxgBhTIgBggIAJAYQAGAPAFACQAFADAJgJIANgOQAFAwAKAfQAKAfATAZQANAQAQAJQAQAJAOgCQAKgCAOgOQARgRAGgDQAZBSAQAdQAaAyAhgNQARgGAZgBIAqABQALAAAGgBQAJgDAHgHIAPgUQAKgNAGgEQAAApAbAbQAUAUAXAEQAPACAUgHIAigOQAmgQAngEQAKgBAIgOIAOgeIAOgdQAJgRAHACQAEBCAwAAIARgBQAJgBAHgEQAFgDAIgIQALgNAUgNIAPgKQAJgHAGgCQAIgCAFgFQAEgDAJgLQAIgKAHgnQAHglAJgKQgDASACAEIAHAMQACAKAAALQAAAUgFARQgFAQgIAIQgsAwgMAKQhtBeiIASQggAEgrAAQhKAAhqgMg");
    this.shape.setTransform(47.425, 19.3883);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_24copy6, new cjs.Rectangle(0, 0, 94.9, 38.8), null);


  (lib.Path_24copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#996E4E").s().p("AhkC2QhOgHgogGQhDgLg+gfIg4gYQgZgLgUgSQgQgOgFhZQgCgxgBhTIgBggIAJAYQAGAPAFACQAFADAJgJIANgOQAFAwAKAfQAKAfATAZQANAQAQAJQAQAJAOgCQAKgCAOgOQARgRAGgDQAZBSAQAdQAaAyAhgNQARgGAZgBIAqABQALAAAGgBQAJgDAHgHIAPgUQAKgNAGgEQAAApAbAbQAUAUAXAEQAPACAUgHIAigOQAmgQAngEQAKgBAIgOIAOgeIAOgdQAJgRAHACQAEBCAwAAIARgBQAJgBAHgEQAFgDAIgIQALgNAUgNIAPgKQAJgHAGgCQAIgCAFgFQAEgDAJgLQAIgKAHgnQAHglAJgKQgDASACAEIAHAMQACAKAAALQAAAUgFARQgFAQgIAIQgsAwgMAKQhtBeiIASQggAEgrAAQhKAAhqgMg");
    this.shape.setTransform(47.425, 19.3883);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_24copy5, new cjs.Rectangle(0, 0, 94.9, 38.8), null);


  (lib.Path_24copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#996E4E").s().p("AhkC2QhOgHgogGQhDgLg+gfIg4gYQgZgLgUgSQgQgOgFhZQgCgxgBhTIgBggIAJAYQAGAPAFACQAFADAJgJIANgOQAFAwAKAfQAKAfATAZQANAQAQAJQAQAJAOgCQAKgCAOgOQARgRAGgDQAZBSAQAdQAaAyAhgNQARgGAZgBIAqABQALAAAGgBQAJgDAHgHIAPgUQAKgNAGgEQAAApAbAbQAUAUAXAEQAPACAUgHIAigOQAmgQAngEQAKgBAIgOIAOgeIAOgdQAJgRAHACQAEBCAwAAIARgBQAJgBAHgEQAFgDAIgIQALgNAUgNIAPgKQAJgHAGgCQAIgCAFgFQAEgDAJgLQAIgKAHgnQAHglAJgKQgDASACAEIAHAMQACAKAAALQAAAUgFARQgFAQgIAIQgsAwgMAKQhtBeiIASQggAEgrAAQhKAAhqgMg");
    this.shape.setTransform(47.425, 19.3883);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_24copy4, new cjs.Rectangle(0, 0, 94.9, 38.8), null);


  (lib.Path_24copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#996E4E").s().p("AhkC2QhOgHgogGQhDgLg+gfIg4gYQgZgLgUgSQgQgOgFhZQgCgxgBhTIgBggIAJAYQAGAPAFACQAFADAJgJIANgOQAFAwAKAfQAKAfATAZQANAQAQAJQAQAJAOgCQAKgCAOgOQARgRAGgDQAZBSAQAdQAaAyAhgNQARgGAZgBIAqABQALAAAGgBQAJgDAHgHIAPgUQAKgNAGgEQAAApAbAbQAUAUAXAEQAPACAUgHIAigOQAmgQAngEQAKgBAIgOIAOgeIAOgdQAJgRAHACQAEBCAwAAIARgBQAJgBAHgEQAFgDAIgIQALgNAUgNIAPgKQAJgHAGgCQAIgCAFgFQAEgDAJgLQAIgKAHgnQAHglAJgKQgDASACAEIAHAMQACAKAAALQAAAUgFARQgFAQgIAIQgsAwgMAKQhtBeiIASQggAEgrAAQhKAAhqgMg");
    this.shape.setTransform(47.425, 19.3883);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_24copy3, new cjs.Rectangle(0, 0, 94.9, 38.8), null);


  (lib.Path_24copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#996E4E").s().p("AhkC2QhOgHgogGQhDgLg+gfIg4gYQgZgLgUgSQgQgOgFhZQgCgxgBhTIgBggIAJAYQAGAPAFACQAFADAJgJIANgOQAFAwAKAfQAKAfATAZQANAQAQAJQAQAJAOgCQAKgCAOgOQARgRAGgDQAZBSAQAdQAaAyAhgNQARgGAZgBIAqABQALAAAGgBQAJgDAHgHIAPgUQAKgNAGgEQAAApAbAbQAUAUAXAEQAPACAUgHIAigOQAmgQAngEQAKgBAIgOIAOgeIAOgdQAJgRAHACQAEBCAwAAIARgBQAJgBAHgEQAFgDAIgIQALgNAUgNIAPgKQAJgHAGgCQAIgCAFgFQAEgDAJgLQAIgKAHgnQAHglAJgKQgDASACAEIAHAMQACAKAAALQAAAUgFARQgFAQgIAIQgsAwgMAKQhtBeiIASQggAEgrAAQhKAAhqgMg");
    this.shape.setTransform(47.425, 19.3883);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_24copy2, new cjs.Rectangle(0, 0, 94.9, 38.8), null);


  (lib.Path_24copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#996E4E").s().p("AhkC2QhOgHgogGQhDgLg+gfIg4gYQgZgLgUgSQgQgOgFhZQgCgxgBhTIgBggIAJAYQAGAPAFACQAFADAJgJIANgOQAFAwAKAfQAKAfATAZQANAQAQAJQAQAJAOgCQAKgCAOgOQARgRAGgDQAZBSAQAdQAaAyAhgNQARgGAZgBIAqABQALAAAGgBQAJgDAHgHIAPgUQAKgNAGgEQAAApAbAbQAUAUAXAEQAPACAUgHIAigOQAmgQAngEQAKgBAIgOIAOgeIAOgdQAJgRAHACQAEBCAwAAIARgBQAJgBAHgEQAFgDAIgIQALgNAUgNIAPgKQAJgHAGgCQAIgCAFgFQAEgDAJgLQAIgKAHgnQAHglAJgKQgDASACAEIAHAMQACAKAAALQAAAUgFARQgFAQgIAIQgsAwgMAKQhtBeiIASQggAEgrAAQhKAAhqgMg");
    this.shape.setTransform(47.425, 19.3883);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_24copy, new cjs.Rectangle(0, 0, 94.9, 38.8), null);


  (lib.Path_24_2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#996E4E").s().p("AhkC2QhOgHgogGQhDgLg+gfIg4gYQgZgLgUgSQgQgOgFhZQgCgxgBhTIgBggIAJAYQAGAPAFACQAFADAJgJIANgOQAFAwAKAfQAKAfATAZQANAQAQAJQAQAJAOgCQAKgCAOgOQARgRAGgDQAZBSAQAdQAaAyAhgNQARgGAZgBIAqABQALAAAGgBQAJgDAHgHIAPgUQAKgNAGgEQAAApAbAbQAUAUAXAEQAPACAUgHIAigOQAmgQAngEQAKgBAIgOIAOgeIAOgdQAJgRAHACQAEBCAwAAIARgBQAJgBAHgEQAFgDAIgIQALgNAUgNIAPgKQAJgHAGgCQAIgCAFgFQAEgDAJgLQAIgKAHgnQAHglAJgKQgDASACAEIAHAMQACAKAAALQAAAUgFARQgFAQgIAIQgsAwgMAKQhtBeiIASQggAEgrAAQhKAAhqgMg");
    this.shape_2.setTransform(47.425, 19.3883);

    this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_24_2, new cjs.Rectangle(0, 0, 94.9, 38.8), null);


  (lib.Path_1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(0, 0, 0, 0);


  (lib._3_Starscopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_150();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_149();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_148();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_147();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_146();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Star_Rcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_145();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_144();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_143();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_142();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_141();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_stae_Lcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_140();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_139();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_138();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_137();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_136();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib.Symbol484 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy5("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol483 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy5("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol482 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy4("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol481 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy4("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol480 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy3("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol479 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy3("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol478 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy2("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol477 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy2("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol476 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol475 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol474 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_R("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol473 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_L("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol25 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#5799DE").s().p("AqOEbQgIgIgDgMQgDgQAKgdQAbhUAyAAQAIAAARAEQARAEAJAAQAZAAAhgdQAngjARgHQAWgIA1AHQAyAGAXgNQAKgGAQgRQARgQAKgGQAOgJASgBQASAAAPAIIAUANQALAHAJgBQAOAAASgQQBOg/A+gPQAUgFAngFIAvgNQAdgJATgCIAXgCQAOgBAKgDQATgFAdgWQA1gmAZgUQAqghAdggQAWgXARgJQAYgOATAKQASAJAJAnQAJAqAOAMQAtgaAdgqQgSBFghA/QgQAdgRAEQgKACgOgGIgXgKQgggLgmATQgcAOgjAgIhNBEQgQAOgJAHQgPAKgOADQgXAFgbgLQgSgHgbgTQgPANgUAEQgVAEgTgHQgBASgUAOIgoAWQgVALgjAeQg8AphfAEQgiABgwgEIhSgHQgtgDgVANQgIAFgNAOQgOAPgHAFQgTAOgeACQgWACghgFQgXgDgJAEQgLAFgQAcQgNAYgPACIgDAAQgJAAgIgHg");
    this.shape.setTransform(12.064, 19.0151);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#5799DE").s().p("AppETQgMgHgFgNQgGgTAKgfQAMgmAWgZQAageAgAAQAHAAAqALQAdAHAQgLQAHgEAJgMQAKgNAGgEQAagUA0AWQAbALANADQAWAFARgHQASgHAaglQAXgjAWgEQANgCAVAHQAcAKAHABQAbAEAjgTQAUgKAmgVIAZgJQAOgGAIgGQAKgHAQgTQAQgUAKgHQAXgPA4ABQA3ABAXgPQAMgJAQgXQARgZAKgIQAXgVA4gHQA+gIAWgNQAGgDAegYQAWgRARgDQARgDAQAHQARAIAIAOQAJAPgCASQgBASgLANQgLAMgdAOQgpAUgWAIQgjANgeAFIgjAFQgUAEgMAIQgIAGgJAMIgPAUQgVAYgcAPQgdAPggACIgwABQgcACgPALQgHAGgKAOQgLAPgGAFQgLAKgTAHIghALQgOAGgXANQgYAOgLAFQgsAUgugGIgUgCQgLAAgIADQgGADgHAGIgKAMQgkAog5AIQg4AJgvgbQgRAWgbAJQgbAKgbgEQgJgCgSgFQgTgGgJgBQgQgDgNAEQgPAEgIALQgEAFgIAYQgGATgKAGQgFACgGAAQgGAAgHgDg");
    this.shape_1.setTransform(20.537, 16.4456);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#5799DE").s().p("AqHECQgHgIgDgLQgGgZAKgYQAJgZAVgOQAVgOAaABQAbAAAUAPQAHgSASgLQARgKATACQATADAPAPQAOAPACATQArgzAcgRQAjgXA2gJQAagEBGgFQA4gDAggJQANgEAUgJIAggOQAPgHBDgVQA0gQAdgRQAugbAegsIAfguQATgaAUgKQATgJAdgBIAyAAQAigBApgPQAXgHAxgWQAngSAZABQASABAOAJQAPAKADAQQADAPgIAPQgHAOgOAJQgLAHgSAGIgeAJQg6ARg9AkQh5ADgIAIQgHAHgGAPIgKAYQgLAYgoAcQhaA/hGAXQgzAOgZAKQgqAUgWAHQgiAMg+ADQhDADgcAFQg0AKggAbIgXAWQgOANgLAHQgZAPgfgFQgfgFgTgXQgZAQgfgEQgfgDgVgTQgGAFgOAYQgLATgNABIgBAAQgJAAgIgJg");
    this.shape_2.setTransform(19.8748, 16.0006);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#5799DE").s().p("ApaDuQgNgGAFgWQAHgcAVgVQAVgWAcgGQATgFBEAEQA1AEAZgVQAFgEAOgRQAMgOAJgFQASgLAgADQAuAFAIgBQAfgDAjgbQAnghAUgNQA2gjA/ADIApADQAXABAQgIQALgFANgNIAVgXQAggfAtgLIAigGQAUgEAMgHQAKgGAOgPQAPgQAHgFQAPgMAUgFQATgEATAEIAYAIQAPAEAKgCQAKgBAKgHQAFgCAOgLQAjgaAegCQASAAAQAKQARALADARQADANgHAPQgGANgMAKQgJAIgPAHIgbAMQhHAehCAvIAkgoQgIAAgQgIQgPgHgIAAQgNAAgMAOQgMASgHAHQgSATgrAHQg0AJgPAIQgQAIgcAbQgaAbgSAIQgYAMgvgCQg4gCgSAFQgiAHhEA2Qg9AwgrgCIgdgDQgSgCgLABQgRADgXASQgcAVgLAEQgWAKgfgCQgPgCgngHQhIgOghAaIgQANQgGADgFAAQgDAAgDgCg");
    this.shape_3.setTransform(14.9406, 19.0831);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#5799DE").s().p("AmbEtQgIgFgQgQQgOgQgKgFQgOgHghACQgaACgMAFIgOAGQgIADgGgBQgHAAgHgHQgFgGgDgIQgDgKACgWQAFgnALgPQAJgMAPgEQAQgEAKAJQADACAGAHQAGAGAEACQAFADARgDQAygGAoAdQAFgRAPgKQAPgLARABIABAAIAOgDIBKgOQASgDAKgDQAIgKAIgFQAHgFAPgGQARgFAGgEIATgMQALgIAIgDQARgEAmARQAjAQARgKQAFgDAGgGIAJgLQAVgWAzgFQBAgGARgHQAGgYAXgNQAXgPAYAFIARAFQAKACAHgDQAGgDAHgJIAMgQQAIgIARgEQAVgFAHgDQALgGAMgPIAUgZQAMgMAQgCQASgDAIANIAng7IAAgKQAAgHABgDIAGgIQAIgIAFAAQAAAAABAAQAAAAABAAQAAAAAAgBQAAAAAAAAQANACADAMQACAIgFAOQgFANgHABIgEABQgHABgEgDIgDgCQgBAkgDAlQgEAsgXAJQgFACgKABIgPADQgLAEgQAQQgbAYg1AlQgYAQgPAHQgXAKgUgEQAEAJgEAKQgEAKgIAGQgMAJgaAEIhAAGQgmAEgYAKQADAVgVAPQgTAOgXgBQgNAAgfgHQgdgHgQABQgMABgYAFQgXADgGAEQgHAFgMAUQgWAggsAUQgdANg3ANQgaAGgQAAIgEAAQgVAAgOgJgAHwjrQgCgEAAgIIABgRIACgKQADgJAKAAQAJgBAFAKQADAGAAAKIAAAPQgBAHgBACQgEAJgLAAQgKgBgEgJgAI+kOQgIgCgEgGQgGgHABgIIgBABIgFACIgCgBIgFAAIgBgBIgBgBQgEgEADgEQADgDAEAAIAHAAIACAEIAAAFIABgCQABgFAFgDQAEgDAFAAIAAgBQAGABAHAFQAKAHABAJQAAAEgDAGIgGAGQgEACgEAAIgGgBg");
    this.shape_4.setTransform(9.1386, 14.1795);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#5799DE").s().p("An1EGQgSgCgMgNQgLALgLgBQgRgCgHgXQgHgUAEgVQADgVANgRQAPgVAUgBQATgCALAPQAHAOAGAGQAJgNAQgFQAQgFAQAEQAKAEAEgBQAFgBAEgFIAHgJQAOgPAUABQACgIAGgGIABgBIgDgIQgEgNAHgNQAHgLAKAAQABAAABAAQAAAAABgBQABAAAAAAQAAAAAAgBQAOAAAIANQAHALgDAOQgBAGgCAEIACADIAGAHIAAgCQAEgHAIgEQAIgEAIABIAMAFQARALACAQQApggAbgKQAQgGAMADIATAIIAJgCQAKgDARgPIAkgjQAUgVANgDQANgDALAGIAIAGIAJAGQAGACAMgBQANgBAFABQAIACAGAIIACACIAKgEQANgFAGgEQAIgGALgOQALgPAGgGQAKgJAOgDQAOgEANAEQAOAEAEgBQAFgCAHgKQARgWAbgIQAIgCAPgCIAOgDQAHgGAIgFQAVgMAhgFIANgEQAEgCAHgIQAZgcAfgTIAVgMQALgHAHgHIAQgTIAAgBQgHgDgEgJQgIgPAIgOQAIgOAQgDQAMAAAHAKQAHAIgCANQgCAHgEAGIgCADQALADAOANQAUATAEALQALAXgNAWQgLASgQAAIgLgCQgHgCgFAAQgIABgJAHIgPANQgLALgQAIIgTALQgFAEgHAJIgKAPQgMANgSACIgSAAIgRAKQgdAPgQADIgOACQgJACgGACQgKAEgNANIgWAUQgWAQgagDQgPgBgEABQgFACgGAFIgJAJQgMAMgWAMIgmAVIgcAQQgRAIgOgDQgEgBgJgFIgLgFQABAEgBAFQgCAKgOAIQgKAFgJAAQgHABgUgDQgFAEgDABQgJACgHgIIgYgBIgHgBQgJAQgPANQgOANgNABQgHABgJgDQgCAIgGAHQgIAJgNADQgMADgMgDQgRgEgMgNIgEgBQgIgCgFgIQgQACgJgFIgCgBIgGAIQgIAKgMAEQgMAFgNgBIgLgCQgHgCgFABIgPAEQgJAEgGgBQgDAAgIgDQgHgDgEABQgGACgHAJQgJALgKAHQgLAHgNAAIgIgBgAGvikIgIgCQgFgBgFgEQgKgHgFgJQgFgLADgLQADgHAIgFQAIgEAGgBQAMAAAIAIQAHAHAEAMQADAOgFAJQgCAEgGAFIgFADIgCAAIgEAAg");
    this.shape_5.setTransform(9.8658, 15.6772);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#5799DE").s().p("AnDDtQgbgIg0gSIgTgFIgEAGQgHAIgJADQgJADgIgEQgPgIABgdQACghAMgcQAIgUAPgLQASgNAPAJQAGADAHAJQAIAKAEADQAVASAqgTIAfgNQASgGAPACIAXAIQAOAFAJgDQAGgBAJgHIAPgJQAIgEAQAAQAQgBAHgDQAHgCALgIQANgKAFgDQALgFARgBQATAAAJgCQAOgBASgHIAegLQAVgHAzgKQAugJAYgJQATgHAkgVIBhg4QA7giAYgVIAlggQAXgRAUgBQAJgBASADQARADAJAAQAZgBAxggQAugeAcAFQANACALAIQALAIAFAMQAGANgDANQgCAOgKAJQgHAIgNAFIgXAHQgHADg/AiQgpAXgfgCIgTgDQgMgBgHACQgMADgUAUQgXAVgxAdIh1BFQggATgRAHQgTAJgbAHIgkAIQgNALgUANQg2AkgnAIQgRADgqABQgmABgVAFQgXAHgvAdQgrAbgcAEQgJACgLAAQgaAAghgLg");
    this.shape_6.setTransform(16.6609, 15.9814);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#5799DE").s().p("AjxDoQAYgvgYAMIgoAUQgeALgvgLIgegHQgSgDgNAAQgKABgQADIgZAFQghAFgQgRQgUAdgSgBQgPAAgIgRQgHgPADgQQAGgZAVgRQAVgRAZABQAeAEAOgCQAIgBAPgGQAPgHAHgBQASgEAnAPQAlAOATgHQAIgDAMgJQAOgLAGgDQASgKAjADQApADAPgEQAVgFAVgSQAIgIAagcQArgvAnAEQALABAbAKQAZAKAPgCQAIgBAWgJQATgIAMACQAHABAQAHQAOAHAJgBQANAAAUgRIA0gwQAoglAZgLQAUgJAUgBQAWgCARAKQgEgcAggcIAZgUQAPgNAIgLIAJgRQAGgLAGgFQARgPAYAKQAYAJAIAWQANAmgdAxQgcAugzArQggAbhCAqQg3AlggATQgxAdgqASQhEAchJAIQgzAGgRAFQgRAGggASQgbAlhFAAQgpAAg5gOg");
    this.shape_7.setTransform(7.5405, 15.676);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#5799DE").s().p("AoXD/QgUgBgMgPQgKgOgCgTQgBgSAHgTQAFgOALgSIATggIAPgZQAKgOALgGQAbgPAtAUIAZALQAPAFALgEQALgDALgMIATgVQAPgNAVgEQAUgDASAIIAjATQAUAJAOgHQAJgEAHgNIAKgXQAMgVAYgLQAYgLAZAFIATAGQALADAHgBQAHgBAJgFIAPgHQAWgLAaAGQAaAHAPATIAKALQAFAFAGABQAKABAPgNQAoglAzgUQAcgLA6gSQAwgTAWgdQAJgLANgYQANgVAPgIQAIgEAPgDQASgEAGgCQAHgDAggVQAXgPARADQASACALATQALARgDAUQgFAeghAjQg8A/hYA2QhFAphlAsQheApg+AMQgVAFhDAIQg4AHghAKQgVAGhDAcQg2AWgjAFQghAFg0gEIhVgHQgVgBgJAIQgFAEgEAJIgIAOQgJANgRAAIgFgBg");
    this.shape_8.setTransform(7.5017, 18.2193);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.f("#5799DE").s().p("AnOEbIgNgEIgFAAQhNACgLgGQgMgIgBgMQgCgNALgOIAXgXIAPgSQAJgNAGgGQAUgVAVAEQAKABAaASQAVAPAOgFQAHgDAGgHIALgOQAQgRAZgDQAagDATANIAQANQAKAGAIAAQAKAAALgLIARgSQAPgOAZgGQARgDAdgBIBrgBQAVgBAHgEQAHgDAJgJIAOgQQAWgUApgHIBIgIQA5gKA+goQAegUBKg+QAfgZASgHQAPgGAZgDIAqgFQAWgEAQgOQASgPgCgTQANAOABAXQAAAUgJAUQgVAtgtAVQgIAEgPAFIgXAJQgiAOgrAjIhJA8QhDAvhQALQgDAJgIAIQgLALgVAJQg+AbgvADQgfACgHACQgMADgRAKIgdAOQgTAIgkADQgrAEgNADIgbAIQgRAFgLACQgWAEgugDQgPAAg5AIQgaADgUAAQgPAAgMgBgAHXiKQgKgGgGgNQgGgLgDgLQgFgQAEgLQADgIAIgEQAJgFAJADQANADAKAVQAKASAAAMQAAAUgPAIQgFADgFAAQgGAAgFgDgAIajgQgKAAgIgIQgNgLACgPQAAgHAFgHQAEgGAHgDIAJgCQAQgBAJAOQAJANgFAQQgCAGgFAFQgGAGgKAAIgCAAg");
    this.shape_9.setTransform(9.6864, 11.6432);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.f("#5799DE").s().p("AosEfQgMgCgIgKQgJgKgBgNQgDgWAQgaQAFgJAGgGQgFgEgDgGQgDgJACgJQACgIAHgHQAHgGAJgDIAFgBQAQAAAHAPQADAIAAAIIAGABQAOADAHAKQAQgZAqgCIAigBQATAAANgFIAVgJQAOgGAIgCQATgFAnAKQAnAKATgEQARgEATgOIAfgcQAqgkAgAJQAAggAbgYQAcgWAfAFIAXAFQANAEAJAAQAGAAARgFQAPgDAIADQAFAAATALQAOAIAKgEQAFgCAEgGIAIgKQAMgSAagJQARgGAggFQgJgQAIgVQAIgTARgOQAMgJAXgKQAbgNAJgGQAHgEAhgaQAZgTASgGQAJgDAVgDQATgDAKgFIATgJQALgGAJgBQAOgBAMAJQAMAJAFAPQAIAXgNAhQgPAmgdAQQgQAJgRAAQgTAAgOgJQgPAjghAXQgiAXgmABQAFAYgbAUQgIAHgOAGIgZAMQgSAJgbAVQgiAZgKAHQhXA3iKgGIgZAAQgPAAgKAEQgMAEgNAKIgXATQgcAVgeAGQgSADgdgCQgmgCgJAAQgPABgqAIQgjAGgWgBQgYgCgIACQgHACgJAGIgPALQgPAIgSgCQgTgDgMgMQADAMgLAKQgIAIgKAAIgFAAgAGIiUIgIAGQgKAIgTAEQgaAFgGADQASAHASABQAGABAFgBQAFgCAGgGQADgDAGgLQAGgKAFgEIgBAAQgEAAgEACgAnnCNQgFgDgDgGQgCgFAAgHQAAgIAEgHQAGgLANgCQAMgDAKAJQAMAKgDAQQgCAPgPAGQgGACgFAAQgJAAgHgGgACagaQgMgMAGgUIAEgKQAAgGADgGQADgHAGgEQALgIANAIQAMAIAAAOIAAACQACAGAAAHQAAAVgLAJQgIAGgLAAQgMgBgGgHgABEgjQgLgHAAgOQgBgKAGgIQAGgIAJABQABAAABgBQAAAAAAAAQABAAAAAAQAAgBgBAAQAIABAHAFQAHAFACAIQACAIgDAIQgDAIgHAEQgEACgBACQgNAAgGgDgAIcj9QgIgGABgKQACgIAHgFQAEgDAEAAQABAAAAAAQAAAAABAAQAAAAAAAAQAAgBAAAAQALADADAGQADAEAAAGQAAAGgDADQgDAFgFAEIgDAAQgIAAgHgEg");
    this.shape_10.setTransform(9.3007, 13.8917);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.f("#5799DE").s().p("Ao5EKQgVgTgBgaQAAgaAVgUQAVgTAaADIAWAEQANADAIgBIAPgEIAOgGQAVgHAoAKQArAKATgDQAIgCATgHQARgHAKgBQAGAAAQABQANABAIgCQALgCANgKIAWgTQAggbAzgQQAegKBIgNIBugTQArgHAVgKQAPgIASgPIAfgbQAngfBJgfIAKgEIAAgDQAAgLAIgEIAKgDIAAgBQAHABAFAFIAMgIIgBgDQgCgIADgIQACgIAHgEQAHgFAKABIACAAIAKgNQALgLAMgEQAOgGAMAFIABgNQgEACgFABQgIAAgGgDQgPgJABgQQAAgPAOgKQAFgCADAAIADABIABgCQAPABAHALQAEgJAEgHQAMgQATgFQAUgGAPALQAPALABAXQABAUgJAWQgNAjgUAWQAMAFAIALQAJAOgFAOQgEAMgOAIIgbAOQgKAGgUAQQgUAQgLAGQgLAFgSAGIgeAJQgLAEgSALQgUAMgJAEIgRAJQgKAFgGAGQgFAFgMAVQgOAZgaAQQgZAPgdACQgKABgSgBIgbgBQgyABgnAhIgRAOQgLAIgJADQgIADgMgBIgTgCQgxgFgrAVIgYAMQgOAGgKACQgNACgLgFIgJAGQgSAMgkADQguAEgLADIgVAHQgNAEgJABQgOABgZgIQgbgJgNAAQgTAAggAQQgjAQgRABIgFAAQgXAAgTgRg");
    this.shape_11.setTransform(9.3508, 13.0786);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape}]}).to({state: [{t: this.shape_1}]}, 16).to({state: [{t: this.shape_2}]}, 15).to({state: [{t: this.shape_3}]}, 13).to({state: [{t: this.shape_4}]}, 14).to({state: [{t: this.shape_5}]}, 13).to({state: [{t: this.shape_6}]}, 14).to({state: [{t: this.shape_7}]}, 11).to({state: [{t: this.shape_8}]}, 11).to({state: [{t: this.shape_9}]}, 15).to({state: [{t: this.shape_10}]}, 14).to({state: [{t: this.shape_11}]}, 4).wait(1));

    // Layer_1
    this.instance = new lib.Symbol24("synched", 0);
    this.instance.setTransform(-0.2, -0.05, 1.6359, 1.6359, 0, 0, 0, 33.5, 0);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(16).to({y: 1.25}, 0).wait(15).to({
      rotation: -3.2275,
      y: -0.15
    }, 0).wait(13).to({rotation: -7.4737, x: -0.15, y: -1.5}, 0).wait(14).to({
      rotation: 0.5211,
      y: -0.15
    }, 0).wait(13).to({rotation: 3.507, y: 0.5}, 0).wait(14).to({rotation: -1.7238, x: 1.7}, 0).wait(11).to({
      regX: 33.4,
      rotation: -4.4643,
      x: 1.55,
      y: -2.75
    }, 0).wait(11).to({y: -0.8}, 0).wait(15).to({
      regX: 33.5,
      rotation: -9.4093,
      x: 4.15,
      y: -0.9
    }, 0).wait(14).to({regY: 0.1, rotation: -3.6923, x: 4.2, y: -0.85}, 0).wait(4).to({
      regY: 0,
      rotation: 0,
      x: -0.2,
      y: -0.05
    }, 0).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-68.4, -46.4, 154.3, 94.4);


  (lib.Symbol22 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol23();

    this.timeline.addTween(cjs.Tween.get(this.instance).to({x: -7.8}, 38).to({x: -246.1}, 1161).to({_off: true}, 1).wait(45));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-279.1, -46.8, 312.8, 121.7);


  (lib.Symbol21 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Tween5("synched", 0);

    this.instance_1 = new lib.Tween6("synched", 0);
    this.instance_1.setTransform(75.8, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance_1}]}, 779).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true, x: 75.8}, 779).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-177.1, -59.6, 430.1, 119.2);


  (lib.Symbol18 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Tween3("synched", 0);

    this.instance_1 = new lib.Tween4("synched", 0);
    this.instance_1.setTransform(43.6, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance_1}]}, 739).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true, x: 43.6}, 739).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-138.9, -58, 321.5, 116.1);


  (lib.Symbol17 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Tween1("synched", 0);

    this.instance_1 = new lib.Tween2("synched", 0);
    this.instance_1.setTransform(117.35, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance_1}]}, 699).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true, x: 117.35}, 699).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-152.1, -80.2, 421.6, 160.4);


  (lib.Symbol8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Symbol21();
    this.instance.setTransform(-1228.45, -301.5, 1.5356, 1.5356, 0, 0, 0, 0.1, -0.1);

    this.instance_1 = new lib.Symbol21();
    this.instance_1.setTransform(321.65, -272.85, 1.5356, 1.5356, 0, 0, 0, 0.1, -0.1);

    this.instance_2 = new lib.Symbol18();
    this.instance_2.setTransform(-274.2, -303.8, 1.5356, 1.5356);

    this.instance_3 = new lib.Symbol17();
    this.instance_3.setTransform(-822.55, -304.5, 1.5356, 1.5356, 0, 0, 0, -0.1, -0.1);

    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["#FFFFFF", "#00CCFF"], [0, 1], 7.6, -61.7, 7.7, -350.6).s().p("EiIUA1sMAAAhrXMEQpAAAMAAABrXg");
    this.shape.setTransform(-206.575, -86.475);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1500.6, -430.1, 2166.6, 687.3);


  (lib.Symbol3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_210();
    this.instance.setTransform(-102.85, -149, 0.5, 0.5);

    this.instance_1 = new lib.CachedBmp_245();
    this.instance_1.setTransform(-102.45, -147.5, 0.5, 0.5);

    this.instance_2 = new lib.Group_1_4();
    this.instance_2.setTransform(4.95, 94.65, 1, 1, 0, 0, 0, 39.9, 52.8);
    this.instance_2.alpha = 0.1992;
    this.instance_2.compositeOperation = "multiply";

    this.instance_3 = new lib.CachedBmp_208();
    this.instance_3.setTransform(-34.75, 43.55, 0.5, 0.5);

    this.instance_4 = new lib.CachedBmp_207();
    this.instance_4.setTransform(-30.6, 44.15, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(77,126,2,0.439)").s().p("AnVCMQjDg6AAhSQAAhRDDg6QDDg5ESgBQETABDDA5QDDA6AABRQAABSjDA6QjDA5kTAAQkSAAjDg5g");
    this.shape.setTransform(9.675, 142.35);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-102.8, -149, 206, 311.1);


  (lib.Symbol1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Stars("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Tween6copy12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy12("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy12("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("4", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-3.95, -40.25, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(195,108,25,0.659)").s().p("AnXHuQjBjEAAkkQAAkiDJjKQDKjJEPAAQERABC7DJQC7DJAIEuQAIEvjIC3QjHC2kUACIgEAAQkRAAjAjCg");
    this.shape.setTransform(-0.0207, -0.312);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_242();
    this.instance.setTransform(-80.85, -81.7, 0.5, 0.5);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.5);


  (lib.Tween6copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween5copy12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy12("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy12("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy8("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween4copy12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy12("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy12("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy8("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween3copy12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy12("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy12("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy8("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween2copy12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy12("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy12("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy8("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween1copy12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy12("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy12("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy8("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Symbol15copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Symbol12copy2("synched", 0);
    this.instance.setTransform(0, 0.05, 1, 1, 0, 0, 0, 0.7, -0.8);

    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("Ehv7BXZMAAAiuxMDf2AAAMAAACuxgEhjTBJpMDDfgEBMAAAiSVMjD9AAAg");
    this.shape.setTransform(-2.1, 1.525);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-718.4, -557.7, 1432.6999999999998, 1118.5);


  (lib.Symbol8copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Path();
    this.instance.setTransform(6, -3.85, 1, 1, 0, 0, 0, 19.7, 19.7);
    this.instance.compositeOperation = "screen";

    this.instance_1 = new lib.Path_1();
    this.instance_1.setTransform(6, -3.85, 1, 1, 0, 0, 0, 19.7, 19.7);
    this.instance_1.compositeOperation = "multiply";

    this.instance_2 = new lib.CachedBmp_241();
    this.instance_2.setTransform(-30.1, -39.95, 0.5, 0.5);

    this.instance_3 = new lib.Blend();
    this.instance_3.setTransform(-0.5, 1.5, 1, 1, 0, 0, 0, 41.5, 38.5);
    this.instance_3.compositeOperation = "multiply";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-42, -39.9, 83.9, 79.9);


  (lib.Symbol7copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFC000").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACIgEgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape.setTransform(5.8, -5.3833);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#542000").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBAAAAgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape_1.setTransform(5.8, -3.8833);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f().s("#F0EDED").ss(0.1, 1, 1).p("AEbABQAAB0hUBTQhSBTh1AAQh0AAhUhTQhShTAAh0QAAh1BShTQBUhTB0AAQB1AABSBTQBUBTAAB1g");
    this.shape_2.setTransform(6.125, -3.425);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("rgba(0,0,0,0.329)").s().p("AjIDIQhShTAAh0QAAh1BShTQBUhTB0AAQB1AABSBTQBUBTAAB1QAAB0hUBTQhSBTh1AAQh0AAhUhTg");
    this.shape_3.setTransform(6.125, -3.425);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#F3F3E8").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACIgEgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape_4.setTransform(5.8, -5.3833);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("rgba(52,100,196,0.769)").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBAAAAgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape_5.setTransform(5.8, -3.8833);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).to({state: [{t: this.shape_3}, {t: this.shape_2}]}, 1).to({state: [{t: this.shape_5}, {t: this.shape_4}]}, 1).wait(2));

    // Layer 1
    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#FFFFFF").s().p("AgbBfQgEgEAAgFQAAgJALgFQApgTAMgZQgWAAgKgGQgMgGAAgUQAAgPAJgOQAJgPAOAAQARAAALAOQAKANACATIAoAAQgHgQAAgKQAAgOAMgNQANgPAUgBQALAAALAHQAOAGAAALQAAALgJAAQgEAAgIgEQgIgDgFAAQgVABAAAQQAAAKAHAGQAHAFAKAAQAKAAAVgFIAGAAQAOAAAAAKQAAALgMAFQgfANg1gBIgoAAQgNAvg5AXIgHABQgGgBgDgDgAAMgRQgDADAAAEQAAAHAKACIAPABQAAgIgEgFQgEgHgHAAQgEAAgDADgAh0A3QABgMATgCIAegDQAWgFAAgGQACgOgggaQgLgKABgHQAAgGAFgEQAEgDAGAAQAPACARAaQARAZAAAQQAAATgOAKQgIAGgUAGQgNADgPABQgcgBACgPgAilAoQgKgIAAgPQAAgLAFgMQAEgJAFgGQgLgFAAgJQAAgGAFgFQAEgGAGAAQAUABAVARQAVATAAATQAAAVgPALQgMAMgSgBQgQAAgJgHgAiXAHQAAALAMAAQAFgBAFgCQAFgDAAgEQAAgLgTgIQgIAKAAAIgAiQhAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGgAi8hAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGg");
    this.shape_6.setTransform(5.6824, -3.8401, 0.8691, 0.8691);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#330000").s().p("AgbBfQgEgEAAgFQAAgJALgFQApgTAMgZQgWAAgKgGQgMgGAAgUQAAgPAJgOQAJgPAOAAQARAAALAOQAKANACATIAoAAQgHgQAAgKQAAgOAMgNQANgPAUgBQALAAALAHQAOAGAAALQAAALgJAAQgEAAgIgEQgIgDgFAAQgVABAAAQQAAAKAHAGQAHAFAKAAQAKAAAVgFIAGAAQAOAAAAAKQAAALgMAFQgfANg1gBIgoAAQgNAvg5AXIgHABQgGgBgDgDgAAMgRQgDADAAAEQAAAHAKACIAPABQAAgIgEgFQgEgHgHAAQgEAAgDADgAh0A3QABgMATgCIAegDQAWgFAAgGQACgOgggaQgLgKABgHQAAgGAFgEQAEgDAGAAQAPACARAaQARAZAAAQQAAATgOAKQgIAGgUAGQgNADgPABQgcgBACgPgAilAoQgKgIAAgPQAAgLAFgMQAEgJAFgGQgLgFAAgJQAAgGAFgFQAEgGAGAAQAUABAVARQAVATAAATQAAAVgPALQgMAMgSgBQgQAAgJgHgAiXAHQAAALAMAAQAFgBAFgCQAFgDAAgEQAAgLgTgIQgIAKAAAIgAiQhAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGgAi8hAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGg");
    this.shape_7.setTransform(5.6824, -2.8901, 0.8691, 0.8691);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape_7}, {t: this.shape_6}]}, 1).to({state: []}, 1).wait(2));

    // Layer 1
    this.instance = new lib.CachedBmp_240();
    this.instance.setTransform(-25.1, -35.05, 0.2582, 0.2582);

    this.instance_1 = new lib.Pathcopy();
    this.instance_1.setTransform(6.2, -3.4, 1.3907, 1.3907, 0, 0, 0, 19.7, 19.8);
    this.instance_1.compositeOperation = "screen";

    this.instance_2 = new lib.CachedBmp_195();
    this.instance_2.setTransform(-25.4, -35.1, 0.2582, 0.2582);

    this.instance_3 = new lib.Path_1copy();
    this.instance_3.setTransform(6.1, -3.8, 1.4695, 1.4695, 0, 0, 0, 19.7, 19.7);
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_239();
    this.instance_4.setTransform(-25.3, -35.2, 0.2582, 0.2582);

    this.instance_5 = new lib.CachedBmp_238();
    this.instance_5.setTransform(-30.05, -39.9, 0.2582, 0.2582);

    this.instance_6 = new lib.Blendcopy();
    this.instance_6.setTransform(-0.45, 1.55, 1, 1, 0, 0, 0, 41.5, 38.5);
    this.instance_6.compositeOperation = "multiply";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_6}, {t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-41.9, -39.9, 83.9, 80);


  (lib.Symbol4copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.instance = new lib.CachedBmp_237();
    this.instance.setTransform(-48, -33.05, 0.0649, 0.0649);

    this.instance_1 = new lib.Path_24_2();
    this.instance_1.setTransform(-0.2, 13.8, 0.9999, 0.9999, 0, 0, 0, 47.4, 19.4);
    this.instance_1.alpha = 0.3203;
    this.instance_1.compositeOperation = "multiply";

    this.instance_2 = new lib.Path_25_2();
    this.instance_2.setTransform(-25.35, -2.5, 0.9999, 0.9999, 0, 0, 0, 22.7, 14.1);
    this.instance_2.alpha = 0.3203;
    this.instance_2.compositeOperation = "multiply";

    this.instance_3 = new lib.Path_26_2();
    this.instance_3.setTransform(27.95, -0.95, 0.9999, 0.9999, 0, 0, 0, 5.7, 11.7);
    this.instance_3.alpha = 0.3203;
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_191();
    this.instance_4.setTransform(-48.05, -16.6, 0.0649, 0.0649);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-48, -33, 96.2, 66.2);


  (lib.Symbol4_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.instance_2 = new lib.CachedBmp_236();
    this.instance_2.setTransform(-48, -33.05, 0.0659, 0.0659);

    this.instance_3 = new lib.Path_24_1();
    this.instance_3.setTransform(-0.2, 13.8, 0.9999, 0.9999, 0, 0, 0, 47.4, 19.4);
    this.instance_3.alpha = 0.3203;
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.Path_25_1();
    this.instance_4.setTransform(-25.35, -2.5, 0.9999, 0.9999, 0, 0, 0, 22.7, 14.1);
    this.instance_4.alpha = 0.3203;
    this.instance_4.compositeOperation = "multiply";

    this.instance_5 = new lib.Path_26_1();
    this.instance_5.setTransform(27.95, -0.95, 0.9999, 0.9999, 0, 0, 0, 5.7, 11.7);
    this.instance_5.alpha = 0.3203;
    this.instance_5.compositeOperation = "multiply";

    this.instance_6 = new lib.CachedBmp_189();
    this.instance_6.setTransform(-48.05, -16.6, 0.0659, 0.0659);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_6}, {t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-48, -33, 96.2, 66.2);


  (lib.Symbol2_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance_2 = new lib._3_Stars("synched", 0);
    this.instance_2.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_2}]}).to({state: [{t: this.instance_2}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_L("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol473("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));


    // Layer_3
    this.instance_3 = new lib._3_Star_R("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol474("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));


    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol1copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy5("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy9, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy4("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy8, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy3("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy6, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy2("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy3, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Less_1_offcopy12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.CachedBmp_235();
    this.instance.setTransform(-1746.5, -33.05, 0.5, 0.5);

    this.instance_1 = new lib.Path_24copy6();
    this.instance_1.setTransform(-1330.15, 281.45, 8.7466, 6.7404, 0, 0, 0, 47.3, 19.4);
    this.instance_1.alpha = 0.3203;
    this.instance_1.compositeOperation = "multiply";

    this.instance_2 = new lib.Path_25copy6();
    this.instance_2.setTransform(-1555.5, 171.85, 8.7466, 6.7404, 0, 0, 0, 21.9, 14.1);
    this.instance_2.alpha = 0.3203;
    this.instance_2.compositeOperation = "multiply";

    this.instance_3 = new lib.Path_26copy6();
    this.instance_3.setTransform(-1089.25, 181.35, 8.7466, 6.7404, 0, 0, 0, 5.8, 11.5);
    this.instance_3.alpha = 0.3203;
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_181();
    this.instance_4.setTransform(-1747.05, 78.05, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1747, -33, 841.5, 445.3);


  (lib.Less_1_offcopy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.CachedBmp_234();
    this.instance.setTransform(-1746.5, -33.05, 0.5, 0.5);

    this.instance_1 = new lib.Path_24copy5();
    this.instance_1.setTransform(-1330.15, 281.45, 8.7466, 6.7404, 0, 0, 0, 47.3, 19.4);
    this.instance_1.alpha = 0.3203;
    this.instance_1.compositeOperation = "multiply";

    this.instance_2 = new lib.Path_25copy5();
    this.instance_2.setTransform(-1555.5, 171.85, 8.7466, 6.7404, 0, 0, 0, 21.9, 14.1);
    this.instance_2.alpha = 0.3203;
    this.instance_2.compositeOperation = "multiply";

    this.instance_3 = new lib.Path_26copy5();
    this.instance_3.setTransform(-1089.25, 181.35, 8.7466, 6.7404, 0, 0, 0, 5.8, 11.5);
    this.instance_3.alpha = 0.3203;
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_181();
    this.instance_4.setTransform(-1747.05, 78.05, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1747, -33, 841.5, 445.3);


  (lib.Less_1_offcopy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.CachedBmp_233();
    this.instance.setTransform(-1746.5, -33.05, 0.5, 0.5);

    this.instance_1 = new lib.Path_24copy4();
    this.instance_1.setTransform(-1330.15, 281.45, 8.7466, 6.7404, 0, 0, 0, 47.3, 19.4);
    this.instance_1.alpha = 0.3203;
    this.instance_1.compositeOperation = "multiply";

    this.instance_2 = new lib.Path_25copy4();
    this.instance_2.setTransform(-1555.5, 171.85, 8.7466, 6.7404, 0, 0, 0, 21.9, 14.1);
    this.instance_2.alpha = 0.3203;
    this.instance_2.compositeOperation = "multiply";

    this.instance_3 = new lib.Path_26copy4();
    this.instance_3.setTransform(-1089.25, 181.35, 8.7466, 6.7404, 0, 0, 0, 5.8, 11.5);
    this.instance_3.alpha = 0.3203;
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_181();
    this.instance_4.setTransform(-1747.05, 78.05, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1747, -33, 841.5, 445.3);


  (lib.Less_1_offcopy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.CachedBmp_232();
    this.instance.setTransform(-1746.5, -33.05, 0.5, 0.5);

    this.instance_1 = new lib.Path_24copy3();
    this.instance_1.setTransform(-1330.15, 281.45, 8.7466, 6.7404, 0, 0, 0, 47.3, 19.4);
    this.instance_1.alpha = 0.3203;
    this.instance_1.compositeOperation = "multiply";

    this.instance_2 = new lib.Path_25copy3();
    this.instance_2.setTransform(-1555.5, 171.85, 8.7466, 6.7404, 0, 0, 0, 21.9, 14.1);
    this.instance_2.alpha = 0.3203;
    this.instance_2.compositeOperation = "multiply";

    this.instance_3 = new lib.Path_26copy3();
    this.instance_3.setTransform(-1089.25, 181.35, 8.7466, 6.7404, 0, 0, 0, 5.8, 11.5);
    this.instance_3.alpha = 0.3203;
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_181();
    this.instance_4.setTransform(-1747.05, 78.05, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1747, -33, 841.5, 445.3);


  (lib.Less_1_offcopy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol4copy("synched", 0);
    this.instance.setTransform(-1339.35, 171.15, 7.7058, 6.3801);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1709.6, -39.7, 741.3999999999999, 422.5);


  (lib.Less_1_offcopy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol4_1("synched", 0);
    this.instance.setTransform(-1322.9, 185, 7.5861, 5.9819, 0, 0, 0, -0.1, 0.3);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1686.6, -14.5, 729.8, 396.2);


  (lib.Less_1_offcopy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.CachedBmp_231();
    this.instance.setTransform(-1746.5, -35.75, 0.5, 0.5);

    this.instance_1 = new lib.Path_24();
    this.instance_1.setTransform(-1329.2, 278.8, 8.7483, 6.7412, 0, 0, 0, 47.4, 19.4);
    this.instance_1.alpha = 0.3203;
    this.instance_1.compositeOperation = "multiply";

    this.instance_2 = new lib.Path_25();
    this.instance_2.setTransform(-1552.85, 169.15, 8.7483, 6.7412, 0, 0, 0, 22.2, 14.1);
    this.instance_2.alpha = 0.3203;
    this.instance_2.compositeOperation = "multiply";

    this.instance_3 = new lib.Path_26();
    this.instance_3.setTransform(-1086.55, 178.65, 8.7483, 6.7412, 0, 0, 0, 5.8, 11.5);
    this.instance_3.alpha = 0.3203;
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_173();
    this.instance_4.setTransform(-1747.05, 75.35, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1747, -35.7, 841.5, 445.3);


  (lib.Less_1_offcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.CachedBmp_230();
    this.instance.setTransform(-1746.5, -33.05, 0.5, 0.5);

    this.instance_1 = new lib.Path_24copy2();
    this.instance_1.setTransform(-1330.15, 281.45, 8.7466, 6.7404, 0, 0, 0, 47.3, 19.4);
    this.instance_1.alpha = 0.3203;
    this.instance_1.compositeOperation = "multiply";

    this.instance_2 = new lib.Path_25copy2();
    this.instance_2.setTransform(-1555.5, 171.85, 8.7466, 6.7404, 0, 0, 0, 21.9, 14.1);
    this.instance_2.alpha = 0.3203;
    this.instance_2.compositeOperation = "multiply";

    this.instance_3 = new lib.Path_26copy2();
    this.instance_3.setTransform(-1089.25, 181.35, 8.7466, 6.7404, 0, 0, 0, 5.8, 11.5);
    this.instance_3.alpha = 0.3203;
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_181();
    this.instance_4.setTransform(-1747.05, 78.05, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1747, -33, 841.5, 445.3);


  (lib.Less_1_offcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.CachedBmp_229();
    this.instance.setTransform(-1726.05, -63.85, 0.5, 0.5);

    this.instance_1 = new lib.Path_24();
    this.instance_1.setTransform(-1323.15, 236.85, 8.4359, 6.497, 0, 0, 0, 47.4, 19.4);
    this.instance_1.alpha = 0.3203;
    this.instance_1.compositeOperation = "multiply";

    this.instance_2 = new lib.Path_25();
    this.instance_2.setTransform(-1535.1, 130.9, 8.4359, 6.497, 0, 0, 0, 22.7, 14.1);
    this.instance_2.alpha = 0.3203;
    this.instance_2.compositeOperation = "multiply";

    this.instance_3 = new lib.Path_26();
    this.instance_3.setTransform(-1087.45, 140.85, 8.4359, 6.497, 0, 0, 0, 6, 11.6);
    this.instance_3.alpha = 0.3203;
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_169();
    this.instance_4.setTransform(-1726.6, 43.2, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1726.6, -63.8, 811.5999999999999, 427);


  (lib.Less_1_offcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.CachedBmp_228();
    this.instance.setTransform(-1733.8, -17.25, 0.5, 0.5);

    this.instance_1 = new lib.Path_24();
    this.instance_1.setTransform(-1330.8, 286, 8.4377, 6.5007, 0, 0, 0, 47.4, 19.3);
    this.instance_1.alpha = 0.3203;
    this.instance_1.compositeOperation = "multiply";

    this.instance_2 = new lib.Path_25();
    this.instance_2.setTransform(-1542.8, 180.05, 8.4377, 6.5007, 0, 0, 0, 22.7, 14);
    this.instance_2.alpha = 0.3203;
    this.instance_2.compositeOperation = "multiply";

    this.instance_3 = new lib.Path_26();
    this.instance_3.setTransform(-1092.95, 190, 8.4377, 6.5007, 0, 0, 0, 5.9, 11.5);
    this.instance_3.alpha = 0.3203;
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_167();
    this.instance_4.setTransform(-1734.35, 89.85, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1734.3, -17.2, 811.5, 430);


  (lib.Less_1_offcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.CachedBmp_227();
    this.instance.setTransform(-1746.5, -34.4, 0.5, 0.5);

    this.instance_1 = new lib.Path_24copy();
    this.instance_1.setTransform(-1329.2, 280.1, 8.7475, 6.7408, 0, 0, 0, 47.4, 19.4);
    this.instance_1.alpha = 0.3203;
    this.instance_1.compositeOperation = "multiply";

    this.instance_2 = new lib.Path_25copy();
    this.instance_2.setTransform(-1553.75, 170.5, 8.7475, 6.7408, 0, 0, 0, 22.1, 14.1);
    this.instance_2.alpha = 0.3203;
    this.instance_2.compositeOperation = "multiply";

    this.instance_3 = new lib.Path_26copy();
    this.instance_3.setTransform(-1087.9, 180, 8.7475, 6.7408, 0, 0, 0, 5.8, 11.5);
    this.instance_3.alpha = 0.3203;
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_165();
    this.instance_4.setTransform(-1747.05, 76.7, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1747, -34.4, 841.5, 445.29999999999995);


  (lib.Less_1_offcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.CachedBmp_226();
    this.instance.setTransform(-1684.5, -8.3, 0.5, 0.5);

    this.instance_1 = new lib.Path_24();
    this.instance_1.setTransform(-1337.2, 246.9, 7.2858, 5.5137, 0, 0, 0, 47.3, 19.3);
    this.instance_1.alpha = 0.3203;
    this.instance_1.compositeOperation = "multiply";

    this.instance_2 = new lib.Path_25();
    this.instance_2.setTransform(-1521, 157.6, 7.2858, 5.5137, 0, 0, 0, 22.5, 14.1);
    this.instance_2.alpha = 0.3203;
    this.instance_2.compositeOperation = "multiply";

    this.instance_3 = new lib.Path_26();
    this.instance_3.setTransform(-1133.4, 166.05, 7.2858, 5.5137, 0, 0, 0, 5.8, 11.6);
    this.instance_3.alpha = 0.3203;
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_163();
    this.instance_4.setTransform(-1684.95, 82.55, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1684.9, -8.3, 701.0000000000001, 362.8);


  (lib.All_Spacecopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_22
    this.instance = new lib.Symbol7copy2();
    this.instance.setTransform(-0.05, 615.3, 1.9367, 1.9367);
    new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib.Symbol7copy2(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_18
    this.instance_1 = new lib.Symbol15copy("synched", 0);
    this.instance_1.setTransform(1.75, -4.85, 1.2652, 1.3156, 0, 0, 0, -0.1, -0.2);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));
    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.All_Spacecopy, new cjs.Rectangle(-907.1, -738.3, 1812.7, 1471.5), null);


  (lib.Symbol10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_6
    this.instance = new lib.CachedBmp_217();
    this.instance.setTransform(-632.85, -56, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_7
    this.instance_1 = new lib.CachedBmp_219();
    this.instance_1.setTransform(302.95, 89, 0.5, 0.5);

    this.instance_2 = new lib.CachedBmp_218();
    this.instance_2.setTransform(491, 254.35, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_2}, {t: this.instance_1}]}).wait(1));

    // Layer_2
    this.instance_3 = new lib.Symbol4("synched", 0);
    this.instance_3.setTransform(519.2, 619.75, 0.9164, 1.1785, 0, 0, 180, 0.1, 0.5);

    this.instance_4 = new lib.Symbol4("synched", 0);
    this.instance_4.setTransform(587.35, 617.8, 0.4791, 0.6161, 0, 0, 180, 0.2, 0.3);

    this.instance_5 = new lib.Symbol3("synched", 0);
    this.instance_5.setTransform(561.2, 540.05, 0.7692, 0.855, 0, 0, 0, -0.2, 0.6);

    this.instance_6 = new lib.CachedBmp_249();
    this.instance_6.setTransform(-193.7, 269.35, 0.5, 0.5);

    this.instance_7 = new lib.Symbol4("synched", 0);
    this.instance_7.setTransform(-69.85, 560.4, 0.4703, 0.9714, 0, 0, 180, 0.1, 0.5);

    this.instance_8 = new lib.Symbol4("synched", 0);
    this.instance_8.setTransform(120.95, 279.95, 0.7554, 0.9714, 0, 0, 180, 0.1, 0.5);

    this.instance_9 = new lib.Symbol4("synched", 0);
    this.instance_9.setTransform(574.85, 258.9, 0.7554, 0.9714, 0, 0, 180, 0.1, 0.5);

    this.instance_10 = new lib.Symbol3("synched", 0);
    this.instance_10.setTransform(539.55, 184.95, 0.6967, 0.6332, 0, 0, 0, -0.3, 0.4);

    this.instance_11 = new lib.Symbol3("synched", 0);
    this.instance_11.setTransform(589.75, 97.8, 0.4915, 0.539, 0, 0, 0, -0.3, 0.5);

    this.instance_12 = new lib.Symbol3("synched", 0);
    this.instance_12.setTransform(465.7, 97.75, 0.4915, 0.539, 0, 0, 0, -0.2, 0.5);

    this.instance_13 = new lib.Symbol7("synched", 0);
    this.instance_13.setTransform(11.15, 84.65, 0.6471, 0.6471, 0, 0, 180, 0.1, 0);

    this.instance_14 = new lib.Symbol7("synched", 0);
    this.instance_14.setTransform(-50.75, 67.25, 0.924, 0.924, 0, 0, 180, 0.1, 0.1);

    this.instance_15 = new lib.Symbol7("synched", 0);
    this.instance_15.setTransform(-112.5, 69.35, 0.6471, 0.6471, 0, 0, 180, 0.1, 0);

    this.instance_16 = new lib.Symbol7("synched", 0);
    this.instance_16.setTransform(-552.95, -3.05, 0.8654, 0.8654, 0, 0, 180);

    this.instance_17 = new lib.Symbol7("synched", 0);
    this.instance_17.setTransform(-621, 427.75, 1.1411, 1.1411, 0, 0, 180);

    this.instance_18 = new lib.Symbol7("synched", 0);
    this.instance_18.setTransform(-534.5, 389.7, 1.1411, 1.1411, 0, 0, 180);

    this.instance_19 = new lib.Symbol7("synched", 0);
    this.instance_19.setTransform(-448.5, 389.7, 1.1411, 1.1411, 0, 0, 180);

    this.instance_20 = new lib.Symbol7("synched", 0);
    this.instance_20.setTransform(-368.85, 353.1, 1.1411, 1.1411, 0, 0, 180);

    this.instance_21 = new lib.Symbol7("synched", 0);
    this.instance_21.setTransform(-434, 284.25, 1.1411, 1.1411, 0, 0, 180);

    this.instance_22 = new lib.Symbol7("synched", 0);
    this.instance_22.setTransform(-521.6, 324.1, 1.1411, 1.1411, 0, 0, 180);

    this.instance_23 = new lib.Symbol7("synched", 0);
    this.instance_23.setTransform(-614, 260.8, 1.1411, 1.1411, 0, 0, 180);

    this.instance_24 = new lib.Symbol6("synched", 0);
    this.instance_24.setTransform(532, 11.05, 0.6041, 0.6041, 0, 0, 0, -0.1, 0.1);

    this.instance_25 = new lib.Symbol2("synched", 0);
    this.instance_25.setTransform(572.65, -31.15, 1.0337, 1.0337, 0, 0, 180);

    this.instance_26 = new lib.Symbol2("synched", 0);
    this.instance_26.setTransform(-534.5, 0.85, 1.2336, 1.2336, 0, 0, 180, 0.1, 0.1);

    this.instance_27 = new lib.Symbol5("synched", 0);
    this.instance_27.setTransform(200.05, 132.7, 1.1375, 0.7869, 0, -8.1983, 171.8028, 0.2, 0.4);

    this.instance_28 = new lib.Symbol5("synched", 0);
    this.instance_28.setTransform(252.8, 113.6, 1.1375, 1.0859, 0, -8.1976, 171.8028, 0.1, 0.5);

    this.instance_29 = new lib.Symbol5("synched", 0);
    this.instance_29.setTransform(246.5, 125.65, 1.1375, 0.7869, 0, -8.1983, 171.8028, 0.2, 0.4);

    this.instance_30 = new lib.Symbol5("synched", 0);
    this.instance_30.setTransform(279.75, 102.35, 1.1375, 0.7869, 0, -8.1983, 171.8028, 0.2, 0.4);

    this.instance_31 = new lib.Symbol5("synched", 0);
    this.instance_31.setTransform(166, 119.15, 0.6691, 1.051, 0, 6.7991, -173.203, 0.5, 0.3);

    this.instance_32 = new lib.Symbol5("synched", 0);
    this.instance_32.setTransform(195.65, 74.6, 0.8096, 1.8762, 0, -8.1926, 171.8116);

    this.instance_33 = new lib.Symbol5("synched", 0);
    this.instance_33.setTransform(86.75, 118.7, 0.9985, 0.9985, 0, 6.8004, -173.1996, 0.1, 0.1);

    this.instance_34 = new lib.Symbol5("synched", 0);
    this.instance_34.setTransform(130.6, 99.4, 0.6691, 1.3855, 0, 6.8022, -173.203, 0.3, 0.3);

    this.instance_35 = new lib.Symbol5("synched", 0);
    this.instance_35.setTransform(85.55, 82.65, 0.4022, 0.8174, 0, 6.7981, -173.209, 0.5, 0.3);

    this.instance_36 = new lib.Symbol5("synched", 0);
    this.instance_36.setTransform(-41.7, 121.55, 1.2681, 0.9984, 0, 6.8094, -173.1922, 0.4, 0.3);

    this.instance_37 = new lib.Symbol5("synched", 0);
    this.instance_37.setTransform(22.3, 96, 0.9985, 0.9985, 0, 6.8004, -173.1996, 0.1, 0.1);

    this.instance_38 = new lib.CachedBmp_248();
    this.instance_38.setTransform(65.55, 422.8, 0.5, 0.5);

    this.instance_39 = new lib.Symbol3("synched", 0);
    this.instance_39.setTransform(269.95, 480.65, 0.7113, 0.7269, 0, 0, 0, -0.1, 0.6);

    this.instance_40 = new lib.CachedBmp_221();
    this.instance_40.setTransform(133.95, 598.05, 0.5, 0.5);

    this.instance_41 = new lib.CachedBmp_220();
    this.instance_41.setTransform(-231.4, -257.25, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_41}, {t: this.instance_40}, {t: this.instance_39}, {t: this.instance_38}, {t: this.instance_37}, {t: this.instance_36}, {t: this.instance_35}, {t: this.instance_34}, {t: this.instance_33}, {t: this.instance_32}, {t: this.instance_31}, {t: this.instance_30}, {t: this.instance_29}, {t: this.instance_28}, {t: this.instance_27}, {t: this.instance_26}, {t: this.instance_25}, {t: this.instance_24}, {t: this.instance_23}, {t: this.instance_22}, {t: this.instance_21}, {t: this.instance_20}, {t: this.instance_19}, {t: this.instance_18}, {t: this.instance_17}, {t: this.instance_16}, {t: this.instance_15}, {t: this.instance_14}, {t: this.instance_13}, {t: this.instance_12}, {t: this.instance_11}, {t: this.instance_10}, {t: this.instance_9}, {t: this.instance_8}, {t: this.instance_7}, {t: this.instance_6}, {t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}]}).wait(1));

    // Layer_5
    this.instance_42 = new lib.CachedBmp_224();
    this.instance_42.setTransform(-499.75, -0.9, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_42).wait(1));

    // Layer_3
    this.instance_43 = new lib.CachedBmp_225();
    this.instance_43.setTransform(-658.5, -108.85, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_43).wait(1));

    // Layer_4
    this.instance_44 = new lib.Symbol5("synched", 0);
    this.instance_44.setTransform(586.4, -63.05, 0.9995, 1.0715, 0, -10.5378, -11.4966, 0.1, 0.3);

    this.instance_45 = new lib.Symbol5("synched", 0);
    this.instance_45.setTransform(655.25, -69.25, 0.8109, 2.0127, 0, 3.431, 4.5851, 0.1, 0);

    this.instance_46 = new lib.Symbol5("synched", 0);
    this.instance_46.setTransform(-559.55, 50.55, 0.9995, 1.0715, 0, -10.5378, -11.4966, 0.2, 0.3);

    this.instance_47 = new lib.Symbol5("synched", 0);
    this.instance_47.setTransform(-601.6, 37, 1.4448, 1.0715, 0, -10.5378, -11.4999, 0.2, 0.5);

    this.instance_48 = new lib.Symbol5("synched", 0);
    this.instance_48.setTransform(-526.45, 18.6, 0.6698, 1.1278, 0, -10.5334, -11.4963, 0.4, 0.4);

    this.instance_49 = new lib.Symbol5("synched", 0);
    this.instance_49.setTransform(-601.15, -12.75, 0.8109, 2.0127, 0, 3.431, 4.5851, 0.1, 0);

    this.instance_50 = new lib.Symbol5("synched", 0);
    this.instance_50.setTransform(-447.45, 12.4, 0.9995, 1.0715, 0, -10.5378, -11.4966, 0.1, 0.3);

    this.instance_51 = new lib.Symbol5("synched", 0);
    this.instance_51.setTransform(-492.8, -5, 0.6698, 1.4867, 0, -10.5382, -11.4963, 0.1, 0.4);

    this.instance_52 = new lib.Symbol5("synched", 0);
    this.instance_52.setTransform(-449.25, -26.3, 0.4026, 0.8771, 0, -10.5317, -11.4896, -0.1, 0.5);

    this.instance_53 = new lib.Symbol5("synched", 0);
    this.instance_53.setTransform(-319.45, 5.75, 1.2701, 1.071, 0, 3.4258, 4.588, 0.1, 0.3);

    this.instance_54 = new lib.Symbol5("synched", 0);
    this.instance_54.setTransform(-385.35, -16.7, 0.9995, 1.0715, 0, -10.5378, -11.4966, -0.3, 0.1);

    this.instance_55 = new lib.Symbol7("synched", 0);
    this.instance_55.setTransform(-171.15, -23.5, 0.6405, 0.6405, 0, 0, 180, 0.1, 0.1);

    this.instance_56 = new lib.Symbol7("synched", 0);
    this.instance_56.setTransform(50.7, 16.15, 0.6405, 0.6405, 0, 0, 180, 0.1, 0.1);

    this.instance_57 = new lib.Symbol7("synched", 0);
    this.instance_57.setTransform(-98.9, -51.95, 0.6405, 0.6405, 0, 0, 180, 0.1, 0.1);

    this.instance_58 = new lib.Symbol4("synched", 0);
    this.instance_58.setTransform(240.05, 42.15, 0.4791, 0.6161, 0, 0, 180, 0.3, 0.4);

    this.instance_59 = new lib.Symbol7("synched", 0);
    this.instance_59.setTransform(411.6, -4.1, 0.6405, 0.6405, 0, 0, 180, 0.1, 0.1);

    this.instance_60 = new lib.Symbol7("synched", 0);
    this.instance_60.setTransform(503.85, -91.05, 0.8626, 0.8626, 0, 0, 180);

    this.instance_61 = new lib.Symbol7("synched", 0);
    this.instance_61.setTransform(454.7, -28.3, 0.8487, 0.8487, 0, 0, 180);

    this.instance_62 = new lib.Symbol7("synched", 0);
    this.instance_62.setTransform(328.9, -2.3, 0.8487, 0.8487, 0, 0, 180, 0.1, 0);

    this.instance_63 = new lib.Symbol7("synched", 0);
    this.instance_63.setTransform(268.25, 39.4, 0.8487, 0.8487, 0, 0, 180, 0.2, 0.1);

    this.instance_64 = new lib.Symbol3("synched", 0);
    this.instance_64.setTransform(357.45, -10.1, 0.5572, 0.5572, 0, 0, 180, 0, 0.1);

    this.instance_65 = new lib.Symbol3("synched", 0);
    this.instance_65.setTransform(269.8, -20.55, 0.8519, 0.9343, 0, 0, 0, -0.1, 0.2);

    this.instance_66 = new lib.Symbol7("synched", 0);
    this.instance_66.setTransform(-270.8, -93, 0.8487, 0.8487, 0, 0, 180);

    this.instance_67 = new lib.Symbol7("synched", 0);
    this.instance_67.setTransform(-491.75, -80.05, 0.8487, 0.8487, 0, 0, 180, 0.1, 0.1);

    this.instance_68 = new lib.Symbol7("synched", 0);
    this.instance_68.setTransform(-414.7, -111.95, 0.8487, 0.8487, 0, 0, 180, 0.1, 0.1);

    this.instance_69 = new lib.Symbol7("synched", 0);
    this.instance_69.setTransform(-364.2, -71.5, 0.8487, 0.8487, 0, 0, 180, 0.1, 0.1);

    this.instance_70 = new lib.Symbol5("synched", 0);
    this.instance_70.setTransform(-522.85, 14.8, 0.9994, 1.0714, 0, -10.5363, -11.4942, 0.1, 0.4);

    this.instance_71 = new lib.Symbol5("synched", 0);
    this.instance_71.setTransform(-564.8, 1.3, 1.4447, 1.0714, 0, -10.5363, -11.4984, 0.2, 0.8);

    this.instance_72 = new lib.Symbol5("synched", 0);
    this.instance_72.setTransform(-489.85, -17.15, 0.6697, 1.1277, 0, -10.5327, -11.4922, 0.2, 0.3);

    this.instance_73 = new lib.Symbol5("synched", 0);
    this.instance_73.setTransform(-522.7, -62.8, 0.8108, 2.0126, 0, 3.4278, 4.5811, 0.1, 0);

    this.instance_74 = new lib.Symbol5("synched", 0);
    this.instance_74.setTransform(-410.7, -23.3, 0.9994, 1.0714, 0, -10.5363, -11.4942, 0.1, 0.2);

    this.instance_75 = new lib.Symbol5("synched", 0);
    this.instance_75.setTransform(-456.1, -40.65, 0.6697, 1.4867, 0, -10.5388, -11.4922, 0.1, 0.5);

    this.instance_76 = new lib.Symbol5("synched", 0);
    this.instance_76.setTransform(-412.5, -61.9, 0.4026, 0.8771, 0, -10.5305, -11.4829, -0.1, 0.5);

    this.instance_77 = new lib.Symbol5("synched", 0);
    this.instance_77.setTransform(-282.7, -29.85, 1.27, 1.0709, 0, 3.4212, 4.5856, 0.1, 0.3);

    this.instance_78 = new lib.Symbol5("synched", 0);
    this.instance_78.setTransform(-348.6, -52.5, 0.9994, 1.0714, 0, -10.5363, -11.4942, -0.3, -0.1);

    this.instance_79 = new lib.Symbol2("synched", 0);
    this.instance_79.setTransform(5.75, 81.1, 1.2128, 1.2128, 0, 0, 180);

    this.instance_80 = new lib.Symbol5("synched", 0);
    this.instance_80.setTransform(535.75, 53.3, 0.9986, 0.9986, 0, 14.9982, -165.0018, 0.1, 0.1);

    this.instance_81 = new lib.Symbol7("synched", 0);
    this.instance_81.setTransform(186.35, -45.95, 1.1411, 1.1411, 0, 0, 180);

    this.instance_82 = new lib.Symbol7("synched", 0);
    this.instance_82.setTransform(112.65, 10.5, 0.6405, 0.6405, 0, 0, 180, 0.1, 0.1);

    this.instance_83 = new lib.Symbol5("synched", 0);
    this.instance_83.setTransform(-304.45, -22.05, 0.9995, 1.0715, 0, -6.3356, -7.2978, 0.1, 0);

    this.instance_84 = new lib.Symbol5("synched", 0);
    this.instance_84.setTransform(-369.55, -24.15, 1.4448, 1.0715, 0, -6.3356, -7.3009, 0.1, 0.3);

    this.instance_85 = new lib.Symbol5("synched", 0);
    this.instance_85.setTransform(-271.3, -21.45, 0.6699, 1.1279, 0, -6.3341, -7.2969, 0.4, 0.3);

    this.instance_86 = new lib.Symbol5("synched", 0);
    this.instance_86.setTransform(-326.75, -106.05, 0.8109, 2.0128, 0, 7.6339, 8.7879, -0.1, 0.1);

    this.instance_87 = new lib.Symbol5("synched", 0);
    this.instance_87.setTransform(-168.75, -21.65, 0.9995, 1.0715, 0, -6.3356, -7.2978, 0.2, 0.4);

    this.instance_88 = new lib.Symbol5("synched", 0);
    this.instance_88.setTransform(-235.8, -42.65, 0.6699, 1.4868, 0, -6.3369, -7.2969, 0.2, 0.2);

    this.instance_89 = new lib.Symbol5("synched", 0);
    this.instance_89.setTransform(-190.9, -60.45, 0.4027, 0.8772, 0, -6.3335, -7.2909, 0, 0.6);

    this.instance_90 = new lib.Symbol5("synched", 0);
    this.instance_90.setTransform(-63.65, 59.45, 1.2701, 1.0711, 0, 7.6318, 8.7895, 0.2, 0.3);

    this.instance_91 = new lib.Symbol5("synched", 0);
    this.instance_91.setTransform(-123.8, 7.9, 0.9995, 1.0715, 0, -6.3356, -7.2978);

    this.timeline.addTween(cjs.Tween.get({}).to({
      state: [{t: this.instance_91}, {t: this.instance_90}, {t: this.instance_89}, {t: this.instance_88}, {t: this.instance_87}, {t: this.instance_86}, {t: this.instance_85}, {t: this.instance_84}, {t: this.instance_83}, {t: this.instance_82}, {t: this.instance_81}, {t: this.instance_80}, {t: this.instance_79}, {t: this.instance_78}, {t: this.instance_77}, {t: this.instance_76}, {t: this.instance_75}, {t: this.instance_74}, {t: this.instance_73}, {t: this.instance_72}, {t: this.instance_71}, {t: this.instance_70}, {t: this.instance_69}, {t: this.instance_68}, {t: this.instance_67}, {t: this.instance_66}, {t: this.instance_65}, {t: this.instance_64}, {t: this.instance_63}, {t: this.instance_62}, {t: this.instance_61}, {t: this.instance_60}, {t: this.instance_59}, {t: this.instance_58}, {t: this.instance_57}, {t: this.instance_56}, {t: this.instance_55}, {t: this.instance_54}, {t: this.instance_53}, {t: this.instance_52}, {t: this.instance_51}, {t: this.instance_50}, {t: this.instance_49}, {t: this.instance_48}, {t: this.instance_47}, {t: this.instance_46}, {t: this.instance_45}, {t: this.instance_44}]
    }).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-698.1, -257.2, 1393.1, 1096.9);


  (lib.Symbol1_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#F3F3E8").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACIgEgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape.setTransform(5.8, -5.3833);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(52,100,196,0.769)").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBAAAAgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape_1.setTransform(5.8, -3.8833);

    this.timeline.addTween(cjs.Tween.get({}).to({
      state: [{
        t: this.shape_1,
        p: {scaleX: 1, scaleY: 1, x: 5.8, y: -3.8833}
      }, {t: this.shape, p: {scaleX: 1, scaleY: 1, x: 5.8, y: -5.3833}}]
    }).to({
      state: [{t: this.shape_1, p: {scaleX: 0.6149, scaleY: 0.6149, x: 5.8163, y: -3.4378}}, {
        t: this.shape,
        p: {scaleX: 0.6149, scaleY: 0.6149, x: 5.8161, y: -4.36}
      }]
    }, 1).to({
      state: [{t: this.shape_1, p: {scaleX: 1, scaleY: 1, x: 5.8, y: -3.8833}}, {
        t: this.shape,
        p: {scaleX: 1, scaleY: 1, x: 5.8, y: -5.3833}
      }]
    }, 1).wait(2));

    // Layer_2
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f().s("#00BFEA").ss(2, 1, 1).p("AB3AAQAAAxgjAjQgjAjgxAAQgwAAgjgjQgjgjAAgxQAAgwAjgjQAjgjAwAAQAxAAAjAjQAjAjAAAwg");
    this.shape_2.setTransform(6.075, -3.425);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#3F6285").s().p("AhTBUQgjgjAAgxQAAgwAjgjQAjgjAwAAQAxAAAjAjQAjAjAAAwQAAAxgjAjQgjAjgxAAQgwAAgjgjg");
    this.shape_3.setTransform(6.075, -3.425);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape_3}, {t: this.shape_2}]}, 1).to({state: []}, 1).wait(2));

    // Layer 1
    this.instance_1 = new lib.Path();
    this.instance_1.setTransform(6, -3.85, 1, 1, 0, 0, 0, 19.7, 19.7);
    this.instance_1.compositeOperation = "screen";

    this.instance_2 = new lib.Path_1();
    this.instance_2.setTransform(6, -3.85, 1, 1, 0, 0, 0, 19.7, 19.7);
    this.instance_2.compositeOperation = "multiply";

    this.instance_3 = new lib.CachedBmp_243();
    this.instance_3.setTransform(-30.1, -39.95, 0.5, 0.5);

    this.instance_4 = new lib.Blend();
    this.instance_4.setTransform(-0.5, 1.5, 1, 1, 0, 0, 0, 41.5, 38.5);
    this.instance_4.compositeOperation = "multiply";

    this.instance_5 = new lib.CachedBmp_244();
    this.instance_5.setTransform(-30.1, -39.95, 0.5, 0.5);

    this.instance_6 = new lib.Symbol8copy("synched", 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}]}).to({state: [{t: this.instance_4}, {t: this.instance_5}, {t: this.instance_2}, {t: this.instance_1}]}, 2).to({state: [{t: this.instance_6}]}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-42, -39.9, 83.9, 79.9);


  (lib.Symbol2copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Starscopy5("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1copy9();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_Lcopy5("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol483("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));


    // Layer_3
    this.instance_3 = new lib._3_Star_Rcopy5("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol484("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));


    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol2copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Starscopy4("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1copy8();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_Lcopy4("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol481("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));


    // Layer_3
    this.instance_3 = new lib._3_Star_Rcopy4("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol482("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));


    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol2copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Starscopy3("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1copy6();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_Lcopy3("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol479("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));


    // Layer_3
    this.instance_3 = new lib._3_Star_Rcopy3("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol480("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));


    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol2copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Starscopy2("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1copy3();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_Lcopy2("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol477("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));


    // Layer_3
    this.instance_3 = new lib._3_Star_Rcopy2("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol478("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));


    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol2copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Starscopy("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1copy();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_Lcopy("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol475("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));


    // Layer_3
    this.instance_3 = new lib._3_Star_Rcopy("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol476("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));


    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol1copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy7("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy7("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.instance_2 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy7, new cjs.Rectangle(-133.3, -172.2, 277.9, 344.6), null);


  (lib.Symbol1copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy6("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy6("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.instance_2 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy5, new cjs.Rectangle(-156.3, -172.2, 320.5, 344.6), null);


  (lib.Symbol1copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy3("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy3("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.instance_2 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy4, new cjs.Rectangle(-151.4, -172.2, 309, 344.6), null);


  (lib.Symbol1copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.instance_2 = new lib.C_1_ONcopy("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy2, new cjs.Rectangle(-132.6, -172.2, 266.9, 344.6), null);


  (lib.OFFcopy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy12("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy12("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy12("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy12("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy12("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy12("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy12("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2copy5();
    this.instance_7.setTransform(78.6, -199.95, 0.3245, 0.3245, 0, 0, 0, 2.3, 44.1);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(7));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 387.5);


  (lib.OFFcopy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy11("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy11("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy11("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy11("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy11("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy11("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy11("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2copy4();
    this.instance_7.setTransform(78.6, -199.95, 0.3245, 0.3245, 0, 0, 0, 2.3, 44.1);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(7));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 387.5);


  (lib.OFFcopy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy10("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy10("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy10("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy10("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy10("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy10("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy10("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2copy3();
    this.instance_7.setTransform(78.6, -199.95, 0.3245, 0.3245, 0, 0, 0, 2.3, 44.1);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(7));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 387.5);


  (lib.OFFcopy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy9("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy6("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy6("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy6("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy6("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy6("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy6("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2copy2();
    this.instance_7.setTransform(78.6, -199.95, 0.3245, 0.3245, 0, 0, 0, 2.3, 44.1);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(7));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 387.5);


  (lib.OFFcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy5("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy3("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy3("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy3("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy3("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy3("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy3("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2copy();
    this.instance_7.setTransform(78.6, -199.95, 0.3245, 0.3245, 0, 0, 0, 2.3, 44.1);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(7));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 387.5);


  (lib.OFFcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(2));

    // K
    this.instance = new lib.Locked_1copy6("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy5();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(142,26,26,0.439)").s().p("AlaMtIgWgBIgXgCIhhgHQiJgHhPgIQjDgVjZhBQgdgSgrgRIiig3QhHgYg5gkIgTgJQg2gdgQibQgIixgFg8QgNjSAAghQgEhgAChQQgCgsAEgkQgCgaANgaQAUgpAwgeIDNiCQBNgwAYgNQA4gZA+gLIClgUQBrgLA5gIQAdgFAugQQAygPAYgFQBCgNBogFIC2gDQA2AABagOQAwgDBPAIIB7AMICeAMQBbAHBDgBQAUAAAsgEQAnACAYAHQANADAhAQQAaAMAUAFQCtAfBlgCQAmARA1gDQA/gEAhALQAUAHAWANIAnAaQAhATBQATQBCAWAwAjQApAiAlAyIAzBFQAYAnAEAjQAHAugPAqQAaDVguGrQgLBMgyAkIhcA6Qg1AjgnAUQktCZlkA0QhNAOhTAIQhuAMiWAAQjzAAlcgfg");
    this.shape.setTransform(83.3935, 80.6922);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy6("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy7("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy7("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy7("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy7("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy7("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy7("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_4}]}, 1).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 418.20000000000005);


  (lib.OFFcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy2("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2_1();
    this.instance_7.setTransform(78.6, -199.95, 0.3245, 0.3245, 0, 0, 0, 2.3, 44.1);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(7));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 387);


  (lib.OFFcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy4("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).wait(2));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(142,26,26,0.439)").s().p("AlQMVQgJgCgMAAIgVgCIhegGQiEgHhOgIQi8gUjQhAQgdgQgpgQIidg3QhGgXg3giIgSgKQgzgcgQiVQgIisgFg7QgNjNAAgfQgDhdAChOQgCgqADgkQgBgZANgZQATgpAvgbIDFh/QBKgvAZgMQA1gZA8gJICggWQBmgKA3gIQAdgFAsgOQAxgPAXgFQBBgMBjgFICxgDQA0AABYgNQAtgFBLAKIB5ALICYAMQBYAGBBgBQATAAArgDQAlABAXAHQANADAgAPQAZALATAFQCnAfBjgBQAkAQAzgDQA9gDAhAJQATAHAVANIAmAZQAgASBNAUQA/AVAvAhQAnAiAkAwIAyBDQAXAmADAhQAIAugOAnQAYDQgsGfQgLBJgxAjQg8AmgcATQg0AhglAUQkjCVlYAxQhLAOhRAIQhsAMiUAAQjoAAlPgeg");
    this.shape.setTransform(85.1309, 64.4513);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

    // Layer_3
    this.instance_1 = new lib.Less_1_offcopy4("synched", 0);
    this.instance_1.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(20));

    // C
    this.instance_2 = new lib.Tween1copy5("synched", 0);
    this.instance_2.setTransform(78.7, 76.2);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween2copy5("synched", 0);
    this.instance_3.setTransform(78.7, -24.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween3copy5("synched", 0);
    this.instance_4.setTransform(78.7, -127.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween4copy5("synched", 0);
    this.instance_5.setTransform(78.6, -71.2);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween5copy5("synched", 0);
    this.instance_6.setTransform(78.7, -96);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween6copy5("synched", 0);
    this.instance_7.setTransform(78.7, -120.65);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).wait(8));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -120.65
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 418.20000000000005);


  (lib.OFFcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(2));

    // K
    this.instance = new lib.Locked_1copy3("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy4();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(142,26,26,0.439)").s().p("AlQMNQgKgCgLAAIgWgBIhegHQiEgGhOgIQi9gUjQg/QgdgQgpgRQhwgngugOQhGgXg3giIgSgKQgzgcgQiTQgIirgFg6QgNjLAAgeQgDhdABhMQgBgrADgjQgCgZANgYQAUgpAvgbIDFh+QBLgvAYgLQA2gYA8gKICggVQBmgKA4gIQAdgFAsgPQAxgOAXgFQBBgLBjgFICxgEQA1AABXgNQAugEBLAJIB5AMQA0ADBlAIQBYAHBBgCQATAAArgDQAlABAXAHQANADAhAPQAYALAUAFQCnAfBjgCQAkAQAzgDQA+gDAgAKQAUAGAVANIAlAZQAhARBMAUQBAAVAvAgQAnAjAkAvIAyBCQAXAmADAgQAJAugPAnQAYDOgsGaQgLBJgxAiQg8AmgcATQg0AhglATQkkCUlZAxQhLANhRAIQhsAMiVAAQjpAAlOgeg");
    this.shape.setTransform(81.9812, 83.2571);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy3("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy4("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy4("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy4("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy4("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy4("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy4("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({
      _off: true,
      y: -24.7
    }, 2).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(2).to({_off: false}, 2).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 418.20000000000005);


  (lib.OFF = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy8("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).wait(2));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(142,26,26,0.439)").s().p("AkvMHIgTgBIgUgCIhVgHQh3gGhGgIQirgUi9g+QgZgRgmgQIiOg1Qg+gXgygiIgRgIQgugcgPiUQgGiogFg6QgLjIAAggQgEhbAChMQgCgqAEgjQgCgYALgZQASgnAqgcICzh9QBEguAVgLQAwgZA3gKICQgTQBdgKAygIQAagFAogPQAsgPAVgFQA6gLBagFICggDQAvAABPgNQAqgEBEAIIBsAMICLALQBPAHA6gCQASAAAngDQAhACAVAGQAMAEAdAOQAWAMASAFQCYAdBYgBQAhAQAugDQA3gEAdAKQASAHATANIAiAYQAdATBGASQA6AVAqAhQAjAgAhAwIAtBCQAVAlADAhQAGAsgNAoQAXDLgoGXQgKBIgsAjIhQA3QgvAhgiAUQkHCSk3AxQhEANhJAIQhgALiDAAQjVAAkwgdg");
    this.shape.setTransform(77.2358, 72.5368);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

    // Layer_3
    this.instance_1 = new lib.Less_1_offcopy8("synched", 0);
    this.instance_1.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(20));

    // C
    this.instance_2 = new lib.Tween1copy9("synched", 0);
    this.instance_2.setTransform(78.7, 76.2);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween2copy9("synched", 0);
    this.instance_3.setTransform(78.7, -24.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween3copy9("synched", 0);
    this.instance_4.setTransform(78.7, -127.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween4copy9("synched", 0);
    this.instance_5.setTransform(78.6, -71.2);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween5copy9("synched", 0);
    this.instance_6.setTransform(78.7, -96);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween6copy9("synched", 0);
    this.instance_7.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_4}]}, 1).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).wait(8));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(3).to({_off: false}, 1).to({y: -76.2}, 1).to({
      _off: true,
      y: -127.7
    }, 1).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(5).to({_off: false}, 1).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 418.20000000000005);


  (lib.B_1copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(2));

    // K
    this.instance = new lib.Locked_1copy7("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy7();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(142,26,26,0.439)").s().p("AksLZIgTgCIgUgCIhUgGQh2gGhFgHQiqgTi7g6QgZgQglgPIiNgyQg+gVgyggIgQgIQgugbgOiKQgHifgFg2QgLi8AAgeQgDhWABhHQgBgoADghQgCgXAMgXQARglAqgaICxh1QBDgrAVgLQAwgXA2gKICPgSQBdgJAxgIQAagFAogNQArgOAVgFQA5gLBagFICegCQAuAABPgNQApgDBEAIIBrAKICKALQBOAGA6gBQARAAAngDQAhACAVAGQALADAdAOQAWAKASAFQCWAcBYgCQAgAPAugCQA3gEAdAKQARAGATAMIAiAXQAdASBFAQQA5AUAqAfQAjAfAgAsIAtA/QAUAjADAfQAHApgNAlQAXC/goF/QgKBEgrAgIhQA1QguAfgiASQkFCJk0AuQhDANhJAIQheAKiCAAQjTAAkugbg");
    this.shape.setTransform(84.7887, 77.1573);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy7("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy8("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy8("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy8("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy8("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy8("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy8("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({
      _off: true,
      y: -24.7
    }, 2).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(2).to({_off: false}, 2).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 418.20000000000005);


  (lib.B_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(2));

    // K
    this.instance = new lib.Locked_1copy("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy2();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(142,26,26,0.439)").s().p("AkgKoIgSgCIgTgBIhRgGQhygGhCgHQijgRizg3QgZgOgjgPIiHguQg8gUgwgeIgQgHQgrgZgOiBQgHiUgEgyQgLiwAAgbQgDhQABhDQgBglADgeQgCgWALgWQARgiAogYICqhtQBBgpAUgKQAugVA0gJICJgRQBZgJAvgHQAZgEAmgNQAqgNAUgFQA3gKBWgEICYgDQAsAABMgLQAogDBBAHIBnAKICEAKQBLAGA3gCQARAAAlgCQAgABAUAGQALADAbANQAWAKARAEQCQAaBVgBQAeAOAsgDQA1gDAbAJQASAFARAMIAhAVQAbAQBDAQQA3ATAoAdQAiAcAfAqIArA6QATAgADAdQAHAngNAjQAWCygmFlQgKA/gpAeIhNAxQgsAdggARQj7CAkpArQg/AMhGAHQhbAJh8AAQjKAAkjgZg");
    this.shape.setTransform(80.8498, 74.0903);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy2("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy2("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy2("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy2("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy2("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy2("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_4}]}, 1).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_5}]}, 1).to({state: [{t: this.instance_6}]}, 1).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      x: 78.65,
      y: -99.45
    }, 1).to({_off: true, x: 78.6, y: -71.2}, 1).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(7).to({_off: false}, 1).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 418.20000000000005);


  (lib._66 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.OFFcopy2();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off: false}, 0).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.329)").s().p("AhnDwIgGAAIgHgBIgdgCQgogCgYgCQg6gGhAgUIgWgKIgwgQQgWgHgRgLIgFgDQgQgIgFguIgEhGIgEhHIAAg0QgBgNABgLQAAgIAEgHQAGgNAOgIIA9gnQAXgOAHgEQARgHASgDIAxgHIAxgFQAJgCANgEIAWgGQAUgEAfgBIA2gBQAQAAAbgEQAOgCAXADIAlAEIAvADQAbACAUAAIATgBQALAAAHACIAOAGIAOAFQAzAJAfAAQALAFAPgBQATgBAKADQAGACAHAEIALAHQAKAGAYAGQATAGAPAKQAMALALAOIAPAVQAHALABAKQADAOgFAMQAIBAgOB9QgDAXgPAKIgbASIgcAQQhZAthqAPQgXAEgZADQghAEguAAQhHAAhngKg");
    this.shape.setTransform(1.5613, -5.0908);
    this.shape._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1).to({_off: false}, 0).to({_off: true}, 1).wait(2));

    // Layer_3
    this.instance_1 = new lib.OFFcopy2();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45.9, -30.6, 95.19999999999999, 50.5);


  (lib._55 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.329)").s().p("AhnDwIgGAAIgHgBIgdgCQgogCgYgCQg6gGhAgUIgWgKIgwgQQgWgHgRgLIgFgDQgQgIgFguIgEhGIgEhHIAAg0QgBgNABgLQAAgIAEgHQAGgNAOgIIA9gnQAXgOAHgEQARgHASgDIAxgHIAxgFQAJgCANgEIAWgGQAUgEAfgBIA2gBQAQAAAbgEQAOgCAXADIAlAEIAvADQAbACAUAAIATgBQALAAAHACIAOAGIAOAFQAzAJAfAAQALAFAPgBQATgBAKADQAGACAHAEIALAHQAKAGAYAGQATAGAPAKQAMALALAOIAPAVQAHALABAKQADAOgFAMQAIBAgOB9QgDAXgPAKIgbASIgcAQQhZAthqAPQgXAEgZADQghAEguAAQhHAAhngKg");
    this.shape.setTransform(1.0613, 0.6592);

    this.instance = new lib.OFFcopy();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-46.5, -24.6, 95.1, 50.3);


  (lib._44 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.329)").s().p("AhdDsIgGAAIgGgBIgbgCQglgCgVgCQg1gGg7gTQgIgFgLgFIgsgQQgUgHgPgLIgFgCQgPgJgEgtIgEhFIgDhGIgBgzQAAgNABgKQgBgIAEgHQAFgMANgJIA4gmIAbgRQAPgIARgDIAtgGIAsgFQAIgCANgEIAUgGQASgEAcgBIAxgBQAPAAAYgEQANgBAVACIAhAEIArADQAZACASAAIARgBQALAAAGACIANAGIAMAFQAvAJAcgBQAKAFAOgBQARgBAJADQAGACAGAEIAKAIQAJAFAWAGQASAGANAKQALAKAKAPIAOAUQAGALABAKQACAOgEAMQAHA+gMB7QgDAWgOALIgZARIgZAQQhRAshhAPQgVAEgWADQgeADgpAAQhCAAhdgJg");
    this.shape.setTransform(-0.125, -2.8419);

    this.instance = new lib.OFF();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFF();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-43.5, -27.6, 86.7, 49.8);


  (lib._2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.329)").s().p("AhtELIgIgBIgHAAIgegDIhFgFQg+gHhFgVQgJgGgNgFIg0gSQgXgIgSgMIgGgDQgRgKgFgyIgEhOIgEhPQgBggAAgaQAAgOABgMQgBgJAEgIQAHgNAPgKIBBgrIAggUQASgIAUgDQAQgDAkgEIA0gHQAJgBAPgFIAXgHQAVgEAhgCIA6gBQARAAAdgEQAPgBAYACIAnAEIAzAEQAcACAWAAIAUgBQAMAAAIADIAOAGQAIAEAHABQA3AKAhAAQALAGARgBQAUgBAKADQAHACAHAFIAMAIQALAGAZAGQAVAIAPALQANALAMARIAQAWQAHANACAMQACAPgFANQAJBGgPCMQgDAYgQAMIgdAUIgeASQhfAyhxARQgZAEgaADQgjAEgwAAQhNAAhtgKg");
    this.shape.setTransform(2.1702, -1.122);
    this.shape._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1).to({_off: false}, 0).to({_off: true}, 1).wait(2));

    // Layer_2
    this.instance = new lib.B_1copy2();
    this.instance.setTransform(69.55, 0.3, 0.3655, 0.3655, 0, 0, 0, 269.3, 83);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off: false}, 0).wait(1));

    // Layer_3
    this.instance_1 = new lib.B_1copy2();
    this.instance_1.setTransform(69.55, 0.3, 0.3655, 0.3655, 0, 0, 0, 269.3, 83);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-48.7, -29.5, 101.6, 56.1);


  (lib.wood = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AlVG7QiOhOAAhvQgBhwCKhNQCKhMC/AAIASAAQDJACCMBPQCNBQABBrQABBsiOBOQiNBOjJgBQjIABiOhOgAgjgPIABgBIgBAAIALAAIABAAIACAAIABAAIAFAAQhjgDhGhIQhKhJAAhoQAAhpBKhJQApgpAzgTQAngOAsABQArgBAoAOQAzATAqApQBIBJAABpQAABohIBJQhIBIhiADIAAAAIAEABgADWknIAAABIABABIgBgDgADQlHIAAABIAAABIAEAIIgFgRIABAHgADJlfIACACIgCgDg");
    this.shape.setTransform(1.0501, -27.35);

    this.instance = new lib.OFFcopy3();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy3();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-47.9, -79.4, 98.3, 104.9);


  (lib.Symbol20 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AlVG7QiOhOAAhvQgBhwCKhNQCKhMC/AAIASAAQDJACCMBPQCNBQABBrQABBsiOBOQiNBOjJgBQjIABiOhOgAgjgPIABgBIgBAAIALAAIABAAIACAAIABAAIAFAAQhjgDhGhIQhKhJAAhoQAAhpBKhJQApgpAzgTQAngOAsABQArgBAoAOQAzATAqApQBIBJAABpQAABohIBJQhIBIhiADIAAAAIAEABgADWknIAAABIABABIgBgDgADQlHIAAABIAAABIAEAIIgFgRIABAHgADJlfIACACIgCgDg");
    this.shape.setTransform(1.0501, -27.35);

    this.instance = new lib.OFFcopy9();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy9();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-47.9, -79.4, 98.3, 105);


  (lib.Symbol19 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AlVG7QiOhOAAhvQgBhwCKhNQCKhMC/AAIASAAQDJACCMBPQCNBQABBrQABBsiOBOQiNBOjJgBQjIABiOhOgAgjgPIABgBIgBAAIALAAIABAAIACAAIABAAIAFAAQhjgDhGhIQhKhJAAhoQAAhpBKhJQApgpAzgTQAngOAsABQArgBAoAOQAzATAqApQBIBJAABpQAABohIBJQhIBIhiADIAAAAIAEABgADWknIAAABIABABIgBgDgADQlHIAAABIAAABIAEAIIgFgRIABAHgADJlfIACACIgCgDg");
    this.shape.setTransform(1.0501, -27.35);

    this.instance = new lib.OFFcopy8();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy8();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-47.9, -79.4, 98.3, 105);


  (lib.Symbol13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AlVG7QiOhOAAhvQgBhwCKhNQCKhMC/AAIASAAQDJACCMBPQCNBQABBrQABBsiOBOQiNBOjJgBQjIABiOhOgAgjgPIABgBIgBAAIALAAIABAAIACAAIABAAIAFAAQhjgDhGhIQhKhJAAhoQAAhpBKhJQApgpAzgTQAngOAsABQArgBAoAOQAzATAqApQBIBJAABpQAABohIBJQhIBIhiADIAAAAIAEABgADWknIAAABIABABIgBgDgADQlHIAAABIAAABIAEAIIgFgRIABAHgADJlfIACACIgCgDg");
    this.shape.setTransform(1.0501, -27.35);

    this.instance = new lib.OFFcopy7();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy7();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-47.9, -79.4, 98.3, 105);


  (lib.Symbol11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AlVG7QiOhOAAhvQgBhwCKhNQCKhMC/AAIASAAQDJACCMBPQCNBQABBrQABBsiOBOQiNBOjJgBQjIABiOhOgAgjgPIABgBIgBAAIALAAIABAAIACAAIABAAIAFAAQhjgDhGhIQhKhJAAhoQAAhpBKhJQApgpAzgTQAngOAsABQArgBAoAOQAzATAqApQBIBJAABpQAABohIBJQhIBIhiADIAAAAIAEABgADWknIAAABIABABIgBgDgADQlHIAAABIAAABIAEAIIgFgRIABAHgADJlfIACACIgCgDg");
    this.shape.setTransform(1.0501, -27.35);

    this.instance = new lib.OFFcopy6();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy6();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-47.9, -79.4, 98.3, 105);


  (lib.Symbol9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AlVG7QiOhOAAhvQgBhwCKhNQCKhMC/AAIASAAQDJACCMBPQCNBQABBrQABBsiOBOQiNBOjJgBQjIABiOhOgAgjgPIABgBIgBAAIALAAIABAAIACAAIABAAIAFAAQhjgDhGhIQhKhJAAhoQAAhpBKhJQApgpAzgTQAngOAsABQArgBAoAOQAzATAqApQBIBJAABpQAABohIBJQhIBIhiADIAAAAIAEABgADWknIAAABIABABIgBgDgADQlHIAAABIAAABIAEAIIgFgRIABAHgADJlfIACACIgCgDg");
    this.shape.setTransform(1.0501, -27.35);

    this.instance = new lib.OFFcopy5();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy5();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-47.9, -79.4, 98.3, 105);


  (lib.Q = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.329)").s().p("AhrD4IgHAAIgHgBIgegBIhDgFQg9gHhDgTQgJgGgNgFIgygRQgXgHgSgLIgFgDQgRgJgFgvIgEhIIgEhKQgBgeAAgYQAAgNABgMQgBgIAEgHQAHgNAPgJIA/goIAggSQARgIATgDIAzgGIAzgGQAJgCAPgFIAXgGQAUgDAggCIA5gBQARAAAcgEQAPgBAXACIAnAEIAxAEQAcACAVgBIAUgBQAMABAHACIAOAFQAIAEAHACQA2AJAfAAQALAFARgBQAUgBAKADQAGACAHAFIAMAHQAKAGAZAGQAVAHAOAKQANALAMAPIAPAVQAIAMABALQACAOgEAMQAIBCgPCBQgDAXgPALIgdASIgdARQhdAvhvAPQgYAFgZACQgiAEguAAQhMAAhsgKg");
    this.shape.setTransform(1.2224, -0.7588);

    this.instance = new lib.OFFcopy4();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy4();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-48.3, -26.7, 99.1, 52.099999999999994);


  (lib.N3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.B_1();
    this.instance.setTransform(128.7, 10.1, 1, 1, 0, 0, 0, 268.7, 82.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.N3, new cjs.Rectangle(-193.7, -68.7, 267.5, 141), null);


  (lib._33 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.329)").s().p("AhpD6IgHgBIgGAAIgegCIhCgFQg8gGhCgUQgIgGgNgFIgygRQgWgHgSgLIgFgDQgQgJgFgvIgEhJIgEhKQgCgeABgYQgBgOABgLQAAgIAEgIQAGgNAPgIIA+gpIAfgSQARgIATgDIAygHIAzgGIAXgGIAWgGQAUgEAggBIA4gBQAQAAAcgFQAOgBAXADIAmAEIAxADQAbACAVAAIATgBQAMAAAIADIANAFIAPAFQA1AKAfgBQAKAGARgBQATgBAKADQAHACAGAEIAMAIQAKAGAYAGQAVAGAOALQANALALAPIAPAVQAIAMABALQACAOgEANQAIBBgPCDQgDAXgPALIgcASIgcARQhcAvhtAQQgYAEgZADQghADguAAQhKAAhqgJg");
    this.shape.setTransform(0.3974, -2.5088);

    this.instance = new lib.N3();
    this.instance.setTransform(69.35, 0.05, 0.3654, 0.3654, 0, 0, 0, 128.8, 10.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.N3();
    this.instance_1.setTransform(69.35, 0.05, 0.3654, 0.3654, 0, 0, 0, 128.8, 10.1);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-48.5, -28.7, 97.8, 52.099999999999994);


  (lib.All = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_1199 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(1199).call(this.frame_1199).wait(1));

    // Layer_15
    this.instance = new lib.Symbol1_1();
    this.instance.setTransform(-28, 476.05);
    new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib.Symbol1_1(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1200));

    // Layer_18
    this.instance_1 = new lib.Symbol20();
    this.instance_1.setTransform(228.25, -64.25, 0.8279, 0.8279);
    this.instance_1._off = true;
    this.instance_1.name = "6";
    this.instance_1.visible = false;

    new cjs.ButtonHelper(this.instance_1, 0, 1, 2, false, new lib.Symbol20(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(236).to({_off: false}, 0).wait(964));

    // Layer_17
    this.instance_2 = new lib.Symbol19();
    this.instance_2.setTransform(-178.05, -77.85, 0.8204, 0.8204, 0, 0, 0, -0.2, -0.3);
    this.instance_2._off = true;
    this.instance_2.name = "5";
    this.instance_2.visible = false;
    new cjs.ButtonHelper(this.instance_2, 0, 1, 2, false, new lib.Symbol19(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(215).to({_off: false}, 0).wait(985));

    // Layer_16
    this.instance_3 = new lib.Symbol13();
    this.instance_3.setTransform(-431.05, -164.95, 0.7523, 0.7523, 0, 0, 0, -0.1, -0.1);
    this.instance_3._off = true;
    this.instance_3.name = "4";
    this.instance_3.visible = false;
    new cjs.ButtonHelper(this.instance_3, 0, 1, 2, false, new lib.Symbol13(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(194).to({_off: false}, 0).wait(1006));

    // Layer_6
    this.instance_4 = new lib.Symbol11();
    this.instance_4.setTransform(-237.55, 137.35, 0.849, 0.849);
    this.instance_4._off = true;
    this.instance_4.name = "3";
    this.instance_4.visible = false;
    new cjs.ButtonHelper(this.instance_4, 0, 1, 2, false, new lib.Symbol11(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(175).to({_off: false}, 0).wait(1025));

    // Layer_5
    this.instance_5 = new lib.Symbol9();
    this.instance_5.setTransform(100.7, 142.5, 0.8933, 0.8933);
    this.instance_5._off = true;
    this.instance_5.name = "2";
    this.instance_5.visible = false;
    new cjs.ButtonHelper(this.instance_5, 0, 1, 2, false, new lib.Symbol9(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(159).to({_off: false}, 0).wait(1041));

    // Layer_4
    this.instance_6 = new lib.wood();
    this.instance_6.setTransform(412.2, 103.6, 0.8548, 0.8548);
    this.instance_6._off = true;
    this.instance_6.name = "1";
    this.instance_6.visible = false;
    new cjs.ButtonHelper(this.instance_6, 0, 1, 2, false, new lib.wood(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(143).to({_off: false}, 0).wait(1057));

    // Layer_2
    this.instance_7 = new lib.Symbol25();
    this.instance_7.setTransform(-62.95, 402.5, 1, 1, 0, 0, 0, 5.2, 6.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1200));

    // Layer_3
    this.instance_8 = new lib.Symbol22("synched", 0);
    this.instance_8.setTransform(-57.85, 73.45, 0.6685, 0.6685, 0, 0, 0, -127.7, 0);

    this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(1200));

    // Layer_7
    this.instance_9 = new lib._66();
    this.instance_9.setTransform(227.7, -60.8, 0.8556, 0.82, 0, 0, 0, 0.1, 0.1);
    this.instance_9._off = true;
    new cjs.ButtonHelper(this.instance_9, 0, 1, 2, false, new lib._66(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(112).to({_off: false}, 0));

    // Layer_8
    this.instance_10 = new lib._55();
    this.instance_10.setTransform(-177.55, -78.45, 0.8556, 0.82, 0, 0, 0, 0.1, 0.1);
    this.instance_10._off = true;
    new cjs.ButtonHelper(this.instance_10, 0, 1, 2, false, new lib._55(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(88).to({_off: false}, 0));

    // Layer_9
    this.instance_11 = new lib._44();
    this.instance_11.setTransform(-429.65, -162.8, 0.8556, 0.82);
    this.instance_11._off = true;
    new cjs.ButtonHelper(this.instance_11, 0, 1, 2, false, new lib._44(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(68).to({_off: false}, 0));

    // Layer_10
    this.instance_12 = new lib._33();
    this.instance_12.setTransform(-236.45, 138.45, 0.8556, 0.82, 0, 0, 0, 0.2, 0.2);
    this.instance_12._off = true;
    new cjs.ButtonHelper(this.instance_12, 0, 1, 2, false, new lib._33(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(50).to({_off: false}, 0));

    // Layer_11
    this.instance_13 = new lib._2();
    this.instance_13.setTransform(100.05, 143.2, 0.8556, 0.82, 0, 0, 0, 0.1, 0.1);
    this.instance_13._off = true;
    new cjs.ButtonHelper(this.instance_13, 0, 1, 2, false, new lib._2(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(32).to({_off: false}, 0));

    // Layer_12
    this.instance_14 = new lib.Q();
    this.instance_14.setTransform(412.55, 103.5, 0.8556, 0.82, 0, 0, 0, 0.1, 0.1);
    this.instance_14._off = true;
    new cjs.ButtonHelper(this.instance_14, 0, 1, 2, false, new lib.Q(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(13).to({_off: false}, 0));

    //peformance
    // Layer_13
    this.instance_15 = new lib.Symbol10("synched", 0);
    this.instance_15.setTransform(-26.1, 13.05, 0.9158, 0.82, 0, 0, 0, -1.3, 291.3);
    this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(1200));

    // Layer_14
    this.instance_16 = new lib.Symbol8("synched", 0);
    this.instance_16.setTransform(-203.7, -146, 0.7053, 0.676, 0, 0, 180, 33.4, 0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(15).to({startPosition: 0}, 0).wait(1185));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-664.2, -436.8, 1542.5, 952.9000000000001);


// stage content:
  (lib.Plains = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.All_Spacecopy();
    this.instance.setTransform(675.3, 585.85, 0.7944, 0.7654, 0, 0, 0, -57.5, 20.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_1
    this.instance_1 = new lib.All();
    this.instance_1.setTransform(800.3, 562.7, 1, 1, 0, 0, 0, 75, 2.4);
    this.instance_1.name = "stages";

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    // stageBackground
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("rgba(0,0,0,0)").ss(1, 1, 1, 3, true).p("EhyDhaEMDkHAAAMAAAC0JMjkHAAAg");
    this.shape.setTransform(720, 566.5);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EhyDBaFMAAAi0JMDkHAAAMAAAC0Jg");
    this.shape_1.setTransform(720, 566.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new lib.AnMovieClip();
  p.nominalBounds = new cjs.Rectangle(709, 555.5, 894.5999999999999, 588.5);
// library properties:
  lib.properties = {
    id: 'B9C248EDD1EADA4AA69064D342F0A988',
    width: 1440,
    height: 1133,
    fps: 24,
    color: "#FFFFFF",
    opacity: 1.00,
    manifest: [
      {src: "assets/images/plains/CachedBmp_225.png", id: "CachedBmp_225"},
      {src: "assets/images/plains/CachedBmp_224.png", id: "CachedBmp_224"},
      {src: "assets/images/plains/CachedBmp_217.png", id: "CachedBmp_217"},
      {src: "assets/images/shared/1.png", id: "CachedBmp_198"},
      {src: "assets/images/plains/Plains_atlas_1.png", id: "Plains_atlas_1"},
      {src: "assets/images/shared/2.png", id: "Plains_atlas_2"},
      {src: "assets/images/shared/2.png", id: "Plains_atlas_3"},
      {src: "assets/images/shared/2.png", id: "Plains_atlas_4"},
      {src: "assets/images/plains/Plains_atlas_5.png", id: "Plains_atlas_5"},
      {src: "assets/images/plains/Plains_atlas_6.png", id: "Plains_atlas_6"},
      {src: "assets/images/plains/Plains_atlas_7.png", id: "Plains_atlas_7"},
      {src: "assets/images/plains/Plains_atlas_8.png", id: "Plains_atlas_8"},
      {src: "assets/images/plains/Plains_atlas_8.png", id: "Plains_atlas_9"},
      {src: "assets/images/plains/Plains_atlas_8.png", id: "Plains_atlas_10"},
      {src: "assets/images/plains/Plains_atlas_11.png", id: "Plains_atlas_11"},
      {src: "assets/images/shared/3.png", id: "Plains_atlas_12"},
      {src: "assets/images/shared/3.png", id: "Plains_atlas_13"},
      {src: "assets/images/plains/Plains_atlas_14.png", id: "Plains_atlas_14"},
      {src: "assets/images/plains/Plains_atlas_15.png", id: "Plains_atlas_15"},
      {src: "assets/images/plains/Plains_atlas_16.png", id: "Plains_atlas_16"}
    ],
    preloads: []
  };


// bootstrap callback support:

  (lib.Stage = function (canvas) {
    createjs.Stage.call(this, canvas);
  }).prototype = p = new createjs.Stage();

  p.setAutoPlay = function (autoPlay) {
    this.tickEnabled = autoPlay;
  }
  p.play = function () {
    this.tickEnabled = true;
    this.getChildAt(0).gotoAndPlay(this.getTimelinePosition())
  }
  p.stop = function (ms) {
    if (ms) this.seek(ms);
    this.tickEnabled = false;
  }
  p.seek = function (ms) {
    this.tickEnabled = true;
    this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000);
  }
  p.getDuration = function () {
    return this.getChildAt(0).totalFrames / lib.properties.fps * 1000;
  }

  p.getTimelinePosition = function () {
    return this.getChildAt(0).currentFrame / lib.properties.fps * 1000;
  }

  an.bootcompsLoaded = an.bootcompsLoaded || [];
  if (!an.bootstrapListeners) {
    an.bootstrapListeners = [];
  }

  an.bootstrapCallback = function (fnCallback) {
    an.bootstrapListeners.push(fnCallback);
    if (an.bootcompsLoaded.length > 0) {
      for (var i = 0; i < an.bootcompsLoaded.length; ++i) {
        fnCallback(an.bootcompsLoaded[i]);
      }
    }
  };

  an.compositions = an.compositions || {};
  an.compositions['B9C248EDD1EADA4AA69064D342F0A988'] = {
    getStage: function () {
      return exportRoot.stage;
    },
    getLibrary: function () {
      return lib;
    },
    getSpriteSheet: function () {
      return ss;
    },
    getImages: function () {
      return img;
    }
  };

  an.compositionLoaded = function (id) {
    an.bootcompsLoaded.push(id);
    for (var j = 0; j < an.bootstrapListeners.length; j++) {
      an.bootstrapListeners[j](id);
    }
  }

  an.getComposition = function (id) {
    return an.compositions[id];
  }


  an.makeResponsive = function (isResp, respDim, isScale, scaleType, domContainers) {
    var lastW, lastH, lastS = 1;
    window.addEventListener('resize', resizeCanvas);
    resizeCanvas();

    function resizeCanvas() {
      var w = lib.properties.width, h = lib.properties.height;
      var iw = window.innerWidth, ih = window.innerHeight;
      var pRatio = window.devicePixelRatio || 1, xRatio = iw / w, yRatio = ih / h, sRatio = 1;
      if (isResp) {
        if ((respDim == 'width' && lastW == iw) || (respDim == 'height' && lastH == ih)) {
          sRatio = lastS;
        } else if (!isScale) {
          if (iw < w || ih < h)
            sRatio = Math.min(xRatio, yRatio);
        } else if (scaleType == 1) {
          sRatio = Math.min(xRatio, yRatio);
        } else if (scaleType == 2) {
          sRatio = Math.max(xRatio, yRatio);
        }
      }
      domContainers[0].width = w * pRatio * sRatio;
      domContainers[0].height = h * pRatio * sRatio;
      domContainers.forEach(function (container) {
        container.style.width = w * sRatio + 'px';
        container.style.height = h * sRatio + 'px';
      });
      stage.scaleX = pRatio * sRatio;
      stage.scaleY = pRatio * sRatio;
      lastW = iw;
      lastH = ih;
      lastS = sRatio;
      stage.tickOnUpdate = false;
      stage.update();
      stage.tickOnUpdate = true;
    }
  }
  an.handleSoundStreamOnTick = function (event) {
    if (!event.paused) {
      var stageChild = stage.getChildAt(0);
      if (!stageChild.paused) {
        stageChild.syncStreamSounds();
      }
    }
  }


})(createjs = createjs || {}, AdobeAn = AdobeAn || {});
var createjs, AdobeAn;
var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;

function initPlains() {
  return new Promise((resolve, reject) => {
    canvas = document.getElementById("canvas");
    anim_container = document.getElementById("animation_container");
    dom_overlay_container = document.getElementById("dom_overlay_container");
    var comp = AdobeAn.getComposition("B9C248EDD1EADA4AA69064D342F0A988");
    var lib = comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function (evt) {
      handleFileLoadPlains(evt, comp)
    });
    loader.addEventListener("complete", function (evt) {
      handleCompletePlains(evt, comp).then(data => {
        resolve(true)
      });
    });
    var lib = comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  })
}

function handleFileLoadPlains(evt, comp) {
  var images = comp.getImages();
  if (evt && (evt.item.type == "image")) {
    images[evt.item.id] = evt.result;
  }
}

function handleCompletePlains(evt, comp) {
  return new Promise((resolve, reject) => {
    var lib = comp.getLibrary();
    var ss = comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for (i = 0; i < ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet({
        "images": [queue.getResult(ssMetadata[i].name)],
        "frames": ssMetadata[i].frames
      })
    }
    var preloaderDiv = document.getElementById("_preload_div_");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.Plains();
    stage = new lib.Stage(canvas);
    stage.enableMouseOver();
    createjs.Ticker.removeAllEventListeners(); // Note the function name
    //Registers the "tick" event listener.
    fnStartAnimation = function () {
      stage.addChild(exportRoot);
      createjs.Ticker.framerate = lib.properties.fps;
      createjs.Ticker.addEventListener("tick", stage);
    }
    //Code to support hidpi screens and responsive scaling.
    AdobeAn.makeResponsive(true, 'both', false, 1, [canvas, preloaderDiv, anim_container, dom_overlay_container]);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
    plainClickEvents();
    plainHoverEvents();
    resolve(true);
  })
}

function plainClickEvents() {
  //stage 1
  exportRoot.instance_1.instance_6.on("click", function (evt) {
    playAudioOnClick();
    instance = 1;
  });
  //stage 2
  exportRoot.instance_1.instance_5.on("click", function (evt) {
    playAudioOnClick();
    instance = 2;
  });
  //stage 3
  exportRoot.instance_1.instance_4.on("click", function (evt) {
    playAudioOnClick();
    instance = 3;
  });
  //stage 4
  exportRoot.instance_1.instance_3.on("click", function (evt) {
    playAudioOnClick();
    instance = 4;
  });
  //stage 5
  exportRoot.instance_1.instance_2.on("click", function (evt) {
    playAudioOnClick();
    instance = 5;
  });
  //stage 6
  exportRoot.instance_1.instance_1.on("click", function (evt) {
    playAudioOnClick();
    instance = 6;
  });
  //back
  exportRoot.instance.instance.on("click", function (evt) {
    playAudioOnClick();
    instance = 7;
  });
}

function plainHoverEvents() {
  //stage 1
  exportRoot.instance_1.instance_6.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 2
  exportRoot.instance_1.instance_5.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 3
  exportRoot.instance_1.instance_4.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 4
  exportRoot.instance_1.instance_3.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 5
  exportRoot.instance_1.instance_2.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 6
  exportRoot.instance_1.instance_1.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //back
  exportRoot.instance.instance.on("mouseover", function (evt) {
    playAudioOnHover();
  });
}
