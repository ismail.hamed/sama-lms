(function (cjs, an) {

  var p; // shortcut to reference prototypes
  var lib = {};
  var ss = {};
  var img = {};
  lib.ssMetadata = [
    {name: "River2_atlas_1", frames: [[0, 0, 967, 1550], [969, 0, 967, 1550]]},
    {name: "River2_atlas_2", frames: [[0, 0, 967, 1550], [969, 0, 967, 1550]]},
    {name: "River2_atlas_3", frames: [[0, 0, 967, 1550], [969, 0, 967, 1550]]},
    {
      name: "River2_atlas_4",
      frames: [[665, 0, 562, 664], [1185, 1164, 433, 343], [1605, 1654, 374, 105], [1229, 0, 394, 388], [0, 1439, 393, 382], [396, 1056, 323, 327], [1185, 1509, 242, 243], [1620, 1164, 243, 243], [1620, 1409, 243, 243], [880, 666, 279, 280], [0, 0, 663, 640], [1625, 0, 394, 388], [1229, 390, 394, 388], [1625, 390, 394, 388], [484, 666, 394, 388], [0, 1049, 394, 388], [395, 1439, 393, 382], [790, 1056, 393, 382], [790, 1440, 393, 382], [1185, 780, 393, 382], [1580, 780, 393, 382], [1429, 1509, 174, 243], [0, 642, 482, 405], [880, 948, 83, 77]]
    },
    {
      name: "River2_atlas_5",
      frames: [[0, 0, 776, 1126], [0, 1128, 859, 899], [778, 0, 859, 899], [861, 901, 859, 899]]
    },
    {name: "River2_atlas_6", frames: [[0, 0, 859, 899], [0, 901, 859, 899], [861, 0, 859, 899], [861, 901, 859, 899]]},
    {name: "River2_atlas_7", frames: [[0, 0, 859, 899], [0, 901, 859, 899], [861, 0, 859, 899], [861, 901, 859, 899]]},
    {
      name: "River2_atlas_8",
      frames: [[861, 0, 423, 1125], [0, 901, 663, 640], [0, 0, 859, 899], [665, 1127, 663, 640], [1286, 0, 663, 640], [1330, 642, 663, 640], [1330, 1284, 663, 640]]
    },
    {name: "River2_atlas_9", frames: [[1134, 0, 715, 1224], [0, 0, 1132, 1002]]}
  ];


  (lib.AnMovieClip = function () {
    this.actionFrames = [];
    this.gotoAndPlay = function (positionOrLabel) {
      cjs.MovieClip.prototype.gotoAndPlay.call(this, positionOrLabel);
    }
    this.play = function () {
      cjs.MovieClip.prototype.play.call(this);
    }
    this.gotoAndStop = function (positionOrLabel) {
      cjs.MovieClip.prototype.gotoAndStop.call(this, positionOrLabel);
    }
    this.stop = function () {
      cjs.MovieClip.prototype.stop.call(this);
    }
  }).prototype = p = new cjs.MovieClip();
// symbols:


  (lib.CachedBmp_455 = function () {
    this.initialize(img.CachedBmp_455);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 5316, 3022);


  (lib.CachedBmp_454 = function () {
    this.initialize(img.CachedBmp_454);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 5270, 2838);


  (lib.CachedBmp_453 = function () {
    this.initialize(img.CachedBmp_453);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 5283, 2715);


  (lib.CachedBmp_443 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_442 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_441 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_440 = function () {
    this.initialize(ss["River2_atlas_5"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_439 = function () {
    this.initialize(ss["River2_atlas_8"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_438 = function () {
    this.initialize(ss["River2_atlas_9"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_437 = function () {
    this.initialize(ss["River2_atlas_8"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_436 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_435 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(4);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_462 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(5);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_433 = function () {
    this.initialize(img.CachedBmp_433);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 3670, 2839);


  (lib.CachedBmp_461 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(6);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_431 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(7);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_460 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(8);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_459 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(9);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_458 = function () {
    this.initialize(ss["River2_atlas_9"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_427 = function () {
    this.initialize(ss["River2_atlas_1"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_426 = function () {
    this.initialize(ss["River2_atlas_1"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_425 = function () {
    this.initialize(ss["River2_atlas_2"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_424 = function () {
    this.initialize(ss["River2_atlas_2"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_423 = function () {
    this.initialize(ss["River2_atlas_3"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_422 = function () {
    this.initialize(ss["River2_atlas_3"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_421 = function () {
    this.initialize(ss["River2_atlas_5"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_420 = function () {
    this.initialize(ss["River2_atlas_5"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_419 = function () {
    this.initialize(ss["River2_atlas_5"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_418 = function () {
    this.initialize(ss["River2_atlas_6"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_417 = function () {
    this.initialize(ss["River2_atlas_6"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_416 = function () {
    this.initialize(ss["River2_atlas_6"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_415 = function () {
    this.initialize(ss["River2_atlas_6"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_414 = function () {
    this.initialize(ss["River2_atlas_7"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_413 = function () {
    this.initialize(ss["River2_atlas_7"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_412 = function () {
    this.initialize(ss["River2_atlas_7"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_411 = function () {
    this.initialize(ss["River2_atlas_7"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_410 = function () {
    this.initialize(ss["River2_atlas_8"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_409 = function () {
    this.initialize(ss["River2_atlas_8"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_408 = function () {
    this.initialize(ss["River2_atlas_8"]);
    this.gotoAndStop(4);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_407 = function () {
    this.initialize(ss["River2_atlas_8"]);
    this.gotoAndStop(5);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_406 = function () {
    this.initialize(ss["River2_atlas_8"]);
    this.gotoAndStop(6);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_405 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(10);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_404 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(11);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_403 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(12);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_402 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(13);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_401 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(14);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_400 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(15);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_399 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(16);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_398 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(17);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_397 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(18);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_396 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(19);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_395 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(20);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_394 = function () {
    this.initialize(img.CachedBmp_394);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 2103, 1088);


  (lib.CachedBmp_393 = function () {
    this.initialize(img.CachedBmp_393);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 2103, 1088);


  (lib.CachedBmp_392 = function () {
    this.initialize(img.CachedBmp_392);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 2103, 1088);


  (lib.CachedBmp_391 = function () {
    this.initialize(img.CachedBmp_391);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 2103, 1088);


  (lib.CachedBmp_390 = function () {
    this.initialize(img.CachedBmp_390);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 2103, 1088);


  (lib.CachedBmp_389 = function () {
    this.initialize(img.CachedBmp_389);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 2103, 1088);


  (lib.CachedBmp_457 = function () {
    this.initialize(img.CachedBmp_457);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 4609, 2652);


  (lib.CachedBmp_456 = function () {
    this.initialize(img.CachedBmp_456);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 4315, 2622);


  (lib.CachedBmp_386 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(21);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_385 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(22);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_384 = function () {
    this.initialize(img.CachedBmp_384);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 2103, 1089);


  (lib.Blend_0 = function () {
    this.initialize(ss["River2_atlas_4"]);
    this.gotoAndStop(23);
  }).prototype = p = new cjs.Sprite();

// helper functions:

  function mc_symbol_clone() {
    var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop, this.reversed));
    clone.gotoAndStop(this.currentFrame);
    clone.paused = this.paused;
    clone.framerate = this.framerate;
    return clone;
  }

  function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
    var prototype = cjs.extend(symbol, cjs.MovieClip);
    prototype.clone = mc_symbol_clone;
    prototype.nominalBounds = nominalBounds;
    prototype.frameBounds = frameBounds;
    return prototype;
  }


  (lib.Tween6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0004, -0.0235, 1.2751, 1.0272, 0, 0, 180);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-177.1, -59.6, 354.29999999999995, 119.2);


  (lib.Tween4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0128, -0.0036);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-138.9, -58, 277.9, 116.1);


  (lib.Tween3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0128, -0.0036);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-138.9, -58, 277.9, 116.1);


  (lib.Tween2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhhMbQg6gRgLgyIAAACQgWAPgYgOQgVgOgMgdIgCABQg3ARg1ggQg5giAVg7QgiAMgKgiQgJgcALgcQgeAdgfgUQgcgTgLgpQgMgqASgZQAWgeA4AOQADADAAACQACgMAIgJQgQgBgRgJQgSgKgHgOQACgEAAgIQAAgHABgEQgYAHgagKIAAADQAOAUgYAYQghAhg3gTQgygSgWgmQgZgqALg0QAMg4AvgPQgugBgcghIAAADQgGAhg3AJQg3AIALgwQgKADgPAAIAFAFQgLA/g3AOQgzANg9ggQg/ggggg5QgkhBAVhFIAAAAIgBABQgVgEgJgUQgJgVASgPQgcgJAAgYQABgVAUgUIgbgMQgFAFgVAQQAeASgQAbQgOAZgfAHIAAABQAMAjglASQggAPgggIQgkgJgbguQgbgvATgaIgDgDQgrAIgGgtQgHgsAjgVQAYgQAeAJQAeAJAFAbQACgVAUgNQASgMAZAAQAZAAARAMQATAOAAAXQARgRAZALIAAgCQgOgPAMgRQALgQAUgCQgqgJgEgfQgEgcAdgdQAxgvBFABQBLABAYA/QARgaAnAGQAmAHAQAbQAWAoggAYQgeAWgmgNQADATgPAQQgOAOgSADQAKAWgSAUIAAAQQATAEAMAKQAOALACARQAdgkBJABQBLABAWArQAQgSASgEIAAgDQhWgNAFhbQACgmATgkQAUglAdgVQAygiBKgEQBMgEA4AgIABgDQgBgRAOgLQANgKASAAQASABALAKQAMAMgEAUQAagEAQAMQgDgPAOgJQAMgIAVgBIABgBQgSgTANgSQANgSAXANQgQgXAbgbQAXgXAdgIQAegJAlAIQAjAHAaAUQAgAWANAtQANAsgMApIAEAAQAXACAQALQATAMABAWIAAgCQATgHAWAGQAVAGARAOQgpguA2g9QAyg5A5gCQg0gIgRguQgRgwAqglQAUgQAggFQgSgPAMgQQAMgQAXAIQgbgWAegJQAdgJAXAOQgggiApgjQAjgfAqgFQAzgEAoAjQArAngYAwIgCAAQAgAFAQAVQAOATgDAZQgEAYgTAQQgVASgfgCQAMAKAAANQAAAOgMAKQAWgFAPADQAVAEgGAVQAZgHAPAKQAOAJABATQADAvg0AOQAUAGAFAUQAFARgJAUQARgOAZgCQAYgCAUAKIACACQAJgRAHgGQgZgDgKgUQgJgRAGgVQAGgVASgLQAUgMAaAIIACAAQhWgdAhhWQANgjAdgeQAegeAfgMQBOgeBDAzQBEA0gWBRQAwgCABAnQABAlgsANQAqgFAnAfQASgoBHAGQgUgIAOgRQANgRATAAQgBgVANgKQALgIAUABQgmhFAvgbQApgXBKASQBKATAfAqQAkAxg2A0QAiAEAMAjQAMAigdAWQAdALAXAgQAaAjgHAdQgGAagnAXQgkAWgdAAQAdAfgbAwQgZAsgoAQIgVAPQAhAwgqAtQgqAtgzgaQAbAcgJAfQgHAagfARQgdAQgegFQgggGgMgfQgSAOgbAKIgEgDQAPAwgcAoQgeArg8gIIhPAaQgWAAgSgHQgXgJgFgSQgWA1hegMQhcgLgZgwIgDAAQgPAvguATQgdAMhAAGQAVBThNAjQggAPglgDQgngEgegWQghArgrgmQgrgmATgsIgCgBQgXALgWgJQAGAOgFAJQgGAIgQgCQAOAig1AWQgrATgcgGQAcAIANAgQAOAkgZAXQAcACgBAbQAAAcgeAGIgDAAQAAAKgHANQA/gFAZAwQAaAyhEAhQgaAOgeAAQgVAAgXgHgA3vk6QgEgOAIgJQAGgHAMgBQAZgDAGANQAFAMgLAPQgKAQgNAAIgCABQgOAAgIgXgAtKmMQgWgNADgZQADgZAdAGQAqAKgLAhQgFANgLAEQgFADgGAAQgIAAgJgGg");
    this.shape.setTransform(0.028, 0.0029);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-152.1, -80.2, 304.29999999999995, 160.4);


  (lib.Tween1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhhMbQg6gRgLgyIAAACQgWAPgYgOQgVgOgMgdIgCABQg3ARg1ggQg5giAVg7QgiAMgKgiQgJgcALgcQgeAdgfgUQgcgTgLgpQgMgqASgZQAWgeA4AOQADADAAACQACgMAIgJQgQgBgRgJQgSgKgHgOQACgEAAgIQAAgHABgEQgYAHgagKIAAADQAOAUgYAYQghAhg3gTQgygSgWgmQgZgqALg0QAMg4AvgPQgugBgcghIAAADQgGAhg3AJQg3AIALgwQgKADgPAAIAFAFQgLA/g3AOQgzANg9ggQg/ggggg5QgkhBAVhFIAAAAIgBABQgVgEgJgUQgJgVASgPQgcgJAAgYQABgVAUgUIgbgMQgFAFgVAQQAeASgQAbQgOAZgfAHIAAABQAMAjglASQggAPgggIQgkgJgbguQgbgvATgaIgDgDQgrAIgGgtQgHgsAjgVQAYgQAeAJQAeAJAFAbQACgVAUgNQASgMAZAAQAZAAARAMQATAOAAAXQARgRAZALIAAgCQgOgPAMgRQALgQAUgCQgqgJgEgfQgEgcAdgdQAxgvBFABQBLABAYA/QARgaAnAGQAmAHAQAbQAWAoggAYQgeAWgmgNQADATgPAQQgOAOgSADQAKAWgSAUIAAAQQATAEAMAKQAOALACARQAdgkBJABQBLABAWArQAQgSASgEIAAgDQhWgNAFhbQACgmATgkQAUglAdgVQAygiBKgEQBMgEA4AgIABgDQgBgRAOgLQANgKASAAQASABALAKQAMAMgEAUQAagEAQAMQgDgPAOgJQAMgIAVgBIABgBQgSgTANgSQANgSAXANQgQgXAbgbQAXgXAdgIQAegJAlAIQAjAHAaAUQAgAWANAtQANAsgMApIAEAAQAXACAQALQATAMABAWIAAgCQATgHAWAGQAVAGARAOQgpguA2g9QAyg5A5gCQg0gIgRguQgRgwAqglQAUgQAggFQgSgPAMgQQAMgQAXAIQgbgWAegJQAdgJAXAOQgggiApgjQAjgfAqgFQAzgEAoAjQArAngYAwIgCAAQAgAFAQAVQAOATgDAZQgEAYgTAQQgVASgfgCQAMAKAAANQAAAOgMAKQAWgFAPADQAVAEgGAVQAZgHAPAKQAOAJABATQADAvg0AOQAUAGAFAUQAFARgJAUQARgOAZgCQAYgCAUAKIACACQAJgRAHgGQgZgDgKgUQgJgRAGgVQAGgVASgLQAUgMAaAIIACAAQhWgdAhhWQANgjAdgeQAegeAfgMQBOgeBDAzQBEA0gWBRQAwgCABAnQABAlgsANQAqgFAnAfQASgoBHAGQgUgIAOgRQANgRATAAQgBgVANgKQALgIAUABQgmhFAvgbQApgXBKASQBKATAfAqQAkAxg2A0QAiAEAMAjQAMAigdAWQAdALAXAgQAaAjgHAdQgGAagnAXQgkAWgdAAQAdAfgbAwQgZAsgoAQIgVAPQAhAwgqAtQgqAtgzgaQAbAcgJAfQgHAagfARQgdAQgegFQgggGgMgfQgSAOgbAKIgEgDQAPAwgcAoQgeArg8gIIhPAaQgWAAgSgHQgXgJgFgSQgWA1hegMQhcgLgZgwIgDAAQgPAvguATQgdAMhAAGQAVBThNAjQggAPglgDQgngEgegWQghArgrgmQgrgmATgsIgCgBQgXALgWgJQAGAOgFAJQgGAIgQgCQAOAig1AWQgrATgcgGQAcAIANAgQAOAkgZAXQAcACgBAbQAAAcgeAGIgDAAQAAAKgHANQA/gFAZAwQAaAyhEAhQgaAOgeAAQgVAAgXgHgA3vk6QgEgOAIgJQAGgHAMgBQAZgDAGANQAFAMgLAPQgKAQgNAAIgCABQgOAAgIgXgAtKmMQgWgNADgZQADgZAdAGQAqAKgLAhQgFANgLAEQgFADgGAAQgIAAgJgGg");
    this.shape.setTransform(-0.022, 0.0029);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-152.1, -80.2, 304.2, 160.4);


  (lib.Symbol40 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_453();
    this.instance.setTransform(-208.1, -1482.1, 0.5, 0.5);

    this.instance_1 = new lib.CachedBmp_454();
    this.instance_1.setTransform(-210.45, -1464.05, 0.5, 0.5);

    this.instance_2 = new lib.CachedBmp_455();
    this.instance_2.setTransform(-216.7, -1477.05, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance_1}]}, 4).to({state: [{t: this.instance_2}]}, 3).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance_1}]}, 4).to({state: [{t: this.instance_2}]}, 3).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance_1}]}, 4).to({state: [{t: this.instance_2}]}, 3).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance_1}]}, 4).to({state: [{t: this.instance_2}]}, 3).wait(3));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-216.7, -1482.1, 2658, 1516.1);


  (lib.Symbol39 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#6B0F0F").ss(0.5, 1, 1).p("AgakqQgCADgEAGQgEAKgBABQgOASgGAJQgHALgDAXQgBANgDAXQgFAcgQA/QgPA7gGAfQgEAWgOA8QgMAzgFAeQgJA1ADAFQAOAWAtAVQAKAEA7AXQBBAZAxAJQA3AKAwgEQAJgBAIgCQAMgBAVgLQAVgHA6gZAD7kbQAAgBgBAAQgDgBgigZQgdgWgIABQgWAFg0AHQgxAGgaAHQgDAAgYABQgTABgGAFQAAABgBAAQgBAGAQAUQAIAKAKANQARAYABABQALAPALAHQAFAEANAFQAMAEAGAEQABABAOAOQAKAKAHADQAEABANACQAKABAGAEQAEALACAOQAAADAAAEAD7kbQgtAngkA5QgBACgKAPQgHAKACArIABAJQAAACAAABQAAABAAAAIAAAAQAAABAAABQAAABABAAQADAgADBPQACAsAHA9QADAjAIBGQACAQAFAmQAEAigCAXAD7kbQAIAHARAnQAIARATAiQAUAjAHAQADyhmQAQgJAZgHQAIgCAegMQAFgCAEgBQAUgFAGAGIAXAXIAFAFQADAEAPALQAJAGAFAGQACACABACQAFAJADAPQACAIAEAQQAIAXANA0QAMAwAGAeQgPAkggAlQgaAfghAZAFthXQAJgDAQAFIAaAGAFthXQABgBABgBQADgDACgHQADgIAEgEAEhg4QAVgMAbgKQAEgBAMgDQAIgCAEgDAEhg4QgBAJALAaQANAfABAEQAIAgASA+QAOA4AFAoQACAIAEASQADAPgBAKADzhhQAZAaAVAPADzhhQgCgCgCgCQABgBACAAQAAACABADQAQBbAbBkQAIAeARA/QAMA4gDApQAEgCAFgCQAEgHAPgHQAIgHAFgCQADgCACABIAAAAIAAgBAi+BIQATgPAKgWQACgDAEgHQAEgHAAgDQgCgKgQgMQgTgNgGgEQgSgOglgTQgkgTgRgKQgBgBgBgBQABAAABABIAAABQALATgHAPIAAABQAGADAGAJQAHALACADQAxA0ANANQAXAcACAEgAk+gWQgBAGADAMQAAANACAbQADAcAAANQAAADACANQABALgCAEQAzgDAsgSQAOgGALgJAkqgzQgCAFgRAYQgBAAAAAAQgPgEgZACQgdADgLgCIAAAAQgFgBgHgHQgIgIgEgDQgUAigKAPQgHAKgEAFQgGAKgBAHQgBAGAIARQAEARAEAFQACAAAFADQAEAEACABQAKADATADQA3AMAwgCAmOgXQgBAIgFAIQgGAHgDAEQgPAZgIAOQgOAYgFARAmmgqQgBgBAAAAQAQgkAjgOQAkgPAgAUACbhoQAAACABAB");
    this.shape.setTransform(-0.0053, -0.01);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#AA9B96").s().p("Ag2BBIgCgQQAAgNgDgcQgCgaAAgOQgDgMABgFIABAAQARgZACgFQAGADAGAJIAJAOQAwA1ANAMQAXAcACAEQgLAJgOAGQgrATgzACQACgEgBgLg");
    this.shape_1.setTransform(-25.5375, 2.75);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#CCBAB3").s().p("AFrBeIgYhdQgbhlgQhaQAYAaAVAPQAAAJAKAZIAPAlIAZBdQAPA4AFAoIAFAZQAEAQgBAKIAAAAIgBAAQgBgBgEACIgMAJQgPAHgEAHIgKAFQAEgpgNg4gAmageQgJgRABgHQABgGAHgKIALgPIAegyIALAKQAIAIAEABQgBAIgFAHIgIAMIgYAoQgOAYgFAQQgDgEgEgRg");
    this.shape_2.setTransform(-5.2692, 9.3);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#765C53").s().p("AAAAJQgQgEgJADIACgCQADgDADgGQADgJADgDIAFAFIASAOQAJAGAFAGg");
    this.shape_3.setTransform(39.025, -9.6);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#947B73").s().p("ABpD6IgHg1IgMhpQgGg9gCgsQgDhPgEggIAAgDIAAAAIAAgBIgBgDIAAgJQgCgsAGgJIAMgRQAjg5AtgoQAIAIASAnQAHAQAUAjQATAiAIARIgJACQgfANgIACQgZAHgPAIIgDACIAEAEQAQBaAbBlIAYBdQANA4gEApQg5AZgVAHQgWAKgLACIgSADQACgXgDgjgAEXDOIgFgZQgFgogPg4IgZhdIgPglQgKgZAAgJQAWgNAagJIAQgEQAIgCAEgDQAJgDAQAEIAbAHIACAEQAFAJAEAOIAGAZQAHAWANA0QANAxAFAeQgOAkggAlQgbAfggAZQABgKgEgQgAkrAQQgOgMgwg1IgKgNQgGgJgFgEIAAAAQAGgQgKgTQAQALAlASQAkAUATANIAYARQAQAOACAKQABADgEAGIgGAKQgLAWgSAPQgCgFgXgcg");
    this.shape_4.setTransform(8.425, 2.3);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#B69F96").s().p("ADdE1QgygKhCgZIhFgbQgsgVgOgWQgDgEAKg1QAFgfAKgzIAShQQAGghAPg6QAQhAAGgcIAEgjQADgXAHgLIATgbIAGgLIAGgJQgBAFAPAUIAUAYIASAZQALAOAKAIQAGAEAMAEQANAFAFAEIAQAPQAKAKAHACIAQAEQALABAFADQAFALABAOIABAIIAAAJIABADIAAABIAAAAIAAACIAAABQAEAgADBPQACAsAGA9IAMBpIAHA1QADAjgCAWIgeABQgjAAglgGgAkYBSQgUgEgJgDIgHgEIgHgEQAFgQAOgZIAYgmIAIgMQAFgHABgIIABAAQAKABAegCQAZgDAOAFQgBAFADAMQAAAOADAaQACAcAAANIACAQQABALgCAEIgRAAQgoAAgtgJg");
    this.shape_5.setTransform(-13.2636, 1.6448);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#C5B9B5").s().p("AknCbQgPgFgZADQgeACgKgBIAAAAQgFgBgHgIIgMgKIgCgBQAQgkAkgPQAkgPAfAUIADACQAKATgGAQIAAAAQgCAFgSAZIAAAAgAEKBQIgBgGQAQgIAZgIQAHgCAfgMIAJgCQAUgGAGAGIAWAYQgDADgDAIQgDAIgDADIgBACQgEACgIADIgQAEQgbAJgWAMQgUgOgZgagACwA0QgBgOgEgLQgGgDgLgBIgQgEQgHgCgKgKIgPgOQgGgEgMgFQgNgEgFgEQgLgIgLgOIgSgZIgUgYQgOgUABgFIABgBQAFgFATgBIAcgCQAagGAxgHQA0gGAWgFQAHgCAeAWQAhAZAEACIAAAAQgtAogjA5IgLAQQgHAJACAsIgBgIg");
    this.shape_6.setTransform(-2.3, -17.7548);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#316011").s().p("AgfCfQgagLgqgYQgvgagUgKIgegNQgRgJgLgJQgNgLgHgOQgIgQADgQQgXgGgigDIg5gGQhIgIgighQgWgUgEgeQgGggASgWQATgYAogEQAUgBAvAIQBZAPBQAHQAnADASAHQAOAFAKAKQAMALADAOQACAHAAAVQAAASAFAJQAFAKATAOIA8AqQAfAWASAHQAVAIAcABQAdAAAkgHQAVgFAsgNQArgNAWgIQAkgMAagRIAcgSQARgLAOgEQAhgJAbATQANAJAGAPQAHAPgCAOQgDAPgLAPQgHAKgQAPQgmAmgYAOQgSAKgkANQg2ATg8ARQgkAJgQADQgiAGgiAAQhJAAhEgeg");
    this.shape_7.setTransform(2.6671, 19.2787);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_7}, {t: this.shape_6}, {t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-48.1, -34.2, 100.5, 72.4);


  (lib.Symbol38 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#6B0F0F").ss(0.5, 1, 1).p("AiRnBQgGgCgHgCQgJgCgMgBQgBACgBADQgTBNgXCDQgOBTgKA2IAAABQgJAxgGAZQggCSgHAtQgRBtAIBWQAAAEAABGQAAAwAKAYQADAHAKAMQAJANADAHQAUAnAHAMQADAGABAAQABAAAAgFQAAgCAAgCIABg1QABhAAAgfQAAg4gCgnQgDhUAHiNQAIilAAg6QAQiIAWhQQAhh2APgMgAiznIQAPgiAlgfQAYgTAygfQA+goAOgHQA6gfAaAEQAfAFAsAlQADADAZAOQATAKAFAKQAEAHgKASQgMAWAAACQgBACgBAGQgEAOgLAnQgLAoAAAVQgBAPAOAPQAQAPABAKQADAPgEAdQgEAjAFBKQACAJABAKQAKBDABAFQAEARAKAZQAGAMAEAJAhKmIQAMgGALgUQAOgaAEgFQAWgXAqgxQAngoAogPQABgBALgHQABgBAGgHQABABgBADQgDAKgYAdQgeApgQAWQgcAogLAbQgBACgDABQgJADgYgJAgKElQAAgBAAAAQgGhGgGhGIhLAiQgFATAAgFQAAgcASgrQAXgyADgSQACgGgIgJQgIgIABgFQACgEAGgFQAHgHACgDQAKgQAEgbQAGgpgDg2QgBgXgHhIQgCgPgBhCQgBgxgIgeQgBgFgHgCQgIgDgBgDQgEgFAAgIQAAgNAAgBQgCgOgSgFQgVgDgHgCQgHgDgMgDAAIjxQgShcgGg1QAAgBgHgTQgGgPAEgGQANgUAZgaQAPgPAcgcQAQgPAfghQAPgPAEgFQAEgDADgBQAEgBADACQgKgBgCAvQAAAZABAjQAAAIgFBAQgDA8AFAEQAEAEAAAAIABAAQgIAHgFAFQgVAUgQALQgKAGgOANQgQAOgHAFQgFADgKAJQgEAEgDACIAAABQgFAFABAAQACABACAAIAAA/QAAApgGBEQgLBzAACBQAAANAFAlQADAfgDAPQAAgBABgCQAIgdADgQQAShZAKgtQAOhEAIghQAMg8AMgqQAFgUAQgqQAPgoAFgWQABgBAAgBQALgsAVhAQACgFALgbIABgBIADgNQgBAAgLAMQgLANgBABQgMAMgKAJQABgCgBgCQgCgGAAgBIACgaQADgoACgUQAIgvAEgiQAHhBgPgDADWn9QABAjAiA2QAnA+AGAZQAEANgBAWQgCAeABAHQAAAGAKAzQAHAlgGAUQgHAYgQAiQgJATgTAlQAIARAAACQAIATAGAlQAHArAEAPQAGAUANAtQAJApgHAaQgDALgJAhQgHAbgFAQQgEANgKAWQgHAUgCAQQgBAIAEAhQADAcgFALQgHAPgYAMQgcAMgLAFQgUAKgZAUQgcAYgOALQgCABgIAKIAAAAACql3QgBAQAPAwQAJAeAOApQAGAWAFAfAB/lVQAAAAAAAAAAIjrQAMADAngBQAYgBApAAAj4JPQADAAACABQAHADAJABQAGACAOABQAyAJBoAaQAIACAdAFQAeAGACgBQAEgDgBgNQgDgOAAgDQgBgHAAgIQgFhAgDghQgDgngEgoQAAgPgBgPQAHAJAHAUQAIAbACAEQAGANAQAZQAQAYAFAOQAEAIAGAYQAHAWAOAIIgBAAQgGAIgGABQgNADgaABQgQAAgHgCQgDgBgEgCAgKElQABABAEgNQAAgCABgCAgBGIQgEgygFgxAANmgQAAAlgFCJ");
    this.shape.setTransform(0.0057, -0.0214);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#6D4B3F").s().p("AiCJRIgIgCIgIhiIgGhOIgDgeQAJAJAGAUIALAfQAFAMARAaQAPAYAGAOQADAIAGAYQAHAVAOAJIAAAAQgHAHgFACQgOADgaAAQgQAAgGgCgABdg+QgLgYgDgSIgLhIIgDgSQgFhKAEgkQADgdgCgOQgCgLgPgPQgPgOACgQQAAgUALgoIAPg2QAAAjAjA3QAnA+AGAYQADANgBAXIgBAkIAKA6QAHAlgFATQgHAZgRAiIgbA4IgKgWgAgalcIgFgEQgFgEAEg9IAEhIIAAg7QABgvALABQAPADgHBAQgEAigIAvIgFA9IgDAZIACAIIABADIgBABg");
    this.shape_1.setTransform(15.396, 0.724);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#886053").s().p("AhKIwQgGgYgEgIQgFgOgQgYQgQgagGgMIgKgfQgHgUgIgJIgJhkQABACAEgOIABgDIABgEQAJgdADgPIAciGIAWhmQAMg7AMgqQAFgUAQgqQAPgpAFgVIABgDQALgrAUhBIANgfIABgCQgBAQAPAxIAXBHQAGAVAFAgIADASIALBIQAEASAKAYIAKAWIAIASQAIAUAGAlQAHAqAEAQIATBBQAJApgHAZIgMAsQgHAcgFAQQgEAMgKAXQgHAUgCAQQgBAIAEAhQADAcgFALQgHAPgYAMIgnARQgUAKgYAUIgqAjIgKAKIAAABQgOgJgHgVgAiJmgQADgBABgDQALgaAcgoIAug/QAYgeADgJQAEgCADADQgKgBgCAvIABA7IgFBIQgDA9AFAEIAEAEIABAAIgNAMQgVATgQAMQgKAGgOAMIgXAUIgPAMIgHAGQAFiJAAglg");
    this.shape_2.setTransform(15.1455, 0.0219);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#765C53").s().p("AgTG3IAAgBIgNiMIhLAiQgFATAAgFQAAgcATgrQAWgyAEgSQABgGgIgJQgIgIACgFQABgEAHgFQAHgHABgDQAKgRAEgbQAGgpgDg2QgBgWgHhIQgBgPgChCQgBgxgHgeQgBgFgHgCQgIgDgCgDQANgGALgUQAOgaAEgFIA/hIQAogoAngPIANgIQgEAFgPAPIgvAwIgsArQgYAagOAUQgDAGAFAPIAHAUQAHA1ATBcQgGAFACAAIAEABIAAA/QAAAogHBEQgLB0AACBQAAANAFAlQADAfgEAPIgBAEIgEAMIAAAAg");
    this.shape_3.setTransform(0.975, -14.6425);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#C5B9B5").s().p("AArIfIgkgIQhngagzgIIgUgDQgJgCgGgCIAAgFIABg1IAAhfQAAg3gBgoQgEhTAHiNQAIimAAg5QAQiIAXhQQAhh3APgLIASAFQAIADAVADQARAEACAOIAAAOQAAAJAEAFQACACAIADQAGADABAFQAHAdABAyQACBBABAPQAHBIABAXQADA3gGAoQgEAbgKASQgBADgGAGQgHAGgBAEQgCAEAIAJQAHAIgBAGQgDASgWAyQgTAqAAAdQAAAFAFgTIBKgiIANCLIAAABIAIBkIADAeIAGBOIAIBiIABAPIADARQACANgFADIgBAAQgFAAgbgFgAA4CFQgFgmAAgMQAAiAALh1QAHhEAAgpIAAg+QAMACAmgBIBCgBIgBADQgFAVgQApQgPAqgGAUQgLAqgNA7IgVBnIgcCFQgDAPgKAdIgBAEQAEgPgDgfg");
    this.shape_4.setTransform(-6.0083, 9.8071);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#BEA79F").s().p("AC0CwIgXhHQgPgwAAgQIADgNIgLAMIgNAOIgVAVIAVgVIANgOIALgMIgDANIAAABIgNAgQgVBAgLAsIhCABQglABgMgDIgEgBQgCAAAGgFIAAgBIAAABQgThcgHg1IgHgUQgFgOADgGQAOgUAZgaIArgrIAvgwQAPgPAEgFIgNAIQgnAPgoAoIg/BIQgEAFgOAaQgLATgNAGQgEgFAAgIIAAgNQgCgOgRgFQgVgDgIgCIgSgGIgOgEQgIgCgMgBQAOgiAmgfQAYgTAygfQA/goAOgHQA4gfAaAEQAfAFAsAlIAcARQAUAKAEAKQAEAHgJASIgNAYIgBAIIgPA1QgLAnAAAVQgCAPAPAPQAPAPACAKQACAPgDAdQgEAjAFBKQgFgfgGgWgAgSCuIAGgGIAOgMIAYgTQAOgNAJgGQARgLAUgUIANgMIABAAIgBgEIgCgHIADgaIAFg7QAIgvAEgiQAHhBgPgDQgEgCgDABQAAAAAAgBQAAgBAAAAQAAgBAAAAQgBAAAAgBIgGAIIAHgEQgEAKgYAdIguA/QgcAogJAbQgBACgEAAIAAAAIgFABIAAAAIgBAAQgKgBgRgFQARAFAKABIABAAIAAAAIAFgBIAAAAQABAlgFCJg");
    this.shape_5.setTransform(2.7843, -41.7285);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#EFE7E4").s().p("AgSIMIgbg0QgDgHgKgNQgJgLgDgIQgLgYAAgwIAAhKQgHhVARhuQAHgsAgiSIAOhLIAAgBIAYiJQAXiDAThNIACgFQAMABAIADIAOADQgPAMghB3QgXBQgPCIQAAA5gICmQgHCNAEBTQABAnAAA4IAABfIgBA1IAAAEIgFAAg");
    this.shape_6.setTransform(-22.9398, 6.75);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#316011").s().p("AjYC/Qg9gbgmgqQgdgjgPgrQgMgkADgcQADgbASgfQALgSAaghQAYgeANgNQAWgXAWgMQAagOAcgBQAfgBAVARQAVAPAHAcQAHAbgGAbQgLArgsAwQBuAHA8gIQBegMA7gvIAsgmQAagVAXgEQAsgHAhApQAfAogHAuQgMBLhgBCQhsBLh8AVQgqAHgpAAQhVAAhMggg");
    this.shape_7.setTransform(-2.0829, 49.4442);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_7}, {t: this.shape_6}, {t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-39, -65.6, 73.9, 137.39999999999998);


  (lib.Symbol37 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#6B0F0F").ss(0.5, 1, 1).p("AhvlzQgRAAgQACQgbAAgRABQggACgIAKQgOAQgUAkQgVAogLAOQgVAegLAPQgUAbgOAQQgJAJgEAGQgJAJgCAHQgJAhgKBAQgMBBgHAeQgFAUgGAvQgGAsgGAXQgJAfACAMQABAFAIAFQAKAHABACQAEAFAMAUQAKAQAHAHQAPAPAjABQAuACAKADQAaAJAnAHQAWAEAtAHQA5AKAhAFQAuAFAngBQACAAACAAQAOgCAagSQAFgEAGgEQABgBABAAQALgJAPgLQAxggA9ggABakqQgJgzgjgLQgJgEhGgFQgogEgmACQAAAOAOARQANARAAAHQABAyAQAmIAAAAQABABAAACQABACAGAJQACADACACQAAACABACQAAABABABQABAFAIAwQAFAgAHAVQAFAMAIAaQAHAWAHAOQACADAHAKQAHAKgCADQgBACgBABQgFAFANALQAOAMgBADQAAAGgFAJQgGAKgBAEQgLBPAABUQAAAIAEATQAEASgBAIQgBAMgEATQgDAQAFAPQAAAAABAQQAAALAEABABakqQgPAEgUAMQgjAUgLADQghANgRAHIgXAJQgDABAAABAg2jNQACACACAEQAEAKACACQAHAJAQAUQAPARALAIQAPAKBEAwQAZAPgBACQAAABgIAEQgIADABACQACADAIgCQAKgCABADQABAAAAADQglANgXAJQgmAPgMAJAhCjhQAKAEARAEQAKACASACQAUAEAqAIQAkAIAYAJQA2AVAAACQAGgEAKAAQAMAAAGgDQAWgLAhgRQADgBAJgIQAIgGAGgCQgBgBgBgBQgUgehdgkQgUgIgLgFQgWgIgKABQgGABgGACAE0BDQAEgdgHgoQgKgugDgWQgCgUgGhQQgBgOAAgJQAAgKgCgHQgBgBAAgCAGeBZQgBgBAAgBQgJgGgqgJQgsgKgJAEQAAAAgBABIAAAAQgHADgJANQgKAQgEAFQgDAGgLALQgGAHgEAFQAKAIAkAIQAUAEAkAHQgEAGAAAEQgDAzALAqQAGAUABAIQAEATgFAJQgDAHgKASQAPgBAOgHQAMgGAQgQQAdgbAIgNQAKgQgIgwQgGglgLgcQgNgvgFgJQgJALgCACQgKALgFAGQgNAOgIAKQgIAKgDAHAClihQAEAJgHAmQgDAVgHAeQgIApgVBPQgVBQgMApAB3grQAAAPgNBGQAAAGgBAGQgGAtgJBPIAAAAQgBAGAAAGQgIBEgMBWAD+CFQgCADgBADQgIARgMAjQgMAlgGAPQgDAIgDAIQAAAAAAAAIAAAAQgOAnAHALQBHAWABAAIABgDQAAgBAAAAQACgGAFgQQAIgZAFACQACABgCAZQgCANAAAHIAAAAQAAAHABABQABABA4AHQACAAACAA");
    this.shape.setTransform(0.0006, 0.0122);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#886053").s().p("AjWElQgEgBAAgLIgBgQQgFgPADgRQAEgSABgMQABgJgEgRQgEgTAAgIQAAhVALhNQABgEAGgLQAFgIAAgGQABgDgOgNQgNgKAFgGIACgCQAMgKAmgPIA8gWQAAAOgNBIIgBAMIgPB6IAAABIAhh4QAVhRAIgpIAKgyQAHgngEgIQAGgEAKgBQAMAAAGgCIA2gcQADgBAJgIQAIgGAGgCIABADQACAGAAALIABAXQAGBQACAUQADAWAKAtQAHAqgEAcIAAAAQgHAEgJAMIgOAVQgDAFgLAMIgKAMIgDAFQgIASgMAjQgLAlgGAPIgGAPIAAABQg9AggxAfIgaAVIgCABIAUiaIABgMIgBAMIgUCaIgLAIQgaASgOACIgEAAgACJDvQAFgJgEgSIgHgcQgLgqADgzQAAgEAEgHQADgGAIgKIAVgYIAPgRIALgNQAFAJANAvQALAbAGAmQAIAvgKAQQgIAOgdAbQgQAPgMAHQgOAHgPABIANgagAifEJIAAAAg");
    this.shape_1.setTransform(22.9278, 7.95);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#967B72").s().p("AgbBbIgKgNQgHgPgHgWIgNgmQgHgTgFggIgJg1IgBgDIAEAHIAGALIAXAdQAPASAMAIIBSA4QAZAPgBACQAAACgIADQgIAEABABQACADAIgBQAKgCABACIABAEIg8AWQglAPgMAKQACgEgHgKg");
    this.shape_2.setTransform(3.225, -10.125);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#BEA79F").s().p("AB/DcQglgHgJgIIAKgMQALgMADgFIAOgVQAJgNAHgEIAAAAIABAAQAJgEAsAJQAqAJAIAHIACACIgLANIgPAQIgVAZQgIAJgDAHIg4gLgAg9hxQgYgJglgHIg+gMIgcgFQgSgDgJgFIgBgCIAAgBQAAAAAAAAQAAgBABAAQAAAAAAAAQABgBAAAAIAYgJIAzgTQALgEAjgUQAUgLAPgFIAMgDQAKgBAVAJIAgANQBcAkAUAeIACACQgGACgIAGQgJAHgDACIg3AbQgGADgMAAQgJABgGADQgBgBg1gWg");
    this.shape_3.setTransform(17.35, -7.1053);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#EFE7E4").s().p("ADdFmIg5gIQgCgBABgHIAAAAQgBgHACgNQADgZgDgBQgFgCgHAZIgHAWIAAABIgCADQgBAAhGgWQgHgLANgnIAAAAIABAAIAFgQQAHgPAMglQALgjAIgRIADgGQAKAIAkAIIA4ALQgDAGAAAEQgEAzAMAqIAHAcQADATgEAJIgOAZIgDAAgAglDAIAOh8IACgMQAMhGAAgPIAAgDQgCgDgJACQgJACgBgDQgBgCAIgDQAIgEAAgBQAAgCgZgPIhTg6QgMgIgOgRIgYgdIgGgMIgDgGIgCgEIgDgFIgHgLQAJAEASAEIAcAEIA+AMQAkAIAZAJQA0AVABACQAEAJgHAmIgKAzQgIApgUBPIghB5gAjUkuQAAgHgOgRQgOgRAAgOQAmgCApAEQBGAFAJAEQAjALAKAzQgPAEgVAMQgjAUgKADIg0AUIgXAJQAAAAgBAAQAAABAAAAQgBAAAAAAQAAABAAAAQgQgmgBgyg");
    this.shape_4.setTransform(12.8516, -1.3955);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#C5B9B5").s().p("ACMFwQghgFg5gKIhCgLQgngHgagJQgJgDgugCQgkgBgPgPQgHgHgKgQIgPgZIgMgJQgHgFgBgFQgDgMAJgfQAHgXAFgsQAGgvAFgUQAIgeALhBQALhAAJghQACgHAIgJIAOgPQANgQAUgbIAggtQALgOAWgoQATgkAOgQQAIgKAfgCIAtgBIAggCQAAAOAOARQAOARAAAHQABAyAQAmIAAAAIABADIAHALIADAFIACAEIAAACIAJA1QAGAgAHAVIAMAmQAIAWAHAOIAKANQAHAKgCADIgDADQgEAFAMALQAOAMAAADQAAAGgGAJQgGAKAAAEQgLBPAABUQAAAIAEATQADASAAAIQgBAMgEATQgDAQAFAPIABAQQAAALAEABIgNAAQgiAAgogEg");
    this.shape_5.setTransform(-21.2263, 0.0327);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-46.3, -38.2, 92.6, 76.5);


  (lib.Symbol36 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_443();
    this.instance.setTransform(-63.25, -74.8, 0.2253, 0.2253);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#316011").s().p("AmAEpQgbAAgRgGQgZgJgcgkQg0hDgJg6QgHgrAOgtQAOgqAegjQA6hCBfgUQgIguADgdQADgqAYgWQASgSAggGQAUgEAlAAIIPAAIAiABQASABAOAEQAmALAOAdIAGANQAEAHAFADQAFADAHAAIANgBQAUAAASAKQARALAIASQAKAWgHAbQgGAYgSAUQgdAjhGAaQhZAhgWANIgcATIgbASQg8AkhoAAIg2AAQggAAgVAEQgYAEg1ASQgaAJgNAGQgVALgNAMQgNANgOAYIgYAnQgiAxg5AQQgYAHgnAAIgLgBg");
    this.shape.setTransform(-7.046, 54.2559);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-63.2, -74.8, 126.6, 158.8);


  (lib.Symbol35 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_442();
    this.instance.setTransform(-108.2, -85.65, 0.5, 0.5);

    this.instance_1 = new lib.CachedBmp_441();
    this.instance_1.setTransform(-85.6, 39, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-108.2, -85.6, 216.5, 177.1);


  (lib.Symbol34 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#6B0F0F").ss(0.5, 1, 1).p("AjsizIAAgBQABgFAGgIQAJgKAAgBQAHgKAJgHQgZAMgWARIgBAAQg6AugrBAQgpA8gZBGQAAAAAAABQgIAYgHAaQAAABAAABIAAABAjsizQgOAMgNAVQgQAagGAIQg4BMg+BaQAAABgBABQgBAAAAABQgFAHgIAPQgIANgGAIQgBABgBABQAAAAAAABQgBALAHAMQADAGANARQAaAiAZARQAbAUAmAJQANADAPACQAfAEApgDQAigDBCgHQBxgKBigUQAxgKAugNQABAEAFAGQAAAAAJAJQAAAAALALQADAEADADQADACADACQAFADANAAQAEAAAWADQAQADAKACQAQAEAgADQAJABAHACQApAEAGgEQABgBAAgHQAAgGABgBQAFgRAAgUQAAgYgHgMQgDgFgJgGQgKgFgEgDIgLgHQgGAEgQALQgSAMgBAEQAAAAAAABQgDgCgOAAQgOABgDAAQgPgCgIAAQgIgCgHgBQgLgBgDABQgCABAAAAQgFAGgFAZQgEAYgHAHAhpg+IgBAAQgBgBgBAAQgTgHgSgPQgWgTgLgKQgQgOgRgRQgYgZgBgJAiOABQgSAmgnBSQgkBIghApAmyBnIgBAAQAAgBABAAQAAAAAAABgAjMjdQAZgUApAJQAfAGBAAVQAMACARAHQAJAEARAIQAHADAVABQATABAIAEQAIAFAMAGQAIAEAKAEQAZAMAMAHQABAAAhAOQAWAKAJAKQAQATALAlQAGAVALAmQAFANALAUQAOAXADAKQgLAAgNAJQgMAJgOARQgcAjAEAXQAAAAAAABABYh8QgPAUg2ASQguAPgkADQgPAEgHABQgMADgIgCQgDAHgRAbQgRAbAAABIAAABQAEAFAxAOQAQAGA1AOQArAMAXANQACABARAFQAPAEADAEQAIA9AYA3ABXikQAAACAEAQQACAMgDAHQAAAAgBABQgBABAAABQgHALABAXQAAAOAAAZQgIBDAJA/AGIB4QAKADgtgTQgXgKgMgGQgWgJgPgBIgBAAAFfCYQAAAEAEAGQACAFAEAGQAFALAGATQAHAVADAKAEJCSQgGgNABgu");
    this.shape.setTransform(0, -0.0254);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#CCBAB3").s().p("AAAA2IgLgLIgJgJQgFgGgBgEIAAgBQgEgXAcgiQANgRAMgJQgBAuAGANIgCABQgFAFgFAZQgEAYgHAHIgFgHg");
    this.shape_1.setTransform(23.675, 14.775);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#947B73").s().p("AB9DGIgKgfQgGgTgGgKIgGgLQgDgHgBgDIAAgCQABgDASgMIAXgPIALAHIAOAIQAJAFADAFQAHAMAAAYQgBAVgFAQIAAAIQAAAGgCABQgDADgLAAQgMAAgUgDgAiuArQgIg9AIhFIgBgnQAAgXAGgKIACgDIAAgBQAEgGgDgNIgEgSIASAJIAlASIAiAPQAWAJAJALQAQATALAlIASA8QAEANALATQANAXAEAKQgMgBgMAKQgMAIgOARQgcAjAEAXIABACQgvANgxAKQgYg4gIg9g");
    this.shape_2.setTransform(25.7061, 3.5125);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#AA9B96").s().p("AD3CxQgggDgRgEIgZgFIgagEQgOAAgFgDIgFgEQAGgGAFgYQAFgaAEgFIACgBQAEgCAKACIAQACIAWACIASAAQANAAADACQABADADAHIAGALQAGAKAGATIAKAfIgQgCgAg1AQIgUgGQgXgLgsgMQg1gPgPgGQgygOgDgFIAAAAQgBgDARgaQASgbACgIQAIACAMgCIAXgFQAjgDAwgQQA1gSAPgTQgGAKAAAXIABAnQgIBFAIA9QgCgDgPgFg");
    this.shape_3.setTransform(11.9493, 5.425);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#886053").s().p("Ai/BzQAhgoAlhIQAmhSATgmQADAFAxAOQAPAGA1APQAsAMAXAMIATAGQAPAFADADQAIA8AYA4QhhAUhyAJQhCAIgiACIghACQgVAAgSgDg");
    this.shape_4.setTransform(-7.75, 11.8143);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#BEA79F").s().p("AgZDKQgmgJgbgUQgZgRgZgiIgRgXQgHgMABgLIAAgBIACgCIAOgVIANgWIABgBIABgCQA+haA4hMIAVgiQANgVAOgMQABAJAYAZQARARAQAOIAhAdQATAPASAHIACABIABAAQgDAHgRAbQgRAbAAACIABABQgTAlgnBSQgkBIghApIgbgFg");
    this.shape_5.setTransform(-27.1062, 2.675);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#C5B9B5").s().p("AFjDAIgSABIgWgCIgQgDQgKgBgEABQgGgNACguQAMgJAMAAIAAAAQAPABAWAJIAkAQQAsATgJgDIgXAPQgSAMgBAEIAAABQgDgCgNAAgAmeCQIAAgBIABgCQAGgaAIgYIABgBQAYhHApg7QAshAA6guIABAAQAVgRAZgMQAagUApAJQAfAGA/AVQANACAPAHIAbAMQAIADAUABQATABAJAEIATALIAEASQADAMgEAHIAAABIgCACQgPAUg1ASQgvAPgjADIgXAFQgMADgIgCIAAAAIgDgBQgSgHgTgPIgggdQgRgOgQgRQgYgZgBgJIAAgBQAAgFAGgIIAJgLQAHgKAJgHQgJAHgHAKIgJALQgGAIAAAFIAAABQgPAMgNAVIgWAiQg3BLg+BbIgCACIgBABIgNAWIgOAVIgBACIgBABIAAgBg");
    this.shape_6.setTransform(-2.0914, -4.2147);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#316011").s().p("AkSC7QhJgCgsgjQgggZgVgsQgPghgNgzQgJglgCgYQgDgiAJgaQAKgeAagTQAcgTAdAHQAtANATBGIANA1QAJAfAOASQBlgiC7gJQDTgLBTgTIAzgLQAdgFAWAEQAcAGAWAVQAWAUAJAcQAIAbgIAeQgIAdgUAVQgdAdg0AJQggAGg+AAQjXAAjPAiQhNANgsAAIgJgBg");
    this.shape_7.setTransform(-1.1375, 16.4031);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_7}, {t: this.shape_6}, {t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-49.6, -24.6, 97, 59.800000000000004);


  (lib.Symbol31 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_440();
    this.instance.setTransform(-50.8, -73.75, 0.131, 0.131);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-50.8, -73.7, 101.69999999999999, 147.5);


  (lib.Symbol23 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_439();
    this.instance.setTransform(-70.05, -186.45, 0.3316, 0.3316);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.rf(["#274805", "rgba(56,117,32,0.635)", "rgba(80,181,71,0.118)"], [0, 0.58, 1], -0.5, 185.8, 0, -0.5, 185.8, 0).s().p("Ah+DEQgvgDgigRQgMgFgegSQgagPgRgGQgVgIg1gEQgxgDgYgMQgygYgMg/QgMg7AfgyQAdgtA3gbQAxgYA9gGQAugFBDAFQBLAGAmACQA3ACBRgFICIgIQA/gCApAEQA4AGAqAWQAwAYAaAvQAcAxgNAvQgXBViUAqQi4A1i7AOQgfACgZAAIgdgBg");
    this.shape.setTransform(-0.5363, 185.8034);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-70, -186.4, 140.2, 392);


  (lib.Symbol22 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_438();
    this.instance.setTransform(-85.4, -146.3, 0.239, 0.239);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.rf(["#274805", "rgba(56,117,32,0.635)", "rgba(80,181,71,0.118)"], [0, 0.58, 1], 6.1, -12, 0, 6.1, -12, 93.8).s().p("Ak4CZIgkgKQgVgFgPAEIgZAJQgOAGgKgDQgRgEgNgaQgNgZgDghQgDgbAFgYQAEgZAKgQQAJgNAOgGQAHgDARgFQAvgKBigkQBfgiAygKQAngIAxgDQAagCA9gBQBqgCAyACQBWACBFANQApAHAWAPQAjAZAPAvQAIAaAAAdQgBAggJAWQgKAZgUAMQgOAJgXAEQglAIg0ACIhZACQhIACidARQiXAQhQABIgMAAQgoAAgagGg");
    this.shape.setTransform(6.5054, 142.3188);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-85.4, -146.3, 170.9, 304.5);


  (lib._3_Stars = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_437();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Star_R = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_436();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_stae_L = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_435();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib.Tween5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0004, -0.0235, 1.2751, 1.0272, 0, 0, 180);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-177.1, -59.6, 354.29999999999995, 119.2);


  (lib.Symbol12copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_433();
    this.instance.setTransform(-696.7, -540.3, 0.3801, 0.3801);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true}, 1).wait(184));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-696.7, -540.3, 1394.8000000000002, 1079);


  (lib.Symbol5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_458();
    this.instance.setTransform(-42.85, -37.95, 0.0758, 0.0758);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-42.8, -37.9, 85.8, 76);


  (lib.Locked_1copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_427();
    this.instance.setTransform(-223.6, -34.05, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-223.6, -34, 483.5, 775);


  (lib.Locked_1copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_426();
    this.instance.setTransform(-251.4, -31.8, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-251.4, -31.8, 483.5, 775);


  (lib.Locked_1copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_425();
    this.instance.setTransform(-338.5, -59.55, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-338.5, -59.5, 483.5, 775);


  (lib.Locked_1copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_424();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Locked_1copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_423();
    this.instance.setTransform(-241.8, 93.45, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, 93.5, 483.5, 775);


  (lib.Locked_1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_422();
    this.instance.setTransform(-241.8, -155.1, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -155.1, 483.5, 775);


  (lib.C_1_ONcopy13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("6", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_421();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("5", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_420();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("4", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_419();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("3", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_418();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("4", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(255,0,0,0.659)").s().p("A0pVaQpRoXAxsqQAxsqIzopQIzooLvABQLwACIEIsQIEIrAYNEQAWNEoKHqQoKHpsUAOIgqABQr5AApBoIg");
    this.shape.setTransform(-2.6405, -24.3306);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_417();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("2", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(255,0,0,0.659)").s().p("A0wVlQodoQANssQANsrIqo3QIro3LsAEQLuADIYJJQIYJKAUM7QAVM9o5HiQo5Hhr7AIIgXAAQrsAAoVoIg");
    this.shape.setTransform(-0.3177, -24.3699);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_416();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("1", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(255,0,0,0.659)").s().p("AgGd1QscgJoaoYQoZoYAcsYQAdsYJDpcQJDpcLhA+QLjA+H7JMQH8JNAVLzQAVLzocIXQoVIPsMAAIgYAAg");
    this.shape.setTransform(-0.1577, -25.9536);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_415();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("2", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_414();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("6", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(255,0,0,0.659)").s().p("A0qVgQogoRAPs1QAPszIWorQIWoqL0AAQL0AAIWIqQIWIrAmMrQAmMtovIXQovIXsGACIgHAAQsCAAodoPg");
    this.shape.setTransform(0.9752, -22.4118);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_413();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("5", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(255,0,0,0.659)").s().p("A09VVQoqohApsHQAosGHwpBQHxpBMNgJQMNgIIjIrQIjIsAWMsQAXMsoQIHQoQIHsmATIg1ABQsDAAoXoQg");
    this.shape.setTransform(0.7834, -24.3999);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_412();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("1", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_411();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("3", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(255,0,0,0.659)").s().p("A0YWAQoboAAFtOQAFtNIoozQIno0L1AQQL2AQIDIsQIDIqAXNCQAXNDomH3QonH3r6AMIgkAAQrkAAoOnzg");
    this.shape.setTransform(0.4414, -23.1574);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_410();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.Blendcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Blend_0();

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Blendcopy, new cjs.Rectangle(0, 0, 83, 77), null);


  (lib.Pathcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.rf(["#2F0202", "#3B0101", "#3B0303", "#4A0101", "#600101", "#790306", "#A50003", "#820506", "#C00004"], [0.604, 0.635, 0.663, 0.698, 0.729, 0.776, 0.808, 0.816, 0.839], 2.7, -3.2, 0, 2.7, -3.2, 26.8).s().p("AiKCLQg6g5AAhSQAAhQA6g6QA6g6BQAAQBSAAA6A6QA5A5AABRQAABSg5A5Qg6A6hSAAQhQAAg6g6g");
    this.shape.setTransform(19.7, 19.725);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Pathcopy, new cjs.Rectangle(0, 0, 39.4, 39.5), null);


  (lib.Path_1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(0, 0, 0, 0);


  (lib._3_Starscopy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_409();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_408();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_407();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_406();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_405();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Star_Rcopy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_404();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_403();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_402();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_401();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_400();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_stae_Lcopy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_399();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_398();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_397();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_396();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_395();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._00000000000copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_394();
    this.instance.setTransform(-76.15, -32.65, 0.0658, 0.0658);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-76.1, -32.6, 138.39999999999998, 71.6);


  (lib._00000000000copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_393();
    this.instance.setTransform(-76.15, -32.65, 0.0658, 0.0658);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-76.1, -32.6, 138.39999999999998, 71.6);


  (lib._00000000000copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_392();
    this.instance.setTransform(-76.15, -32.65, 0.0658, 0.0658);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-76.1, -32.6, 138.39999999999998, 71.6);


  (lib._00000000000copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_391();
    this.instance.setTransform(-76.15, -32.65, 0.0658, 0.0658);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-76.1, -32.6, 138.39999999999998, 71.6);


  (lib._00000000000copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_390();
    this.instance.setTransform(-76.15, -32.65, 0.0658, 0.0658);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-76.1, -32.6, 138.39999999999998, 71.6);


  (lib._00000000000 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_389();
    this.instance.setTransform(-76.15, -32.65, 0.0658, 0.0658);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-76.1, -32.6, 138.39999999999998, 71.6);


  (lib._00000000000_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance_1 = new lib.CachedBmp_384();
    this.instance_1.setTransform(-76.15, -32.65, 0.0658, 0.0658);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-76.1, -32.6, 138.39999999999998, 71.6);


  (lib.Symbol486 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_R("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol485 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_L("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol484 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy6("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol483 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy6("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol482 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy4("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol481 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy4("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol480 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy2("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol479 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy2("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol478 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy3("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol477 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy3("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol476 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol475 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    // this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol21 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Tween5("synched", 0);

    this.instance_1 = new lib.Tween6("synched", 0);
    this.instance_1.setTransform(75.8, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance_1}]}, 779).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true, x: 75.8}, 779).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-177.1, -59.6, 430.1, 119.2);


  (lib.Symbol18 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Tween3("synched", 0);

    this.instance_1 = new lib.Tween4("synched", 0);
    this.instance_1.setTransform(43.6, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance_1}]}, 739).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true, x: 43.6}, 739).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-138.9, -58, 321.5, 116.1);


  (lib.Symbol17 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Tween1("synched", 0);

    this.instance_1 = new lib.Tween2("synched", 0);
    this.instance_1.setTransform(117.35, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance_1}]}, 699).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true, x: 117.35}, 699).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-152.1, -80.2, 421.6, 160.4);


  (lib.Symbol14 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Symbol5("synched", 0);
    this.instance.setTransform(585.4, -144.85, 0.8099, 2.222, 0, -8.1925, 170.171);

    this.instance_1 = new lib.Symbol5("synched", 0);
    this.instance_1.setTransform(478.75, -163.55, 1.1745, 2.7219, 0, -8.1924, 171.8116, -0.1, 0);

    this.instance_2 = new lib.Symbol5("synched", 0);
    this.instance_2.setTransform(542.4, -134.6, 0.8099, 1.8762, 0, -8.1926, 170.171);

    this.instance_3 = new lib.Symbol5("synched", 0);
    this.instance_3.setTransform(253.55, -29.9, 0.8096, 1.8762, 0, -8.1926, 171.8116);

    this.instance_4 = new lib.Symbol5("synched", 0);
    this.instance_4.setTransform(297.65, -29.95, 0.8096, 1.8762, 0, -8.1926, 171.8116);

    this.instance_5 = new lib.Symbol5("synched", 0);
    this.instance_5.setTransform(341.75, -56.1, 0.8096, 1.8762, 0, -8.1926, 171.8116);

    this.instance_6 = new lib.Symbol5("synched", 0);
    this.instance_6.setTransform(429.9, -104.55, 0.8096, 1.8762, 0, -8.1926, 171.8116);

    this.instance_7 = new lib.Symbol5("synched", 0);
    this.instance_7.setTransform(382.4, -111.7, 0.8096, 2.5361, 0, -8.1927, 171.8116, -0.1, 0);

    this.instance_8 = new lib.Symbol5("synched", 0);
    this.instance_8.setTransform(209.65, -29.95, 0.8099, 1.8762, 0, -8.1926, 170.171);

    this.instance_9 = new lib.Symbol5("synched", 0);
    this.instance_9.setTransform(102.1, -29.15, 1.1745, 2.7219, 0, -8.1924, 171.8116, -0.1, 0);

    this.instance_10 = new lib.Symbol5("synched", 0);
    this.instance_10.setTransform(163.3, -47.45, 0.8099, 2.3466, 0, -8.1925, 170.171, -0.1, 0);

    this.instance_11 = new lib.Symbol5("synched", 0);
    this.instance_11.setTransform(-123.1, 0.1, 0.8096, 1.8762, 0, -8.1926, 171.8116);

    this.instance_12 = new lib.Symbol5("synched", 0);
    this.instance_12.setTransform(-79, 0.05, 0.8096, 1.8762, 0, -8.1926, 171.8116);

    this.instance_13 = new lib.Symbol5("synched", 0);
    this.instance_13.setTransform(-36.9, -14.1, 0.8096, 2.2582, 0, -8.1925, 171.8116);

    this.instance_14 = new lib.Symbol5("synched", 0);
    this.instance_14.setTransform(53.25, 0, 0.8096, 1.8762, 0, -8.1926, 171.8116);

    this.instance_15 = new lib.Symbol5("synched", 0);
    this.instance_15.setTransform(9.2, -0.05, 0.8096, 1.8762, 0, -8.1926, 171.8116);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_15}, {t: this.instance_14}, {t: this.instance_13}, {t: this.instance_12}, {t: this.instance_11}, {t: this.instance_10}, {t: this.instance_9}, {t: this.instance_8}, {t: this.instance_7}, {t: this.instance_6}, {t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-167.7, -272.9, 799.3, 353.4);


  (lib.Symbol10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib._00000000000_1("synched", 0);
    this.instance.setTransform(2.1, 6.15, 0.5065, 0.5065, 0, 0, 0, 0.1, 0.2);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-36.5, -10.4, 70.1, 36.3);


  (lib.Symbol9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Symbol21();
    this.instance.setTransform(-1228.45, -301.5, 1.5356, 1.5356, 0, 0, 0, 0.1, -0.1);

    this.instance_1 = new lib.Symbol21();
    this.instance_1.setTransform(321.65, -272.85, 1.5356, 1.5356, 0, 0, 0, 0.1, -0.1);

    this.instance_2 = new lib.Symbol18();
    this.instance_2.setTransform(-274.2, -303.8, 1.5356, 1.5356);

    this.instance_3 = new lib.Symbol17();
    this.instance_3.setTransform(-822.55, -304.5, 1.5356, 1.5356, 0, 0, 0, -0.1, -0.1);

    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["#FFFFFF", "#00CCFF"], [0, 1], 9.5, -128.9, 9.7, -733.2).s().p("EirDBwTMAAAjglMFWHAAAMAAADglg");
    this.shape.setTransform(-428.775, 148.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-1523.5, -569.9, 2189.5, 1437.4), null);


  (lib.Symbol1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Stars("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Tween6copy13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy13("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy13("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("4", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-3.95, -40.25, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(255,0,0,0.659)").s().p("AnXHuQjBjEAAkkQAAkiDJjKQDKjJEPAAQERABC7DJQC7DJAIEuQAIEvjIC3QjHC2kUACIgEAAQkRAAjAjCg");
    this.shape.setTransform(-0.0207, -0.312);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_462();
    this.instance.setTransform(-80.85, -81.7, 0.5, 0.5);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.5);


  (lib.Tween6copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween5copy13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy13("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy13("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy8("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(34.4, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(34.4, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-66.4, -74.4, 201.70000000000002, 148.9);


  (lib.Tween5copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween4copy13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy13("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy13("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy8("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween3copy13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy13("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy13("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy8("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween2copy13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy13("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy13("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy8("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween1copy13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy13("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy13("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy11("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy11("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy10("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy10("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy8("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy8("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy7("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy6("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy9("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy9("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Symbol15copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Symbol12copy2("synched", 0);
    this.instance.setTransform(0, 0.05, 1, 1, 0, 0, 0, 0.7, -0.8);

    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("Ehv7BXZMAAAiuxMDf2AAAMAAACuxgEhjTBJpMDDfgEBMAAAiSVMjD9AAAg");
    this.shape.setTransform(-2.1, 1.525);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-718.4, -557.7, 1432.6999999999998, 1118.5);


  (lib.Symbol10copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib._00000000000copy6("synched", 0);
    this.instance.setTransform(2.1, 6.15, 0.5065, 0.5065, 0, 0, 0, 0.1, 0.2);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-36.5, -10.4, 70.1, 36.2);


  (lib.Symbol10copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib._00000000000copy4("synched", 0);
    this.instance.setTransform(2.1, 6.15, 0.5065, 0.5065, 0, 0, 0, 0.1, 0.2);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-36.5, -10.4, 70.1, 36.2);


  (lib.Symbol10copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib._00000000000copy3("synched", 0);
    this.instance.setTransform(2.1, 6.15, 0.5065, 0.5065, 0, 0, 0, 0.1, 0.2);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-36.5, -10.4, 70.1, 36.2);


  (lib.Symbol10copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib._00000000000copy2("synched", 0);
    this.instance.setTransform(2.1, 6.15, 0.5065, 0.5065, 0, 0, 0, 0.1, 0.2);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-36.5, -10.4, 70.1, 36.2);


  (lib.Symbol10copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib._00000000000copy("synched", 0);
    this.instance.setTransform(2.1, 6.15, 0.5065, 0.5065, 0, 0, 0, 0.1, 0.2);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-36.5, -10.4, 70.1, 36.2);


  (lib.Symbol10_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance_1 = new lib._00000000000("synched", 0);
    this.instance_1.setTransform(2.1, 6.15, 0.5065, 0.5065, 0, 0, 0, 0.1, 0.2);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-36.5, -10.4, 70.1, 36.2);


  (lib.Symbol7copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFC000").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACIgEgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape.setTransform(5.8, -5.3833);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#542000").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBAAAAgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape_1.setTransform(5.8, -3.8833);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f().s("#F0EDED").ss(0.1, 1, 1).p("AEbABQAAB0hUBTQhSBTh1AAQh0AAhUhTQhShTAAh0QAAh1BShTQBUhTB0AAQB1AABSBTQBUBTAAB1g");
    this.shape_2.setTransform(6.125, -3.425);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("rgba(0,0,0,0.329)").s().p("AjIDIQhShTAAh0QAAh1BShTQBUhTB0AAQB1AABSBTQBUBTAAB1QAAB0hUBTQhSBTh1AAQh0AAhUhTg");
    this.shape_3.setTransform(6.125, -3.425);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#F3F3E8").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACIgEgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape_4.setTransform(5.8, -5.3833);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("rgba(52,100,196,0.769)").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBAAAAgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape_5.setTransform(5.8, -3.8833);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).to({state: [{t: this.shape_3}, {t: this.shape_2}]}, 1).to({state: [{t: this.shape_5}, {t: this.shape_4}]}, 1).wait(2));

    // Layer 1
    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#FFFFFF").s().p("AgbBfQgEgEAAgFQAAgJALgFQApgTAMgZQgWAAgKgGQgMgGAAgUQAAgPAJgOQAJgPAOAAQARAAALAOQAKANACATIAoAAQgHgQAAgKQAAgOAMgNQANgPAUgBQALAAALAHQAOAGAAALQAAALgJAAQgEAAgIgEQgIgDgFAAQgVABAAAQQAAAKAHAGQAHAFAKAAQAKAAAVgFIAGAAQAOAAAAAKQAAALgMAFQgfANg1gBIgoAAQgNAvg5AXIgHABQgGgBgDgDgAAMgRQgDADAAAEQAAAHAKACIAPABQAAgIgEgFQgEgHgHAAQgEAAgDADgAh0A3QABgMATgCIAegDQAWgFAAgGQACgOgggaQgLgKABgHQAAgGAFgEQAEgDAGAAQAPACARAaQARAZAAAQQAAATgOAKQgIAGgUAGQgNADgPABQgcgBACgPgAilAoQgKgIAAgPQAAgLAFgMQAEgJAFgGQgLgFAAgJQAAgGAFgFQAEgGAGAAQAUABAVARQAVATAAATQAAAVgPALQgMAMgSgBQgQAAgJgHgAiXAHQAAALAMAAQAFgBAFgCQAFgDAAgEQAAgLgTgIQgIAKAAAIgAiQhAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGgAi8hAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGg");
    this.shape_6.setTransform(5.6825, -3.8402, 0.8692, 0.8692);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#330000").s().p("AgbBfQgEgEAAgFQAAgJALgFQApgTAMgZQgWAAgKgGQgMgGAAgUQAAgPAJgOQAJgPAOAAQARAAALAOQAKANACATIAoAAQgHgQAAgKQAAgOAMgNQANgPAUgBQALAAALAHQAOAGAAALQAAALgJAAQgEAAgIgEQgIgDgFAAQgVABAAAQQAAAKAHAGQAHAFAKAAQAKAAAVgFIAGAAQAOAAAAAKQAAALgMAFQgfANg1gBIgoAAQgNAvg5AXIgHABQgGgBgDgDgAAMgRQgDADAAAEQAAAHAKACIAPABQAAgIgEgFQgEgHgHAAQgEAAgDADgAh0A3QABgMATgCIAegDQAWgFAAgGQACgOgggaQgLgKABgHQAAgGAFgEQAEgDAGAAQAPACARAaQARAZAAAQQAAATgOAKQgIAGgUAGQgNADgPABQgcgBACgPgAilAoQgKgIAAgPQAAgLAFgMQAEgJAFgGQgLgFAAgJQAAgGAFgFQAEgGAGAAQAUABAVARQAVATAAATQAAAVgPALQgMAMgSgBQgQAAgJgHgAiXAHQAAALAMAAQAFgBAFgCQAFgDAAgEQAAgLgTgIQgIAKAAAIgAiQhAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGgAi8hAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGg");
    this.shape_7.setTransform(5.6825, -2.8902, 0.8692, 0.8692);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape_7}, {t: this.shape_6}]}, 1).to({state: []}, 1).wait(2));

    // Layer 1
    this.instance = new lib.CachedBmp_461();
    this.instance.setTransform(-25.1, -35.05, 0.2582, 0.2582);

    this.instance_1 = new lib.Pathcopy();
    this.instance_1.setTransform(6.2, -3.4, 1.3907, 1.3907, 0, 0, 0, 19.7, 19.8);
    this.instance_1.compositeOperation = "screen";

    this.instance_2 = new lib.CachedBmp_431();
    this.instance_2.setTransform(-25.4, -35.1, 0.2582, 0.2582);

    this.instance_3 = new lib.Path_1copy();
    this.instance_3.setTransform(6.1, -3.8, 1.4695, 1.4695, 0, 0, 0, 19.7, 19.7);
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_460();
    this.instance_4.setTransform(-25.3, -35.2, 0.2582, 0.2582);

    this.instance_5 = new lib.CachedBmp_459();
    this.instance_5.setTransform(-30.05, -39.9, 0.2582, 0.2582);

    this.instance_6 = new lib.Blendcopy();
    this.instance_6.setTransform(-0.45, 1.55, 1, 1, 0, 0, 0, 41.5, 38.5);
    this.instance_6.compositeOperation = "multiply";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_6}, {t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-41.9, -39.9, 83.9, 80);


  (lib.Symbol1copy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy6("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy10, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy4("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy4, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy2("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy3, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy3("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy2, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Symbol1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Starscopy("synched", 0);
    this.instance.setTransform(2.95, 30.95);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy, new cjs.Rectangle(-115.5, -111.5, 231, 223.1), null);


  (lib.Less_1_offcopy13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol10copy6("synched", 0);
    this.instance.setTransform(-1336.55, 174.05, 14.9975, 14.9975, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1863.2, 4.8, 1051.5, 544);


  (lib.Less_1_offcopy11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol10copy4("synched", 0);
    this.instance.setTransform(-1336.55, 174.05, 14.9975, 14.9975, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1863.2, 4.8, 1051.5, 544);


  (lib.Less_1_offcopy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol10copy3("synched", 0);
    this.instance.setTransform(-1336.55, 174.05, 14.9975, 14.9975, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1863.2, 4.8, 1051.5, 544);


  (lib.Less_1_offcopy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol10copy2("synched", 0);
    this.instance.setTransform(-1336.55, 174.05, 14.9975, 14.9975, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1863.2, 4.8, 1051.5, 544);


  (lib.Less_1_offcopy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib.Symbol10("synched", 0);
    this.instance.setTransform(-1343.3, 160.05, 15, 15, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1870, -9.2, 1051.4, 544.5);


  (lib.Less_1_offcopy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol10("synched", 0);
    this.instance.setTransform(-1375.55, 201.95, 15, 15, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1902.2, 32.7, 1051.4, 544.4);


  (lib.Less_1_offcopy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol10("synched", 0);
    this.instance.setTransform(-1336.55, 174.05, 14.9975, 14.9975, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1863.2, 4.8, 1051.2, 544.3000000000001);


  (lib.Less_1_offcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol10copy("synched", 0);
    this.instance.setTransform(-1336.55, 174.05, 14.9975, 14.9975, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1863.2, 4.8, 1051.5, 544);


  (lib.Less_1_offcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib.Symbol10("synched", 0);
    this.instance.setTransform(-1315.6, 216.75, 15, 15, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1842.3, 47.5, 1051.4, 544.4);


  (lib.Less_1_offcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib.Symbol10("synched", 0);
    this.instance.setTransform(-1335.95, 282, 15, 15, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1862.6, 112.7, 1051.3999999999999, 544.5);


  (lib.Less_1_offcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol10_1("synched", 0);
    this.instance.setTransform(-1336.55, 174.05, 14.9975, 14.9975, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1863.2, 4.8, 1051.5, 544);


  (lib.Less_1_offcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib.Symbol10("synched", 0);
    this.instance.setTransform(-1384.75, 219.65, 15, 15, 0, 0, 0, -1.4, 0.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1911.4, 50.4, 1051.4, 544.4);


  (lib.All_Spacecopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_22
    this.instance = new lib.Symbol7copy2();
    this.instance.setTransform(-0.05, 615.3, 1.9367, 1.9367);
    new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib.Symbol7copy2(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_18
    this.instance_1 = new lib.Symbol15copy("synched", 0);
    this.instance_1.setTransform(1.75, -4.85, 1.2652, 1.3156, 0, 0, 0, -0.1, -0.2);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.All_Spacecopy, new cjs.Rectangle(-907.1, -738.3, 1812.7, 1471.5), null);


  (lib.AllSpace = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_9
    this.instance = new lib.Symbol5("synched", 0);
    this.instance.setTransform(-362.45, 13.65, 0.5221, 0.6873, 0, 0.767, -179.228, 0.4, -0.1);

    this.instance_1 = new lib.Symbol5("synched", 0);
    this.instance_1.setTransform(-501.8, 51.6, 0.8637, 0.9545, 0, 0.7676, -179.2287, 0.4, 0);

    this.instance_2 = new lib.Symbol5("synched", 0);
    this.instance_2.setTransform(-339.9, 716.15, 2.2128, 1.7388, 0, 0.7683, -179.2276, 0.3, -0.1);

    this.instance_3 = new lib.Symbol5("synched", 0);
    this.instance_3.setTransform(131.05, 121.3, 1.8022, 1.4161, 0, 0.768, -179.2277, 0.4, -0.1);

    this.instance_4 = new lib.Symbol5("synched", 0);
    this.instance_4.setTransform(834.2, 560.35, 1.8022, 1.4161, 0, 0.768, -179.2277, 0.4, -0.1);

    this.instance_5 = new lib.Symbol5("synched", 0);
    this.instance_5.setTransform(-27.2, -166.05, 1.8022, 1.4161, 0, 0.768, -179.2277, 0.4, -0.1);

    this.instance_6 = new lib.Symbol5("synched", 0);
    this.instance_6.setTransform(-769.05, 480.5, 2.023, 1.5896, 0, 0.7678, -179.2277, 0.4, -0.1);

    this.instance_7 = new lib.Symbol5("synched", 0);
    this.instance_7.setTransform(176.85, 728.1, 1.9502, 1.5324, 0, 0.7679, -179.2276, 0.3, -0.1);

    this.instance_8 = new lib.Symbol5("synched", 0);
    this.instance_8.setTransform(-983.5, 110.1, 1.579, 1.2407, 0, 0.7674, -179.2281, 0.4, -0.1);

    this.instance_9 = new lib.Symbol5("synched", 0);
    this.instance_9.setTransform(619.95, -38.2, 2.2128, 1.7388, 0, 0.7683, -179.2276, 0.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_9}, {t: this.instance_8}, {t: this.instance_7}, {t: this.instance_6}, {t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    // Layer_4
    this.instance_10 = new lib.Symbol39("synched", 0);
    this.instance_10.setTransform(-104.35, 792.9, 2.6727, 2.6727, 0, 0, 0, 0.1, 0);

    this.instance_11 = new lib.Symbol31("synched", 0);
    this.instance_11.setTransform(-155.2, 461.8, 3.8158, 3.8158, 0, 0, 0, -0.1, 0.3);

    this.instance_12 = new lib.CachedBmp_386();
    this.instance_12.setTransform(-938.3, 271.25, 0.5, 0.5);

    this.instance_13 = new lib.Symbol23("synched", 0);
    this.instance_13.setTransform(152.95, -262.95, 0.9127, 0.9127, 0, 0, 0, 0.1, -0.3);

    this.instance_14 = new lib.Symbol23("synched", 0);
    this.instance_14.setTransform(171.15, -282.75, 0.7498, 0.7498, 0, 0, 0, 0.1, -0.2);

    this.instance_15 = new lib.Symbol23("synched", 0);
    this.instance_15.setTransform(98.9, -285.8, 0.7498, 0.7498, 0, 0, 0, 0.1, -0.2);

    this.instance_16 = new lib.Symbol36("synched", 0);
    this.instance_16.setTransform(873.3, 286.1, 2.2189, 2.2189);

    this.instance_17 = new lib.Symbol37("synched", 0);
    this.instance_17.setTransform(522.6, 313.75, 2.1698, 2.1698, 0, 0, 0, 0, 0.1);

    this.instance_18 = new lib.Symbol22("synched", 0);
    this.instance_18.setTransform(734.4, -347.25, 2.0917, 2.0917, 0, 0, 0, 0.1, -0.1);

    this.instance_19 = new lib.Symbol22("synched", 0);
    this.instance_19.setTransform(905.5, -208.3, 1.1639, 1.1639, 0, 0, 0, 0.2, -0.1);

    this.instance_20 = new lib.Symbol22("synched", 0);
    this.instance_20.setTransform(849.25, -445.2, 2.0917, 2.0917, 0, 0, 0, 0.1, -0.1);

    this.instance_21 = new lib.Symbol22("synched", 0);
    this.instance_21.setTransform(624.55, -437.7, 1.1639, 1.1639, 0, 0, 0, 0.2, -0.1);

    this.instance_22 = new lib.Symbol22("synched", 0);
    this.instance_22.setTransform(791.7, -543.6, 1.6113, 1.6113, 0, 0, 0, 0.1, -0.1);

    this.instance_23 = new lib.Symbol22("synched", 0);
    this.instance_23.setTransform(690.15, -587.9, 1.1314, 1.1314);

    this.instance_24 = new lib.Symbol35("synched", 0);
    this.instance_24.setTransform(283.15, -261.35, 0.8733, 0.8733, 0, 0, 0, 0.1, -0.1);

    this.instance_25 = new lib.Symbol34("synched", 0);
    this.instance_25.setTransform(-638.75, 235, 1.8458, 1.8458, 0, 0, 0, 0, 0.1);

    this.instance_26 = new lib.Symbol38("synched", 0);
    this.instance_26.setTransform(-1094.75, -100.25, 1.9182, 1.9182, 0, 0, 0, -0.1, -0.1);

    this.instance_27 = new lib.Symbol23("synched", 0);
    this.instance_27.setTransform(-926.75, -293.65, 1.508, 1.508);

    this.instance_28 = new lib.Symbol23("synched", 0);
    this.instance_28.setTransform(-823.2, -320.85, 1.1097, 1.1097, 0, 0, 0, -0.1, -0.2);

    this.instance_29 = new lib.Symbol23("synched", 0);
    this.instance_29.setTransform(-1010.8, -285.75, 1.1096, 1.1096, 0, 0, 0, -0.1, -0.2);

    this.instance_30 = new lib.CachedBmp_385();
    this.instance_30.setTransform(442.7, 585.6, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_30}, {t: this.instance_29}, {t: this.instance_28}, {t: this.instance_27}, {t: this.instance_26}, {t: this.instance_25}, {t: this.instance_24}, {t: this.instance_23}, {t: this.instance_22}, {t: this.instance_21}, {t: this.instance_20}, {t: this.instance_19}, {t: this.instance_18}, {t: this.instance_17}, {t: this.instance_16}, {t: this.instance_15}, {t: this.instance_14}, {t: this.instance_13}, {t: this.instance_12}, {t: this.instance_11}, {t: this.instance_10}]}).wait(1));

    // Layer_7
    this.instance_31 = new lib.Symbol40();
    this.instance_31.setTransform(-1048.9, 1029.75, 0.8733, 0.8733);

    this.timeline.addTween(cjs.Tween.get(this.instance_31).wait(1));

    // Layer_6
    this.instance_32 = new lib.CachedBmp_456();
    this.instance_32.setTransform(-1118.8, -494.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_32).wait(1));

    // Layer_3
    this.instance_33 = new lib.CachedBmp_457();
    this.instance_33.setTransform(-1220.1, -352.95, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_33).wait(1));

    // Layer_5
    this.instance_34 = new lib.Symbol14("synched", 0);
    this.instance_34.setTransform(-537.6, -229.75, 2.4219, 1.3051, 0, 11.0206, 0, 25.4, -0.1);

    this.instance_35 = new lib.Symbol22("synched", 0);
    this.instance_35.setTransform(859.35, -502.05, 1.1693, 1.1693, 0, 0, 0, 0.3, -0.3);

    this.instance_36 = new lib.Symbol22("synched", 0);
    this.instance_36.setTransform(746.7, -502.2, 1.1693, 1.1693, 0, 0, 0, 0.3, -0.3);

    this.instance_37 = new lib.Symbol22("synched", 0);
    this.instance_37.setTransform(646.9, -502.1, 0.7811, 0.7811, 0, 0, 0, 0.3, -0.5);

    this.instance_38 = new lib.Symbol22("synched", 0);
    this.instance_38.setTransform(546.7, -484.05, 1.1693, 1.1693, 0, 0, 0, 0.3, -0.3);

    this.instance_39 = new lib.Symbol22("synched", 0);
    this.instance_39.setTransform(455.95, -488.75, 1.1693, 1.1693, 0, 0, 0, 0.2, -0.3);

    this.instance_40 = new lib.Symbol22("synched", 0);
    this.instance_40.setTransform(418.1, -478.3, 1.1693, 1.1693, 0, 0, 0, 0.3, -0.3);

    this.instance_41 = new lib.Symbol22("synched", 0);
    this.instance_41.setTransform(301.9, -427.35, 1.1693, 1.1693, 0, 0, 0, 0.2, -0.3);

    this.instance_42 = new lib.Symbol22("synched", 0);
    this.instance_42.setTransform(183, -451.75, 1.1693, 1.3376, 0, 0, 0, 0.1, -0.1);

    this.instance_43 = new lib.Symbol22("synched", 0);
    this.instance_43.setTransform(107.05, -440.15, 1.1693, 1.1693, 0, 0, 0, 0.1, -0.3);

    this.instance_44 = new lib.Symbol22("synched", 0);
    this.instance_44.setTransform(-10.3, -418.55, 1.1693, 1.1693, 0, 0, 0, -0.1, -0.4);

    this.instance_45 = new lib.Symbol22("synched", 0);
    this.instance_45.setTransform(-121.3, -418.25, 1.1693, 1.1693, 0, 0, 0, -0.1, -0.3);

    this.instance_46 = new lib.Symbol22("synched", 0);
    this.instance_46.setTransform(13.1, -432.85, 1.1693, 1.2849, 0, 0, 0, 0.4, -0.3);

    this.instance_47 = new lib.Symbol22("synched", 0);
    this.instance_47.setTransform(-68.65, -399.1, 1.1693, 1.0536, 0, 0, 0, 0.1, -0.4);

    this.instance_48 = new lib.Symbol22("synched", 0);
    this.instance_48.setTransform(-182.1, -433.35, 1.1693, 1.4122, 0, 0, 0, 0.1, -0.3);

    this.instance_49 = new lib.Symbol22("synched", 0);
    this.instance_49.setTransform(-281.95, -397.85, 1.1693, 1.1693, 0, 0, 0, -0.1, -0.3);

    this.instance_50 = new lib.Symbol22("synched", 0);
    this.instance_50.setTransform(-372.4, -438, 1.1693, 1.4122, 0, 0, 0, 0.1, -0.3);

    this.instance_51 = new lib.Symbol22("synched", 0);
    this.instance_51.setTransform(-410.5, -392.05, 1.1693, 1.1693, 0, 0, 0, -0.1, -0.3);

    this.instance_52 = new lib.Symbol22("synched", 0);
    this.instance_52.setTransform(-535.45, -329.35, 1.1693, 1.0882, 0, 0, 0, -0.1, -0.4);

    this.instance_53 = new lib.Symbol22("synched", 0);
    this.instance_53.setTransform(-645.4, -412.35, 1.1693, 1.3196, 0, 0, 0, -0.1, -0.4);

    this.instance_54 = new lib.Symbol22("synched", 0);
    this.instance_54.setTransform(-747.75, -352.8, 1.1693, 1.042, 0, 0, 0, -0.2, -0.4);

    this.instance_55 = new lib.Symbol22("synched", 0);
    this.instance_55.setTransform(-838.85, -440.45, 1.1693, 1.3314, 0, 0, 0, -0.3, -0.4);

    this.instance_56 = new lib.Symbol22("synched", 0);
    this.instance_56.setTransform(-949.7, -363.8, 1.1693, 1.1693, 0, 0, 0, 0, -0.3);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_56}, {t: this.instance_55}, {t: this.instance_54}, {t: this.instance_53}, {t: this.instance_52}, {t: this.instance_51}, {t: this.instance_50}, {t: this.instance_49}, {t: this.instance_48}, {t: this.instance_47}, {t: this.instance_46}, {t: this.instance_45}, {t: this.instance_44}, {t: this.instance_43}, {t: this.instance_42}, {t: this.instance_41}, {t: this.instance_40}, {t: this.instance_39}, {t: this.instance_38}, {t: this.instance_37}, {t: this.instance_36}, {t: this.instance_35}, {t: this.instance_34}]}).wait(1));

    // Layer_8
    this.instance_57 = new lib.Symbol9();
    this.instance_57.setTransform(-84.1, -484.25, 1, 1, 0, 0, 0, -417.4, -86.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_57).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.AllSpace, new cjs.Rectangle(-1230.6, -967.7, 2315, 1940.8000000000002), null);


  (lib.Symbol2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Stars("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));


    // Layer_2
    this.instance_1 = new lib._3_stae_L("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol485("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));


    // Layer_3
    this.instance_3 = new lib._3_Star_R("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol486("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));


    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol2copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Starscopy6("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1copy10();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_Lcopy6("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol483("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));


    // Layer_3
    this.instance_3 = new lib._3_Star_Rcopy6("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol484("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));


    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol2copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Starscopy4("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1copy4();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_Lcopy4("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol481("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));


    // Layer_3
    this.instance_3 = new lib._3_Star_Rcopy4("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol482("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));


    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol2copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Starscopy2("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1copy3();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_Lcopy2("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol479("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));


    // Layer_3
    this.instance_3 = new lib._3_Star_Rcopy2("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol480("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));


    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol2copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Starscopy3("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1copy2();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_Lcopy3("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol477("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));


    // Layer_3
    this.instance_3 = new lib._3_Star_Rcopy3("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol478("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));


    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol2copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // Layer_1
    this.instance = new lib._3_Starscopy("synched", 0);
    this.instance.setTransform(2.95, 50.95, 0.2172, 0.2172);

    this.movieClip_1 = new lib.Symbol1copy();
    this.movieClip_1.name = "movieClip_1";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 2).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.movieClip_1}]}, 11).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.7133,
      scaleY: 1.4347,
      x: 3,
      y: 30.95
    }, 3).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
      regX: 0,
      regY: 0,
      scaleX: 1,
      scaleY: 1.1333,
      x: 2.95,
      y: 30.95
    }, 2).to({scaleY: 1}, 1).to({_off: true}, 11).wait(1));

    // Layer_2
    this.instance_1 = new lib._3_stae_Lcopy("synched", 0);
    this.instance_1.setTransform(0, -0.05);
    this.instance_1._off = true;

    this.instance_2 = new lib.Symbol475("synched", 0);
    this.instance_2.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 5).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_1}]}, 3).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_1}]}, 6).wait(1));


    // Layer_3
    this.instance_3 = new lib._3_Star_Rcopy("synched", 0);
    this.instance_3.setTransform(-0.05, 0);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol476("synched", 0);
    this.instance_4.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_3}]}, 6).wait(1));


    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -173.4, 550.0999999999999, 331.4);


  (lib.Symbol1copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy3("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy3("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.instance_2 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy9, new cjs.Rectangle(-200.3, -172.2, 400.3, 456.9), null);


  (lib.Symbol1copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.instance_2 = new lib.C_1_ONcopy("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy8, new cjs.Rectangle(-218.9, -172.2, 400.4, 405.5), null);


  (lib.Symbol1copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy7("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy7("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.instance_2 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy7("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy7, new cjs.Rectangle(-215.4, -172.2, 400.4, 427.7), null);


  (lib.Symbol1copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy6("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy6("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.instance_2 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy6("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy6, new cjs.Rectangle(-200.6, -172.2, 400.29999999999995, 421.2), null);


  (lib.OFFcopy10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy13("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy13("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy13("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy13("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy13("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy13("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy13("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2copy6();
    this.instance_7.setTransform(77.2, -203.25, 0.4106, 0.4106, 0, 0, 0, 2.5, 44.1);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(6).to({y: -212.85}, 0).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-121.6, -222.7, 400.4, 440.4);


  (lib.OFFcopy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy11("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy11("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy11("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy11("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy11("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy11("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy5("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2copy4();
    this.instance_7.setTransform(77.2, -203.25, 0.4106, 0.4106, 0, 0, 0, 2.5, 44.1);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(6).to({y: -212.85}, 0).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-121.6, -222.7, 400.4, 440.4);


  (lib.OFFcopy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy10("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy10("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy10("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy10("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy10("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy10("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy4("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2copy3();
    this.instance_7.setTransform(77.2, -203.25, 0.4106, 0.4106, 0, 0, 0, 2.5, 44.1);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(6).to({y: -212.85}, 0).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-121.6, -222.7, 400.4, 440.4);


  (lib.OFFcopy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy9("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy6("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy6("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy6("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy6("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy6("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2copy();
    this.instance_7.setTransform(77.2, -203.25, 0.4106, 0.4106, 0, 0, 0, 2.5, 44.1);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(6).to({y: -212.85}, 0).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-121.6, -222.7, 400.4, 440.4);


  (lib.OFFcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy5("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy3("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy3("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy3("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy3("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy3("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy3("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2copy2();
    this.instance_7.setTransform(77.2, -203.25, 0.4106, 0.4106, 0, 0, 0, 2.5, 44.1);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(6).to({y: -212.85}, 0).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-121.6, -222.7, 400.4, 440.4);


  (lib.OFFcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(2));

    // K
    this.instance = new lib.Locked_1copy6("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy6();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(56,108,135,0.329)").s().p("A4nPpQiog4hThoQgbglgPgrIgDgLIgHgVQgEgSgCgSQgIhGAbg1QAEgJAGgKIgkALIgBgDIgCAAQgFgLgJgRIgagoIgFgHQgig0gNgcIgJgTQgEgNgCgNQgHgaAAgZQAAgVAFgUQAAgFACgBQAAgBAAAAQAAAAAAgBQABAAAAgBQAAAAABAAQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAAAgBQAHgSANgNIAAACIABAAQgNANgGAQIABAEQAFgRAMgNQAWgfA2gXQBMggAnAAQBGgFAfA0QAFAHAFALIABAAIAAgCQACAFAFAFIAAACIADAKIgBAAIADAEIADAHQAAADADAFQAFAFADAHQAAAAABAAQAAABAAAAQAAABABAAQAAAAAAABIAAgPIAAgGIAAgJIAFgnQAFg6AFgjIAIggQAAgDABgCQAAAAAAAAQgBAAAAgBQAAAAAAAAQAAAAAAgBIgFgFQgFgDgDgFIgCAAIgIgKIgCgGQgIgNgFgTQgEgVAAgcIAAgGIAAgHIAbjTIAKgeIAAgBQAahIAthFIAPAKQgDgPABgOQACgkAQgkQAAAAAAgBQABAAAAgBQAAAAAAgBQABAAAAAAQAihFBehCQBdhHCehBQAvgXA5gVQB7gvCEgkQAJgDAMgCIAGgDQBagVBbgSIF9gvQAKgCAKAAIAFAAQCUgLCcABIAuAAIAgACIEQAOIAaAEIADAAIAAABQABAKAFASIAEgcIBrANQGeA5FXCbIACAAIAaAMIAIAFIAfAOQAEACAEADQAIACAFADIALAHIAsAgQCIBVBlBoICJBqQAAADABACIAAACQAAAAAAAAQABABAAAAQAAAAAAAAQABAAAAAAIAAACIADAKIAAABQAFAUACATIAAAOQAAARgEARIgBAAIAAABIABAAQAAAEACADQAAAFACADIALBlQAAANACAMQABAOAAAPIAAACIACAAIgCCAIAAANQgDAOgCANQgNBLgnAsIgIAIIAAACQAPAaAIAiQADAIACAPIAIAnIAFAfIAAAqQAAAegCAaIgDASIADABIACAEQA0ANAdBiQADAGACAIIACAAIAJAiQAFAXACASQABANgBAMQAABJhLBIQhBA5hMANQgvAIgngLQgigJgSgWIgEgEIgJgBIgCAAQgPgHgOgIQgUALgQANIgdAXQgmAchWAcIgYAAQgQAAgIgEQgRgDgJAHQgngHgcgaIgPAPQhmgMhZA+Qg0AsgcARQg5AfhegMQhVgKhKgsQg2gggYgCQgsgFhEA0IgRAMIAIAOIgBACIAAgCIgIgOIgCABQgLgLgPgFQgagKgfAEQhbADhYApQglAOgKAAQgQAAgYgNQgmgOhXgXQhSgZgqgTQgsgSgcgFQgpgHgaAUIgRgCQghgWgngEQgngFgkARQgqANgQgFQgMgFgNgNIgWgVQghgUhJAaQhhAhg2gIQgFAAhdgUQg6gPgkAPIgTgSIgHAHQheBlhuAkQg+AVhIAAQhpAAh9gsgAZqM8IAHACIgCgEQgDAAgCACgAqDKuIAIAmIAAgnIgIAAgA9HI6QgTAogCAxQADguASgnIANgZIgCACIgBAAQgHAKgDAJgAd1GyQAAAAgBAAQAAAAAAAAQAAAAgBABQAAAAAAAAQAdAIAXAkQgXglgbgKgAdrGtQAAAAABAAQAAAAABAAQAAAAABABQAAAAAAAAQABABAAAAQAAAAAAABQABAAAAABQAAAAAAABIABAAIAAgCIgDgCIAAgBIgDgEIAAAEgAFwGjIAPgDIgDgEQgDgDgCAAIgNgIQACAGAEAHIAAACIgBAAIABADgAFsGbIgFgKIAAgCIgCAAIAHAMgA/OEDQAAAAAAAAQgBAAAAABQAAAAAAAAQAAAAAAABQgCADAAADIAAACQgEAOAAAQQABgNADgOIADgIQAAAAAAgBQABAAAAAAQAAgBAAAAQABAAAAAAIABAAIgBgDIgCgBgAxiEGIAAAHIAAACIAAAEQAFgIAAgFIAAgDgA6LDSQACAFAFAFIgCgDIgFgHgAdGCeQAGAKAFALQgFgNgGgLgAGeBRIAAgNIgHAAIgBAAQABAFAHAIgATeAHIAAABIAAADQAAgBAFAAIAAgCIgHgcQACANAAAOgAoHgCIADgIIAAgCIgCAAIgBAGIABgBgAxVhZIAAACIALgqIgFgCQgGAUAAAWgAE0kxIAAgNIgGAAgAlMk2QABgFAFgDQAKgSALgRIgDAAIgNgBIgFAaIAAAFIgCABIAAgBQAAgMAFgTIgGAAgAWVnMIgDAGQAFgDAIgDIgBgEIAAgBIgEgQQACAJgHAMgAvGoCIAAADIATgsQgVAVACAUgAHdrJQAAAFACADIAIAQQAAABAAAAQABABAAAAQAAABAAAAQABAAAAABIABgsQgDgCgFAAIgDAAQgDAKABAIgAANrgIAEAAIACAAIAAgFQgCgDAAgDQgDgHgDgDg");
    this.shape.setTransform(78.2625, 114.0306);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy6("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy7("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy7("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy7("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy7("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy7("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy7("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_4}]}, 1).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-122.6, -221.9, 401.70000000000005, 510.79999999999995);


  (lib.OFFcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(1));

    // Layer_3
    this.instance = new lib.Less_1_offcopy2("synched", 0);
    this.instance.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(19));

    // C
    this.instance_1 = new lib.Tween1copy("synched", 0);
    this.instance_1.setTransform(78.7, 76.2);
    this.instance_1._off = true;

    this.instance_2 = new lib.Tween2copy("synched", 0);
    this.instance_2.setTransform(78.7, -24.7);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween3copy("synched", 0);
    this.instance_3.setTransform(78.7, -127.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween4copy("synched", 0);
    this.instance_4.setTransform(78.6, -71.2);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween5copy("synched", 0);
    this.instance_5.setTransform(78.7, -96);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween6copy2("synched", 0);
    this.instance_6.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_1}]}, 2).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(7));

    // Layer_2
    this.instance_7 = new lib.Symbol2();
    this.instance_7.setTransform(77.2, -203.25, 0.4106, 0.4106, 0, 0, 0, 2.5, 44.1);
    this.instance_7._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(12).to({_off: false}, 0).wait(6).to({y: -212.85}, 0).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-121.6, -222.7, 400.4, 440.4);


  (lib.OFFcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(2));

    // K
    this.instance = new lib.Locked_1copy4("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).wait(2));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(56,108,135,0.329)").s().p("A4hPiQing3hShoQgcglgPgqIgDgLIgGgVQgFgSgCgSQgIhGAcgzQADgKAGgKIgjAMIgCgEIgBAAQgFgLgKgQIgagpIgFgGQgig0gNgbQgFgKgDgKQgFgNgBgNQgHgaAAgYQAAgVAFgTQAAgFACgCIABgDIACgDQAGgSANgNIAAACIACAAQgNAMgGARIABADQAFgQALgNQAXgfA1gXQBNggAmAAQBGgFAfA0QAFAGAEAMIACAAIAAgCQACAFAFAFIAAABIADAKIgCAAIADAFIAEAGQAAAEADAFQAFAEADAHQAAAAAAABQAAAAAAAAQABABAAAAQAAABABAAIAAgOIAAgHIAAgIIAEgnQAFg6AFgiIAIggQAAgEACgBQgBAAAAgBQAAAAAAAAQgBAAAAAAQAAgBAAAAIgFgFIgIgIIgBAAIgIgKIgCgGQgIgNgFgSQgFgVAAgcIAAgGIAAgHIAcjSIAJgdIAAgCQAahHAuhEIAOAJQgDgOACgPQABgjAQgkIACgDQAihEBehCQBchGCdhBQAvgWA5gVQB7gvCDgkQAJgDAMgCIAGgDQBZgVBbgSIF8gvQAKgBAKAAIAEAAQCUgMCcACIAtAAQAQAAAQABIEPAPIAaADIADAAIAAACQABAKAFARIAEgbIBqANQGdA4FWCaIABAAIAaAMIAIAEIAfAPQAFACADADQAIABAFAEIALAGIAsAgQCIBVBkBnICIBpQAAAEACABIAAACQAAAAAAAAQAAABAAAAQAAAAABAAQAAABAAAAIAAABIAEAKIAAABQAFAUABATIAAANQAAASgDAQIgCAAIAAACIACAAQAAADACADQAAAFABADIALBlQAAANACALQACAPAAAOIAAACIABAAIgBCAIAAANQgEANgBANQgNBLgnArIgIAIIAAACQAPAaAIAiQADAIABAOIAJAnIAEAfIAAAqQAAAdgBAaIgDASQAAAAAAAAQABABAAAAQABAAAAAAQAAABABAAIABADQA0ANAdBhQAEAGABAIIACAAIAKAiQAEAXACASQACANgCALQAABJhKBHQhBA5hMANQgvAIgngLQgigJgSgWIgDgDQgFAAgFgCIgBAAIgegPQgTAMgQANIgdAWQglAchWAbIgZAAQgQAAgIgDQgQgDgKAGQgngGgbgaIgPAPQhmgMhZA+QgzArgcARQg5AehdgLQhVgKhKgrQg1ghgZgBQgrgFhEA0IgRALIAJAOIgCACIAAgCIgIgOIgCACQgLgMgPgFQgagJgeADQhbADhXApQgmAOgJAAQgQAAgZgNQglgOhXgXQhSgYgqgUQgsgSgbgEQgpgHgaAUIgRgCQghgXgngDQgmgFgkAQQgqANgQgFQgMgEgNgNIgWgVQghgUhIAaQhiAgg1gIQgFAAhcgTQg6gPgkAPIgTgSIgHAHQheBkhuAkQg9AVhIAAQhoAAh9gsgAZkM2IAGACIgBgDQgEAAgBABgAqAKqIAIAlIAAgnIgIAAgA9AI3QgTAmgCAxQAEgtARgnIANgYIgBABIgCAAQgGAKgEAKgAdtGvQAAAAAAAAQgBAAAAAAQAAAAAAAAQAAABAAAAQAdAIAWAkQgWglgcgKgAdkGqIADABIABAEIACAAIAAgCQAAAAgBAAQAAAAgBAAQAAgBAAAAQgBAAAAgBIAAgBIgDgDIAAADgAFvGgIAPgDIgEgDQgDgEgCAAIgMgIQABAHAFAGIAAACIgCAAQAAAAAAABQAAAAABAAQAAABAAAAQABABAAAAgAFqGYQgBgFgEgFIAAgBIgBAAIAGALgA/GECQgBAAAAAAQAAAAAAAAQgBAAAAAAQAAABAAAAQgBADAAAEIAAABQgEAOgBAQQABgNAEgOIADgIQAAAAAAgBQAAAAAAAAQABAAAAAAQAAAAABAAIABAAIgBgDIgCgCgAxeEFIAAAGIAAACIAAAFQAFgIAAgFIAAgDgA5/DZIAAgCIgFgGQACAEADAEgAc+CdQAHAKAFALQgFgNgHgLgAGcBRIAAgNIgGAAIgCAAQACAFAGAIgATZAHIAAABIAAAEQAAgCAFAAIAAgCIgHgcQACANAAAOgAoFgCIADgIIAAgCIgCAAIgBAHIABgCgAxRhYIAAACIAMgqIgFgCQgHAUAAAWgAEzkvIAAgNIgGAAgAlLk0QABgFAFgDQAKgSALgQIgDAAQgGAAgHgCIgFAaIAAAFIgBACIAAgCQAAgLAFgUIgHAAgAWQnJIgDAHQAEgEAJgDIgCgDIAAgCIgDgQQABAKgGALgAvCn+IAAADIATgsQgVAVACAUgAHbrEQAAAFACADIAIARIACADIABgsQgDgBgFAAIgDAAQgDAJABAIgAANraIAEAAIACAAIAAgFQgCgDAAgEQgDgGgDgDg");
    this.shape.setTransform(85.625, 131.1938);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

    // Layer_3
    this.instance_1 = new lib.Less_1_offcopy4("synched", 0);
    this.instance_1.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(20));

    // C
    this.instance_2 = new lib.Tween1copy5("synched", 0);
    this.instance_2.setTransform(78.7, 76.2);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween2copy5("synched", 0);
    this.instance_3.setTransform(78.7, -24.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween3copy5("synched", 0);
    this.instance_4.setTransform(78.7, -127.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween4copy5("synched", 0);
    this.instance_5.setTransform(78.6, -71.2);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween5copy5("synched", 0);
    this.instance_6.setTransform(78.7, -96);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween6copy11("synched", 0);
    this.instance_7.setTransform(78.7, -120.65);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).wait(8));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -120.65
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-114.4, -221.9, 401.20000000000005, 456.9);


  (lib.OFFcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(2));

    // K
    this.instance = new lib.Locked_1copy3("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy9();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(56,108,135,0.329)").s().p("A4nPXQing2hThnQgcgkgOgqIgDgLIgHgVQgFgSgBgRQgJhFAcgzQADgKAHgKIgkAMIgCgEIgBAAQgFgLgKgQIgagoQgBgDgEgDQgig0gNgbQgFgJgDgKQgFgNgBgNQgHgZAAgYQAAgVAFgTQAAgFACgCIABgDIACgDQAGgSANgNIAAACIACAAQgNANgHAQQAAAAAAABQAAAAABAAQAAABAAAAQABAAAAABQAFgQALgNQAXgeA2gXQBMggAnAAQBGgFAfA0QAFAGAEALIACAAIAAgBQACAFAFAEIAAACIADAKIgCAAIADAEQACACACAFQAAADADAFQAFAFADAGQAAAAAAABQAAAAABABQAAAAAAAAQAAABABAAIAAgOIAAgHIAAgIIAFgmQAEg6AFghIAIggQAAgEACgBQAAAAgBAAQAAAAAAgBQgBAAAAAAQAAAAAAgBIgEgFQgFgDgEgFIgBAAIgIgJIgCgHQgIgMgFgTQgFgVAAgbIAAgGIAAgHIAcjQIAKgdIAAgBQAahHAthDIAPAKQgEgPACgOQACgjAQgkIABgDQAjhDBehCQBchFCehAQAvgWA5gVQB7guCEgkIAVgFIAGgDQBagVBbgRIF9gvIAUgBIAFAAQCUgLCcABIAuAAIAgACIEPAOIAaADIAEAAIAAACQABAKAFARIADgbIBsANQGeA4FWCYIACAAQANAFANAGIAIAFIAfAOIAIAFQAIACAFADIAMAGIArAhQCJBTBlBmICIBoQAAAEACABIAAACQAAAAAAAAQAAABAAAAQAAAAAAAAQABAAAAAAIAAACIADAKIAAABQAFATACAUIAAANQAAARgDAQIgCAAIAAACIACAAIABAGQAAAFACADQAGA/AFAkQAAANACAMQACAOAAAOIAAACIABAAIgBB+IAAANQgEAOgBANQgNBJgnArIgIAIIAAACQAOAaAIAhQAEAIABAPIAIAmIAFAfIAAApQAAAdgBAaIgEARQABABAAAAQAAAAABAAQAAABABAAQAAAAABAAIABADQA0ANAdBgQAEAHABAIIACAAIAKAhQAFAXABARQACANgCALQAABIhLBHQhBA4hMANQgvAIgngMQgigIgSgWIgDgDQgFAAgFgCIgCAAIgdgOQgTALgRANIgdAWQglAbhWAcIgZAAQgQAAgIgDQgQgEgKAHQgngHgbgZIgPAOQhmgLhaA9Qg0ArgbAQQg5AehegLQhVgJhLgsQg1gggZgBQgrgFhFAzIgQALIAIAPIgBABIAAgBIgIgPIgCACQgLgLgPgFQgagKgfAEQhbADhYAoQglAOgKAAQgQAAgYgNQgmgOhXgWQhSgYgqgUQgsgRgcgFQgpgGgZATIgSgCQghgWgngDQgngFgkAQQgqANgQgFQgLgFgNgNIgXgVQghgThJAaQhhAgg2gIQgFAAhcgTQg7gPgjAPIgUgSIgGAGQhfBkhuAjQg+AVhJAAQhoAAh9gsgAZqMuQADABADAAIgBgDQgEAAgBACgAqDKiQAEALAFAaIAAgmIgJAAgA9GIxQgUAmgBAwQADgtASgmIANgYIgCABIgCAAQgGAKgDAKgAd0GqQAAAAgBAAQAAAAAAAAQgBABAAAAQAAAAAAABQAeAIAWAjQgWglgcgKgAdqGlIAEACIABADIACAAIAAgCIgDgBIAAgCIgEgDIAAADgAFwGcIAPgEIgDgDQgEgDgBAAIgNgIQABAGAFAHIAAABIgBAAIABAEgAFrGUIgEgKIAAgCIgCAAIAGAMgA/ND/QgBAAAAAAQAAAAAAAAQgBAAAAABQAAAAAAAAQgBADAAAEIAAABQgEAOgBAQQABgNAEgOIADgIQAAAAAAAAQAAgBAAAAQAAAAAAAAQABAAAAAAIACAAQAAgBgBAAQAAAAAAgBQgBAAAAAAQAAgBAAAAIgBgCgAxiECIAAAGIAAACIAAAFQAFgIAAgFIAAgDgA6KDPQABAEAEAEIAAgCIgFgGgAdFCbQAGAKAFALQgFgNgGgLgAGeBQIAAgNIgHAAIgBAAQABAFAHAIgATdAGIAAACIAAADQAAgBAFAAIAAgCIgGgcQABANAAANgAoHgCIADgIIAAgCIgBAAIgCAHIACgCgAxVhXIAAABIAMgpIgFgCQgHATAAAXgAE0ksIAAgNIgGAAgAlMkxQABgFAFgDQAKgSALgQIgDAAQgGAAgHgBIgFAZIAAAFIgBACIAAgCQAAgLAEgTIgGAAgAWVnEIgDAGQAFgDAIgDIgCgEIAAgBIgDgQQABAJgGAMgAvGn5IAAADIAUgrQgVAVABATgAHdq9QAAAFACADIAIAQQAAABAAAAQAAAAAAABQAAAAABABQAAAAABABIABgsQgDgBgFAAIgDAAQgDAJABAIgAANrTIAEAAIACAAIAAgFQgCgDAAgDQgDgHgDgDg");
    this.shape.setTransform(78.9875, 155.9227);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy3("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy4("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy4("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy4("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy4("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy4("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy10("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({
      _off: true,
      y: -24.7
    }, 2).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(2).to({_off: false}, 2).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-121.8, -221.9, 401.6, 550.2);


  (lib.OFF = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(2));

    // K
    this.instance = new lib.Locked_1copy8("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).wait(2));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(56,108,135,0.329)").s().p("A4jPcQing2hThnQgbglgPgqIgDgMIgHgUQgFgSgBgSQgIhFAbg0QADgJAHgKIgkALIgBgDIgCAAQgFgLgKgQIgagoQgBgEgDgDQgigzgNgcQgFgJgDgKQgFgNgCgNQgGgZAAgZQAAgUAEgUQAAgFACgBIACgDQAAgBAAAAQAAgBAAAAQAAgBAAAAQABgBAAAAQAHgRANgNIAAABIABAAQgNANgGAQQAAAAAAABQAAAAAAABQAAAAABABQAAAAABABQAEgRAMgMQAWgfA2gXQBMggAnAAQBGgFAeA0QAFAGAFAMIACAAIAAgCQABAFAFAFIAAABIAEAKIgCAAIADAFIADAGQAAADAEAFQAEAFAEAHIABADIAAgPIAAgGIAAgIIAFgnQAFg6AFgiIAIggQAAgDACgCQgBAAAAAAQAAAAAAAAQgBgBAAAAQAAAAAAAAIgFgFQgFgDgDgFIgCAAIgIgKIgBgGQgIgNgFgSQgFgVAAgcIAAgGIAAgGIAbjSIAKgdIAAgBQAahHAthEIAPAKQgDgOABgPQACgjAQgkIACgDQAihDBehCQBchGCdhAQAvgWA5gVQB7gvCEgjQAJgEAMgBIAGgDQBZgVBbgSIF9gvIATgBIAFAAQCUgMCcACIAtAAQAQAAARACIEOAOIAaADIADAAIAAACIAHAbIADgbIBrANQGdA4FWCZIACAAIAaALIAIAFQAPAGAQAJQAFABADADQAIACAFADIALAHIAsAgQCIBTBlBnICIBpQAAADABACIAAABQAAABAAAAQAAAAABABQAAAAAAAAQAAAAABAAIAAACIADAJIAAACQAFATACAUIAAAMQAAASgEAQIgBAAIAAACIABAAQAAADACADQAAAFACADIALBkQAAANACALQABAPAAAOIAAACIACAAIgCB/IAAANIgFAaQgNBKgmAsIgJAIIAAABQAPAaAIAiQADAIACAOIAIAnIAFAeIAAAqQAAAdgCAaIgDASIADABIACADQA0ANAdBhQADAGACAIIABAAIAKAiQAFAXACARQABANgBAMQAABIhLBHQhBA4hMANQgvAIgngLQgigIgSgXIgDgDIgKgCIgBAAIgdgOQgUALgQANIgdAWQglAchWAbIgZAAQgQAAgIgDQgQgDgKAGQgngGgbgaIgPAOQhmgLhZA9Qg0AsgcAQQg4AehegLQhVgJhKgsQg2gggYgCQgsgFhEA0IgQALIAIAPIgBABIAAgBIgJgPIgBACQgMgLgOgFQgagKgfADQhbAEhXAoQglAOgKAAQgQAAgZgNQglgOhXgXQhSgYgqgTQgsgSgbgFQgpgGgaATIgSgBQgggXgngDQgngFgkAQQgqANgQgFQgLgFgNgMIgXgVQgggUhJAaQhiAgg1gIQgFAAhcgTQg7gPgjAPIgUgSIgGAHQheBjhuAkQg+AVhJAAQhnAAh9gsgAZmMyQADABAEAAQAAAAgBgBQAAAAAAAAQAAgBgBAAQAAgBAAAAQgBAAgBAAQAAAAgBAAQgBABAAAAQgBAAAAABgAqBKmQADALAFAaIAAgnIgIAAgA9CIzQgUAngBAwQADgtASgmIANgYIgCABIgBAAQgHAKgDAJgAdwGsQgBAAAAAAQAAAAAAAAQgBABAAAAQAAAAAAABQAdAIAXAjQgXglgbgJgAdmGnQABAAAAAAQAAAAABABQAAAAAAAAQABABAAAAIACADIACAAIAAgBQgBAAAAgBQgBAAAAAAQgBAAAAAAQAAgBgBAAIAAgCIgDgDIAAADgAFwGeIAOgDIgDgEQgDgDgCAAQgGgFgHgDQACAHAFAGIAAACIgCAAQAAAAAAAAQAAABAAAAQAAAAABABQAAAAABABgAFrGWQgCgFgDgFIAAgBIgCAAIAHALgA/JEAQAAAAgBAAQAAAAAAAAQAAABAAAAQAAAAAAABQgCADAAADIAAACQgEAOgBAPQABgNAEgNIADgIQAAgBAAAAQAAAAABgBQAAAAAAAAQAAAAABAAIACAAQgBAAAAgBQgBAAAAAAQAAgBAAAAQAAgBAAAAIgCgBgAxfEDIAAAHIAAABIAAAFQAFgIAAgFIAAgDgA6GDQQABAFAFAFIgBgEIgFgHgAdBCcQAGAKAFALQgFgNgGgLgAGdBRIAAgNIgHAAIgBAAQABAFAHAIgATbAHIAAABIAAAEQAAgBAAAAQAAgBABAAQAAAAABAAQABAAABAAIAAgCIgGgcQACANAAAOgAoGgCIADgIIAAgCIgBAAIgCAHIACgCgAxShXIAAABIALgpIgFgCQgGATAAAXgAE0ktIAAgNIgHAAgAlMkyQACgFAFgDQAKgSALgQIgDAAIgNgBIgFAZIAAAFIgCACIAAgCQAAgLAFgTIgGAAgAWSnGIgDAGIAMgGIgBgDIAAgCIgDgQQABAKgGALgAvEn7IAAADIAUgrQgVAVABATgAHcq/QAAAEACAEIAIAQIABADIACgrQgDgCgFAAIgDAAQgEAKACAIgAANrWIAEAAIACAAIAAgFQgCgDAAgDQgDgHgDgDg");
    this.shape.setTransform(76.2375, 109.998);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

    // Layer_3
    this.instance_1 = new lib.Less_1_offcopy8("synched", 0);
    this.instance_1.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(20));

    // C
    this.instance_2 = new lib.Tween1copy9("synched", 0);
    this.instance_2.setTransform(78.7, 76.2);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween2copy9("synched", 0);
    this.instance_3.setTransform(78.7, -24.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween3copy9("synched", 0);
    this.instance_4.setTransform(78.7, -127.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween4copy9("synched", 0);
    this.instance_5.setTransform(78.6, -71.2);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween5copy9("synched", 0);
    this.instance_6.setTransform(78.7, -96);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween6copy9("synched", 0);
    this.instance_7.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_4}]}, 1).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).wait(8));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(3).to({_off: false}, 1).to({y: -76.2}, 1).to({
      _off: true,
      y: -127.7
    }, 1).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(5).to({_off: false}, 1).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-124.1, -221.9, 400.70000000000005, 514.7);


  (lib.B_1copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(2));

    // K
    this.instance = new lib.Locked_1copy7("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy7();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(56,108,135,0.329)").s().p("A4mPoQing4hThoQgcglgOgrIgEgLIgGgVQgFgSgCgSQgIhGAcg0QADgKAHgKIgkAMIgCgDIgBAAQgFgMgKgQIgagpIgFgGQgig0gNgcQgFgKgDgKQgFgNgBgNQgHgaAAgYQAAgVAFgUQAAgFACgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQABgBAAAAIACgDQAGgSANgNIAAACIACAAQgNANgHAQQAAAAAAABQAAAAABABQAAAAAAAAQABABAAAAQAFgQALgNQAXgfA2gXQBMggAnAAQBGgFAeA0QAFAGAFAMIACAAIAAgCQABAFAFAFIAAACIAEAJIgCAAIADAFIADAHQAAADAEAFQAFAFADAGIABAEIAAgPIAAgHIAAgIIAFgnQAFg6AFgjIAIggQAAgDACgCQgBAAAAgBQAAAAgBAAQAAAAAAAAQAAgBAAAAIgFgFQgFgDgDgFIgBAAIgJgJIgBgHQgIgNgFgTQgFgVAAgbIAAgHIAAgGIAcjUIAJgdIAAgCQAahIAuhEIAOAKQgDgPACgPQABgjARgkIABgDQAihFBehDQBdhGCdhBQAwgWA4gWQB8gvCDgkIAVgFIAHgDQBZgVBbgSIF+gvQAJgCAKAAIAFAAQCUgLCcACIAuAAIAgABIEPAPIAaADIADAAIAAACIAHAbIADgbIBrANQGeA5FXCaIABAAIAaAMIAJAFIAeAOQAFACADADQAJACAEADIAMAHIAsAgQCIBVBlBoICIBqQAAADACACIAAABQAAAAAAABQAAAAAAAAQAAAAAAAAQABABAAAAIAAACIAEAJIAAACQAEAUACATIAAANQAAASgDAQIgCAAIAAACIACAAQAAADABADQAAAFACAEIALBkQAAAOACALIACAdIAAACIABAAIgBCAIAAANQgEAOgBANQgNBLgnAsIgIAIIAAACQAOAaAIAiQAEAIABAPIAIAnIAFAfIAAAqQAAAdgBAaIgEASIAEACIABADQA0ANAdBiQAEAGABAJIACAAIAKAiQAFAXABASQACANgCALQAABJhKBIQhBA5hNANQgvAIgngLQgigJgSgWIgDgEIgKgBIgBAAIgegPQgTAMgQANIgdAWQgmAchWAcIgYAAQgQAAgJgEQgQgDgKAHQgngHgbgaIgPAPQhmgMhZA+Qg0AsgcAQQg5AfhegLQhUgKhLgsQg1gggZgCQgsgFhEA0IgQAMIAIAOIgCACIAAgCIgIgOIgBABQgMgLgOgFQgagKgfAEQhbADhYAoQglAPgKAAQgQAAgYgNQgmgPhXgWQhSgZgqgTQgsgSgcgFQgogHgaAUIgSgCQghgWgngEQgngFgjARQgrANgQgFQgLgFgNgNIgXgVQgggUhJAaQhiAhg1gIQgFAAhdgUQg6gPgkAPIgTgSIgHAHQheBlhuAjQg/AWhIAAQhoAAh9gsgAZpM7IAGACIgBgDQgDAAgCABgAqCKtIAIAmIAAgnIgIAAgA9FI6QgUAngCAxQAEguASgnIANgYIgCABIgCAAQgGAKgDAKgAdzGxQAAAAgBAAQAAAAAAAAQAAABAAAAQAAAAAAABQAdAIAWAkQgWgmgcgJgAdpGsQABAAAAAAQABAAAAAAQAAABABAAQAAAAABABIABADIACAAIAAgBQgBAAAAgBQAAAAgBAAQAAAAgBgBQAAAAAAAAIAAgCIgEgDIAAADgAFwGiIAPgDIgDgDQgEgDgBAAQgHgFgGgDQABAGAFAHIAAABIgBAAIABADgAFrGaQgBgFgEgEIAAgCIgBAAIAGALgA/MEDQgBAAAAAAQAAAAgBAAQAAAAAAABQAAAAAAABQgBADAAADIAAACQgEAOgBAQQABgOAEgNIADgIQAAgBAAAAQAAAAAAgBQAAAAAAAAQABAAAAAAIACAAQAAAAgBgBQAAAAAAgBQgBAAAAAAQAAgBAAAAIgBgCgAxhEGIAAAHIAAABIAAAFQAFgIAAgFIAAgDgA6JDSQABAFAFAEIgBgCIgFgIIAAABgAdECeQAHAKAEALQgEgNgHgLgAGeBRIAAgNIgHAAIgCAAQACAFAHAIgATdAHIAAABIAAADQAAgBAFAAIAAgCIgHgcQACANAAAOgAoHgCIADgIIAAgCIgBAAIgCAHIACgCgAxUhYIAAABIALgqIgFgCQgGAUAAAXgAE0kxIAAgNIgGAAgAlMk2QABgFAFgDQAKgSALgQIgDAAIgNgCIgFAaIAAAFIgBACIAAgCQAAgLAFgUIgHAAgAWUnMIgDAHIANgHIgCgDIAAgCIgDgQQACAKgHALgAvFoBIAAADIATgsQgVAVACAUgAHdrIQAAAFACADIAIAQQAAABAAAAQAAABAAAAQAAAAAAABQABAAAAABIACgsQgDgCgFAAIgDAAQgEAKACAIgAANrfIAEAAIACAAIAAgFQgCgDAAgDQgDgHgDgDg");
    this.shape.setTransform(64.1875, 122.0911);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy7("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy8("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy8("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy8("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy8("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy8("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy8("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({
      _off: true,
      y: -24.7
    }, 2).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(2).to({_off: false}, 2).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-136.5, -221.9, 401.4, 516.1);


  (lib.B_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_18 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(2));

    // K
    this.instance = new lib.Locked_1copy("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy8();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(56,108,135,0.329)").s().p("A4cPaQimg2hShnQgcglgOgqIgDgLIgHgVQgFgSgBgRQgIhFAbg0QADgJAHgKIgkALIgBgDIgCAAQgFgLgKgQIgZgoQgCgEgDgDQgigzgNgcIgIgTQgFgNgCgMQgGgaAAgYQAAgVAFgTQAAgFABgCIACgDIACgDQAGgSANgNIAAACIACAAQgNANgHAQIACADQAFgQALgNQAXgeA1gXQBMggAmAAQBGgFAeA0QAFAGAFALIACAAIAAgBQABAEAFAFIAAACIADAJIgBAAIADAFQACACABAFQAAADADAFQAFAFADAGQAAAAAAAAQAAABABAAQAAABAAAAQABABAAAAIAAgOIAAgHIAAgIIAFgmQAFg6AFgiIAIggQAAgDABgCQAAAAgBAAQAAAAAAAAQAAgBAAAAQAAAAAAAAIgFgFQgFgDgDgFIgCAAIgIgKIgCgGQgIgNgEgSQgFgVAAgcIAAgGIAAgGIAbjRIAKgdIAAgCQAahGAthEIAOAKQgDgPACgOQABgjAQgkQAAAAAAgBQAAAAABAAQAAgBAAAAQABgBAAAAQAihDBehCQBbhFCdhBQAvgWA4gVQB7guCCgkIAVgFIAHgDQBYgVBbgRIF7gvQAKgCAJAAIAFAAQCTgLCbACIAtAAQARAAAQABIENAPIAaADIADAAIAAACIAHAbIADgbIBqANQGcA4FUCYIABAAIAaAMIAIAEIAfAPQAFABADAEQAIABAFADIALAHIAsAgQCHBUBkBmICIBpQAAADABACIAAABQAAAAAAABQAAAAABAAQAAAAAAAAQABABAAAAIAAABIADAKIAAACQAFATACATIAAANQAAASgEAQIgBAAIAAABIABAAQAAADACAEQAAAFACADIALBjQAAANACALQABAPAAAOIAAACIACAAIgCB/IAAANQgDANgCANQgNBKgmArIgIAIIAAACQAOAZAIAiQADAIACAPIAIAmIAFAfIAAApQAAAdgCAaIgDASIADABQABAAAAABQAAAAABABQAAAAAAABQAAAAAAABQA0AMAdBhQADAGABAIIACAAIAKAiQAFAWABASQACANgCALQAABIhKBHQhAA4hMANQgvAIgngLQgigIgRgXIgEgDIgJgBIgCAAIgdgPQgTALgQANIgdAXQgmAbhVAbIgYAAQgQAAgIgDQgRgDgJAGQgngGgbgaIgPAPQhlgMhZA9Qg0AsgbAQQg5AehdgLQhUgJhKgsQg2gggYgCQgrgEhEAzIgQALIAIAPIgCABIAAgBIgIgPIgBACQgMgLgOgFQgagKgfADQhaAEhXAoQglAOgKAAQgQAAgYgNQglgOhXgXQhSgYgqgTQgrgSgcgEQgogHgaATIgRgBQghgXgmgDQgngFgkAQQgqANgQgFQgLgEgNgNIgWgVQghgThIAZQhhAgg1gIQgFAAhcgTQg6gOgkAOIgTgSIgGAHQheBjhuAkQg9AVhIAAQhnAAh9gsgAZeMwIAHACIgCgEQgDAAgCACgAp+KkQADAMAFAZIAAgmIgIAAgA85IyQgUAngBAwQADgtASgmIAMgZIgBACIgCAAQgGAKgDAJgAdnGrQgBAAAAAAQAAAAgBAAQAAABAAAAQAAAAAAABQAdAIAXAjQgXglgbgJgAddGnIADABQABAAAAABQAAAAABABQAAAAAAAAQAAABAAAAIACAAIAAgBIgEgCIAAgBIgDgEIAAAEgAFuGdIAOgDIgDgDQgDgEgCAAQgGgFgHgDQACAHAFAGIAAACIgCAAIACADgAFpGVIgFgKIAAgBIgCAAIAHALgA+/EAQgBAAAAAAQAAAAgBAAQAAAAAAAAQAAABAAAAQgCADAAAEIAAABQgDAOgBAQQABgNADgOIAEgIQAAAAAAgBQAAAAAAAAQAAAAAAAAQABAAAAAAIACAAIgCgDIgBgCgAxaEDIAAAGIAAACIAAAFQAFgIAAgFIAAgDgA5+DPQABAFAFAFIgBgDIgFgHgAc4CcQAHAKAEALQgEgNgHgLgAGbBRIAAgNIgHAAIgBAAQABAEAHAJgATVAHIAAABIAAADQAAgBAFAAIAAgCIgHgcQACANAAAOgAoEgCIAEgIIAAgCIgCAAIgCAHIACgCgAxNhXIAAACIALgqIgFgCQgGATAAAXgAEyktIAAgNIgGAAgAlKkyQACgEAEgEQAKgRALgQIgDAAIgNgCIgFAaIAAAFIgBABIAAgBQAAgMAFgTIgHAAgAWLnFIgDAGQAFgDAIgDIgCgDIAAgCIgDgQQABAKgGALgAu/n6IAAADIATgrQgVAUACAUgAHaq+QAAAFABADIAJAQQAAAAAAAAQAAABAAAAQAAABAAAAQABABAAAAIACgrQgDgCgFAAIgEAAQgDAKACAIgAANrVIAEAAIACAAIAAgEIgCgHQgDgGgDgEg");
    this.shape.setTransform(60.7875, 130.4977);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy2("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy2("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy2("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy2("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy2("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy6("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_4}]}, 1).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_5}]}, 1).to({state: [{t: this.instance_6}]}, 1).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      x: 78.65,
      y: -99.45
    }, 1).to({_off: true, x: 78.6, y: -71.2}, 1).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(7).to({_off: false}, 1).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-140, -221.9, 400.4, 482);


  (lib._6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.OFFcopy2();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off: false}, 0).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.22)").s().p("AnkEzQg0gRgZggQgJgLgEgNIgBgEIgCgGIgCgMQgDgVAJgQIADgGIgLAEIgBgBIAAAAIgFgJIgIgNIgBgCQgLgPgEgJIgCgGIgCgIQgCgIAAgIQAAgGABgGIABgCIAAgBIABgBQACgGAEgEIAAABIAAAAQgEAEgCAFIABABQABgFAEgEQAHgKAQgGQAYgKAMAAQAVgCAKAQIADAGIAAAAIAAgBIACADIAAAAIABADIAAAAIABACIABACIABACIACAEIABABIAAgFIAAgBIAAgDIABgMIADgdIADgKIAAgBIAAAAIgCgCIgCgDIgBAAIgCgDIgBgBQgCgFgCgFQgBgGAAgJIAAgBIAAgCIAIhCIADgJIAAAAQAIgWAOgVIAFADQgBgEAAgFQABgLAFgLIAAgBQALgVAdgUQAcgWAxgUQAOgHASgGQAmgPAogLIAHgBIACgCIA3gMIB2gOIAGgBIABAAQAugDAvAAIAOAAIAKABIBUAFIAIABIABAAIAAAAIACAJIABgJIAhAEQB/ARBqAwIAAAAIAIAEIADABIAJAFIADABIAEACIADABIAOAKQAqAaAfAhIAqAgIAAABIAAABIABABIAAAAIABADIAAAAIACAMIAAAFIgBAKIgBAAIAAAAIABAAIAAACIABADIADAfIABAIIAAAJIAAAAIABAAIgBAnIAAAEIgBAIQgEAXgMAOIgDACIAAAAQAFAJACAKIACAHIACAMIACAJIAAAOIgBARIgBAFIABAAIABABQAQAFAJAdIABAFIABAAIADALIACAMIAAAHQAAAXgXAWQgUASgYADQgOADgMgEQgLgCgFgHIgBgBIgDgBIgBAAIgJgEIgLAHIgJAHQgLAJgbAIIgHAAIgIAAQgFgBgDABQgMgCgIgHIgFAEQgfgDgcASQgQAOgIAFQgSAJgdgDQgagDgXgOQgQgKgIAAQgNgCgVAQIgFAEIACAEIAAABIAAgBIgDgEIAAABQgEgEgEgCQgIgDgKABQgcABgbANQgLAEgDAAQgFAAgIgDQgLgFgbgHQgZgIgNgFQgNgGgJgCQgMgCgIAHIgGgBQgKgHgMgBQgMgBgLAEQgNAEgFgBQgDgBgEgEIgHgHQgKgGgXAIQgeAKgQgDIgegGQgSgEgLAEIgGgFIgCACQgdAfgiALQgTAHgWAAQggAAgngOgAH5D+IACAAIAAAAIgCAAgAjFDSIACAMIAAgMIgCAAgAo9CvQgGAMAAAPQABgOAFgMIAEgIIAAABIgBAAIgDAGgAJLCFIAAABQAJACAHALQgHgLgJgEgAJICDIABABIABABIAAAAIAAgBIgBAAIAAgBIgBgBIAAABgABxCAIAFAAIgBgBIgCgBIgEgDIACAEIAAABIAAAAIAAAAgABwB+IgCgDIAAgBIAAAAIACAEgApmBQIgBAAIAAACIAAABIgCAJIACgJIABgCIAAAAIABAAIgBgBIAAgBgAlZBRIAAACIAAAAIAAABIACgDIAAgBgAoDBBIACACIAAgBIgCgCgAI9AwIADAHIgDgIgAB/AZIAAgEIgCAAIAAAAIACAEgAF/ACIAAAAIAAABIACAAIAAgBIgCgIIAAAIgAifAAIABgDIAAAAIgBAAIAAACIAAgBgAlVgaIAAAAIAEgNIgCgBQgCAHAAAHgABfhdIAAgEIgCAAgAhmhfIACgCIAHgKIgBAAIgEgBIgCAIIAAACIAAAAIAAAAIABgKIgCAAgAG4iNIgBACIAEgCIgBAAIAAgBIgBgFQABADgCADgAkpidIAAABIAGgNQgGAGAAAGgACTjaIAAADIADAEIAAABIABgNIgDAAIgBAAIAAAFgAAEjhIABAAIABAAIAAgCIgBgCIgCgCg");
    this.shape.setTransform(1.4125, 14.6001);
    this.shape._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1).to({_off: false}, 0).to({_off: true}, 1).wait(2));

    // Layer_3
    this.instance_1 = new lib.OFFcopy2();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-60.4, -17.4, 124.1, 64.19999999999999);


  (lib._5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.22)").s().p("AniE2QgzgRgZggQgJgMgEgNIgBgEIgCgGIgCgLQgCgWAIgQQgFAMgBAPQABgOAFgMIAEgHIAAAAIgBAAIgDAGIADgGIgLADIAAAAIgBAAIgEgJIgIgNIgCgCQgKgQgEgIIgDgGIgCgJQgCgIAAgIQAAgGACgGIAAgCIABgBIAAgBQACgGAEgEIAAABIABAAQgEAEgCAFIAAABQACgFADgEQAHgKARgHQAXgKAMAAQAVgBAKAPIADAHIAAAAIAAgBIACADIAAAAIABADIAAAAIABACIABACIABACIACAEIABABIAAgFIAAgBIAAgDIABgNIADgcIADgKIAAgBIAAgBIgCgBIgCgEIgBAAIgCgCIgBgCQgCgFgCgFQgBgGAAgIIAAgCIAAgDIAIhCIADgJIAAAAQAIgWAOgWIAEAEIAAgJQAAgLAFgMIABgBQAKgVAdgVQAdgVAwgUIAggPQAlgOApgLIAGgCIACgBIA3gLIB1gQIAGAAIACAAQAtgEAvABIAOAAIAKABIBTAEIAIABIABAAIAAAAIACAJIABgJIAhAFQB+ASBpAvIABAAIAIAEIACACIAKAEIACABIAEACIAEACIANAKQAqAaAfAhIApAgIABACIAAAAIAAABIAAAAIABAEIAAAAIACAMIAAAFIgBAKIAAAAIAAABIAAAAIABABIAAADIAEAgIAAAHIABAJIAAABIAAAAIAAAnIAAAEIgCAIQgEAYgMANIgCADIAAAAQAEAIADALIABAHIADAMIABAJIAAAOIAAARIgBAFIABABIAAABQAQAEAJAeIACAFIAAAAIADALIACAMIAAAIQAAAWgXAXQgUARgXAFQgOACgMgEQgLgCgFgHIgBgBIgDgBIgBAAIgJgEIgKAIIgJAGQgMAJgaAIIgIAAIgHgBQgFgBgDACQgMgBgIgJIgFAFQgfgEgcAUQgQAOgIAEQgRAKgdgEQgagCgXgOQgQgKgIgBQgNgCgVARIgFADIACAFIAAABIAAgBIgDgFIAAABQgEgEgEgBQgIgDgJABQgcABgbANQgMAEgDAAQgFAAgHgEQgLgFgbgGQgZgIgNgHQgNgFgIgCQgNgCgIAHIgFgBQgKgHgMgBQgMgCgLAFQgNAFgFgCQgDgCgEgDIgHgHQgKgGgXAIQgdAKgRgDIgegFQgSgFgLAFIgGgGIgBACQgdAfgiAMQgTAGgWAAQggAAgngOgAH2EBIACAAIAAgBIgCABgAjEDVIACAMIAAgNIgCAAgAJICHIgBAAQAJACAHALQgHgLgIgDgAJFCFIABAAIAAACIABAAIAAgBIgBgBIAAAAIgBgBIAAABgABxCCIAEgBIgBgBIgBgBIgEgDIACAFIAAAAIgBAAIABABgABvCAIgBgEIAAAAIgBAAIACAEgApjBRIAAAAIgBACIAAABIgBAJIABgJIABgCIABAAIAAAAIAAgBIgBgBgAlXBSIAAACIAAAAIAAABIACgDIAAgBgAoABCIACACIAAgBIgCgCgAI6AxIADAHIgDgIgAB/AZIAAgEIgCAAIgBAAIADAEgAF9ACIAAABIAAAAIACAAIAAAAIgCgIIAAAHgAieAAIABgDIAAAAIgBAAIAAACIAAgBgAlTgaIAAAAIAEgOIgCAAQgCAGAAAIgABeheIAAgEIgCAAgAhlhfIACgDIAGgLIgBAAIgEAAIgBAIIAAABIgBABIAAgBIACgJIgCAAgAG1iOIgBACIAEgCIAAgBIAAAAIgBgGQAAADgCAEgAknieIAAAAIAGgOQgHAHABAHgACSjdIAAADIADAGIAAAAIABgOIgDAAIgBAAIAAAFgAAEjkIABAAIABAAIAAgBIgBgCIgCgDg");
    this.shape.setTransform(-0.2375, 22.1501);

    this.instance = new lib.OFFcopy();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-61.7, -10.2, 123.30000000000001, 64.8);


  (lib._4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.22)").s().p("AnkEwQg0gRgZgfQgJgMgEgNIgBgDIgCgHIgCgKQgDgWAJgQIADgFIgLADIgBgBIAAAAIgFgIIgIgNIgBgCQgLgQgEgIIgCgGIgCgIQgCgIAAgHQAAgHABgGIABgCIAAgBIABgBQACgFAEgEIAAABIAAAAQgEAEgCAEIABABQABgFAEgDQAHgKAQgHQAYgKAMAAQAVgBAKAQIADAFIAAAAIAAAAIACADIAAAAIABADIAAAAIABACIABACIABACIACADIABABIAAgEIAAgCIAAgCIABgMIADgdIADgJIAAgCIAAAAIgCgCIgCgCIgBAAIgCgDIgBgCQgCgEgCgGQgBgGAAgIIAAgCIAAgCIAIhAIADgJIAAgBQAIgWAOgUIAFADIgBgJQABgLAFgLIAAgBQALgVAdgUQAcgVAxgUQAOgHASgHQAmgOAogLIAHgBIACgBIA3gMIB2gPIAGAAIABAAQAugDAvAAIAOAAIAKAAIBUAFIAIABIABAAIAAAAIACAJIABgJIAhAEQB/ASBqAvIAAAAIAIADIADACIAJAEIADACIAEABIADACIAOAKQAqAaAfAfIAqAhIAAABIAAABIABAAIAAABIABADIACAMIAAAEIgBAKIgBAAIAAABIABAAIAAACIABACIADAfIABAHIAAAJIAAABIABAAIgBAnIAAADIgBAIQgEAXgMANIgDADIAAAAQAFAIACALIACAHIACALIACAKIAAANIgBAQIgBAGIABAAIABABQAQAEAJAeIABAFIABAAIADAKIACAMIAAAIQAAAWgXAWQgUARgYAEQgOADgMgEQgLgCgFgHIgBgBIgDgBIgBAAIgJgEIgLAHIgJAHQgLAIgbAJIgHAAIgIgBQgFgBgDACQgMgCgIgIIgFAEQgfgDgcATQgQANgIAFQgSAKgdgEQgagDgXgNQgQgKgIgBQgNgBgVAQIgFADIACAFIAAAAIAAAAIgDgFIAAABQgEgEgEgBQgIgDgKABQgcABgbAMQgLAFgDAAQgFAAgIgEQgLgFgbgHQgZgHgNgGQgNgGgJgBQgMgCgIAGIgGgBQgKgHgMgBQgMgBgLAFQgNAEgFgCQgDgBgEgEIgHgHQgKgFgXAHQgeAKgQgCIgegGQgSgFgLAFIgGgFIgCABQgdAfgiALQgTAGgWAAQggAAgngNgAH5D8IACAAIAAgBIgCABgAjFDRIACALIAAgMIgCAAgAo9CtQgGAMAAAPQABgOAFgMIAEgHIAAABIgBAAIgDAFgAJLCEIAAAAQAJADAHALQgHgMgJgDgAJICCIABABIABABIAAAAIAAgBIgBAAIAAgBIgBgBIAAABgABxB/IAFgBIgBgBIgCgBIgEgCIACAEIAAAAIAAAAIAAABgABwB9IgCgDIAAAAIACADgApmBPIgBAAIAAACIAAABIgCAJIACgIIABgDIAAAAIABAAIgBgBIAAgBgAlZBQIAAACIAAAAIAAACIACgEIAAgBgAoDBAIACADIAAgBIgCgCgAI9AwIADAHIgDgIgAB/AZIAAgEIgCAAIAAAAIACAEgAF/ACIAAABIAAABIACgBIAAAAIgCgIIAAAHgAifAAIABgCIAAgBIgBAAIAAACIAAAAgAlVgaIAAABIAEgNIgCgBQgCAGAAAHgABfhcIAAgEIgCAAgAhmhdIACgDIAHgKIgBAAIgEgBIgCAIIAAACIAAAAIAAAAIABgKIgCAAgAG4iLIgBACIAEgCIgBgBIAAAAIgBgFQABADgCADgAkpibIAAABIAGgOQgGAHAAAGgACTjYIAAADIADAFIAAABIABgNIgDgBIgBAAIAAAFgAAEjeIABAAIABAAIAAgCIgBgCIgCgDg");
    this.shape.setTransform(-0.4375, 7.9911);

    this.instance = new lib.OFF();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFF();
    this.instance_1.setTransform(59.1, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-62.3, -23.7, 123.69999999999999, 63.8);


  (lib._2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.22)").s().p("Ao7FpQg8gUgfglQgKgOgFgPIgBgEIgDgIIgCgNQgDgZAKgTIADgHIgMAEIgBgBIAAAAIgGgKIgKgPIgBgCQgMgTgFgKIgDgHIgDgJQgCgKAAgJQAAgHACgHIAAgDIABgBIAAgBQADgHAFgEIAAAAIAAAAQgEAFgCAGIAAABQABgGAFgEQAIgMATgIQAcgMAOAAQAagBALASIADAHIABAAIAAgBQAAABAAAAQABABAAAAQAAAAABABQAAAAABABIAAAAIABAEIgBAAIABACIABACIABADIAEAEIAAABIAAgFIAAgCIAAgDIACgOIADgiIAEgMIAAgBIAAgBIgDgCIgDgDIAAAAIgDgDIAAgDQgEgEgCgHQgBgHAAgKIAAgCIAAgDIAKhMIADgLIAAAAQAKgaAQgZIAGAEQgCgGABgFQABgNAFgNIACgBQALgZAjgYQAigZA5gYQAQgIAVgIQAtgRAwgNIAHgBIADgBQAggIAigHICKgRIAHAAIACAAQA2gEA3AAIARAAIAMABIBiAFIAKABIABAAIAAABIADAKIABgKIAmAFQCXAUB9A4IAAAAIAKAEIACACIAMAFIADACIAEACIAEACIAQAMQAyAeAkAmIAyAmIABACIAAABIAAAAIAAABIABADIAAABQACAHAAAHIAAAFIgBAMIAAAAIAAABIAAAAIABACIAAADIAFAkIAAAJIABALIAAAAIABAAIgBAvIAAAEIgCAKQgEAbgPAQIgDACIAAABQAFAJADANIACAIIADAOIACALIAAAQIAAAUIgCAGIACABIAAABQATAFAKAjIACAFIABAAIAEAMIABAPQABAFgBAEQAAAagbAaQgXAVgcAFQgRADgOgFQgMgDgHgIIgBgBIgEgBIAAAAIgLgFIgNAJIgLAIQgNAKgfAKIgJAAIgJgBQgGgBgEACQgOgCgJgKIgGAGQglgEghAWQgSAQgKAGQgVALgigEQgfgEgbgQQgTgLgJgBQgQgCgZATIgGAEIADAGIAAAAIAAAAIgEgGIAAABQgEgEgGgCQgJgEgLACQghABggAOQgOAGgDAAQgGAAgJgFQgOgFgfgIQgdgJgQgHQgPgHgLgCQgOgCgKAHIgGAAQgMgJgOgBQgOgCgNAGQgPAFgHgCQgEgCgEgEIgIgIQgMgHgbAJQgjAMgUgDIgjgHQgVgFgNAFIgHgGIgDACQgiAlgoAMQgXAIgaAAQgmAAgtgQgAJUErIADAAIgBgBIgCABgAjpD4IADANIAAgOIgDAAgAqjDOQgIAOAAASQABgRAHgOIAEgJIAAABIgBAAIgDAHgAK1CdIgBAAQAJADAIAKQgIgLgIgDgAKxCbIACAAIAAACIABAAIAAgBIgBgBIAAAAIgCgBIAAABgACGCXIAFgBIgBgBIgCgBIgEgDIACAFIAAAAIgBAAIABABgACECUIgCgDIAAgBIgBAAIADAEgArVBeIAAAAIgBADIAAAAIgCALIACgKIABgDIACAAIAAAAIAAgBIgCgBgAmWBfIAAACIAAABIAAACIABgFIAAgBgApfBMIADAEIgBgCIgCgCgAKjA5IAFAIIgFgJgACXAeIAAgFIgDAAIgBAAIAEAFgAHEADIAAAAIAAABIACAAIAAgBIgDgJIABAJgAi8AAIABgDIAAgBIAAAAIgBADIABgBgAmSgfIAAAAIAFgPIgCAAQgDAHAAAIgABwhtIAAgFIgDAAgAh4hvIADgDIAHgMIgBAAIgFgBIgBAJIAAACIgBABIAAgBIABgLIgCAAgAIHilIgBACIAEgCIgBgBIAAgBIAAgGQAAAEgCAEgAlei5IAAACIAHgQQgHAHAAAHgACtkAIABADIADAFIABACIAAgQIgDgBIgBAAQgBAEAAADgAAFkJIACAAIAAAAIAAgBIAAgDIgDgDg");
    this.shape.setTransform(-5.8, 15.3396);
    this.shape._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1).to({_off: false}, 0).to({_off: true}, 1).wait(2));

    // Layer_2
    this.instance = new lib.B_1copy2();
    this.instance.setTransform(69.55, 0.3, 0.3655, 0.3655, 0, 0, 0, 269.3, 83);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off: false}, 0).wait(1));

    // Layer_3
    this.instance_1 = new lib.B_1copy2();
    this.instance_1.setTransform(69.55, 0.3, 0.3655, 0.3655, 0, 0, 0, 269.3, 83);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-78.7, -23.6, 146.7, 77.1);


  (lib._1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.22)").s().p("Ad9doQgzgRgaggQgIgLgFgNIgBgEIgCgGIgCgLQgCgWAIgQIADgGIgLAEIAAgBIgBAAIgEgJIgIgMIgCgCQgKgQgEgJIgDgGIgCgIQgCgIAAgHQAAgHACgGIAAgCIABgBIAAgBQACgFAEgEIAAAAIABAAQgEAEgCAFIAAABQACgFADgEQAHgJARgHQAXgKAMAAQAWgCAJAQIADAGIABABIACADIgBgBIgCgDIABAAIAAgBIACADIAAABIABADIgBAAIABABIABACIABADIADADIAAABIAAgEIAAgCIAAgDIACgMIADgcIACgKIABgCIgBAAIgBgCIgDgCIAAAAIgDgDIAAgCQgDgEgBgGQgCgHAAgIIAAgCIAAgCIAJhBIADgJIAAgBQAIgWAOgVIAEADQgBgEABgFQAAgLAFgLIABgBQAKgVAdgUQAdgWAwgUQAPgHARgGQAmgPApgLIAGgBIACgBIA4gMIB1gPIAGAAIACAAQAtgEAxABIAOAAIAKAAIBTAFIAIABIABAAIAAAAIACAJIABgJIAhAEQCAASBpAvIABAAIAIAEIACABIAKAFIACABIAEACIAEACIANAKQAqAaAfAgIAqAgIABACIAAAAIAAABIAAAAIABADIAAABIACAMIAAAEIgBAKIAAAAIAAABIAAAAIABACIAAACIAEAfIAAAIIABAJIAAAAIAAAAIAAAoIAAAEIgCAIQgEAXgMAOIgCACIAAABQAEAIADAKIABAHIADAMIABAKIAAANIAAARIgBAFIABABIAAABQAQAEAJAeIACAEIAAAAIADALIACAMIAAAIQAAAWgXAWQgUASgXAEQgPACgMgDQgKgDgGgHIgBgBIgDAAIAAAAIgJgFIgLAIIgJAHQgMAIgaAJIgIAAIgHgBQgFgBgDACQgMgCgJgIIgEAEQgggDgbATQgQANgJAFQgRAKgdgEQgagDgXgNQgRgKgHgBQgOgBgVAQIgFADIADAFIgBAAIAAAAIgCgFIgBABQgDgEgFgBQgIgDgJABQgcABgbAMQgMAFgDAAQgFAAgHgEQgMgFgbgHQgZgHgNgGQgOgGgIgBQgNgCgIAGIgFgBQgKgHgMgBQgMgBgLAFQgNAEgFgCQgEgBgEgEIgHgHQgKgGgWAIQgeAKgRgCIgegGQgSgFgLAFIgGgGIgCACQgdAfgiALQgTAHgWAAQggAAgngOgEAtcAczIACABIgBgBIgBAAgEAicAcIIADALIAAgMIgDAAgAclbkQgGAMgBAPQABgOAGgMIAEgHIgBAAIAAAAIgDAGgEAuuAa6IgBABQAJACAHALQgHgLgIgDgEAurAa5IABAAIAAABIABAAIAAAAIgBgBIAAAAIgBgBIAAABgEAnUAa2IAEgBIgBgBIgBgBIgEgDIACAEIAAABIgBAAIABABgEAnSAazIgBgDIAAAAIgBAAIACADgAb7aFIAAAAIgBACIAAABQgBAFAAAHIABgLIABgDIABAAIAAAAIAAgBIgBgBgEAgJAaGIAAACIAAAAIAAACIABgEIAAgBgEAufAZmIAEAGIgEgHgEAniAZOIAAgEIgCAAIgBAAIADAEgEAriAY3IAAABIAAABIABgBIAAAAIgCgJIABAIgEAjCAY0IABgCIAAgBIAAAAIgBACIABAAgEAgNAYaIAAAAIADgNIgBAAQgCAGAAAHgEAnBAXXIAAgEIgCAAgEAj8AXWIACgDIAGgKIgBAAIgEgBIgBAIIAAACIgBAAIAAAAIACgKIgCAAgEAsaAWoIgBACIAEgCIAAgBIAAgBIgBgFQAAADgCAEgEAg5AWXIAAABIAGgNQgHAGABAGgEAn1AVaIABADIACAFIABABIAAgOIgCAAIgBAAIgBAFgEAlmAVTIACAAIAAAAIAAgBIAAgCIgCgDgEgmrAWDIgBgCIgCAAIgBACIgBAAIAAgWIAAAAQAEgqAIg5IAAACIACABIACgBIABgCQgIA5gEAqIAAAAIAAAXIAAgBgEgxLATbIgBgBIAAgCIABgBIABAAQDOhUBeAAIACABIABACIgBABIgCABQheAAjNBTIgBABIgBgBgEAxKgc+QmhgunBgEIgBgBIgBgBIABgCIABgBQHBAEGiAuIABABIABABIgBACIgBABIgBAAg");
    this.shape.setTransform(-240.2963, -149.0218);

    this.instance = new lib.OFFcopy4();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy4();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-555.2, -340, 629.8000000000001, 382);


  (lib.Symbol12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.OFFcopy10();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off: false}, 0).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy10();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-61.5, -22, 122.8, 63.5);


  (lib.Symbol6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.OFFcopy8();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off: false}, 0).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy8();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-61.5, -22, 122.8, 63.5);


  (lib.Symbol5_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.OFFcopy7();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off: false}, 0).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy7();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-61.5, -22, 122.8, 63.5);


  (lib.Symbol4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.OFFcopy6();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off: false}, 0).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy6();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-61.5, -22, 122.8, 63.5);


  (lib.Symbol3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.OFFcopy5();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off: false}, 0).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy5();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-61.5, -22, 122.8, 63.5);


  (lib.Symbol1_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.OFFcopy3();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off: false}, 0).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy3();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-61.5, -22, 122.8, 63.5);


  (lib.N3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.B_1();
    this.instance.setTransform(128.7, 10.1, 1, 1, 0, 0, 0, 268.7, 82.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.N3, new cjs.Rectangle(-279.9, -44.7, 400.4, 207.5), null);


  (lib._3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.22)").s().p("Ao9FxQg9gUgfgnQgKgOgFgPIgBgFIgDgHIgCgOQgDgZAKgUIAEgHIgNAEIgBgBIAAAAIgGgKIgJgPIgCgCIgRgeIgDgHIgDgKQgCgJAAgJQAAgIACgHIAAgDIABgBIABgBQACgHAFgFIAAABIAAAAQgEAFgDAGIABABQACgGAEgFQAIgLAUgJQAbgMAPAAQAZgBALATIAEAGIAAAAIAAAAIADADIAAABIABAEIgBAAIACABIABADIABADIADAEIABABIAAgFIAAgDIAAgDIABgOIAEgiIADgMIAAgCIAAgBIgCgBIgDgDIgBAAIgCgEIgBgCQgDgFgCgHQgCgHAAgKIAAgDIAAgCIAKhOIAEgLIAAgBQAKgaAQgZIAFADQgBgFABgGQAAgNAGgNIABgBQAMgZAjgZQAhgaA6gYQARgIAVgIQAtgSAwgNIAHgCIADgBQAggIAhgGICLgSIAIAAIABAAQA2gEA5AAIAQAAIAMABIBjAFIAJABIABAAIAAABIADAKIABgKIAnAFQCXAVB9A5IABAAIAJAEIADACIALAFIADACIAFACIAEACIAQAMQAyAgAkAmIAyAnIABACIAAAAIAAABIAAABIABADIAAABIADAOIAAAFIgBANIgBAAIAAAAIABAAIAAADIABADIAEAlIABAJIAAALIAAAAIABAAIgBAwIAAAEIgCAKQgEAbgPARIgCADIAAAAQAFAKADAMIACAJIADAOIABAMIAAAPIAAAVIgBAGIABABIAAABQATAFALAkIACAFIAAAAIAEANIACAPIAAAJQAAAbgbAaQgYAVgcAFQgRADgOgEQgMgDgHgIIgBgCIgEAAIAAAAIgLgGIgNAJIgKAJQgOAKgfAKIgJAAIgJgBQgGgBgEACQgOgCgKgKIgFAGQgmgEggAWQgTARgKAGQgVALgigEQgfgEgbgQQgUgMgJgBQgQgBgYATIgGAEIADAFIgBABIAAgBIgDgFIgBABQgEgFgFgBQgJgEgMABQghABggAPQgNAGgEAAQgGAAgJgFQgNgFgggJQgegJgPgHQgQgHgKgBQgPgDgJAHIgHAAQgMgJgOgBQgOgCgNAGQgPAFgGgCQgEgBgFgFIgIgIQgMgHgbAJQgjAMgUgDIgjgHQgWgFgNAFIgHgGIgCACQgjAlgoANQgWAIgbAAQglAAgugQgAJWExIACABIAAgBIgCAAgAjqD9IADAOIAAgPIgDAAgAqmDSQgHAPgBASQABgRAHgOIAFgJIgBAAIgBAAIgDAHgAK3CgIgBABQALADAIANQgIgOgKgEgAK0CeIABABIAAABIABAAIAAgBIgBAAIAAgBIgBgBIAAABgACGCbIAFgCIgBgBIgCgBIgEgDIACAFIAAAAIgBAAIABACgACECYIgCgEIAAgBIAAAAIACAFgArXBgIgBAAIgBADIAAAAIgBALIABgKIACgDIAAAAIABAAIgBgBIAAgBgAmYBhIAAACIAAABIAAACIABgFIAAgBgApiBOIACADIAAgBIgCgDIAAABgAKmA6IAEAIIgEgJgACXAeIAAgFIgDAAIAAAAIADAFgAHGADIAAAAIAAABIABAAIAAgBIgCgKIABAKgAi9AAIABgDIAAgBIAAAAIgBADIABgBgAmUggIAAABIAFgQIgCAAQgDAHAAAIgABwhwIAAgFIgCAAgAh5hyIADgDIAHgMIgBAAIgFgBIgBAKIAAACIgBAAIAAAAIACgMIgCAAgAIIipIgBADIAFgDIgBgBIAAgBIgBgGQABAEgDAEgAlfi9IAAACIAHgRQgIAIABAHgACukGIAAADIADAGIABABIAAgQIgDgBIgBAAIAAAHgAAEkOIACAAIABAAIAAgCIgBgDIgCgDg");
    this.shape.setTransform(-6.6375, 17.9475);

    this.instance = new lib.N3();
    this.instance.setTransform(69.35, 0.05, 0.3654, 0.3654, 0, 0, 0, 128.8, 10.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.N3();
    this.instance_1.setTransform(69.35, 0.05, 0.3654, 0.3654, 0, 0, 0, 128.8, 10.1);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80, -20.5, 146.5, 77);


  (lib.All_Space = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_249 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(249).call(this.frame_249).wait(1));

    // Layer_14
    this.instance = new lib.Symbol12();
    this.instance.setTransform(22, -130, 1.0166, 1.0166);
    this.instance._off = true;
    this.instance.name = "6";
    this.instance.visible = false;

    new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib.Symbol12(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(228).to({_off: false}, 0).wait(22));

    // Layer_13
    this.instance_1 = new lib.Symbol6();
    this.instance_1.setTransform(-654.3, 43, 1.1176, 1.1176);
    this.instance_1._off = true;
    this.instance_1.name = "5";
    this.instance_1.visible = false;

    new cjs.ButtonHelper(this.instance_1, 0, 1, 2, false, new lib.Symbol6(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(215).to({_off: false}, 0).wait(35));

    // Layer_12
    this.instance_2 = new lib.Symbol5_1();
    this.instance_2.setTransform(-506.8, 291.1, 1.3256, 1.3256, 0, 0, 0, -0.2, 0.1);
    this.instance_2._off = true;
    this.instance_2.name = "4";
    this.instance_2.visible = false;

    new cjs.ButtonHelper(this.instance_2, 0, 1, 2, false, new lib.Symbol5_1(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(202).to({_off: false}, 0).wait(48));

    // Layer_11
    this.instance_3 = new lib.Symbol4();
    this.instance_3.setTransform(134, 72, 1.2961, 1.2961, 0, 0, 0, 0.1, 0.1);
    this.instance_3._off = true;
    this.instance_3.name = "3";
    this.instance_3.visible = false;

    new cjs.ButtonHelper(this.instance_3, 0, 1, 2, false, new lib.Symbol4(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(190).to({_off: false}, 0).wait(60));

    // Layer_10
    this.instance_4 = new lib.Symbol3();
    this.instance_4.setTransform(160, 456.9, 1.2138, 1.2138, 0, 0, 0, 0.1, 0.1);
    this.instance_4._off = true;
    this.instance_4.name = "2";
    this.instance_4.visible = false;

    new cjs.ButtonHelper(this.instance_4, 0, 1, 2, false, new lib.Symbol3(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(177).to({_off: false}, 0).wait(73));

    // Layer_9
    this.instance_5 = new lib.Symbol1_1();
    this.instance_5.setTransform(607.45, 359.3, 1.2287, 1.2287);
    this.instance_5._off = true;
    this.instance_5.name = "1";
    this.instance_5.visible = false;

    new cjs.ButtonHelper(this.instance_5, 0, 1, 2, false, new lib.Symbol1_1(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(161).to({_off: false}, 0).wait(89));

    // Layer_2
    this.instance_6 = new lib._6();
    this.instance_6.setTransform(21.6, -124.7);
    this.instance_6._off = true;
    new cjs.ButtonHelper(this.instance_6, 0, 1, 2, false, new lib._6(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(132).to({_off: false}, 0).wait(22));

    // Layer_3
    this.instance_7 = new lib._5();
    this.instance_7.setTransform(-654.65, 39.1, 1.1093, 1.1093);
    this.instance_7._off = true;
    new cjs.ButtonHelper(this.instance_7, 0, 1, 2, false, new lib._5(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(107).to({_off: false}, 0).wait(35));

    // Layer_4
    this.instance_8 = new lib._4();
    this.instance_8.setTransform(-506.3, 293.2, 1.3135, 1.3135);
    this.instance_8._off = true;
    new cjs.ButtonHelper(this.instance_8, 0, 1, 2, false, new lib._4(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(82).to({_off: false}, 0).wait(48));

    // Layer_5
    this.instance_9 = new lib._3();
    this.instance_9.setTransform(129.2, 74.2, 1.0751, 1.0751, 0, 0, 0, -5.2, 5.9);
    this.instance_9._off = true;
    new cjs.ButtonHelper(this.instance_9, 0, 1, 2, false, new lib._3(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(55).to({_off: false}, 0).wait(60));

    // Layer_6
    this.instance_10 = new lib._2();
    this.instance_10.setTransform(160.5, 453.55);
    this.instance_10._off = true;
    new cjs.ButtonHelper(this.instance_10, 0, 1, 2, false, new lib._2(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(35).to({_off: false}, 0).wait(73));

    // Layer_7
    this.instance_11 = new lib._1();
    this.instance_11.setTransform(607.3, 359.05, 1.2358, 1.2358);
    this.instance_11._off = true;
    new cjs.ButtonHelper(this.instance_11, 0, 1, 2, false, new lib._1(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(13).to({_off: false}, 0).wait(89));

    // Layer_20
    this.instance_12 = new lib.AllSpace();
    this.instance_12.setTransform(-0.6, -4.15, 0.703, 0.6426, 0, 0, 0, -62.8, 3.8);

    this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(250));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-821.5, -628.4, 1627.4, 1247.1);


// stage content:
  (lib.River2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.All_Spacecopy();
    this.instance.setTransform(675.3, 585.85, 0.7944, 0.7654, 0, 0, 0, -57.5, 20.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_1
    this.instance_1 = new lib.All_Space();
    this.instance_1.setTransform(676.15, 584.7, 0.7755, 0.7471, 0, 0, 0, -57.4, 20.6);
    this.instance_1.name = 'stages';

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    // stageBackground
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("rgba(0,0,0,0)").ss(1, 1, 1, 3, true).p("EhyDhaEMDkHAAAMAAAC0JMjkHAAAg");
    this.shape.setTransform(720, 566.5);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EhyDBaFMAAAi0JMDkHAAAMAAAC0Jg");
    this.shape_1.setTransform(720, 566.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new lib.AnMovieClip();
  p.nominalBounds = new cjs.Rectangle(709, 555.5, 742, 588.5);
// library properties:
  lib.properties = {
    id: '7A66752FB4A23549BD9FD1C980E281BA',
    width: 1440,
    height: 1133,
    fps: 24,
    color: "#FFFFFF",
    opacity: 1.00,
    manifest: [
      {src: "assets/images/rivers/CachedBmp_453.png", id: "CachedBmp_455"},
      {src: "assets/images/rivers/CachedBmp_453.png", id: "CachedBmp_454"},
      {src: "assets/images/rivers/CachedBmp_453.png", id: "CachedBmp_453"},
      {src: "assets/images/shared/1.png", id: "CachedBmp_433"},
      {src: "assets/images/rivers/CachedBmp_457.png", id: "CachedBmp_457"},
      {src: "assets/images/rivers/CachedBmp_456.png", id: "CachedBmp_456"},
      {src: "assets/images/rivers/CachedBmp_384.png", id: "CachedBmp_394"},
      {src: "assets/images/rivers/CachedBmp_384.png", id: "CachedBmp_393"},
      {src: "assets/images/rivers/CachedBmp_384.png", id: "CachedBmp_392"},
      {src: "assets/images/rivers/CachedBmp_384.png", id: "CachedBmp_391"},
      {src: "assets/images/rivers/CachedBmp_384.png", id: "CachedBmp_390"},
      {src: "assets/images/rivers/CachedBmp_384.png", id: "CachedBmp_389"},
      {src: "assets/images/rivers/CachedBmp_384.png", id: "CachedBmp_384"},
      {src: "assets/images/shared/2.png", id: "River2_atlas_1"},
      {src: "assets/images/shared/2.png", id: "River2_atlas_2"},
      {src: "assets/images/shared/2.png", id: "River2_atlas_3"},
      {src: "assets/images/rivers/River2_atlas_4.png", id: "River2_atlas_4"},
      {src: "assets/images/rivers/River2_atlas_5.png", id: "River2_atlas_5"},
      {src: "assets/images/shared/3.png", id: "River2_atlas_6"},
      {src: "assets/images/shared/3.png", id: "River2_atlas_7"},
      {src: "assets/images/rivers/River2_atlas_8.png", id: "River2_atlas_8"},
      {src: "assets/images/rivers/River2_atlas_9.png", id: "River2_atlas_9"}
    ],
    preloads: []
  };


// bootstrap callback support:

  (lib.Stage = function (canvas) {
    createjs.Stage.call(this, canvas);
  }).prototype = p = new createjs.Stage();

  p.setAutoPlay = function (autoPlay) {
    this.tickEnabled = autoPlay;
  }
  p.play = function () {
    this.tickEnabled = true;
    this.getChildAt(0).gotoAndPlay(this.getTimelinePosition())
  }
  p.stop = function (ms) {
    if (ms) this.seek(ms);
    this.tickEnabled = false;
  }
  p.seek = function (ms) {
    this.tickEnabled = true;
    this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000);
  }
  p.getDuration = function () {
    return this.getChildAt(0).totalFrames / lib.properties.fps * 1000;
  }

  p.getTimelinePosition = function () {
    return this.getChildAt(0).currentFrame / lib.properties.fps * 1000;
  }

  an.bootcompsLoaded = an.bootcompsLoaded || [];
  if (!an.bootstrapListeners) {
    an.bootstrapListeners = [];
  }

  an.bootstrapCallback = function (fnCallback) {
    an.bootstrapListeners.push(fnCallback);
    if (an.bootcompsLoaded.length > 0) {
      for (var i = 0; i < an.bootcompsLoaded.length; ++i) {
        fnCallback(an.bootcompsLoaded[i]);
      }
    }
  };

  an.compositions = an.compositions || {};
  an.compositions['7A66752FB4A23549BD9FD1C980E281BA'] = {
    getStage: function () {
      return exportRoot.stage;
    },
    getLibrary: function () {
      return lib;
    },
    getSpriteSheet: function () {
      return ss;
    },
    getImages: function () {
      return img;
    }
  };

  an.compositionLoaded = function (id) {
    an.bootcompsLoaded.push(id);
    for (var j = 0; j < an.bootstrapListeners.length; j++) {
      an.bootstrapListeners[j](id);
    }
  }

  an.getComposition = function (id) {
    return an.compositions[id];
  }


  an.makeResponsive = function (isResp, respDim, isScale, scaleType, domContainers) {
    var lastW, lastH, lastS = 1;
    window.addEventListener('resize', resizeCanvas);
    resizeCanvas();

    function resizeCanvas() {
      var w = lib.properties.width, h = lib.properties.height;
      var iw = window.innerWidth, ih = window.innerHeight;
      var pRatio = window.devicePixelRatio || 1, xRatio = iw / w, yRatio = ih / h, sRatio = 1;
      if (isResp) {
        if ((respDim == 'width' && lastW == iw) || (respDim == 'height' && lastH == ih)) {
          sRatio = lastS;
        } else if (!isScale) {
          if (iw < w || ih < h)
            sRatio = Math.min(xRatio, yRatio);
        } else if (scaleType == 1) {
          sRatio = Math.min(xRatio, yRatio);
        } else if (scaleType == 2) {
          sRatio = Math.max(xRatio, yRatio);
        }
      }
      domContainers[0].width = w * pRatio * sRatio;
      domContainers[0].height = h * pRatio * sRatio;
      domContainers.forEach(function (container) {
        container.style.width = w * sRatio + 'px';
        container.style.height = h * sRatio + 'px';
      });
      stage.scaleX = pRatio * sRatio;
      stage.scaleY = pRatio * sRatio;
      lastW = iw;
      lastH = ih;
      lastS = sRatio;
      stage.tickOnUpdate = false;
      stage.update();
      stage.tickOnUpdate = true;
    }
  }
  an.handleSoundStreamOnTick = function (event) {
    if (!event.paused) {
      var stageChild = stage.getChildAt(0);
      if (!stageChild.paused) {
        stageChild.syncStreamSounds();
      }
    }
  }


})(createjs = createjs || {}, AdobeAn = AdobeAn || {});
var createjs, AdobeAn, instance;
var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;

function initRivers() {
  return new Promise((resolve, reject) => {
    canvas = document.getElementById("canvas");
    anim_container = document.getElementById("animation_container");
    dom_overlay_container = document.getElementById("dom_overlay_container");
    var comp = AdobeAn.getComposition("7A66752FB4A23549BD9FD1C980E281BA");
    var lib = comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function (evt) {
      handleFileLoadRivers(evt, comp)
    });
    loader.addEventListener("complete", function (evt) {
      handleCompleteRivers(evt, comp).then(data => {
        resolve(true)
      });
    });
    var lib = comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  });
}

function handleFileLoadRivers(evt, comp) {
  var images = comp.getImages();
  if (evt && (evt.item.type == "image")) {
    images[evt.item.id] = evt.result;
  }
}

function handleCompleteRivers(evt, comp) {
  return new Promise((resolve, reject) => {
    var lib = comp.getLibrary();
    var ss = comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for (i = 0; i < ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet({
        "images": [queue.getResult(ssMetadata[i].name)],
        "frames": ssMetadata[i].frames
      })
    }
    var preloaderDiv = document.getElementById("_preload_div_");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.River2();
    stage = new lib.Stage(canvas);
    stage.enableMouseOver();
    createjs.Ticker.removeAllEventListeners(); // Note the function name
    //Registers the "tick" event listener.
    fnStartAnimation = function () {
      stage.addChild(exportRoot);
      createjs.Ticker.framerate = lib.properties.fps;
      createjs.Ticker.addEventListener("tick", stage);
    }
    //Code to support hidpi screens and responsive scaling.
    AdobeAn.makeResponsive(true, 'both', false, 1, [canvas, preloaderDiv, anim_container, dom_overlay_container]);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
    riverClickEvents();
    riverHoverEvents();
    resolve(true)
  });
}

function riverClickEvents() {
  //stage 1
  exportRoot.instance_1.instance_5.on("click", function (evt) {
    playAudioOnClick();
    instance = 1;
  });
  //stage 2
  exportRoot.instance_1.instance_4.on("click", function (evt) {
    playAudioOnClick();
    instance = 2;
  });
  //stage 3
  exportRoot.instance_1.instance_3.on("click", function (evt) {
    playAudioOnClick();
    instance = 3;
  });
  //stage 4
  exportRoot.instance_1.instance_2.on("click", function (evt) {
    playAudioOnClick();
    instance = 4;
  });
  //stage 5
  exportRoot.instance_1.instance_1.on("click", function (evt) {
    playAudioOnClick();
    instance = 5;
  });
  //stage 6
  exportRoot.instance_1.instance.on("click", function (evt) {
    playAudioOnClick();
    instance = 6;
  });
  //back
  exportRoot.instance.instance.on("click", function (evt) {
    playAudioOnClick();
    instance = 7;
  });
}

function riverHoverEvents() {
  //stage 1
  exportRoot.instance_1.instance_5.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 2
  exportRoot.instance_1.instance_4.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 3
  exportRoot.instance_1.instance_3.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 4
  exportRoot.instance_1.instance_2.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 5
  exportRoot.instance_1.instance_1.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 6
  exportRoot.instance_1.instance.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //back
  exportRoot.instance.instance.on("mouseover", function (evt) {
    playAudioOnHover();
  });
}
