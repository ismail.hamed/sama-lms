(function (cjs, an) {

  var p; // shortcut to reference prototypes
  var lib = {};
  var ss = {};
  var img = {};
  lib.ssMetadata = [
    {name: "Sea_atlas_1", frames: [[0, 0, 1657, 1104]]},
    {name: "Sea_atlas_2", frames: [[0, 0, 1613, 1075]]},
    {name: "Sea_atlas_3", frames: [[0, 0, 1657, 1028]]},
    {name: "Sea_atlas_4", frames: [[0, 0, 967, 1550], [969, 0, 967, 1550]]},
    {name: "Sea_atlas_5", frames: [[0, 0, 967, 1550], [969, 0, 967, 1550]]},
    {name: "Sea_atlas_6", frames: [[0, 0, 967, 1550], [969, 0, 967, 1550]]},
    {name: "Sea_atlas_7", frames: [[0, 0, 1443, 961], [0, 963, 1443, 961]]},
    {name: "Sea_atlas_8", frames: [[0, 0, 1443, 961], [0, 963, 1443, 961]]},
    {name: "Sea_atlas_9", frames: [[0, 0, 1039, 854], [1041, 0, 891, 933], [0, 856, 891, 933], [893, 935, 891, 933]]},
    {name: "Sea_atlas_10", frames: [[0, 0, 891, 933], [0, 935, 891, 933], [893, 0, 891, 933], [893, 935, 859, 899]]},
    {name: "Sea_atlas_11", frames: [[0, 0, 859, 899], [0, 901, 859, 899], [861, 0, 859, 899], [861, 901, 859, 899]]},
    {name: "Sea_atlas_12", frames: [[861, 0, 931, 761], [861, 763, 879, 752], [0, 901, 802, 797], [0, 0, 859, 899]]},
    {
      name: "Sea_atlas_13",
      frames: [[624, 684, 612, 905], [0, 628, 622, 910], [1238, 684, 612, 905], [0, 0, 996, 626], [998, 0, 845, 682]]
    },
    {
      name: "Sea_atlas_14",
      frames: [[0, 0, 609, 897], [0, 899, 609, 897], [611, 0, 609, 897], [611, 899, 1042, 524], [1222, 0, 663, 640]]
    },
    {
      name: "Sea_atlas_15",
      frames: [[665, 642, 421, 741], [1137, 1094, 421, 637], [665, 1385, 470, 636], [1088, 642, 597, 450], [1560, 1094, 434, 522], [0, 0, 663, 640], [665, 0, 663, 640], [1330, 0, 663, 640], [0, 642, 663, 640], [0, 1284, 663, 640]]
    },
    {
      name: "Sea_atlas_16",
      frames: [[396, 1729, 621, 189], [300, 244, 639, 240], [300, 0, 662, 242], [964, 0, 639, 240], [1019, 1394, 579, 189], [1344, 1585, 256, 362], [1602, 1691, 305, 212], [1600, 1135, 325, 272], [396, 1266, 331, 461], [0, 1858, 373, 176], [1582, 774, 337, 359], [0, 688, 394, 388], [792, 486, 393, 382], [1019, 1585, 323, 327], [0, 0, 298, 686], [729, 1266, 243, 243], [1602, 1409, 279, 280], [0, 1078, 394, 388], [0, 1468, 394, 388], [1605, 0, 394, 388], [396, 486, 394, 388], [396, 876, 394, 388], [792, 870, 393, 382], [1187, 242, 393, 382], [1187, 626, 393, 382], [1582, 390, 393, 382], [1187, 1010, 393, 382]]
    },
    {
      name: "Sea_atlas_17",
      frames: [[395, 245, 47, 82], [489, 0, 4, 5], [444, 245, 35, 35], [0, 245, 308, 142], [245, 0, 242, 243], [0, 0, 243, 243], [310, 245, 83, 77]]
    }
  ];


  (lib.AnMovieClip = function () {
    this.actionFrames = [];
    this.gotoAndPlay = function (positionOrLabel) {
      cjs.MovieClip.prototype.gotoAndPlay.call(this, positionOrLabel);
    }
    this.play = function () {
      cjs.MovieClip.prototype.play.call(this);
    }
    this.gotoAndStop = function (positionOrLabel) {
      cjs.MovieClip.prototype.gotoAndStop.call(this, positionOrLabel);
    }
    this.stop = function () {
      cjs.MovieClip.prototype.stop.call(this);
    }
  }).prototype = p = new cjs.MovieClip();
// symbols:


  (lib.CachedBmp_329 = function () {
    this.initialize(ss["Sea_atlas_15"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_328 = function () {
    this.initialize(ss["Sea_atlas_15"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_327 = function () {
    this.initialize(ss["Sea_atlas_2"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_326 = function () {
    this.initialize(ss["Sea_atlas_14"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_325 = function () {
    this.initialize(ss["Sea_atlas_14"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_324 = function () {
    this.initialize(ss["Sea_atlas_13"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_323 = function () {
    this.initialize(ss["Sea_atlas_13"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_322 = function () {
    this.initialize(ss["Sea_atlas_13"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_321 = function () {
    this.initialize(ss["Sea_atlas_14"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_320 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_319 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_318 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_317 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_316 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(4);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_315 = function () {
    this.initialize(ss["Sea_atlas_17"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_314 = function () {
    this.initialize(img.CachedBmp_314);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 3610, 1889);


  (lib.CachedBmp_313 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(5);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_312 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(6);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_311 = function () {
    this.initialize(ss["Sea_atlas_17"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_310 = function () {
    this.initialize(ss["Sea_atlas_14"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_309 = function () {
    this.initialize(ss["Sea_atlas_13"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_383 = function () {
    this.initialize(ss["Sea_atlas_9"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_307 = function () {
    this.initialize(ss["Sea_atlas_12"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_306 = function () {
    this.initialize(ss["Sea_atlas_17"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_305 = function () {
    this.initialize(ss["Sea_atlas_12"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_304 = function () {
    this.initialize(ss["Sea_atlas_15"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_303 = function () {
    this.initialize(ss["Sea_atlas_13"]);
    this.gotoAndStop(4);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_302 = function () {
    this.initialize(ss["Sea_atlas_12"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_301 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(7);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_300 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(8);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_299 = function () {
    this.initialize(ss["Sea_atlas_15"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_298 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(9);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_297 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(10);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_296 = function () {
    this.initialize(ss["Sea_atlas_15"]);
    this.gotoAndStop(4);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_295 = function () {
    this.initialize(ss["Sea_atlas_17"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_294 = function () {
    this.initialize(ss["Sea_atlas_14"]);
    this.gotoAndStop(4);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_293 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(11);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_292 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(12);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_382 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(13);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_363 = function () {
    this.initialize(ss["Sea_atlas_7"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_362 = function () {
    this.initialize(ss["Sea_atlas_7"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_361 = function () {
    this.initialize(ss["Sea_atlas_8"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_360 = function () {
    this.initialize(ss["Sea_atlas_8"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_381 = function () {
    this.initialize(ss["Sea_atlas_3"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_279 = function () {
    this.initialize(ss["Sea_atlas_1"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_278 = function () {
    this.initialize(img.CachedBmp_278);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 3670, 2839);


  (lib.CachedBmp_277 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(14);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_380 = function () {
    this.initialize(ss["Sea_atlas_17"]);
    this.gotoAndStop(4);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_275 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(15);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_379 = function () {
    this.initialize(ss["Sea_atlas_17"]);
    this.gotoAndStop(5);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_378 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(16);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_267 = function () {
    this.initialize(ss["Sea_atlas_4"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_266 = function () {
    this.initialize(ss["Sea_atlas_4"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_265 = function () {
    this.initialize(ss["Sea_atlas_5"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_264 = function () {
    this.initialize(ss["Sea_atlas_5"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_263 = function () {
    this.initialize(ss["Sea_atlas_6"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_262 = function () {
    this.initialize(ss["Sea_atlas_6"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_355 = function () {
    this.initialize(ss["Sea_atlas_9"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_354 = function () {
    this.initialize(ss["Sea_atlas_9"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_353 = function () {
    this.initialize(ss["Sea_atlas_9"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_352 = function () {
    this.initialize(ss["Sea_atlas_10"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_351 = function () {
    this.initialize(ss["Sea_atlas_10"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_261 = function () {
    this.initialize(ss["Sea_atlas_10"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_255 = function () {
    this.initialize(ss["Sea_atlas_10"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_254 = function () {
    this.initialize(ss["Sea_atlas_11"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_253 = function () {
    this.initialize(ss["Sea_atlas_11"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_252 = function () {
    this.initialize(ss["Sea_atlas_11"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_251 = function () {
    this.initialize(ss["Sea_atlas_11"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_250 = function () {
    this.initialize(ss["Sea_atlas_12"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_350 = function () {
    this.initialize(ss["Sea_atlas_15"]);
    this.gotoAndStop(5);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_349 = function () {
    this.initialize(ss["Sea_atlas_15"]);
    this.gotoAndStop(6);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_348 = function () {
    this.initialize(ss["Sea_atlas_15"]);
    this.gotoAndStop(7);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_347 = function () {
    this.initialize(ss["Sea_atlas_15"]);
    this.gotoAndStop(8);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_346 = function () {
    this.initialize(ss["Sea_atlas_15"]);
    this.gotoAndStop(9);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_345 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(17);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_344 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(18);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_343 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(19);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_342 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(20);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_341 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(21);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_340 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(22);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_339 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(23);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_338 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(24);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_337 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(25);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_336 = function () {
    this.initialize(ss["Sea_atlas_16"]);
    this.gotoAndStop(26);
  }).prototype = p = new cjs.Sprite();


  (lib.Blend_0 = function () {
    this.initialize(ss["Sea_atlas_17"]);
    this.gotoAndStop(6);
  }).prototype = p = new cjs.Sprite();

// helper functions:

  function mc_symbol_clone() {
    var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop, this.reversed));
    clone.gotoAndStop(this.currentFrame);
    clone.paused = this.paused;
    clone.framerate = this.framerate;
    return clone;
  }

  function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
    var prototype = cjs.extend(symbol, cjs.MovieClip);
    prototype.clone = mc_symbol_clone;
    prototype.nominalBounds = nominalBounds;
    prototype.frameBounds = frameBounds;
    return prototype;
  }


  (lib.Tween8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_329();
    this.instance.setTransform(-105.25, -79.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-105.2, -79.7, 210.5, 370.5);


  (lib.Tween7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_328();
    this.instance.setTransform(-105.25, -79.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-105.2, -79.7, 210.5, 318.5);


  (lib.Tween6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0004, -0.0235, 1.2751, 1.0272, 0, 0, 180);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-177.1, -59.6, 354.29999999999995, 119.2);


  (lib.Tween4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0128, -0.0036);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-138.9, -58, 277.9, 116.1);


  (lib.Tween3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0128, -0.0036);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-138.9, -58, 277.9, 116.1);


  (lib.Tween2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhhMbQg6gRgLgyIAAACQgWAPgYgOQgVgOgMgdIgCABQg3ARg1ggQg5giAVg7QgiAMgKgiQgJgcALgcQgeAdgfgUQgcgTgLgpQgMgqASgZQAWgeA4AOQADADAAACQACgMAIgJQgQgBgRgJQgSgKgHgOQACgEAAgIQAAgHABgEQgYAHgagKIAAADQAOAUgYAYQghAhg3gTQgygSgWgmQgZgqALg0QAMg4AvgPQgugBgcghIAAADQgGAhg3AJQg3AIALgwQgKADgPAAIAFAFQgLA/g3AOQgzANg9ggQg/ggggg5QgkhBAVhFIAAAAIgBABQgVgEgJgUQgJgVASgPQgcgJAAgYQABgVAUgUIgbgMQgFAFgVAQQAeASgQAbQgOAZgfAHIAAABQAMAjglASQggAPgggIQgkgJgbguQgbgvATgaIgDgDQgrAIgGgtQgHgsAjgVQAYgQAeAJQAeAJAFAbQACgVAUgNQASgMAZAAQAZAAARAMQATAOAAAXQARgRAZALIAAgCQgOgPAMgRQALgQAUgCQgqgJgEgfQgEgcAdgdQAxgvBFABQBLABAYA/QARgaAnAGQAmAHAQAbQAWAoggAYQgeAWgmgNQADATgPAQQgOAOgSADQAKAWgSAUIAAAQQATAEAMAKQAOALACARQAdgkBJABQBLABAWArQAQgSASgEIAAgDQhWgNAFhbQACgmATgkQAUglAdgVQAygiBKgEQBMgEA4AgIABgDQgBgRAOgLQANgKASAAQASABALAKQAMAMgEAUQAagEAQAMQgDgPAOgJQAMgIAVgBIABgBQgSgTANgSQANgSAXANQgQgXAbgbQAXgXAdgIQAegJAlAIQAjAHAaAUQAgAWANAtQANAsgMApIAEAAQAXACAQALQATAMABAWIAAgCQATgHAWAGQAVAGARAOQgpguA2g9QAyg5A5gCQg0gIgRguQgRgwAqglQAUgQAggFQgSgPAMgQQAMgQAXAIQgbgWAegJQAdgJAXAOQgggiApgjQAjgfAqgFQAzgEAoAjQArAngYAwIgCAAQAgAFAQAVQAOATgDAZQgEAYgTAQQgVASgfgCQAMAKAAANQAAAOgMAKQAWgFAPADQAVAEgGAVQAZgHAPAKQAOAJABATQADAvg0AOQAUAGAFAUQAFARgJAUQARgOAZgCQAYgCAUAKIACACQAJgRAHgGQgZgDgKgUQgJgRAGgVQAGgVASgLQAUgMAaAIIACAAQhWgdAhhWQANgjAdgeQAegeAfgMQBOgeBDAzQBEA0gWBRQAwgCABAnQABAlgsANQAqgFAnAfQASgoBHAGQgUgIAOgRQANgRATAAQgBgVANgKQALgIAUABQgmhFAvgbQApgXBKASQBKATAfAqQAkAxg2A0QAiAEAMAjQAMAigdAWQAdALAXAgQAaAjgHAdQgGAagnAXQgkAWgdAAQAdAfgbAwQgZAsgoAQIgVAPQAhAwgqAtQgqAtgzgaQAbAcgJAfQgHAagfARQgdAQgegFQgggGgMgfQgSAOgbAKIgEgDQAPAwgcAoQgeArg8gIIhPAaQgWAAgSgHQgXgJgFgSQgWA1hegMQhcgLgZgwIgDAAQgPAvguATQgdAMhAAGQAVBThNAjQggAPglgDQgngEgegWQghArgrgmQgrgmATgsIgCgBQgXALgWgJQAGAOgFAJQgGAIgQgCQAOAig1AWQgrATgcgGQAcAIANAgQAOAkgZAXQAcACgBAbQAAAcgeAGIgDAAQAAAKgHANQA/gFAZAwQAaAyhEAhQgaAOgeAAQgVAAgXgHgA3vk6QgEgOAIgJQAGgHAMgBQAZgDAGANQAFAMgLAPQgKAQgNAAIgCABQgOAAgIgXgAtKmMQgWgNADgZQADgZAdAGQAqAKgLAhQgFANgLAEQgFADgGAAQgIAAgJgGg");
    this.shape.setTransform(0.028, 0.0029);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-152.1, -80.2, 304.29999999999995, 160.4);


  (lib.Tween1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhhMbQg6gRgLgyIAAACQgWAPgYgOQgVgOgMgdIgCABQg3ARg1ggQg5giAVg7QgiAMgKgiQgJgcALgcQgeAdgfgUQgcgTgLgpQgMgqASgZQAWgeA4AOQADADAAACQACgMAIgJQgQgBgRgJQgSgKgHgOQACgEAAgIQAAgHABgEQgYAHgagKIAAADQAOAUgYAYQghAhg3gTQgygSgWgmQgZgqALg0QAMg4AvgPQgugBgcghIAAADQgGAhg3AJQg3AIALgwQgKADgPAAIAFAFQgLA/g3AOQgzANg9ggQg/ggggg5QgkhBAVhFIAAAAIgBABQgVgEgJgUQgJgVASgPQgcgJAAgYQABgVAUgUIgbgMQgFAFgVAQQAeASgQAbQgOAZgfAHIAAABQAMAjglASQggAPgggIQgkgJgbguQgbgvATgaIgDgDQgrAIgGgtQgHgsAjgVQAYgQAeAJQAeAJAFAbQACgVAUgNQASgMAZAAQAZAAARAMQATAOAAAXQARgRAZALIAAgCQgOgPAMgRQALgQAUgCQgqgJgEgfQgEgcAdgdQAxgvBFABQBLABAYA/QARgaAnAGQAmAHAQAbQAWAoggAYQgeAWgmgNQADATgPAQQgOAOgSADQAKAWgSAUIAAAQQATAEAMAKQAOALACARQAdgkBJABQBLABAWArQAQgSASgEIAAgDQhWgNAFhbQACgmATgkQAUglAdgVQAygiBKgEQBMgEA4AgIABgDQgBgRAOgLQANgKASAAQASABALAKQAMAMgEAUQAagEAQAMQgDgPAOgJQAMgIAVgBIABgBQgSgTANgSQANgSAXANQgQgXAbgbQAXgXAdgIQAegJAlAIQAjAHAaAUQAgAWANAtQANAsgMApIAEAAQAXACAQALQATAMABAWIAAgCQATgHAWAGQAVAGARAOQgpguA2g9QAyg5A5gCQg0gIgRguQgRgwAqglQAUgQAggFQgSgPAMgQQAMgQAXAIQgbgWAegJQAdgJAXAOQgggiApgjQAjgfAqgFQAzgEAoAjQArAngYAwIgCAAQAgAFAQAVQAOATgDAZQgEAYgTAQQgVASgfgCQAMAKAAANQAAAOgMAKQAWgFAPADQAVAEgGAVQAZgHAPAKQAOAJABATQADAvg0AOQAUAGAFAUQAFARgJAUQARgOAZgCQAYgCAUAKIACACQAJgRAHgGQgZgDgKgUQgJgRAGgVQAGgVASgLQAUgMAaAIIACAAQhWgdAhhWQANgjAdgeQAegeAfgMQBOgeBDAzQBEA0gWBRQAwgCABAnQABAlgsANQAqgFAnAfQASgoBHAGQgUgIAOgRQANgRATAAQgBgVANgKQALgIAUABQgmhFAvgbQApgXBKASQBKATAfAqQAkAxg2A0QAiAEAMAjQAMAigdAWQAdALAXAgQAaAjgHAdQgGAagnAXQgkAWgdAAQAdAfgbAwQgZAsgoAQIgVAPQAhAwgqAtQgqAtgzgaQAbAcgJAfQgHAagfARQgdAQgegFQgggGgMgfQgSAOgbAKIgEgDQAPAwgcAoQgeArg8gIIhPAaQgWAAgSgHQgXgJgFgSQgWA1hegMQhcgLgZgwIgDAAQgPAvguATQgdAMhAAGQAVBThNAjQggAPglgDQgngEgegWQghArgrgmQgrgmATgsIgCgBQgXALgWgJQAGAOgFAJQgGAIgQgCQAOAig1AWQgrATgcgGQAcAIANAgQAOAkgZAXQAcACgBAbQAAAcgeAGIgDAAQAAAKgHANQA/gFAZAwQAaAyhEAhQgaAOgeAAQgVAAgXgHgA3vk6QgEgOAIgJQAGgHAMgBQAZgDAGANQAFAMgLAPQgKAQgNAAIgCABQgOAAgIgXgAtKmMQgWgNADgZQADgZAdAGQAqAKgLAhQgFANgLAEQgFADgGAAQgIAAgJgGg");
    this.shape.setTransform(-0.022, 0.0029);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-152.1, -80.2, 304.2, 160.4);


  (lib.Symbol33 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#FFFFFF").ss(5, 1, 1).p("AaGfEQr6CcsKApQsKApsGhLQnFgskphmQmMiIjckHQhfhyhzjbQiLkHg3hSQiQjYjVicQjWidj5hJQicgphMgaQiCgthJhHQhlhjgPieQgOiVBBiLQBwjuEmiVQDMhnFrhXQHUhxEUgtQGdhEFUAMQFBAMF4BVQEYA/GRB/QFWBtCCAiQEKBGDYAQQEFASDmg9QD/hECvibQBwhkCSjXQCmj3BMhTQDzkLGkiJQE2hlHggtQKYhCLiAG");
    this.shape.setTransform(0.0017, 0.0033);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol33, new cjs.Rectangle(-459.1, -222.4, 918.3, 444.9), null);


  (lib.Symbol28 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.instance = new lib.CachedBmp_327();
    this.instance.setTransform(-64.35, -42.9, 0.0799, 0.0799);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#3860B1").s().p("Ah7CdQgNgCgKgHQgLgIgEgMQgmADgSgDQgggEgSgUQgOgPgCgWQgDgWAKgTIAGgMQAEgGABgGQADgIgBgSQACgYAYgXQAKgJATgLIARgKIA6ggQAWgMAMgEQAUgIAQAAQAUAAAQALQAQAMACASQBXgdAkgIQAjgHAQAJQAZANAGAvQAVgGAWAJQAVAJALATQAHALABANQABAJgBAJQgDAWgQAQQAPAGAHAQQAHAQgIAMQgIANgYAHQhDAVhdAQQg2AKhsARQgWAEgPAAIgNgBg");
    this.shape.setTransform(0.6373, -4.725);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#5387EE").s().p("AjAG3QhwgOhPgwQgbgQglgeQhphThMhXQgkgqgMgdQgYg2AMhSQARhpA5hJQAaggAuglQBRhBBYgnQBfgqBggGQAxgEA7AGQAqAEBCALQCAAVBXAZQBzAgBWAyQAAgKAJgGQAIgGAKAAQAOAAAWANQAgAUA9AxQAYAUAKAKQASARALARQANAUAEAVQAFAYgIAUIgFANQgCAHAAAGQAAAGAJAUQAJAUgCAbQgBASgHAfQgJAogKARQgIAQgSAUIgfAgIgWAdIgVAdQghAng1AiQgiAVhDAhQhQAogoAMQg2ARhFAGQgqAEhUABIgrAAQhkAAg6gIgAoihCQgdAdgCAiQAPAIAKAYQAIAdAGANQAMAdAmAdQAsAeATASQAKAJAUAWQASAVALAKQArAmBQAUQBKATBYACQBJACBagJQBBgGAwgLQBHgPBVgmQBNgjAmgiQA5g1AFhAQAEgogUgtQgOghggguQgjg0gbgVQgYgSgqgSQhNghhlgXQhFgQhygSQgzgIgigDQhkgGhmAlQhhAjhTBDQg/AygSAxQAqgEApgSQgrAbgPAQgAJThzQAAABABAAQAAABAAAAQABABAAAAQAAAAABAAQACgCgBgEIgDgEIgEgEIgCgDIgDgBgAkjBSQhpgtAAg/QAAhABpgtQAlgPApgLQgTAMgKAJQgYAWgCAZQABASgDAJQgBAFgEAHIgGALQgKATADAWQACAWAOAPQASATAgAFQASADAmgDQAEALALAIQAKAHANADQARADAhgGQBsgRA2gKQBdgRBDgVQAYgHAIgMQAIgMgHgQQgHgPgPgHQAQgPADgXQABgJgBgJQAzAjAAAsQAAA/hqAtQhqAtiVAAQiVAAhqgtg");
    this.shape_1.setTransform(1.1041, 2.2029);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    // Layer_3
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#FFFFFF").s().p("AjfG4QgngKg7gWQhEgagjgQQg4gbgngfQgwgng3hNQgkgxgVgoQgbgzgLgvQgKgtADgnQAEg1AehFQAYg4AbgfQAWgZAlgZQB6hSCtgYQCCgRC4AQQBJAGApAKQAcAGAwAQQA1AQAXAGQAqALBXANQBMAOAuAZQA9AgAyBEQBGBggCBiQAAAhgNA2QgNA0gSAdQgLATgSATIgiAjQhEBAgoAdQhQA6hxAhQhXAZh9AQIhLAJQhKAHg1AAQhFAAg5gOgAiqlKQhlAChRATQhOAThIAnQgxAagZAdQgSAVgQAjQgWAzACAoQACAnAeA3QBLCQBoA7QAbAQA8AYIBPAfQAtAQAlAHQBCAOBTgCQA3gCBegMQBfgLAzgOQgZgOhFgOQg+gMgcgXIA9gJQArgHAbgHIApgNIAogNQATgFAjgHIA2gMQA7gQAygiQAmgaAZggQAcglAGgnQAJgxgYgyQgWgvgrgiQghgZgngOQgdgKgtgHIhLgNQgigHhOgYQhIgXgpgHQgZgEgygEQhggHhBAAIgXAAg");
    this.shape_2.setTransform(1.2562, 5.5929);

    this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-71.3, -42.9, 145.1, 93.9);


  (lib.Symbol27 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_316();
    this.instance.setTransform(-156.85, 156.7, 0.5, 0.5);

    this.instance_1 = new lib.CachedBmp_317();
    this.instance_1.setTransform(-169.75, 134.65, 0.5, 0.5);

    this.instance_2 = new lib.CachedBmp_318();
    this.instance_2.setTransform(-173.35, 139, 0.5, 0.5);

    this.instance_3 = new lib.CachedBmp_319();
    this.instance_3.setTransform(-169.75, 134.65, 0.5, 0.5);

    this.instance_4 = new lib.CachedBmp_320();
    this.instance_4.setTransform(-156.05, 151.3, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance_1}]}, 8).to({state: [{t: this.instance_2}]}, 9).to({state: [{t: this.instance_3}]}, 8).to({state: [{t: this.instance_4}]}, 8).wait(9));

    // Layer_1_copy
    this.instance_5 = new lib.CachedBmp_321();
    this.instance_5.setTransform(-152.15, -219.6, 0.5, 0.5);

    this.instance_6 = new lib.CachedBmp_322();
    this.instance_6.setTransform(-151.1, -207.75, 0.5, 0.5);
    this.instance_6._off = true;

    this.instance_7 = new lib.CachedBmp_323();
    this.instance_7.setTransform(-146.6, -202.75, 0.5, 0.5);
    this.instance_7._off = true;

    this.instance_8 = new lib.CachedBmp_324();
    this.instance_8.setTransform(-151.1, -207.75, 0.5, 0.5);
    this.instance_8._off = true;

    this.instance_9 = new lib.CachedBmp_325();
    this.instance_9.setTransform(-152.15, -219.6, 0.5, 0.5);
    this.instance_9._off = true;

    this.instance_10 = new lib.CachedBmp_326();
    this.instance_10.setTransform(-152.15, -219.6, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_5}]}).to({state: [{t: this.instance_6}]}, 8).to({state: [{t: this.instance_7}]}, 9).to({state: [{t: this.instance_8}]}, 8).to({state: [{t: this.instance_9}]}, 8).to({state: [{t: this.instance_10}]}, 8).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).to({_off: true, x: -151.1, y: -207.75}, 8).wait(34));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).to({_off: false}, 8).to({
      _off: true,
      x: -146.6,
      y: -202.75
    }, 9).wait(25));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 9).to({
      _off: true,
      x: -151.1,
      y: -207.75
    }, 8).wait(17));
    this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(17).to({_off: false}, 8).to({
      _off: true,
      x: -152.15,
      y: -219.6
    }, 8).wait(9));
    this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(25).to({_off: false}, 8).to({_off: true}, 8).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-173.3, -219.8, 337.70000000000005, 479.8);


  (lib.Symbol19 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(0, 0, 0, 0);


  (lib.Symbol18 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.CachedBmp_315();
    this.instance.setTransform(383.5, 56.9, 0.2159, 0.2159);

    this.instance_1 = new lib.CachedBmp_314();
    this.instance_1.setTransform(-355.6, -211.2, 0.2159, 0.2159);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-355.6, -211.2, 779.4000000000001, 407.9);


  (lib.Symbol16 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#740303").ss(0.5, 1, 1).p("Ag3myQAEgIACgIQAAAAABgBQALANATAQQAMAKAOAMIADACQACABACACQAlAcAPARQAmApAVBEQAMArANBTAg4mvQAAgBABgCAg8mkQAAgBAAgBQACgEACgFAhOmAIASgkAiziSQAFg8AkhFQAFgKArhNQAGgLAGgLAiwhWQAAgBAAgBQgGgcADgeAishOQgEgBgDgBIAAgBQgKgFgJgEIg4gUIhUgMQgNABgMACQgOACgPAEQgGABgFACQgEABgFACAAbCUQgIgsgXgmQguhMhqg7QgIgEgIgFAixCbQAXgCAWADQAzAHApAeQAiAZAQAjIAAAAQABACABAEQghAegeAYIgrAiQgNAIgLAIQgDADgDACQgEACgDADQgMAHgLAHQhpA/hiACQgKAAgSgCIhMgIQgEAAgEgBQhFgJgpAHIgBAAQACgDACgDQAGgKAGgJQABgCACgDQAog7A0gxQAvgrA0ghQAOgIAPgIQAIgGAJgEQAmgTAkgLQAmgMAjgCgAAcCLQAGgsANgoQAHgWAJgUQAIgRAKgRQAKgRAMgQQAQgWAUgSQAEgDADgDQADgDADgCIAEgDQACgCADgDQARgMATgLIAUgKIBdgdQAHgCAIgBQAOgCAQgCQAxgFBAAEIABAAQABAAAAAAQAJAAAOACIBDAFQAFABAFABQABAAACAAQAFAAAFABQgEAEgEAFIgPARQgFAGgEAGQgEAFgEAFIgWAfIhBByQgMAZgKAbQAAABgBABQgCAFgCAFQgDAIgDAJQAIACAIAEIBqBBQABABACACQAoAhAqAwQAMAPANAQQizA9hZAgQgWAHgLADQgTAEgQgBQgPgCgSgHQgHgDgagNQhjg1hpgkIgWgIQgEgBgEgBQgDgBgCgEQACgGACgFIABAAQAAgBAAgBQAAAAAAgBIAAgBQABgBABgCQAAgBAAgBQAAgZABgYIABgJQABgUADgSQAAgEABgFgAAcCcQgBgEAAgEAn+g7QgBABAAAAQgHAEgGAEIh+BHAn+g7QABAAAAAAQACgCACgBAnyhCQAAAAgBABQgDABgDACAnvhEIgBABAnyhCQABgBABAAAnohIQgBABgBABQgDABgCABAnfhNQgFADgEACAnfhNIBDgeApnAmIAbAQQAHAFAHAFQAEADAHAFIA9AxIBHAyQALAGAKAFQARAIARAFIAfAGAqDAaIABABAp7AeIgHgDAp2AgQgCgCgCAAQAAAAgBAAAqOAXQABAAACABQAEABAEABAqOAXQABgBABAAQABgBABAAAp2AgQAHADAIADAAeDOIAAgIAAdDTIAAgDAAdDWQAAgCAAgBAATD9QAAgBABgCQAEgGAEgGQAFgJAGgHQAZgkAjgcQAlgdAwgUQACgBADgBQADgBADgBAAVD2QAFgQADgQAASD+QgCABgBABQgBABgCACQABACABADAASD9QAAABAAAAAAdCjQACASgBARAD/BYIg8ARAGnBkIAAABAD/BYQBSgQBOAaQAEABAEAB");
    this.shape.setTransform(0.025, -10.525);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#D7A100").s().p("AgDgRIABAAIAFAjIgGgjg");
    this.shape_1.setTransform(19.05, 53.9);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#99FF66").s().p("AAAgGIAAAAIAFAGIgEADIgFAEg");
    this.shape_2.setTransform(15.1, -21.4);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#338533").s().p("AmmEtIhMgIIgIgBQhGgJgpAGIAAAAIADgGIAMgSQAIgDALgCQC4gXCmhUIBUgrQAygXAngGIABAAIADgBIAIAAIgHgIIgHgJQgIgKgXgMQgZgOgQgDQgIgCgXgCQgUgCgLgFIgTgIIAPAAIABAAIAAAAQANAAANACIADAAQA0AGApAfQAiAZAQAiIAAAAIAAACIABAAIACAEIgDACIgGAFIgFAEIgFAFIgKAIIAAABIgRAOIgIAHIgLAJIgrAhIgXARIgHAEIAHgEIgGAEIgBAAIhCAiQggAQgVAIQgwAThAAHIgCAAIgDAAIgYgBgAkmDgIgCABIAEgCgAEwDxQgfgEgqgLIgPgIQhkg1hogkIgWgHIgHgDQgEgBgCgEIAFgKIgFAKIAEgKIAAAAIABgBIABgBIABgBIgBgBIAAAAIABgEIABgBIABgDIAAgCIABgDIAEgTIABgDIAAgCIAAgCIABgCIAAgCIABgJQAAgQgCgSIgBgHIAAAAIACgMIgCgEQAHgtAMgnQAHgWAJgUIgCAIQgKAvAGAmQA7grBEgaQASgIAUgFQBHgTBQANQAlAGAlANIgDAJIgEAIIgDAHIgBAEIgBACIgBADIgBACIAAABIgBABIgDAKIgJAAQhegGgwAEIgIABIgQAEIg7ARIglALIgHADIABAAIgIAEQgZAQgVATQgiAcgaAjIAAAAQgEAAgEgDQAAALgFAJIADgBIAEgBQA0gHBAAEQAyADBDAMQAWAEALADQATAGAMAJIANAKQAHAGAGACQALAEAUgDQAcgDAygIIBPgMIArgFIARgBIACgBIAYgDIAaAfIkNBcIghAKIgJACIgDgBgAgSBoIgBAAIABAAIAAgBgAgSBnIACgCIgCACIABgBIgBABgAgQBlIAHgNIgHANgAgRBkIACgEQAFgPACgQQgCAQgFAPIAAAAIAAgBIAAABIAAAAIgCAEgAgSB3QgDgBgCgEQACAEAEABIAHADIgIgDgAgXByIAAAAgAgbBnIAAAAgAhNAsQgpgfg0gGIgDAAQgNgCgNAAIAAAAIgBAAIgPAAIgEgBQgJgCgTAEQgTAFgWAIQgjAMgmATQgJAEgJAFIgcARIgEACIAggTIgEgHIgfgGQgQgFgRgIIABgCQBAgvAfgRQA5gfA0gEQAXgCAnAEQBEAHAkAYQAiAWAXAqIAFAIIAAAAIAAAJIgBAJIgBAQIAAAPIAAAEIAAABIAAACIAAADIgLAHQgQgigigZgAmXBMIAAAAgAl7A7QAJgFAJgEQAmgTAjgMQgUAIgWAKIhNAnIAcgRgAABBIQAagjAigcQgSATgPAXQgOAWgMAAIgBgBgAnAAhIABAAQARAIAQAFQgQgFgSgIgAm/AhIAAAAgAkgATIAAAAgAA9AJIAAAAgAgJgJIgCgQIAAgRQgFgBgDgEQgEgEgEgPQgHgUgUgaQgigvgvgjIgRgMIgBgBQgGgBgIgEIgTgLQgNgJgNgFIgCgBQgEgSAAgVIABgTIgBATQAAAVAEASIAAAAIAAAAIgBgDQgCgNgBgMIAAgJIAAgHIAAgEIABgKIAAAAIACABQA4AYAxAnQApAiAfApQALgaATgVQAIgKAUgTIAcgYIADgDIABAAIAdgYIAFgEIABAAIALgIQAFgEAEgBIAGAkIgFAPIAGgFIAEgDIAFgEIAKgGIAIgGQgFAIgNAIIgVAPIAHgHIAFgFIgFAFIgHAHIgQAMQgGAGgSAXIgKAPIgDAEIAAAAIgBABIgGAIIgKAOIgGAZQgJAUgHAWQgMAngHAtIAAAAgAGMhLIAAAAgAGMhMIAAAAIAAABIAAgBgAGMhMgAAaiHIAAAAgABmjzIAAAAgAB3kDIABAAIADgDIgFAEIABgBgAB7kGIAAAAg");
    this.shape_3.setTransform(3.775, 4.4012);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#44A144").s().p("AjmGfQAUgIAhgQIBCghIABgBIgIAFIAHgEIgHAEIgXAPQhoA+hiACQBBgGAwgUgAo3GsIABAAIAAAAgAonGUIAEgFIABgBIAVgRQAHgGAXgeQAYgeAugiQB1hZBqgKQAZgCAgABIA6AEQAlADARAKQAKAGAOAPIABABIgBAAQgnAGgyAXIhUArQimBVi5AWQgLACgHADIAAAAgAokGPIABAAIgEAFIADgFgAojGPIAAAAgAFMF+QgPgBgSgIIgRgIQAqAMAfADIADABQgLACgKAAIgFgBgAj0FrIADgBIgFACgAh2FqIAAAAgAFxEWQgGgCgIgGIgMgJQgMgJgTgHQgLgDgXgEQhDgMgygDQg/gEg1AHIgEABIALgQIgLAQIgDABQAFgIAAgMQAEADAEAAIABAAQAMACAOgXQAPgXATgTQAVgUAZgQIAIgDIAKgEIAVgGQAugNAMgCQAtgIBJAMQAeAEASAHQAzAOBDA1QAbAXAOAPQAKAKATAaIAIAKIgRACIgsAEIhOAMQgzAJgbADIgOAAQgLAAgGgCgABwCUQAmgdAvgUQgvAUgmAdgAApDjIAAAAgAAfCoQgXgrghgWQglgXhEgIQgmgDgYABQg0AEg4AgQggAQg/AxIgCABIgVgLIhHgyIg9gxIgLgIIgOgKIgbgQIgOgHQAJgCALgGQBDgeAigNQA6gVAwgCQAcgCA2AFQAhAEANAFIAQAGQAIAEAHAAQAXADAVgWIAKgIIAMgEQAFgEACgHQACgGgDgHIgBgCIARAJQBpA7AtBLQAYAnAIArQgDATgBAUIgBAAIgEgIgAmiChIAVALIAAAAIgVgLgAmiChIAAAAgAAoCJIABgIIgBAIQgIgrgYgnQgthLhpg7IgRgJIgFgGIABgCIgBgCIABAAIAAABIAAABIAAgBIAAgBIABAAQANAFAOAJIATALQAIAEAGABIABABIARAMQAuAkAjAtQASAaAIAVQAEAOAEAEQADAEAFACIAAAQIACAQIABAFIgBAMIgBgJgABLALIABgIIAHgYIAKgOIAGgIQgFAHAAAFQgJARgJARQAJgRAJgRIAAABQgBAGAEAFQAFAGAFAAQAGABAIgDIALgHQCKhdC/gbQA3gJAkAEIAMACIAAAAIgIAKIAIgKIgIAKIgWAfIhBByIgIASQglgNgmgHQhPgMhHASQgUAFgTAIQhDAag8ArQgFglAKgwgAoyA2IgNgKIAOAKIALAIIgMgIgAg7hdQgxgng5gYIgBgBQAFg7AkhGIAwhWIAMgWIASgkIAAgCIAEgIIAEAIIADAFIAXAfQAKAOAYAzQAVAoASAVIAPAOQAGAJACAHQABAGgBAIIgDAMQgCASACARIAFAVIAAALQgBAIABADQACANAJAEIADABIgcAYQgVATgJAKQgSAVgLAaQgegpgpgigABegfIAAAAgABxg+QASgYAGgFIAPgNIAWgPQANgIAFgHIASgMIAUgKIBdgeIAPgCIgPACIAQgCIgBAAIgKADQgSAFgcALQg7AZgnAYQgoAagcAeQAQgWAUgSQgUASgQAWIgNAQgAifhYIAAAAgAijhiIAAAAgAIsiHIAAAAgACMiYQAHgHACgGQAFgHgCgIQAAgIgHgFQADgGABgIIAJAtQgFACgEADIgLAIIACgDgAimidQAFg7AjhGIAxhWIAMgWIgMAWIgwBWQgkBGgFA7gAJKi5IAKABIgFAGIgFgHgAJUi4IAAAAgAJKi5IgDAAIgBAAIAOABIgKgBgABilLIgXgcIgagZIgSgTQAjAaAOARQAWAXAQAhIgUgbg");
    this.shape_4.setTransform(-1.3, -9.5);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#71C571").s().p("AnIFCQArgpAzgfIABgCIAEgCIBNgmQAWgKAUgIQAWgIATgFQATgEAJABIAFACIASAIQALAFAUABQAXACAJACQAPAEAZANQAXANAIAKIAHAJIAGAIIgHAAIgCABIgBgBQgPgPgKgHQgRgJglgDIg5gEQgggCgaADQhpAKh1BZQguAhgYAfQgYAdgHAHIgVARIgBAAQAog7A0gwQAugsA1ghQg1AhguAsQg0AwgoA7IAAABQAog8A0gxgAjuC+QAngMAjgCQgjACgnAMgAJhEPQgUgagJgKQgOgPgbgWQhEg1gygPQgTgGgegFQhIgMguAJQgMABgtANIAEgCIAHgCIgHACIgEACIgVAGIgLAEIgBAAIAHgEIAlgKIA8gRIAPgFIAIgBQAwgEBeAHIAJAAIgHAQIgHgDIgCAAIAAAAIgDgBQgvgPgvAAIAAAAIgBAAQgbAAgbAFIgDAAIgDABIADgBIADAAQAbgFAbAAIABAAIAAAAQAvAAAvAPIADABIAAAAIACAAIAHADIAAAAIAQAGIBqBBIAEADQAoAiApAwIgXADIgCAAIgIgKgADFCCIAAAAgAGzB6IAAAAgAppA1IgEgCIgBAAIgHgDIgBAAIgHgDIgBAAIgDgBIACgBIACgBIB9hGIAOgIIABgBIABgBIAEgDIAGgDIABAAIAAgBIACgBIAAAAIAFgDIAAAAIABgBIACgBIAGgDIACgBIBEgfIAJgDIABAAIAJgDIAJgDIACAAIASgDQANgCANAAIBTALIA4AUIATAJIABABIACgDIAEAGIABACQADAGgCAGQgBAIgFADIgNAFIgKAIQgUAVgYgDQgGAAgIgEIgQgFQgNgGghgDQg3gGgbACQgwACg6AVQgiAMhDAgQgMAGgJACgAnjguIAFgDIgFADgAnbgzIAIgEIgIAEgAigg4IgGgDIAGADgAlfhiQgNACgQAEQAQgEANgCIAagCIgaACgAp+AtIgDgBIADABgABrASQgGgBgEgGQgEgFAAgFIAAgBQABgEAEgHIABgBIAAgBIADgDIAOgRQAbgeAogZQAngZA7gYQAdgMARgEIAKgEIACAAQAOgCAPgBQAxgFBAADIABAAIABAAIACABIABAAIAUABIBDAGIAJABIAAAAIgJgBIAJABIADAAIAGAGIgDAEIgPARIgKAMIAKgMIgJAMIgBAAIgMgBQgkgEg3AIQi+AbiKBdIgMAHQgFACgEAAIgEAAgABdAAQALgRAMgQQgMAQgLARgAFRidIAfgDIgfADgAnzgkIACgBIgBABIgOAIIANgIgAnxglIAAAAgAnogrIACgBIgGADIAEgCgAnmgsIAAAAgAmOhXIAEgBIACgBIACAAIgJADgABghYQgJgDgCgOQgBgDAAgHIAAgLIgEgWQgDgQADgTIACgMQABgHgBgGQgCgIgGgIIgOgPQgSgUgWgpQgYgygKgPIgWgeIgDgGIgFgIIABgBIABgDIAAAAIAGgPIAAAAIAAgCQALANAUAQIAaAWIADACIADADIADACIATAUIAZAYIAYAdIATAaQAMAYAJAdIABADIAEAQIAGAZQgCAIgDAHQAHAEABAIQABAJgEAHQgDAGgGAGIgDAEIgBAAIgEADIgeAZIgBAAIgDACIgCgBgAmGhZgAAbl2IAAAAgAAYl5IADACIAAABIgDgDg");
    this.shape_5.setTransform(-1.25, -12.65);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_5}, {t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-66.5, -56.7, 133.1, 112.4);


  (lib.Symbol15 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#740303").ss(0.5, 1, 1).p("AC/lWQgJAAgJAAQghAAgcAHAC/lWIAAAAABDk8QAHgEAHgEQACAAACgBIAAAAQAJgEAKgDQAEgCAEgBAhbnTQALAJALAKQA+A8AcBQQADAMAEAMIAAAAQATBFgLBKQgBAEAAADQgNBTgsBBIAAAAIgBAAQAMg0gSg5QgTg8gtgsQgpgog6gaQgBAAAAAAQgJgEgKgEQgtgRg1gHIgCAAQgDAAgDgBQgHAAgIgBIhPgDQgKAAgJABQgMAAgPABIg0ADQgFABgGAAQgDAAgCAAQgNABgNABIAEAJQADAHAEAHQACAFACAEIApBHQAeAtAmApQAIAJAJAJQACACACACAAgkeQAOgRAVgNAFtlAIABAAQgkAVgWA6QgDAIgEAKAFtlAQgHAAgJgBQgcgCgrgHQg8gLgbgBAEKiUQAMgSAMgcQAAgBAAgBQABgBAAgBQAFgOAFgLAjAoNQgBgBgBAAAjAoNIBlA6AjVnyQAAgRAAgRQAKADAJADAjSkXQgBgXAAgYQgChSAAhaAgUARQAAAAABAAQAigfAcgVQASgNASgLQAYgPAYgKQAugUAvgGAgbAYQAAABAAAAQABADABADAgbAYQAAgBACgBQACgCADgDAgcAYIABAAAgZAWQABgEABgEIAAAAQgCgEgIgKAgoAHQADAEADADQADAGADAEAgYAgQgBgBAAAAQAAADAAADQAAADAAADQAAADAAACQAAADAAADQgBABAAAAQgBACgBACQgBABgBABQgBABAAABAlRhcQAugOArgBQA+gCAwAaQAcAPAcAdQAMAMAeAiAmEhJQAQgGAMgFQAMgEALgEAmHhIQABAAACgBAjkCMQgSAAgRgCIhPgPIgQgFQgFgBgFgCQhWgdhXg7QgBgBgCgBQgMgIgMgJQgBgBgBgBQAEgBAFgDQAJgCAKgEIBMgiAhABgIgCACQgDACgDACQgeAUgpAJQgLADgMABQgDABgEAAQgBABgBAAQgBAAgBAAQgBAAgBAAIAAAAQgCAAgBABIAAAAQgEAAgFABIglABAhABgQADgCAEgDAgzBWQgDADgDACAgzBWIAUgXAgWCoQACAKABAJQABACAAABQACAPADAOIAJAdQACAHACAGIABADQAHARAIAPQAQAdAaAbQADAEAEADQAHAIAJAIQABABABABQAjAfA1AeQAdAQBCAfQA9AeAhASIAAAAQgBgDgDgLIgBgHQgBgEgBgFIgQhFQgGgYgGgeQgVhbgigzQgQgYgXgXQgRgTgWgRQgfgZg/gnQglgXgXgMQgMgGgMgFQgDgCgDgBQgQgGgQgEAgZA2QgBBKAEAoAnPgnIAhgQQAQgHAOgGQAFgCAEgCAnPgnQgBAAgCABAHAgCQADADADAEQABABA4BGIAlArAImB6QALAMAKAKQgFABgEAAIk6AsQgIABgJABQgCAAgEAAAEyhoQAFACAGACQAIACAIAEIBzBcADehuIgBAAADlhyQgEACgEACQgBAAAAAAAERhtIAhAFAEKhuQABAAACAAQAAAAABAAQABAAABAAQAAABABAAAEJhuIABAAAEKiUIgQATQgJAJgMAGAEJhuQgVgCgWAC");
    this.shape.setTransform(0, 0.025);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#D7A100").s().p("AhLAoIAAgBIABAAIABAAIABgBIABAAIABAAIAAAAIAFgBIgLAHIABgEgAhIAmIACAAIgCAAgABMgoIABgDIABgBIgBACIgCADIgBABIACgCgABLgnIAAAAgABNgqIABACIgDABIACgDgABNgqIAAAAg");
    this.shape_1.setTransform(-10.45, 9.9);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#338533").s().p("AEJHfQhBgfgdgQQg1gfgjgeIgBgCIABACQAkAfA0AeQAdAQBCAfQA+AeAgASIAAAAQgggSg/gegAEKHfQhCgfgdgQQg0gegkgfIgBgCIgBgBIgQgPIgHgHQgZgggNgWIgUgoIgEgKIgIgdIgGgdIAAgDIgDgTQgEgkAAg/IAAgPIAAgGIAAgFIAAgGIAAgGIABABIAEgPIAAAAIgCgDQAthBANhTQgNBTgtBBIAAAAIgBAAQAFgUAAgVQAAghgLgjQgSg8gugsQgpgog6gaQgNgIgGgCQgCgBgBAAQgBgBgBAAQAAAAAAAAQABAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAAAgBIAAAAQAGgJgHgmIgCisIACCsIAAgCQgIgrACgtQABgzADgaIAAgFQADgUAEgBQAIgEAMAOQBPBjBBCYQAZA6AKAmQAMAwAFA5QADgYAIgTIAPgiIACgHQAHgWABgcQABgSgCgiQgBgOgCgFQgHgKgCgFIgCgIIABAAIAAgBQAOgRAVgNIAAAAQAHAFAGAJQAKAPAKATQAKAUAEAEQAGAHANAHQApAaAtAhQARAMAHAKQAEAIgBAIQAAAEgBAFIgQAKIgIAEIgBAAIgKABQgVACgpAOQgjALgNAIQgJAFgMAKQgSALgRANQgdAVgiAfIgBAAIABAAQAigfAdgVQARgNASgLIgaAZQgcAcgTAKIAAAAQBGANBAAKQAYAEALgCIAQgCQAIAAAFADQANAIgFAXQgDAJADAFIAIAEIAXAIQANAEAIAAQALAAARgJQAUgKAUgOQALgIAHgCQALgDAIAGQAIAIgEATIgJApQBHAFBFgBIAngBIAHAAIAVAWIgJABIk6AsIgNgEIgDgBQgJgEgLgIIgTgRQgdgbhEgpIgqgZQgUgMgOgFIgKgDIgCAAQgQgHgQgDQAQADAQAHQgLAAgIgCIAAABQAFADAEAKQANAkAoBEQAKASAJAGIAKAEIAIAGQAGAFgBAKQAAAJgIAFQASAbAgAXQAUAPAoAWQAXANABANQABAEgCAIIAAAMQAAAGAJAKQA2BHBGA5IACABIACAJIACAHIADAOQgggSg+gegAAgkkQAPgRAUgNQgUANgPARgABSFxIAAAAgAA8FdIgCgDIAHAHIgFgEgAA6FaIAAAAgAgYB6QgCgYABggIAAgFIAAgCIAAgHIgBgDIABgBIAAAPQAAA/AEAkQgDgRAAgXgAjpCGIgCAAIgCAAIgagCIhOgPIgRgFIgKgDQhVgdhXg7QBXA7BVAdIAKADIgKgDQhVgdhWg6IgBgBIgBgBIgCgBIgYgRIgDgCIADACIgDgCIAKgDIAFgBIAXADQAbAEAmAOQBBAZAdADQADgEACgGIACgMQADgQAKgCQAHgBAHAJIAJAOQAMAOAgAFQCGAVBbgnQAHgCADgFQACgCAAgEIACgHQADgFAEgCIAEgBQgHgDgIgNIgKgOIgEgDQgNgKgGgHQgWgWgMgJQgPgMgagKQgrgQgsAAQguAAgmASIgGADIgXAIIgdALIgIAAIgHgCIgEgIIAAAAIgEgEIgRgSQglgpgegtIgqhHIgCgFIgCgEIgGgOIgFgJIAagCIAFAAIAFAAIAAAAIAGADQAmATA+AaIB3AxIBLAeIA8AYQBVAlAxBDQABgrghg3Qghg4g3gmQgggVgSgLQA6AaApAoQAuAsASA8QALAjAAAhQAAAVgFAUIgKgOIAKAOIAAAAIAAAAIgBACIgBAGIgCACIgGgKIgHgHIAHAHIAGAKIAAAAIAAABIABACIABAEIAAAAIAAAGIAAAGIAAAFIAAAGIgBABIABgBIgBABIgBACIgCACIgBACIgBACIgUAXIgGAFIgGAFIAAgDIgUAJIgNAGIgCAFQgFAEgKAEQgJADgMABIgZAIIgOADIgBABIgBAAIgBAAIgCAAIgBAAIgDABIgGAAIgDABIglABIgFAAgAkrBHIAAAAIACAAIgEAAIACAAgAo6AAIAKgDIgKADgAiyCDIACAAIgBAAgAiwCDgAiuCDIABgBIgBABIAAAAgAitCCIAAAAgAgzBQIgBABIAAABIgFADIAGgFgAgzBQgAgfA5IAAAAgAgeA3IAAABIgBABIABgCgAgeA3IAAAAgAokAQIgSgNIgBgBIAYARgAo3ACIAAAAgABPhBIAAAAgAmEhPIAAAAgAlnhaIAXgIIgbANIgDACQgLADgHABIgEAAIAdgLgAlQhiIAAAAgADdh0IAIgEIgBABIgGADgADlh4gAjAkPIAAAAgAoekWIgEgIIgBgFIAGAOIgBgBgAoektIAQgBIgaACIAKgBgAoOkuIAAAAg");
    this.shape_2.setTransform(-0.05, 0.6306);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#44A144").s().p("AFtIFQhFg5g3hHQgIgJgBgHIABgMQABgHAAgFQgBgNgXgMQgogXgVgPQgfgXgSgbQAHgFABgJQAAgJgGgGIgIgGIgKgEQgJgGgJgRQgohFgOgkQgEgKgFgDIAAgBQAHACANAAIABAAIAKADIgFAAIgGgDIAGADIgGAAQgGAAgCADQgDAGALAFQAmANAjAYQAhAUAwAsQA1AxAXAgQATAcAQAlQAKAXAPAvQAJAXAIAJQAIAIAHgDIABAAIAGAWIAPBEIgCgBgAAeE3QgIgPgGgQQAHAQAIAPQAQAeAaAaIACADQgbgcgSgfgAAfE3QgIgPgHgQIgBgEIgCgDIAVAoQANAXAaAfQgagagQgegAD2DIIADABIAOADIgLABIgGgFgAieCYIAOgEIAYgIQAMgBAKgDQAKgDAEgFIADgFIANgFIATgJIAAACIgCACIgHAFQgSALgWAIQgPAGgPADIgRADIgGACIgHABIAHgBIgDAAIgBAAIgEABIABAAgAiXCXIAAAAgAF7CHIAJgqQADgTgIgIQgHgGgMADQgHADgLAHQgTAOgUAKQgRAJgLAAQgIAAgOgEIgWgIIgIgEQgDgFADgJQAEgXgMgHQgFgEgJAAIgPADQgMABgXgEQhBgKhFgNIAAgBQATgKAbgbIAagYQAMgKAJgGQAOgIAjgLQApgOAVgCIAKAAIABgBIABAAQAWgCAVACQgDAEADAFQACADAGADQA4AfAZAUQAPANAbAbIAgAfIAbAcQAcAgATAhIABADIgZAAQg4AAg6gDgACOhEQgYAKgZAPQAZgPAYgKQAugVAvgFQgvAFguAVgAkcBdIAAAAIgCgBIADABIgBAAgAkQBDQgfgEgMgPIgJgOQgHgJgHACQgLABgDARIgCALQgBAHgDADQgdgDhCgYQglgPgcgEIAVgIIBFgYQAmgMA7gKQBggTA+AKIAfACQARABAMgHQgCgMgWgJQg0gShTAKQgoAFgoAJIAEgCIAagMIAGgDQAngTAuABQArAAArAPQAbAKAOAMQANAKAWAVQAFAHAOAKIADADIAKANQAIANAHAEIgDABQgegigMgMQgdgcgbgQIgCgBIgCgBIAAAAQgrgWg2AAIAAAAIAAAAIgJAAQgrACguAOQAugOArgCIAJAAIAAAAIAAAAQA2AAArAWIAAAAIACABIACABQAbAQAdAcQAMAMAeAiQgFADgCAFIgCAGQgBAEgCACQgDAFgHACQg7AahOAAQgpAAgvgIgAiNiCIg9gYIhKgeIh3gxQg+gagmgTIAEACIA/ADQAxACAeAGQASAEAgALQA4ARAZAKQAtAQAhARQAYANALACQAAgNgLgLQgIgHgQgHIg5gcQgUgLgTgHIgngPIgcgIIAGAAIACAAQA0AIAvARIAAgCQAGABANAJQASAKAfAVQA4AmAgA4QAiA3gBAsQgxhEhVglgAiyj6IABABIgBgBIgSgHIASAHgAAGiTQgJgmgZg5QhBiZhPhjQgLgOgJAEQgEACgCAUIAAgjIAAAjIgBAEIAAgoIAUAHIACABQAEAGAHAHQASANAOAIIATAJQAKAGAHAGIAOARQAKALAWAXIAOATQAYAiAJASQAFAMAFAEQAIAJAJgCIAGgBIAHASQAEANADAMIAAgBIAEAAIACAHQABAFAIAKQACAFAAAOQACAjgBARQAAAdgIAVIgCAHIABgHQAEgbAAgZQAAgvgNgrQANArAAAvQAAAZgEAbIgBAHIgOAiQgIAUgEAYQgEg6gOgwgAAqkMIAAgBgAizn/IgTgGIATAGgAmFg7IAHABIAIABIAEgBIgGACIACgBIgCABIgKADgAl4g4IAAAAgAEFh2QAAgIgEgHQgGgKgRgNQgtghgqgZQgNgIgFgHQgEgEgLgUQgJgSgLgPQgGgKgGgEIAAgBIAOgHIgOAIIAOgIIABgBIAAAIQABAPAPAKQANAJAPABQAQACAOgFQAKgFATgMQAUgKAbAAQARAAAiADQAUADAJAEIAPAGQAFABAGgCQgSAWgNAhIgGASIgKAaIgBABIgBADQgLAcgMARIgQATIgGAFQACgEAAgFgAAyh3IAAAAgAjEk5IAAACQAHAmgGAJIgBgvIABAvIAAAAIgBgxgAjDkIIAAAAgAkvkaIAAgBIAGABIgGAAgAkvkaIAAAAgAFskxIAAgBIAQABQgJAFgIAHQADgFgCgHgABgk0IAAAAgAjGniIAAAAg");
    this.shape_3.setTransform(-1.5, -1.5);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#71C571").s().p("AE/HQQgJgJgIgXQgQgvgKgXQgPgmgUgbQgWghg1gwQgwgsghgVQgjgXgmgOQgLgEADgGQABgDAHAAIAGAAIAFAAQAOAFAUAMIAqAZQBEApAdAbIATARQALAIAJAEIAFAGIgGABQgQgTgXgRQgfgZg+gnQglgXgYgMIgXgLIAXALQAYAMAlAXQA+AnAfAZQAXARAQATIgFAAIAFAAQAXAXAQAYQAiAzAVBbIAHAhIgBAAIgDABQgGAAgGgGgAH6CwQgUghgcggIgbgcIgfggQgcgbgPgNQgZgTg4ggQgFgCgDgEQgDgFADgDIABAAIADAAIACAAIABABIABAAIAhAGIALADIAQAGIBzBbIAGAIIA6BHIAkAsIACACIgHAAIgnABIgBgDgAooA5IANgGIBNgiIACgBIAigQIAdgMIgBgBIALgCIAGgCQAHgBALgDQAngJApgFQBSgKA1ASQAVAJACAMQgMAGgRAAIgegDQg+gJhhARQg7ALglAMIhFAZIgVAHIgXgDgAmNgMIgdAMIAdgMIAKgDIgKADgAiXiVQghgRgtgRQgagJg3gSQgggKgTgEQgegGgxgCIg/gDIgEgCIgGgDIAAAAIAGgBIA1gDIAbgBIASgBIBPADIAPACIAdAIIAnAOQASAIAUAKIA5AcQARAIAHAHQAMAKgBAOQgKgCgYgNgACCjhQgQgCgMgIQgQgKAAgPIAAgIIACgBIABAAIARgHIAJgDQAdgHAgAAIASAAIAAAAQAcACA8AKQAqAHAdACIAAABQABAGgCAGIgLAMQgGACgFgBIgPgGQgJgEgUgDQghgEgRABQgbAAgVAKQgSAMgLAEQgLAFgLAAIgHgBgABrkUIgSAHIASgHIAIgDIgIADgAlKjzIAHAAIAIABIAAABIgPgCgAmrj2IASAAIgSABIgbABIAbgCgAgFkUQgFgEgGgMQgKgTgXghIgPgTQgWgXgJgMIgPgRQgGgGgLgFIgTgKQgOgHgRgNQgIgHgEgGIBmA6IAVATQA3A1AbBEIgFACIgDABQgIAAgFgIg");
    this.shape_4.setTransform(-0.35, -5.5952);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-58.1, -54.3, 116.2, 108.69999999999999);


  (lib.Symbol13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_313();
    this.instance.setTransform(-53.15, -75.2, 0.4152, 0.4152);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-53.1, -75.2, 106.30000000000001, 150.3);


  (lib.Symbol8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_304();
    this.instance.setTransform(-117.5, -159.05, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(145,124,63,0.549)").s().p("AiMKSQgLgEgSgLIlKjMQg3gigVgTIgZgaIgYgaQgygyhFgcIgogPQgagJgOgGQgvgSg0giQghgWg6grQgQgNgHgIQgMgMgEgNQgGgXARggIAcg2QADgJADgUIADgTQgfgdgmgkIhlhfQgUgTABgPQAAgIAHgHQAGgGAIgDQAOgEASAIQAJAEAUAOQA4AmAbAWQANALAcAbQAZAZAQANIAUARQALAKAFAKQAFAKgBAMQgBAMgHAJIgEAFQgBA8ghAwQAlArBOAgQBaAhAqAUQA4AcBJA+IB5BlQAmAdBSAyICXBdQAbARARAAQAPgBAbgQQCUhdE3iOQE9iRCQhYQgIgKACgQQAAgIAFgUQAEgQgCgMQgCgLgGgHIATAOIBGiLQARghATgMQgFgFgJgEIgbgHQgVgGgYgRIgpgiQg0gog4gYQgIgDAAgFQAAgEAFgBQAEgCAFABQA+ALAzAmQATAPAJAEQATAIAOgKQAJgGADgPQACgIAAgSQABgkAUgGQhTg4i+h5IhNgwQgSgLgGgKQgEgHABgJQAAgJAGgEQAKgIATAHQAYAHAcAQQAPAIAiAWQA7AnB3BLQBDAqAdAQIBIAlICIBDQAZAMAGAOQAJAZghAdIgnAiQgWAUgNAQQgJANgNAZQgOAbgHALQgMATgeAgQggAhgMARQgOAVgIAKQgGAIgHAFQgKAjgNATQgSAcghAYQgXARgpAWQh0A9kUCAQj/B2iHBMQg2AfgWAYIgUAVQgMAMgMADIgKABQgJAAgLgEgAQ1pFIgHgFIgDgCQgBgBgBgDIAFAAIAEACIAHAFQAEACACADIgCADQgDAAgFgEg");
    this.shape.setTransform(12.6539, 98.4511);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-117.5, -159, 249.6, 323.7);


  (lib.Symbol6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_301();
    this.instance.setTransform(-81.2, -68.05, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(145,124,63,0.549)").s().p("AjqF1QgRgCgggHIl8hSQgrgKgDgYQgBgOANgLQALgJARgFQAngKBKAIQCGAPCBAlQAaAIAMACQAWAFASgCQAZgDApgXIG5jvQA+ghAegXQAwgkAWgqQgtgjg6gLQgSgDgEgIQgEgKALgIQAJgHAMABIAXADQANABAIgFQAPgrgNgsIgHgUQgDgMABgIQADgXAZgKQAYgJAYAIQARAFAYARQAeAUALAGQAUAKAgAHIA3AMQAmAJAJAVQAIAPgIASQgGAQgQAJQgZAPgqgFQgNAUgsBYQgiBEgjAfQgVATghARIg7AcQg1AZhnA6Ik0CuQgfASgRAHQgbAMgYADIgVACQgNAAgPgCg");
    this.shape.setTransform(16.2297, 35.5114);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-81.2, -68, 168.4, 141);


  (lib.Symbol5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1_copy
    this.instance = new lib.CachedBmp_300();
    this.instance.setTransform(-82.95, -115.3, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#6D1A1A").ss(0.5, 1, 2).p("AjAvcQgDgQACgPQAAgDABgDQAMhABfgjQBjgjCFARQCFARBYA6QBMA1AEA6QAAAIgBAJQAAABAAABIgBA1IgBBTQABABAAADQADAOgCAOQgBAIgCAGQgPA6hbAgQgDAAgDACQgJBBgFBKQhrAmiBgQQiEgRhXg9QgigYgdgfQgBABAAACQgKArgJArQgTBXgRBVQgJAsgIArQgHAigFAhQgZCBgRB6QgXCYgNCMIBWgbAikviQAFgbAZgUQAMgKASgHQAQgJAUgGQAIgDAIgCQASgFATgDIABBZQAigFAlABQAsgBAwAGQARACAQADQASADARAFQAkAHAhALQAPAGAOAHQAcALAaAPQAJAFAIAGQAFADAEADQADACACACQgHAKgLAHQgYARglAMQhaAdh5gPQh4gQhSgyQgUgMgPgOQASgKAYgIQBjgjCFARQCFARBYA6QAHAFAHAFAikviQgDgLACgMQAHg4BagdQBagdB5AQQB5APBRAzQBRAygIA4QAAACgBADAg0w2IAABSABqxCIAABUAA5xEIAABbAC3w4IgBBZABqxCQATABASACQAUADAUAEQATADARAEQA3AOAsAXQAOAHANAIQAPAJAMALQAOALALALIgCBGAA5xEQAYAAAZACAgPw+QAigGAmAAAG9shIAAALIgBAjAG+sdQAAADgBAEQgFAWgRATQgIAJgKAHQgLAIgNAIQgRAIgVAIQgDABgCABQgmAOgqAGQgSADgSABQgvAEgzgGQgKgBgJgBQgSgBgRgDQgrgJgjgMQgRgGgPgGQgngQgggWQgCgCgBAAQgVgPgPgPQgFgEgEgEQgZgbgHgcAGfs0IAIBHAEBr+QgTADgUACQgpACgtgEQgNgBgOgCQgQgCgQgDIgDBYAG+t0QgGAlgjAbQgaATgpAPQgCAAgCABQglAOgoAFIABBXAG/upQgEAogmAdQgaAUgpAOQhkAliFgRQiEgRhXg9QgVgOgPgPQgjgggHgjIABAyIAABOIAAALIAAAhQgEgRACgRQABgFABgFAFOsRIAJBUAF2sbIAHBOAFMqXQhjAiiAgQQiEgRhXg9QgigXgUgZQgSgWgFgXACEr7IgIBWADar5IAEBWAE/jaQACAlACAoQAIB1AQCDQAeD/A5E0QAAABABACQABAHACAIQADAVAEAUQAAABAAACQgSAKgMARIBnAaIALADIAtA8AGNvTQARAUADAVQgDASgOAPAE+oMQgJCJAKCpQjFBCh+gQQiFgRhXg9QgagSgXgXAEgDiQgcgbgjABQguAAgfArQgfArAAA/QgCA9AiAsQAeArAuAAQAUAAARgJQAXgLASgYQAdgrABg9QABg/gggsQgHgJgHgHQgigogugBQgxABgjAtQggAxgBBCQgBBDAkAuQAiAvAxgBQAdAAAYgRACqGwQAAABABABQAWAhAiABQAigBAYgiQAYgjgBgzQABgwgYgjQgYglgiABQgIgBgIACACqGwQATAOAXABQAhABAYgeQAWgggBguQgCgsgYggQgXgfgegEQgYAHgRAcQgXAjAAAwQAAAxAWAjQABABAAAAgAAQNXQALgKAPgBQAEAAAYAAQAKACASACQAUAIAEAAQAKABANgDQAJgCAPgGQAugIAhAVQgFAKgGAMQgfBGgQAkQgJANAGALQADAHAQAHQBeAdAoAKQA5AJAZgJQAWgFANgSQAMgPAAgRQAJArgEAVQgIAigVAVQgPAPgPAEQgfAGgggZQgTgOgLgDIgYAAQgVAAgegOQgkgMgLgXQgFgVgIgMQgEgGAAgDQgEgGAIgDAi4QOQACADADACQAKANAiAGQCSAUBqgpQAigNAQgRQAbgcgLgbQgHgfgygTQgFgCgFgDQg3gggtgOQgZgJgXgCQgKgCgJAAQg0gDhAAVQglAOhIAjQAAACABADQAGAuAYANQAHAEAJABQAIABAKgBQAagBAKADQAKAFAAAEQAAAFgGACQgFAEgDABQgLAHABANIAAABQAAAIAFAIQABAKgBAFQgDAGgCADQgBAIADAOQACAHABAGAGNNAQgVgHgKgEQgSgHgPAAQghgFgcATQgKAHgOAYAGNNAQAAAUAEAXQAAABAAACIAKBMQBYBQBigaQADgBAEgBIAIgCAI4KuIAogCQAVAGAJAIQACACAEAHQADAEADABQACAFAEAFQADAEAAAIIAAAeAGtJfQA0gPAZAMQAFADAcAYQAMAHAFAIQgLACgRAMQgQAJgKABQgHAAgMgFIgTAAQgTAAgGAKQgDAAgBgCAIsKGQABACABADIAAAOQAAACAHAMQACADABAEQADALgEAPQA5AHAVAvIAEADIgHBMIAAADICuBWQgOAmg+AEIiAAcAG9LZQBKgWAwAFAIGMRIgMBdAIzOBIgkBMIhjgUAKJMBICmBrQAJAjgIAXIAJAEAKCNNIhPAtAGfL3QgTAcABAtAG2KwQj5BNifgPQhxgLhYgdIADiWIAAgCIgGACIgwATIg6AXIguARIgCAAIgUAIIgHgaAhstHQgEgDgCgCQgVgOgPgPQgIgGgFgHQgXgagFgaAiRu8QAHgEAIgEQAOgHAQgGQAogOAugGAh/upQgLgJgHgKQgPgTgEgTAhstHIgDBVAijt2IAEBcAiosDQgQBDgPBCQgBACAAABAgEsRIgEBRAgqseIACBSAh3k7QgPAGgPgEQgXgFgKgZQgJgZAJgfQAIgdAXgRQAVgRAUADQAPAIAHAXQAJAbgIAhQgKAhgUATQgBABgBAAQAAAAAAAAQgVATgXgGQgXgGgKgbQgJgcAJgiQAIghAXgTQAWgUAXAGQAGABAEADAignbQAegVAfAIQAgAJAPAlQANAmgLAtQgLAtghAZQgfAZghgJQgTgGgNgQQgOgLgHgTQgMgjAKgpQALgqAdgYQAHgEAGgEQAXgNAXAGQAfAJAOAjQANAigMAqQgKAqgeAXQgcAYgfgJQgNgEgKgJAoWJ5QgPgLgUACQgVACgQALQgKAIgTASQgTARgLAHQgUAOgyAVQgvATgVASQgOALgFAPQgFARAHANQAGAMASAFQAFACBJAIQAuADATAZQgOAAgHAMQgJAMAEANQAJAXAqAFQBzAMAzADQBcACBKgMAFbBrQjXA6iMgPQiegRhng9QgfgSgbgXAkfFcQAOgEAFgQQAAgBABgBQAHAGAKAAQAHAAAFgBQABAAACAAIAKgPIALAaIASApIALAYIAFAMQgEAHgMAFIgCAAIgMAAQgGAAgEgFQAAgCgBgCQAKgDAEgNQACgFAAgDQABgFgBgCAlAFVQANAMARgEQABAAACgBAkcFjQAAgCACgCQgDgCgCgBAkBFtQgEAAgFAAQgKAAgGgGQgCgCAAgCQgIAQgOAGQgCABgCABQgFACgGgCQgHgDgEgGIgFgEIgEgFQADAAAEgCQAKgJAEgJQAEgKgEgNAjsFQQABAGgBAFQgBAIgGAEQgEAEgKACAj+FwQgBgDgCAAAjsFQQALANALgHAjuFLQABACABADAj+FwQAEACAMAFQAJAFAIgHQAGgEAAgGQADAAAAgEAkLFGQACgQgMgOIAmgMIAOAgAl2FHQAJANgCAOQAIAJAIAAQAEAAAGgCAlvFiQgBAHgDAHQgDAHgJAGAl2FHIALgEAk4GBQAAgEACgCAlFGZQACgBABgBQAHgHAAgJIACgEQAAgBABgBQAGAPAQADQAKAAAHgFQAFgCADgCQAEgDACgCQAFgMAAgCQAAgEAAgDAkoGqQgFAAgFAAQgJgCgDgCQgDgDgBgDQgCgDgBgEAlpGSQAEAHAGADQAIAFAGgFQAIAAAEgDAlqGNQABACAAADQABAMgKAGQgLAHgMgDQgCgCgDAAQgCAAgBAAIAIAcIABAFQAMAMAKAAIAIAAIACAAQANgHAAgRQgBgDAAgDQgPACgHgNQgDARgLAFQgCACgHAAAlqGNQgDgBgCAAQgLgFgFgKQgCAAgBABQgPADgNgPIABAFIAMAvAlRFuQAAANgGAKQgMAHgHABAlCGgQgIARgNADQgDABgCAAAloHcQgCgEAAgFAk/HLQgFARgTAFIgNAAQgCgCgCgDQAAAIgHADQgGAHgLgHIAEANQAEAJALAAQAIADAGgEQACgBADgDQAGgHABgKQAEAKAMAAQADABAJgBQADgBAEgBIAIgKQACgDACgGQACAGAGADQAGAFAHAAQAHAAAHgEQAFgDAEgFQAJgMAAgJQgBgBAAgCIAAgCQgCAAgBAAQgHADgJgDQgFAAgEgEQgBgCgBgCQALgEAJgJQAHgKgBgNQgBgCAAgDQAHgBAFgGQAHgGAAgIQAHAJAHAAQAEAAADAAQAEgDAFgFAmIHHIAIAgAlbG7QABAIAJADQAIAFAGgFQABAAACAAQAKgBALgKQAEgEAAgFQAAgEgBgEAk/HLQgBgCAAgDAkkHWQgHADgHgCQgLgEgCgIAj0GYQAIAMAKACQACAAADAAAkJGKQAAACACABQAFAGAKAAQABAAACAAAkHGNQgGATgLAGQgFADgLABAknGyQAHALADAAQAFAGAEgGQADgBADAAAkkHcQAAgDAAgDAkMHAQgGALgLAIQgEACgDABAj/HqQgCgCAAgCAjEG9QAAgDAAgCQABADAEADQADADAEAAQAAADACACQABACADABQAEACAEgCAjYHQQAGgCAFgDIAHgHQACgDAAgEQgEgIACgGAjcGqQAAAKgJAJQgGAEgFADAjVHTQgCAOgGAGQgDADgDADAi+HTQgGAEgHAAQgGAAgEgEQgCgBgBgCAjvHJQAGAHAKABQADAAAEgBAi4HBQABgBABAAIAKgKIANAcIAKAWIAOAgIAAABIAIAUIgUAIIgNAHAi7HfQABADAEAAQAFACAEgCQAFAAAEgDQACgCADgGIAAgFAi2HGQgBAGgGAGIgBABAjIH1QAHgEADgHQADgGAAgFQgBgHgCgFAi0GjIAIATAjNGOQAFAJAPAAAl2FHIgvAPIABACIAGAaAoKI9Ig8gJICijcAmdF3IhqCqIgDAcIgRC/IAyAJIAJiCQAIgiAhgBQAPACAJAJQAOAOAAAeIgEAvIgJBVIAAATIBEATICuhSIABgpAnYLdIBDgXAmfJrQgpgDgHAaIgJBbIgEAqIA+AUAnpMFIANACAiVHoQgDAGgEAFIgHAHQgDACgCABAioIDQAAgDAAgDQgHAEgCAAQgIADgLgLQgCgCgCgCAi6IiQAFgDAEgEQAHgKACgOQAGAJALADQACACAFAAQAEgCAFgGAiQIWQACAJgFAGAi6IiQACAJAJAFIABAAQADAAADAAQAFgCADgCIgIACAjmHzQAFAFAHACQAIACAFgEQACgBADgCAj3IOQAFgDAEgFQADgGAFgNQABgDACgDQgHAEgGAAQgKAAgFgHQgJAPgOAEQgCAAAAABQgOACgHgIIgEgDIgPAOQgHAFgGACQgDABgDAAQgFAAgEgFQgCgDgBgDQgBgBABgDAjEH5QgIATgDAFQgDACgDACAjSIfQAEAIAKAAQAGgCAEgDAjnI5QACgBADgBQAIgFAFgJQACgFABgFQAAgFgDgFQgKAGgGAAQgKAAgGgJQgEAPgOAIQgEACgGABQgJAAgDgDQgHgDAAgHQgBAAgBgBQgBgDgBgGQAMgGADgNQABgCAAgCAkZICQAGANAQAAQAHAAAFgBAj1ISQgBgCgBgCAkaJMQACgBACgBQAIgDAFgJQACgDAEgLQAAgCgBgBQAGAHAJADQAHADAHgBQACAGAEADQABABACAAAkaJMIACAOAkEItQgCgCgBgCAkxH1IgGgEAlLIQQABAJgHAGQgEAFgFABAlLIQQAGAJATAAQAFgCAFgCAlNIKQABAEABACAlfIAQgBAPgKAFQgDAQAMAAQADACAEgBAlXItQADAIgGAHQgHAGgHABIAFAWQACACAFADQAEACAFgBQAFAAAEgDQAEgDABgGQAIgHACgQQAAgEAAgEAkmIeQgGAMgKAGQgCACgDACQgIACgGgBQgKgEgEgEQgDgEAAgEAk7I8QAAACAGAHQAFAFAIACQAIAAAGAAAl8H0IAUBPAlKJcIAEAPAjlPNIAAAFIgCASQgDAKAAACIgBAaQAAALAAALQACATAHAPIAoAOIACAAQAAgDAAgEQgDAEABADQACAGAGAGQAIAHAKACQAIADAkAAQAKgBBmAFQBDADAogRQAUgHAcgUQAggWARgHAkTOSQgNAagBAEQgGASADANQAFAOACAHQgBALAAAEQgBALAFAHQABADAFAHQAFAFAAAEQATAFACAAIAPADAgpMuQgeAAgNAGQgJAKgJgCAgpMuQAAACAAAEQAAASAEAEQADAAACACAhbMbQAKgHAOgDQABADANANIAKAIQABACABADAilL3IAnAWIAIACIAGgCQAHgEAHAEQAIADAAAIIgBADQgBADgDAGQgCADgCAFQgHAKgPAUQgJAJgKAAQgLgFgIgHQgagagEgJQgBgEgBgEQAVAIAHAHIAHAHQAFACAJAAIATgCIAYgGAi/McQgCgHABgHQACgNAIgHQAKgHAEAJAlIJrIgSDWAHVPxQAAgDAAgDABJsDQhrgShKgyAKCNQQhECEAmAYAjAwBIAAAlAiGwRIAEBNAhowiIABBMADbwxIgCBaAE+wMIgDBUAEfwVIgBBQAF0vpIgDBL");
    this.shape.setTransform(0.0337, 0.0104);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-83.5, -115.8, 167.1, 231.7);


  (lib.Symbol4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.CachedBmp_298();
    this.instance.setTransform(-83.4, 34.6, 0.4388, 0.4388);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_1_copy
    this.instance_1 = new lib.CachedBmp_299();
    this.instance_1.setTransform(-130.8, -98.7, 0.4388, 0.4388);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#6D1A1A").ss(0.5, 1, 2).p("AC1nGQgDAOgZAEQgYAEgfgIQgegJgTgPQgTgQADgOQAEgOAZgDQAYgEAeAIQAfAIATAQQATAPgEAOgANNrXQAHAMAEAPQBwD2hIGIQgDARgDARQgDAPgEAPQgfBxgoBjAC5ElQgLo4D/nHQC0kjCACEQAvBJgFAzQAsgEAWAqQAyNvmGIIQhfBUhwA4QgKBUg8BMAL6r6QAJgCAIgBAC5ElQAKiYAykZQBOmfDbkVQBzh9BFBoQACACACADQAZA2AHAgALor0QAJgEAJgCAKCqvQA7gzArgSQBaN8oYJRQABAWABAWQAAAUgCATAIKozQAJgMAKgLQA3g9AugoQg/K1kvI1QARBRAFBNAGYmIQA0heA+hNQhoIUi0HSQAXBFAPBDAMlsAQgGi4j5gdQnggSjxHvQjYHKBXHiQAXEPjginQhJg+g/hBQiDiIhViRQgBABgBACQgwBMCYEjQABAAABgBAOAg+QEsB9BrD6QixhjihA7Qg7gwhegLQgMAbgMAbQhwD5irCbAC5ElQAFAMAFANQAZA7ASA6AC9M3QADAGADAGQAGAOAFAQQARAzAFA4AN6gcQDTBGClDnACXDYIAAhCAB/CnQAMAZAMAYQASAnAQAmQBHmcCYkRIjVLGAiEFOQALAhAMAgQACAFACAGIBFC9QAkAUAfAWQAhAYAdAaQAZAYAVAbQAeAmAUArAhpGaIg9gyABdKzQj8EolQi6QiJhahJh7ArPIzQAFAIAEAIIAFAJQARAfATAhQAWAmAZAoQCeDLC0AoQAhAHAjACQDSAIDUh9ArvH2QAPAdARAgQGCHnFsmZAtXCHQiiDykfgIQDzDPE2hKAqdKMQCECkA3AZQFCDdErlAAkcPNQE4AEChiaABrCxQEPIqiWDxAtVCEQCqgpBDjoQBvFMiEDe");
    this.shape.setTransform(0.0677, 0.0163);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#000000").s().p("AgIAhQgfgIgSgPQgUgPAEgOQAEgOAYgEQAYgEAeAJQAeAIATAPQAUAOgEAOQgEAOgYAFIgQABQgSAAgUgGg");
    this.shape_1.setTransform(10.8074, -47.4357);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-131.4, -99.2, 262.8, 211.10000000000002);


  (lib.Symbol3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1_copy
    this.instance = new lib.CachedBmp_297();
    this.instance.setTransform(-84.3, -89.85, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#6D1A1A").ss(0.5, 1, 2).p("Aj3rvQAAALgDAIQgDAIgEAAQgEAAgDgIQgDgIAAgLQAAgLADgIQADgIAEAAQAEAAADAIQADAIAAALgAjfq4IAng2QgQhwjCgeQhqALBUCEQA7BLA/BMQA/BLgLBPQgpBmhIAqIAdAAQgdA1ATAdQAFAHAJAGAjfq4IgnAXQAIAjAOAmQALAcAOAfQAXAtAfAxIAPBVQAZAlgDAiIAmATQAigLBlA1QBOAxAJAxQAFAugHAgQgCAMgEAKQgMAcgXAMQgBABgBABQgQAIgUACQgRACgTgBQgOgCgOgDAjfq4QgWBTCIA0QCHA0BDA0QAlAcATAOQAQALAFACQAuAcgDAkQARhHg3gjIlnkyAhxkEQAPgIAMgEAh8kjQgCAYgQAWQgWAfghAIAlQjaQgjCcCnBWQAeAJAYAFAiCAoQApAEAWgQAhqgHQAOAiAZABIALAKIAGAAIAKAAIAGAAIAAgcAggAXIAXgHQgMgCgNgEQgMgEgKgEQhwgxADiCAiRBJIgFgjQAKABAKABIAUBQAgyAmIAABVIghBRIB2gDIhChbIgJhIAh0BWIA+ASAhuB7IhEBYIhkgnICLg/IgGgkAjzIQQgBABgCAAQABABAAAAQACACADABQB7BOCHAxQABAAACgBQgRAZAGAdQABACAAAHQAAADgEACQgBABgFACQgQALAEAnQABAUADApQAFAFABAJQABAIgDAGQAoAPBAgUQABAAAygUQAegNAVgEQAKgDBGgIQAxgGAbgQQAWgMAHgRQABgEABgEQABgGgCgHQgBgJgGgGQgBgBgBgCQgRgegMgxQgPg5gFgTQgCgIAAgBQgPgwgYghQgdgogogMQgZgIggADQgVACgmAIQhOARhiAcQgyAOh6AjQgBABgBABQgbAdgNARQgWAegGAcQgGAcAGAqQAJBCAAAFQACASAAADQACAMAFAGQAIAJAWAHQARAGA3AbQAsAWAeAFQA6AMA1gcQADgCACgDAGZDiQAAAAAAABQgDAcgjAdQgmAggqAKQgxANgkgVQgLgHgVgTQgVgUgNgHQgYgNgkAAQgWABgqAHQi/AkirACQgqAAgdgNQgRgHgKgLQgLgMgBgRQgCgPAHgNQgfAfg0A1QgpAsgQAbQgRAegPBBIgvDZQgBADgBADQgEALgIAEQAfATBVAyQAzAeAfAIQAnALAzgHQAggFA6gRAnDDSQABgEACgDQAKgPARgKQAUgNAwgKQBzgbBdg3Am8EHQAAACAAACQAEDMgcDAAm3DFQgGAGgGAHApgGIQgGgBglgGQgagFgQAEQgSAFgdAaQg4A3gpA7QABAAABABQAdAWAhADQASACAUgFQAGgDANgHQAHgEAKgGQAYgPAggEQACgBACAAAnaDwQADAigPA/QgeCBgqB2AtDISQAMBAAABKQABAAABABQASAUAvAIQAfAEAegCQAHAAAEgBAiSJ4QgNB3AJB+ArMIeQAEBOgEBNAjwIVQgJBlgUBgAmWEnQAHC/gKDVABFh1IA8AhAA5hfIBNApACAggIAKAFACIiEIBlCiQhVA2hhhAAA4hAIBIAgAA5hfQAMgigNgpIBNA9AMMFoQAEACAEgDQADgEAAgFQAAgHgHgGQgXgdgQgIQgQgHgYACQgMABgeAHQgrAJguACQgQACgDAAQgKACgGAEQgHAEgLARQgQAYgfAwIAbgDIAFAAQAQAWASAFQAIACANgEQAHgDANgFQATgGAggBQAugBAHgBQAkgGAWgQQAUgPARgXgAIfG7QgHAfACAfQABADACAHAHkIYQAUgDATABQALgBALABQA6AFAcAcQAGAHACAFQAAACAAABIAAAAQAAABgBACQAHALACATQAEAbADAIQABAEADAHQACAGAAAFQgCAOgMADQgIADgCAGQAAABABACQACAGgDAFQgCADgFABIgJACQgRABgXAPQgMAJgZAQQggARgugEQgdgCgugNQgDgBgDgBAH1GcQgRA/AAA9AILIWQA6gZAwgYQAWgKALgDQATgGAPAEQAQAFASARIBiBSQAHAHACAEQgDAHgCAHQgIAfAEAjQACAJgEAFQgDAFgIAAQgfAHgQADQgbAGgSAHQgGACgEABAMkIkQgWhagChiAGTDNQADAJADAMQANAxAJBhQALBsAbA2AgaBrQCdgRCqAwQAzAMAVANQARALAKARQAKARgBASAAVKTQAjgOAxg3QAzg5AigLQAGgCAygGQAhgFAQgOAFLI7QBHgVAkgHQAYgFAWgCAGzKOQgCABgCACQgEAEgIAIQgHAHgHACQgPAHgdgMAGzKOQgEAcAAAQQAAAaAHATAC0IhQgBAUgKBMQgHA7ADAlANFJJQABABAAABQAAAGgGAGQgCACgCACQgJAHgOAFQgRAFgIADQgQAGgJACQgPADgKgEQAAAAgBAAQgGgDgHgIQgLgLgBgBQgNgOgSgIQgRgHgRABALZJ2QAAACAAADIABAJQAAAIgGAOQgHAOgBAHQgBADAAAIQgCAHAAAFQgBAAAAABQgDABgDAAQgJADgHgBQgFgBgSgHQgNgGgJADIgFABIgDgDAJ+JJQgCAHgMAFQgSAHgDAEQAEACAAAGQAAACgBACQALAcASAxQAAAAABABAJbJkQABAEACAEQgBAEgEADQgHAGgOgDQgrAAgzAHQgjAGgQALAA4iqQgGgSgKgSAg4ACQBdgmAUg7AAggfIBNAXAACgLIBZAVAihm/QCLDJCrip");
    this.shape.setTransform(0, -0.0041);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#000000").s().p("AgGATQgDgIAAgLQAAgKADgIQADgIADAAQAEAAADAIQADAIAAAKQAAALgDAIQgDAIgEAAQgDAAgDgIg");
    this.shape_1.setTransform(-25.8, -75.25);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    // Layer_2
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#1072A3").s().p("AiuFaQhFgGgwgcIgcgSQgRgLgNgDQgPgEgUACIgjAFQiFAXiFgcQhBgNgigcQgXgRgLgZQgMgbAGgaIAIgfQADgSgIgKQgHgKgWgDQgugIhhgDQhVgHgtgiQgjgZgDggQgEgjAigmQAvgzBRgfQA0gUBjgVQEvg9DDgZQETgjDiAMQCLAGBoAZQAGgFABgHQAtAXBBAEQAhADBUgEQBogGBJAIQAoAEAkAJQCFADBgAiQAZAJAQAPQATASgEAUQgDAUgbAPQgnAXgFAFQAggCASAhQARAhgLAgQgSAwhPAiQhXAlh1APQhJAIiMAEIAeApQgfAqg1APQg0APgwgTQhjBIiXAfQhfATi0AKQgoACggAAQgfAAgWgCgAi+FKQEFATD9hFQAygOAegPQApgVAVggQAmARAsgDQAsgEAjgXQAXgPgEgPQgCgIgTgTQgOgPAIgIQCigBBVgMQCIgUBdg5QAmgVAHgaQAJgegcgdQgVgWgngTQAKgHArgNQAhgJAFgUQAFgSgQgRQgOgOgWgIQgwgTg+gHQgdgEgqgCQAYAIAVAKQAdAOAHARQAQAlg9AqQAFAMAgATQAbAQgDARQgBAMgRANQhPA/hqAbIgnAJQAMgGAKgIIAOgLQAIgGAHgCQANgEAaAKQARgTAFgWQAQAEAQgFQAQgFAJgNQAJgNgFgRQgGgSgPgEQgDADABAGQACAGAEAEIAIAIQAEAEAAAFQACAIgJAGQgIAFgKABIgTAAQgLABgGAGQgEAEgEALQgFAMgFADQgGAEgOgCQgRgCgFACQgJACgJAJQgKAKgGAFQgOALgZADQgeABgOACQgSADgOAHQghABgjgEQgWgCgJABIgHAAQALgDAUAAIAfgDIAFgCQABAAAAgBQABAAAAgBQAAAAAAgBQAAAAgBgBQgcgCgOAAQgXAAgSAGIgXAJQgOAGgJABIgRACQgKABgGADQgNAIgCARQgCANAEASQgZACgXAGQgKADgEAEQgCAEABAEQAAAFAEABQAFgJAOgCQAGgCASAAQAOAAAGgHQAGgGgDgVQgCgUAKgFQAEgDALAAQAPAAAYgKIATgIIgDACQgKAHgIAQIgOAcQgKARgRAMQgRALgTAEQgRAEgjgEQgjgDgRADQgZAEgeAUIgzAjQg7AmhWALQg3AIhmgBQhEgBgqgNQg7gTgUgsIgJgSQgFgLgHgFQgNgKgdAGIhUATQgzALghAEQhjAMhggYQgwgMgFgdQgCgRATgZQAUgdACgOQgvgahagJQhrgLgkgLQgfgKgUgTQgXgXADgbQAFgnBCgbQCag+DMglQB7gWD4gaQDDgUBggEQCigFCAAWQARADANgCQjYgskKAQQi9AMmuBGQiLAWhFAWQhyAjhGA/QgeAcgDAYQgCAVANAUQAMAQAUANQAsAdBHAIQAPACBtAEQA8ADAIAhQACAKgFAPIgIAaQgGAZAMAaQAMAZAXAQQAiAZBCAKQCvAcCrglQAWA8BzAJgAgHD2QAlAGAUgDQAQgCAVgJIAjgRQAtgUAeAOIANgNQgugKgtARIgkAQQgXALgPADQgYAEg0gKQgzgKgaAHQABgJgHgGQgHgGgJgBQgIAAgJADIgRAGQgVAIgNgGQAEgMgDgNQgBgHgEgGIAUAJIAXANQAGADADAAQAAAAABgBQAAAAABAAQAAAAABgBQAAAAABgBQAAAAAAgBQAAAAABgBQAAAAAAgBQgBAAAAgBQgHgBgLgJQgLgIgHgCQgEgBgCgBIgCgDQgDgDgHgBQgIgCAAgEQgEgCgCADQgCADABAEQABADACABIADACQgBAXAFAMQAEAKAIAGQAJAHAKgCQAFgBARgJQAOgIAIAEIAIAGIAHAHQAJAHAPABIAagCIAFAAQARAAAkAHgACFC3QgQAHgiAFIhCALIgFADQAAAAgBABQAAAAAAABQAAAAAAABQAAAAABABQAUACAKgBIAfgFQAggFAQgFIAPgGQAJgDAHAAIAJgBQAGgDgBgEIgbgDIgGAEgAjZBkQANADAHAEQAJAGAMAVQASAfAWANQAXAMApgDQA6gEALABIAPACQAJABAHgBQAHgBAKgFIARgHQAQgGAngDQAlgCARgJIARgLIARgKQAOgFAlAAQAhAAAPgKQALgKAGgDQALgFAZAFQAZAEALgGQAHgDAMgQQALgOAJgDIAKgBIALABQANAAAEgKQgGACgXgDQgRgCgIAHQgDADgEAHIgGALQgLAQgigEIgYgCQgOAAgJAGIgJAGQgFAFgEACQgHADgQABIgtADQgTABgJAEQgFACgIAHQgIAIgFACQgMAIgXABQgZgBgNACQgOABgfAMQgcAKgRABQgKAAgXgDQgJgBgnAFQgiAFgTgJQgOgGgPgSQgVgYgOgfIhGgJQgPgCgKACQgOADgEAKQASgCASAAQAZAAAZAFgAptBCQgGAEgDAGQgKAcAHAcQASAAAPAKQAPAKAHAQQAMAFATgGQAVgJALgCQAOgEAgAFQAfAFAQgFQAKgDAMgHIAUgOQAagRATAFQAGACARAKQANAIAJgBQgNgTgZgGQgYgHgVAKQgHADgPALQgOAKgJADQgRAGgfgEQgjgFgOADQgJACgTAHQgRAFgLgDQgJgOgPgIQgOgJgRgBQgDgPAEgQQAEgQAKgMQAAAAgBgBQAAAAgBAAQAAgBgBAAQgBAAAAAAQgDAAgDADgAoHBoQAEABAEAFIAIAJQAQANAagPIAUgLQAMgGAJAAQAQAAgBgHQgXgGgVAMIgWANQgSAGgNgNIgHgIQgEgFgFgBQgEgCgOAEQgLACgEgEQgEgEABgJQABgLgBgDIgFgIIgEgHQgBgEABgHQAAgHgBgDQgCgJgNgEQgMgEgYgBQgEAAgDACQgBAAAAABQgBAAAAABQAAAAAAABQAAAAAAABQAdABAXAPQgDAIADAMIAGAVIAFAVQAGANAKABIANgDQAGgCADAAIAEABgAKDAZQAeAEAZgSQAPgLAIABQAFAAALAKQAKAIAGgCQAHgCAHgMQAHgGAVgCQA+gFAYgeQgHgDgPAHQgaANgbADQgZADgGACQgSAGgDAOQgXgDgNgRQgPAPgUAHIABgEIgbgCIAAACIgRAGIgFACQgCADACACIAIACQAEABADgCQgGADgBAFgAr+gEQAJACAMAFIATAKQAYALAdACQAFgGgFgHQggADgbgQIgOgHQgJgFgGgCQgIgCgQABIhAAEQABgLgDgKQgGgCgKAAIgQgBQgJgBgGgGQgGgGACgIIAFgKQAEgGgDgDQgIABgGAHQgFAIACAJQACAQATAIQANAFAVgBQgDAFACAGQACAGAFADQAJAEANgBIAggEQALgCAJAAIAMABgAush2IgJAEQgJAEgEALQgFAKABALQAAAKACAGQAGATAXAOQANAJAOAEQAEgEAAgFQAAgGgDgDQgEAGgLgEQgJgCgKgIQgGgGgEgHQgKgRAGgTQADgIAEgEQAGgHAUgDIAGgCQAEgCgCgEIgUgCQgBADgFACgAqfgfQAFACAHALQAGgDAAgGQgCgCgEgFQgDgFgDgBIgFgBIgjgBIgOAEQgJACgFAAQgDgFgFgBQgGAAgDADQAHAOASAAIAFgBIAFgDIAHgBIAdgBIADgBIAFABgAtUiJIgSAbQgIANACAIQACAHAJADQAHACAJAAIgBAQQgBALAGAEIAJACIAVABQAEABADgCQAEgDgDgDIghgEQgBgEABgMQABgKgEgEQgDgDgJgCQgJgBgDgCQgBgFAFgIIAZgiQAGAGAHgEQgEgIgJgFQgGABgIAMgANtj2QgEABAAAFIADAHQAKAAAOAFQgDANAIALQAJAMANABQgDACgHABQgIABgDACQgHAEABALQAAAGACAMIAFAAQADgJgBgLQANgBAJgGQAHgDAEgGQAFgHgEgHIgRgCQgLgDgDgGQgCgEgBgIQAAgJgCgDQgNgBgLgGIgFgCIgBAAgAGFkqIAYAGIAGACIACADQAEABAEgBIAHgCIgDgDIgjgIIgEAAQgBAAgBAAQgBAAAAABQgBAAAAAAQgBABAAAAg");
    this.shape_2.setTransform(-3.9186, 75.3453);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#F7F7F7").s().p("Ai+FMQhygJgWg8QirAliwgcQhCgKgigZQgXgQgLgZQgNgaAGgZIAJgaQAEgPgCgKQgHghg8gDQhugEgPgCQhGgIgtgdQgUgNgLgQQgOgUACgVQADgYAfgcQBGg/BxgjQBFgWCMgWQGuhGC9gMQEKgPDYArQgOACgQgDQiAgWijAFQhfAEjDAUQj4Aah7AWQjNAliZA+QhCAbgFAnQgDAbAXAXQAUATAeAKQAkALBrALQBbAJAuAaQgBAOgVAdQgTAZADARQAFAdAwAMQBgAYBjgMQAggEA0gLIBTgTQAdgGAOAKQAHAFAFALIAIASQAUAsA7ATQAqANBFABQBmABA2gIQBXgLA6gmIAzgjQAfgUAZgEQARgDAjADQAiAEARgEQAUgEARgLQARgMAKgRIANgcQAJgQAKgHIADgBIgUAHQgXAKgPAAQgLAAgFADQgJAFACAUQADAVgHAGQgGAHgNAAQgTAAgGACQgNACgGAJQgDgBgBgFQgBgEADgEQADgEAKgDQAYgGAYgCQgDgSABgNQADgRANgIQAGgDAJgBIARgCQAKgBAOgGIAXgJQARgGAYAAQAOAAAcACQAAABAAAAQAAABAAAAQAAABAAAAQgBABAAAAIgFACIgfADQgUAAgMADIAHAAQAJgBAXACQAiAEAiAAQAOgIASgDQAOgCAdgBQAZgDAOgLQAGgFAKgKQAKgJAIgCQAGgCAQACQAOACAHgFQAFgCAEgMQAFgLAEgEQAGgGAKgBIATAAQAKgBAIgFQAJgGgBgIQgBgFgEgEIgIgIQgEgEgBgGQgCgGAEgDQAPAEAFASQAFARgJANQgIANgRAFQgPAFgQgEQgFAVgRAUQgbgKgNAEQgHACgIAGIgNALQgKAIgNAGIAngJQBrgbBPg/QAQgNACgMQACgRgagQQgggTgGgMQA+gqgRglQgGgRgdgOQgWgKgXgIQApACAeAEQA9AHAwATQAXAIANAOQARARgFASQgFAUghAJQgsANgJAHQAmATAWAWQAbAdgIAeQgHAagmAVQhdA5iIAUQhWAMihABQgJAIAPAPQASATACAIQAEAPgWAPQgjAXgsAEQgsADgngRQgUAggqAVQgeAPgxAOQjHA2jLAAQg4AAg5gEgAgGD4QgpgIgRABIgbACQgPgBgIgHIgHgHIgIgGQgJgEgNAIQgRAJgFABQgKACgJgHQgJgGgEgKQgFgMACgXIgEgCQgCgBgBgDQgBgEACgDQACgDAEACQABAEAHACQAIABACADIADADQABABAFABQAGACAMAIQALAJAHABQAAABAAAAQAAABAAAAQAAABAAAAQAAABgBAAQAAABgBAAQAAABAAAAQgBAAgBABQAAAAgBAAQgCAAgGgDIgYgNIgTgJQADAGACAHQADANgEAMQAMAGAWgIIAQgGQAKgDAHAAQAKABAHAGQAHAGgBAJQAagHAyAKQA1AKAYgEQAPgDAWgLIAlgQQAtgRAuAKIgNANQgegOgtAUIgkARQgVAJgPACIgPABQgSAAgYgEgAAMDXQAAgBAAAAQAAAAAAgBQAAAAAAgBQAAgBABAAIAFgDIBCgLQAigFAPgHIAGgEIAbADQACAEgGADIgKABQgHAAgJADIgPAGQgPAFggAFIgfAFIgIAAIgXgBgAiIC0QgVgNgTgfQgMgVgJgGQgGgEgNgDQgrgJgrAGQAEgKANgDQALgCAOACIBGAJQAOAfAWAYQAPASANAGQATAJAjgFQAmgFAKABQAWADAKAAQARgBAdgKQAfgMAOgBQAMgCAaABQAWgBANgIQAEgCAJgIQAHgHAGgCQAIgEATgBIAtgDQARgBAHgDQADgCAGgFIAIgGQAJgGAOAAIAZACQAhAEAMgQIAGgLQADgHAEgDQAHgHASACQAWADAGgCQgDAKgOAAIgKgBIgKABQgKADgKAOQgMAQgHADQgLAGgZgEQgZgFgLAFQgHADgLAKQgPAKghAAQgkAAgOAFIgRAKIgRALQgSAJgkACQgoADgQAGIgRAHQgJAFgIABQgGABgJgBIgPgCQgLgBg6AEIgQABQgeAAgTgKgApBCqQgHgQgQgKQgPgKgRAAQgHgcAKgcQACgGAGgEQAHgFAEAEQgKAMgEAQQgEAQADAPQAQABAPAJQAOAIAJAOQAMADARgFQASgHAKgCQAOgDAjAFQAfAEAQgGQAJgDAOgKQAQgLAGgDQAWgKAYAHQAYAGAOATQgKABgNgIQgRgKgFgCQgUgFgaARIgUAOQgMAHgKADQgPAFgfgFQgggFgPAEQgKACgWAJQgKADgIAAQgHAAgFgCgAn2B5IgIgJQgFgFgEgBQgEgCgIADIgNADQgKgBgGgNIgFgVIgGgVQgDgMACgIQgXgPgcgBQgBAAAAgBQAAgBABAAQAAgBAAAAQABgBABAAQADgCADAAQAZABAMAEQAMAEADAJQABADgBAHQAAAHABAEIAEAHIAEAIQABADgBALQAAAJADAEQAFAEALgCQANgEAFACQAEABAEAFIAHAIQAOANASgGIAVgNQAWgMAXAGQABAHgQAAQgKAAgLAGIgUALQgOAIgLAAQgKAAgHgGgAKEAbQABgEAGgEIAKgEIgIACIgCACQgEACgEgBIgIgCQgBgCACgDIAEgCIARgGIAAgCIAbACIAAAEQATgIAQgOQANARAWADQADgOATgGQAGgCAZgDQAbgDAZgNQAQgHAGADQgYAeg+AFQgVACgGAGQgIAMgGACQgHACgJgIQgLgKgGAAQgIgCgPAMQgUAPgYAAIgKgBgArVAPIgUgKQgMgFgJgCQgMgCgTADIggAEQgNABgJgEQgFgDgCgGQgCgGACgFQgUABgNgFQgUgIgCgQQgBgJAFgIQAFgHAJgBQACADgDAGIgGAKQgCAIAHAGQAGAGAIABIAQABQALAAAFACQAEAKgCALIBBgEQAQgBAHACQAHACAIAFIAOAHQAcAQAggDQAEAHgEAGQgegCgXgLgAungbQgWgOgGgTQgDgGAAgKQAAgLAEgKQAFgLAJgEIAIgEQAFgCACgDIAUACQACAEgEACIgHACQgTADgHAHQgEAEgDAIQgFATAKARQADAHAHAGQAKAIAIACQALAEAEgGQAEADAAAGQAAAFgEAEQgPgEgNgJgAqfgdQgDgBgFABIgdABIgGABIgFADIgGABQgSAAgHgOQAEgDAFAAQAGABACAFQAGAAAJgCIAOgEIAiABIAGABQADABADAFQADAFADACQAAAGgHADQgGgLgGgCgAsvgpIgVgBIgIgCQgGgEABgLIAAgQQgJAAgGgCQgJgDgCgHQgCgIAIgNIASgbQAHgMAHgBQAIAFAFAIQgHAEgGgGIgZAiQgFAIABAFQADACAJABQAIACADADQAEAEgBAKQgBAMABAEIAiAEQADADgFADIgDABIgEAAgAOHiWQgDgMAAgGQgBgLAHgEQAEgCAHgBQAIgBADgCQgNgBgJgMQgIgLADgNQgOgFgLAAIgCgHQAAgFAEgBQACAAAEACQALAGANABQABADABAJQAAAIACAEQADAGALADIARACQAEAHgFAHQgDAGgIADQgJAGgMABQABALgDAJg");
    this.shape_3.setTransform(-3.9798, 75.1209);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#3464C4").s().p("AgCAAIAHgBIgJADg");
    this.shape_4.setTransform(61.625, 76.8375);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_4}, {t: this.shape_3}, {t: this.shape_2}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-120.2, -90.3, 232.60000000000002, 200.5);


  (lib.Symbol2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.CachedBmp_295();
    this.instance.setTransform(-31.55, 43.15, 0.4315, 0.4315);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_1_copy
    this.instance_1 = new lib.CachedBmp_296();
    this.instance_1.setTransform(-93.6, -112.55, 0.4315, 0.4315);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#6D1A1A").ss(0.5, 1, 2).p("AEmncQAFAAAEgCQALgFACgRQADgUgNgRQgKgMgNgEQgFgCgEAAQgSgDgaALQgZAJglATQhBAggXAqQgKAPAFALQACAHAIADQAHADAHgEQBkhGBSAFQAHAAAGgBQAOAJAHAKQAJAOgFANQgHAKgQAEQAAADACAFQABAGgBADQgBABgBAAIgIAEQgLAIACAIQADAKAGABQADADAKAAQABAAABAAQAFgBAGgBQACAAACgBIASAAIASAAQAAABAAABQADALAIAFQALALAJABQABABABAAQAIACAKgCQAKgDAGgGQAUABALgCQABAKABAKQADBchIAjAD+oPIAQAhIAiAAIgCgbIgagagAGZnRQgOgFgHAGQgFADgBAHQgJgDgEAAQgHgCgDAEQgDACAAAJIAAALIACALQABADAAAGQACAGACAEQABACABABQADABAGAAQAJgCACgEQAGgGgCgGQABABAAABQACADAGADQADAAAHgDQAKgDAAgFQABgDAAgEQABgCAAgDQAAgDgBAAQgCgGgBgFQgFgUgBgJQAAgKABgEQAFgJAHABQAHACADAKQACAHAAASQAAACgBAEQgBABAAABQgCACgDABQgHAFgFgCIARgGIAsgQIAWgHIABAAQAEAEADAIQAGAPABAZQABAYgEAKQgDAIgEADQgFgEgEgJQgIgRAAgVQAAgUAFgPQAEgIAEgDQABgBABAAIAAgBQACgBACAAQAJAAAGAPQAGAPABAZQABAYgEAKQgFALgGADQgBAAgCAAQgBAAgBAAIgcADIgYADQANASgSAFAHznbIADgBAHhl0QgFgEgEgIQgIgRAAgVQAAgUAGgPQADgIAEgDAGgm+IAAAAAHJlxIgiAEQgDAAgKgEQgMgCgIAEIgWgRQgEgDgBAAQgDAAgEAHQgDAHABAGAH9l3QgCgBgDgCAF+nGQgBADAAAEQAAATAIALAFinCQgIAAgCACQgCABgCAEQgEAKgBAEIAAAJQAAAHABAEQAFAKAKAAQAHAAAFgEAE6luIALAWQABACACAEAFQlvQgSgSAPgYQgSAEgLAFQgDAAgDABQgIAWANANAGDlQQAFAOADAMQAKAzggAeQAKAJACAPQAAADAAADQAAAPgMAMQAJAIADAMQADALgEAMQgFALgLAHQgLAHgLgBQAAAEgBAEQgDANgJAHQgMAJgIABQgHAAgCgBQgEgBgCgEAEFmfQgCAQAHAOQAGAOAMAJQAGAFAOAIQAOAHAGAFQATARAAAZQgBADAAADQAPAGAIAQQAHARgHAPQAFACADAEAFWkhQgEAVgRAOQgOAKgTgBQgFAAgEgBQgNgCgLgJQgNgMgDgTQgDgTAJgPAGLk2QAfAWAcgmAEomgQgHABgJAAQgKAAgJAAQgwAAgaAAIgKAaIA+AAIAAgQIg0gKQgOABgMAAQgSBAAHBDQAJgUAbgJQAYgFAYAFQAAACgBABADulnQgOgGgOAGQgKADgPAJADrlsQgKgGgKACQgFAAgKADQgLAEgEABAA6kSQAFAGAKAFIALAEIAGAAQADABACgBQAJgCAEgLQAFgMgCgJIAAgFQgCgPgIgGQgGgEgEgCQgCgBgBAAQgDgCgBAAIgGAAQgFAAgMAGQgDACgFAFIgEAHQgEASAKANQABACACABQgSAHgKAMQgGAFAAAHIAAAJQABAGgFALQgDALAJAJQAIAJAKgBQABAAABAAADCjfQAAgGACgGQAEgJAFgIQADgDADgEQALgKAMgFQACgCAAACADCjfQAEAAANgCQAKgBAFABQAIABAFAEQAdAPAIAhQAAACAAABQAAAAgBAAQgCABgDAAQgFABgIgBQgJgCgEAAQgNgEgFgCQgKgDgGgFQgLgJgEgPQgCgHABgIgADNj8QAMALAdAHQABAAAIACIAIACIAFAAQADgCAIAAAEXj2QgBAHAIALQAOASAEANQgEAMgJAHQgGAGgJADADrkkQgjAEgRAeQgTAcAKAkQADAQAOAOQAJAJAVAHQAhAKAQgBQAcAAASgTQAJgJADgNQADgNgGgLQgCgFgLgOQgKgLgCgJABYjSQABgBABgFQACgEACAAIAFABQAEAAACAEIABACQAAABACACQACgCAAgBQAGgEAFAEQAGAEACAHIgBAJQgCABAAADABmh1QAIAJALABQAKADAFgEQADAGADAEIgOA3IAoAUIAhgQQgOA7gBAsQgCAoAIAZQgSAGgQgGQhJACgugrQAAgCAAgBQAAgBAAAAQgBACABACADJhhQABgCACgDADLhtQgEAFACAHQABADACAEQABADAEADQAEADAFADQALAKAKgEQAIgBADgKQACAHAEAHQAJALAKAAQAMAAALgPQgBgCgBgCIgCgMADJgnIAAAAACXhWIARA/IApg9ACXhWQABABABABQAFAEAJABQAIAAAGgDQAMgEAIgLACOhiQAEAIAFAEAEAhFIg3AeADahOQgKAUgHATAFXh4QADAHAAAIAEZBWIBNhBQASgDAFAKQACAEAAAHIg8AyQA4AIBbgeIAJANIATAcQAUAgAMAiQALAdAEAgQACAMABAMQAEAxgHAnQgTBlhkAjQgFACgFABQjpBQimhZQgWgNgWgPQgcgSgOgSIgLAJIieCDQADA9huBzQgKADgKACAH9ATIAkAAQBzCQhVDgQghBQhWAhQhoA1jOgLQiSgchQhcAICl3IgFAAAl5reIAZAAQD0FiA1F8IgOgOIgGgFQgFgCgFACIAAAAQgEACgCAEQgFAHAFALQAEAKAJAGAl5reQAGBchvAXQh7AOgFBTQBQhfBgA1QB3BKA9hEABlmZQgCABgCAAQABgBABgBABlmZQgUAagIAZQADgDAEgCQAGgCAEACQgFADgDAFQgDAFABAGQAAAFAEAFQAEAFAGACABekqQgGgKgCgCQAAgBgFgCQgCgBgFAAQgFAAgDABQAAACgBACQgBABgBAAABRk1QgGAJAAABQgBACAAAJAhlA1QgRgYgKgeQACgaAeABIALAHAAsi9QgBAAAAABQgCACAAACQgCAMAHAMQAGANAMAFQAGADAMAEQALAEAFAFQABAEACAEQABABAAAAIhNAAQgzAJgTBwQgBAAAAAAQgJgDgJAJQgIAIgGANQgFAMADAGQACADAIAGQAAABAAABQgCAJAGADQAEAHALAAQgIAFgHAGQgZAUgQATABjiMQgCAHABAHAgjATQgCgGgEgEQgCgDgCgCAgLAiQgRgKgDAAQgMgDgHAEQgLAGgFAPQgEAHAAAFAASATIAdAMQgIAZAAAcQgCgEgHgIQgOgNgEgDIgBgBQgHgIgDgFIgCgDQgCgCgCAAQgCgCgEgBQgCgBgDgBQgGAAgIAFQgRAKgCAbQAAAHABADQABABAGAEQAAAAABAAQAIADAJgFQAIgFAFgRQABgFABgBQADgDAEAAQABAAAAADAASATIAOhWQAygMAUAXQgpAogOAvAl5reQANCQBuAgAl5reQBGBTA1BdQCLDvAeEsAgFBKQAFAMAVAJQABABACAAQADgBAEgCQAHgEABgBAgwBlIgdAvIgIAMQAKAGAJAJQAeAeAAApQAAAVgIATQgHARgPAOQgRASgWAHQgIACgIACQgIABgIAAQgpAAgegeQgOgOgHgQQgIgTAAgWQAAgpAdgeQAegdApAAQABgRAFgQQAIggAWgcAgoBZIgIAMQBXBECYgKIhQBCIhkBTQgbgKgcgMAhZCnQAWAZAAAjQAAAXgKATQgHAMgKALQgbAbgmAAQgmAAgbgbQgDgDgCgCAhVCgIgEAHIgSAcQACABACACQAQAPAAAVQAAABAAACQAAANgIAKQgDAFgFAEQgLALgPADQgGABgGAAQgEAAgEgBQgLgaAGgqQAFgEAGgBQAHgCAHAAQACAAACAAAiHBvQACABACABQALAJANAHQgHAIgFAHQgBABgBACQgNATgDASQAAABAAABQADAUAJACQABAAABAAQAPACALALQAHAHADAJAiJCSQAIAAAIABAioEVQgGgDgGgFQgQgPAAgUQAAgVAQgPQAQgOAXAAQACAAACAAAhrDDIgFAHQgGAGgFAAAioEVQgHgKAAgOQAAgTANgNQAEgEAEgCAhnFbQgBgDgBgCAg/FyIgVARIgKAIQgWgZgFgYAgiGBIgjAcAALE0IhKA+QgTgLgVgMAAsE/IhOBCAhUGDQgNgYgGgQAiVEbQgKgBgJgFAhCBaQgUgQgPgVAiBEbQgSgjAEgmAj6ImQgBA7hJBRIgpAsAkJIYQhSgMiACOQAaBRBUgNAkJIYIAPAOIJ3oKAheGLIirCNAkLInIiHB0AH9ATIgvAlIAIALAFnByIANAeQA/gDA/ghAFnByQgPACgRABIALAlIAFAQIAGAWIALApAGDC1QADAAAEAAQAkAAAaAXQADACACADQAaAaACAkAF7CiQADgBADAAQArAAAfAfQAEAEADAEQAXAcAAAlQAAADAAADQAAADAAACQAAAGAAAGQgEAggYAYQgcAcgnAAQgQAAgOgFQgVgHgRgQQgCgDgCgCQgYgbAAgkQAAgVAIgRQAHgQANgNQALgLAMgGAF7CiIAIATIAPAnIAAAAIAHARQgHAhgkgQIgGgVAEcDwQAHgbAVgVQAOgOARgIAFbEjQgEgJAAgLQAAgVAPgQQABgBABAAAGSDcQABAAAAAAQAOACAKAIQAGALAAANQAAAVgPAPQgPAPgVAAQgUAAgPgOAGSDcQABAAAAAAAGrDmQADACACACQAPAQAAAVQAAAWgPAPQgDACgCADQgOAKgSAAQgBAAgCAAQgTgBgPgOQgHgIgEgJAHfBQQg2AZhCAJAF0CQIAHASAEZBWIgxAnIgpAiAE5FUQAAAAgBgBQgfgfAAgrQAAgNADgMACUDpQBEALBEgEADtCgIhZBJIhoBWAC6BAQA6AZAlgDADoB9QgQALgYgHAEfB2IgyAqAFCFTQgFABgEAAQiHAViGgqAFsFvQgdgFgWgWAH9ATQBnCIgbCeQgWB2hUA3QgLAIgMAGAFDBZIgkAdAFSCaQg3ANgugHAFHB1QgTABgVAAAHSDIQAfgLAhgPAIkEDQgeAQgdANAARHdQCBA9EOg0AhNCUQBcA3BgAWAIhFbQkvCvkUiJAASATIgGAlACogXQgZBRAPBHAChmeQgiACgaAD");
    this.shape.setTransform(-0.0044, -0.0206, 1.5245, 1.5245);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FF0000").s().p("AgCAAIAFAAIgDAAIgCAAg");
    this.shape_1.setTransform(78.0035, -57.375, 1.5245, 1.5245);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-94.2, -113.1, 195.60000000000002, 226.2);


  (lib._3_Stars = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_294();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Star_R = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_293();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_stae_L = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_292();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib.Blend = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Blend_0();

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Blend, new cjs.Rectangle(0, 0, 83, 77), null);


  (lib.Path_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(0, 0, 0, 0);


  (lib.Path = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.rf(["#2F0202", "#3B0101", "#3B0303", "#4A0101", "#600101", "#790306", "#A50003", "#820506", "#C00004"], [0.604, 0.635, 0.663, 0.698, 0.729, 0.776, 0.808, 0.816, 0.839], 2.7, -3.2, 0, 2.7, -3.2, 26.8).s().p("AiKCLQg6g5AAhSQAAhQA6g6QA6g6BQAAQBSAAA6A6QA5A5AABRQAABSg5A5Qg6A6hSAAQhQAAg6g6g");
    this.shape.setTransform(19.7, 19.725);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path, new cjs.Rectangle(0, 0, 39.4, 39.5), null);


  (lib.Path_1_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#745F5F").s().p("ACiAHQhFgWhdgCQhagChGATQhEATgPAfQAMgkAigaQAzgpBbgGQCJgJBTAeQBbAhgPBDQgLghhEgWg");
    this.shape.setTransform(24.3501, 6.1562);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_1_1, new cjs.Rectangle(0, 0, 48.7, 12.4), null);


  (lib.Path_0 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(0, 0, 0, 0);


  (lib.Tween5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AgCIoQgzglAKhLQgUAWgbgNQgagMAAgcQgOAwgnANQgiAMgrgRQgpgQgcghQgdgjACgkQgNABgMgFQgMgFgGgLQgOALgSgDQgRgEgNgOQAWAqgWAdQgUAbgqAFQgqAGgfgTQgkgWgBgqQgUAfgXgTQgXgUAXgZQghARgogJQAHAjgLAeQgMAhgfAUQgPAKgjAFQgkAFgggEQhRgKAZg1QgOAZgvAQQg0ASgJgiQgGAfgZgBQgZgBgLgcQgyAtg2AOQhDARgugpQgZgXgBgdQgBgbARgVQARgWAagEQAcgEAaAUQAPgrA5AIQA6AIgEAvIAJgJQAHgGACgDQADADANAGQALAFAEAEQgDgpApgQQAlgOAlAQQgngTgDgeQgCgaAWgXQAWgXAYAAQAaAAAIAfQAJgiAegCQAdgBAJAfQgKglASgcQAUgfA0gNQghgDALgeQAMgeAcAIQghgQAFgYQAFgVAcgOQAdgPAYAHQAcAIACAkQAPgkAygCQAGgqAcgSQAggVAsAYQgPgYASgQQARgQAVATQgIgeAagRQAZgQAdAKQgrg6A8hIQAxg6BHgaQBMgbBVAeQBfAhgBBUQAYgsAoAaQAlAZAFAqQAagLAXAQQATANAPAdQAXgGAyAAQAAgaAYgNIgSAJQgogQAUgbQAUgaAhAQQgjg9AvgfQArgdA/AQQgUg8AtgzQApguBIgTQBIgUA3AUQA9AWAGBAQAVgUAZAHQAaAGAGAcQAhgEAYAaQAWAYgBAiQgCAigXAVQgOAMgmARQBKgIgHBFQgDAbgQAbQgPAagUALQAcgFALASQALASgbAJQAUgEAYADQAXADAMAJQgLg+AcgZQAYgWApAIQAmAIAYAYQAZAbgRAWQAggFAZAbQAZAagoAEQAuAOgCArQgCAoglAmQgnAnguADQg2ADgog1QgPATgXgGQgYgGADgVQgSARgMgKQgKgJgBgXQgYAggSAJQg1AbhFgCQhNgCglgpQgIAagfgIQgagIgSgUQgeATgggKQgdgJgWgdQgWgdgGggQgHgkAQgaQglALgZgPQASAsgnANQgjAMgvgUQgTA5hBAhQAbgKAYADQAfAFABAbQAngOAdANQAiAOgJArQAvgYApAgQAqAfgKA0QgJAsg0ApQgoAfg2AVQAagHANAOQANAOgVATQA3APgfApQgdAmgrACQANAkgxAdQgvAbhCACIgMAAQg/AAgmgcgAtNDiIgHgRIACAPIAFACIAAAAgAUSDkQgegHgCgaQgDgbAjgEQASgCAMAMQAKAKAAAPQAAAPgKAIQgHAHgMAAIgLgBgAUdB2QgegDgQgQQgSgRAPgVIAGgFQgUgGgFgSQgDgPAJgQQAIgOANgCQAPgBAKASQAQgXAZABQAWABASARQARARgCARQgDAUgfAFQAQAigRAQQgMALgXAAIgKAAgAqNjuQgQgDAFgWQgXAJgTgMQgRgLgHgVQgGgWAJgQQALgTAbgCQgOgMACgNQACgMARgIQADAPAHAFQADADARAHQgNAvAjAfIgDAGQAeAEAAAOQAAAMgSALQgPAJgLAAIgGAAgAokkRQgxgNgWgeQgYgiAhgdQghgBALggQAKggAgAEQgggXAVghQgbgBAFgkQAEglAfgHQAWgGAQALQAOAKADATQApgNAcAOQAgAPAAAwQAUgUAUAHQATAFAJAUQAKAUgHARQgIATgbACQAOAQgLASQgMASgZABQAHA6goAVQgTAKgYAAQgTAAgXgGgAk/nrQgTgIgDgXQgEgiAigJQAhgKASAZQAQAVgIARQgIAQgVAGQgJADgJAAQgKAAgKgEg");
    this.shape.setTransform(0.0004, -0.0235, 1.2751, 1.0272, 0, 0, 180);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-177.1, -59.6, 354.29999999999995, 119.2);


  (lib.Symbol28copy9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.instance = new lib.CachedBmp_363();
    this.instance.setTransform(-64.35, -42.9, 0.0893, 0.0893);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#3860B1").s().p("Ah7CdQgNgCgKgHQgLgIgEgMQgmADgSgDQgggEgSgUQgOgPgCgWQgDgWAKgTIAGgMQAEgGABgGQADgIgBgSQACgYAYgXQAKgJATgLIARgKIA6ggQAWgMAMgEQAUgIAQAAQAUAAAQALQAQAMACASQBXgdAkgIQAjgHAQAJQAZANAGAvQAVgGAWAJQAVAJALATQAHALABANQABAJgBAJQgDAWgQAQQAPAGAHAQQAHAQgIAMQgIANgYAHQhDAVhdAQQg2AKhsARQgWAEgPAAIgNgBg");
    this.shape.setTransform(0.6373, -4.725);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#5387EE").s().p("AjAG3QhwgOhPgwQgbgQglgeQhphThMhXQgkgqgMgdQgYg2AMhSQARhpA5hJQAaggAuglQBRhBBYgnQBfgqBggGQAxgEA7AGQAqAEBCALQCAAVBXAZQBzAgBWAyQAAgKAJgGQAIgGAKAAQAOAAAWANQAgAUA9AxQAYAUAKAKQASARALARQANAUAEAVQAFAYgIAUIgFANQgCAHAAAGQAAAGAJAUQAJAUgCAbQgBASgHAfQgJAogKARQgIAQgSAUIgfAgIgWAdIgVAdQghAng1AiQgiAVhDAhQhQAogoAMQg2ARhFAGQgqAEhUABIgrAAQhkAAg6gIgAoihCQgdAdgCAiQAPAIAKAYQAIAdAGANQAMAdAmAdQAsAeATASQAKAJAUAWQASAVALAKQArAmBQAUQBKATBYACQBJACBagJQBBgGAwgLQBHgPBVgmQBNgjAmgiQA5g1AFhAQAEgogUgtQgOghggguQgjg0gbgVQgYgSgqgSQhNghhlgXQhFgQhygSQgzgIgigDQhkgGhmAlQhhAjhTBDQg/AygSAxQAqgEApgSQgrAbgPAQgAJThzQAAABABAAQAAABAAAAQABABAAAAQAAAAABAAQACgCgBgEIgDgEIgEgEIgCgDIgDgBgAkjBSQhpgtAAg/QAAhABpgtQAlgPApgLQgTAMgKAJQgYAWgCAZQABASgDAJQgBAFgEAHIgGALQgKATADAWQACAWAOAPQASATAgAFQASADAmgDQAEALALAIQAKAHANADQARADAhgGQBsgRA2gKQBdgRBDgVQAYgHAIgMQAIgMgHgQQgHgPgPgHQAQgPADgXQABgJgBgJQAzAjAAAsQAAA/hqAtQhqAtiVAAQiVAAhqgtg");
    this.shape_1.setTransform(1.1041, 2.2029);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    // Layer_3
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#FFFFFF").s().p("AjfG4QgngKg7gWQhEgagjgQQg4gbgngfQgwgng3hNQgkgxgVgoQgbgzgLgvQgKgtADgnQAEg1AehFQAYg4AbgfQAWgZAlgZQB6hSCtgYQCCgRC4AQQBJAGApAKQAcAGAwAQQA1AQAXAGQAqALBXANQBMAOAuAZQA9AgAyBEQBGBggCBiQAAAhgNA2QgNA0gSAdQgLATgSATIgiAjQhEBAgoAdQhQA6hxAhQhXAZh9AQIhLAJQhKAHg1AAQhFAAg5gOgAiqlKQhlAChRATQhOAThIAnQgxAagZAdQgSAVgQAjQgWAzACAoQACAnAeA3QBLCQBoA7QAbAQA8AYIBPAfQAtAQAlAHQBCAOBTgCQA3gCBegMQBfgLAzgOQgZgOhFgOQg+gMgcgXIA9gJQArgHAbgHIApgNIAogNQATgFAjgHIA2gMQA7gQAygiQAmgaAZggQAcglAGgnQAJgxgYgyQgWgvgrgiQghgZgngOQgdgKgtgHIhLgNQgigHhOgYQhIgXgpgHQgZgEgygEQhggHhBAAIgXAAg");
    this.shape_2.setTransform(1.2562, 5.5929);

    this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-71.3, -42.9, 145.1, 93.9);


  (lib.Symbol28copy8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.instance = new lib.CachedBmp_362();
    this.instance.setTransform(-64.35, -42.9, 0.0893, 0.0893);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#3860B1").s().p("Ah7CdQgNgCgKgHQgLgIgEgMQgmADgSgDQgggEgSgUQgOgPgCgWQgDgWAKgTIAGgMQAEgGABgGQADgIgBgSQACgYAYgXQAKgJATgLIARgKIA6ggQAWgMAMgEQAUgIAQAAQAUAAAQALQAQAMACASQBXgdAkgIQAjgHAQAJQAZANAGAvQAVgGAWAJQAVAJALATQAHALABANQABAJgBAJQgDAWgQAQQAPAGAHAQQAHAQgIAMQgIANgYAHQhDAVhdAQQg2AKhsARQgWAEgPAAIgNgBg");
    this.shape.setTransform(0.6373, -4.725);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#5387EE").s().p("AjAG3QhwgOhPgwQgbgQglgeQhphThMhXQgkgqgMgdQgYg2AMhSQARhpA5hJQAaggAuglQBRhBBYgnQBfgqBggGQAxgEA7AGQAqAEBCALQCAAVBXAZQBzAgBWAyQAAgKAJgGQAIgGAKAAQAOAAAWANQAgAUA9AxQAYAUAKAKQASARALARQANAUAEAVQAFAYgIAUIgFANQgCAHAAAGQAAAGAJAUQAJAUgCAbQgBASgHAfQgJAogKARQgIAQgSAUIgfAgIgWAdIgVAdQghAng1AiQgiAVhDAhQhQAogoAMQg2ARhFAGQgqAEhUABIgrAAQhkAAg6gIgAoihCQgdAdgCAiQAPAIAKAYQAIAdAGANQAMAdAmAdQAsAeATASQAKAJAUAWQASAVALAKQArAmBQAUQBKATBYACQBJACBagJQBBgGAwgLQBHgPBVgmQBNgjAmgiQA5g1AFhAQAEgogUgtQgOghggguQgjg0gbgVQgYgSgqgSQhNghhlgXQhFgQhygSQgzgIgigDQhkgGhmAlQhhAjhTBDQg/AygSAxQAqgEApgSQgrAbgPAQgAJThzQAAABABAAQAAABAAAAQABABAAAAQAAAAABAAQACgCgBgEIgDgEIgEgEIgCgDIgDgBgAkjBSQhpgtAAg/QAAhABpgtQAlgPApgLQgTAMgKAJQgYAWgCAZQABASgDAJQgBAFgEAHIgGALQgKATADAWQACAWAOAPQASATAgAFQASADAmgDQAEALALAIQAKAHANADQARADAhgGQBsgRA2gKQBdgRBDgVQAYgHAIgMQAIgMgHgQQgHgPgPgHQAQgPADgXQABgJgBgJQAzAjAAAsQAAA/hqAtQhqAtiVAAQiVAAhqgtg");
    this.shape_1.setTransform(1.1041, 2.2029);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    // Layer_3
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#FFFFFF").s().p("AjfG4QgngKg7gWQhEgagjgQQg4gbgngfQgwgng3hNQgkgxgVgoQgbgzgLgvQgKgtADgnQAEg1AehFQAYg4AbgfQAWgZAlgZQB6hSCtgYQCCgRC4AQQBJAGApAKQAcAGAwAQQA1AQAXAGQAqALBXANQBMAOAuAZQA9AgAyBEQBGBggCBiQAAAhgNA2QgNA0gSAdQgLATgSATIgiAjQhEBAgoAdQhQA6hxAhQhXAZh9AQIhLAJQhKAHg1AAQhFAAg5gOgAiqlKQhlAChRATQhOAThIAnQgxAagZAdQgSAVgQAjQgWAzACAoQACAnAeA3QBLCQBoA7QAbAQA8AYIBPAfQAtAQAlAHQBCAOBTgCQA3gCBegMQBfgLAzgOQgZgOhFgOQg+gMgcgXIA9gJQArgHAbgHIApgNIAogNQATgFAjgHIA2gMQA7gQAygiQAmgaAZggQAcglAGgnQAJgxgYgyQgWgvgrgiQghgZgngOQgdgKgtgHIhLgNQgigHhOgYQhIgXgpgHQgZgEgygEQhggHhBAAIgXAAg");
    this.shape_2.setTransform(1.2562, 5.5929);

    this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-71.3, -42.9, 145.1, 93.9);


  (lib.Symbol28copy7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.instance = new lib.CachedBmp_361();
    this.instance.setTransform(-64.35, -42.9, 0.0893, 0.0893);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#3860B1").s().p("Ah7CdQgNgCgKgHQgLgIgEgMQgmADgSgDQgggEgSgUQgOgPgCgWQgDgWAKgTIAGgMQAEgGABgGQADgIgBgSQACgYAYgXQAKgJATgLIARgKIA6ggQAWgMAMgEQAUgIAQAAQAUAAAQALQAQAMACASQBXgdAkgIQAjgHAQAJQAZANAGAvQAVgGAWAJQAVAJALATQAHALABANQABAJgBAJQgDAWgQAQQAPAGAHAQQAHAQgIAMQgIANgYAHQhDAVhdAQQg2AKhsARQgWAEgPAAIgNgBg");
    this.shape.setTransform(0.6373, -4.725);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#5387EE").s().p("AjAG3QhwgOhPgwQgbgQglgeQhphThMhXQgkgqgMgdQgYg2AMhSQARhpA5hJQAaggAuglQBRhBBYgnQBfgqBggGQAxgEA7AGQAqAEBCALQCAAVBXAZQBzAgBWAyQAAgKAJgGQAIgGAKAAQAOAAAWANQAgAUA9AxQAYAUAKAKQASARALARQANAUAEAVQAFAYgIAUIgFANQgCAHAAAGQAAAGAJAUQAJAUgCAbQgBASgHAfQgJAogKARQgIAQgSAUIgfAgIgWAdIgVAdQghAng1AiQgiAVhDAhQhQAogoAMQg2ARhFAGQgqAEhUABIgrAAQhkAAg6gIgAoihCQgdAdgCAiQAPAIAKAYQAIAdAGANQAMAdAmAdQAsAeATASQAKAJAUAWQASAVALAKQArAmBQAUQBKATBYACQBJACBagJQBBgGAwgLQBHgPBVgmQBNgjAmgiQA5g1AFhAQAEgogUgtQgOghggguQgjg0gbgVQgYgSgqgSQhNghhlgXQhFgQhygSQgzgIgigDQhkgGhmAlQhhAjhTBDQg/AygSAxQAqgEApgSQgrAbgPAQgAJThzQAAABABAAQAAABAAAAQABABAAAAQAAAAABAAQACgCgBgEIgDgEIgEgEIgCgDIgDgBgAkjBSQhpgtAAg/QAAhABpgtQAlgPApgLQgTAMgKAJQgYAWgCAZQABASgDAJQgBAFgEAHIgGALQgKATADAWQACAWAOAPQASATAgAFQASADAmgDQAEALALAIQAKAHANADQARADAhgGQBsgRA2gKQBdgRBDgVQAYgHAIgMQAIgMgHgQQgHgPgPgHQAQgPADgXQABgJgBgJQAzAjAAAsQAAA/hqAtQhqAtiVAAQiVAAhqgtg");
    this.shape_1.setTransform(1.1041, 2.2029);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    // Layer_3
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#FFFFFF").s().p("AjfG4QgngKg7gWQhEgagjgQQg4gbgngfQgwgng3hNQgkgxgVgoQgbgzgLgvQgKgtADgnQAEg1AehFQAYg4AbgfQAWgZAlgZQB6hSCtgYQCCgRC4AQQBJAGApAKQAcAGAwAQQA1AQAXAGQAqALBXANQBMAOAuAZQA9AgAyBEQBGBggCBiQAAAhgNA2QgNA0gSAdQgLATgSATIgiAjQhEBAgoAdQhQA6hxAhQhXAZh9AQIhLAJQhKAHg1AAQhFAAg5gOgAiqlKQhlAChRATQhOAThIAnQgxAagZAdQgSAVgQAjQgWAzACAoQACAnAeA3QBLCQBoA7QAbAQA8AYIBPAfQAtAQAlAHQBCAOBTgCQA3gCBegMQBfgLAzgOQgZgOhFgOQg+gMgcgXIA9gJQArgHAbgHIApgNIAogNQATgFAjgHIA2gMQA7gQAygiQAmgaAZggQAcglAGgnQAJgxgYgyQgWgvgrgiQghgZgngOQgdgKgtgHIhLgNQgigHhOgYQhIgXgpgHQgZgEgygEQhggHhBAAIgXAAg");
    this.shape_2.setTransform(1.2562, 5.5929);

    this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-71.3, -42.9, 145.1, 93.9);


  (lib.Symbol28copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.instance = new lib.CachedBmp_360();
    this.instance.setTransform(-64.35, -42.9, 0.0893, 0.0893);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#3860B1").s().p("Ah7CdQgNgCgKgHQgLgIgEgMQgmADgSgDQgggEgSgUQgOgPgCgWQgDgWAKgTIAGgMQAEgGABgGQADgIgBgSQACgYAYgXQAKgJATgLIARgKIA6ggQAWgMAMgEQAUgIAQAAQAUAAAQALQAQAMACASQBXgdAkgIQAjgHAQAJQAZANAGAvQAVgGAWAJQAVAJALATQAHALABANQABAJgBAJQgDAWgQAQQAPAGAHAQQAHAQgIAMQgIANgYAHQhDAVhdAQQg2AKhsARQgWAEgPAAIgNgBg");
    this.shape.setTransform(0.6373, -4.725);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#5387EE").s().p("AjAG3QhwgOhPgwQgbgQglgeQhphThMhXQgkgqgMgdQgYg2AMhSQARhpA5hJQAaggAuglQBRhBBYgnQBfgqBggGQAxgEA7AGQAqAEBCALQCAAVBXAZQBzAgBWAyQAAgKAJgGQAIgGAKAAQAOAAAWANQAgAUA9AxQAYAUAKAKQASARALARQANAUAEAVQAFAYgIAUIgFANQgCAHAAAGQAAAGAJAUQAJAUgCAbQgBASgHAfQgJAogKARQgIAQgSAUIgfAgIgWAdIgVAdQghAng1AiQgiAVhDAhQhQAogoAMQg2ARhFAGQgqAEhUABIgrAAQhkAAg6gIgAoihCQgdAdgCAiQAPAIAKAYQAIAdAGANQAMAdAmAdQAsAeATASQAKAJAUAWQASAVALAKQArAmBQAUQBKATBYACQBJACBagJQBBgGAwgLQBHgPBVgmQBNgjAmgiQA5g1AFhAQAEgogUgtQgOghggguQgjg0gbgVQgYgSgqgSQhNghhlgXQhFgQhygSQgzgIgigDQhkgGhmAlQhhAjhTBDQg/AygSAxQAqgEApgSQgrAbgPAQgAJThzQAAABABAAQAAABAAAAQABABAAAAQAAAAABAAQACgCgBgEIgDgEIgEgEIgCgDIgDgBgAkjBSQhpgtAAg/QAAhABpgtQAlgPApgLQgTAMgKAJQgYAWgCAZQABASgDAJQgBAFgEAHIgGALQgKATADAWQACAWAOAPQASATAgAFQASADAmgDQAEALALAIQAKAHANADQARADAhgGQBsgRA2gKQBdgRBDgVQAYgHAIgMQAIgMgHgQQgHgPgPgHQAQgPADgXQABgJgBgJQAzAjAAAsQAAA/hqAtQhqAtiVAAQiVAAhqgtg");
    this.shape_1.setTransform(1.1041, 2.2029);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    // Layer_3
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#FFFFFF").s().p("AjfG4QgngKg7gWQhEgagjgQQg4gbgngfQgwgng3hNQgkgxgVgoQgbgzgLgvQgKgtADgnQAEg1AehFQAYg4AbgfQAWgZAlgZQB6hSCtgYQCCgRC4AQQBJAGApAKQAcAGAwAQQA1AQAXAGQAqALBXANQBMAOAuAZQA9AgAyBEQBGBggCBiQAAAhgNA2QgNA0gSAdQgLATgSATIgiAjQhEBAgoAdQhQA6hxAhQhXAZh9AQIhLAJQhKAHg1AAQhFAAg5gOgAiqlKQhlAChRATQhOAThIAnQgxAagZAdQgSAVgQAjQgWAzACAoQACAnAeA3QBLCQBoA7QAbAQA8AYIBPAfQAtAQAlAHQBCAOBTgCQA3gCBegMQBfgLAzgOQgZgOhFgOQg+gMgcgXIA9gJQArgHAbgHIApgNIAogNQATgFAjgHIA2gMQA7gQAygiQAmgaAZggQAcglAGgnQAJgxgYgyQgWgvgrgiQghgZgngOQgdgKgtgHIhLgNQgigHhOgYQhIgXgpgHQgZgEgygEQhggHhBAAIgXAAg");
    this.shape_2.setTransform(1.2562, 5.5929);

    this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-71.3, -42.9, 145.1, 93.9);


  (lib.Symbol12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_278();
    this.instance.setTransform(-696.7, -540.3, 0.3801, 0.3801);

    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true}, 1).wait(184));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-696.7, -540.3, 1394.8000000000002, 1079);


  (lib.Symbol8_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance_1 = new lib.CachedBmp_277();
    this.instance_1.setTransform(-74.25, -171.55, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol8_1, new cjs.Rectangle(-74.2, -171.5, 149, 343), null);


  (lib.Locked_1copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_267();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Locked_1copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_266();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Locked_1copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_265();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Locked_1copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_264();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Locked_1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_263();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.Locked_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Key_1
    this.instance = new lib.CachedBmp_262();
    this.instance.setTransform(-241.8, -387.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-241.8, -387.5, 483.5, 775);


  (lib.C_ONcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, 2.275);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_355();
    this.instance.setTransform(-214.65, -220.3, 0.4819, 0.4819);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -224.5, 449.1, 453.9);


  (lib.C_ONcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, 2.275);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_354();
    this.instance.setTransform(-214.65, -220.3, 0.4819, 0.4819);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -224.5, 449.1, 453.9);


  (lib.C_ONcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, 2.275);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_353();
    this.instance.setTransform(-214.65, -220.3, 0.4819, 0.4819);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -224.5, 449.1, 453.9);


  (lib.C_ONcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, 2.275);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_352();
    this.instance.setTransform(-214.65, -220.3, 0.4819, 0.4819);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -224.5, 449.1, 453.9);


  (lib.C_ONcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, 2.275);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_351();
    this.instance.setTransform(-214.65, -220.3, 0.4819, 0.4819);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -224.5, 449.1, 453.9);


  (lib.C_ON = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape.setTransform(0.05, 2.275);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_261();
    this.instance.setTransform(-214.65, -220.3, 0.4819, 0.4819);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -224.5, 449.1, 453.9);


  (lib.C_1_ONcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("1", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,87,255,0.659)").s().p("AgGd1QscgJoaoYQoZoYAcsYQAdsYJDpcQJDpcLhA+QLjA+H7JMQH8JNAVLzQAVLzocIXQoVIPsMAAIgYAAg");
    this.shape.setTransform(-0.1577, -25.9536);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_255();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("6", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,87,255,0.659)").s().p("A0qVgQogoRAPs1QAPszIWorQIWoqL0AAQL0AAIWIqQIWIrAmMrQAmMtovIXQovIXsGACIgHAAQsCAAodoPg");
    this.shape.setTransform(0.9752, -22.4118);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_254();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("5", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,87,255,0.659)").s().p("A09VVQoqohApsHQAosGHwpBQHxpBMNgJQMNgIIjIrQIjIsAWMsQAXMsoQIHQoQIHsmATIg1ABQsDAAoXoQg");
    this.shape.setTransform(0.7834, -24.3999);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_253();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("4", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,87,255,0.659)").s().p("A0pVaQpRoXAxsqQAxsqIzopQIzooLvABQLwACIEIsQIEIrAYNEQAWNEoKHqQoKHpsUAOIgqABQr5AApBoIg");
    this.shape.setTransform(-2.6405, -24.3306);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_252();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ONcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("3", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,87,255,0.659)").s().p("A0YWAQoboAAFtOQAFtNIoozQIno0L1AQQL2AQIDIsQIDIqAXNCQAXNDomH3QonH3r6AMIgkAAQrkAAoOnzg");
    this.shape.setTransform(0.4414, -23.1574);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_251();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib.C_1_ON = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("2", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-11.1, -132.8);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,87,255,0.659)").s().p("A0wVlQodoQANssQANsrIqo3QIro3LsAEQLuADIYJJQIYJKAUM7QAVM9o5HiQo5Hhr7AIIgXAAQrsAAoVoIg");
    this.shape.setTransform(-0.3177, -24.3699);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EAAAAjcQuhAAqSqZQqRqYAAurQAAuqKRqYQKSqZOhAAQOiAAKSKZQKRKYABOqQgBOrqRKYQqSKZuiAAIAAAAgA2f2vQpVJbABNUQgBNVJVJbQJVJbNKAAQNLAAJVpbQJVpbgBtVQABtUpVpbQpVpbtLAAQtKAApVJbg");
    this.shape_1.setTransform(0.05, -21.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_250();
    this.instance.setTransform(-214.65, -243.75, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-224.5, -247.9, 449.1, 453.70000000000005);


  (lib._3_Starscopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_350();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_349();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_348();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_347();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Starscopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_346();
    this.instance.setTransform(-118.5, -142.45, 0.3485, 0.3485);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-118.5, -142.4, 231.1, 223);


  (lib._3_Star_Rcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_345();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_344();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_343();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_342();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_Star_Rcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_341();
    this.instance.setTransform(-98.5, -97, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.5, -97, 197, 194);


  (lib._3_stae_Lcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_340();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_339();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_338();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_337();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib._3_stae_Lcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_336();
    this.instance.setTransform(-98.35, -95.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-98.3, -95.5, 196.5, 191);


  (lib.Symbol520 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_R("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol519 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_L("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol518 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy5("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol517 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy5("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol516 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy4("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol515 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy4("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol514 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy3("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol513 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy3("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol512 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol511 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol510 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_Star_Rcopy2("synched", 0);
    this.instance.setTransform(-165.7, 48.9, 0.7989, 0.7989, -126.6131, 0, 0, -0.3, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-274.9, -60.7, 218.29999999999998, 218.7);


  (lib.Symbol509 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib._3_stae_Lcopy2("synched", 0);
    this.instance.setTransform(179.15, 40.9, 0.7987, 0.7987, -164.4116, 0, 0, -0.1, -0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(83.1, -53.7, 192.1, 189.10000000000002);


  (lib.Symbol45 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_134 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(134).call(this.frame_134).wait(1));

    // Layer_2 (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    var mask_graphics_0 = new cjs.Graphics().p("ACfJ1QgvgNgoguQgggkgZg3QgQgigXhDQiNmShym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBjE4QA8C7AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgLAAQgQAAgPgEg");
    var mask_graphics_3 = new cjs.Graphics().p("AFgJ1QgvgNgoguQgggkgZg3QgRgigXhDQiNmShxm9QgNg0AEgbQAGgkAegYQAegZAjABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C7AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgLAAQgQAAgPgEgAivE+QgbgKgZgbQgigkgjhCQh8jkg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAuIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgPAIgQAAQgPAAgQgHg");
    var mask_graphics_5 = new cjs.Graphics().p("AI6J1QgugNgoguQgggkgag3QgQgigYhDQiNmShxm9QgNg0AEgbQAFgkAegYQAegZAkABQAeAAAcASQAZAQATAbQAQAWANAgIAUA6IBkE4QA8C7AgBdQA1CbAyB5IATAyQAJAcADAXQADAdgIAZQgIAbgSARQgTASgcAFQgKACgLAAQgQAAgQgEgAm5F7QgYgLgOgVQgLgPgMggIh5lGQgehPgOgsQgVhFgKg5QgFghAEgSQAGgcAYgUQAXgTAdgBQAcgCAaARQAaAPANAaQAFAMAFASIAIAeQALAsAfBAQArBaAHARQAMAfAWBEQAUBCAOAiIAWAwQAMAdAGAUQAQA8gWArQgLAWgWAPQgWAOgZABIgIABQgVAAgUgKgAAqE+QgagKgZgbQgigkgjhCQh7jkg2j9QgNg/AIgkQAFgZARgTQASgUAXgFQAWgEAWALQAVAJAPATQANAPAKAXQAHAOAKAdIA0CSQAaBMAOAjQAYA9AXAuIAyBgQAbA5AHAtQAFAjgIAbQgLAigcANQgOAIgQAAQgPAAgRgHg");
    var mask_graphics_7 = new cjs.Graphics().p("AL7J1QgvgNgoguQgggkgZg3QgRgigXhDQiNmShym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C7AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAqHGlQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhHgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBfQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAj4F7QgYgLgPgVQgLgPgLggIh6lGQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBEQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgADrE+QgbgKgZgbQgigkgjhCQh7jkg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAUgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAuIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgPAIgQAAQgPAAgQgHg");
    var mask_graphics_10 = new cjs.Graphics().p("AO6J1QgvgNgoguQgggkgZg3QgRgigXhDQiNmShym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C7AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAnIGlQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhHgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBfQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAtyGZQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi4Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBaQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgKAAgJgCgAg5F7QgYgLgPgVQgLgPgLggIh6lGQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAUBEQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgYABIgHABQgVAAgUgKgAGqE+QgbgKgZgbQgigkgjhCQh8jkg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAuIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHg");
    var mask_graphics_13 = new cjs.Graphics().p("ASSJ1QgvgNgoguQgggkgZg3QgRgigXhDQiNmShym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C7AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAjwGlQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhHgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBfQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgUAAgUgJgAqaGZQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi4Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBaQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAxfGKQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhKQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBMQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgUAAQgPAAgOgEgACeF7QgYgLgPgVQgLgPgLggIh5lGQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAbgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBEQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAKCE+QgbgKgZgbQgigkgjhCQh8jkg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAuIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgQAAQgPAAgRgHg");
    var mask_graphics_16 = new cjs.Graphics().p("AVoJ1QgugNgoguQgggkgag3QgQgigYhDQiNmShxm9QgNg0AEgbQAFgkAegYQAegZAkABQAeAAAcASQAZAQATAbQAQAWANAgIAUA6IBkE4QA8C7AgBdQA1CbAyB5IATAyQAJAcADAXQADAdgIAZQgIAbgSARQgTASgcAFQgKACgLAAQgQAAgQgEgA1rGfQgggcgJgpQgDgPgBgUIgCgkQgBgdgGg6IgGg2QgHg6gVhpQgYhxgHgzQgEgggEgpQgDgoAFgWQAFgcATgWQAUgWAagEQAZgEAZAOQAXANAQAXQANAUAJAcQAFAQAHAjIAyDsQASBUADAsIAFAvIAFAYQAEAOABAJQACAOgBAaQAAAbABANIAFAZIAFAZQAEAsgaAhQgNAQgTAKQgTAKgUABIgFAAQgkAAgggcgAgZGlQgngSgTgvQgIgTgFgcIgJgxQgIgrgdhPIgohyQgXhHgliNIgWhXQgShDAagaQAMgNATgCQATgCARAHQAcALAaAlQAdApAZBAQAOAlAZBMQAMAiAmBfQAgBRAQAyQALAlASBMQAMA2gCAdQgEAxggAXQgSANgYABIgEAAQgTAAgTgJgAnEGZQgVgFgRgNQgXgSgNgjQgIgXgHgrIgLhEIgMg+QgEgXgzi4Qgjh9AAhUQAAgmAKgTQAIgRAQgLQAQgLATgCQATgBARAIQASAIALAPQAKAOAJAgIBSESIAaBaQAQA/AaB3QAHAjABARQACAegJAVQgHAUgQAOQgRAPgUAGQgLADgLAAQgKAAgKgCgAuJGKQgmgMgQgiQgIgRgDgkQgIhQgLg/QgMhHgOgyIgXhKQgQgwgHgbQgPg4gHg8QgLhSAhgiQAMgMARgGQARgFAQADQAkAGAYAmQAOAWAOAxIBJD3IAUBMQAIAhAHAmIAVCFQAEAXAAANQAAAUgGAOQgMAgglAPQgWAJgVAAQgPAAgOgEgAF0F7QgYgLgOgVQgLgPgMggIh5lGQgehPgOgsQgVhFgKg5QgFghAEgSQAGgcAYgUQAXgTAdgBQAcgCAaARQAaAPANAaQAFAMAFASIAIAeQALAsAfBAQArBaAHARQAMAfAWBEQAUBCAOAiIAWAwQAMAdAGAUQAQA8gWArQgLAWgWAPQgWAOgZABIgHABQgWAAgUgKgANYE+QgagKgagbQgigkgjhCQh7jkg2j9QgNg/AIgkQAFgZARgTQASgUAXgFQAWgEAWALQAVAJAPATQANAPAKAXQAHAOAKAdIA0CSQAbBMAOAjQAYA9AXAuIAyBgQAbA5AHAtQAFAjgIAbQgLAigcANQgOAIgQAAQgPAAgRgHg");
    var mask_graphics_19 = new cjs.Graphics().p("AYfJ1QgvgNgoguQgggkgZg3QgRgigXhDQiNmShym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C7AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAy0GfQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhpQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBUADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgA4YGtQgcgCgXgOQgkgUgMgjQgFgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgxIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAdgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA8QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgACcGlQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgXhHgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAaALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBfQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAkNGZQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi4Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBaQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgKAAgJgCgArSGKQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhKQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBMQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAIrF7QgYgLgPgVQgLgPgLggIh6lGQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBEQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAQPE+QgbgKgZgbQgigkgjhCQh8jkg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAuIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHg");
    var mask_graphics_22 = new cjs.Graphics().p("Ab8J1QgugNgoguQgggkgag3QgQgigYhDQiNmShxm9QgNg0AEgbQAFgkAegYQAegZAkABQAeAAAcASQAZAQATAbQAQAWANAgIAUA6IBkE4QA8C7AgBdQA1CbAyB5IATAyQAJAcADAXQADAdgIAZQgIAbgSARQgTASgcAFQgKACgLAAQgQAAgQgEgAvXGfQgggcgJgpQgDgPgBgUIgCgkQgBgdgGg6IgGg2QgHg6gVhpQgYhxgHgzQgEgggEgpQgDgoAFgWQAFgcATgWQAUgWAagEQAZgEAZAOQAXANAQAXQANAUAJAcQAFAQAHAjIAyDsQASBUADAsIAFAvIAFAYQAEAOABAJQACAOgBAaQAAAbABANIAFAZIAFAZQAEAsgaAhQgNAQgTAKQgTAKgUABIgFAAQgkAAgggcgA07GtQgbgCgYgOQgkgUgMgjQgFgOAAgWIgBgmQgBgPgEgbIgFgrQgDgaAEhIQADhIgIgxIgLg7QgFgdgBhHIgBiGQAAgoAHgSQALgcAdgOQAdgOAdAJQAiAKAXAnQAOAYAMAxQALApAEAaIADAnIAEAnQACARAJAjQAJAiACASQAFAggBA8QgCBAADAdIAGAlIAGAlQACAXABAuIADAfQABATgCANQgDAagTAWQgRAVgaAKQgSAHgVAAIgNgBgAF6GlQgngSgTgvQgIgTgFgcIgJgxQgIgrgdhPIgohyQgXhHgliNIgWhXQgShDAagaQAMgNATgCQATgCARAHQAcALAaAlQAdApAZBAQAOAlAZBMQAMAiAnBfQAgBRAQAyQALAlASBMQAMA2gCAdQgEAxggAXQgSANgYABIgEAAQgUAAgTgJgAgwGZQgVgFgRgNQgXgSgNgjQgIgXgHgrIgLhEIgMg+QgEgXgzi4Qgjh9AAhUQAAgmAKgTQAIgRAQgLQAQgLATgCQATgBARAIQASAIALAPQAKAOAJAgIBSESIAZBaQAQA/AaB3QAHAjABARQACAegJAVQgHAUgQAOQgRAPgTAGQgLADgLAAQgKAAgKgCgAn1GKQgmgMgQgiQgIgRgDgkQgIhQgLg/QgMhHgOgyIgXhKQgQgwgHgbQgPg4gHg8QgLhSAhgiQAMgMARgGQARgFAQADQAkAGAYAmQAOAWAOAxIBJD3IAUBMQAIAhAHAmIAVCFQAEAXAAANQAAAUgGAOQgMAgglAPQgWAJgVAAQgPAAgOgEgAMIF7QgYgLgOgVQgLgPgMggIh5lGQgehPgOgsQgVhFgKg5QgFghAEgSQAGgcAYgUQAXgTAdgBQAcgCAaARQAaAPANAaQAFAMAFASIAIAeQALAsAfBAQArBaAHARQAMAfAWBEQAUBCAOAiIAWAwQAMAdAGAUQAQA8gWArQgLAWgWAPQgWAOgZABIgHABQgWAAgUgKgA9CFZQgfgXgMgiQgHgVgBgeIADg1QADg6gGhtQgGhxACg1IACgxQABgdgCgUIgFgvQgDgcACgSQABgNAEgSIAIgfIANgvQAJgaANgQQANgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaAAA2QACAgAIA/QACAbAAA1QgBA2ACAaIAJBOQAJBHABCQIgBAoQgCAWgFARQgMAigfAWQgfAXgkAAIgCAAQgjAAgfgVgATsE+QgagKgagbQgigkgjhCQh7jkg2j9QgNg/AIgkQAFgZARgTQASgUAXgFQAWgEAWALQAVAJAPATQANAPAKAXQAHAOAKAdIA0CSQAbBMAOAjQAYA9AXAuIAyBgQAbA5AHAtQAFAjgIAbQgLAigcANQgOAIgQAAQgPAAgRgHg");
    var mask_graphics_25 = new cjs.Graphics().p("AfRJ1QgvgNgoguQgggkgZg3QgRgigXhDQiNmShym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C7AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgLAAQgQAAgPgEgAsCGfQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhpQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBUADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgAxmGtQgcgCgXgOQgkgUgMgjQgFgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgxIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAdgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA8QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAJOGlQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhHgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBfQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgACkGZQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgyi4Qgih9AAhUQAAgmAJgTQAIgRARgLQAPgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBaQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgMADgLAAQgKAAgJgCgAkgGKQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhKQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBMQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgEggGAGHQgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh6QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBAIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQABQgRAAgPgEgAPdF7QgYgLgPgVQgLgPgLggIh6lGQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBEQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgA5tFZQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhxACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBHABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgCAAQgjAAgegVgAXBE+QgbgKgZgbQgigkgjhCQh8jkg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAuIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgPAIgQAAQgPAAgQgHg");
    var mask_graphics_28 = new cjs.Graphics().p("EAkCAKNQgugNgoguQgggkgag2QgQgjgYhDQiNmShxm8QgNg1AEgbQAFgjAegZQAegZAkABQAeABAcASQAZAQATAaQAQAWANAgIAUA7IBkE4QA8C6AgBdQA1CbAyB6IATAyQAJAcADAXQADAcgIAZQgIAcgSARQgTARgcAGQgKACgLAAQgQAAgQgFgAnRG4QgggdgJgpQgDgOgBgVIgCgjQgBgegGg6IgGg1QgHg7gVhpQgYhxgHgzQgEgggEgpQgDgnAFgWQAFgdATgVQAUgXAagEQAZgDAZAOQAXANAQAXQANATAJAcQAFARAHAjIAyDqQASBVADAsIAFAwIAFAXQAEAPABAJQACANgBAbQAAAaABANIAFAZIAFAZQAEAsgaAhQgNAQgTAKQgTAKgUABIgFABQgkAAgggcgAs1HGQgbgDgYgNQgkgUgMgjQgFgPAAgWIgBglQgBgQgEgbIgFgrQgDgZAEhIQADhJgIgxIgLg6QgFgegBhGIgBiHQAAgnAHgSQALgcAdgPQAdgOAdAJQAiALAXAnQAOAYAMAwQALAqAEAaIADAnIAEAmQACARAJAjQAJAiACASQAFAfgBA9QgCBAADAdIAGAmIAGAlQACAXABAtIADAgQABASgCANQgDAbgTAVQgRAVgaAKQgSAIgVAAIgNgBgAOAG9QgngSgTgvQgIgSgFgdIgJgwQgIgrgdhQIgohyQgXhHgliMIgWhXQgShDAagbQAMgMATgCQATgDARAHQAcAMAaAkQAdApAZBBQAOAlAZBMQAMAhAnBgQAgBRAQAyQALAlASBMQAMA2gCAcQgEAxggAXQgSANgYABIgEABQgUAAgTgKgAHVGxQgVgEgRgNQgXgTgNgjQgIgWgHgrIgLhEIgMg/QgEgWgzi4Qgjh9AAhVQAAglAKgUQAIgQAQgLQAQgLATgCQATgCARAIQASAIALAPQAKAPAJAfIBSERIAaBcQAQA+AaB4QAHAiABASQACAdgJAWQgHATgQAPQgRAPgUAFQgLADgLAAQgKAAgKgCgAAQGiQglgLgQgjQgIgQgDglQgIhPgLg/QgMhHgOgzIgXhJQgQgxgHgaQgPg5gHg7QgLhSAhgjQAMgMARgFQARgGAQADQAkAHAYAlQAOAXANAwIBJD3IAUBNQAIAgAHAnIAVCEQAEAYAAAMQAAAUgGAPQgMAfglAQQgWAJgVAAQgPAAgOgFgA7UGfQgXgHgTgRQgSgRgJgXQgMgeAFgpQADgUAMgzQAXhaAPh6QANhsAHiJQAEhMAFiqQABgoAJgRQAKgTAVgLQAUgKAWACQAWACASAOQATAMALATQALAUAFAcQADARACAjIAMDsQAGBkgDA4QgBASgHBBIgYDLQgHA7gMAiQgRAxgiAXQgUAOgZAEIgRABQgQAAgPgFgAUOGTQgYgLgOgUQgLgQgMgfIh5lHQgehOgOgtQgVhEgKg6QgFghAEgSQAGgcAYgTQAXgUAdgBQAcgBAaAQQAaAQANAaQAFAMAFARIAIAeQALAsAfBBQArBaAHAQQAMAgAWBEQAUBCAOAiIAWAwQAMAcAGAVQAQA7gWArQgLAXgWAOQgWAOgZACIgHAAQgWAAgUgKgA08FxQgfgWgMgiQgHgWgBgeIADg1QADg5gGhuQgGhxACg1IACgxQABgdgCgUIgFguQgDgcACgTQABgNAEgSIAIgeIANgwQAJgaANgQQANgOASgJQASgIATAAQApABAbAfQAWAZAFAuQADAaAAA1QACAgAIBAQACAaAAA2QgBA1ACAbIAJBNQAJBHABCRIgBAoQgCAWgFAQQgMAigfAXQgfAWgkABIgCAAQgjAAgfgWgAbyFXQgagLgagaQgiglgjhBQh7jkg2j9QgNhAAIgjQAFgZARgTQASgVAXgEQAWgEAWAKQAVAKAPASQANAQAKAXQAHAOAKAcIA0CTQAbBLAOAkQAYA9AXAtIAyBhQAbA4AHAtQAFAkgIAaQgLAjgcANQgOAHgQAAQgPAAgRgGgEgklACnQgZgEgVgOQgVgOgMgVQgQgagBghQgBgfALgeQALgcAmg3QCRjMBPicIAnhOQAYgtAUgfQAXgjAXgMQAUgKAYAEQAXADASAPQASAOAKAWQAJAUAAAYQABAXgNAsQglB6g/B1QgcA0hJBxQhDBogfA9IgQAfQgKARgLALQgRARgYAJQgRAGgRAAIgPgCg");
    var mask_graphics_32 = new cjs.Graphics().p("EAl0AL1QgugNgoguQgggkgag3QgQgigYhDQiNmThxm8QgNg0AEgbQAFgkAegYQAegZAkABQAeAAAcASQAZAQATAbQAQAWANAgIAUA6IBkE4QA8C7AgBdQA1CbAyB5IATAyQAJAcADAXQADAdgIAZQgIAbgSARQgTASgcAFQgKACgLAAQgQAAgQgEgAlfIfQgggcgJgpQgDgPgBgUIgCgkQgBgdgGg6IgGg2QgHg6gVhqQgYhwgHgzQgEgggEgpQgDgoAFgWQAFgcATgWQAUgWAagEQAZgEAZAOQAXANAQAXQANAUAJAcQAFAQAHAjIAyDrQASBVADAsIAFAvIAFAYQAEAOABAJQACAOgBAaQAAAbABANIAFAZIAFAZQAEAsgaAhQgNAQgTAKQgTAKgUABIgFAAQgkAAgggcgArDItQgbgCgYgOQgkgUgMgjQgFgOAAgWIgBgmQgBgPgEgbIgFgrQgDgaAEhIQADhIgIgyIgLg7QgFgdgBhGIgBiGQAAgoAHgSQALgcAdgOQAdgOAdAJQAiAKAXAnQAOAYAMAxQALApAEAaIADAnIAEAnQACARAJAiQAJAiACASQAFAggBA9QgCBAADAdIAGAlIAGAlQACAXABAuIADAfQABATgCANQgDAagTAWQgRAVgaAKQgSAHgVAAIgNgBgAPyIlQgngSgTgvQgIgTgFgcIgJgxQgIgrgdhPIgohyQgXhIgliMIgWhXQgShDAagaQAMgNATgCQATgCARAHQAcALAaAlQAdApAZBAQAOAlAZBLQAMAiAnBgQAgBRAQAyQALAlASBMQAMA2gCAdQgEAxggAXQgSANgYABIgEAAQgUAAgTgJgAJHIZQgVgFgRgNQgXgSgNgjQgIgXgHgrIgLhEIgMg+QgEgXgzi5Qgjh8AAhUQAAgmAKgTQAIgRAQgLQAQgLATgCQATgBARAIQASAIALAPQAKAOAJAgIBSERIAaBbQAQA/AaB3QAHAjABARQACAegJAVQgHAUgQAOQgRAPgUAGQgLADgMAAQgJAAgKgCgACCIKQgmgMgQgiQgIgRgDgkQgIhQgLg/QgMhHgOgyIgWhLQgQgwgHgbQgPg3gHg8QgLhSAhgiQAMgMAQgGQARgFAQADQAkAGAYAmQAOAWAOAxIBJD2IAUBNQAIAhAHAmIAVCFQAEAXAAANQAAAUgGAOQgMAgglAPQgWAJgVAAQgPAAgOgEgA5iIHQgXgHgTgSQgSgRgJgXQgMgdAFgpQADgUAMg0QAXhaAPh7QANhrAHiJQAEhMAFiqQABgnAJgRQAKgTAVgLQAUgLAWADQAWACASANQATANALATQALATAFAdQADAQACAjIAMDtQAGBjgDA3QgBATgHBBIgYDMQgHA6gMAiQgRAxgiAYQgUANgZAEIgRABQgQAAgPgEgAWAH7QgYgLgOgVQgLgPgMggIh5lHQgehPgOgrQgVhFgKg5QgFghAEgSQAGgcAYgUQAXgTAdgBQAcgCAaARQAaAPANAaQAFAMAFASIAIAeQALAsAfBAQArBZAHARQAMAfAWBFQAUBCAOAiIAWAwQAMAdAGAUQAQA8gWArQgLAWgWAPQgWAOgZABIgIABQgVAAgUgKgAzKHZQgfgXgMgiQgHgVgBgeIADg1QADg6gGhtQgGhyACg0IACgxQABgdgCgUIgFgvQgDgcACgSQABgNAEgSIAIgfIANgvQAJgaANgQQANgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaAAA2QACAgAIA/QACAbAAA1QgBA1ACAaIAJBOQAJBIABCQIgBAoQgCAWgFARQgMAigfAWQgfAXgkAAIgCAAQgjAAgfgVgAdkG+QgagKgagbQgigkgjhCQh7jlg2j8QgNg/AIgkQAFgZARgTQASgUAXgFQAWgEAWALQAVAJAPATQANAPAKAXQAHAOAKAdIA0CSQAbBMAOAiQAYA9AXAvIAyBgQAbA5AHAtQAFAjgIAbQgLAigcANQgOAIgQAAQgPAAgRgHgEgizAEPQgZgEgVgOQgVgOgMgWQgQgZgBgiQgBgfALgeQALgdAmg2QCRjLBPidIAnhOQAYgtAUgeQAXgkAXgLQAUgKAYADQAXAEASAOQASAOAKAWQAJAVAAAXQABAYgNAsQglB5g/B2QgcAzhJBwQhDBpgfA9IgQAfQgKARgLALQgRASgYAIQgRAGgRAAIgPgBgEgmRgEXQgggBgbgVQgagVgKgfQgLgeAHghQAHggAVgZQAQgTAsgeIBJgwQBAgrAagaIAngrQAYgaASgNQAbgVAtgNQAXgGAPABQATACAQAMQAPALAKASQAMAXgCAVQgBARgMATQgIAMgQASQg9BEgVAgQgNAWgHAKQgTAbgvAoIhLBAQglAggZAOQgjATggAAIgEAAg");
    var mask_graphics_34 = new cjs.Graphics().p("EAnuAOVQgugNgoguQgggkgag3QgQgigYhDQiNmThxm8QgNg0AEgbQAFgkAegYQAegZAkABQAeAAAcASQAZAQATAbQAQAWANAgIAUA6IBkE3QA8C8AgBdQA1CbAyB5IATAyQAJAcADAXQADAdgIAZQgIAbgSARQgTASgcAFQgKACgLAAQgQAAgQgEgAjlK/QgggcgJgpQgDgPgBgUIgCgkQgBgdgGg6IgGg2QgHg6gVhqQgYhxgHgzQgEgggEgpQgDgnAFgWQAFgcATgWQAUgWAagEQAZgEAZAOQAXANAQAXQANAUAJAcQAFAQAHAiIAyDsQASBVADAsIAFAvIAFAYQAEAOABAJQACAOgBAaQAAAbABANIAFAZIAFAZQAEAsgaAhQgNAQgTAKQgTAKgUABIgFAAQgkAAgggcgApJLNQgbgCgYgOQgkgUgMgjQgFgOAAgWIgBgmQgBgPgEgbIgFgrQgDgaAEhIQADhIgIgyIgLg7QgFgdgBhHIgBiFQAAgoAHgSQALgcAdgOQAdgOAdAJQAiAKAXAnQAOAYAMAxQALAoAEAaIADAnIAEAnQACARAJAjQAJAiACASQAFAggBA9QgCBAADAdIAGAlIAGAlQACAXABAuIADAfQABATgCANQgDAagTAWQgRAVgaAKQgSAHgVAAIgNgBgARsLFQgngSgTgvQgIgTgFgcIgJgxQgIgrgdhPIgohyQgXhIgliNIgWhXQgShCAagaQAMgNATgCQATgCARAHQAcALAaAlQAdApAZA/QAOAlAZBMQAMAiAnBgQAgBRAQAyQALAlASBMQAMA2gCAdQgEAxggAXQgSANgYABIgEAAQgUAAgTgJgALBK5QgVgFgRgNQgXgSgNgjQgIgXgHgrIgLhEIgMg+QgEgXgzi5Qgjh9AAhUQAAglAKgTQAIgRAQgLQAQgLATgCQATgBARAIQASAIALAPQAKAOAJAgIBSERIAaBbQAQA/AaB3QAHAjABARQACAegJAVQgHAUgQAOQgRAPgUAGQgLADgMAAQgJAAgKgCgAD8KqQgmgMgQgiQgIgRgDgkQgIhQgLg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gHg8QgLhRAhgiQAMgMARgGQARgFAQADQAkAGAYAmQAOAWAOAwIBJD3IAUBNQAIAhAHAmIAVCFQAEAXAAANQAAAUgGAOQgMAgglAPQgWAJgVAAQgPAAgOgEgA3oKnQgXgHgTgSQgSgRgJgXQgMgdAFgpQADgUAMg0QAXhaAPh7QANhsAHiJQAEhLAFiqQABgnAJgRQAKgTAVgLQAUgLAWADQAWACASANQATANALATQALATAFAdQADAQACAjIAMDsQAGBjgDA4QgBATgHBBIgYDMQgHA6gMAiQgRAxgiAYQgUANgZAEIgRACQgQAAgPgFgAX6KbQgYgLgOgVQgLgPgMggIh5lHQgehPgOgsQgVhFgKg5QgFggAEgSQAGgcAYgUQAXgTAdgBQAcgCAaARQAaAPANAaQAFAMAFASIAIAdQALAsAfBAQArBaAHARQAMAfAWBFQAUBCAOAiIAWAwQAMAdAGAUQAQA8gWArQgLAWgWAPQgWAOgZABIgIABQgVAAgUgKgAxQJ5QgfgXgMgiQgHgVgBgeIADg1QADg6gGhtQgGhyACg1IACgxQABgdgCgUIgFguQgDgcACgSQABgNAEgSIAIgfIANgvQAJgaANgQQANgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaAAA2QACAgAIA+QACAbAAA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgCAWgFARQgMAigfAWQgfAXgkAAIgCAAQgjAAgfgVgAfeJeQgagKgagbQgigkgjhCQh7jlg2j8QgNg/AIgkQAFgZARgTQASgUAXgFQAWgEAWALQAVAJAPATQANAPAKAXQAHAOAKAdIA0CRQAbBMAOAjQAYA9AXAvIAyBgQAbA5AHAtQAFAjgIAbQgLAigcANQgOAIgQAAQgPAAgRgHgEgg5AGvQgZgEgVgOQgVgOgMgWQgQgZgBgiQgBgfALgeQALgdAmg2QCRjLBPidIAnhOQAYgtAUgeQAXgkAXgLQAUgKAYADQAXAEASAOQASAOAKAWQAJAVAAAXQABAYgNAsQglB5g/B1QgcAzhJBxQhDBpgfA9IgQAfQgKARgLALQgRASgYAIQgRAGgRAAIgPgBgEgkXgB3QgggBgbgVQgagVgKgfQgLgeAHghQAHggAVgZQAQgTAsgeIBJgwQBAgrAagaIAngrQAYgaASgNQAbgVAtgNQAXgGAPABQATACAQAMQAPALAKASQAMAXgCAVQgBARgMATQgIAMgQASQg9BEgVAgQgNAWgHAKQgTAbgvAoIhLBAQglAggZAOQgjATggAAIgEAAgEgoigHuQgigJgVgZQgVgZgCgjQgCgjARgcQAQgaAugeQAkgZBTgyICbhfQAdgRARgIQAZgNAXgEQAbgEAZAHQAbAIAPAUQAQAWgCAeQgCAdgQAZQgOAVgYAUQgPAMgfAVIi4B8QgfATgTAQIgkAdQgVAQgSAHQgRAHgTAAQgOAAgOgEg");
    var mask_graphics_37 = new cjs.Graphics().p("EApEAQqQgugNgoguQgggkgag2QgQgjgYhDQiNmShxm8QgNg1AEgbQAFgjAegZQAegZAkABQAeABAcASQAZAQATAaQAQAWANAgIAUA7IBkE3QA8C7AgBdQA1CbAyB6IATAyQAJAcADAXQADAcgIAZQgIAcgSARQgTARgcAGQgKACgLAAQgQAAgQgFgAiPNVQgggdgJgpQgDgOgBgVIgCgjQgBgegGg6IgGg1QgHg7gVhqQgYhxgHgzQgEgggEgpQgDgnAFgWQAFgdATgVQAUgXAagEQAZgDAZAOQAXANAQAXQANATAJAcQAFARAHAjIAyDrQASBVADAsIAEAwIAFAXQAEAPABAJQACANgBAbQAAAaABANIAFAZIAFAZQAEAsgaAhQgMAQgTAKQgTAKgUABIgFAAQgkAAgggbgAnzNjQgbgDgYgNQgkgUgMgjQgFgPAAgWIgBglQgBgQgEgbIgFgrQgDgZAEhIQADhJgIgxIgLg7QgFgegBhGIgBiHQAAgnAHgSQALgcAdgPQAdgOAdAJQAiALAXAnQAOAYAMAwQALAqAEAaIADAnIAEAmQACARAJAjQAJAiACASQAFAggBA9QgCBAADAdIAGAmIAGAlQACAXABAtIADAgQABASgCANQgDAbgTAVQgRAVgaAKQgSAHgVAAIgNAAgATCNaQgngSgTgvQgIgSgFgdIgJgwQgIgrgdhQIgohyQgXhIgliMIgWhXQgShDAagbQAMgMATgCQATgDARAHQAcAMAaAkQAdApAZBBQAOAlAZBMQAMAhAnBhQAgBRAQAyQALAlASBMQAMA2gCAcQgEAxggAXQgSANgYABIgEAAQgUAAgTgJgAMXNOQgVgEgRgNQgXgTgNgjQgIgWgHgrIgLhEIgMg/QgEgWgzi5Qgjh9AAhVQAAglAKgUQAIgQAQgLQAQgLATgCQATgCARAIQASAIALAPQAKAPAJAfIBSESIAaBcQAQA+AaB4QAHAiABASQACAdgJAWQgHATgQAPQgRAPgUAFQgLADgMAAQgJAAgKgCgAFSM/QgmgLgQgjQgIgQgDglQgIhPgLg/QgMhHgOgzIgXhKQgQgxgHgaQgPg5gHg7QgLhSAhgjQAMgMARgFQARgGAQADQAkAHAYAlQAOAXAOAwIBJD4IAUBNQAIAgAHAnIAVCEQAEAYAAAMQAAAUgGAPQgMAfglAQQgWAJgVAAQgPAAgOgFgA2SM8QgXgHgTgRQgSgRgJgXQgMgeAFgpQADgUAMgzQAXhaAPh7QANhsAHiJQAEhMAFipQABgoAJgRQAKgTAVgLQAUgKAWACQAWACASAOQATAMALATQALAUAFAcQADARACAjIAMDrQAGBkgDA4QgBASgHBCIgYDLQgHA7gMAiQgRAxgiAXQgUAOgZAEIgRABQgQAAgPgFgAZQMwQgYgLgOgUQgLgQgMgfIh5lIQgehOgOgtQgVhEgKg6QgFghAEgSQAGgcAYgTQAXgUAdgBQAcgBAaAQQAaAQANAaQAFAMAFARIAIAeQALAsAfBBQArBaAHAQQAMAgAWBFQAUBCAOAiIAWAwQAMAcAGAVQAQA7gWArQgLAXgWAOQgWAOgZACIgIAAQgVAAgUgKgAv6MOQgfgWgMgiQgHgWgBgeIADg1QADg5gGhuQgGhyACg1IACgxQABgdgCgUIgFguQgDgcACgTQABgNAEgSIAIgeIANgvQAJgaANgQQANgOASgJQASgIATAAQApABAbAfQAWAZAFAtQADAaAAA1QACAgAIBAQACAaAAA2QgBA1ACAbIAJBOQAJBHABCRIgBAoQgCAWgFAQQgMAigfAXQgfAWgkABIgCAAQgjAAgfgWgEAg0AL0QgagLgagaQgiglgjhBQh7jlg2j9QgNhAAIgjQAFgZARgSQASgVAXgEQAWgEAWAKQAVAKAPARQANAQAKAXQAHAOAKAcIA0CTQAbBLAOAkQAYA9AXAuIAyBhQAbA4AHAtQAFAkgIAaQgLAjgcANQgOAHgQAAQgPAAgRgGgA/jJEQgZgEgVgOQgVgOgMgVQgQgagBghQgBgfALgfQALgcAmg3QCRjMBPibIAnhOQAYgtAUgfQAXgjAXgMQAUgKAYAEQAXADASAPQASAOAKAWQAJAUAAAYQABAXgNAsQglB5g/B1QgcA0hJBxQhDBpgfA9IgQAfQgKARgLALQgRARgYAJQgRAFgRAAIgPgBgEgjBAAeQgggCgbgVQgagUgKgeQgLgeAHghQAHggAVgZQAQgTAsgeIBJgwQBAgrAagaIAngrQAYgbASgNQAbgUAtgNQAXgHAPABQATACAQAMQAPAMAKARQAMAXgCAWQgBARgMASQgIAMgQATQg9BDgVAhQgNAWgHAKQgTAbgvAoIhLA/QglAggZANQgjAUggAAIgEAAgEgnMgFYQgigJgVgZQgVgagCgjQgCgjARgcQAQgaAugeQAkgYBTgzICbheQAdgSARgIQAZgMAXgEQAbgFAZAHQAbAIAPAUQAQAWgCAfQgCAcgQAZQgOAVgYAUQgPAMgfAWIi4B7QgfAUgTAPIgkAeQgVAQgSAHQgRAHgTAAQgOAAgOgEgEgqMgKQQgYgKgOgTQgPgUgEgZQgEgZAIgXQALgfAigbQAQgMAwgeQBMgtA/g4IAogjQAXgTAUgMQA2ggAvAKQAbAFATAUQAUAVABAZQABAWgMAXQgJAQgTAWQg2A9hLA9Qg7AvhVA6QgbASgOAHQgYAMgWABIgJABQgUAAgSgJg");
    var mask_graphics_40 = new cjs.Graphics().p("EArfASvQgvgNgoguQgggkgZg2QgRgjgXhDQiNmShym9QgNg1AEgbQAGgjAegYQAegZAkABQAdABAcARQAaARATAaQAQAVANAgIAUA7IBkE3QA8C8AgBdQA1CbAxB5IAUAzQAJAcACAWQADAdgHAZQgIAcgTAQQgSASgcAGQgLABgKABQgQAAgQgFgAALPaQgfgdgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg1QgHg7gWhqQgXhxgHgzQgFgggDgpQgDgnAEgXQAGgcASgVQAUgXAbgEQAZgDAYAOQAXAMAPAYQANATAJAcQAFAQAIAkIAyDrQASBVADAsIAEAwIAGAXQAEAOABAKQACANgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgbgAlYPnQgcgCgXgNQgkgVgMgiQgFgPgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhHQAEhJgIgyIgLg6QgFgegBhGIgBiHQAAgnAGgTQALgcAdgOQAegOAcAJQAiAKAXAoQAOAXANAxQALApADAaIAEAoIADAmQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAYACAtIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAVcPfQgmgSgUgvQgHgTgGgcIgIgwQgIgsgdhPIgohyQgYhIgkiMIgXhYQgShDAagaQANgNATgCQASgCASAHQAbAMAaAkQAeApAYBBQAOAkAZBMQAMAiAnBhQAhBRAPAxQAMAlASBNQAMA1gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAOyPTQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAglAJgUQAIgQARgLQAQgMASgCQATgBASAIQARAIALAPQALAPAJAfIBRESIAaBbQARA/AZB4QAHAiACARQABAegIAWQgIATgQAPQgQAOgUAGQgLADgMAAQgJAAgKgCgAHtPEQgngMgPgiQgIgQgDglQgJhQgKg+QgMhIgOgyIgXhKQgQgxgHgaQgPg5gIg7QgLhSAhgjQAMgMARgFQARgGARADQAkAGAXAmQAPAXAOAwIBJD4IATBMQAJAhAHAmIAVCFQADAYAAAMQABAUgGAPQgMAfgmAQQgWAIgVAAQgOAAgOgEgAz4PBQgXgHgSgSQgTgQgJgXQgLgeAFgpQACgUANg0QAWhZAQh7QANhtAHiJQAEhLAFirQABgnAJgQQAJgTAVgLQAVgKAWACQAVACATANQASANALASQAMATAFAdQADAQACAkIAMDsQAGBjgEA4QgBATgHBCIgYDLQgGA6gMAjQgRAwgjAYQgUANgZAFIgQABQgQAAgQgFgAbrO1QgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgBAaAQQAZAPANAaQAGANAFARIAHAeQAMAsAfBBQArBZAGARQANAfAVBGQAVBBAOAjIAVAvQAMAdAGAVQARA7gWArQgMAWgWAPQgWAOgZACIgHAAQgVAAgUgKgAtfOTQgggXgLghQgIgWAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgbACgTQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBPQAJBHABCRIgBAnQgBAXgGAQQgLAiggAXQgfAWgkAAIgBABQgjAAgfgWgEAjPAN4QgbgKgZgaQgiglgjhBQh8jlg1j+QgOg/AIgkQAGgYAQgUQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAYQAGAOAKAcIA0CTQAbBLAOAkQAYA8AYAvIAyBhQAbA4AGAtQAFAjgIAbQgLAjgbAMQgOAIgRAAQgPAAgQgHgA9JLJQgYgEgVgOQgVgOgNgVQgPgagBgiQgCgeAMgfQALgcAmg3QCRjMBPidIAnhOQAXgrAUgfQAXgjAXgMQAVgKAYADQAXAEASAPQARANAKAWQAJAVABAYQAAAWgNAsQgkB6hAB1QgcA0hIBwQhDBpgfA+IgQAeQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgEggnACiQgfgBgbgVQgbgVgKgfQgKgdAHgiQAGgeAVgZQARgUAsgeIBIgwQBBgrAagaIAngqQAXgbASgNQAcgVAsgMQAYgHAPABQASACAQAMQAPAMAKARQAMAXgBAWQgCARgMASQgHAMgRASQg8BEgVAgQgNAXgIAKQgTAbgvAmIhKBAQgmAggYAOQgjAUggAAIgFgBgEgkygDTQghgKgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgYBTgzICbhfQAcgRARgIQAagNAXgDQAbgFAZAHQAbAIAOAUQAQAWgCAfQgCAcgQAZQgNAVgZAUQgOAMggAVIi4B8QgeAUgUAPIgjAeQgVAQgSAGQgSAHgSAAQgOAAgPgDgEgnygILQgXgLgPgSQgPgUgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgUATgLQA3ggAvAJQAaAGAUAUQAUAUABAaQABAVgNAXQgJARgTAWQg2A9hLA8Qg6AwhWA6QgbARgOAIQgYALgVACIgKAAQgTAAgTgIgEgsZgKCQglgJgTgdQgZgnANg0QAJgmAhgvQAthBBEhEQAwgxBShFQA3gvAlgWQAfgTAYgFQAXgFAXAFQAYAGARAOQATAPAHAXQAHAYgHAVQgFAQgNAPQgJAJgRAQIhnBYQg8A1gnAoQg+A/giA5IgdAwQgSAbgUANQgVANgYAAQgLAAgMgDg");
    var mask_graphics_43 = new cjs.Graphics().p("EAtrAUXQgugNgoguQgggkgag3QgQgigYhDQiNmThxm9QgNg0AEgbQAFgkAegYQAegZAkABQAeAAAcASQAZAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAyB5IATAyQAJAcADAXQADAdgIAZQgIAbgSARQgTASgcAFQgKACgLAAQgQAAgQgEgACXRBQgggcgJgpQgDgPgBgUIgCgkQgBgdgGg6IgGg2QgHg6gVhqQgYhxgHgzQgEgggEgpQgDgoAFgWQAFgcATgWQAUgWAagEQAZgEAZAOQAXANAQAXQANAUAJAcQAFAQAHAjIAyDsQASBVADAsIAFAvIAFAYQAEAOABAJQACAOgBAaQAAAbABANIAFAZIAFAZQAEAsgaAhQgNAQgTAKQgTAKgUABIgFAAQgkAAgggcgAjMRPQgbgCgYgOQgkgUgMgjQgFgOAAgWIgBgmQgBgPgEgbIgFgrQgDgaAEhIQADhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAHgSQALgcAdgOQAdgOAdAJQAiAKAXAnQAOAYAMAxQALApAEAaIADAnIAEAnQACARAJAjQAJAiACASQAFAggBA9QgCBAADAdIAGAlIAGAlQACAXABAuIADAfQABATgCANQgDAagTAWQgRAVgaAKQgSAHgVAAIgNgBgAXpRHQgngSgTgvQgIgTgFgcIgJgxQgIgrgdhPIgohyQgXhIgliNIgWhXQgShDAagaQAMgNATgCQATgCARAHQAcALAaAlQAdApAZBAQAOAlAZBMQAMAiAnBgQAgBRAQAyQALAlASBMQAMA2gCAdQgEAxggAXQgSANgYABIgEAAQgUAAgTgJgAQ+Q7QgVgFgRgNQgXgSgNgjQgIgXgHgrIgLhEIgMg+QgEgXgzi5Qgjh9AAhUQAAgmAKgTQAIgRAQgLQAQgLATgCQATgBARAIQASAIALAPQAKAOAJAgIBSESIAaBbQAQA/AaB3QAHAjABARQACAegJAVQgHAUgQAOQgRAPgUAGQgLADgMAAQgJAAgKgCgAJ5QsQgmgMgQgiQgIgRgDgkQgIhQgLg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gHg8QgLhSAhgiQAMgMARgGQARgFAQADQAkAGAYAmQAOAWAOAxIBJD3IAUBNQAIAhAHAmIAVCFQAEAXAAANQAAAUgGAOQgMAgglAPQgWAJgVAAQgPAAgOgEgAxrQpQgXgHgTgSQgSgRgJgXQgMgdAFgpQADgUAMg0QAXhaAPh7QANhsAHiJQAEhMAFiqQABgnAJgRQAKgTAVgLQAUgLAWADQAWACASANQATANALATQALATAFAdQADAQACAjIAMDtQAGBjgDA4QgBATgHBBIgYDMQgHA6gMAiQgRAxgiAYQgUANgZAEIgRACQgQAAgPgFgAd3QdQgYgLgOgVQgLgPgMggIh5lHQgehPgOgsQgVhFgKg5QgFghAEgSQAGgcAYgUQAXgTAdgBQAcgCAaARQAaAPANAaQAFAMAFASIAIAeQALAsAfBAQArBaAHARQAMAfAWBFQAUBCAOAiIAWAwQAMAdAGAUQAQA8gWArQgLAWgWAPQgWAOgZABIgIABQgVAAgUgKgArTP7QgfgXgMgiQgHgVgBgeIADg1QADg6gGhtQgGhyACg1IACgxQABgdgCgUIgFgvQgDgcACgSQABgNAEgSIAIgfIANgvQAJgaANgQQANgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaAAA2QACAgAIA/QACAbAAA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgCAWgFARQgMAigfAWQgfAXgkAAIgCAAQgjAAgfgVgEAlbAPgQgagKgagbQgigkgjhCQh7jlg2j9QgNg/AIgkQAFgZARgTQASgUAXgFQAWgEAWALQAVAJAPATQANAPAKAXQAHAOAKAdIA0CSQAbBMAOAjQAYA9AXAvIAyBgQAbA5AHAtQAFAjgIAbQgLAigcANQgOAIgQAAQgPAAgRgHgA68MxQgZgEgVgOQgVgOgMgWQgQgZgBgiQgBgfALgeQALgdAmg2QCRjMBPidIAnhOQAYgtAUgeQAXgkAXgKQAUgKAYADQAXAEASANQASAOAKAWQAJAVAAAXQABAYgNAsQglB5g/B2QgcAzhJBxQhDBpgfA9IgQAfQgKARgLALQgRASgYAIQgRAGgRAAIgPgBgA+aEKQgggBgbgVQgagVgKgfQgLgeAHghQAHggAVgZQAQgTAsgeIBJgvQBAgrAagaIAngrQAYgaASgNQAbgVAtgNQAXgGAPABQATACAQAMQAPALAKASQAMAXgCAVQgBARgMATQgIAMgQASQg9BDgVAgQgNAWgHAKQgTAbgvAoIhLBAQglAggZAOQgjAUggAAIgEgBgEgilgBsQgigJgVgZQgVgZgCgjQgCgjARgcQAQgaAugeQAkgZBTgyICbhfQAdgRARgIQAZgNAXgEQAbgEAZAHQAbAIAPAUQAQAWgCAeQgCAdgQAZQgOAVgYAUQgPAMgfAVIi4B8QgfATgTAQIgkAdQgVAQgSAHQgRAHgTAAQgOAAgOgEgEgllgGjQgYgLgOgTQgPgTgEgZQgEgZAIgXQALgfAigbQAQgNAwgdQBMgtA/g4IAogjQAXgUAUgMQA2ggAvAKQAbAGATAUQAUAUABAaQABAVgMAXQgJAQgTAWQg2A+hLA8Qg7AwhVA5QgbASgOAHQgYAMgWACIgJAAQgUAAgSgIgEgqMgIaQgmgJgTgeQgYgmANg1QAJglAhgvQAshBBEhEQAxgxBRhFQA4gwAkgWQAggSAXgFQAYgFAXAFQAXAFASAPQASAPAHAXQAIAXgIAWQgFAPgNAPQgIAKgRAPIhnBZQg8A0gnAoQg/BAgiA5IgdAwQgSAbgUANQgUANgYAAQgMAAgLgDgEgusgMhQgWgGgQgQQgQgPgHgWQgGgWACgWQAEgcAXgnQAVgjAhgpIA+hHQAkgoAVgTQArgjAVgTIAegcQARgQAOgJQAogbAkAFQAiAGATAdQASAdgJAhQgGATgVAbQgdAkhhBhQhQBRglA6QgdAtgHAJQgWAcgbAIQgLADgLAAQgLAAgKgDg");
    var mask_graphics_46 = new cjs.Graphics().p("EAwBAVaQgvgNgoguQgggkgZg2QgRgjgXhDQiNmShym9QgNg1AEgbQAGgjAegZQAegZAkABQAdABAcASQAaAQATAaQAQAWANAgIAUA7IBkE4QA8C7AgBdQA1CbAxB6IAUAyQAJAcACAXQADAcgHAZQgIAcgTARQgSARgcAGQgLACgLAAQgPAAgQgFgAEtSFQgggdgJgpQgEgOgBgVIgBgjQgBgegGg6IgGg1QgHg7gWhqQgXhxgHgzQgFgggDgpQgDgnAEgWQAGgdASgVQAUgXAbgEQAZgDAZAOQAXANAPAXQANATAJAcQAFARAIAjIAyDrQASBVADAsIAEAwIAGAXQAEAPABAJQACANgBAbQgBAaACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgbgAg2STQgcgDgXgNQgkgUgMgjQgFgPgBgWIAAglQgBgQgEgbIgGgrQgCgZADhIQAEhJgIgxIgLg7QgFgegBhGIgBiHQAAgnAGgSQALgcAdgPQAegOAcAJQAiALAXAnQAOAYANAwQAKAqADAaIAEAnIADAmQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAmIAFAlQACAXACAtIADAgQABASgCANQgEAbgSAVQgSAVgYAKQgTAHgUAAIgNAAgAZ+SKQgmgSgUgvQgHgSgGgdIgIgwQgIgrgdhQIgohyQgYhIgkiMIgXhXQgShDAagbQANgMATgCQASgDASAHQAbAMAaAkQAeApAYBBQAOAlAZBMQAMAhAnBhQAhBRAPAyQAMAlASBMQAMA2gDAcQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgATUR+QgWgEgQgNQgXgTgNgjQgIgWgIgrIgLhEIgLg/QgFgWgzi5Qgih9AAhVQAAglAJgUQAIgQARgLQAQgLASgCQATgCASAIQARAIALAPQALAPAJAfIBRESIAaBcQARA+AZB4QAHAiACASQABAdgIAWQgIATgQAPQgQAPgUAFQgMADgLAAQgKAAgJgCgAMPRvQgngLgPgjQgIgQgDglQgJhPgKg/QgMhHgOgzIgXhKQgQgxgHgaQgPg5gIg7QgLhSAhgjQAMgMARgFQARgGARADQAkAHAXAlQAPAXAOAwIBJD4IATBNQAJAgAHAnIAVCEQADAYAAAMQABAUgGAPQgMAfgmAQQgWAIgVAAQgOAAgOgEgAvWRsQgXgHgSgRQgTgRgJgXQgLgeAFgpQACgUANgzQAWhaAQh7QANhsAHiJQAEhMAFiqQABgoAJgRQAJgTAVgLQAVgKAWACQAVACATAOQASAMALATQAMAUAFAcQADARACAjIAMDsQAGBkgEA4QgBASgHBCIgYDLQgGA7gMAiQgRAxgjAXQgUAOgZAEIgQABQgRAAgPgFgEAgNARgQgYgLgPgUQgLgQgLgfIh6lIQgdhOgOgtQgWhEgJg6QgFghAEgSQAFgcAYgTQAYgUAdgBQAcgBAaAQQAZAQANAaQAGAMAFARIAHAeQAMAsAfBBQArBaAGAQQANAgAVBFQAVBCAOAiIAVAwQAMAcAGAVQARA7gWArQgMAXgWAOQgWAOgZACIgHAAQgVAAgUgKgAo9Q+QgggWgLgiQgIgWAAgeIACg1QAEg5gGhuQgHhyACg1IACgxQABgdgBgUIgFguQgEgcACgTQACgNAEgSIAIgeIAMgwQAJgaAOgQQAMgOASgJQASgIATAAQApABAbAfQAWAZAFAuQADAaABA1QACAgAIBAQACAagBA2QgBA1ACAbIAJBOQAJBHABCRIgBAoQgBAWgGAQQgLAiggAXQgfAWgkABIgBAAQgkAAgegWgEAnxAQkQgbgLgZgaQgiglgjhBQh8jlg1j9QgOhAAIgjQAGgZAQgTQASgVAYgEQAVgEAXAKQAUAKAPASQANAQALAXQAGAOAKAcIA0CTQAbBLAOAkQAYA9AYAuIAyBhQAbA4AGAtQAFAkgIAaQgLAjgbANQgPAHgQAAQgPAAgQgGgA4nN0QgYgEgVgOQgVgOgNgVQgPgagBghQgCgfAMgfQALgcAmg3QCRjMBPicIAnhOQAXgtAUgfQAXgjAXgMQAVgKAYAEQAXADASAPQARAOAKAWQAJAUABAYQAAAXgNAsQgkB6hAB1QgcA0hIBxQhDBpgfA9IgQAfQgKARgLALQgRARgZAJQgRAFgRAAIgPgBgA8FFOQgfgCgbgVQgbgVgKgeQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagZIAngrQAXgbASgNQAcgUAsgNQAYgHAPABQASACAQAMQAPAMAKARQAMAXgBAWQgCARgMASQgHAMgRASQg8BDgVAhQgNAWgIAKQgTAbgvAoIhKA/QgmAggYAOQgjAUggAAIgFAAgEggQgAoQghgJgVgZQgVgagDgjQgCgjARgcQARgaAtgeQAlgYBTgzICbheQAcgSARgIQAagMAXgEQAbgFAZAHQAbAIAOAUQAQAWgCAfQgCAcgQAZQgNAVgZAUQgOAMggAWIi4B7QgeAUgUAPIgjAeQgVAQgSAHQgSAGgSAAQgOAAgPgDgEgjQgFgQgXgKgPgTQgPgUgEgZQgEgZAJgXQALgfAigbQAPgMAxgeQBMgtA/g4IAogjQAXgTATgMQA3ggAvAKQAaAFAUAUQAUAVABAZQABAWgNAXQgJAQgTAWQg2A9hLA9Qg6AvhWA6QgbASgOAHQgYAMgVABIgKABQgTAAgTgJgEgn3gHXQglgJgTgdQgZgmANg1QAJglAhgwQAthBBEhEQAwgwBShFQA3gwAlgWQAfgSAYgGQAXgFAXAFQAYAGARAOQATAPAHAXQAHAYgHAWQgFAPgNAPQgJAKgRAPIhnBYQg8A1gnAoQg+A/giA5IgdAxQgSAbgUAMQgVANgYAAQgLAAgMgDgEgsXgLdQgWgGgQgQQgPgQgHgWQgHgVADgXQADgcAXgmQAWgkAhgoIA+hHQAkgoAUgTQAsgkAVgTIAdgbQASgRANgJQApgbAjAGQAjAFASAdQATAdgKAhQgFATgWAcQgcAjhhBiQhRBRgkA5QgdAugHAIQgXAcgaAIQgLADgMAAQgKAAgLgCgEgxlgN7QgbgUgDggQgBgSAKgdQAQgwAXgoQAOgZAagjQBBhbAigoQAignAdgXQAdgYAfgLQAkgMAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUASQgaAZggArIg0BIIgbAjQgRAVgJAPQgMATgXApQgVAhgYAQQgYAPgbABIgFAAQgeAAgXgRg");
    var mask_graphics_48 = new cjs.Graphics().p("EAyMAWeQgvgNgoguQgggkgZg2QgRgjgXhDQiNmShym9QgNg1AEgbQAGgjAegZQAegZAkABQAdABAcASQAaAQATAaQAQAWANAgIAUA7IBkE4QA8C7AgBdQA1CbAxB6IAUAyQAJAcACAXQADAcgHAZQgIAcgTARQgSARgcAGQgLACgKAAQgQAAgQgFgAG4TJQgggdgJgpQgEgOgBgVIgBgjQgBgegGg6IgGg1QgHg7gWhqQgXhxgHgzQgFgggDgpQgDgnAEgWQAGgdASgVQAUgXAbgEQAZgDAZAOQAXANAPAXQANATAJAcQAFARAIAjIAyDrQASBVADAsIAEAwIAGAXQAEAPABAJQACANgBAbQgBAaACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgbgABUTXQgcgDgXgNQgjgUgMgjQgFgPgBgWIAAglQgBgQgEgbIgGgrQgCgZADhIQAEhJgIgxIgLg7QgFgegBhGIgBiHQAAgnAGgSQALgcAdgPQAdgOAcAJQAiALAXAnQAOAYANAwQALAqADAaIAEAnIADAmQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAmIAFAlQACAXACAtIADAgQABASgCANQgEAbgSAVQgSAVgZAKQgTAHgUAAIgNAAgAcJTOQgmgSgUgvQgHgSgGgdIgIgwQgIgrgdhQIgohyQgYhIgkiMIgXhXQgShDAagbQANgMATgCQASgDASAHQAbAMAaAkQAeApAYBBQAOAlAZBMQAMAhAnBhQAhBRAPAyQAMAlASBMQAMA2gDAcQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVfTCQgWgEgQgNQgXgTgNgjQgIgWgIgrIgLhEIgLg/QgFgWgzi5Qgih9AAhVQAAglAJgUQAIgQARgLQAQgLASgCQATgCASAIQARAIALAPQALAPAJAfIBRESIAaBcQARA+AZB4QAHAiACASQABAdgIAWQgIATgQAPQgQAPgUAFQgLADgMAAQgJAAgKgCgAOaSzQgngLgPgjQgIgQgDglQgJhPgKg/QgMhHgOgzIgXhKQgQgxgHgaQgPg5gIg7QgLhSAhgjQAMgMARgFQARgGARADQAkAHAXAlQAPAXAOAwIBJD4IATBNQAJAgAHAnIAVCEQADAYAAAMQABAUgGAPQgMAfgmAQQgWAJgVAAQgOAAgOgFgAtLSwQgXgHgSgRQgTgRgJgXQgLgeAFgpQACgUANgzQAWhaAQh7QANhsAHiJQAEhMAFiqQABgoAJgRQAJgTAVgLQAVgKAWACQAVACATAOQASAMALATQAMAUAFAcQADARACAjIAMDsQAGBkgEA4QgBASgHBCIgYDLQgGA7gMAiQgRAxgjAXQgUAOgZAEIgQABQgQAAgQgFgEAiYASkQgYgLgPgUQgLgQgLgfIh6lIQgdhOgOgtQgWhEgJg6QgFghAEgSQAFgcAYgTQAYgUAdgBQAcgBAaAQQAZAQANAaQAGAMAFARIAHAeQAMAsAfBBQArBaAGAQQANAgAVBFQAVBCAOAiIAVAwQAMAcAGAVQARA7gWArQgMAXgWAOQgWAOgZACIgHAAQgVAAgUgKgAmySCQgggWgLgiQgIgWAAgeIACg1QAEg5gGhuQgHhyACg1IACgxQABgdgBgUIgFguQgEgcACgTQACgNAEgSIAIgeIAMgwQAJgaAOgQQAMgOASgJQASgIATAAQApABAbAfQAWAZAFAuQADAaABA1QACAgAIBAQACAagBA2QgBA1ACAbIAJBOQAJBHABCRIgBAoQgBAWgGAQQgLAiggAXQgfAWgkABIgBAAQgjAAgfgWgEAp8ARoQgbgLgZgaQgiglgjhBQh8jlg1j9QgOhAAIgjQAGgZAQgTQASgVAYgEQAVgEAXAKQAUAKAPASQANAQALAXQAGAOAKAcIA0CTQAbBLAOAkQAYA9AYAuIAyBhQAbA4AGAtQAFAkgIAaQgLAjgbANQgOAHgRAAQgPAAgQgGgA2cO4QgYgEgVgOQgVgOgNgVQgPgagBghQgCgfAMgfQALgcAmg3QCRjMBPicIAnhOQAXgtAUgfQAXgjAXgMQAVgKAYAEQAXADASAPQARAOAKAWQAJAUABAYQAAAXgNAsQgkB6hAB1QgcA0hIBxQhDBpgfA9IgQAfQgKARgLALQgRARgZAJQgRAFgRAAIgPgBgA56GSQgfgCgbgVQgbgVgKgeQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngqQAXgbASgNQAcgUAsgNQAYgHAPABQASACAQAMQAPAMAKARQAMAXgBAVQgCARgMASQgHAMgRATQg8BDgVAhQgNAWgIAKQgTAbgvAoIhKA/QgmAggYAOQgjAUggAAIgFAAgA+FAbQghgJgVgYQgVgagDgjQgCgjARgcQARgaAtgeQAlgYBTgzICbheQAcgSARgIQAagMAXgEQAbgFAZAHQAbAIAOAUQAQAWgCAfQgCAcgQAZQgNAVgZAUQgOAMggAWIi4B7QgeAUgUAPIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEghFgEcQgXgKgPgTQgPgUgEgZQgEgZAJgXQALgfAigbQAPgMAxgeQBMgtA/g4IAogjQAXgTATgMQA3ggAvAKQAaAFAUAUQAUAVABAZQABAWgNAXQgJAQgTAWQg2A9hLA9Qg6AvhWA6QgbASgOAHQgYAMgVABIgKABQgTAAgTgJgEglsgGTQglgJgTgdQgZgmANg1QAJglAhgwQAthBBEhEQAwgwBShFQA3gwAlgWQAfgSAYgGQAXgFAXAFQAYAGARAOQATAPAHAXQAHAYgHAWQgFAPgNAPQgJAKgRAPIhnBYQg8A1gnAoQg+A/giA5IgdAxQgSAbgUAMQgVANgYAAQgLAAgMgDgEgqMgKZQgWgGgQgQQgPgQgHgWQgHgVADgXQADgcAXgmQAWgkAhgoIA+hHQAkgoAUgTQAsgkAVgTIAdgbQASgRANgJQApgbAjAGQAjAFASAdQATAdgKAhQgFATgWAcQgcAjhhBiQhRBRgkA5QgdAugHAIQgXAcgaAIQgLAEgLAAQgLAAgLgDgEgvagM3QgbgUgDggQgBgSAKgdQAQgwAXgoQAOgZAagjQBBhbAigoQAignAdgXQAdgYAfgLQAkgMAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUASQgaAZggArIg0BIIgbAjQgRAVgJAPQgMATgXApQgVAhgYAQQgYAPgbABIgFAAQgeAAgXgRgEgzcgRjQgXgHgOgXQgOgVABgZQAAguApgoQAXgXA5ghIBzhDQAugbAcgFQAVgEATAGQAVAGAMAOQAVAagJAmQgHAbgcAeQgTAVgdAVQgRAMglAXIhrBEQgeATgTAHQgRAGgPAAQgKAAgKgDg");
    var mask_graphics_51 = new cjs.Graphics().p("EAybAYqQgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHVUQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjViQgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYVaQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVuVOQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpU/QgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8U8QgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQABQgQAAgQgEgEAinAUwQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjUOQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLATzQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NREQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rIdQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92CmQghgJgVgZQgVgZgDgjQgCgjARgcQARgZAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B7QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2gCQQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgUATgMQA3ggAvAKQAaAGAUAUQAUAUABAaQABAVgNAXQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldgEHQglgJgTgeQgZgmANg1QAJglAhgvQAthBBEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8A0gnAoQg+BAgiA5IgdAwQgSAbgUANQgVANgYAAQgLAAgMgDgEgp9gIOQgWgGgQgQQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAIQgLADgLAAQgLAAgLgDgEgvLgKsQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgPYQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gU9QgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMg");
    var mask_graphics_54 = new cjs.Graphics().p("EAybAa7QgvgNgoguQgggkgZg2QgRgjgXhDQiNmShym9QgNg1AEgbQAGgjAegZQAegZAkABQAdABAcASQAaAQATAaQAQAWANAgIAUA7IBkE4QA8C7AgBdQA1CbAxB6IAUAyQAJAcACAXQADAcgHAZQgIAcgTARQgSARgcAGQgLACgKAAQgQAAgQgFgAHHXmQgggdgJgpQgEgOgBgVIgBgjQgBgegGg6IgGg1QgHg7gWhqQgXhxgHgzQgFgggDgpQgDgnAEgWQAGgdASgVQAUgXAbgEQAZgDAZAOQAXANAPAXQANATAJAcQAFARAIAjIAyDrQASBVADAsIAEAwIAGAXQAEAPABAJQACANgBAbQgBAaACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgbgABjX0QgcgDgXgNQgkgUgMgjQgEgPgBgWIAAglQgBgQgEgbIgGgrQgCgZADhIQAEhJgIgxIgLg7QgFgegBhGIgBiHQAAgnAGgSQALgcAcgPQAegOAcAJQAiALAXAnQAOAYANAwQALAqADAaIAEAnIADAmQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAmIAFAlQACAXACAtIADAgQABASgCANQgEAbgSAVQgSAVgZAKQgTAIgUAAIgNgBgAcYXrQgmgSgUgvQgHgSgGgdIgIgwQgIgrgdhQIgohyQgYhIgkiMIgXhXQgShDAagbQANgMATgCQASgDASAHQAbAMAaAkQAeApAYBBQAOAlAZBMQAMAhAnBhQAhBRAPAyQAMAlASBMQAMA2gDAcQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVuXfQgWgEgQgNQgXgTgNgjQgIgWgIgrIgLhEIgLg/QgFgWgzi5Qgih9AAhVQAAglAJgUQAIgQARgLQAQgLASgCQATgCASAIQARAIALAPQALAPAJAfIBRESIAaBcQARA+AZB4QAHAiACASQABAdgIAWQgIATgQAPQgQAPgUAFQgLADgMAAQgJAAgKgCgAOpXQQgngLgPgjQgIgQgDglQgJhPgKg/QgMhHgOgzIgXhKQgQgxgHgaQgPg5gIg7QgLhSAhgjQAMgMARgFQARgGARADQAkAHAXAlQAPAXAOAwIBJD4IATBNQAJAgAHAnIAVCEQADAYAAAMQABAUgGAPQgMAfgmAQQgWAJgVAAQgOAAgOgFgAs8XNQgXgHgSgRQgTgRgJgXQgLgeAFgpQACgUANgzQAWhaAQh7QANhsAHiJQAEhMAFiqQABgoAJgRQAJgTAVgLQAVgKAWACQAVACATAOQASAMALATQAMAUAFAcQADARACAjIAMDsQAGBkgEA4QgBASgHBCIgYDLQgGA7gMAiQgRAxgjAXQgUAOgZAEIgQABQgQAAgQgFgEAinAXBQgYgLgPgUQgLgQgLgfIh6lIQgdhOgOgtQgWhEgJg6QgFghAEgSQAFgcAYgTQAYgUAdgBQAcgBAaAQQAZAQANAaQAGAMAFARIAHAeQAMAsAfBBQArBaAGAQQANAgAVBFQAVBCAOAiIAVAwQAMAcAGAVQARA7gWArQgMAXgWAOQgWAOgZACIgHAAQgVAAgUgKgAmjWfQgggWgLgiQgIgWAAgeIACg1QAEg5gGhuQgHhyACg1IACgxQABgdgBgUIgFguQgEgcACgTQACgNAEgSIAIgeIAMgwQAJgaAOgQQAMgOASgJQASgIATAAQApABAbAfQAWAZAFAuQADAaABA1QACAgAIBAQACAagBA2QgBA1ACAbIAJBOQAJBHABCRIgBAoQgBAWgGAQQgLAiggAXQgfAWgkABIgBAAQgjAAgfgWgEAqLAWFQgbgLgZgaQgiglgjhBQh8jlg1j9QgOhAAIgjQAGgZAQgTQASgVAYgEQAVgEAXAKQAUAKAPASQANAQALAXQAGAOAKAcIA0CTQAbBLAOAkQAYA9AYAuIAyBhQAbA4AGAtQAFAkgIAaQgLAjgbANQgOAHgRAAQgPAAgQgGgA2NTVQgYgEgVgOQgVgOgNgVQgPgagBghQgCgfAMgfQALgcAmg3QCRjMBPicIAnhOQAXgtAUgfQAXgjAXgMQAVgKAYAEQAXADASAPQARAOAKAWQAJAUABAYQAAAXgNAsQgkB6hAB1QgcA0hIBxQhDBpgfA9IgQAfQgKARgLALQgRARgZAJQgRAFgRAAIgPgBgA5rKvQgfgCgbgVQgbgVgKgeQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgbASgNQAcgUAsgNQAYgHAPABQASACAQAMQAPAMAKARQAMAXgBAWQgCARgMASQgHAMgRATQg8BDgVAhQgNAWgIAKQgTAbgvAoIhKA/QgmAggYAOQgjAUggAAIgFAAgA92E4QghgJgVgZQgVgagDgjQgCgjARgcQARgaAtgeQAlgYBTgzICbhdQAcgSARgIQAagMAXgEQAbgFAZAHQAbAIAOAUQAQAWgCAfQgCAcgQAYQgNAVgZAUQgOAMggAWIi4B7QgeAUgUAPIgjAeQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AAAQgXgJgPgTQgPgUgEgZQgEgZAJgXQALgfAigbQAPgMAxgeQBMgtA/g4IAogjQAXgTATgMQA3ggAvAKQAaAFAUAUQAUAVABAZQABAWgNAXQgJAQgTAWQg2A9hLA9Qg6AvhWA6QgbASgOAHQgYALgVABIgKABQgTAAgTgJgEgldgB2QglgJgTgdQgZgmANg1QAJglAhgwQAthBBEhEQAwgwBShFQA3gwAlgWQAfgSAYgGQAXgFAXAFQAYAGARAOQATAPAHAXQAHAYgHAWQgFAPgNAPQgJAKgRAPIhnBYQg8A1gnAoQg+A/giA5IgdAxQgSAbgUAMQgVANgYAAQgLAAgMgDgEgp9gF8QgWgGgQgQQgPgQgHgWQgHgVADgXQADgcAXgmQAWgkAhgoIA+hHQAkgoAUgTQAsgkAVgTIAdgbQASgRANgJQApgbAjAGQAjAFASAdQATAdgKAhQgFATgWAcQgcAjhhBiQhRBRgkA5QgdAugHAIQgXAcgaAIQgLAEgLAAQgLAAgLgDgEgvLgIaQgbgUgDggQgBgSAKgdQAQgwAXgoQAOgZAagjQBBhbAigoQAignAdgXQAdgYAfgLQAkgMAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUASQgaAZggArIg0BIIgbAjQgRAVgJAPQgMATgXApQgVAhgYAQQgYAPgbABIgFAAQgeAAgXgRgEgzNgNGQgXgHgOgXQgOgVABgZQAAguApgoQAXgXA5ghIBzhDQAugbAcgFQAVgEATAGQAVAGAMAOQAVAagJAmQgHAbgcAeQgTAVgdAVQgRAMglAXIhrBEQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gSsQgdgRgHgeQgKgpAjgwQAog4BFgbQBCgbBHAIQAwAFAeAUQASAMALASQAMASAAAUQACAjgdAcQgbAagnAHQgMADgrADQgjADgTAHQgNAFghATQgcAQgSAEQgIABgIAAQgWAAgWgMgEgufgXfIgXgOQgNgJgJgEQgWgJggABIgjADQgWACgNAAQgqgBgWgVQgTgSgBgdQgBgbAOgYQAZgnA9gVQBJgYBLASQBOASAsA6QASAWAHAYQAIAbgHAYQgGAUgRAPQgQAPgVAGQgNADgNAAQgcAAgcgQg");
    var mask_graphics_57 = new cjs.Graphics().p("EAybAdQQgvgNgoguQgggkgZg2QgRgjgXhDQiNmShym9QgNg1AEgbQAGgjAegZQAegZAkABQAdABAcASQAaAQATAaQAQAWANAgIAUA7IBkE4QA8C7AgBdQA1CbAxB6IAUAyQAJAcACAXQADAcgHAZQgIAcgTARQgSARgcAGQgLACgKAAQgQAAgQgFgAHHZ7QgggdgJgpQgEgOgBgVIgBgjQgBgegGg6IgGg1QgHg7gWhqQgXhxgHgzQgFgggDgpQgDgnAEgWQAGgdASgVQAUgXAbgEQAZgDAZAOQAXANAPAXQANATAJAcQAFARAIAjIAyDrQASBVADAsIAEAwIAGAXQAEAPABAJQACANgBAbQgBAaACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFABQglAAgfgcgABjaJQgcgDgXgNQgkgUgMgjQgEgPgBgWIAAglQgBgQgEgbIgGgrQgCgZADhIQAEhJgIgxIgLg7QgFgegBhGIgBiHQAAgnAGgSQALgcAcgPQAegOAcAJQAiALAXAnQAOAYANAwQALAqADAaIAEAnIADAmQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAmIAFAlQACAXACAtIADAgQABASgCANQgEAbgSAVQgSAVgZAKQgTAIgUAAIgNgBgAcYaAQgmgSgUgvQgHgSgGgdIgIgwQgIgrgdhQIgohyQgYhIgkiMIgXhXQgShDAagbQANgMATgCQASgDASAHQAbAMAaAkQAeApAYBBQAOAlAZBMQAMAhAnBhQAhBRAPAyQAMAlASBMQAMA2gDAcQgDAxghAXQgSANgXABIgEABQgVAAgTgKgAVuZ0QgWgEgQgNQgXgTgNgjQgIgWgIgrIgLhEIgLg/QgFgWgzi5Qgih9AAhVQAAglAJgUQAIgQARgLQAQgLASgCQATgCASAIQARAIALAPQALAPAJAfIBRESIAaBcQARA+AZB4QAHAiACASQABAdgIAWQgIATgQAPQgQAPgUAFQgLADgMAAQgJAAgKgCgAOpZlQgngLgPgjQgIgQgDglQgJhPgKg/QgMhHgOgzIgXhKQgQgxgHgaQgPg5gIg7QgLhSAhgjQAMgMARgFQARgGARADQAkAHAXAlQAPAXAOAwIBJD4IATBNQAJAgAHAnIAVCEQADAYAAAMQABAUgGAPQgMAfgmAQQgWAJgVAAQgOAAgOgFgAs8ZiQgXgHgSgRQgTgRgJgXQgLgeAFgpQACgUANgzQAWhaAQh7QANhsAHiJQAEhMAFiqQABgoAJgRQAJgTAVgLQAVgKAWACQAVACATAOQASAMALATQAMAUAFAcQADARACAjIAMDsQAGBkgEA4QgBASgHBCIgYDLQgGA7gMAiQgRAxgjAXQgUAOgZAEIgQABQgQAAgQgFgEAinAZWQgYgLgPgUQgLgQgLgfIh6lIQgdhOgOgtQgWhEgJg6QgFghAEgSQAFgcAYgTQAYgUAdgBQAcgBAaAQQAZAQANAaQAGAMAFARIAHAeQAMAsAfBBQArBaAGAQQANAgAVBFQAVBCAOAiIAVAwQAMAcAGAVQARA7gWArQgMAXgWAOQgWAOgZACIgHAAQgVAAgUgKgAmjY0QgggWgLgiQgIgWAAgeIACg1QAEg5gGhuQgHhyACg1IACgxQABgdgBgUIgFguQgEgcACgTQACgNAEgSIAIgeIAMgwQAJgaAOgQQAMgOASgJQASgIATAAQApABAbAfQAWAZAFAuQADAaABA1QACAgAIBAQACAagBA2QgBA1ACAbIAJBOQAJBHABCRIgBAoQgBAWgGAQQgLAiggAXQgfAWgkABIgBAAQgjAAgfgWgEAqLAYaQgbgLgZgaQgiglgjhBQh8jlg1j9QgOhAAIgjQAGgZAQgTQASgVAYgEQAVgEAXAKQAUAKAPASQANAQALAXQAGAOAKAcIA0CTQAbBLAOAkQAYA9AYAuIAyBhQAbA4AGAtQAFAkgIAaQgLAjgbANQgOAHgRAAQgPAAgQgGgA2NVqQgYgEgVgOQgVgOgNgVQgPgagBghQgCgfAMgfQALgcAmg3QCRjMBPicIAnhOQAXgtAUgfQAXgjAXgMQAVgKAYAEQAXADASAPQARAOAKAWQAJAUABAYQAAAXgNAsQgkB6hAB1QgcA0hIBxQhDBpgfA9IgQAfQgKARgLALQgRARgZAJQgRAGgRAAIgPgCgA5rNEQgfgCgbgVQgbgVgKgeQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgbASgNQAcgUAsgNQAYgHAPABQASACAQAMQAPAMAKARQAMAXgBAWQgCARgMASQgHAMgRATQg8BDgVAhQgNAWgIAKQgTAbgvAoIhKA/QgmAggYAOQgjAUggAAIgFAAgA92HNQghgJgVgZQgVgagDgjQgCgjARgcQARgaAtgeQAlgYBTgzICbheQAcgSARgIQAagMAXgEQAbgFAZAHQAbAIAOAUQAQAWgCAfQgCAcgQAZQgNAVgZAUQgOAMggAWIi4B7QgeAUgUAPIgjAeQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2ACVQgXgKgPgTQgPgUgEgZQgEgZAJgXQALgeAigbQAPgMAxgeQBMgtA/g4IAogjQAXgTATgMQA3ggAvAKQAaAFAUAUQAUAVABAZQABAWgNAXQgJAQgTAWQg2A9hLA8Qg6AvhWA6QgbASgOAHQgYAMgVABIgKABQgTAAgTgJgEgldAAeQglgJgTgcQgZgmANg1QAJglAhgwQAthBBEhEQAwgwBShFQA3gwAlgWQAfgSAYgGQAXgFAXAFQAYAGARAOQATAPAHAXQAHAYgHAWQgFAPgNAPQgJAKgRAPIhnBYQg8A1gnAoQg+A/giA5IgdAxQgSAagUAMQgVANgYAAQgLAAgMgDgEgp9gDnQgWgGgQgQQgPgQgHgWQgHgVADgXQADgcAXgmQAWgkAhgoIA+hHQAkgoAUgTQAsgkAVgTIAdgbQASgRANgJQApgbAjAGQAjAFASAdQATAdgKAhQgFATgWAcQgcAjhhBiQhRBRgkA5QgdAugHAIQgXAcgaAIQgLAEgLAAQgLAAgLgDgEgvLgGFQgbgUgDggQgBgSAKgdQAQgwAXgoQAOgZAagjQBBhbAigoQAignAdgXQAdgYAfgLQAkgMAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUASQgaAZggArIg0BIIgbAjQgRAVgJAPQgMATgXApQgVAhgYAQQgYAPgbABIgFAAQgeAAgXgRgEgzNgKxQgXgHgOgXQgOgVABgZQAAguApgoQAXgXA5ghIBzhDQAugbAcgFQAVgEATAGQAVAGAMAOQAVAagJAmQgHAbgcAeQgTAVgdAVQgRAMglAXIhrBEQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gQXQgdgRgHgeQgKgpAjgwQAog4BFgbQBCgbBHAIQAwAFAeAUQASAMALASQAMASAAAUQACAjgdAcQgbAagnAHQgMADgrADQgjADgTAHQgNAFghATQgcAQgSAEQgIACgIAAQgWAAgWgNgEgufgVKIgXgOQgNgJgJgEQgWgJggABIgjADQgWACgNAAQgqgBgWgVQgTgSgBgdQgBgbAOgYQAZgnA9gVQBJgYBLASQBOASAsA6QASAWAHAYQAIAbgHAYQgGAUgRAPQgQAPgVAGQgNADgNAAQgcAAgcgQgEgqCgYyQgagJghgZIg2gqQgPgJgggRQgbgQgPgQQgeggAAgoQABgVAJgSQAJgTARgLQAfgVA4AIQBEAKA6AnQA7AmAjA6QAbAtgFAjQgDAUgOARQgOARgUAIQgTAJgVAAQgUAAgWgIg");
    var mask_graphics_60 = new cjs.Graphics().p("EAybAe4QgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHbiQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjbwQgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYboQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVubcQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpbNQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8bKQgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQABQgQAAgQgEgEAinAa+QgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjacQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAaBQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NXSQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rOrQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92I0QghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AD9QgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgsA/g4IAogjQAXgUATgMQA3ggAvAKQAaAGAUAUQAUAUABAaQABAVgNAXQgJAQgTAWQg2A9hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldACGQglgJgTgeQgZgmANg1QAJgkAhgvQAthBBEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8A0gnAoQg+BAgiA4IgdAwQgSAbgUANQgVAMgYAAQgLAAgMgCgEgp9gCAQgWgGgQgQQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAIQgLADgLAAQgLAAgLgDgEgvLgEeQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgJKQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gOvQgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgEgufgTiIgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNAEgNAAQgcAAgcgQgEgqCgXLQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgEgj3gYsQgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKg");
    var mask_graphics_63 = new cjs.Graphics().p("EAybAfsQgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHcWQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjckQgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYccQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVucQQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpcBQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8b+QgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQABQgQAAgQgEgEAinAbyQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjbQQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAa1QgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NYGQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rPfQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92JoQghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AExQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g3IAogjQAXgUATgMQA3ggAvAKQAaAGAUAUQAUAUABAaQABAVgNAWQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldAC6QglgJgTgeQgZgmANg1QAJglAhguQAthBBEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8A0gnAoQg+A/giA5IgdAwQgSAbgUANQgVAMgYAAQgLAAgMgCgEgp9gBMQgWgGgQgQQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAIQgLADgLAAQgLAAgLgDgEgvLgDqQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgIWQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gN7QgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgEgufgSuIgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNAEgNAAQgcAAgcgQgEgqCgWXQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgEgj3gX4QgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKgA944KQgfgOgbgjQgcglgnhfQglhaghgoIgrgwQgWgdACgbQACggAhgVQAegTAkACQAlACAlAUQAhASAcAfQAoAsAoBaQAoBaALBAQAIAngHAgQgIAmgcAQQgQAJgUAAQgTAAgTgIg");
    var mask_graphics_65 = new cjs.Graphics().p("EAybAgFQgvgNgoguQgggkgZg2QgRgjgXhDQiNmShym9QgNg1AEgbQAGgjAegZQAegZAkABQAdABAcASQAaAQATAaQAQAWANAgIAUA7IBkE4QA8C7AgBdQA1CbAxB6IAUAyQAJAcACAXQADAcgHAZQgIAcgTARQgSARgcAGQgLACgKAAQgQAAgQgFgAHHcwQgggdgJgpQgEgOgBgVIgBgjQgBgegGg6IgGg1QgHg7gWhqQgXhxgHgzQgFgggDgpQgDgnAEgWQAGgdASgVQAUgXAbgEQAZgDAZAOQAXANAPAXQANATAJAcQAFARAIAjIAyDrQASBVADAsIAEAwIAGAXQAEAPABAJQACANgBAbQgBAaACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFABQglAAgfgcgABjc+QgcgDgXgNQgkgUgMgjQgEgPgBgWIAAglQgBgQgEgbIgGgrQgCgZADhIQAEhJgIgxIgLg7QgFgegBhGIgBiHQAAgnAGgSQALgcAcgPQAegOAcAJQAiALAXAnQAOAYANAwQALAqADAaIAEAnIADAmQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAmIAFAlQACAXACAtIADAgQABASgCANQgEAbgSAVQgSAVgZAKQgTAIgUAAIgNgBgAcYc1QgmgSgUgvQgHgSgGgdIgIgwQgIgrgdhQIgohyQgYhIgkiMIgXhXQgShDAagbQANgMATgCQASgDASAHQAbAMAaAkQAeApAYBBQAOAlAZBMQAMAhAnBhQAhBRAPAyQAMAlASBMQAMA2gDAcQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVucpQgWgEgQgNQgXgTgNgjQgIgWgIgrIgLhEIgLg/QgFgWgzi5Qgih9AAhVQAAglAJgUQAIgQARgLQAQgLASgCQATgCASAIQARAIALAPQALAPAJAfIBRESIAaBcQARA+AZB4QAHAiACASQABAdgIAWQgIATgQAPQgQAPgUAFQgLADgMAAQgJAAgKgCgAOpcaQgngLgPgjQgIgQgDglQgJhPgKg/QgMhHgOgzIgXhKQgQgxgHgaQgPg5gIg7QgLhSAhgjQAMgMARgFQARgGARADQAkAHAXAlQAPAXAOAwIBJD4IATBNQAJAgAHAnIAVCEQADAYAAAMQABAUgGAPQgMAfgmAQQgWAJgVAAQgOAAgOgFgAs8cXQgXgHgSgRQgTgRgJgXQgLgeAFgpQACgUANgzQAWhaAQh7QANhsAHiJQAEhMAFiqQABgoAJgRQAJgTAVgLQAVgKAWACQAVACATAOQASAMALATQAMAUAFAcQADARACAjIAMDsQAGBkgEA4QgBASgHBCIgYDLQgGA7gMAiQgRAxgjAXQgUAOgZAEIgQABQgQAAgQgFgEAinAcLQgYgLgPgUQgLgQgLgfIh6lIQgdhOgOgtQgWhEgJg6QgFghAEgSQAFgcAYgTQAYgUAdgBQAcgBAaAQQAZAQANAaQAGAMAFARIAHAeQAMAsAfBBQArBaAGAQQANAgAVBFQAVBCAOAiIAVAwQAMAcAGAVQARA7gWArQgMAXgWAOQgWAOgZACIgHAAQgVAAgUgKgAmjbpQgggWgLgiQgIgWAAgeIACg1QAEg5gGhuQgHhyACg1IACgxQABgdgBgUIgFguQgEgcACgTQACgNAEgSIAIgeIAMgwQAJgaAOgQQAMgOASgJQASgIATAAQApABAbAfQAWAZAFAuQADAaABA1QACAgAIBAQACAagBA2QgBA1ACAbIAJBOQAJBHABCRIgBAoQgBAWgGAQQgLAiggAXQgfAWgkABIgBAAQgjAAgfgWgEAqLAbPQgbgLgZgaQgiglgjhBQh8jlg1j9QgOhAAIgjQAGgZAQgTQASgVAYgEQAVgEAXAKQAUAKAPASQANAQALAXQAGAOAKAcIA0CTQAbBLAOAkQAYA9AYAuIAyBhQAbA4AGAtQAFAkgIAaQgLAjgbANQgOAHgRAAQgPAAgQgGgA2NYfQgYgEgVgOQgVgOgNgVQgPgagBghQgCgfAMgfQALgcAmg3QCRjMBPicIAnhOQAXgtAUgfQAXgjAXgMQAVgKAYAEQAXADASAPQARAOAKAWQAJAUABAYQAAAXgNAsQgkB6hAB1QgcA0hIBxQhDBpgfA9IgQAfQgKARgLALQgRARgZAJQgRAFgRAAIgPgBgA5rP5QgfgCgbgVQgbgVgKgeQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgbASgNQAcgUAsgNQAYgHAPABQASACAQAMQAPAMAKARQAMAXgBAWQgCARgMASQgHAMgRATQg8BDgVAhQgNAWgIAKQgTAbgvAoIhKA/QgmAggYAOQgjAUggAAIgFAAgA92KCQghgJgVgZQgVgagDgjQgCgjARgcQARgaAtgeQAlgYBTgzICbheQAcgSARgIQAagMAXgEQAbgFAZAHQAbAIAOAUQAQAWgCAfQgCAcgQAZQgNAVgZAUQgOAMggAWIi4B7QgeAUgUAPIgjAeQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AFKQgXgKgPgTQgPgUgEgZQgEgZAJgXQALgfAigbQAPgMAxgeQBMgtA/g4IAogiQAXgTATgMQA3ggAvAKQAaAFAUAUQAUAVABAZQABAVgNAXQgJAQgTAWQg2A9hLA9Qg6AvhWA6QgbASgOAHQgYAMgVABIgKABQgTAAgTgJgEgldADTQglgJgTgdQgZgmANg1QAJglAhgvQAthBBEhEQAwgwBShFQA3gwAlgWQAfgSAYgGQAXgFAXAFQAYAGARAOQATAPAHAXQAHAYgHAWQgFAPgNAPQgJAKgRAPIhnBYQg8A1gnAoQg+A+giA5IgdAxQgSAbgUAMQgVANgYAAQgLAAgMgDgEgp9gAyQgWgGgQgQQgPgQgHgWQgHgVADgXQADgcAXgmQAWgkAhgoIA+hHQAkgoAUgTQAsgkAVgTIAdgbQASgRANgJQApgbAjAGQAjAFASAdQATAdgKAhQgFATgWAcQgcAjhhBiQhRBRgkA5QgdAugHAIQgXAcgaAIQgLAEgLAAQgLAAgLgDgEgvLgDQQgbgUgDggQgBgSAKgdQAQgwAXgoQAOgZAagjQBBhbAigoQAignAdgXQAdgYAfgLQAkgMAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUASQgaAZggArIg0BIIgbAjQgRAVgJAPQgMATgXApQgVAhgYAQQgYAPgbABIgFAAQgeAAgXgRgEgzNgH8QgXgHgOgXQgOgVABgZQAAguApgoQAXgXA5ghIBzhDQAugbAcgFQAVgEATAGQAVAGAMAOQAVAagJAmQgHAbgcAeQgTAVgdAVQgRAMglAXIhrBEQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gNiQgdgRgHgeQgKgpAjgwQAog4BFgbQBCgbBHAIQAwAFAeAUQASAMALASQAMASAAAUQACAjgdAcQgbAagnAHQgMADgrADQgjADgTAHQgNAFghATQgcAQgSAEQgIABgIAAQgWAAgWgMgEgufgSVIgXgOQgNgJgJgEQgWgJggABIgjADQgWACgNAAQgqgBgWgVQgTgSgBgdQgBgbAOgYQAZgnA9gVQBJgYBLASQBOASAsA6QASAWAHAYQAIAbgHAYQgGAUgRAPQgQAPgVAGQgNADgNAAQgcAAgcgQgEgqCgV9QgagJghgZIg2gqQgPgJgggRQgbgQgPgQQgeggAAgoQABgVAJgSQAJgTARgLQAfgVA4AIQBEAKA6AnQA7AmAjA6QAbAtgFAjQgDAUgOARQgOARgUAIQgTAJgVAAQgUAAgWgIgEgj3gXfQgagNgYgbQgRgVgthGQgmg6gfgcIgwgsQgYgbADgbQAEgkAxgaQA5gfAsANQAWAGAVATQAPANAUAZQBbBuAaBaQAOAvgHAgQgEAUgMAQQgNARgSAGQgKAEgLAAQgSAAgUgKgA943wQgfgOgbgkQgcglgnhfQglhaghgnIgrgxQgWgdACgbQACggAhgUQAegTAkABQAlACAlAUQAhATAcAeQAoAtAoBZQAoBbALA/QAIAogHAfQgIAmgcAQQgQAKgUAAQgTAAgTgIgA4z5QQgogLgjgqQgjgsgOg2QgCgJgIguQgGgigJgUQgGgNgMgVIgUgiQgVgsAQgfQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAfAPAtQAIAcALA5IAQBQQAGAhAAAUQAAAcgOATQgNATgYAHQgNAEgMAAQgLAAgLgDg");
    var mask_graphics_67 = new cjs.Graphics().p("EAybAgkQgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHdOQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjdcQgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYdUQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVudIQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpc5QgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8c2QgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQABQgQAAgQgEgEAinAcqQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHAAQgVAAgUgJgAmjcIQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAbtQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAHgRAAQgPAAgQgGgA2NY+QgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rQXQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92KgQghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AFpQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgTATgMQA3ggAvAKQAaAGAUAUQAUAUABAZQABAVgNAXQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldADyQglgJgTgeQgZgmANg1QAJglAhgvQAthABEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8A0gnAnQg+BAgiA5IgdAwQgSAbgUANQgVAMgYAAQgLAAgMgCgEgp9gAUQgWgGgQgQQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAIQgLADgLAAQgLAAgLgDgEgvLgCyQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgHeQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gNDQgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgEgufgR2IgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNADgNAAQgcAAgcgPgEgqCgVfQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgEgj3gXAQgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKgA943SQgfgOgbgjQgcglgnhfQglhaghgoIgrgwQgWgdACgbQACggAhgVQAegTAkACQAlACAlAUQAhASAcAfQAoAsAoBaQAoBaALBAQAIAngHAgQgIAmgcAQQgQAJgUAAQgTAAgTgIgA4z4yQgogKgjgrQgjgsgOg1QgCgJgIguQgGgigJgUQgGgOgMgVIgUghQgVgtAQgeQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAeAPAuQAIAbALA5IAQBRQAGAgAAAUQAAAdgOATQgNASgYAHQgNAEgMAAQgLAAgLgDgA0L6sQgUgFgQgPQgZgVgXgvQgVgpgNgkQgQgsgKg0QgLhAAXgeQAPgSAbgFQAZgEAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAgABAZQACAggOAbQgPAegcAIQgJADgKAAQgKAAgMgEg");
    var mask_graphics_70 = new cjs.Graphics().p("EAybAgpQgvgNgoguQgggkgZg2QgRgjgXhDQiNmShym9QgNg1AEgbQAGgjAegZQAegZAkABQAdABAcASQAaAQATAaQAQAWANAgIAUA7IBkE4QA8C7AgBdQA1CbAxB6IAUAyQAJAcACAXQADAcgHAZQgIAcgTARQgSARgcAGQgLACgKAAQgQAAgQgFgAHHdUQgggdgJgpQgEgOgBgVIgBgjQgBgegGg6IgGg1QgHg7gWhqQgXhxgHgzQgFgggDgpQgDgnAEgWQAGgdASgVQAUgXAbgEQAZgDAZAOQAXANAPAXQANATAJAcQAFARAIAjIAyDrQASBVADAsIAEAwIAGAXQAEAPABAJQACANgBAbQgBAaACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgbgABjdiQgcgDgXgNQgkgUgMgjQgEgPgBgWIAAglQgBgQgEgbIgGgrQgCgZADhIQAEhJgIgxIgLg7QgFgegBhGIgBiHQAAgnAGgSQALgcAcgPQAegOAcAJQAiALAXAnQAOAYANAwQALAqADAaIAEAnIADAmQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAmIAFAlQACAXACAtIADAgQABASgCANQgEAbgSAVQgSAVgZAKQgTAHgUAAIgNAAgAcYdZQgmgSgUgvQgHgSgGgdIgIgwQgIgrgdhQIgohyQgYhIgkiMIgXhXQgShDAagbQANgMATgCQASgDASAHQAbAMAaAkQAeApAYBBQAOAlAZBMQAMAhAnBhQAhBRAPAyQAMAlASBMQAMA2gDAcQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVudNQgWgEgQgNQgXgTgNgjQgIgWgIgrIgLhEIgLg/QgFgWgzi5Qgih9AAhVQAAglAJgUQAIgQARgLQAQgLASgCQATgCASAIQARAIALAPQALAPAJAfIBRESIAaBcQARA+AZB4QAHAiACASQABAdgIAWQgIATgQAPQgQAPgUAFQgLADgMAAQgJAAgKgCgAOpc+QgngLgPgjQgIgQgDglQgJhPgKg/QgMhHgOgzIgXhKQgQgxgHgaQgPg5gIg7QgLhSAhgjQAMgMARgFQARgGARADQAkAHAXAlQAPAXAOAwIBJD4IATBNQAJAgAHAnIAVCEQADAYAAAMQABAUgGAPQgMAfgmAQQgWAIgVAAQgOAAgOgEgAs8c7QgXgHgSgRQgTgRgJgXQgLgeAFgpQACgUANgzQAWhaAQh7QANhsAHiJQAEhMAFiqQABgoAJgRQAJgTAVgLQAVgKAWACQAVACATAOQASAMALATQAMAUAFAcQADARACAjIAMDsQAGBkgEA4QgBASgHBCIgYDLQgGA7gMAiQgRAxgjAXQgUAOgZAEIgQABQgQAAgQgFgEAinAcvQgYgLgPgUQgLgQgLgfIh6lIQgdhOgOgtQgWhEgJg6QgFghAEgSQAFgcAYgTQAYgUAdgBQAcgBAaAQQAZAQANAaQAGAMAFARIAHAeQAMAsAfBBQArBaAGAQQANAgAVBFQAVBCAOAiIAVAwQAMAcAGAVQARA7gWArQgMAXgWAOQgWAOgZACIgHAAQgVAAgUgKgAmjcNQgggWgLgiQgIgWAAgeIACg1QAEg5gGhuQgHhyACg1IACgxQABgdgBgUIgFguQgEgcACgTQACgNAEgSIAIgeIAMgwQAJgaAOgQQAMgOASgJQASgIATAAQApABAbAfQAWAZAFAuQADAaABA1QACAgAIBAQACAagBA2QgBA1ACAbIAJBOQAJBHABCRIgBAoQgBAWgGAQQgLAiggAXQgfAWgkABIgBAAQgjAAgfgWgEAqLAbzQgbgLgZgaQgiglgjhBQh8jlg1j9QgOhAAIgjQAGgZAQgTQASgVAYgEQAVgEAXAKQAUAKAPASQANAQALAXQAGAOAKAcIA0CTQAbBLAOAkQAYA9AYAuIAyBhQAbA4AGAtQAFAkgIAaQgLAjgbANQgOAHgRAAQgPAAgQgGgA2NZDQgYgEgVgOQgVgOgNgVQgPgagBghQgCgfAMgfQALgcAmg3QCRjMBPicIAnhOQAXgtAUgfQAXgjAXgMQAVgKAYAEQAXADASAPQARAOAKAWQAJAUABAYQAAAXgNAsQgkB6hAB1QgcA0hIBxQhDBpgfA9IgQAfQgKARgLALQgRARgZAJQgRAFgRAAIgPgBgA5rQdQgfgCgbgVQgbgVgKgeQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgbASgNQAcgUAsgNQAYgHAPABQASACAQAMQAPAMAKARQAMAXgBAWQgCARgMASQgHAMgRATQg8BDgVAhQgNAWgIAKQgTAbgvAoIhKA/QgmAggYAOQgjAUggAAIgFAAgA92KmQghgJgVgZQgVgagDgjQgCgjARgcQARgaAtgeQAlgYBTgzICbheQAcgSARgIQAagMAXgEQAbgFAZAHQAbAIAOAUQAQAWgCAfQgCAcgQAZQgNAVgZAUQgOAMggAWIi4B7QgeAUgUAPIgjAeQgVAQgSAHQgSAGgSAAQgOAAgPgDgEgg2AFuQgXgKgPgTQgPgUgEgZQgEgZAJgXQALgfAigbQAPgMAxgeQBMgtA/g4IAogjQAXgSATgMQA3ggAvAKQAaAFAUAUQAUAUABAZQABAWgNAXQgJAQgTAWQg2A9hLA9Qg6AvhWA6QgbASgOAHQgYAMgVABIgKABQgTAAgTgJgEgldAD3QglgJgTgdQgZgmANg1QAJglAhgwQAthABEhEQAwgwBShFQA3gwAlgWQAfgSAYgGQAXgFAXAFQAYAGARAOQATAPAHAXQAHAYgHAWQgFAPgNAPQgJAKgRAPIhnBYQg8A1gnAnQg+A/giA5IgdAxQgSAbgUAMQgVANgYAAQgLAAgMgDgEgp9gAOQgWgGgQgQQgPgQgHgWQgHgVADgXQADgcAXgmQAWgkAhgoIA+hHQAkgoAUgTQAsgkAVgTIAdgbQASgRANgJQApgbAjAGQAjAFASAdQATAdgKAhQgFATgWAcQgcAjhhBiQhRBRgkA5QgdAugHAIQgXAcgaAIQgLADgLAAQgLAAgLgCgEgvLgCsQgbgUgDggQgBgSAKgdQAQgwAXgoQAOgZAagjQBBhbAigoQAignAdgXQAdgYAfgLQAkgMAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUASQgaAZggArIg0BIIgbAjQgRAVgJAPQgMATgXApQgVAhgYAQQgYAPgbABIgFAAQgeAAgXgRgEgzNgHYQgXgHgOgXQgOgVABgZQAAguApgoQAXgXA5ghIBzhDQAugbAcgFQAVgEATAGQAVAGAMAOQAVAagJAmQgHAbgcAeQgTAVgdAVQgRAMglAXIhrBEQgeATgTAHQgRAFgPAAQgKAAgKgCgEgz3gM+QgdgRgHgeQgKgpAjgwQAog4BFgbQBCgbBHAIQAwAFAeAUQASAMALASQAMASAAAUQACAjgdAcQgbAagnAHQgMADgrADQgjADgTAHQgNAFghATQgcAQgSAEQgIABgIAAQgWAAgWgMgEgufgRxIgXgOQgNgJgJgEQgWgJggABIgjADQgWACgNAAQgqgBgWgVQgTgSgBgdQgBgbAOgYQAZgnA9gVQBJgYBLASQBOASAsA6QASAWAHAYQAIAbgHAYQgGAUgRAPQgQAPgVAGQgNADgNAAQgcAAgcgQgEgqCgVZQgagJghgZIg2gqQgPgJgggRQgbgQgPgQQgeggAAgoQABgVAJgSQAJgTARgLQAfgVA4AIQBEAKA6AnQA7AmAjA6QAbAtgFAjQgDAUgOARQgOARgUAIQgTAIgVAAQgUAAgWgHgEgj3gW7QgagNgYgbQgRgVgthGQgmg6gfgcIgwgsQgYgbADgbQAEgkAxgaQA5gfAsANQAWAGAVATQAPANAUAZQBbBuAaBaQAOAvgHAgQgEAUgMAQQgNARgSAGQgKAEgLAAQgSAAgUgKgA943MQgfgOgbgkQgcglgnhfQglhaghgnIgrgxQgWgdACgbQACggAhgUQAegTAkABQAlACAlAUQAhATAcAeQAoAtAoBZQAoBbALA/QAIAogHAfQgIAmgcAQQgQAKgUAAQgTAAgTgIgA4z4sQgogLgjgqQgjgsgOg2QgCgJgIguQgGgigJgUQgGgNgMgVIgUgiQgVgsAQgfQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAfAPAtQAIAcALA5IAQBQQAGAhAAAUQAAAcgOATQgNATgYAHQgNADgMAAQgLAAgLgCgAug6AQgfgMgSgdQgPgZgHgkQgEgXgCgqIgHiDQgCgwAFgUQAJgnAcgPQAZgPAgAMQAeALARAaQAPAXAIAhQAEAVAEAnIAMCEIADAmQAAAWgDAQQgJAsggARQgOAIgQAAQgPAAgRgHgA0L6mQgUgGgQgOQgZgVgXgvQgVgqgNgjQgQgtgKg0QgLhAAXgdQAPgTAbgEQAZgFAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAhABAZQACAggOAbQgPAdgcAIQgJADgKAAQgKAAgMgDg");
    var mask_graphics_72 = new cjs.Graphics().p("EAybAg/QgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHdpQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjd3QgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYdvQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVudjQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpdUQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8dRQgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQACQgQAAgQgFgEAinAdFQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjcjQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAcIQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NZZQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rQyQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92K7QghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AGEQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgUATgLQA3ggAvAKQAaAGAUATQAUAUABAaQABAVgNAXQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldAENQglgJgTgeQgZgmANg1QAJglAhgvQAthABEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8AzgnAoQg+BAgiA5IgdAwQgSAbgUANQgVANgYAAQgLAAgMgDgEgp9AAGQgWgGgQgPQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAHQgLADgLAAQgLAAgLgDgEgvLgCXQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgHDQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gMoQgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgEgufgRbIgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNAEgNAAQgcAAgcgQgEgqCgVEQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgEgj3gWlQgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKgA9423QgfgOgbgjQgcglgnhfQglhaghgoIgrgwQgWgdACgbQACggAhgVQAegTAkACQAlACAlAUQAhASAcAfQAoAsAoBaQAoBaALBAQAIAngHAgQgIAmgcAQQgQAJgUAAQgTAAgTgIgA4z4XQgogKgjgrQgjgsgOg1QgCgJgIguQgGgigJgUQgGgOgMgVIgUghQgVgtAQgeQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAeAPAuQAIAbALA5IAQBRQAGAgAAAUQAAAdgOATQgNASgYAHQgNAEgMAAQgLAAgLgDgAug5qQgfgNgSgdQgPgZgHgjQgEgXgCgqIgHiEQgCgvAFgUQAJgnAcgQQAZgOAgALQAeALARAbQAPAWAIAhQAEAWAEAnIAMCEIADAmQAAAVgDARQgJAsggARQgOAHgQAAQgPAAgRgGgApb6LQgRgHgMgOQgQgTgFgfQgEgVABgkIABi1QAAgbABgNQACgXAHgRQAIgUAPgOQAQgOATgCQAYgCAWARQAUARAIAZQAIAVABAdIAAAyIAFBsQACA/gMAqQgGAbgOASQgQAWgXAFIgLACQgMAAgMgFgA0L6RQgUgFgQgPQgZgVgXgvQgVgpgNgkQgQgsgKg0QgLhAAXgeQAPgSAbgFQAZgEAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAgABAZQACAggOAbQgPAegcAIQgJADgKAAQgKAAgMgEg");
    var mask_graphics_75 = new cjs.Graphics().p("EAybAg/QgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHdpQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjd3QgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYdvQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVudjQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpdUQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8dRQgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQACQgQAAgQgFgEAinAdFQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjcjQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAcIQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NZZQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rQyQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92K7QghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AGEQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgUATgLQA3ggAvAKQAaAGAUATQAUAUABAaQABAVgNAXQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldAENQglgJgTgeQgZgmANg1QAJglAhgvQAthABEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8AzgnAoQg+BAgiA5IgdAwQgSAbgUANQgVANgYAAQgLAAgMgDgEgp9AAGQgWgGgQgPQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAHQgLADgLAAQgLAAgLgDgEgvLgCXQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgHDQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gMoQgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgEgufgRbIgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNAEgNAAQgcAAgcgQgEgqCgVEQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgEgj3gWlQgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKgA9423QgfgOgbgjQgcglgnhfQglhaghgoIgrgwQgWgdACgbQACggAhgVQAegTAkACQAlACAlAUQAhASAcAfQAoAsAoBaQAoBaALBAQAIAngHAgQgIAmgcAQQgQAJgUAAQgTAAgTgIgAlg4MQgdgOgLgnQgIgcABgtIACknQAAgmAGgXQAIghAVgRQAQgNAXgDQAVgCAUAJQAnASAQAsQAJAaADAyQAFBYABAtQABBKgGA6QgIBIgcAkQgSAYgdAJQgNAEgMAAQgQAAgOgHgA4z4XQgogKgjgrQgjgsgOg1QgCgJgIguQgGgigJgUQgGgOgMgVIgUghQgVgtAQgeQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAeAPAuQAIAbALA5IAQBRQAGAgAAAUQAAAdgOATQgNASgYAHQgNAEgMAAQgLAAgLgDgAug5qQgfgNgSgdQgPgZgHgjQgEgXgCgqIgHiEQgCgvAFgUQAJgnAcgQQAZgOAgALQAeALARAbQAPAWAIAhQAEAWAEAnIAMCEIADAmQAAAVgDARQgJAsggARQgOAHgQAAQgPAAgRgGgApb6LQgRgHgMgOQgQgTgFgfQgEgVABgkIABi1QAAgbABgNQACgXAHgRQAIgUAPgOQAQgOATgCQAYgCAWARQAUARAIAZQAIAVABAdIAAAyIAFBsQACA/gMAqQgGAbgOASQgQAWgXAFIgLACQgMAAgMgFgA0L6RQgUgFgQgPQgZgVgXgvQgVgpgNgkQgQgsgKg0QgLhAAXgeQAPgSAbgFQAZgEAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAgABAZQACAggOAbQgPAegcAIQgJADgKAAQgKAAgMgEg");
    var mask_graphics_78 = new cjs.Graphics().p("EAybAg/QgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHdpQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjd3QgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYdvQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVudjQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpdUQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8dRQgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQACQgQAAgQgFgEAinAdFQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjcjQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAcIQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NZZQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rQyQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92K7QghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AGEQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgUATgLQA3ggAvAKQAaAGAUATQAUAUABAaQABAVgNAXQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldAENQglgJgTgeQgZgmANg1QAJglAhgvQAthABEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8AzgnAoQg+BAgiA5IgdAwQgSAbgUANQgVANgYAAQgLAAgMgDgEgp9AAGQgWgGgQgPQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAHQgLADgLAAQgLAAgLgDgEgvLgCXQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgHDQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gMoQgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgEgufgRbIgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNAEgNAAQgcAAgcgQgEgqCgVEQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgEgj3gWlQgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKgA9423QgfgOgbgjQgcglgnhfQglhaghgoIgrgwQgWgdACgbQACggAhgVQAegTAkACQAlACAlAUQAhASAcAfQAoAsAoBaQAoBaALBAQAIAngHAgQgIAmgcAQQgQAJgUAAQgTAAgTgIgAg747QgJgfAAg8IgBjNQgBg1AKgbQAHgUAOgOQAQgPATgDQAXgDAWAPQAUAOAKAXQAJAUADAbQACARgBAgIgBCwQgBAlgBARQgDAegJAXQgKAbgTATQgVAUgYAEQgngTgPgzgAlg4MQgdgOgLgnQgIgcABgtIACknQAAgmAGgXQAIghAVgRQAQgNAXgDQAVgCAUAJQAnASAQAsQAJAaADAyQAFBYABAtQABBKgGA6QgIBIgcAkQgSAYgdAJQgNAEgMAAQgQAAgOgHgA4z4XQgogKgjgrQgjgsgOg1QgCgJgIguQgGgigJgUQgGgOgMgVIgUghQgVgtAQgeQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAeAPAuQAIAbALA5IAQBRQAGAgAAAUQAAAdgOATQgNASgYAHQgNAEgMAAQgLAAgLgDgAug5qQgfgNgSgdQgPgZgHgjQgEgXgCgqIgHiEQgCgvAFgUQAJgnAcgQQAZgOAgALQAeALARAbQAPAWAIAhQAEAWAEAnIAMCEIADAmQAAAVgDARQgJAsggARQgOAHgQAAQgPAAgRgGgApb6LQgRgHgMgOQgQgTgFgfQgEgVABgkIABi1QAAgbABgNQACgXAHgRQAIgUAPgOQAQgOATgCQAYgCAWARQAUARAIAZQAIAVABAdIAAAyIAFBsQACA/gMAqQgGAbgOASQgQAWgXAFIgLACQgMAAgMgFgA0L6RQgUgFgQgPQgZgVgXgvQgVgpgNgkQgQgsgKg0QgLhAAXgeQAPgSAbgFQAZgEAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAgABAZQACAggOAbQgPAegcAIQgJADgKAAQgKAAgMgEg");
    var mask_graphics_81 = new cjs.Graphics().p("EAybAg/QgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHdpQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjd3QgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYdvQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVudjQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpdUQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8dRQgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQACQgQAAgQgFgEAinAdFQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjcjQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAcIQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NZZQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rQyQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92K7QghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AGEQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgUATgLQA3ggAvAKQAaAGAUATQAUAUABAaQABAVgNAXQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldAENQglgJgTgeQgZgmANg1QAJglAhgvQAthABEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8AzgnAoQg+BAgiA5IgdAwQgSAbgUANQgVANgYAAQgLAAgMgDgEgp9AAGQgWgGgQgPQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAHQgLADgLAAQgLAAgLgDgEgvLgCXQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgHDQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gMoQgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgEgufgRbIgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNAEgNAAQgcAAgcgQgEgqCgVEQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgEgj3gWlQgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKgA9423QgfgOgbgjQgcglgnhfQglhaghgoIgrgwQgWgdACgbQACggAhgVQAegTAkACQAlACAlAUQAhASAcAfQAoAsAoBaQAoBaALBAQAIAngHAgQgIAmgcAQQgQAJgUAAQgTAAgTgIgAg747QgJgfAAg8IgBjNQgBg1AKgbQAHgUAOgOQAQgPATgDQAXgDAWAPQAUAOAKAXQAJAUADAbQACARgBAgIgBCwQgBAlgBARQgDAegJAXQgKAbgTATQgVAUgYAEQgngTgPgzgAFD4BQgQgKgJgRQgOgaABgoIAGhFIACg3QABghAEgWQALhBAqgcQAagRAYAIQAOAEAKANQAJAMAEAQQADANAAASIgBAeQAABRgCAoQgEBFgYAsQgWAngfADIgFABQgPAAgOgJgAlg4MQgdgOgLgnQgIgcABgtIACknQAAgmAGgXQAIghAVgRQAQgNAXgDQAVgCAUAJQAnASAQAsQAJAaADAyQAFBYABAtQABBKgGA6QgIBIgcAkQgSAYgdAJQgNAEgMAAQgQAAgOgHgA4z4XQgogKgjgrQgjgsgOg1QgCgJgIguQgGgigJgUQgGgOgMgVIgUghQgVgtAQgeQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAeAPAuQAIAbALA5IAQBRQAGAgAAAUQAAAdgOATQgNASgYAHQgNAEgMAAQgLAAgLgDgAug5qQgfgNgSgdQgPgZgHgjQgEgXgCgqIgHiEQgCgvAFgUQAJgnAcgQQAZgOAgALQAeALARAbQAPAWAIAhQAEAWAEAnIAMCEIADAmQAAAVgDARQgJAsggARQgOAHgQAAQgPAAgRgGgApb6LQgRgHgMgOQgQgTgFgfQgEgVABgkIABi1QAAgbABgNQACgXAHgRQAIgUAPgOQAQgOATgCQAYgCAWARQAUARAIAZQAIAVABAdIAAAyIAFBsQACA/gMAqQgGAbgOASQgQAWgXAFIgLACQgMAAgMgFgA0L6RQgUgFgQgPQgZgVgXgvQgVgpgNgkQgQgsgKg0QgLhAAXgeQAPgSAbgFQAZgEAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAgABAZQACAggOAbQgPAegcAIQgJADgKAAQgKAAgMgEg");
    var mask_graphics_84 = new cjs.Graphics().p("EAybAg/QgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHdpQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjd3QgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYdvQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVudjQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpdUQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8dRQgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQACQgQAAgQgFgEAinAdFQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjcjQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAcIQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NZZQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rQyQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92K7QghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AGEQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgUATgLQA3ggAvAKQAaAGAUATQAUAUABAaQABAVgNAXQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldAENQglgJgTgeQgZgmANg1QAJglAhgvQAthABEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8AzgnAoQg+BAgiA5IgdAwQgSAbgUANQgVANgYAAQgLAAgMgDgEgp9AAGQgWgGgQgPQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAHQgLADgLAAQgLAAgLgDgEgvLgCXQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgHDQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gMoQgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgEgufgRbIgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNAEgNAAQgcAAgcgQgEgqCgVEQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgAJG2NQgpgbALhaIAQiVQAEgjADgRQAGgcAKgVQANgZAUgRQAWgRAZgCQAdgDAZAVQAYAUADAeQACAPgFAUIgKAjQgIAhgGA9QgGBEgGAbQgMA8gdAbQgTARgZAFIgOABQgSAAgOgJgEgj3gWlQgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKgA9423QgfgOgbgjQgcglgnhfQglhaghgoIgrgwQgWgdACgbQACggAhgVQAegTAkACQAlACAlAUQAhASAcAfQAoAsAoBaQAoBaALBAQAIAngHAgQgIAmgcAQQgQAJgUAAQgTAAgTgIgAg747QgJgfAAg8IgBjNQgBg1AKgbQAHgUAOgOQAQgPATgDQAXgDAWAPQAUAOAKAXQAJAUADAbQACARgBAgIgBCwQgBAlgBARQgDAegJAXQgKAbgTATQgVAUgYAEQgngTgPgzgAFD4BQgQgKgJgRQgOgaABgoIAGhFIACg3QABghAEgWQALhBAqgcQAagRAYAIQAOAEAKANQAJAMAEAQQADANAAASIgBAeQAABRgCAoQgEBFgYAsQgWAngfADIgFABQgPAAgOgJgAlg4MQgdgOgLgnQgIgcABgtIACknQAAgmAGgXQAIghAVgRQAQgNAXgDQAVgCAUAJQAnASAQAsQAJAaADAyQAFBYABAtQABBKgGA6QgIBIgcAkQgSAYgdAJQgNAEgMAAQgQAAgOgHgA4z4XQgogKgjgrQgjgsgOg1QgCgJgIguQgGgigJgUQgGgOgMgVIgUghQgVgtAQgeQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAeAPAuQAIAbALA5IAQBRQAGAgAAAUQAAAdgOATQgNASgYAHQgNAEgMAAQgLAAgLgDgAug5qQgfgNgSgdQgPgZgHgjQgEgXgCgqIgHiEQgCgvAFgUQAJgnAcgQQAZgOAgALQAeALARAbQAPAWAIAhQAEAWAEAnIAMCEIADAmQAAAVgDARQgJAsggARQgOAHgQAAQgPAAgRgGgApb6LQgRgHgMgOQgQgTgFgfQgEgVABgkIABi1QAAgbABgNQACgXAHgRQAIgUAPgOQAQgOATgCQAYgCAWARQAUARAIAZQAIAVABAdIAAAyIAFBsQACA/gMAqQgGAbgOASQgQAWgXAFIgLACQgMAAgMgFgA0L6RQgUgFgQgPQgZgVgXgvQgVgpgNgkQgQgsgKg0QgLhAAXgeQAPgSAbgFQAZgEAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAgABAZQACAggOAbQgPAegcAIQgJADgKAAQgKAAgMgEg");
    var mask_graphics_87 = new cjs.Graphics().p("EAybAg/QgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHdpQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjd3QgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYdvQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVudjQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpdUQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8dRQgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQACQgQAAgQgFgEAinAdFQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjcjQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAcIQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NZZQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rQyQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92K7QghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AGEQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgUATgLQA3ggAvAKQAaAGAUATQAUAUABAaQABAVgNAXQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldAENQglgJgTgeQgZgmANg1QAJglAhgvQAthABEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8AzgnAoQg+BAgiA5IgdAwQgSAbgUANQgVANgYAAQgLAAgMgDgEgp9AAGQgWgGgQgPQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAHQgLADgLAAQgLAAgLgDgEgvLgCXQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgHDQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gMoQgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgEgufgRbIgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNAEgNAAQgcAAgcgQgANS0KQgdgQgOgdQgTgrAOhPQANhCAnh8QAPg0ARgYQANgSARgKQATgMATAAQAdgBAYAZQAWAWAGAfQAFAbgGAhQgDAUgLAoIg4DAQgJAhgKASQgPAagWAKQgLAGgNAAQgQAAgSgJgEgqCgVEQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgAJG2NQgpgbALhaIAQiVQAEgjADgRQAGgcAKgVQANgZAUgRQAWgRAZgCQAdgDAZAVQAYAUADAeQACAPgFAUIgKAjQgIAhgGA9QgGBEgGAbQgMA8gdAbQgTARgZAFIgOABQgSAAgOgJgEgj3gWlQgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKgA9423QgfgOgbgjQgcglgnhfQglhaghgoIgrgwQgWgdACgbQACggAhgVQAegTAkACQAlACAlAUQAhASAcAfQAoAsAoBaQAoBaALBAQAIAngHAgQgIAmgcAQQgQAJgUAAQgTAAgTgIgAg747QgJgfAAg8IgBjNQgBg1AKgbQAHgUAOgOQAQgPATgDQAXgDAWAPQAUAOAKAXQAJAUADAbQACARgBAgIgBCwQgBAlgBARQgDAegJAXQgKAbgTATQgVAUgYAEQgngTgPgzgAFD4BQgQgKgJgRQgOgaABgoIAGhFIACg3QABghAEgWQALhBAqgcQAagRAYAIQAOAEAKANQAJAMAEAQQADANAAASIgBAeQAABRgCAoQgEBFgYAsQgWAngfADIgFABQgPAAgOgJgAlg4MQgdgOgLgnQgIgcABgtIACknQAAgmAGgXQAIghAVgRQAQgNAXgDQAVgCAUAJQAnASAQAsQAJAaADAyQAFBYABAtQABBKgGA6QgIBIgcAkQgSAYgdAJQgNAEgMAAQgQAAgOgHgA4z4XQgogKgjgrQgjgsgOg1QgCgJgIguQgGgigJgUQgGgOgMgVIgUghQgVgtAQgeQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAeAPAuQAIAbALA5IAQBRQAGAgAAAUQAAAdgOATQgNASgYAHQgNAEgMAAQgLAAgLgDgAug5qQgfgNgSgdQgPgZgHgjQgEgXgCgqIgHiEQgCgvAFgUQAJgnAcgQQAZgOAgALQAeALARAbQAPAWAIAhQAEAWAEAnIAMCEIADAmQAAAVgDARQgJAsggARQgOAHgQAAQgPAAgRgGgApb6LQgRgHgMgOQgQgTgFgfQgEgVABgkIABi1QAAgbABgNQACgXAHgRQAIgUAPgOQAQgOATgCQAYgCAWARQAUARAIAZQAIAVABAdIAAAyIAFBsQACA/gMAqQgGAbgOASQgQAWgXAFIgLACQgMAAgMgFgA0L6RQgUgFgQgPQgZgVgXgvQgVgpgNgkQgQgsgKg0QgLhAAXgeQAPgSAbgFQAZgEAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAgABAZQACAggOAbQgPAegcAIQgJADgKAAQgKAAgMgEg");
    var mask_graphics_90 = new cjs.Graphics().p("EAybAg/QgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHdpQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjd3QgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYdvQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVudjQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpdUQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8dRQgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQACQgQAAgQgFgEAinAdFQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjcjQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAcIQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NZZQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rQyQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92K7QghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AGEQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgUATgLQA3ggAvAKQAaAGAUATQAUAUABAaQABAVgNAXQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldAENQglgJgTgeQgZgmANg1QAJglAhgvQAthABEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8AzgnAoQg+BAgiA5IgdAwQgSAbgUANQgVANgYAAQgLAAgMgDgEgp9AAGQgWgGgQgPQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAHQgLADgLAAQgLAAgLgDgEgvLgCXQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgHDQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gMoQgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgEgufgRbIgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNAEgNAAQgcAAgcgQgAR8zAQgcgQgIglQgGgXADgqQAGhSANg4QARhJAig2QASgcAXgPQAbgSAaAJQATAGAMAUQAKASABAXQABASgGAXIgMApQgQA2gFAvQgDA8gFAdQgJA1gdAbQgRAQgXAFQgHACgIAAQgPAAgNgHgANS0KQgdgQgOgdQgTgrAOhPQANhCAnh8QAPg0ARgYQANgSARgKQATgMATAAQAdgBAYAZQAWAWAGAfQAFAbgGAhQgDAUgLAoIg4DAQgJAhgKASQgPAagWAKQgLAGgNAAQgQAAgSgJgEgqCgVEQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgAJG2NQgpgbALhaIAQiVQAEgjADgRQAGgcAKgVQANgZAUgRQAWgRAZgCQAdgDAZAVQAYAUADAeQACAPgFAUIgKAjQgIAhgGA9QgGBEgGAbQgMA8gdAbQgTARgZAFIgOABQgSAAgOgJgEgj3gWlQgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKgA9423QgfgOgbgjQgcglgnhfQglhaghgoIgrgwQgWgdACgbQACggAhgVQAegTAkACQAlACAlAUQAhASAcAfQAoAsAoBaQAoBaALBAQAIAngHAgQgIAmgcAQQgQAJgUAAQgTAAgTgIgAg747QgJgfAAg8IgBjNQgBg1AKgbQAHgUAOgOQAQgPATgDQAXgDAWAPQAUAOAKAXQAJAUADAbQACARgBAgIgBCwQgBAlgBARQgDAegJAXQgKAbgTATQgVAUgYAEQgngTgPgzgAFD4BQgQgKgJgRQgOgaABgoIAGhFIACg3QABghAEgWQALhBAqgcQAagRAYAIQAOAEAKANQAJAMAEAQQADANAAASIgBAeQAABRgCAoQgEBFgYAsQgWAngfADIgFABQgPAAgOgJgAlg4MQgdgOgLgnQgIgcABgtIACknQAAgmAGgXQAIghAVgRQAQgNAXgDQAVgCAUAJQAnASAQAsQAJAaADAyQAFBYABAtQABBKgGA6QgIBIgcAkQgSAYgdAJQgNAEgMAAQgQAAgOgHgA4z4XQgogKgjgrQgjgsgOg1QgCgJgIguQgGgigJgUQgGgOgMgVIgUghQgVgtAQgeQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAeAPAuQAIAbALA5IAQBRQAGAgAAAUQAAAdgOATQgNASgYAHQgNAEgMAAQgLAAgLgDgAug5qQgfgNgSgdQgPgZgHgjQgEgXgCgqIgHiEQgCgvAFgUQAJgnAcgQQAZgOAgALQAeALARAbQAPAWAIAhQAEAWAEAnIAMCEIADAmQAAAVgDARQgJAsggARQgOAHgQAAQgPAAgRgGgApb6LQgRgHgMgOQgQgTgFgfQgEgVABgkIABi1QAAgbABgNQACgXAHgRQAIgUAPgOQAQgOATgCQAYgCAWARQAUARAIAZQAIAVABAdIAAAyIAFBsQACA/gMAqQgGAbgOASQgQAWgXAFIgLACQgMAAgMgFgA0L6RQgUgFgQgPQgZgVgXgvQgVgpgNgkQgQgsgKg0QgLhAAXgeQAPgSAbgFQAZgEAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAgABAZQACAggOAbQgPAegcAIQgJADgKAAQgKAAgMgEg");
    var mask_graphics_93 = new cjs.Graphics().p("EAybAg/QgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHdpQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjd3QgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYdvQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVudjQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpdUQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8dRQgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQACQgQAAgQgFgEAinAdFQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjcjQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAcIQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NZZQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rQyQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92K7QghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AGEQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgUATgLQA3ggAvAKQAaAGAUATQAUAUABAaQABAVgNAXQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldAENQglgJgTgeQgZgmANg1QAJglAhgvQAthABEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8AzgnAoQg+BAgiA5IgdAwQgSAbgUANQgVANgYAAQgLAAgMgDgEgp9AAGQgWgGgQgPQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAHQgLADgLAAQgLAAgLgDgEgvLgCXQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgHDQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gMoQgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgEgufgRbIgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNAEgNAAQgcAAgcgQgAWoxUQgYgIgTgiQgcg3AAhRQAAgPAKiCIAFg5QAGggAMgWQAOgaAcgNQAcgOAZAMQAsAVgBBUQAAAzgFAvIgFAtQgCAZAAATIAEA9QACAkgGAYQgIAfgZAUQgTAOgTAAQgIAAgJgDgAR8zAQgcgQgIglQgGgXADgqQAGhSANg4QARhJAig2QASgcAXgPQAbgSAaAJQATAGAMAUQAKASABAXQABASgGAXIgMApQgQA2gFAvQgDA8gFAdQgJA1gdAbQgRAQgXAFQgHACgIAAQgPAAgNgHgANS0KQgdgQgOgdQgTgrAOhPQANhCAnh8QAPg0ARgYQANgSARgKQATgMATAAQAdgBAYAZQAWAWAGAfQAFAbgGAhQgDAUgLAoIg4DAQgJAhgKASQgPAagWAKQgLAGgNAAQgQAAgSgJgEgqCgVEQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgAJG2NQgpgbALhaIAQiVQAEgjADgRQAGgcAKgVQANgZAUgRQAWgRAZgCQAdgDAZAVQAYAUADAeQACAPgFAUIgKAjQgIAhgGA9QgGBEgGAbQgMA8gdAbQgTARgZAFIgOABQgSAAgOgJgEgj3gWlQgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKgA9423QgfgOgbgjQgcglgnhfQglhaghgoIgrgwQgWgdACgbQACggAhgVQAegTAkACQAlACAlAUQAhASAcAfQAoAsAoBaQAoBaALBAQAIAngHAgQgIAmgcAQQgQAJgUAAQgTAAgTgIgAg747QgJgfAAg8IgBjNQgBg1AKgbQAHgUAOgOQAQgPATgDQAXgDAWAPQAUAOAKAXQAJAUADAbQACARgBAgIgBCwQgBAlgBARQgDAegJAXQgKAbgTATQgVAUgYAEQgngTgPgzgAFD4BQgQgKgJgRQgOgaABgoIAGhFIACg3QABghAEgWQALhBAqgcQAagRAYAIQAOAEAKANQAJAMAEAQQADANAAASIgBAeQAABRgCAoQgEBFgYAsQgWAngfADIgFABQgPAAgOgJgAlg4MQgdgOgLgnQgIgcABgtIACknQAAgmAGgXQAIghAVgRQAQgNAXgDQAVgCAUAJQAnASAQAsQAJAaADAyQAFBYABAtQABBKgGA6QgIBIgcAkQgSAYgdAJQgNAEgMAAQgQAAgOgHgA4z4XQgogKgjgrQgjgsgOg1QgCgJgIguQgGgigJgUQgGgOgMgVIgUghQgVgtAQgeQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAeAPAuQAIAbALA5IAQBRQAGAgAAAUQAAAdgOATQgNASgYAHQgNAEgMAAQgLAAgLgDgAug5qQgfgNgSgdQgPgZgHgjQgEgXgCgqIgHiEQgCgvAFgUQAJgnAcgQQAZgOAgALQAeALARAbQAPAWAIAhQAEAWAEAnIAMCEIADAmQAAAVgDARQgJAsggARQgOAHgQAAQgPAAgRgGgApb6LQgRgHgMgOQgQgTgFgfQgEgVABgkIABi1QAAgbABgNQACgXAHgRQAIgUAPgOQAQgOATgCQAYgCAWARQAUARAIAZQAIAVABAdIAAAyIAFBsQACA/gMAqQgGAbgOASQgQAWgXAFIgLACQgMAAgMgFgA0L6RQgUgFgQgPQgZgVgXgvQgVgpgNgkQgQgsgKg0QgLhAAXgeQAPgSAbgFQAZgEAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAgABAZQACAggOAbQgPAegcAIQgJADgKAAQgKAAgMgEg");
    var mask_graphics_96 = new cjs.Graphics().p("EAybAg/QgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHdpQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjd3QgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYdvQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVudjQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpdUQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8dRQgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQACQgQAAgQgFgEAinAdFQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjcjQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAcIQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NZZQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rQyQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92K7QghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AGEQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgUATgLQA3ggAvAKQAaAGAUATQAUAUABAaQABAVgNAXQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldAENQglgJgTgeQgZgmANg1QAJglAhgvQAthABEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8AzgnAoQg+BAgiA5IgdAwQgSAbgUANQgVANgYAAQgLAAgMgDgEgp9AAGQgWgGgQgPQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAHQgLADgLAAQgLAAgLgDgEgvLgCXQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgHDQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gMoQgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgEgufgRbIgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNAEgNAAQgcAAgcgQgAWoxUQgYgIgTgiQgcg3AAhRQAAgPAKiCIAFg5QAGggAMgWQAOgaAcgNQAcgOAZAMQAsAVgBBUQAAAzgFAvIgFAtQgCAZAAATIAEA9QACAkgGAYQgIAfgZAUQgTAOgTAAQgIAAgJgDgAa3yGQgYgKgOgVQgNgSgHgbQgFgQgFggQgJg3AAgbQAAgXAKg+QAFgdAEgNQAGgYALgQQAMgTAUgKQAVgLAUAFQATAFANASQAMAQAEAWQADARgBAYIgCApQgBAeAEAqIAIBIQAGAtgHAaQgFASgLAOQgMAPgQAGQgJAEgKAAQgNAAgOgHgAR8zAQgcgQgIglQgGgXADgqQAGhSANg4QARhJAig2QASgcAXgPQAbgSAaAJQATAGAMAUQAKASABAXQABASgGAXIgMApQgQA2gFAvQgDA8gFAdQgJA1gdAbQgRAQgXAFQgHACgIAAQgPAAgNgHgANS0KQgdgQgOgdQgTgrAOhPQANhCAnh8QAPg0ARgYQANgSARgKQATgMATAAQAdgBAYAZQAWAWAGAfQAFAbgGAhQgDAUgLAoIg4DAQgJAhgKASQgPAagWAKQgLAGgNAAQgQAAgSgJgEgqCgVEQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgAJG2NQgpgbALhaIAQiVQAEgjADgRQAGgcAKgVQANgZAUgRQAWgRAZgCQAdgDAZAVQAYAUADAeQACAPgFAUIgKAjQgIAhgGA9QgGBEgGAbQgMA8gdAbQgTARgZAFIgOABQgSAAgOgJgEgj3gWlQgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKgA9423QgfgOgbgjQgcglgnhfQglhaghgoIgrgwQgWgdACgbQACggAhgVQAegTAkACQAlACAlAUQAhASAcAfQAoAsAoBaQAoBaALBAQAIAngHAgQgIAmgcAQQgQAJgUAAQgTAAgTgIgAg747QgJgfAAg8IgBjNQgBg1AKgbQAHgUAOgOQAQgPATgDQAXgDAWAPQAUAOAKAXQAJAUADAbQACARgBAgIgBCwQgBAlgBARQgDAegJAXQgKAbgTATQgVAUgYAEQgngTgPgzgAFD4BQgQgKgJgRQgOgaABgoIAGhFIACg3QABghAEgWQALhBAqgcQAagRAYAIQAOAEAKANQAJAMAEAQQADANAAASIgBAeQAABRgCAoQgEBFgYAsQgWAngfADIgFABQgPAAgOgJgAlg4MQgdgOgLgnQgIgcABgtIACknQAAgmAGgXQAIghAVgRQAQgNAXgDQAVgCAUAJQAnASAQAsQAJAaADAyQAFBYABAtQABBKgGA6QgIBIgcAkQgSAYgdAJQgNAEgMAAQgQAAgOgHgA4z4XQgogKgjgrQgjgsgOg1QgCgJgIguQgGgigJgUQgGgOgMgVIgUghQgVgtAQgeQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAeAPAuQAIAbALA5IAQBRQAGAgAAAUQAAAdgOATQgNASgYAHQgNAEgMAAQgLAAgLgDgAug5qQgfgNgSgdQgPgZgHgjQgEgXgCgqIgHiEQgCgvAFgUQAJgnAcgQQAZgOAgALQAeALARAbQAPAWAIAhQAEAWAEAnIAMCEIADAmQAAAVgDARQgJAsggARQgOAHgQAAQgPAAgRgGgApb6LQgRgHgMgOQgQgTgFgfQgEgVABgkIABi1QAAgbABgNQACgXAHgRQAIgUAPgOQAQgOATgCQAYgCAWARQAUARAIAZQAIAVABAdIAAAyIAFBsQACA/gMAqQgGAbgOASQgQAWgXAFIgLACQgMAAgMgFgA0L6RQgUgFgQgPQgZgVgXgvQgVgpgNgkQgQgsgKg0QgLhAAXgeQAPgSAbgFQAZgEAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAgABAZQACAggOAbQgPAegcAIQgJADgKAAQgKAAgMgEg");
    var mask_graphics_99 = new cjs.Graphics().p("EAybAg/QgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHdpQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjd3QgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYdvQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVudjQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpdUQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8dRQgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQACQgQAAgQgFgEAinAdFQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjcjQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAcIQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NZZQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rQyQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92K7QghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AGEQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgUATgLQA3ggAvAKQAaAGAUATQAUAUABAaQABAVgNAXQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldAENQglgJgTgeQgZgmANg1QAJglAhgvQAthABEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8AzgnAoQg+BAgiA5IgdAwQgSAbgUANQgVANgYAAQgLAAgMgDgEgp9AAGQgWgGgQgPQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAHQgLADgLAAQgLAAgLgDgEgvLgCXQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgHDQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gMoQgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgAfbwZQgWgLgOgWQgMgSgGgaQgEgSgDgeQgNhogEhHQgGhfAGhQQADgwANgZQAJgSAQgLQARgLASAAQATABAQAOQAQANAHAUQAHAQACAXIACAoQACBGAJBhIARCoQAFAzgHAZQgFATgMAPQgNAQgSAFQgIACgIAAQgOAAgPgHgEgufgRbIgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNAEgNAAQgcAAgcgQgAWoxUQgYgIgTgiQgcg3AAhRQAAgPAKiCIAFg5QAGggAMgWQAOgaAcgNQAcgOAZAMQAsAVgBBUQAAAzgFAvIgFAtQgCAZAAATIAEA9QACAkgGAYQgIAfgZAUQgTAOgTAAQgIAAgJgDgAa3yGQgYgKgOgVQgNgSgHgbQgFgQgFggQgJg3AAgbQAAgXAKg+QAFgdAEgNQAGgYALgQQAMgTAUgKQAVgLAUAFQATAFANASQAMAQAEAWQADARgBAYIgCApQgBAeAEAqIAIBIQAGAtgHAaQgFASgLAOQgMAPgQAGQgJAEgKAAQgNAAgOgHgAR8zAQgcgQgIglQgGgXADgqQAGhSANg4QARhJAig2QASgcAXgPQAbgSAaAJQATAGAMAUQAKASABAXQABASgGAXIgMApQgQA2gFAvQgDA8gFAdQgJA1gdAbQgRAQgXAFQgHACgIAAQgPAAgNgHgANS0KQgdgQgOgdQgTgrAOhPQANhCAnh8QAPg0ARgYQANgSARgKQATgMATAAQAdgBAYAZQAWAWAGAfQAFAbgGAhQgDAUgLAoIg4DAQgJAhgKASQgPAagWAKQgLAGgNAAQgQAAgSgJgEgqCgVEQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgAJG2NQgpgbALhaIAQiVQAEgjADgRQAGgcAKgVQANgZAUgRQAWgRAZgCQAdgDAZAVQAYAUADAeQACAPgFAUIgKAjQgIAhgGA9QgGBEgGAbQgMA8gdAbQgTARgZAFIgOABQgSAAgOgJgEgj3gWlQgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKgA9423QgfgOgbgjQgcglgnhfQglhaghgoIgrgwQgWgdACgbQACggAhgVQAegTAkACQAlACAlAUQAhASAcAfQAoAsAoBaQAoBaALBAQAIAngHAgQgIAmgcAQQgQAJgUAAQgTAAgTgIgAg747QgJgfAAg8IgBjNQgBg1AKgbQAHgUAOgOQAQgPATgDQAXgDAWAPQAUAOAKAXQAJAUADAbQACARgBAgIgBCwQgBAlgBARQgDAegJAXQgKAbgTATQgVAUgYAEQgngTgPgzgAFD4BQgQgKgJgRQgOgaABgoIAGhFIACg3QABghAEgWQALhBAqgcQAagRAYAIQAOAEAKANQAJAMAEAQQADANAAASIgBAeQAABRgCAoQgEBFgYAsQgWAngfADIgFABQgPAAgOgJgAlg4MQgdgOgLgnQgIgcABgtIACknQAAgmAGgXQAIghAVgRQAQgNAXgDQAVgCAUAJQAnASAQAsQAJAaADAyQAFBYABAtQABBKgGA6QgIBIgcAkQgSAYgdAJQgNAEgMAAQgQAAgOgHgA4z4XQgogKgjgrQgjgsgOg1QgCgJgIguQgGgigJgUQgGgOgMgVIgUghQgVgtAQgeQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAeAPAuQAIAbALA5IAQBRQAGAgAAAUQAAAdgOATQgNASgYAHQgNAEgMAAQgLAAgLgDgAug5qQgfgNgSgdQgPgZgHgjQgEgXgCgqIgHiEQgCgvAFgUQAJgnAcgQQAZgOAgALQAeALARAbQAPAWAIAhQAEAWAEAnIAMCEIADAmQAAAVgDARQgJAsggARQgOAHgQAAQgPAAgRgGgApb6LQgRgHgMgOQgQgTgFgfQgEgVABgkIABi1QAAgbABgNQACgXAHgRQAIgUAPgOQAQgOATgCQAYgCAWARQAUARAIAZQAIAVABAdIAAAyIAFBsQACA/gMAqQgGAbgOASQgQAWgXAFIgLACQgMAAgMgFgA0L6RQgUgFgQgPQgZgVgXgvQgVgpgNgkQgQgsgKg0QgLhAAXgeQAPgSAbgFQAZgEAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAgABAZQACAggOAbQgPAegcAIQgJADgKAAQgKAAgMgEg");
    var mask_graphics_102 = new cjs.Graphics().p("EAybAg/QgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHdpQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjd3QgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYdvQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVudjQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpdUQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8dRQgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQACQgQAAgQgFgEAinAdFQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjcjQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAcIQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NZZQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rQyQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92K7QghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AGEQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgUATgLQA3ggAvAKQAaAGAUATQAUAUABAaQABAVgNAXQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldAENQglgJgTgeQgZgmANg1QAJglAhgvQAthABEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8AzgnAoQg+BAgiA5IgdAwQgSAbgUANQgVANgYAAQgLAAgMgDgEgp9AAGQgWgGgQgPQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAHQgLADgLAAQgLAAgLgDgEgvLgCXQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgHDQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gMoQgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgAfbwZQgWgLgOgWQgMgSgGgaQgEgSgDgeQgNhogEhHQgGhfAGhQQADgwANgZQAJgSAQgLQARgLASAAQATABAQAOQAQANAHAUQAHAQACAXIACAoQACBGAJBhIARCoQAFAzgHAZQgFATgMAPQgNAQgSAFQgIACgIAAQgOAAgPgHgEAjtgRIQgYgIgQgTQgNgQgJgZQgGgQgHgfQgliwgMi8QgCgqALgTQAMgUAYgGQAYgHAWAKQAeANARAlQAMAaAIAuQAUBxAYDCQAHA1gFAbQgIAlgbAOQgMAGgMAAQgKAAgLgDgEgufgRbIgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNAEgNAAQgcAAgcgQgAWoxUQgYgIgTgiQgcg3AAhRQAAgPAKiCIAFg5QAGggAMgWQAOgaAcgNQAcgOAZAMQAsAVgBBUQAAAzgFAvIgFAtQgCAZAAATIAEA9QACAkgGAYQgIAfgZAUQgTAOgTAAQgIAAgJgDgAa3yGQgYgKgOgVQgNgSgHgbQgFgQgFggQgJg3AAgbQAAgXAKg+QAFgdAEgNQAGgYALgQQAMgTAUgKQAVgLAUAFQATAFANASQAMAQAEAWQADARgBAYIgCApQgBAeAEAqIAIBIQAGAtgHAaQgFASgLAOQgMAPgQAGQgJAEgKAAQgNAAgOgHgAR8zAQgcgQgIglQgGgXADgqQAGhSANg4QARhJAig2QASgcAXgPQAbgSAaAJQATAGAMAUQAKASABAXQABASgGAXIgMApQgQA2gFAvQgDA8gFAdQgJA1gdAbQgRAQgXAFQgHACgIAAQgPAAgNgHgANS0KQgdgQgOgdQgTgrAOhPQANhCAnh8QAPg0ARgYQANgSARgKQATgMATAAQAdgBAYAZQAWAWAGAfQAFAbgGAhQgDAUgLAoIg4DAQgJAhgKASQgPAagWAKQgLAGgNAAQgQAAgSgJgEgqCgVEQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgAJG2NQgpgbALhaIAQiVQAEgjADgRQAGgcAKgVQANgZAUgRQAWgRAZgCQAdgDAZAVQAYAUADAeQACAPgFAUIgKAjQgIAhgGA9QgGBEgGAbQgMA8gdAbQgTARgZAFIgOABQgSAAgOgJgEgj3gWlQgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKgA9423QgfgOgbgjQgcglgnhfQglhaghgoIgrgwQgWgdACgbQACggAhgVQAegTAkACQAlACAlAUQAhASAcAfQAoAsAoBaQAoBaALBAQAIAngHAgQgIAmgcAQQgQAJgUAAQgTAAgTgIgAg747QgJgfAAg8IgBjNQgBg1AKgbQAHgUAOgOQAQgPATgDQAXgDAWAPQAUAOAKAXQAJAUADAbQACARgBAgIgBCwQgBAlgBARQgDAegJAXQgKAbgTATQgVAUgYAEQgngTgPgzgAFD4BQgQgKgJgRQgOgaABgoIAGhFIACg3QABghAEgWQALhBAqgcQAagRAYAIQAOAEAKANQAJAMAEAQQADANAAASIgBAeQAABRgCAoQgEBFgYAsQgWAngfADIgFABQgPAAgOgJgAlg4MQgdgOgLgnQgIgcABgtIACknQAAgmAGgXQAIghAVgRQAQgNAXgDQAVgCAUAJQAnASAQAsQAJAaADAyQAFBYABAtQABBKgGA6QgIBIgcAkQgSAYgdAJQgNAEgMAAQgQAAgOgHgA4z4XQgogKgjgrQgjgsgOg1QgCgJgIguQgGgigJgUQgGgOgMgVIgUghQgVgtAQgeQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAeAPAuQAIAbALA5IAQBRQAGAgAAAUQAAAdgOATQgNASgYAHQgNAEgMAAQgLAAgLgDgAug5qQgfgNgSgdQgPgZgHgjQgEgXgCgqIgHiEQgCgvAFgUQAJgnAcgQQAZgOAgALQAeALARAbQAPAWAIAhQAEAWAEAnIAMCEIADAmQAAAVgDARQgJAsggARQgOAHgQAAQgPAAgRgGgApb6LQgRgHgMgOQgQgTgFgfQgEgVABgkIABi1QAAgbABgNQACgXAHgRQAIgUAPgOQAQgOATgCQAYgCAWARQAUARAIAZQAIAVABAdIAAAyIAFBsQACA/gMAqQgGAbgOASQgQAWgXAFIgLACQgMAAgMgFgA0L6RQgUgFgQgPQgZgVgXgvQgVgpgNgkQgQgsgKg0QgLhAAXgeQAPgSAbgFQAZgEAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAgABAZQACAggOAbQgPAegcAIQgJADgKAAQgKAAgMgEg");
    var mask_graphics_105 = new cjs.Graphics().p("EAybAg/QgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHdpQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjd3QgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYdvQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVudjQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpdUQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8dRQgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQACQgQAAgQgFgEAinAdFQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjcjQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAcIQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NZZQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rQyQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92K7QghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AGEQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgUATgLQA3ggAvAKQAaAGAUATQAUAUABAaQABAVgNAXQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldAENQglgJgTgeQgZgmANg1QAJglAhgvQAthABEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8AzgnAoQg+BAgiA5IgdAwQgSAbgUANQgVANgYAAQgLAAgMgDgEgp9AAGQgWgGgQgPQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAHQgLADgLAAQgLAAgLgDgEgvLgCXQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgHDQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gMoQgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgAfbwZQgWgLgOgWQgMgSgGgaQgEgSgDgeQgNhogEhHQgGhfAGhQQADgwANgZQAJgSAQgLQARgLASAAQATABAQAOQAQANAHAUQAHAQACAXIACAoQACBGAJBhIARCoQAFAzgHAZQgFATgMAPQgNAQgSAFQgIACgIAAQgOAAgPgHgEAjtgRIQgYgIgQgTQgNgQgJgZQgGgQgHgfQgliwgMi8QgCgqALgTQAMgUAYgGQAYgHAWAKQAeANARAlQAMAaAIAuQAUBxAYDCQAHA1gFAbQgIAlgbAOQgMAGgMAAQgKAAgLgDgEgufgRbIgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNAEgNAAQgcAAgcgQgAWoxUQgYgIgTgiQgcg3AAhRQAAgPAKiCIAFg5QAGggAMgWQAOgaAcgNQAcgOAZAMQAsAVgBBUQAAAzgFAvIgFAtQgCAZAAATIAEA9QACAkgGAYQgIAfgZAUQgTAOgTAAQgIAAgJgDgAa3yGQgYgKgOgVQgNgSgHgbQgFgQgFggQgJg3AAgbQAAgXAKg+QAFgdAEgNQAGgYALgQQAMgTAUgKQAVgLAUAFQATAFANASQAMAQAEAWQADARgBAYIgCApQgBAeAEAqIAIBIQAGAtgHAaQgFASgLAOQgMAPgQAGQgJAEgKAAQgNAAgOgHgAR8zAQgcgQgIglQgGgXADgqQAGhSANg4QARhJAig2QASgcAXgPQAbgSAaAJQATAGAMAUQAKASABAXQABASgGAXIgMApQgQA2gFAvQgDA8gFAdQgJA1gdAbQgRAQgXAFQgHACgIAAQgPAAgNgHgEAoAgT6Qg2gmgthXIhLiTIgdgtQgRgbgHgVQgJgbAFgaQAEgdAUgRQARgOAagCQAXgBAXAKQAiARAjAuQArA5AvBkQAhBKAXBBQAQArgCAbQgBATgJAPQgKARgQAHQgKAEgLAAQgYAAgegUgANS0KQgdgQgOgdQgTgrAOhPQANhCAnh8QAPg0ARgYQANgSARgKQATgMATAAQAdgBAYAZQAWAWAGAfQAFAbgGAhQgDAUgLAoIg4DAQgJAhgKASQgPAagWAKQgLAGgNAAQgQAAgSgJgEgqCgVEQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgAJG2NQgpgbALhaIAQiVQAEgjADgRQAGgcAKgVQANgZAUgRQAWgRAZgCQAdgDAZAVQAYAUADAeQACAPgFAUIgKAjQgIAhgGA9QgGBEgGAbQgMA8gdAbQgTARgZAFIgOABQgSAAgOgJgEgj3gWlQgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKgA9423QgfgOgbgjQgcglgnhfQglhaghgoIgrgwQgWgdACgbQACggAhgVQAegTAkACQAlACAlAUQAhASAcAfQAoAsAoBaQAoBaALBAQAIAngHAgQgIAmgcAQQgQAJgUAAQgTAAgTgIgAg747QgJgfAAg8IgBjNQgBg1AKgbQAHgUAOgOQAQgPATgDQAXgDAWAPQAUAOAKAXQAJAUADAbQACARgBAgIgBCwQgBAlgBARQgDAegJAXQgKAbgTATQgVAUgYAEQgngTgPgzgAFD4BQgQgKgJgRQgOgaABgoIAGhFIACg3QABghAEgWQALhBAqgcQAagRAYAIQAOAEAKANQAJAMAEAQQADANAAASIgBAeQAABRgCAoQgEBFgYAsQgWAngfADIgFABQgPAAgOgJgAlg4MQgdgOgLgnQgIgcABgtIACknQAAgmAGgXQAIghAVgRQAQgNAXgDQAVgCAUAJQAnASAQAsQAJAaADAyQAFBYABAtQABBKgGA6QgIBIgcAkQgSAYgdAJQgNAEgMAAQgQAAgOgHgA4z4XQgogKgjgrQgjgsgOg1QgCgJgIguQgGgigJgUQgGgOgMgVIgUghQgVgtAQgeQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAeAPAuQAIAbALA5IAQBRQAGAgAAAUQAAAdgOATQgNASgYAHQgNAEgMAAQgLAAgLgDgAug5qQgfgNgSgdQgPgZgHgjQgEgXgCgqIgHiEQgCgvAFgUQAJgnAcgQQAZgOAgALQAeALARAbQAPAWAIAhQAEAWAEAnIAMCEIADAmQAAAVgDARQgJAsggARQgOAHgQAAQgPAAgRgGgApb6LQgRgHgMgOQgQgTgFgfQgEgVABgkIABi1QAAgbABgNQACgXAHgRQAIgUAPgOQAQgOATgCQAYgCAWARQAUARAIAZQAIAVABAdIAAAyIAFBsQACA/gMAqQgGAbgOASQgQAWgXAFIgLACQgMAAgMgFgA0L6RQgUgFgQgPQgZgVgXgvQgVgpgNgkQgQgsgKg0QgLhAAXgeQAPgSAbgFQAZgEAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAgABAZQACAggOAbQgPAegcAIQgJADgKAAQgKAAgMgEg");
    var mask_graphics_108 = new cjs.Graphics().p("EAybAg/QgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHdpQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjd3QgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYdvQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVudjQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpdUQgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8dRQgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQACQgQAAgQgFgEAinAdFQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjcjQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAcIQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NZZQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rQyQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92K7QghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AGEQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgUATgLQA3ggAvAKQAaAGAUATQAUAUABAaQABAVgNAXQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldAENQglgJgTgeQgZgmANg1QAJglAhgvQAthABEhEQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8AzgnAoQg+BAgiA5IgdAwQgSAbgUANQgVANgYAAQgLAAgMgDgEgp9AAGQgWgGgQgPQgPgPgHgWQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAtgHAJQgXAcgaAHQgLADgLAAQgLAAgLgDgEgvLgCXQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgHDQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gMoQgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgAfbwZQgWgLgOgWQgMgSgGgaQgEgSgDgeQgNhogEhHQgGhfAGhQQADgwANgZQAJgSAQgLQARgLASAAQATABAQAOQAQANAHAUQAHAQACAXIACAoQACBGAJBhIARCoQAFAzgHAZQgFATgMAPQgNAQgSAFQgIACgIAAQgOAAgPgHgEAjtgRIQgYgIgQgTQgNgQgJgZQgGgQgHgfQgliwgMi8QgCgqALgTQAMgUAYgGQAYgHAWAKQAeANARAlQAMAaAIAuQAUBxAYDCQAHA1gFAbQgIAlgbAOQgMAGgMAAQgKAAgLgDgEgufgRbIgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNAEgNAAQgcAAgcgQgAWoxUQgYgIgTgiQgcg3AAhRQAAgPAKiCIAFg5QAGggAMgWQAOgaAcgNQAcgOAZAMQAsAVgBBUQAAAzgFAvIgFAtQgCAZAAATIAEA9QACAkgGAYQgIAfgZAUQgTAOgTAAQgIAAgJgDgAa3yGQgYgKgOgVQgNgSgHgbQgFgQgFggQgJg3AAgbQAAgXAKg+QAFgdAEgNQAGgYALgQQAMgTAUgKQAVgLAUAFQATAFANASQAMAQAEAWQADARgBAYIgCApQgBAeAEAqIAIBIQAGAtgHAaQgFASgLAOQgMAPgQAGQgJAEgKAAQgNAAgOgHgAR8zAQgcgQgIglQgGgXADgqQAGhSANg4QARhJAig2QASgcAXgPQAbgSAaAJQATAGAMAUQAKASABAXQABASgGAXIgMApQgQA2gFAvQgDA8gFAdQgJA1gdAbQgRAQgXAFQgHACgIAAQgPAAgNgHgEAoAgT6Qg2gmgthXIhLiTIgdgtQgRgbgHgVQgJgbAFgaQAEgdAUgRQARgOAagCQAXgBAXAKQAiARAjAuQArA5AvBkQAhBKAXBBQAQArgCAbQgBATgJAPQgKARgQAHQgKAEgLAAQgYAAgegUgANS0KQgdgQgOgdQgTgrAOhPQANhCAnh8QAPg0ARgYQANgSARgKQATgMATAAQAdgBAYAZQAWAWAGAfQAFAbgGAhQgDAUgLAoIg4DAQgJAhgKASQgPAagWAKQgLAGgNAAQgQAAgSgJgEgqCgVEQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgAJG2NQgpgbALhaIAQiVQAEgjADgRQAGgcAKgVQANgZAUgRQAWgRAZgCQAdgDAZAVQAYAUADAeQACAPgFAUIgKAjQgIAhgGA9QgGBEgGAbQgMA8gdAbQgTARgZAFIgOABQgSAAgOgJgEgj3gWlQgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKgEAsogWuQgdgIgegXQiNhphOjbQgWg8AFgjQADgZAPgUQAQgWAWgFQATgFAVAIQATAHAQAPQANANAMATQAHALANAZQBEB+BZB2QAWAeAHAMQAPAYAGAVQAHAagHAZQgHAbgVANQgQAKgUAAQgLAAgNgDgA9423QgfgOgbgjQgcglgnhfQglhaghgoIgrgwQgWgdACgbQACggAhgVQAegTAkACQAlACAlAUQAhASAcAfQAoAsAoBaQAoBaALBAQAIAngHAgQgIAmgcAQQgQAJgUAAQgTAAgTgIgAg747QgJgfAAg8IgBjNQgBg1AKgbQAHgUAOgOQAQgPATgDQAXgDAWAPQAUAOAKAXQAJAUADAbQACARgBAgIgBCwQgBAlgBARQgDAegJAXQgKAbgTATQgVAUgYAEQgngTgPgzgAFD4BQgQgKgJgRQgOgaABgoIAGhFIACg3QABghAEgWQALhBAqgcQAagRAYAIQAOAEAKANQAJAMAEAQQADANAAASIgBAeQAABRgCAoQgEBFgYAsQgWAngfADIgFABQgPAAgOgJgAlg4MQgdgOgLgnQgIgcABgtIACknQAAgmAGgXQAIghAVgRQAQgNAXgDQAVgCAUAJQAnASAQAsQAJAaADAyQAFBYABAtQABBKgGA6QgIBIgcAkQgSAYgdAJQgNAEgMAAQgQAAgOgHgA4z4XQgogKgjgrQgjgsgOg1QgCgJgIguQgGgigJgUQgGgOgMgVIgUghQgVgtAQgeQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAeAPAuQAIAbALA5IAQBRQAGAgAAAUQAAAdgOATQgNASgYAHQgNAEgMAAQgLAAgLgDgAug5qQgfgNgSgdQgPgZgHgjQgEgXgCgqIgHiEQgCgvAFgUQAJgnAcgQQAZgOAgALQAeALARAbQAPAWAIAhQAEAWAEAnIAMCEIADAmQAAAVgDARQgJAsggARQgOAHgQAAQgPAAgRgGgApb6LQgRgHgMgOQgQgTgFgfQgEgVABgkIABi1QAAgbABgNQACgXAHgRQAIgUAPgOQAQgOATgCQAYgCAWARQAUARAIAZQAIAVABAdIAAAyIAFBsQACA/gMAqQgGAbgOASQgQAWgXAFIgLACQgMAAgMgFgA0L6RQgUgFgQgPQgZgVgXgvQgVgpgNgkQgQgsgKg0QgLhAAXgeQAPgSAbgFQAZgEAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAgABAZQACAggOAbQgPAegcAIQgJADgKAAQgKAAgMgEg");
    var mask_graphics_111 = new cjs.Graphics().p("EAybAhnQgvgNgoguQgggkgZg3QgRgigXhDQiNmThym9QgNg0AEgbQAGgkAegYQAegZAkABQAdAAAcASQAaAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAxB5IAUAyQAJAcACAXQADAdgHAZQgIAbgTARQgSASgcAFQgLACgKAAQgQAAgQgEgAHHeRQgggcgJgpQgEgPgBgUIgBgkQgBgdgGg6IgGg2QgHg6gWhqQgXhxgHgzQgFgggDgpQgDgoAEgWQAGgcASgWQAUgWAbgEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAIAjIAyDsQASBVADAsIAEAvIAGAYQAEAOABAJQACAOgBAaQgBAbACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgcgABjefQgcgCgXgOQgkgUgMgjQgEgOgBgWIAAgmQgBgPgEgbIgGgrQgCgaADhIQAEhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAGgSQALgcAcgOQAegOAcAJQAiAKAXAnQAOAYANAxQALApADAaIAEAnIADAnQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAlIAFAlQACAXACAuIADAfQABATgCANQgEAagSAWQgSAVgZAKQgTAHgUAAIgNgBgAcYeXQgmgSgUgvQgHgTgGgcIgIgxQgIgrgdhPIgohyQgYhIgkiNIgXhXQgShDAagaQANgNATgCQASgCASAHQAbALAaAlQAeApAYBAQAOAlAZBMQAMAiAnBgQAhBRAPAyQAMAlASBMQAMA2gDAdQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgAVueLQgWgFgQgNQgXgSgNgjQgIgXgIgrIgLhEIgLg+QgFgXgzi5Qgih9AAhUQAAgmAJgTQAIgRARgLQAQgLASgCQATgBASAIQARAIALAPQALAOAJAgIBRESIAaBbQARA/AZB3QAHAjACARQABAegIAVQgIAUgQAOQgQAPgUAGQgLADgMAAQgJAAgKgCgAOpd8QgngMgPgiQgIgRgDgkQgJhQgKg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gIg8QgLhSAhgiQAMgMARgGQARgFARADQAkAGAXAmQAPAWAOAxIBJD3IATBNQAJAhAHAmIAVCFQADAXAAANQABAUgGAOQgMAggmAPQgWAJgVAAQgOAAgOgEgAs8d5QgXgHgSgSQgTgRgJgXQgLgdAFgpQACgUANg0QAWhaAQh7QANhsAHiJQAEhMAFiqQABgnAJgRQAJgTAVgLQAVgLAWADQAVACATANQASANALATQAMATAFAdQADAQACAjIAMDtQAGBjgEA4QgBATgHBBIgYDMQgGA6gMAiQgRAxgjAYQgUANgZAEIgQABQgQAAgQgEgEAinAdtQgYgLgPgVQgLgPgLggIh6lHQgdhPgOgsQgWhFgJg5QgFghAEgSQAFgcAYgUQAYgTAdgBQAcgCAaARQAZAPANAaQAGAMAFASIAHAeQAMAsAfBAQArBaAGARQANAfAVBFQAVBCAOAiIAVAwQAMAdAGAUQARA8gWArQgMAWgWAPQgWAOgZABIgHABQgVAAgUgKgAmjdLQgggXgLgiQgIgVAAgeIACg1QAEg6gGhtQgHhyACg1IACgxQABgdgBgUIgFgvQgEgcACgSQACgNAEgSIAIgfIAMgvQAJgaAOgQQAMgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaABA2QACAgAIA/QACAbgBA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgBAWgGARQgLAiggAWQgfAXgkAAIgBAAQgjAAgfgVgEAqLAcwQgbgKgZgbQgigkgjhCQh8jlg1j9QgOg/AIgkQAGgZAQgTQASgUAYgFQAVgEAXALQAUAJAPATQANAPALAXQAGAOAKAdIA0CSQAbBMAOAjQAYA9AYAvIAyBgQAbA5AGAtQAFAjgIAbQgLAigbANQgOAIgRAAQgPAAgQgHgA2NaBQgYgEgVgOQgVgOgNgWQgPgZgBgiQgCgfAMgeQALgdAmg2QCRjMBPidIAnhOQAXgtAUgeQAXgkAXgLQAVgKAYADQAXAEASAOQARAOAKAWQAJAVABAXQAAAYgNAsQgkB5hAB2QgcAzhIBxQhDBpgfA9IgQAfQgKARgLALQgRASgZAIQgRAGgRAAIgPgBgA5rRaQgfgBgbgVQgbgVgKgfQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgaASgNQAcgVAsgNQAYgGAPABQASACAQAMQAPALAKASQAMAXgBAVQgCARgMATQgHAMgRASQg8BEgVAgQgNAWgIAKQgTAbgvAoIhKBAQgmAggYAOQgjATggAAIgFAAgA92LjQghgJgVgZQgVgZgDgjQgCgjARgcQARgaAtgeQAlgZBTgyICbhfQAcgRARgIQAagNAXgEQAbgEAZAHQAbAIAOAUQAQAWgCAeQgCAdgQAZQgNAVgZAUQgOAMggAVIi4B8QgeATgUAQIgjAdQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AGsQgXgLgPgTQgPgTgEgZQgEgZAJgXQALgfAigbQAPgNAxgdQBMgtA/g4IAogjQAXgUATgMQA3ggAvAKQAaAGAUAUQAUAUABAaQABAVgNAXQgJAQgTAWQg2A+hLA8Qg6AwhWA5QgbASgOAHQgYAMgVACIgKAAQgTAAgTgIgEgldAE1QglgJgTgeQgZgmANg1QAJglAhgvQAthBBEhDQAwgxBShFQA3gwAlgWQAfgSAYgFQAXgFAXAFQAYAFARAPQATAPAHAXQAHAXgHAWQgFAPgNAPQgJAKgRAPIhnBZQg8AzgnAoQg+BAgiA5IgdAwQgSAbgUANQgVANgYAAQgLAAgMgDgEgp9AAuQgWgGgQgQQgPgPgHgVQgHgWADgWQADgcAXgnQAWgjAhgpIA+hHQAkgoAUgTQAsgjAVgTIAdgcQASgQANgJQApgbAjAFQAjAGASAdQATAdgKAhQgFATgWAbQgcAkhhBhQhRBRgkA6QgdAsgHAJQgXAcgaAIQgLADgLAAQgLAAgLgDgEgvLgBvQgbgTgDghQgBgRAKgdQAQgxAXgoQAOgYAagjQBBhcAignQAignAdgYQAdgYAfgKQAkgNAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUATQgaAYggArIg0BIIgbAkQgRAVgJAPQgMATgXAoQgVAigYAPQgYAQgbAAIgFAAQgeAAgXgRgEgzNgGbQgXgHgOgWQgOgWABgZQAAgtApgpQAXgXA5ghIBzhDQAugaAcgGQAVgDATAFQAVAGAMAPQAVAZgJAmQgHAcgcAdQgTAVgdAVQgRANglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gMAQgdgSgHgdQgKgqAjgwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATAAAUQACAigdAcQgbAagnAIQgMACgrAEQgjACgTAIQgNAEghATQgcAQgSAEQgIACgIAAQgWAAgWgMgAfbvxQgWgLgOgWQgMgSgGgaQgEgSgDgeQgNhogEhHQgGhfAGhQQADgwANgZQAJgSAQgLQARgLASAAQATABAQAOQAQANAHAUQAHAQACAXIACAoQACBGAJBhIARCoQAFAzgHAZQgFATgMAPQgNAQgSAFQgIACgIAAQgOAAgPgHgEAjtgQgQgYgIgQgTQgNgQgJgZQgGgQgHgfQgliwgMi8QgCgqALgTQAMgUAYgGQAYgHAWAKQAeANARAlQAMAaAIAuQAUBxAYDCQAHA1gFAbQgIAlgbAOQgMAGgMAAQgKAAgLgDgEgufgQzIgXgPQgNgJgJgEQgWgJggABIgjADQgWADgNAAQgqgBgWgVQgTgSgBgdQgBgcAOgXQAZgnA9gVQBJgZBLASQBOATAsA5QASAXAHAYQAIAbgHAXQgGAUgRAQQgQAPgVAFQgNAEgNAAQgcAAgcgQgAWowsQgYgIgTgiQgcg3AAhRQAAgPAKiCIAFg5QAGggAMgWQAOgaAcgNQAcgOAZAMQAsAVgBBUQAAAzgFAvIgFAtQgCAZAAATIAEA9QACAkgGAYQgIAfgZAUQgTAOgTAAQgIAAgJgDgAa3xeQgYgKgOgVQgNgSgHgbQgFgQgFggQgJg3AAgbQAAgXAKg+QAFgdAEgNQAGgYALgQQAMgTAUgKQAVgLAUAFQATAFANASQAMAQAEAWQADARgBAYIgCApQgBAeAEAqIAIBIQAGAtgHAaQgFASgLAOQgMAPgQAGQgJAEgKAAQgNAAgOgHgAR8yYQgcgQgIglQgGgXADgqQAGhSANg4QARhJAig2QASgcAXgPQAbgSAaAJQATAGAMAUQAKASABAXQABASgGAXIgMApQgQA2gFAvQgDA8gFAdQgJA1gdAbQgRAQgXAFQgHABgIAAQgPAAgNgGgEAoAgTSQg2gmgthXIhLiTIgdgtQgRgbgHgVQgJgbAFgaQAEgdAUgRQARgOAagCQAXgBAXAKQAiARAjAuQArA5AvBkQAhBKAXBBQAQArgCAbQgBATgJAPQgKARgQAHQgKAEgLAAQgYAAgegUgANSziQgdgQgOgdQgTgrAOhPQANhCAnh8QAPg0ARgYQANgSARgKQATgMATAAQAdgBAYAZQAWAWAGAfQAFAbgGAhQgDAUgLAoIg4DAQgJAhgKASQgPAagWAKQgLAGgNAAQgQAAgSgJgEgqCgUcQgagJghgZIg2gpQgPgKgggRQgbgPgPgQQgeggAAgpQABgUAJgSQAJgTARgLQAfgWA4AJQBEAKA6AmQA7AnAjA6QAbAtgFAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgAJG1lQgpgbALhaIAQiVQAEgjADgRQAGgcAKgVQANgZAUgRQAWgRAZgCQAdgDAZAVQAYAUADAeQACAPgFAUIgKAjQgIAhgGA9QgGBEgGAbQgMA8gdAbQgTARgZAFIgOABQgSAAgOgJgEgj3gV9QgagNgYgcQgRgUgthHQgmg6gfgbIgwgsQgYgbADgbQAEgkAxgbQA5geAsAMQAWAHAVASQAPANAUAZQBbBvAaBaQAOAugHAgQgEAUgMAQQgNARgSAHQgKAEgLAAQgSAAgUgKgEAsogWGQgdgIgegXQiNhphOjbQgWg8AFgjQADgZAPgUQAQgWAWgFQATgFAVAIQATAHAQAPQANANAMATQAHALANAZQBEB+BZB2QAWAeAHAMQAPAYAGAVQAHAagHAZQgHAbgVANQgQAKgUAAQgLAAgNgDgA942PQgfgOgbgjQgcglgnhfQglhaghgoIgrgwQgWgdACgbQACggAhgVQAegTAkACQAlACAlAUQAhASAcAfQAoAsAoBaQAoBaALBAQAIAngHAgQgIAmgcAQQgQAJgUAAQgTAAgTgIgAg74TQgJgfAAg8IgBjNQgBg1AKgbQAHgUAOgOQAQgPATgDQAXgDAWAPQAUAOAKAXQAJAUADAbQACARgBAgIgBCwQgBAlgBARQgDAegJAXQgKAbgTATQgVAUgYAEQgngTgPgzgAFD3ZQgQgKgJgRQgOgaABgoIAGhFIACg3QABghAEgWQALhBAqgcQAagRAYAIQAOAEAKANQAJAMAEAQQADANAAASIgBAeQAABRgCAoQgEBFgYAsQgWAngfADIgFABQgPAAgOgJgAlg3kQgdgOgLgnQgIgcABgtIACknQAAgmAGgXQAIghAVgRQAQgNAXgDQAVgCAUAJQAnASAQAsQAJAaADAyQAFBYABAtQABBKgGA6QgIBIgcAkQgSAYgdAJQgNAEgMAAQgQAAgOgHgA4z3vQgogKgjgrQgjgsgOg1QgCgJgIguQgGgigJgUQgGgOgMgVIgUghQgVgtAQgeQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAeAPAuQAIAbALA5IAQBRQAGAgAAAUQAAAdgOATQgNASgYAHQgNAEgMAAQgLAAgLgDgAug5CQgfgNgSgdQgPgZgHgjQgEgXgCgqIgHiEQgCgvAFgUQAJgnAcgQQAZgOAgALQAeALARAbQAPAWAIAhQAEAWAEAnIAMCEIADAmQAAAVgDARQgJAsggARQgOAHgQAAQgPAAgRgGgApb5jQgRgHgMgOQgQgTgFgfQgEgVABgkIABi1QAAgbABgNQACgXAHgRQAIgUAPgOQAQgOATgCQAYgCAWARQAUARAIAZQAIAVABAdIAAAyIAFBsQACA/gMAqQgGAbgOASQgQAWgXAFIgLABQgMAAgMgEgA0L5pQgUgFgQgPQgZgVgXgvQgVgpgNgkQgQgsgKg0QgLhAAXgeQAPgSAbgFQAZgEAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAgABAZQACAggOAbQgPAegcAIQgJADgKAAQgKAAgMgEgEAuxgccQgxgHg0goQgTgOhChBIgjgiQgUgVgKgSQgNgXgDgYQgCgaALgVQAPgbAigKQAhgJAhAKQAcAJAdAXQARANAfAfIBfBdQAaAaALATQARAbgDAaQgCARgMAOQgKAOgQAIQgVALgaAAQgKAAgLgCg");
    var mask_graphics_114 = new cjs.Graphics().p("EAybAj9QgvgNgoguQgggkgZg2QgRgjgXhDQiNmShym9QgNg1AEgbQAGgjAegZQAegZAkABQAdABAcASQAaAQATAaQAQAWANAgIAUA7IBkE4QA8C7AgBdQA1CbAxB6IAUAyQAJAcACAXQADAcgHAZQgIAcgTARQgSARgcAGQgLACgKAAQgQAAgQgFgEAHHAgoQgggdgJgpQgEgOgBgVIgBgjQgBgegGg6IgGg1QgHg7gWhqQgXhxgHgzQgFgggDgpQgDgnAEgWQAGgdASgVQAUgXAbgEQAZgDAZAOQAXANAPAXQANATAJAcQAFARAIAjIAyDrQASBVADAsIAEAwIAGAXQAEAPABAJQACANgBAbQgBAaACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFABQglAAgfgcgEABjAg2QgcgDgXgNQgkgUgMgjQgEgPgBgWIAAglQgBgQgEgbIgGgrQgCgZADhIQAEhJgIgxIgLg7QgFgegBhGIgBiHQAAgnAGgSQALgcAcgPQAegOAcAJQAiALAXAnQAOAYANAwQALAqADAaIAEAnIADAmQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAmIAFAlQACAXACAtIADAgQABASgCANQgEAbgSAVQgSAVgZAKQgTAIgUAAIgNgBgEAcYAgtQgmgSgUgvQgHgSgGgdIgIgwQgIgrgdhQIgohyQgYhIgkiMIgXhXQgShDAagbQANgMATgCQASgDASAHQAbAMAaAkQAeApAYBBQAOAlAZBMQAMAhAnBhQAhBRAPAyQAMAlASBMQAMA2gDAcQgDAxghAXQgSANgXABIgEABQgVAAgTgKgEAVuAghQgWgEgQgNQgXgTgNgjQgIgWgIgrIgLhEIgLg/QgFgWgzi5Qgih9AAhVQAAglAJgUQAIgQARgLQAQgLASgCQATgCASAIQARAIALAPQALAPAJAfIBRESIAaBcQARA+AZB4QAHAiACASQABAdgIAWQgIATgQAPQgQAPgUAFQgLADgMAAQgJAAgKgCgEAOpAgSQgngLgPgjQgIgQgDglQgJhPgKg/QgMhHgOgzIgXhKQgQgxgHgaQgPg5gIg7QgLhSAhgjQAMgMARgFQARgGARADQAkAHAXAlQAPAXAOAwIBJD4IATBNQAJAgAHAnIAVCEQADAYAAAMQABAUgGAPQgMAfgmAQQgWAJgVAAQgOAAgOgFgEgM8AgPQgXgHgSgRQgTgRgJgXQgLgeAFgpQACgUANgzQAWhaAQh7QANhsAHiJQAEhMAFiqQABgoAJgRQAJgTAVgLQAVgKAWACQAVACATAOQASAMALATQAMAUAFAcQADARACAjIAMDsQAGBkgEA4QgBASgHBCIgYDLQgGA7gMAiQgRAxgjAXQgUAOgZAEIgQABQgQAAgQgFgEAinAgDQgYgLgPgUQgLgQgLgfIh6lIQgdhOgOgtQgWhEgJg6QgFghAEgSQAFgcAYgTQAYgUAdgBQAcgBAaAQQAZAQANAaQAGAMAFARIAHAeQAMAsAfBBQArBaAGAQQANAgAVBFQAVBCAOAiIAVAwQAMAcAGAVQARA7gWArQgMAXgWAOQgWAOgZACIgHAAQgVAAgUgKgAmjfhQgggWgLgiQgIgWAAgeIACg1QAEg5gGhuQgHhyACg1IACgxQABgdgBgUIgFguQgEgcACgTQACgNAEgSIAIgeIAMgwQAJgaAOgQQAMgOASgJQASgIATAAQApABAbAfQAWAZAFAuQADAaABA1QACAgAIBAQACAagBA2QgBA1ACAbIAJBOQAJBHABCRIgBAoQgBAWgGAQQgLAiggAXQgfAWgkABIgBAAQgjAAgfgWgEAqLAfHQgbgLgZgaQgiglgjhBQh8jlg1j9QgOhAAIgjQAGgZAQgTQASgVAYgEQAVgEAXAKQAUAKAPASQANAQALAXQAGAOAKAcIA0CTQAbBLAOAkQAYA9AYAuIAyBhQAbA4AGAtQAFAkgIAaQgLAjgbANQgOAHgRAAQgPAAgQgGgA2NcXQgYgEgVgOQgVgOgNgVQgPgagBghQgCgfAMgfQALgcAmg3QCRjMBPicIAnhOQAXgtAUgfQAXgjAXgMQAVgKAYAEQAXADASAPQARAOAKAWQAJAUABAYQAAAXgNAsQgkB6hAB1QgcA0hIBxQhDBpgfA9IgQAfQgKARgLALQgRARgZAJQgRAGgRAAIgPgCgA5rTxQgfgCgbgVQgbgVgKgeQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgbASgNQAcgUAsgNQAYgHAPABQASACAQAMQAPAMAKARQAMAXgBAWQgCARgMASQgHAMgRATQg8BDgVAhQgNAWgIAKQgTAbgvAoIhKA/QgmAggYAOQgjAUggAAIgFAAgA92N6QghgJgVgZQgVgagDgjQgCgjARgcQARgaAtgeQAlgYBTgzICbheQAcgSARgIQAagMAXgEQAbgFAZAHQAbAIAOAUQAQAWgCAfQgCAcgQAZQgNAVgZAUQgOAMggAWIi4B7QgeAUgUAPIgjAeQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgg2AJCQgXgKgPgTQgPgUgEgZQgEgZAJgXQALgfAigbQAPgMAxgeQBMgtA/g4IAogjQAXgTATgMQA3ggAvAKQAaAFAUAUQAUAVABAZQABAWgNAXQgJAQgTAWQg2A9hLA9Qg6AvhWA6QgbASgOAHQgYAMgVABIgKABQgTAAgTgJgEgldAHLQglgJgTgdQgZgmANg1QAJglAhgwQAthBBEhEQAwgwBShEQA3gwAlgWQAfgSAYgGQAXgFAXAFQAYAGARAOQATAPAHAXQAHAYgHAVQgFAPgNAPQgJAKgRAPIhnBYQg8A1gnAoQg+A/giA5IgdAxQgSAbgUAMQgVANgYAAQgLAAgMgDgEgp9ADFQgWgGgQgQQgPgQgHgWQgHgVADgXQADgcAXgmQAWgjAhgoIA+hHQAkgoAUgTQAsgkAVgTIAdgbQASgRANgJQApgbAjAGQAjAFASAdQATAdgKAhQgFATgWAcQgcAjhhBiQhRBQgkA5QgdAugHAIQgXAcgaAIQgLAEgLAAQgLAAgLgDgEgvLAAnQgbgUgDgfQgBgSAKgdQAQgwAXgoQAOgZAagjQBBhbAigoQAignAdgXQAdgYAfgLQAkgMAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUASQgaAZggArIg0BIIgbAjQgRAVgJAPQgMATgXApQgVAggYAQQgYAPgbABIgFAAQgeAAgXgRgEgzNgEEQgXgHgOgXQgOgVABgZQAAguApgoQAXgXA5ghIBzhDQAugbAcgFQAVgEATAGQAVAGAMAOQAVAagJAmQgHAbgcAeQgTAVgdAVQgRAMglAXIhrBEQgeATgTAHQgRAGgPAAQgKAAgKgDgEgz3gJqQgdgRgHgeQgKgpAjgwQAog4BFgbQBCgbBHAIQAwAFAeAUQASAMALASQAMASAAAUQACAjgdAcQgbAagnAHQgMADgrADQgjADgTAHQgNAFghATQgcAQgSAEQgIACgIAAQgWAAgWgNgAfbtbQgWgLgOgVQgMgTgGgaQgEgRgDgfQgNhngEhHQgGhgAGhQQADgwANgZQAJgRAQgLQARgMASABQATAAAQAPQAQANAHATQAHARACAWIACAoQACBGAJBiIARCnQAFA0gHAYQgFAUgMAOQgNAQgSAFQgIADgIAAQgOAAgPgIgEAjtgOKQgYgHgQgTQgNgRgJgZQgGgPgHgfQglixgMi8QgCgqALgTQAMgTAYgHQAYgGAWAJQAeANARAmQAMAaAIAtQAUBxAYDCQAHA2gFAaQgIAmgbAOQgMAGgMAAQgKAAgLgEgEgufgOdIgXgOQgNgJgJgEQgWgJggABIgjADQgWACgNAAQgqgBgWgVQgTgSgBgdQgBgbAOgYQAZgnA9gVQBJgYBLASQBOASAsA6QASAWAHAYQAIAbgHAYQgGAUgRAPQgQAPgVAGQgNADgNAAQgcAAgcgQgAWouVQgYgIgTgjQgcg2AAhSQAAgOAKiCIAFg6QAGggAMgWQAOgaAcgNQAcgNAZAMQAsAVgBBTQAAA0gFAvIgFAsQgCAaAAASIAEA9QACAkgGAYQgIAfgZAUQgTAPgTAAQgIAAgJgDgAa3vHQgYgKgOgWQgNgSgHgaQgFgQgFggQgJg3AAgcQAAgWAKg+QAFgdAEgOQAGgXALgQQAMgUAUgKQAVgKAUAFQATAFANASQAMAQAEAWQADARgBAYIgCApQgBAeAEAqIAIBHQAGAtgHAaQgFATgLANQgMAQgQAGQgJADgKAAQgNAAgOgGgAR8wCQgcgPgIglQgGgXADgrQAGhRANg4QARhKAig1QASgcAXgPQAbgSAaAIQATAGAMAUQAKATABAWQABATgGAXIgMApQgQA1gFAvQgDA8gFAeQgJA1gdAbQgRAQgXAEQgHACgIAAQgPAAgNgHgEAoAgQ8Qg2gmgthWIhLiUIgdgtQgRgagHgVQgJgbAFgbQAEgdAUgQQARgPAagBQAXgBAXAKQAiAQAjAuQArA5AvBlQAhBKAXBAQAQArgCAbQgBATgJAQQgKARgQAGQgKAFgLAAQgYAAgegVgANSxMQgdgPgOgeQgTgrAOhPQANhCAnh8QAPg0ARgXQANgSARgLQATgMATAAQAdAAAYAYQAWAXAGAfQAFAagGAiQgDAUgLAnIg4DBQgJAggKATQgPAagWAKQgLAFgNAAQgQAAgSgJgEgqCgSFQgagJghgZIg2gqQgPgJgggRQgbgQgPgQQgeggAAgoQABgVAJgSQAJgTARgLQAfgVA4AIQBEAKA6AnQA7AmAjA6QAbAtgFAjQgDAUgOARQgOARgUAIQgTAJgVAAQgUAAgWgIgAJGzPQgpgbALhaIAQiUQAEgjADgRQAGgdAKgVQANgZAUgQQAWgSAZgCQAdgCAZAUQAYAVADAdQACAQgFAUIgKAjQgIAhgGA8QgGBEgGAbQgMA8gdAbQgTASgZAEIgOACQgSAAgOgKgEgj3gTnQgagNgYgbQgRgVgthGQgmg6gfgcIgwgsQgYgbADgbQAEgkAxgaQA5gfAsANQAWAGAVATQAPANAUAZQBbBuAaBaQAOAvgHAgQgEAUgMAQQgNARgSAGQgKAEgLAAQgSAAgUgKgEAsogTwQgdgIgegWQiNhqhOjbQgWg7AFgkQADgZAPgUQAQgVAWgGQATgEAVAIQATAHAQAPQANAMAMAUQAHAKANAZQBEB+BZB3QAWAdAHAMQAPAYAGAWQAHAagHAYQgHAbgVANQgQALgUAAQgLAAgNgEgA94z4QgfgOgbgkQgcglgnhfQglhaghgnIgrgxQgWgdACgbQACggAhgUQAegTAkABQAlACAlAUQAhATAcAeQAoAtAoBZQAoBbALA/QAIAogHAfQgIAmgcAQQgQAKgUAAQgTAAgTgIgAg719QgJgeAAg8IgBjOQgBg1AKgbQAHgUAOgOQAQgPATgDQAXgDAWAPQAUAOAKAYQAJAUADAaQACASgBAfIgBCxQgBAkgBASQgDAegJAWQgKAbgTATQgVAUgYAFQgngTgPg0gAFD1DQgQgKgJgRQgOgaABgnIAGhGIACg3QABghAEgVQALhCAqgbQAagSAYAIQAOAFAKANQAJAMAEAQQADANAAARIgBAfQAABQgCAoQgEBFgYAsQgWAogfADIgFAAQgPAAgOgJgAlg1NQgdgPgLgnQgIgcABgtIACkmQAAgnAGgXQAIghAVgRQAQgNAXgCQAVgCAUAJQAnARAQAtQAJAZADAzQAFBYABAsQABBKgGA7QgIBHgcAlQgSAYgdAJQgNAEgMAAQgQAAgOgHgA4z1YQgogLgjgqQgjgsgOg2QgCgJgIguQgGgigJgUQgGgNgMgVIgUgiQgVgsAQgfQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAfAPAtQAIAcALA5IAQBQQAGAhAAAUQAAAcgOATQgNATgYAHQgNAEgMAAQgLAAgLgDgAug2sQgfgMgSgdQgPgZgHgkQgEgXgCgqIgHiDQgCgwAFgUQAJgnAcgPQAZgPAgAMQAeALARAaQAPAXAIAhQAEAVAEAnIAMCEIADAmQAAAWgDAQQgJAsggARQgOAIgQAAQgPAAgRgHgApb3NQgRgGgMgOQgQgUgFgfQgEgUABgkIABi1QAAgbABgOQACgXAHgQQAIgUAPgOQAQgPATgCQAYgCAWASQAUAQAIAaQAIAUABAdIAAAzIAFBrQACA/gMArQgGAagOATQgQAWgXAFIgLABQgMAAgMgFgA0L3SQgUgGgQgOQgZgVgXgvQgVgqgNgjQgQgtgKg0QgLhAAXgdQAPgTAbgEQAZgFAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAhABAZQACAggOAbQgPAdgcAIQgJADgKAAQgKAAgMgDgEAuxgaFQgxgHg0goQgTgPhChAIgjgjQgUgUgKgSQgNgXgDgYQgCgbALgUQAPgcAigKQAhgJAhALQAcAJAdAWQARAOAfAfIBfBdQAaAZALATQARAcgDAZQgCARgMAOQgKAOgQAIQgVAMgaAAQgKAAgLgCgEAyngegQgZgJgcgWQgtglgigqIgUgZQgMgOgLgIQgJgHgSgKIgbgRQgUgNgNgTQgNgUgBgWQgBggAZgZQAZgZAhgFQA5gIBDAsQA0AiAmAxQAnAxAVA6QANAngDAcQgBARgJAPQgJAQgOAIQgOAIgQAAQgMAAgPgFg");
    var mask_graphics_117 = new cjs.Graphics().p("EAwHAl0QgvgNgoguQgggkgZg2QgRgjgXhDQiNmShym9QgNg1AEgbQAGgjAegZQAegZAkABQAdABAcASQAaAQATAaQAQAWANAgIAUA7IBkE4QA8C7AgBdQA1CbAxB6IAUAyQAJAcACAXQADAcgHAZQgIAcgTARQgSARgcAGQgLACgLAAQgQAAgPgFgEAEzAifQgggdgJgpQgEgOgBgVIgBgjQgBgegGg6IgGg1QgHg7gWhqQgXhxgHgzQgFgggDgpQgDgnAEgWQAGgdASgVQAUgXAbgEQAZgDAZAOQAXANAPAXQANATAJAcQAFARAIAjIAyDrQASBVADAsIAEAwIAGAXQAEAPABAJQACANgBAbQgBAaACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgbgEgAwAitQgcgDgXgNQgkgUgMgjQgFgPgBgWIAAglQgBgQgEgbIgGgrQgCgZADhIQAEhJgIgxIgLg7QgFgegBhGIgBiHQAAgnAGgSQALgcAdgPQAegOAcAJQAiALAXAnQAOAYAMAwQALAqADAaIAEAnIADAmQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAmIAFAlQACAXACAtIADAgQABASgCANQgEAbgSAVQgSAVgZAKQgSAHgUAAIgNAAgEAaEAikQgmgSgUgvQgHgSgGgdIgIgwQgIgrgdhQIgohyQgYhIgkiMIgXhXQgShDAagbQANgMATgCQASgDASAHQAbAMAaAkQAeApAYBBQAOAlAZBMQAMAhAnBhQAhBRAPAyQAMAlASBMQAMA2gDAcQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgEATaAiYQgWgEgQgNQgXgTgNgjQgIgWgIgrIgLhEIgLg/QgFgWgzi5Qgih9AAhVQAAglAJgUQAIgQARgLQAQgLASgCQATgCASAIQARAIALAPQALAPAJAfIBRESIAaBcQARA+AZB4QAHAiACASQABAdgIAWQgIATgQAPQgQAPgUAFQgMADgLAAQgKAAgJgCgEAMVAiJQgngLgPgjQgIgQgDglQgJhPgKg/QgMhHgOgzIgXhKQgQgxgHgaQgPg5gIg7QgLhSAhgjQAMgMARgFQARgGARADQAkAHAXAlQAPAXAOAwIBJD4IATBNQAJAgAHAnIAVCEQADAYAAAMQABAUgGAPQgMAfgmAQQgWAJgVAAQgOAAgOgFgEgPQAiGQgXgHgSgRQgTgRgJgXQgLgeAFgpQACgUANgzQAWhaAQh7QANhsAHiJQAEhMAFiqQABgoAJgRQAJgTAVgLQAVgKAWACQAVACATAOQASAMALATQAMAUAFAcQADARACAjIAMDsQAGBkgEA4QgBASgHBCIgYDLQgGA7gMAiQgRAxgjAXQgUAOgZAEIgQABQgRAAgPgFgEAgTAh6QgYgLgPgUQgLgQgLgfIh6lIQgdhOgOgtQgWhEgJg6QgFghAEgSQAFgcAYgTQAYgUAdgBQAcgBAaAQQAZAQANAaQAGAMAFARIAHAeQAMAsAfBBQArBaAGAQQANAgAVBFQAVBCAOAiIAVAwQAMAcAGAVQARA7gWArQgMAXgWAOQgWAOgZACIgHAAQgVAAgUgKgEgI3AhYQgggWgLgiQgIgWAAgeIACg1QAEg5gGhuQgHhyACg1IACgxQABgdgBgUIgFguQgEgcACgTQACgNAEgSIAIgeIAMgwQAJgaAOgQQAMgOASgJQASgIATAAQApABAbAfQAWAZAFAuQADAaABA1QACAgAIBAQACAagBA2QgBA1ACAbIAJBOQAJBHABCRIgBAoQgBAWgGAQQgLAiggAXQgfAWgkABIgCAAQgjAAgegWgEAn3Ag+QgbgLgZgaQgiglgjhBQh8jlg1j9QgOhAAIgjQAGgZAQgTQASgVAYgEQAVgEAXAKQAUAKAPASQANAQALAXQAGAOAKAcIA0CTQAbBLAOAkQAYA9AYAuIAyBhQAbA4AGAtQAFAkgIAaQgLAjgbANQgPAHgQAAQgPAAgQgGgA4heOQgYgEgVgOQgVgOgNgVQgPgagBghQgCgfAMgfQALgcAmg3QCRjMBPicIAnhOQAXgtAUgfQAXgjAXgMQAVgKAYAEQAXADASAPQARAOAKAWQAJAUABAYQAAAXgNAsQgkB6hAB1QgcA0hIBxQhDBpgfA9IgQAfQgKARgLALQgRARgZAJQgRAFgRAAIgPgBgA7/VoQgfgCgbgVQgbgVgKgeQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgbASgNQAcgUAsgNQAYgHAPABQASACAQAMQAPAMAKARQAMAXgBAWQgCARgMASQgHAMgRATQg8BDgVAhQgNAWgIAKQgTAbgvAoIhKA/QgmAggYAOQgjAUggAAIgFAAgEggKAPxQghgJgVgZQgVgagDgjQgCgjARgcQARgaAtgeQAlgYBTgzICbheQAcgSARgIQAagMAXgEQAbgFAZAHQAbAIAOAUQAQAWgCAfQgCAcgQAZQgNAVgZAUQgOAMggAWIi4B7QgeAUgUAPIgjAeQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgjKAK5QgXgKgPgTQgPgUgEgZQgEgZAJgXQALgfAigbQAPgMAxgeQBMgtA/g4IAogjQAXgTATgMQA3ggAvAKQAaAFAUAUQAUAVABAZQABAWgNAXQgJAQgTAWQg2A9hLA9Qg6AvhWA6QgbASgOAHQgYAMgVABIgKABQgTAAgTgJgEgnxAJCQglgJgTgdQgZgmANg1QAJglAhgwQAthBBEhEQAwgwBShFQA3gwAlgWQAfgSAYgGQAXgFAXAFQAYAGARAOQATAPAHAXQAHAYgHAWQgFAPgNAPQgJAKgRAPIhnBYQg8A1gnAoQg+A/giA5IgdAxQgSAbgUAMQgVANgYAAQgLAAgMgDgEgsRAE8QgWgGgQgQQgPgQgHgWQgHgVADgXQADgcAXgmQAWgkAhgoIA+hGQAkgoAUgTQAsgkAVgTIAdgbQASgRANgJQApgbAjAGQAjAFASAdQATAdgKAhQgFATgWAcQgcAjhhBhQhRBRgkA5QgdAugHAIQgXAcgaAIQgLAEgMAAQgKAAgLgDgEgxfACeQgbgUgDggQgBgSAKgdQAQgwAXgnQAOgZAagjQBBhbAigoQAignAdgXQAdgYAfgLQAkgMAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUASQgaAZggArIg0BIIgbAiQgRAVgJAPQgMATgXApQgVAhgYAQQgYAPgbABIgFAAQgeAAgXgRgEg1hgCNQgXgHgOgXQgOgVABgZQAAguApgoQAXgXA5ghIBzhDQAugbAcgFQAVgEATAGQAVAGAMAOQAVAagJAmQgHAbgcAeQgTAVgdAVQgRAMglAXIhrBEQgeATgTAHQgRAGgPAAQgLAAgJgDgEg2LgHzQgdgRgHgeQgKgpAjgwQAog4BFgbQBCgbBHAIQAwAFAeAUQASAMALASQAMASAAAUQACAjgdAcQgbAagnAHQgMADgrADQgjADgTAHQgNAFghATQgcAQgSAEQgJABgIAAQgWAAgVgMgAdHrkQgWgLgOgVQgMgTgGgaQgEgRgDgfQgNhngEhHQgGhgAGhQQADgwANgZQAJgRAQgLQARgMASABQATAAAQAPQAQANAHATQAHARACAWIACAoQACBGAJBiIARCnQAFA0gHAYQgFAUgMAOQgNAQgSAFQgIADgIAAQgPAAgOgIgEAhZgMTQgYgHgQgTQgNgRgJgZQgGgPgHgfQglixgMi8QgCgqALgTQAMgTAYgHQAYgGAWAJQAeANARAmQAMAaAIAtQAUBxAYDCQAHA2gFAaQgIAmgbAOQgMAGgNAAQgKAAgKgEgEgwzgMmIgXgOQgNgJgJgEQgWgJggABIgjADQgWACgNAAQgqgBgWgVQgTgSgBgdQgBgbAOgYQAZgnA9gVQBJgYBLASQBOASAsA6QASAWAHAYQAIAbgHAYQgGAUgRAPQgQAPgVAGQgNADgNAAQgcAAgcgQgAUUseQgYgIgTgjQgcg2AAhSQAAgOAKiCIAFg6QAGggAMgWQAOgaAcgNQAcgNAZAMQAsAVgBBTQAAA0gFAvIgFAsQgCAaAAASIAEA9QACAkgGAYQgIAfgZAUQgTAPgTAAQgIAAgJgDgAYjtQQgYgKgOgWQgNgSgHgaQgFgQgFggQgJg3AAgcQAAgWAKg+QAFgdAEgOQAGgXALgQQAMgUAUgKQAVgKAUAFQATAFANASQAMAQAEAWQADARgBAYIgCApQgBAeAEAqIAIBHQAGAtgHAaQgFATgLANQgMAQgQAGQgJADgKAAQgNAAgOgGgAPouLQgcgPgIglQgGgXADgrQAGhRANg4QARhKAig1QASgcAXgPQAbgSAaAIQATAGAMAUQAKATABAWQABATgGAXIgMApQgQA1gFAvQgDA8gFAeQgJA1gdAbQgRAQgXAEQgIACgHAAQgPAAgNgHgEAlsgPFQg2gmgthWIhLiUIgdgtQgRgagHgVQgJgbAFgbQAEgdAUgQQARgPAagBQAXgBAXAKQAiAQAjAuQArA5AvBlQAhBKAXBAQAQArgCAbQgBATgJAQQgKARgQAGQgKAFgLAAQgYAAgegVgAK+vVQgdgPgOgeQgTgrAOhPQANhCAnh8QAPg0ARgXQANgSARgLQATgMATAAQAdAAAYAYQAWAXAGAfQAFAagGAiQgDAUgLAnIg4DBQgJAggKATQgPAagWAKQgMAFgMAAQgRAAgRgJgEgsWgQOQgagJghgZIg2gqQgPgJgggRQgbgQgPgQQgeggAAgoQABgVAJgSQAJgTARgLQAfgVA4AIQBEAKA6AnQA7AmAjA6QAbAtgFAjQgDAUgOARQgOARgUAIQgUAJgVAAQgUAAgVgIgAGyxYQgpgbALhaIAQiUQAEgjADgRQAGgdAKgVQANgZAUgQQAWgSAZgCQAdgCAZAUQAYAVADAdQACAQgFAUIgKAjQgIAhgGA8QgGBEgGAbQgMA8gdAbQgTASgZAEIgOACQgSAAgOgKgEgmLgRwQgagNgYgbQgRgVgthGQgmg6gfgcIgwgsQgYgbADgbQAEgkAxgaQA5gfAsANQAWAGAVATQAPANAUAZQBbBuAaBaQAOAvgHAgQgEAUgMAQQgNARgSAGQgKAEgMAAQgSAAgTgKgEAqUgR5QgdgIgegWQiNhqhOjbQgWg7AFgkQADgZAPgUQAQgVAWgGQATgEAVAIQATAHAQAPQANAMAMAUQAHAKANAZQBEB+BZB3QAWAdAHAMQAPAYAGAWQAHAagHAYQgHAbgVANQgQALgUAAQgLAAgNgEgEggMgSBQgfgOgbgkQgcglgnhfQglhaghgnIgrgxQgWgdACgbQACggAhgUQAegTAkABQAlACAlAUQAhATAcAeQAoAtAoBZQAoBbALA/QAIAogHAfQgIAmgcAQQgQAKgUAAQgTAAgTgIgAjP0GQgJgeAAg8IgBjOQgBg1AKgbQAHgUAOgOQAQgPATgDQAYgDAWAPQAUAOAKAYQAJAUADAaQACASgBAfIgBCxQgBAkgBASQgDAegJAWQgKAbgTATQgVAUgZAFQgngTgPg0gACvzMQgQgKgJgRQgOgaABgnIAGhGIACg3QABghAEgVQALhCAqgbQAagSAYAIQAOAFAKANQAJAMAEAQQADANAAARIgBAfQAABQgCAoQgEBFgYAsQgWAogfADIgFAAQgPAAgOgJgAn0zWQgdgPgLgnQgIgcABgtIACkmQAAgnAGgXQAIghAVgRQAQgNAXgCQAVgCAUAJQAnARAQAtQAJAZADAzQAFBYABAsQABBKgGA7QgIBHgcAlQgSAYgdAJQgNAEgMAAQgQAAgOgHgA7HzhQgogLgjgqQgjgsgOg2QgCgJgIguQgGgigJgUQgGgNgMgVIgUgiQgVgsAQgfQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAfAPAtQAIAcALA5IAQBQQAGAhAAAUQAAAcgOATQgNATgYAHQgNAEgMAAQgLAAgLgDgAw001QgfgMgSgdQgPgZgHgkQgEgXgCgqIgHiDQgCgwAFgUQAJgnAcgPQAZgPAgAMQAeALARAaQAPAXAIAhQAEAVAEAnIAMCEIADAmQAAAWgDAQQgJAsggARQgPAIgQAAQgPAAgQgHgArv1WQgRgGgMgOQgQgUgFgfQgEgUABgkIABi1QAAgbABgOQACgXAHgQQAIgUAPgOQAQgPATgCQAYgCAWASQAUAQAIAaQAIAUABAdIAAAzIAFBrQACA/gMArQgGAagOATQgQAWgXAFIgLABQgMAAgMgFgA2f1bQgUgGgQgOQgZgVgXgvQgVgqgNgjQgQgtgKg0QgLhAAXgdQAPgTAbgEQAZgFAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAhABAZQACAggOAbQgPAdgcAIQgJADgKAAQgLAAgLgDgEAsdgYOQgxgHg0goQgTgPhChAIgjgjQgUgUgKgSQgNgXgDgYQgCgbALgUQAPgcAigKQAhgJAhALQAcAJAdAWQARAOAfAfIBfBdQAaAZALATQARAcgDAZQgCARgMAOQgKAOgQAIQgWALgZAAQgKAAgLgBgEAwTgcpQgZgJgcgWQgtglgigqIgUgZQgMgOgLgIQgJgHgSgKIgbgRQgUgNgNgTQgNgUgBgWQgBggAZgZQAZgZAhgFQA5gIBDAsQA0AiAmAxQAnAxAVA6QANAngDAcQgBARgJAPQgJAQgOAIQgOAHgQAAQgNAAgOgEgEA1UgfbQgkgHgtgnQhnhZhAh9QgrhTAigrQATgZAlgCQAigBAdATQAYAPAXAeQAOARAZAlQARAXAoAxQAmAvASAaQA1BOgfAuQgMATgZAHQgNAEgNAAQgKAAgKgDg");
    var mask_graphics_120 = new cjs.Graphics().p("EAtwAnMQgugNgoguQgggkgag2QgQgjgYhDQiNmShxm9QgNg1AEgbQAFgjAegZQAegZAkABQAeABAcASQAZAQATAaQAQAWANAgIAUA7IBkE4QA8C7AgBdQA1CbAyB6IATAyQAJAcADAXQADAcgIAZQgIAcgSARQgTARgcAGQgKACgLAAQgQAAgQgFgEACcAj3QgggdgJgpQgDgOgBgVIgCgjQgBgegGg6IgGg1QgHg7gVhqQgYhxgHgzQgEgggEgpQgDgnAFgWQAFgdATgVQAUgXAagEQAZgDAZAOQAXANAQAXQANATAJAcQAFARAHAjIAyDrQASBVADAsIAFAwIAFAXQAEAPABAJQACANgBAbQAAAaABANIAFAZIAFAZQAEAsgaAhQgNAQgTAKQgTAKgUABIgFAAQgkAAgggbgEgDHAkFQgbgDgYgNQgkgUgMgjQgFgPAAgWIgBglQgBgQgEgbIgFgrQgDgZAEhIQADhJgIgxIgLg7QgFgegBhGIgBiHQAAgnAHgSQALgcAdgPQAdgOAdAJQAiALAXAnQAOAYAMAwQALAqAEAaIADAnIAEAmQACARAJAjQAJAiACASQAFAggBA9QgCBAADAdIAGAmIAGAlQACAXABAtIADAgQABASgCANQgDAbgTAVQgRAVgaAKQgSAHgVAAIgNAAgEAXuAj8QgngSgTgvQgIgSgFgdIgJgwQgIgsgdhPIgohyQgXhIgliMIgWhXQgShDAagbQAMgMATgCQATgDARAHQAcAMAaAkQAdApAZBBQAOAlAZBMQAMAhAnBhQAgBRAQAyQALAlASBMQAMA2gCAcQgEAxggAXQgSANgYABIgEAAQgUAAgTgJgEARDAjwQgVgEgRgNQgXgTgNgjQgIgWgHgrIgLhEIgMg/QgEgWgzi5Qgjh9AAhVQAAglAKgUQAIgQAQgLQAQgLATgCQATgCARAIQASAIALAPQAKAPAJAfIBSESIAaBcQAQA+AaB4QAHAiABASQACAdgJAWQgHATgQAPQgRAPgUAFQgLADgMAAQgJAAgKgCgEAJ+AjhQgmgLgQgjQgIgQgDglQgIhPgLg/QgMhHgOgzIgXhKQgQgxgHgaQgPg5gHg7QgLhSAhgjQAMgMARgFQARgGAQADQAkAHAYAlQAOAXAOAwIBJD4IAUBNQAIAgAHAnIAVCEQAEAYAAAMQAAAUgGAPQgMAfglAQQgWAIgVAAQgPAAgOgEgEgRmAjeQgXgHgTgRQgSgRgJgXQgMgeAFgpQADgUAMgzQAXhaAPh7QANhsAHiJQAEhMAFiqQABgoAJgRQAKgTAVgLQAUgKAWACQAWACASAOQATAMALATQALAUAFAcQADARACAjIAMDsQAGBkgDA4QgBASgHBCIgYDLQgHA7gMAiQgRAxgiAXQgUAOgZAEIgRABQgQAAgPgFgEAd8AjSQgYgLgOgUQgLgQgMgfIh5lIQgehOgOgtQgVhEgKg6QgFghAEgSQAGgcAYgTQAXgUAdgBQAcgBAaAQQAaAQANAaQAFAMAFARIAIAeQALAsAfBBQArBaAHAQQAMAgAWBFQAUBCAOAiIAWAwQAMAcAGAVQAQA7gWArQgLAXgWAOQgWAOgZACIgIAAQgVAAgUgKgEgLOAiwQgfgWgMgiQgHgWgBgeIADg1QADg5gGhuQgGhyACg1IACgxQABgdgCgUIgFguQgDgcACgTQABgNAEgSIAIgeIANgwQAJgaANgQQANgOASgJQASgIATAAQApABAbAfQAWAZAFAuQADAaAAA1QACAgAIBAQACAaAAA2QgBA1ACAbIAJBOQAJBHABCRIgBAoQgCAWgFAQQgMAigfAXQgfAWgkABIgCAAQgjAAgfgWgEAlgAiWQgagLgagaQgiglgjhBQh7jlg2j9QgNhAAIgjQAFgZARgTQASgVAXgEQAWgEAWAKQAVAKAPASQANAQAKAXQAHAOAKAcIA0CTQAbBLAOAkQAYA9AXAuIAyBhQAbA4AHAtQAFAkgIAaQgLAjgcANQgOAHgQAAQgPAAgRgGgA63fmQgZgEgVgOQgVgOgMgVQgQgagBghQgBgfALgfQALgcAmg3QCRjMBPicIAnhOQAYgtAUgfQAXgjAXgMQAUgKAYAEQAXADASAPQASAOAKAWQAJAUAAAYQABAXgNAsQglB6g/B1QgcA0hJBxQhDBpgfA9IgQAfQgKARgLALQgRARgYAJQgRAFgRAAIgPgBgA+VXAQgggCgbgVQgagVgKgeQgLgeAHghQAHggAVgZQAQgTAsgeIBJgwQBAgrAagaIAngrQAYgbASgNQAbgUAtgNQAXgHAPABQATACAQAMQAPAMAKARQAMAXgCAWQgBARgMASQgIAMgQATQg9BDgVAhQgNAWgHAKQgTAbgvAoIhLA/QglAggZAOQgjAUggAAIgEAAgEgigARJQgigJgVgZQgVgagCgjQgCgjARgcQAQgaAugeQAkgYBTgzICbheQAdgSARgIQAZgMAXgEQAbgFAZAHQAbAIAPAUQAQAWgCAfQgCAcgQAZQgOAVgYAUQgPAMgfAWIi4B7QgfAUgTAPIgkAeQgVAQgSAHQgRAGgTAAQgOAAgOgDgEglgAMRQgYgKgOgTQgPgUgEgZQgEgZAIgXQALgfAigbQAQgMAwgeQBMgtA/g4IAogjQAXgTAUgMQA2ggAvAKQAbAFATAUQAUAVABAZQABAWgMAXQgJAQgTAWQg2A9hLA9Qg7AvhVA6QgbASgOAHQgYALgWACIgJAAQgUAAgSgIgEgqHAKaQgmgJgTgdQgYgmANg1QAJglAhgwQAshBBEhEQAxgwBRhFQA4gwAkgWQAggSAXgGQAYgFAXAFQAXAGASAOQASAPAHAXQAIAYgIAWQgFAPgNAPQgIAKgRAPIhnBYQg8A1gnAoQg/A/giA5IgdAxQgSAbgUAMQgUANgYAAQgMAAgLgDgEgunAGUQgWgGgQgQQgQgQgHgWQgGgVACgXQAEgcAXgmQAVgkAhgoIA+hHQAkgoAVgTQArgjAVgTIAegbQARgRAOgJQAogbAkAGQAiAFATAdQASAdgJAhQgGASgVAcQgdAjhhBiQhQBRglA5QgdAugHAIQgWAcgbAIQgLADgLAAQgLAAgKgCgEgz1AD2QgcgUgCggQgBgSAJgdQAQgwAYgoQAOgZAZgiQBBhbAigoQAignAdgXQAegYAegLQAkgMAfAIQAeAIASAXQARAUABAWQAAAZgVAZIgTATIgVASQgaAZgfArIg0BHIgcAjQgQAVgKAPQgMATgWApQgWAhgXAQQgYAPgbABIgFAAQgfAAgWgRgEg34gA1QgWgHgPgXQgNgVAAgZQABguAogoQAXgXA5ghIB0hDQAtgbAcgFQAVgEAUAGQAVAGAMAOQAVAagKAmQgHAbgbAeQgUAVgcAVQgSAMgkAXIhrBEQgeATgTAHQgRAFgQAAQgKAAgKgCgEg4hgGbQgdgRgIgeQgJgpAigwQAog4BFgbQBCgbBHAIQAwAFAeAUQASAMALASQAMASABAUQACAjgeAcQgbAagmAHQgNADgrADQgiADgUAHQgNAFggATQgcAQgTAEQgIABgIAAQgWAAgVgMgAawqMQgWgLgOgVQgLgTgGgaQgEgRgEgfQgMhngEhHQgGhgAFhQQADgwANgZQAKgRAPgLQARgMASABQATAAARAPQAPANAIATQAGARADAWIACAoQABBGAKBiIAQCnQAFA0gGAYQgFAUgMAOQgOAQgRAFQgIADgJAAQgOAAgPgIgAfCq7QgYgHgPgTQgOgRgJgZQgFgPgHgfQgmixgLi8QgCgqALgTQALgTAZgHQAYgGAVAJQAeANASAmQAMAaAIAtQATBxAZDCQAHA2gGAaQgIAmgbAOQgLAFgNAAQgKAAgLgDgEgzKgLOIgWgOQgOgJgJgEQgVgJggABIgjADQgWACgNAAQgqgBgXgVQgSgSgBgdQgCgbAPgYQAYgnA9gVQBJgYBLASQBPASAsA6QARAWAHAYQAIAbgHAYQgGAUgQAPQgQAPgVAGQgNADgNAAQgdAAgcgQgAR+rGQgZgIgSgjQgdg2AAhSQAAgOAKiCIAGg6QAFggAMgWQAPgaAbgNQAdgNAZAMQAsAVgBBTQAAA0gFAvIgFAsQgDAaAAASIAEA9QACAkgGAYQgHAfgaAUQgSAOgTAAQgJAAgIgCgAWMr4QgXgKgPgWQgMgSgIgaQgEgQgGggQgJg3ABgcQAAgWAKg+QAFgdAEgOQAGgXAKgQQANgUATgKQAVgKAUAFQATAFAOASQAMAQAEAWQADARgBAYIgDApQgBAeAEAqIAIBHQAGAtgHAaQgEATgLANQgMAQgQAGQgKADgJAAQgOAAgOgGgANSszQgcgPgJglQgFgXADgrQAGhRANg4QARhKAhg1QASgcAXgPQAcgSAZAIQATAGAMAUQALATABAWQAAATgFAXIgNApQgQA1gFAvQgCA8gFAeQgJA1gdAbQgRAQgXAEQgIACgIAAQgOAAgNgHgEAjWgNtQg2gmgthWIhMiUIgdgtQgQgagHgVQgJgbAEgbQAFgdATgQQASgPAZgBQAYgBAWAKQAiAQAjAuQAsA5AuBlQAiBKAWBAQAQArgBAbQgBATgKAQQgJARgQAGQgKAEgLAAQgYAAgegUgAIot9QgegPgNgeQgUgrAPhPQANhCAmh8QAQg0ARgXQANgSAQgLQATgMAUAAQAdAAAXAYQAWAXAGAfQAFAagFAiQgEAUgLAnIg4DBQgJAggKATQgOAagWAKQgMAFgNAAQgQAAgRgJgEgutgO2QgZgJghgZIg3gqQgPgJgfgRQgcgQgPgQQgeggABgoQAAgVAJgSQAKgTAQgLQAggVA4AIQBDAKA7AnQA6AmAkA6QAbAtgGAjQgDAUgOARQgOARgUAIQgTAIgVAAQgUAAgWgHgAEbwAQgogbAKhaIAQiUQAEgjAEgRQAFgdALgVQAMgZAUgQQAWgSAZgCQAdgCAZAUQAZAVADAdQABAQgFAUIgJAjQgJAhgGA8QgGBEgFAbQgMA8geAbQgTASgYAEIgPABQgRAAgPgJgEgohgQYQgbgNgXgbQgSgVgthGQglg6gfgcIgwgsQgYgbADgbQAEgkAwgaQA5gfAsANQAXAGAVATQAOANAVAZQBaBuAbBaQANAvgGAgQgEAUgMAQQgNARgSAGQgLAEgLAAQgSAAgTgKgEAn+gQhQgdgIgegWQiOhqhOjbQgVg7AEgkQADgZAPgUQAQgVAXgGQATgEAUAIQATAHAQAPQANAMAMAUQAHAKAOAZQBDB+BZB3QAWAdAIAMQAPAYAFAWQAHAagHAYQgHAbgUANQgQAKgUAAQgMAAgMgDgEgiigQpQgfgOgbgkQgcglgnhfQglhagignIgqgxQgWgdACgbQACggAggUQAegTAkABQAmACAkAUQAiATAbAeQAoAtAoBZQAoBbAMA/QAHAogHAfQgIAmgbAQQgQAKgVAAQgTAAgSgIgAllyuQgJgeAAg8IgCjOQgBg1AKgbQAHgUAPgOQAQgPATgDQAXgDAWAPQAUAOALAYQAJAUADAaQABASAAAfIgCCxQAAAkgCASQgDAegIAWQgKAbgTATQgVAUgaAFQgmgTgPg0gAAZx0QgQgKgJgRQgNgaAAgnIAGhGIACg3QABghAEgVQAKhCAqgbQAbgSAXAIQAOAFAKANQAKALADARQAEANAAARIgCAfQABBQgDAoQgEBFgYAsQgWAogeADIgGAAQgOAAgOgJgAqLx+QgcgPgLgnQgIgcAAgtIADkmQAAgnAFgXQAIghAVgRQAQgNAXgCQAWgCAUAJQAmARARAtQAJAZADAzQAFBYAAAsQACBKgHA7QgHBHgcAlQgTAYgdAJQgNAEgMAAQgQAAgOgHgA9dyJQgpgLgigqQgkgsgNg2QgDgJgIguQgGgigJgUQgFgNgNgVIgTgiQgVgsAPgfQAMgZAggJQAcgIAfAHQBDAQApA2QAYAfAOAtQAJAcALA5IAPBQQAHAhAAAUQgBAcgNATQgOATgYAHQgMADgNAAQgKAAgLgCgAzKzdQgggMgSgdQgPgZgGgkQgEgXgCgqIgHiDQgCgwAEgUQAKgnAbgPQAagPAfAMQAeALASAaQAPAXAHAhQAFAVADAnIANCEIACAmQABAWgEAQQgJAsggARQgOAIgQAAQgPAAgQgHgAuFz+QgRgGgMgOQgQgUgGgfQgDgUAAgkIABi1QAAgbACgOQACgXAGgQQAIgUAPgOQAQgPAUgCQAYgCAVASQAUAQAJAaQAHAUABAdIABAzIAEBrQACA/gLArQgHAagOATQgQAWgWAFIgMABQgLAAgMgFgA410DQgUgGgRgOQgYgVgYgvQgVgqgMgjQgRgtgKg0QgLhAAYgdQAPgTAagEQAagFAXAKQAqARAaA1QAJASAKAcIAQAvQAPAlAHATQALAhACAZQACAggPAbQgPAdgbAIQgKADgJAAQgLAAgLgDgEAqHgW2QgxgHg0goQgTgPhDhAIgjgjQgTgUgLgSQgNgXgCgYQgDgbALgUQAPgcAjgKQAhgJAgALQAcAJAdAWQARAOAgAfIBeBdQAaAZAMATQARAcgEAZQgCARgLAOQgLAOgQAIQgVALgaAAQgKAAgKgBgEAt9gbRQgagJgcgWQgtglghgqIgVgZQgMgOgLgIQgJgHgRgKIgcgRQgUgNgMgTQgOgUgBgWQgBggAagZQAYgZAigFQA5gIBDAsQAzAiAnAxQAnAxAUA6QAOAngDAcQgCARgIAPQgJAQgPAIQgNAHgRAAQgMAAgOgEgEAy9geDQgjgHgtgnQhnhZhAh9QgrhTAhgrQAUgZAkgCQAigBAeATQAXAPAYAeQANARAZAlQARAXAoAxQAmAvASAaQA2BOgfAuQgNATgYAHQgNADgNAAQgKAAgLgCgEA3AghGQgegagZgyQgbg6gPgcIgegzQgSgegHgXQgKgeAFgdQAFggAVgSQAVgSAfAAQAdgBAZAQQAnAWAlA9QAqBHAcBPQAUA8gFAlQgEAagQAUQgQAVgYAFQgHACgIAAQgeAAgfgag");
    var mask_graphics_122 = new cjs.Graphics().p("EAq/AoJQgugNgoguQgggkgag3QgQgigYhDQiNmThxm9QgNg0AEgbQAFgkAegYQAegZAkABQAeAAAcASQAZAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAyB5IATAyQAJAcADAXQADAdgIAZQgIAbgSARQgTASgcAFQgLACgKAAQgQAAgQgEgEgAUAkzQgggcgJgpQgDgPgBgUIgCgkQgBgdgGg6IgGg2QgHg6gVhqQgYhxgHgzQgEgggEgpQgDgoAFgWQAFgcATgWQAUgWAagEQAZgEAZAOQAXANAPAXQANAUAJAcQAFAQAHAjIAyDsQASBVADAsIAFAvIAFAYQAEAOABAJQACAOgBAaQAAAbABANIAFAZIAFAZQAEAsgaAhQgNAQgTAKQgTAKgUABIgFAAQglAAgegcgEgF4AlBQgbgCgYgOQgkgUgMgjQgFgOAAgWIgBgmQgBgPgEgbIgFgrQgDgaAEhIQADhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAHgSQALgcAdgOQAdgOAdAJQAiAKAXAnQAOAYAMAxQALApAEAaIADAnIAEAnQACARAJAjQAJAiACASQAFAggBA9QgCBAADAdIAGAlIAGAlQACAXABAuIADAfQABATgCANQgDAagTAWQgRAVgaAKQgTAHgUAAIgNgBgEAU9Ak5QgngSgTgvQgIgTgFgcIgJgxQgIgrgdhPIgohyQgXhIgliNIgWhXQgShDAagaQAMgNATgCQATgCARAHQAcALAaAlQAdApAZBAQAOAlAZBMQAMAiAnBgQAgBRAQAyQALAlASBMQAMA2gCAdQgEAxggAXQgSANgYABIgEAAQgUAAgTgJgEAOSAktQgVgFgRgNQgXgSgNgjQgIgXgHgrIgLhEIgMg+QgEgXgzi5Qgjh9AAhUQAAgmAKgTQAIgRAQgLQAQgLATgCQATgBARAIQASAIALAPQAKAOAJAgIBSESIAaBbQAQA/AaB3QAHAjABARQACAegJAVQgHAUgQAOQgRAPgUAGQgLADgMAAQgJAAgKgCgEAHNAkeQgmgMgQgiQgIgRgDgkQgIhQgLg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gHg8QgLhSAhgiQAMgMARgGQARgFAQADQAkAGAYAmQAOAWAOAxIBJD3IAUBNQAIAhAHAmIAVCFQAEAXAAANQAAAUgGAOQgMAgglAPQgWAJgVAAQgPAAgOgEgEgUXAkbQgXgHgTgSQgSgRgJgXQgMgdAFgpQADgUAMg0QAXhaAPh7QANhsAHiJQAEhMAFiqQABgnAJgRQAKgTAVgLQAUgLAWADQAWACASANQATANALATQALATAFAdQADAQACAjIAMDtQAGBjgDA4QgBATgHBBIgYDMQgHA6gMAiQgRAxgiAYQgUANgZAEIgRABQgQAAgPgEgEAbLAkPQgYgLgOgVQgLgPgMggIh5lHQgehPgOgsQgVhFgKg5QgFghAEgSQAGgcAYgUQAXgTAdgBQAcgCAaARQAaAPANAaQAFAMAFASIAIAeQALAsAfBAQArBaAHARQAMAfAWBFQAUBCAOAiIAWAwQAMAdAGAUQAQA8gWArQgLAWgWAPQgWAOgZABIgIAAQgVAAgUgJgEgN/AjtQgfgXgMgiQgHgVgBgeIADg1QADg6gGhtQgGhyACg1IACgxQABgdgCgUIgFgvQgDgcACgSQABgNAEgSIAIgfIANgvQAJgaANgQQANgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaAAA2QACAgAIA/QACAbAAA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgCAWgFARQgMAigfAWQgfAXgkAAIgCAAQgjAAgfgVgEAivAjSQgagKgagbQgigkgjhCQh7jlg2j9QgNg/AIgkQAFgZARgTQASgUAXgFQAWgEAWALQAVAJAPATQANAPAKAXQAHAOAKAdIA0CSQAbBMAOAjQAYA9AXAvIAyBgQAbA5AHAtQAFAjgIAbQgLAigcANQgOAHgQAAQgPAAgRgGgEgdoAgjQgZgEgVgOQgVgOgMgWQgQgZgBgiQgBgfALgeQALgdAmg2QCRjMBPidIAnhOQAYgtAUgeQAXgkAXgLQAUgKAYADQAXAEASAOQASAOAKAWQAJAVAAAXQABAYgNAsQglB5g/B2QgcAzhJBxQhDBpgfA9IgQAfQgKARgLALQgRASgYAIQgRAGgSAAIgOgBgEghGAX8QgggBgbgVQgagVgKgfQgLgeAHghQAHggAVgZQAQgTAsgeIBJgwQBAgrAagaIAngrQAYgaASgNQAbgVAtgNQAXgGAPABQATACAQAMQAPALAKASQAMAXgCAVQgBARgMATQgIAMgQASQg9BEgVAgQgNAWgHAKQgTAbgvAoIhLBAQglAggZAOQgjATggAAIgEAAgEglRASFQgigJgVgZQgVgZgCgjQgCgjARgcQAQgaAugeQAkgZBTgyICbhfQAdgRARgIQAZgNAXgEQAbgEAZAHQAbAIAPAUQAQAWgCAeQgCAdgQAZQgOAVgYAUQgPAMgfAVIi4B8QgfATgTAQIgkAdQgVAQgSAHQgRAHgTAAQgOAAgOgEgEgoRANOQgYgLgOgTQgPgTgEgZQgEgZAIgXQALgfAigbQAQgNAwgdQBMgtA/g4IAogjQAXgUAUgMQA2ggAvAKQAbAGATAUQAUAUABAaQABAVgMAXQgJAQgTAWQg2A+hLA8Qg7AwhVA5QgbASgOAHQgYAMgWACIgJAAQgUAAgSgIgEgs4ALXQgmgJgTgeQgYgmANg1QAJglAhgvQAshBBEhEQAxgxBRhFQA4gwAkgWQAggSAXgFQAYgFAXAFQAXAFASAPQASAPAHAXQAIAXgIAWQgFAPgNAPQgIAKgRAPIhnBZQg8A0gnAoQg/BAgiA5IgdAwQgSAbgUANQgUAMgZAAQgLAAgLgCgEgxYAHQQgWgGgQgQQgQgPgHgWQgGgWACgWQAEgcAXgnQAVgjAhgpIA+hHQAkgoAVgTQArgjAVgTIAegcQARgPAOgJQAogbAkAFQAiAGATAdQASAcgJAhQgGATgVAbQgdAkhhBhQhQBRglA6QgdAtgHAJQgWAcgbAIQgLADgLAAQgLAAgKgDgEg2mAEyQgcgTgCghQgBgRAJgdQAQgxAYgoQAOgYAZgjQBBhbAignQAignAdgYQAegYAegKQAkgNAfAIQAeAIASAXQARAUABAWQAAAZgVAZIgTATIgVATQgaAXgfArIg0BIIgcAkQgQAVgKAPQgMATgWAoQgWAigXAPQgYAQgbAAIgFAAQgfAAgWgRgEg6pAAGQgWgGgPgWQgNgWAAgZQABgtAogpQAXgXA5ghIB0hDQAtgaAcgGQAVgDAUAFQAVAGAMAPQAVAZgKAmQgHAcgbAdQgUAVgcAVQgSANgkAXIhrBDQgeATgTAGQgRAGgQAAQgKAAgKgDgEg7SgFeQgdgSgIgdQgJgqAigwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATABAUQACAigeAcQgbAagmAIQgNACgrAEQgiACgUAIQgNAEggATQgcAQgTAEQgIACgIAAQgWAAgVgMgAX/pPQgWgLgOgWQgLgSgGgaQgEgSgEgeQgMhogEhHQgGhfAFhQQADgwANgZQAKgSAPgLQARgLASAAQATABARAOQAPANAIAUQAGAQADAXIACAoQABBGAKBhIAQCoQAFAzgGAZQgFATgMAPQgOAQgRAFQgJACgIAAQgOAAgPgHgAcRp+QgYgIgPgTQgOgQgJgZQgFgQgHgfQgmiwgLi8QgCgqALgTQALgUAZgGQAYgHAVAKQAeANASAlQAMAaAIAuQATBxAZDCQAHA1gGAbQgIAlgbAOQgLAGgNAAQgKAAgLgDgEg17gKRIgWgPQgOgJgJgEQgVgJggABIgjADQgWADgNAAQgqgBgXgVQgSgSgBgdQgCgcAPgXQAYgnA9gVQBJgZBLASQBPATAsA5QARAXAHAYQAIAbgHAXQgGAUgQAQQgQAPgVAFQgNADgNAAQgdAAgcgPgAPNqKQgZgIgSgiQgdg3AAhRQAAgPAKiCIAGg5QAFggAMgWQAPgaAbgNQAdgOAZAMQAsAVgBBUQAAAzgFAvIgFAtQgDAZAAATIAEA9QACAkgGAYQgHAfgaAUQgTAOgSAAQgJAAgIgDgATbq8QgXgKgPgVQgMgSgIgbQgEgQgGggQgJg3ABgbQAAgXAKg+QAFgdAEgNQAGgYAKgQQANgTATgKQAVgLAUAFQATAFAOASQAMAQAEAWQADARgBAYIgDApQgBAeAEAqIAIBIQAGAtgHAaQgEASgLAOQgMAPgQAGQgKAEgJAAQgOAAgOgHgAKhr2QgcgQgJglQgFgXADgqQAGhSANg4QARhJAhg2QASgcAXgPQAcgSAZAJQATAGAMAUQALASABAXQAAASgFAXIgNApQgQA2gFAvQgCA8gFAdQgJA1gdAbQgRAQgXAFQgIABgIAAQgOAAgNgGgEAglgMwQg2gmgthXIhMiTIgdgtQgQgbgHgVQgJgbAEgaQAFgdATgRQASgOAZgCQAYgBAWAKQAiARAjAuQAsA5AuBkQAiBKAWBBQAQArgBAbQgBATgKAPQgJARgQAHQgKAEgLAAQgZAAgdgUgAF3tAQgegQgNgdQgUgrAPhPQANhCAmh8QAQg0ARgYQANgSAQgKQATgMAUAAQAdgBAXAZQAWAWAGAfQAFAbgFAhQgEAUgLAoIg4DAQgJAhgKASQgOAagWAKQgMAGgNAAQgQAAgRgJgEgxegN6QgZgJghgZIg3gpQgPgKgfgRQgcgPgPgQQgeggABgpQAAgUAJgSQAKgTAQgLQAggWA4AJQBDAKA7AmQA6AnAkA6QAbAtgGAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgABqvDQgogbAKhaIAQiVQAEgjAEgRQAFgcALgVQAMgZAUgRQAWgRAZgCQAdgDAZAVQAZAUADAeQABAPgFAUIgJAjQgJAhgGA9QgGBEgFAbQgMA8geAbQgTARgYAFIgPABQgRAAgPgJgEgrSgPbQgbgNgXgcQgSgUgthHQglg6gfgbIgwgsQgYgbADgbQAEgkAwgbQA5geAsAMQAXAHAVASQAOANAVAZQBaBvAbBaQANAugGAgQgEAUgMAQQgNARgSAHQgLADgLAAQgSAAgTgJgEAlNgPkQgdgIgegXQiOhphOjbQgVg8AEgjQADgZAPgUQAQgWAXgFQATgFAUAIQATAHAQAPQANANAMATQAHALAOAZQBDB+BZB2QAWAeAIAMQAPAYAFAVQAHAagHAZQgHAbgUANQgQAKgVAAQgLAAgMgDgEglTgPtQgfgOgbgjQgcglgnhfQglhagigoIgqgwQgWgdACgbQACggAggVQAegTAkACQAmACAkAUQAiASAbAfQAoAsAoBaQAoBaAMBAQAHAngHAgQgIAmgbAQQgQAJgVAAQgTAAgSgIgAoWxxQgJgfAAg8IgCjNQgBg1AKgbQAHgUAPgOQAQgPATgDQAXgDAWAPQAUAOALAXQAJAUADAbQABARAAAgIgCCwQAAAlgCARQgDAegIAXQgKAbgTATQgVAUgaAEQgmgTgPgzgAiXw3QgQgKgJgRQgOgaAAgoIAGhFIACg3QABghAEgWQALhBAqgcQAbgRAXAIQAOAEAKANQAKAMADAQQAEANAAASIgCAeQABBRgDAoQgEBFgYAsQgWAngeADIgGAAQgPAAgNgIgAs8xCQgcgOgLgnQgIgcAAgtIADknQAAgmAFgXQAIghAVgRQAQgNAXgDQAWgCAUAJQAmASARAsQAJAaADAyQAFBYAAAtQACBKgHA6QgHBIgcAkQgTAYgdAJQgNAEgMAAQgQAAgOgHgEggOgRNQgpgKgigrQgkgsgNg1QgDgJgIguQgGgigJgUQgFgOgNgVIgTghQgVgtAPgeQAMgZAggJQAcgIAfAHQBDAQApA2QAYAeAOAuQAJAbALA5IAPBRQAHAgAAAUQgBAdgNATQgOASgYAHQgMAEgNAAQgLAAgKgDgA17ygQgggNgSgdQgPgZgGgjQgEgXgCgqIgHiEQgCgvAEgUQAKgnAbgQQAagOAfALQAeALASAbQAPAWAHAhQAFAWADAnIANCEIACAmQABAVgEARQgJAsggARQgOAHgQAAQgPAAgQgGgAw2zBQgRgHgMgOQgQgTgGgfQgDgVAAgkIABi1QAAgbACgNQACgXAGgRQAIgUAPgOQAQgOAUgCQAYgCAVARQAUARAJAZQAHAVABAdIABAyIAEBsQACA/gLAqQgHAbgOASQgQAWgWAFIgMABQgLAAgMgEgA7mzHQgUgFgRgPQgYgVgYgvQgVgpgMgkQgRgsgKg0QgLhAAYgeQAPgSAagFQAagEAXAKQAqARAaA1QAJASAKAcIAQAvQAPAlAHATQALAgACAZQACAggPAbQgPAegbAIQgKADgKAAQgKAAgLgEgEAnWgV6QgxgHg0goQgTgOhDhBIgjgiQgTgVgLgSQgNgXgCgYQgDgaALgVQAPgbAjgKQAhgJAgAKQAcAJAdAXQARANAgAfIBeBdQAaAaAMATQARAbgEAaQgCARgLAOQgLAOgQAIQgVALgaAAQgKAAgKgCgEArMgaVQgagIgcgXQgtglghgpIgVgZQgMgOgLgJQgJgHgRgKIgcgQQgUgOgMgSQgOgUgBgWQgBggAagaQAYgZAigEQA5gJBDAsQAzAiAnAxQAnAxAUA6QAOAngDAcQgCASgIAPQgJAPgPAIQgNAIgRAAQgMAAgOgFgEAwMgdGQgjgIgtgmQhnhZhAh+QgrhSAhgsQAUgZAkgBQAigBAeASQAXAQAYAdQANASAZAkQARAYAoAxQAmAuASAaQA2BOgfAvQgNASgYAHQgNAEgNAAQgLAAgKgCgEA0PggKQgegZgZgyQgbg7gPgcIgegzQgSgegHgXQgKgdAFgeQAFggAVgSQAVgSAfAAQAdAAAZAPQAnAXAlA9QAqBHAcBOQAUA8gFAlQgEAagQAUQgQAVgYAGQgIABgHAAQgeAAgfgagEA6dgf5QgWgMgOgYQgOgWgLggIgQg5Qgfh4gthyQgMghgEgPQgGgcAGgVQAIgcAdgPQAcgQAcAJQAaAJAUAbQAPATAPAjQAaA/AXA+QAZBGAKAzQAOBDgHA5QgHA+glANQgIACgIAAQgPAAgQgJg");
    var mask_graphics_124 = new cjs.Graphics().p("EAoEAomQgvgNgoguQgggkgZg2QgRgjgXhDQiNmShym9QgNg1AEgbQAGgjAegZQAegZAkABQAdABAcASQAaAQATAaQAQAWANAgIAUA7IBkE4QA8C7AgBdQA1CbAxB6IAUAyQAJAcACAXQADAcgHAZQgIAcgTARQgSARgcAGQgLACgLAAQgPAAgQgFgEgDPAlRQgggdgJgpQgEgOgBgVIgBgjQgBgegGg6IgGg1QgHg7gWhqQgXhxgHgzQgFgggDgpQgDgnAEgWQAGgdASgVQAUgXAbgEQAZgDAZAOQAXANAPAXQANATAJAcQAFARAIAjIAyDrQASBVADAsIAEAwIAGAXQAEAPABAJQACANgBAbQgBAaACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgbgEgIzAlfQgcgDgXgNQgkgUgMgjQgFgPgBgWIAAglQgBgQgEgbIgGgrQgCgZADhIQAEhJgIgxIgLg7QgFgegBhGIgBiHQAAgnAGgSQALgcAdgPQAegOAcAJQAiALAXAnQAOAYANAwQALAqADAaIAEAnIADAmQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAmIAFAlQACAXACAtIADAgQABASgCANQgEAbgSAVQgSAVgZAKQgTAHgUAAIgNAAgEASBAlWQgmgSgUgvQgHgSgGgdIgIgwQgIgrgdhQIgohyQgYhIgkiMIgXhXQgShDAagbQANgMATgCQASgDASAHQAbAMAaAkQAeApAYBBQAOAlAZBMQAMAhAnBhQAhBRAPAyQAMAlASBMQAMA2gDAcQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgEALXAlKQgWgEgQgNQgXgTgNgjQgIgWgIgrIgLhEIgLg/QgFgWgzi5Qgih9AAhVQAAglAJgUQAIgQARgLQAQgLASgCQATgCASAIQARAIALAPQALAPAJAfIBRESIAaBcQARA+AZB4QAHAiACASQABAdgIAWQgIATgQAPQgQAPgUAFQgMADgLAAQgKAAgJgCgEAESAk7QgngLgPgjQgIgQgDglQgJhPgKg/QgMhHgOgzIgXhKQgQgxgHgaQgPg5gIg7QgLhSAhgjQAMgMARgFQARgGARADQAkAHAXAlQAPAXAOAwIBJD4IATBNQAJAgAHAnIAVCEQADAYAAAMQABAUgGAPQgMAfgmAQQgWAIgVAAQgOAAgOgEgEgXTAk4QgXgHgSgRQgTgRgJgXQgLgeAFgpQACgUANgzQAWhaAQh7QANhsAHiJQAEhMAFiqQABgoAJgRQAJgTAVgLQAVgKAWACQAVACATAOQASAMALATQAMAUAFAcQADARACAjIAMDsQAGBkgEA4QgBASgHBCIgYDLQgGA7gMAiQgRAxgjAXQgUAOgZAEIgQABQgQAAgQgFgEAYQAksQgYgLgPgUQgLgQgLgfIh6lIQgdhOgOgtQgWhEgJg6QgFghAEgSQAFgcAYgTQAYgUAdgBQAcgBAaAQQAZAQANAaQAGAMAFARIAHAeQAMAsAfBBQArBaAGAQQANAgAVBFQAVBCAOAiIAVAwQAMAcAGAVQARA7gWArQgMAXgWAOQgWAOgZACIgHAAQgVAAgUgKgEgQ6AkKQgggWgLgiQgIgWAAgeIACg1QAEg5gGhuQgHhyACg1IACgxQABgdgBgUIgFguQgEgcACgTQACgNAEgSIAIgeIAMgwQAJgaAOgQQAMgOASgJQASgIATAAQApABAbAfQAWAZAFAuQADAaABA1QACAgAIBAQACAagBA2QgBA1ACAbIAJBOQAJBHABCRIgBAoQgBAWgGAQQgLAiggAXQgfAWgkABIgBAAQgkAAgegWgEAf0AjwQgbgLgZgaQgiglgjhBQh8jlg1j9QgOhAAIgjQAGgZAQgTQASgVAYgEQAVgEAXAKQAUAKAPASQANAQALAXQAGAOAKAcIA0CTQAbBLAOAkQAYA9AYAuIAyBhQAbA4AGAtQAFAkgIAaQgLAjgbANQgPAHgQAAQgPAAgQgGgEggkAhAQgYgEgVgOQgVgOgNgVQgPgagBghQgCgfAMgfQALgcAmg3QCRjMBPicIAnhOQAXgtAUgfQAXgjAXgMQAVgKAYAEQAXADASAPQARAOAKAWQAJAUABAYQAAAXgNAsQgkB6hAB1QgcA0hIBxQhDBpgfA9IgQAfQgKARgLALQgRARgZAJQgRAFgRAAIgPgBgEgkCAYaQgfgCgbgVQgbgVgKgeQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgbASgNQAcgUAsgNQAYgHAPABQASACAQAMQAPAMAKARQAMAXgBAWQgCARgMASQgHAMgRATQg8BDgVAhQgNAWgIAKQgTAbgvAoIhKA/QgmAggYAOQgjAUggAAIgFAAgEgoNASjQghgJgVgZQgVgagDgjQgCgjARgcQARgaAtgeQAlgYBTgzICbheQAcgSARgIQAagMAXgEQAbgFAZAHQAbAIAOAUQAQAWgCAfQgCAcgQAZQgNAVgZAUQgOAMggAWIi4B7QgeAUgUAPIgjAeQgVAQgSAHQgSAGgSAAQgOAAgPgDgEgrNANrQgXgKgPgTQgPgUgEgZQgEgZAJgXQALgfAigbQAPgMAxgeQBMgtA/g4IAogjQAXgTATgMQA3ggAvAKQAaAFAUAUQAUAVABAZQABAWgNAXQgJAQgTAWQg2A9hLA9Qg6AvhWA6QgbASgOAHQgYAMgVABIgKAAQgTAAgTgIgEgv0AL0QglgJgTgdQgZgmANg1QAJglAhgwQAthBBEhEQAwgwBShFQA3gwAlgWQAfgSAYgGQAXgFAXAFQAYAGARAOQATAPAHAXQAHAYgHAWQgFAPgNAPQgJAKgRAPIhnBYQg8A1gnAoQg+A/giA5IgdAxQgSAbgUAMQgVANgYAAQgLAAgMgDgEg0UAHuQgWgGgQgQQgPgQgHgWQgHgVADgXQADgcAXgmQAWgkAhgoIA+hHQAkgoAUgTQAsgkAVgTIAdgbQASgRANgJQApgaAjAGQAjAFASAcQATAdgKAhQgFATgWAcQgcAjhhBiQhRBRgkA5QgdAugHAIQgXAcgaAIQgLADgMAAQgKAAgLgCgEg5iAFQQgbgUgDggQgBgSAKgdQAQgwAXgoQAOgZAagjQBBhaAigoQAignAdgXQAdgYAfgLQAkgMAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUATIgUARQgaAZggArIg0BIIgbAjQgRAVgJAPQgMATgXApQgVAhgYAQQgYAPgbABIgFAAQgeAAgXgRgEg9kAAkQgXgHgOgXQgOgUABgZQAAguApgoQAXgXA5ghIBzhDQAugbAcgFQAVgEATAGQAVAGAMAOQAVAagJAmQgHAbgcAeQgTAVgdAVQgRAMglAXIhrBDQgeATgTAHQgRAFgPAAQgLAAgJgCgEg+OgFBQgdgRgHgeQgKgpAjgwQAog4BFgbQBCgbBHAIQAwAFAeAUQASAMALASQAMASAAAUQACAjgdAcQgbAagnAHQgMADgrADQgjADgTAHQgNAFghATQgcAQgSAEQgIABgIAAQgXAAgVgMgAVEoyQgWgLgOgVQgMgTgGgaQgEgRgDgfQgNhngEhHQgGhgAGhQQADgwANgZQAJgRAQgLQARgMASABQATAAAQAPQAQANAHATQAHARACAWIACAoQACBGAJBiIARCnQAFA0gHAYQgFAUgMAOQgNAQgSAFQgIADgIAAQgOAAgPgIgAZWphQgYgHgQgTQgNgRgJgZQgGgPgHgfQglixgMi8QgCgqALgTQAMgTAYgHQAYgGAWAJQAeANARAmQAMAaAIAtQAUBxAYDCQAHA2gFAaQgIAmgbAOQgMAFgNAAQgKAAgKgDgEg42gJ0IgXgOQgNgJgJgEQgWgJggABIgjADQgWACgNAAQgqgBgWgVQgTgSgBgdQgBgbAOgYQAZgnA9gVQBJgYBLASQBOASAsA6QASAWAHAYQAIAbgHAYQgGAUgRAPQgQAPgVAGQgNADgNAAQgcAAgcgQgAMRpsQgYgIgTgjQgcg2AAhSQAAgOAKiCIAFg6QAGggAMgWQAOgaAcgNQAcgNAZAMQAsAVgBBTQAAA0gFAvIgFAsQgCAaAAASIAEA9QACAkgGAYQgIAfgZAUQgTAOgTAAQgIAAgJgCgAQgqeQgYgKgOgWQgNgSgHgaQgFgQgFggQgJg3AAgcQAAgWAKg+QAFgdAEgOQAGgXALgQQAMgUAUgKQAVgKAUAFQATAFANASQAMAQAEAWQADARgBAYIgCApQgBAeAEAqIAIBHQAGAtgHAaQgFATgLANQgMAQgQAGQgJADgKAAQgNAAgOgGgAHlrZQgcgPgIglQgGgXADgrQAGhRANg4QARhKAig1QASgcAXgPQAbgSAaAIQATAGAMAUQAKATABAWQABATgGAXIgMApQgQA1gFAvQgDA8gFAeQgJA1gdAbQgRAQgXAEQgIACgHAAQgPAAgNgHgAdpsTQg2gmgthWIhLiUIgdgtQgRgagHgVQgJgbAFgbQAEgdAUgQQARgPAagBQAXgBAXAKQAiAQAjAuQArA5AvBlQAhBKAXBAQAQArgCAbQgBATgJAQQgKARgQAGQgKAEgLAAQgYAAgegUgAC7sjQgdgPgOgeQgTgrAOhPQANhCAnh8QAPg0ARgXQANgSARgLQATgMATAAQAdAAAYAYQAWAXAGAfQAFAagGAiQgDAUgLAnIg4DBQgJAggKATQgPAagWAKQgMAFgMAAQgQAAgSgJgEg0ZgNcQgagJghgZIg2gqQgPgJgggRQgbgQgPgQQgeggAAgoQABgVAJgSQAJgTARgLQAfgVA4AIQBEAKA6AnQA7AmAjA6QAbAtgFAjQgDAUgOARQgOARgUAIQgUAIgUAAQgVAAgVgHgAhQumQgpgbALhaIAQiUQAEgjADgRQAGgdAKgVQANgZAUgQQAWgSAYgCQAdgCAZAUQAYAVADAdQACAQgFAUIgKAjQgIAhgGA8QgGBEgGAbQgMA8gdAbQgSASgZAEIgOABQgSAAgOgJgEguOgO+QgagNgYgbQgRgVgthGQgmg6gfgcIgwgsQgYgbADgbQAEgkAxgaQA5gfAsANQAWAGAVATQAPANAUAZQBbBuAaBaQAOAvgHAgQgEAUgMAQQgNARgSAGQgKAEgMAAQgRAAgUgKgEAiRgPHQgdgIgegWQiNhqhOjbQgWg7AFgkQADgZAPgUQAQgVAWgGQATgEAVAIQATAHAQAPQANAMAMAUQAHAKANAZQBEB+BZB3QAWAdAHAMQAPAYAGAWQAHAagHAYQgHAbgVANQgQAKgUAAQgLAAgNgDgEgoPgPPQgfgOgbgkQgcglgnhfQglhaghgnIgrgxQgWgdACgbQACggAhgUQAegTAkABQAlACAlAUQAhATAcAeQAoAtAoBZQAoBbALA/QAIAogHAfQgIAmgcAQQgQAKgUAAQgTAAgTgIgArSxUQgJgeAAg8IgBjOQgBg1AKgbQAHgUAOgOQAQgPATgDQAYgDAWAPQAUAOAKAYQAJAUADAaQACASgBAfIgBCxQgBAkgBASQgDAegJAWQgKAbgTATQgVAUgZAFQgngTgPg0gAlTwaQgQgKgJgRQgOgaABgnIAGhGIACg3QABghAEgVQALhCAqgbQAagSAYAIQAOAFAKANQAJAMAEAQQADANAAARIgBAfQAABQgCAoQgEBFgYAsQgWAogfADIgFAAQgPAAgOgJgAv3wkQgdgPgLgnQgIgcABgtIACkmQAAgnAGgXQAIghAVgRQAQgNAXgCQAVgCAUAJQAnARAQAtQAJAZADAzQAFBYABAsQABBKgGA7QgIBHgcAlQgSAYgdAJQgNAEgMAAQgQAAgOgHgEgjKgQvQgogLgjgqQgjgsgOg2QgCgJgIguQgGgigJgUQgGgNgMgVIgUgiQgVgsAQgfQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAfAPAtQAIAcALA5IAQBQQAGAhAAAUQAAAcgOATQgNATgYAHQgNADgMAAQgLAAgLgCgA43yDQgfgMgSgdQgPgZgHgkQgEgXgCgqIgHiDQgCgwAFgUQAJgnAcgPQAZgPAgAMQAeALARAaQAPAXAIAhQAEAVAEAnIAMCEIADAmQAAAWgDAQQgJAsggARQgPAIgQAAQgOAAgRgHgAzyykQgRgGgMgOQgQgUgFgfQgEgUABgkIABi1QAAgbABgOQACgXAHgQQAIgUAPgOQAQgPATgCQAYgCAWASQAUAQAIAaQAIAUABAdIAAAzIAFBrQACA/gMArQgGAagOATQgQAWgXAFIgLABQgMAAgMgFgA+iypQgUgGgQgOQgZgVgXgvQgVgqgNgjQgQgtgKg0QgLhAAXgdQAPgTAbgEQAZgFAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAhABAZQACAggOAbQgPAdgcAIQgJADgKAAQgLAAgLgDgEAkagVcQgxgHg0goQgTgPhChAIgjgjQgUgUgKgSQgNgXgDgYQgCgbALgUQAPgcAigKQAhgJAhALQAcAJAdAWQARAOAfAfIBfBdQAaAZALATQARAcgDAZQgCARgMAOQgKAOgQAIQgWALgZAAQgKAAgLgBgEAoQgZ3QgZgJgcgWQgtglgigqIgUgZQgMgOgLgIQgJgHgSgKIgbgRQgUgNgNgTQgNgUgBgWQgBggAZgZQAZgZAhgFQA5gIBDAsQA0AiAmAxQAnAxAVA6QANAngDAcQgBARgJAPQgJAQgOAIQgOAHgQAAQgNAAgOgEgEAtRgcpQgkgHgtgnQhnhZhAh9QgrhTAigrQATgZAlgCQAigBAdATQAYAPAXAeQAOARAZAlQARAXAoAxQAmAvASAaQA1BOgfAuQgMATgZAHQgNADgNAAQgKAAgKgCgEA9fgdsQgRgGgOgNQgUgTgNgfQgJgWgIglQgZhsgMhkIgJhGIgLg0IgKgzQgEgkgEgMQgEgNgKgaQgKgkASghQAMgWATgIQARgJAWAEQAUADASAMQAeASATAfQAMATAIAcQAFAQAHAjIAOBGIAbCcQAMBOADAnQAEAugBAvQgBAogIAUQgNAigdAJQgHACgIAAQgKAAgKgDgEAxTgfsQgegagZgyQgag6gPgcIgegzQgSgegIgXQgJgeAEgdQAFggAVgSQAVgSAfAAQAdgBAaAQQAnAWAkA9QArBHAcBPQAUA8gGAlQgEAagPAUQgRAVgXAFQgIACgIAAQgeAAgfgagEA3hgfbQgVgNgPgXQgOgXgKgfIgQg6Qgfh3gthzQgNgggDgQQgHgbAHgVQAIgdAcgPQAdgPAcAJQAaAIAUAcQAOATAPAjQAbA+AWA/QAaBGAKAzQANBDgHA5QgHA9glANQgIADgIAAQgPAAgQgJg");
    var mask_graphics_126 = new cjs.Graphics().p("EAlXAonQgvgNgoguQgggkgZg2QgRgjgXhDQiNmShym9QgNg1AEgbQAGgjAegZQAegZAkABQAdABAcASQAaAQATAaQAQAWANAgIAUA7IBkE4QA8C7AgBdQA1CbAxB6IAUAyQAJAcACAXQADAcgHAZQgIAcgTARQgSARgcAGQgLACgKAAQgQAAgQgFgEgF8AlSQgggdgJgpQgEgOgBgVIgBgjQgBgegGg6IgGg1QgHg7gWhqQgXhxgHgzQgFgggDgpQgDgnAEgWQAGgdASgVQAUgXAbgEQAZgDAZAOQAXANAPAXQANATAJAcQAFARAIAjIAyDrQASBVADAsIAEAwIAGAXQAEAPABAJQACANgBAbQgBAaACANIAFAZIAEAZQAEAsgaAhQgNAQgSAKQgTAKgUABIgFAAQglAAgfgbgEgLgAlgQgcgDgXgNQgkgUgMgjQgFgPgBgWIAAglQgBgQgEgbIgGgrQgCgZADhIQAEhJgIgxIgLg7QgFgegBhGIgBiHQAAgnAGgSQALgcAdgPQAegOAcAJQAiALAXAnQAOAYANAwQALAqADAaIAEAnIADAmQADARAJAjQAIAiADASQAFAggCA9QgCBAAEAdIAGAmIAFAlQACAXACAtIADAgQABASgCANQgEAbgSAVQgSAVgZAKQgTAIgUAAIgNgBgEAPUAlXQgmgSgUgvQgHgSgGgdIgIgwQgIgrgdhQIgohyQgYhIgkiMIgXhXQgShDAagbQANgMATgCQASgDASAHQAbAMAaAkQAeApAYBBQAOAlAZBMQAMAhAnBhQAhBRAPAyQAMAlASBMQAMA2gDAcQgDAxghAXQgSANgXABIgEAAQgVAAgTgJgEAIqAlLQgWgEgQgNQgXgTgNgjQgIgWgIgrIgLhEIgLg/QgFgWgzi5Qgih9AAhVQAAglAJgUQAIgQARgLQAQgLASgCQATgCASAIQARAIALAPQALAPAJAfIBRESIAaBcQARA+AZB4QAHAiACASQABAdgIAWQgIATgQAPQgQAPgUAFQgLADgMAAQgJAAgKgCgEABlAk8QgngLgPgjQgIgQgDglQgJhPgKg/QgMhHgNgzIgXhKQgQgxgHgaQgPg5gIg7QgLhSAhgjQAMgMARgFQARgGAQADQAkAHAXAlQAPAXAOAwIBJD4IATBNQAJAgAHAnIAVCEQADAYAAAMQABAUgGAPQgMAfgmAQQgWAJgVAAQgOAAgOgFgEgaAAk5QgXgHgSgRQgTgRgJgXQgLgeAFgpQACgUANgzQAWhaAQh7QANhsAHiJQAEhMAFiqQABgoAJgRQAJgTAVgLQAVgKAWACQAVACATAOQASAMALATQAMAUAFAcQADARACAjIAMDsQAGBkgEA4QgBASgHBCIgYDLQgGA7gMAiQgRAxgjAXQgUAOgZAEIgQABQgQAAgQgFgEAVjAktQgYgLgPgUQgLgQgLgfIh6lIQgdhOgOgtQgWhEgJg6QgFghAEgSQAFgcAYgTQAYgUAdgBQAcgBAaAQQAZAQANAaQAGAMAFARIAHAeQAMAsAfBBQArBaAGAQQANAgAVBFQAVBCAOAiIAVAwQAMAcAGAVQARA7gWArQgMAXgWAOQgWAOgZACIgHAAQgVAAgUgKgEgTnAkLQgggWgLgiQgIgWAAgeIACg1QAEg5gGhuQgHhyACg1IACgxQABgdgBgUIgFguQgEgcACgTQACgNAEgSIAIgeIAMgwQAJgaAOgQQAMgOASgJQASgIATAAQApABAbAfQAWAZAFAuQADAaABA1QACAgAIBAQACAagBA2QgBA1ACAbIAJBOQAJBHABCRIgBAoQgBAWgGAQQgLAiggAXQgfAWgkABIgBAAQgjAAgfgWgEAdHAjxQgbgLgZgaQgiglgjhBQh8jlg1j9QgOhAAIgjQAGgZAQgTQASgVAYgEQAVgEAXAKQAUAKAPASQANAQALAXQAGAOAKAcIA0CTQAbBLAOAkQAYA9AYAuIAyBhQAbA4AGAtQAFAkgIAaQgLAjgbANQgOAHgQAAQgPAAgRgGgEgjRAhBQgYgEgVgOQgVgOgNgVQgPgagBghQgCgfAMgfQALgcAmg3QCRjMBPicIAnhOQAXgtAUgfQAXgjAXgMQAVgKAYAEQAXADASAPQARAOAKAWQAJAUABAYQAAAXgNAsQgkB6hAB1QgcA0hIBxQhDBpgfA9IgQAfQgKARgLALQgRARgZAJQgRAFgRAAIgPgBgEgmvAYbQgfgCgbgVQgbgVgKgeQgKgeAHghQAGggAVgZQARgTAsgeIBIgwQBBgrAagaIAngrQAXgbASgNQAcgUAsgNQAYgHAPABQASACAQAMQAPAMAKARQAMAXgBAWQgCARgMASQgHAMgRATQg8BDgVAhQgNAWgIAKQgTAbgvAoIhKA/QgmAggYAOQgjAUggAAIgFAAgEgq6ASkQghgJgVgZQgVgagDgjQgCgjARgcQARgaAtgeQAlgYBTgzICbheQAcgSARgIQAagMAXgEQAbgFAZAHQAbAIAOAUQAQAWgCAfQgCAcgQAZQgNAVgZAUQgOAMggAWIi4B7QgeAUgUAPIgjAeQgVAQgSAHQgSAHgSAAQgOAAgPgEgEgt6ANsQgXgKgPgTQgPgUgEgZQgEgZAJgXQALgfAigbQAPgMAxgeQBMgtA/g4IAogjQAXgTATgMQA3ggAvAKQAaAFAUAUQAUAVABAZQABAWgNAXQgJAQgTAWQg2A9hLA9Qg6AvhWA6QgbASgOAHQgYAMgVABIgJABQgUAAgTgJgEgyhAL1QglgJgTgdQgZgmANg1QAJglAhgwQAthBBEhEQAwgwBShFQA3gwAlgWQAfgSAYgGQAXgFAXAFQAYAGARAOQATAPAHAXQAHAYgHAWQgFAPgNAPQgJAKgRAPIhnBYQg8A1gnAoQg+A/giA5IgdAxQgSAbgUAMQgVANgYAAQgLAAgMgDgEg3BAHvQgWgGgQgQQgPgQgHgWQgHgVADgXQADgcAXgmQAWgkAhgoIA+hHQAkgoAUgTQAsgkAVgTIAdgbQASgRANgJQApgaAjAGQAjAFASAcQATAdgKAhQgFATgWAcQgcAjhhBiQhRBRgkA5QgdAugHAIQgXAcgaAIQgLAEgLAAQgLAAgLgDgEg8PAFRQgbgUgDggQgBgSAKgdQAQgwAXgoQAOgZAagjQBBhaAigoQAignAdgXQAdgYAfgLQAkgMAfAIQAdAIATAXQARAUAAAWQAAAZgUAZIgUASIgUASQgaAZggArIg0BIIgbAjQgRAVgJAPQgMATgXApQgVAhgYAQQgYAPgbABIgFAAQgeAAgXgRgEhARAAlQgXgHgOgXQgOgUABgZQAAguApgoQAXgXA5ghIBzhDQAugbAcgFQAVgEATAGQAVAGAMAOQAVAagJAmQgHAbgcAeQgTAVgdAVQgRAMglAXIhrBDQgeATgTAHQgRAGgPAAQgKAAgKgDgEhA7gFAQgdgRgHgeQgKgpAjgwQAog4BFgbQBCgbBHAIQAwAFAeAUQASAMALASQAMASAAAUQACAjgdAcQgbAagnAHQgMADgrADQgjADgTAHQgNAFghATQgcAQgSAEQgIABgIAAQgWAAgWgMgASXoxQgWgLgOgVQgMgTgGgaQgEgRgDgfQgNhngEhHQgGhgAGhQQADgwANgZQAJgRAQgLQARgMASABQATAAAQAPQAQANAHATQAHARACAWIACAoQACBGAJBiIARCnQAFA0gHAYQgFAUgMAOQgNAQgSAFQgIADgIAAQgOAAgPgIgAWppgQgYgHgQgTQgNgRgJgZQgGgPgHgfQglixgMi8QgCgqALgTQAMgTAYgHQAYgGAWAJQAeANARAmQAMAaAIAtQAUBxAYDCQAHA2gFAaQgIAmgbAOQgLAGgNAAQgKAAgLgEgEg7jgJzIgXgOQgNgJgJgEQgWgJggABIgjADQgWACgNAAQgqgBgWgVQgTgSgBgdQgBgbAOgYQAZgnA9gVQBJgYBLASQBOASAsA6QASAWAHAYQAIAbgHAYQgGAUgRAPQgQAPgVAGQgNADgNAAQgcAAgcgQgAJkprQgYgIgTgjQgcg2AAhSQAAgOAKiCIAFg6QAGggAMgWQAOgaAcgNQAcgNAZAMQAsAVgBBTQAAA0gFAvIgFAsQgCAaAAASIAEA9QACAkgGAYQgIAfgZAUQgTAPgTAAQgIAAgJgDgANzqdQgYgKgOgWQgNgSgHgaQgFgQgFggQgJg3AAgcQAAgWAKg+QAFgdAEgOQAGgXALgQQAMgUAUgKQAVgKAUAFQATAFANASQAMAQAEAWQADARgBAYIgCApQgBAeAEAqIAIBHQAGAtgHAaQgFATgLANQgMAQgQAGQgJADgKAAQgNAAgOgGgAE4rYQgcgPgIglQgGgXADgrQAGhRANg4QARhKAig1QASgcAXgPQAbgSAaAIQATAGAMAUQAKATABAWQABATgGAXIgMApQgQA1gFAvQgDA8gFAeQgJA1gdAbQgRAQgXAEQgHACgIAAQgPAAgNgHgAa8sSQg2gmgthWIhLiUIgdgtQgRgagHgVQgJgbAFgbQAEgdAUgQQARgPAagBQAXgBAXAKQAiAQAjAuQArA5AvBlQAhBKAXBAQAQArgCAbQgBATgJAQQgKARgQAGQgJAFgMAAQgYAAgegVgAAOsiQgcgPgOgeQgTgrAOhPQANhCAmh8QAPg0ARgXQANgSARgLQATgMATAAQAdAAAYAYQAWAXAGAfQAFAagGAiQgDAUgLAnIg4DBQgJAggKATQgPAagWAKQgLAFgNAAQgQAAgSgJgEg3GgNbQgagJghgZIg2gqQgPgJgggRQgbgQgPgQQgeggAAgoQABgVAJgSQAJgTARgLQAfgVA4AIQBEAKA6AnQA7AmAjA6QAbAtgFAjQgDAUgOARQgOARgUAIQgTAJgVAAQgUAAgWgIgAj9ulQgpgbALhaIAQiUQAEgjADgRQAGgdAKgVQANgZAUgQQAWgSAZgCQAdgCAZAUQAYAVADAdQACAQgFAUIgKAjQgIAhgGA8QgGBEgGAbQgMA8gdAbQgTASgZAEIgOACQgSAAgOgKgEgw7gO9QgagNgYgbQgRgVgthGQgmg6gfgcIgwgsQgYgbADgbQAEgkAxgaQA5gfAsANQAWAGAVATQAPANAUAZQBbBuAaBaQAOAvgHAgQgEAUgMAQQgNARgSAGQgKAEgLAAQgSAAgUgKgAfkvGQgdgIgegWQiNhqhOjbQgWg7AFgkQADgZAPgUQAQgVAWgGQATgEAVAIQATAHAQAPQANAMAMAUQAHAKANAZQBEB+BZB3QAWAdAHAMQAPAYAGAWQAHAagHAYQgHAbgVANQgQALgUAAQgLAAgNgEgEgq8gPOQgfgOgbgkQgcglgnhfQglhaghgnIgrgxQgWgdACgbQACggAhgUQAegTAkABQAlACAlAUQAhATAcAeQAoAtAoBZQAoBbALA/QAIAogHAfQgIAmgcAQQgQAKgUAAQgTAAgTgIgAt/xTQgJgeAAg8IgBjOQgBg1AKgbQAHgUAOgOQAQgPATgDQAYgDAWAPQAUAOAKAYQAJAUADAaQACASgBAfIgBCxQgBAkgBASQgDAegJAWQgKAbgTATQgVAUgZAFQgngTgPg0gAoAwZQgQgKgJgRQgOgaABgnIAGhGIACg3QABghAEgVQALhCAqgbQAagSAYAIQAOAFAKANQAJAMAEAQQADANAAARIgBAfQAABQgCAoQgEBFgYAsQgWAogfADIgFAAQgPAAgOgJgAykwjQgdgPgLgnQgIgcABgtIACkmQAAgnAGgXQAIghAVgRQAQgNAXgCQAVgCAUAJQAnARAQAtQAJAZADAzQAFBYABAsQABBKgGA7QgIBHgcAlQgSAYgdAJQgNAEgMAAQgQAAgOgHgEgl3gQuQgogLgjgqQgjgsgOg2QgCgJgIguQgGgigJgUQgGgNgMgVIgUgiQgVgsAQgfQAMgZAfgJQAcgIAgAHQBDAQApA2QAXAfAPAtQAIAcALA5IAQBQQAGAhAAAUQAAAcgOATQgNATgYAHQgMAEgNAAQgLAAgLgDgA7kyCQgfgMgSgdQgPgZgHgkQgEgXgCgqIgHiDQgCgwAFgUQAJgnAcgPQAZgPAgAMQAeALARAaQAPAXAIAhQAEAVAEAnIAMCEIADAmQAAAWgDAQQgJAsggARQgOAIgQAAQgPAAgRgHgA2fyjQgRgGgMgOQgQgUgFgfQgEgUABgkIABi1QAAgbABgOQACgXAHgQQAIgUAPgOQAQgPATgCQAYgCAWASQAUAQAIAaQAIAUABAdIAAAzIAFBrQACA/gMArQgGAagOATQgQAWgXAFIgLABQgMAAgMgFgEghPgSoQgUgGgQgOQgZgVgXgvQgVgqgNgjQgQgtgKg0QgLhAAXgdQAPgTAbgEQAZgFAYAKQAqARAZA1QAKASAJAcIARAvQAPAlAGATQAMAhABAZQACAggOAbQgPAdgcAIQgJADgKAAQgKAAgMgDgEAhtgVbQgxgHg0goQgTgPhChAIgjgjQgUgUgKgSQgNgXgDgYQgCgbALgUQAPgcAigKQAhgJAhALQAcAJAdAWQARAOAfAfIBfBdQAaAZALATQARAcgDAZQgCARgMAOQgKAOgQAIQgVALgaAAQgKAAgLgBgEAljgZ2QgZgJgcgWQgtglgigqIgUgZQgMgOgLgIQgJgHgSgKIgbgRQgUgNgNgTQgNgUgBgWQgBggAZgZQAZgZAhgFQA5gIBDAsQA0AiAmAxQAnAxAVA6QANAngDAcQgBARgJAPQgJAQgOAIQgOAHgQAAQgMAAgPgEgEAqkgcoQgkgHgtgnQhnhZhAh9QgrhTAigrQATgZAlgCQAigBAdATQAYAPAXAeQAOARAZAlQARAXAoAxQAmAvASAaQA1BOgfAuQgMATgZAHQgMAEgOAAQgKAAgKgDgEA6ygdrQgRgGgOgNQgUgTgNgfQgJgWgIglQgZhsgMhkIgJhGIgLg0IgKgzQgEgkgEgMQgEgNgKgaQgKgkASghQAMgWATgIQARgJAWAEQAUADASAMQAeASATAfQAMATAIAcQAFAQAHAjIAOBGIAbCcQAMBOADAnQAEAugBAvQgBAogIAUQgNAigdAJQgHACgIAAQgKAAgKgDgEAumgfrQgegagZgyQgag6gPgcIgegzQgSgegIgXQgJgeAEgdQAFggAVgSQAVgSAfAAQAdgBAaAQQAnAWAkA9QArBHAcBPQAUA8gGAlQgEAagPAUQgRAVgXAFQgIACgIAAQgeAAgfgagEA00gfaQgVgNgPgXQgOgXgKgfIgQg6Qgfh3gthzQgNgggDgQQgHgbAHgVQAIgdAcgPQAdgPAcAJQAaAIAUAcQAOATAPAjQAbA+AWA/QAaBGAKAzQANBDgHA5QgHA9glANQgHADgJAAQgPAAgQgJgEBAFgfeQgXgGgSgRQgcgbgQg3QgLgmgRhsQgQhdgVgzIgRgoQgLgYgEgRQgGgWACgVQACgYANgQQAPgUAagHQAZgGAZAHQAsANAhAwQASAaAOAkQAJAZAMAqQAXBSAKA+QANBPgEBDQgDBPgrAVQgNAHgPAAQgJAAgJgCg");
    var mask_graphics_128 = new cjs.Graphics().p("EAi3ApEQgugNgoguQgggkgag2QgQgjgYhDQiNmShxm9QgNg1AEgbQAFgjAegZQAegZAkABQAeABAcASQAZAQATAaQAQAWANAgIAUA7IBkE4QA8C7AgBdQA1CbAyB6IATAyQAJAcADAXQADAcgIAZQgIAcgSARQgTARgcAGQgKACgLAAQgQAAgQgFgEgIcAlvQgggdgJgpQgDgOgBgVIgCgjQgBgegGg6IgGg1QgHg7gVhqQgYhxgHgzQgEgggEgpQgDgnAFgWQAFgdATgVQAUgXAagEQAZgDAZAOQAXANAQAXQANATAJAcQAFARAHAjIAyDrQASBVADAsIAFAwIAFAXQAEAPABAJQACANgBAbQAAAaABANIAFAZIAFAZQAEAsgaAhQgNAQgTAKQgTAKgUABIgFAAQgkAAgggbgEgOAAl9QgbgDgYgNQgkgUgMgjQgFgPAAgWIgBglQgBgQgEgbIgFgrQgDgZAEhIQADhJgIgxIgLg7QgFgegBhGIgBiHQAAgnAHgSQALgcAdgPQAdgOAdAJQAiALAXAnQAOAYAMAwQALAqAEAaIADAnIAEAmQACARAJAjQAJAiACASQAFAggBA9QgCBAADAdIAGAmIAGAlQACAXABAtIADAgQABASgCANQgDAbgTAVQgRAVgaAKQgSAHgVAAIgNAAgEAM1Al0QgngSgTgvQgIgSgFgdIgJgwQgIgrgdhQIgohyQgXhIgliMIgWhXQgShDAagbQAMgMATgCQATgDARAHQAcAMAaAkQAdApAZBBQAOAlAZBMQAMAhAnBhQAgBRAQAyQALAlASBMQAMA2gCAcQgEAxggAXQgSANgYABIgEAAQgUAAgTgJgEAGKAloQgVgEgRgNQgXgTgNgjQgIgWgHgrIgLhEIgMg/QgEgWgzi5Qgjh9AAhVQAAglAKgUQAIgQAQgLQAQgLATgCQATgCARAIQASAIALAPQAKAPAJAfIBSESIAaBcQAQA+AaB4QAHAiABASQACAdgJAWQgHATgQAPQgRAPgUAFQgLADgLAAQgKAAgKgCgEgA6AlZQgmgLgQgjQgIgQgDglQgIhPgLg/QgMhHgOgzIgXhKQgQgxgHgaQgPg5gHg7QgLhSAhgjQAMgMARgFQARgGAQADQAkAHAYAlQAOAXAOAwIBID4IAUBNQAIAgAHAnIAVCEQAEAYAAAMQAAAUgGAPQgMAfglAQQgVAIgVAAQgPAAgOgEgEgcfAlWQgXgHgTgRQgSgRgJgXQgMgeAFgpQADgUAMgzQAXhaAPh7QANhsAHiJQAEhMAFiqQABgoAJgRQAKgTAVgLQAUgKAWACQAWACASAOQATAMALATQALAUAFAcQADARACAjIAMDsQAGBkgDA4QgBASgHBCIgYDLQgHA7gMAiQgRAxgiAXQgUAOgZAEIgRABQgQAAgPgFgEATDAlKQgYgLgOgUQgLgQgMgfIh5lIQgehOgOgtQgVhEgKg6QgFghAEgSQAGgcAYgTQAXgUAdgBQAcgBAaAQQAaAQANAaQAFAMAFARIAIAeQALAsAfBBQArBaAHAQQAMAgAWBFQAUBCAOAiIAWAwQAMAcAGAVQAQA7gWArQgLAXgWAOQgWAOgZACIgHAAQgWAAgUgKgEgWHAkoQgfgWgMgiQgHgWgBgeIADg1QADg5gGhuQgGhyACg1IACgxQABgdgCgUIgFguQgDgcACgTQABgNAEgSIAIgeIANgwQAJgaANgQQANgOASgJQASgIATAAQApABAbAfQAWAZAFAuQADAaAAA1QACAgAIBAQACAaAAA2QgBA1ACAbIAJBOQAJBHABCRIgBAoQgCAWgFAQQgMAigfAXQgfAWgkABIgCAAQgjAAgfgWgEAanAkOQgagLgagaQgiglgjhBQh7jlg2j9QgNhAAIgjQAFgZARgTQASgVAXgEQAWgEAWAKQAVAKAPASQANAQAKAXQAHAOAKAcIA0CTQAbBLAOAkQAYA9AXAuIAyBhQAbA4AHAtQAFAkgIAaQgLAjgcANQgOAHgQAAQgPAAgRgGgEglwAheQgZgEgVgOQgVgOgMgVQgQgagBghQgBgfALgfQALgcAmg3QCRjMBPicIAnhOQAYgtAUgfQAXgjAXgMQAUgKAYAEQAXADASAPQASAOAKAWQAJAUAAAYQABAXgNAsQglB6g/B1QgcA0hJBxQhDBpgfA9IgQAfQgKARgLALQgRARgYAJQgRAFgRAAIgPgBgEgpOAY4QgggCgbgVQgagVgKgeQgLgeAHghQAHggAVgZQAQgTAsgeIBJgwQBAgrAagaIAngrQAYgbASgNQAbgUAtgNQAXgHAPABQATACAQAMQAPAMAKARQAMAXgCAWQgBARgMASQgIAMgQATQg9BDgVAhQgNAWgHAKQgTAbgvAoIhLA/QglAggZAOQgjAUgfAAIgFAAgEgtZATBQgigJgVgZQgVgagCgjQgCgjARgcQAQgaAugeQAkgYBTgzICbheQAdgSARgIQAZgMAXgEQAbgFAZAHQAbAIAPAUQAQAWgCAfQgCAcgQAZQgOAVgYAUQgPAMgfAWIi4B7QgfAUgTAPIgkAeQgVAQgSAHQgRAGgTAAQgOAAgOgDgEgwZAOJQgYgKgOgTQgPgUgEgZQgEgZAIgXQALgfAigbQAQgMAwgeQBMgtA/g4IAogjQAXgTAUgMQA2ggAvAKQAbAFATAUQAUAVABAZQABAWgMAXQgJAQgTAWQg2A9hLA9Qg7AvhVA6QgbASgOAHQgYAMgWABIgJABQgTAAgTgJgEg1AAMSQgmgJgTgdQgYgmANg1QAJglAhgwQAshBBEhEQAxgwBRhFQA4gwAkgWQAggSAXgGQAYgFAXAFQAXAGASAOQASAPAHAXQAIAYgIAWQgFAPgNAPQgIAKgRAPIhnBYQg8A1gnAoQg/A/giA5IgdAxQgSAbgUAMQgUANgYAAQgLAAgMgDgEg5gAIMQgWgGgQgQQgQgQgHgWQgGgVACgXQAEgcAXgmQAVgkAhgoIA+hHQAkgoAVgTQArgkAVgTIAegbQARgRAOgJQAogbAkAGQAiAFATAdQASAdgJAhQgGATgVAcQgdAjhhBiQhQBRglA5QgdAugHAIQgWAcgbAIQgKADgMAAQgKAAgLgCgEg+uAFuQgcgUgCggQgBgSAJgdQAQgwAYgoQAOgZAZgjQBBhbAignQAignAdgXQAegYAegLQAkgMAfAIQAeAIASAXQARAUABAWQAAAZgVAYIgTATIgVASQgaAZgfArIg0BIIgcAjQgQAVgKAPQgMATgWApQgWAhgXAQQgYAPgbABIgFAAQgeAAgXgRgEhCxABCQgWgHgPgXQgNgVAAgYQABguAogoQAXgXA5ghIB0hDQAtgbAcgFQAVgEAUAGQAVAGAMAOQAVAagKAmQgHAbgbAeQgUAVgcAVQgSAMgkAXIhrBDQgeATgTAHQgRAFgPAAQgLAAgKgCgEhDagEjQgdgRgIgeQgJgpAigwQAog4BFgbQBCgbBHAIQAwAFAeAUQASAMALASQAMASABAUQACAjgeAcQgbAagmAHQgNADgrADQgiADgUAHQgNAFggATQgcAQgTAEQgIABgIAAQgWAAgVgMgAP3oUQgWgLgOgVQgLgTgGgaQgEgRgEgfQgMhngEhHQgGhgAFhQQADgwANgZQAKgRAPgLQARgMASABQATAAARAPQAPANAIATQAGARADAWIACAoQABBGAKBiIAQCnQAFA0gGAYQgFAUgMAOQgOAQgRAFQgIADgIAAQgPAAgPgIgAUJpDQgYgHgPgTQgOgRgJgZQgFgPgHgfQgmixgLi8QgCgqALgTQALgTAZgHQAYgGAVAJQAeANASAmQAMAaAIAtQATBxAZDCQAHA2gGAaQgIAmgbAOQgLAGgNAAQgKAAgLgEgEg+DgJWIgWgOQgOgJgJgEQgVgJggABIgjADQgWACgNAAQgqgBgXgVQgSgSgBgdQgCgbAPgYQAYgnA9gVQBJgYBLASQBPASAsA6QARAWAHAYQAIAbgHAYQgGAUgQAPQgQAPgVAGQgNADgNAAQgcAAgdgQgAHFpOQgZgIgSgjQgdg2AAhSQAAgOAKiCIAGg6QAFggAMgWQAPgaAbgNQAdgNAZAMQAsAVgBBTQAAA0gFAvIgFAsQgDAaAAASIAEA9QACAkgGAYQgHAfgaAUQgSAPgTAAQgJAAgIgDgALTqAQgXgKgPgWQgMgSgIgaQgEgQgGggQgJg3ABgcQAAgWAKg+QAFgdAEgOQAGgXAKgQQANgUATgKQAVgKAUAFQATAFAOASQAMAQAEAWQADARgBAYIgDApQgBAeAEAqIAIBHQAGAtgHAaQgEATgLANQgMAQgQAGQgJADgKAAQgNAAgPgGgACZq7QgcgPgJglQgFgXADgrQAGhRANg4QARhKAhg1QASgcAXgPQAcgSAZAIQATAGAMAUQALATABAWQAAATgFAXIgNApQgQA1gFAvQgCA8gFAeQgJA1gdAbQgRAQgXAEQgIACgHAAQgPAAgNgHgAYdr1Qg2gmgthWIhMiUIgdgtQgQgagHgVQgJgbAEgbQAFgdATgQQASgPAZgBQAYgBAWAKQAiAQAjAuQAsA5AuBlQAiBKAWBAQAQArgBAbQgBATgKAQQgJARgQAGQgKAEgLAAQgYAAgegUgAiQsFQgegPgNgeQgUgrAPhPQANhCAmh8QAQg0ARgXQANgSAQgLQATgMAUAAQAdAAAWAYQAWAXAGAfQAFAagFAiQgEAUgLAnIg3DBQgJAggKATQgOAagWAKQgMAFgNAAQgQAAgRgJgEg5mgM+QgZgJghgZIg3gqQgPgJgfgRQgcgQgPgQQgeggABgoQAAgVAJgSQAKgTAQgLQAggVA4AIQBDAKA7AnQA6AmAkA6QAbAtgGAjQgDAUgOARQgOARgUAIQgTAIgVAAQgUAAgWgHgAmduIQgogbAKhaIAQiUQAEgjAEgRQAFgdALgVQAMgZAUgQQAWgSAZgCQAdgCAZAUQAZAVADAdQABAQgFAUIgJAjQgJAhgGA8QgGBEgFAbQgMA8geAbQgTASgYAEIgOABQgSAAgPgJgEgzagOgQgbgNgXgbQgSgVgthGQglg6gfgcIgwgsQgYgbADgbQAEgkAwgaQA5gfAsANQAXAGAVATQAOANAVAZQBaBuAbBaQANAvgGAgQgEAUgMAQQgNARgSAGQgLAEgLAAQgSAAgTgKgAdFupQgdgIgegWQiOhqhOjbQgVg7AEgkQADgZAPgUQAQgVAXgGQATgEAUAIQATAHAQAPQANAMAMAUQAHAKAOAZQBDB+BZB3QAWAdAIAMQAPAYAFAWQAHAagHAYQgHAbgUANQgQAKgUAAQgLAAgNgDgEgtbgOxQgfgOgbgkQgcglgnhfQglhagignIgqgxQgWgdACgbQACggAggUQAegTAkABQAmACAkAUQAiATAbAeQAoAtAoBZQAoBbAMA/QAHAogHAfQgIAmgbAQQgQAKgVAAQgTAAgSgIgAwew2QgJgeAAg8IgCjOQgBg1AKgbQAHgUAPgOQAQgPATgDQAXgDAWAPQAUAOALAYQAJAUADAaQABASAAAfIgCCxQAAAkgCASQgDAegIAWQgKAbgTATQgVAUgaAFQgmgTgPg0gAqfv8QgQgKgJgRQgOgaAAgnIAGhGIACg3QABghAEgVQALhCAqgbQAbgSAXAIQAOAFAKANQAKAMADAQQAEANAAARIgCAfQABBQgDAoQgEBFgYAsQgWAogeADIgGAAQgOAAgOgJgA1EwGQgcgPgLgnQgIgcAAgtIADkmQAAgnAFgXQAIghAVgRQAQgNAXgCQAWgCAUAJQAmARARAtQAJAZADAzQAFBYAAAsQACBKgHA7QgHBHgcAlQgTAYgdAJQgNAEgMAAQgPAAgPgHgEgoWgQRQgpgLgigqQgkgsgNg2QgDgJgIguQgGgigJgUQgFgNgNgVIgTgiQgVgsAPgfQAMgZAggJQAcgIAfAHQBDAQApA2QAYAfAOAtQAJAcALA5IAPBQQAHAhAAAUQgBAcgNATQgOATgYAHQgMADgMAAQgLAAgLgCgA+DxlQgggMgSgdQgPgZgGgkQgEgXgCgqIgHiDQgCgwAEgUQAKgnAbgPQAagPAfAMQAeALASAaQAPAXAHAhQAFAVADAnIANCEIACAmQABAWgEAQQgJAsggARQgOAIgQAAQgPAAgQgHgA4+yGQgRgGgMgOQgQgUgGgfQgDgUAAgkIABi1QAAgbACgOQACgXAGgQQAIgUAPgOQAQgPAUgCQAYgCAVASQAUAQAJAaQAHAUABAdIABAzIAEBrQACA/gLArQgHAagOATQgQAWgWAFIgMABQgLAAgMgFgEgjugSLQgUgGgRgOQgYgVgYgvQgVgqgMgjQgRgtgKg0QgLhAAYgdQAPgTAagEQAagFAXAKQAqARAaA1QAJASAKAcIAQAvQAPAlAHATQALAhACAZQACAggPAbQgPAdgbAIQgJADgKAAQgLAAgLgDgAfO0+QgxgHg0goQgTgPhDhAIgjgjQgTgUgLgSQgNgXgCgYQgDgbALgUQAPgcAjgKQAhgJAgALQAcAJAdAWQARAOAgAfIBeBdQAaAZAMATQARAcgEAZQgCARgLAOQgLAOgQAIQgVALgaAAQgJAAgLgBgEAjEgZZQgagJgcgWQgtglghgqIgVgZQgMgOgLgIQgJgHgRgKIgcgRQgUgNgMgTQgOgUgBgWQgBggAagZQAYgZAigFQA5gIBDAsQAzAiAnAxQAnAxAUA6QAOAngDAcQgCARgIAPQgJAQgPAIQgNAHgQAAQgNAAgOgEgEAoEgcLQgjgHgtgnQhnhZhAh9QgrhTAhgrQAUgZAkgCQAigBAeATQAXAPAYAeQANARAZAlQARAXAoAxQAmAvASAaQA2BOgfAuQgNATgYAHQgNAEgNAAQgKAAgLgDgEA4SgdOQgRgGgOgNQgUgTgMgfQgJgWgJglQgYhsgMhkIgKhGIgKg0IgKgzQgFgkgDgMQgEgNgLgaQgJgkARghQAMgWATgIQASgJAWAEQATADATAMQAeASASAfQANATAIAcQAFAQAHAjIAOBGIAaCcQAMBOAEAnQAEAugBAvQgBAogIAUQgNAigdAJQgIACgIAAQgJAAgLgDgEAsHgfOQgegagZgyQgbg6gPgcIgegzQgSgegHgXQgKgeAFgdQAFggAVgSQAVgSAfAAQAdgBAZAQQAnAWAlA9QAqBHAcBPQAUA8gFAlQgEAagQAUQgQAVgYAFQgHACgIAAQgeAAgfgagEAyVge9QgWgNgOgXQgOgXgLgfIgQg6Qgfh3gthzQgMgggEgQQgGgbAGgVQAIgdAdgPQAcgPAcAJQAaAIAUAcQAPATAPAjQAaA+AXA/QAZBGAKAzQAOBDgHA5QgHA9glANQgIADgIAAQgPAAgQgJgEA9mgfBQgYgGgRgRQgdgbgPg3QgLgmgShsQgPhdgVgzIgSgoQgLgYgEgRQgGgWACgVQADgYAMgQQAPgUAagHQAZgGAZAHQAsANAhAwQASAaAOAkQAKAZALAqQAXBSAKA+QANBPgDBDQgDBPgsAVQgNAHgPAAQgIAAgJgCgEBBggfPQgTgLgLgWQgKgTgCgZQgCgRACgcIACgtQABgngKgzIgUhZIgLg2QgLg5gJg6QgEgkAEgRQAHgjAegSQAfgSAhALQASAGAUARQAZAXAQAhQALAXAKApIAiCEQAMAzAHAsQAKA4gDAlQgDAegMAlQgUA6ggAbQgVAQgZAEIgOABQgSAAgQgIg");
    var mask_graphics_130 = new cjs.Graphics().p("EAgUApWQgugNgoguQgggkgag3QgQgigYhDQiNmThxm9QgNg0AEgbQAFgkAegYQAegZAkABQAeAAAcASQAZAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAyB5IATAyQAJAcADAXQADAdgIAZQgIAbgSARQgTASgcAFQgKACgLAAQgQAAgQgEgEgK/AmAQgggcgJgpQgDgPgBgUIgCgkQgBgdgGg6IgGg2QgHg6gVhqQgYhxgHgzQgEgggEgpQgDgoAFgWQAFgcATgWQAUgWAagEQAZgEAZAOQAXANAQAXQANAUAJAcQAFAQAHAjIAyDsQASBVADAsIAFAvIAFAYQAEAOABAJQACAOgBAaQAAAbABANIAFAZIAFAZQAEAsgaAhQgNAQgTAKQgTAKgUABIgFAAQgkAAgggcgEgQjAmOQgbgCgYgOQgkgUgMgjQgFgOAAgWIgBgmQgBgPgEgbIgFgrQgDgaAEhIQADhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAHgSQALgcAdgOQAdgOAdAJQAiAKAXAnQAOAYAMAxQALApAEAaIADAnIAEAnQACARAJAjQAJAiACASQAFAggBA9QgCBAADAdIAGAlIAGAlQACAXABAuIADAfQABATgCANQgDAagTAWQgRAVgaAKQgSAHgUAAIgOgBgEAKSAmGQgngSgTgvQgIgTgFgcIgJgxQgIgrgdhPIgohyQgXhIgliNIgWhXQgShDAagaQAMgNATgCQATgCARAHQAcALAaAlQAdApAZBAQAOAlAZBMQAMAiAnBgQAgBRAQAyQALAlASBMQAMA2gCAdQgEAxggAXQgSANgYABIgEAAQgUAAgTgJgEADnAl6QgVgFgRgNQgXgSgNgjQgIgXgHgrIgLhEIgMg+QgEgXgzi5Qgjh9AAhUQAAgmAKgTQAIgRAQgLQAQgLATgCQATgBARAIQASAIALAPQAKAOAJAgIBSESIAaBbQAQA/AaB3QAHAjABARQACAegJAVQgHAUgQAOQgRAPgUAGQgLADgLAAQgKAAgKgCgEgDdAlrQgmgMgQgiQgIgRgDgkQgIhQgLg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gHg8QgLhSAhgiQAMgMARgGQARgFAQADQAkAGAYAmQAOAWAOAxIBJD3IAUBNQAIAhAHAmIAVCFQAEAXAAANQAAAUgGAOQgMAgglAPQgWAJgVAAQgPAAgOgEgEgfCAloQgXgHgTgSQgSgRgJgXQgMgdAFgpQADgUAMg0QAXhaAPh7QANhsAHiJQAEhMAFiqQABgnAJgRQAKgTAVgLQAUgLAWADQAWACASANQATANALATQALATAFAdQADAQACAjIAMDtQAGBjgDA4QgBATgHBBIgYDMQgHA6gMAiQgRAxgiAYQgUANgZAEIgQACQgRAAgPgFgEAQgAlcQgYgLgOgVQgLgPgMggIh5lHQgehPgOgsQgVhFgKg5QgFghAEgSQAGgcAYgUQAXgTAdgBQAcgCAaARQAaAPANAaQAFAMAFASIAIAeQALAsAfBAQArBaAHARQAMAfAWBFQAUBCAOAiIAWAwQAMAdAGAUQAQA8gWArQgLAWgWAPQgWAOgZABIgHABQgWAAgUgKgEgYqAk6QgfgXgMgiQgHgVgBgeIADg1QADg6gGhtQgGhyACg1IACgxQABgdgCgUIgFgvQgDgcACgSQABgNAEgSIAIgfIANgvQAJgaANgQQANgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaAAA2QACAgAIA/QACAbAAA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgCAWgFARQgMAigfAWQgfAXgkAAIgCAAQgjAAgfgVgEAYEAkfQgagKgagbQgigkgjhCQh7jlg2j9QgNg/AIgkQAFgZARgTQASgUAXgFQAWgEAWALQAVAJAPATQANAPAKAXQAHAOAKAdIA0CSQAbBMAOAjQAYA9AXAvIAyBgQAbA5AHAtQAFAjgIAbQgLAigcANQgOAIgQAAQgPAAgRgHgEgoTAhwQgZgEgVgOQgVgOgMgWQgQgZgBgiQgBgfALgeQALgdAmg2QCRjMBPidIAnhOQAYgtAUgeQAXgkAXgLQAUgKAYADQAXAEASAOQASAOAKAWQAJAVAAAXQABAYgNAsQglB5g/B2QgcAzhJBxQhDBpgfA9IgQAfQgKARgLALQgRASgYAIQgRAGgRAAIgPgBgEgrxAZJQgggBgbgVQgagVgKgfQgLgeAHghQAHggAVgZQAQgTAsgeIBJgwQBAgrAagaIAngrQAYgaASgNQAbgVAtgNQAXgGAPABQATACAQAMQAPALAKASQAMAXgCAVQgBARgMATQgIAMgQASQg9BEgVAgQgNAWgHAKQgTAbgvAoIhLBAQglAggZAOQgjATgfAAIgFAAgEgv8ATSQgigJgVgZQgVgZgCgjQgCgjARgcQAQgaAugeQAkgZBTgyICbhfQAdgRARgIQAZgNAXgEQAbgEAZAHQAbAIAPAUQAQAWgCAeQgCAdgQAZQgOAVgYAUQgPAMgfAVIi4B8QgfATgTAQIgkAdQgVAQgSAHQgRAHgTAAQgNAAgPgEgEgy8AObQgYgLgOgTQgPgTgEgZQgEgZAIgXQALgfAigbQAQgNAwgdQBMgtA/g4IAogjQAXgUAUgMQA2ggAvAKQAbAGATAUQAUAUABAaQABAVgMAXQgJAQgTAWQg2A+hLA8Qg7AwhVA5QgbASgOAHQgYAMgWACIgJAAQgTAAgTgIgEg3jAMkQgmgJgTgeQgYgmANg1QAJglAhgvQAshBBEhEQAxgxBRhFQA4gwAkgWQAggSAXgFQAYgFAXAFQAXAFASAPQASAPAHAXQAIAXgIAWQgFAPgNAPQgIAKgRAPIhnBZQg8A0gnAoQg/BAgiA5IgdAwQgSAbgUANQgUANgYAAQgLAAgMgDgEg8DAIdQgWgGgQgQQgQgPgHgWQgGgWACgWQAEgcAXgnQAVgjAhgpIA+hHQAkgoAVgTQArgjAVgTIAegcQARgQAOgJQAogbAkAFQAiAGATAdQASAdgJAhQgGATgVAbQgdAkhhBhQhQBRglA6QgdAtgHAJQgWAcgbAIQgKADgMAAQgKAAgLgDgEhBRAF/QgcgTgCghQgBgRAJgdQAQgxAYgoQAOgYAZgjQBBhcAignQAigmAdgYQAegYAegKQAkgNAfAIQAeAIASAXQARAUABAWQAAAYgVAZIgTATIgVATQgaAYgfArIg0BIIgcAkQgQAVgKAPQgMATgWAoQgWAigXAPQgYAQgbAAIgFAAQgeAAgXgRgEhFUABTQgWgHgPgWQgNgWAAgZQABgsAogpQAXgXA5ghIB0hDQAtgaAcgGQAVgDAUAFQAVAGAMAPQAVAZgKAmQgHAcgbAdQgUAVgcAVQgSANgkAXIhrBCQgeATgTAHQgRAGgPAAQgLAAgKgDgEhF9gERQgdgSgIgdQgJgqAigwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATABAUQACAigeAcQgbAagmAIQgNACgrAEQgiACgUAIQgNAEggATQgcAQgTAEQgIACgIAAQgWAAgVgMgANUoCQgWgLgOgWQgLgSgGgaQgEgSgEgeQgMhogEhHQgGhfAFhQQADgwANgZQAKgSAPgLQARgLASAAQATABARAOQAPANAIAUQAGAQADAXIACAoQABBGAKBhIAQCoQAFAzgGAZQgFATgMAPQgOAQgRAFQgIACgIAAQgPAAgPgHgARmoxQgYgIgPgTQgOgQgJgZQgFgQgHgfQgmiwgLi8QgCgqALgTQALgUAZgGQAYgHAVAKQAeANASAlQAMAaAIAuQATBxAZDCQAHA1gGAbQgIAlgbAOQgLAGgNAAQgKAAgLgDgEhAmgJEIgWgPQgOgJgJgEQgVgJggABIgjADQgWADgNAAQgqgBgXgVQgSgSgBgdQgCgcAPgXQAYgnA9gVQBJgZBLASQBPATAsA5QARAXAHAYQAIAbgHAXQgGAUgQAQQgQAPgVAFQgNAEgNAAQgcAAgdgQgAEio9QgZgIgSgiQgdg3AAhRQAAgPAKiCIAGg5QAFggAMgWQAPgaAbgNQAdgOAZAMQAsAVgBBUQAAAzgFAvIgFAtQgDAZAAATIAEA9QACAkgGAYQgHAfgaAUQgSAOgTAAQgIAAgJgDgAIwpvQgXgKgPgVQgMgSgIgbQgEgQgGggQgJg3ABgbQAAgXAKg+QAFgdAEgNQAGgYAKgQQANgTATgKQAVgLAUAFQATAFAOASQAMAQAEAWQADARgBAYIgDApQgBAeAEAqIAIBIQAGAtgHAaQgEASgLAOQgMAPgQAGQgJAEgKAAQgNAAgPgHgAgJqpQgcgQgJglQgFgXADgqQAGhSANg4QARhJAgg2QASgcAXgPQAcgSAZAJQATAGAMAUQALASABAXQAAASgFAXIgNApQgQA2gFAvQgCA8gFAdQgJA1gdAbQgRAQgXAFQgIACgHAAQgPAAgMgHgAV6rjQg2gmgthXIhMiTIgdgtQgQgbgHgVQgJgbAEgaQAFgdATgRQASgOAZgCQAYgBAWAKQAiARAjAuQAsA5AuBkQAiBKAWBBQAQArgBAbQgBATgKAPQgJARgQAHQgKAEgLAAQgYAAgegUgAkzrzQgegQgNgdQgUgrAPhPQANhCAmh8QAQg0ARgYQANgSAQgKQATgMAUAAQAdgBAXAZQAWAWAGAfQAFAbgFAhQgEAUgLAoIg4DAQgJAhgKASQgOAagWAKQgMAGgMAAQgRAAgRgJgEg8JgMtQgZgJghgZIg3gpQgPgKgfgRQgcgPgPgQQgeggABgpQAAgUAJgSQAKgTAQgLQAggWA4AJQBDAKA7AmQA6AnAkA6QAbAtgGAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgApAt2QgogbAKhaIAQiVQAEgjAEgRQAFgcALgVQAMgZAUgRQAWgRAZgCQAdgDAZAVQAZAUADAeQABAPgFAUIgJAjQgJAhgGA9QgGBEgFAbQgMA8geAbQgTARgYAFIgOABQgSAAgPgJgEg19gOOQgbgNgXgcQgSgUgthHQglg6gfgbIgwgsQgYgbADgbQAEgkAwgbQA5geAsAMQAXAHAVASQAOANAVAZQBaBvAbBaQANAugGAgQgEAUgMAQQgNARgSAHQgLAEgLAAQgSAAgTgKgAaiuXQgdgIgegXQiOhphOjbQgVg8AEgjQADgZAPgUQAQgWAXgFQATgFAUAIQATAHAQAPQANANAMATQAHALAOAZQBDB+BZB2QAWAeAIAMQAPAYAFAVQAHAagHAZQgHAbgUANQgQAKgUAAQgLAAgNgDgEgv+gOgQgfgOgbgjQgcglgnhfQglhagigoIgqgwQgWgdACgbQACggAggVQAegTAkACQAmACAkAUQAiASAbAfQAoAsAoBaQAoBaAMBAQAHAngHAgQgIAmgbAQQgQAJgVAAQgTAAgSgIgAzBwkQgJgfAAg8IgCjNQgBg1AKgbQAHgUAPgOQAQgPATgDQAXgDAWAPQAUAOALAXQAJAUADAbQABARAAAgIgCCwQAAAlgCARQgDAegIAXQgKAbgTATQgVAUgaAEQgmgTgPgzgAtCvqQgQgKgJgRQgOgaAAgoIAGhFIACg3QABghAEgWQALhBAqgcQAbgRAXAIQAOAEAKANQAKAMADAQQAEANAAASIgCAeQABBRgDAoQgEBFgYAsQgWAngeADIgGABQgOAAgOgJgA3nv1QgcgOgLgnQgIgcAAgtIADknQAAgmAFgXQAIghAVgRQAQgNAXgDQAWgCAUAJQAmASARAsQAJAaADAyQAFBYAAAtQACBKgHA6QgHBIgcAkQgTAYgdAJQgMAEgNAAQgPAAgPgHgEgq5gQAQgpgKgigrQgkgsgNg1QgDgJgIguQgGgigJgUQgFgOgNgVIgTghQgVgtAPgeQAMgZAggJQAcgIAfAHQBDAQApA2QAYAeAOAuQAJAbALA5IAPBRQAHAgAAAUQgBAdgNATQgOASgYAHQgMAEgMAAQgLAAgLgDgEggmgRTQgggNgSgdQgPgZgGgjQgEgXgCgqIgHiEQgCgvAEgUQAKgnAbgQQAagOAfALQAeALASAbQAPAWAHAhQAFAWADAnIANCEIACAmQABAVgEARQgJAsggARQgOAHgQAAQgPAAgQgGgA7hx0QgRgHgMgOQgQgTgGgfQgDgVAAgkIABi1QAAgbACgNQACgXAGgRQAIgUAPgOQAQgOAUgCQAYgCAVARQAUARAJAZQAHAVABAdIABAyIAEBsQACA/gLAqQgHAbgOASQgQAWgWAFIgLACQgMAAgMgFgEgmRgR6QgUgFgRgPQgYgVgYgvQgVgpgMgkQgRgsgKg0QgLhAAYgeQAPgSAagFQAagEAXAKQAqARAaA1QAJASAKAcIAQAvQAPAlAHATQALAgACAZQACAggPAbQgPAegbAIQgJADgKAAQgLAAgLgEgAcr0tQgxgHg0goQgTgOhDhBIgjgiQgTgVgLgSQgNgXgCgYQgDgaALgVQAPgbAjgKQAhgJAgAKQAcAJAdAXQARANAgAfIBeBdQAaAaAMATQARAbgEAaQgCARgLAOQgLAOgQAIQgVALgZAAQgKAAgLgCgEAghgZIQgagIgcgXQgtglghgpIgVgZQgMgOgLgJQgJgHgRgKIgcgQQgUgOgMgSQgOgUgBgWQgBggAagaQAYgZAigEQA5gJBDAsQAzAiAnAxQAnAxAUA6QAOAngDAcQgCASgIAPQgJAPgPAIQgNAIgQAAQgNAAgOgFgEAlhgb5QgjgIgtgmQhnhZhAh+QgrhSAhgsQAUgZAkgBQAigBAeASQAXAQAYAdQANASAZAkQARAYAoAxQAmAuASAaQA2BOgfAvQgNASgYAHQgNAEgNAAQgKAAgLgCgEA1vgc9QgRgGgOgNQgUgSgMgfQgJgWgJglQgYhtgMhkIgKhGIgKgzIgKg0QgFgkgDgMQgEgNgLgZQgJglARggQAMgWATgJQASgIAWADQATAEATALQAeASASAfQANAUAIAcQAFAQAHAjIAOBGIAaCbQAMBOAEAoQAEAugBAuQgBAogIAVQgNAhgdAJQgIADgIAAQgJAAgLgEgEBEggdNQgUgNgNgYQgVgmgIhEQgIhCACiwQACiagUhXQgMgzgBgTQgBgqAXgXQAMgMASgFQARgEASADQAhAFAbAdQAUAUAPAfQAKAVAOAlQAQAwAEAcQAFAcAAAwQACC9gFBeQgDA4gGAjQgKAxgWAiQgUAggYAFIgLABQgQAAgRgLgEApkge9QgegZgZgyQgbg7gPgcIgegzQgSgegHgXQgKgdAFgeQAFggAVgSQAVgSAfAAQAdAAAZAPQAnAXAlA9QAqBHAcBOQAUA8gFAlQgEAagQAUQgQAVgYAGQgHACgIAAQgeAAgfgbgEAvygesQgWgMgOgYQgOgWgLggIgQg5Qgfh4gthyQgMghgEgPQgGgcAGgVQAIgcAdgPQAcgQAcAJQAaAJAUAbQAPATAPAjQAaA/AXA+QAZBGAKAzQAOBDgHA5QgHA+glANQgIACgIAAQgPAAgQgJgEA7DgewQgYgFgRgRQgdgbgPg4QgLglgShtQgPhcgVgzIgSgoQgLgYgEgRQgGgXACgVQADgXAMgRQAPgTAagHQAZgHAZAHQAsANAhAxQASAaAOAkQAKAZALApQAXBSAKA+QANBPgDBEQgDBPgsAVQgNAGgPAAQgIAAgJgCgEA+9ge+QgTgKgLgWQgKgUgCgYQgCgSACgbIACguQABgngKgzIgUhZIgLg2QgLg4gJg7QgEgjAEgSQAHgjAegSQAfgSAhALQASAGAUASQAZAWAQAhQALAYAKAoIAiCFQAMAyAHAsQAKA5gDAlQgDAdgMAmQgUA6ggAaQgVARgZAEIgOABQgSAAgQgJg");
    var mask_graphics_132 = new cjs.Graphics().p("EAeFApWQgugNgoguQgggkgag3QgQgigYhDQiNmThxm9QgNg0AEgbQAFgkAegYQAegZAkABQAeAAAcASQAZAQATAbQAQAWANAgIAUA6IBkE4QA8C8AgBdQA1CbAyB5IATAyQAJAcADAXQADAdgIAZQgIAbgSARQgTASgcAFQgKACgLAAQgQAAgQgEgEgNOAmAQgggcgJgpQgDgPgBgUIgCgkQgBgdgGg6IgGg2QgHg6gVhqQgYhxgHgzQgEgggEgpQgDgoAFgWQAFgcATgWQAUgWAagEQAZgEAZAOQAXANAQAXQANAUAJAcQAFAQAHAjIAyDsQASBVADAsIAFAvIAFAYQAEAOABAJQACAOgBAaQAAAbABANIAFAZIAFAZQAEAsgaAhQgNAQgTAKQgTAKgUABIgFAAQgkAAgggcgEgSyAmOQgbgCgYgOQgkgUgMgjQgFgOAAgWIgBgmQgBgPgEgbIgFgrQgDgaAEhIQADhIgIgyIgLg7QgFgdgBhHIgBiGQAAgoAHgSQALgcAdgOQAdgOAdAJQAiAKAXAnQAOAYAMAxQALApAEAaIADAnIAEAnQACARAJAjQAJAiACASQAFAggBA9QgCBAADAdIAGAlIAGAlQACAXABAuIADAfQABATgCANQgDAagTAWQgRAVgaAKQgSAHgVAAIgNgBgEAIDAmGQgngSgTgvQgIgTgFgcIgJgxQgIgrgdhPIgohyQgXhIgliNIgWhXQgShDAagaQAMgNATgCQATgCARAHQAcALAaAlQAdApAZBAQAOAlAZBMQAMAiAnBgQAgBRAQAyQALAlASBMQAMA2gCAdQgEAxggAXQgSANgYABIgEAAQgUAAgTgJgEABYAl6QgVgFgRgNQgXgSgNgjQgIgXgGgrIgLhEIgMg+QgEgXgzi5Qgjh9AAhUQAAgmAKgTQAIgRAQgLQAQgLATgCQATgBARAIQARAIALAPQAKAOAJAgIBSESIAaBbQAQA/AaB3QAHAjABARQACAegJAVQgHAUgQAOQgRAPgUAGQgLADgMAAQgJAAgKgCgEgFsAlrQgmgMgQgiQgIgRgDgkQgIhQgLg/QgMhHgOgyIgXhLQgQgwgHgbQgPg4gHg8QgLhSAhgiQAMgMARgGQARgFAQADQAkAGAYAmQAOAWAOAxIBJD3IAUBNQAIAhAHAmIAVCFQAEAXAAANQAAAUgGAOQgMAgglAPQgWAJgVAAQgPAAgOgEgEghRAloQgXgHgTgSQgSgRgJgXQgMgdAFgpQADgUAMg0QAXhaAPh7QANhsAHiJQAEhMAFiqQABgnAJgRQAKgTAVgLQAUgLAWADQAWACASANQATANALATQALATAFAdQADAQACAjIAMDtQAGBjgDA4QgBATgHBBIgYDMQgHA6gMAiQgRAxgiAYQgUANgZAEIgRACQgQAAgPgFgEAORAlcQgYgLgOgVQgLgPgMggIh5lHQgehPgOgsQgVhFgKg5QgFghAEgSQAGgcAYgUQAXgTAdgBQAcgCAaARQAaAPANAaQAFAMAFASIAIAeQALAsAfBAQArBaAHARQAMAfAWBFQAUBCAOAiIAWAwQAMAdAGAUQAQA8gWArQgLAWgWAPQgWAOgZABIgIABQgVAAgUgKgEga5Ak6QgfgXgMgiQgHgVgBgeIADg1QADg6gGhtQgGhyACg1IACgxQABgdgCgUIgFgvQgDgcACgSQABgNAEgSIAIgfIANgvQAJgaANgQQANgPASgIQASgIATAAQApABAbAeQAWAaAFAtQADAaAAA2QACAgAIA/QACAbAAA1QgBA2ACAaIAJBOQAJBIABCQIgBAoQgCAWgFARQgMAigfAWQgfAXgkAAIgCAAQgjAAgfgVgEAV1AkfQgagKgagbQgigkgjhCQh7jlg2j9QgNg/AIgkQAFgZARgTQASgUAXgFQAWgEAWALQAVAJAPATQANAPAKAXQAHAOAKAdIA0CSQAbBMAOAjQAYA9AXAvIAyBgQAbA5AHAtQAFAjgIAbQgLAigcANQgOAIgQAAQgPAAgRgHgEgqiAhwQgZgEgVgOQgVgOgMgWQgQgZgBgiQgBgfALgeQALgdAmg2QCRjMBPidIAnhOQAYgtAUgeQAXgkAXgLQAUgKAYADQAXAEASAOQASAOAKAWQAJAVAAAXQABAYgNAsQglB5g/B2QgcAzhJBxQhDBpgfA9IgQAfQgKARgLALQgRASgYAIQgRAGgRAAIgPgBgEguAAZJQgggBgbgVQgagVgKgfQgLgeAHghQAHggAVgZQAQgTAsgeIBJgwQBAgrAagaIAngrQAYgaASgNQAbgVAtgNQAXgGAPABQATACAQAMQAPALAKASQAMAXgCAVQgBARgMATQgIAMgQASQg9BEgVAgQgNAWgHAKQgTAbgvAoIhLBAQglAggZAOQgjATggAAIgEAAgEgyLATSQgigJgVgZQgVgZgCgjQgCgjARgcQAQgaAugeQAkgZBTgyICbhfQAdgRARgIQAZgNAXgEQAbgEAZAHQAbAIAPAUQAQAWgCAeQgCAdgQAZQgOAVgYAUQgPAMgfAVIi4B8QgfATgTAQIgkAdQgVAQgSAHQgRAHgTAAQgOAAgOgEgEg1LAObQgYgLgOgTQgPgTgEgZQgEgZAIgXQALgfAigbQAQgNAwgdQBMgtA/g4IAogjQAXgUAUgMQA2ggAvAKQAbAGATAUQAUAUABAaQABAVgMAXQgJAQgTAWQg2A+hLA8Qg7AwhVA5QgbASgOAHQgYAMgWACIgJAAQgUAAgSgIgEg5yAMkQgmgJgTgeQgYgmANg1QAJglAhgvQAshBBEhEQAxgxBRhFQA4gwAkgWQAggSAXgFQAYgFAXAFQAXAFASAPQASAPAHAXQAIAXgIAWQgFAPgNAPQgIAKgRAPIhnBZQg8A0gnAoQg/BAgiA5IgdAwQgSAbgUANQgUANgYAAQgLAAgMgDgEg+SAIdQgWgGgQgQQgQgPgHgWQgGgWACgWQAEgcAXgnQAVgjAhgpIA+hHQAkgoAVgTQArgjAVgTIAegcQARgQAOgJQAogbAkAFQAiAGATAdQASAdgJAhQgGATgVAbQgdAkhhBhQhQBRglA6QgdAtgHAJQgWAcgbAIQgLADgLAAQgKAAgLgDgEhDgAF/QgcgTgCghQgBgRAJgdQAQgxAYgoQAOgYAZgjQBBhcAignQAigmAdgYQAegYAegKQAkgNAfAIQAeAIASAXQARAUABAWQAAAYgVAZIgTATIgVATQgaAYgfArIg0BIIgcAkQgQAVgKAPQgMATgWAoQgWAigXAPQgYAQgbAAIgFAAQgfAAgWgRgEhHjABTQgWgHgPgWQgNgWAAgZQABgsAogpQAXgXA5ghIB0hDQAtgaAcgGQAVgDAUAFQAVAGAMAPQAVAZgKAmQgHAcgbAdQgUAVgcAVQgSANgkAXIhrBCQgeATgTAHQgRAGgPAAQgLAAgKgDgEhIMgERQgdgSgIgdQgJgqAigwQAog3BFgcQBCgaBHAHQAwAFAeAVQASAMALARQAMATABAUQACAigeAcQgbAagmAIQgNACgrAEQgiACgUAIQgNAEggATQgcAQgTAEQgIACgIAAQgWAAgVgMgALFoCQgWgLgOgWQgLgSgGgaQgEgSgEgeQgMhogEhHQgGhfAFhQQADgwANgZQAKgSAPgLQARgLASAAQATABARAOQAPANAIAUQAGAQADAXIACAoQABBGAKBhIAQCoQAFAzgGAZQgFATgMAPQgOAQgRAFQgIACgJAAQgOAAgPgHgAPXoxQgYgIgPgTQgOgQgJgZQgFgQgHgfQgmiwgLi8QgCgqALgTQALgUAZgGQAYgHAVAKQAeANASAlQAMAaAIAuQATBxAZDCQAHA1gGAbQgIAlgbAOQgLAGgNAAQgKAAgLgDgEhC1gJEIgWgPQgOgJgJgEQgVgJggABIgjADQgWADgNAAQgqgBgXgVQgSgSgBgdQgCgcAPgXQAYgnA9gVQBJgZBLASQBPATAsA5QARAXAHAYQAIAbgHAXQgGAUgQAQQgQAPgVAFQgNAEgNAAQgdAAgcgQgACTo9QgZgIgSgiQgdg3AAhRQAAgPAKiCIAGg5QAFggAMgWQAPgaAbgNQAdgOAZAMQAsAVgBBUQAAAzgFAvIgFAtQgDAZAAATIAEA9QACAkgGAYQgHAfgaAUQgSAOgTAAQgJAAgIgDgAGhpvQgXgKgPgVQgMgSgIgbQgEgQgGggQgJg3ABgbQAAgXAKg+QAFgdAEgNQAGgYAKgQQANgTATgKQAVgLAUAFQATAFAOASQAMAQAEAWQADARgBAYIgDApQgBAeAEAqIAIBIQAGAtgHAaQgEASgLAOQgMAPgQAGQgJAEgKAAQgOAAgOgHgAiYqpQgcgQgJglQgFgXADgqQAGhSANg4QARhJAhg2QASgcAXgPQAcgSAZAJQATAGALAUQALASABAXQAAASgFAXIgMApQgQA2gFAvQgCA8gFAdQgJA1gdAbQgRAQgXAFQgIACgHAAQgPAAgNgHgATrrjQg2gmgthXIhMiTIgdgtQgQgbgHgVQgJgbAEgaQAFgdATgRQASgOAZgCQAYgBAWAKQAiARAjAuQAsA5AuBkQAiBKAWBBQAQArgBAbQgBATgKAPQgJARgQAHQgKAEgLAAQgYAAgegUgAnCrzQgegQgNgdQgUgrAPhPQANhCAmh8QAQg0ARgYQANgSAQgKQATgMAUAAQAdgBAXAZQAWAWAGAfQAFAbgFAhQgEAUgLAoIg4DAQgJAhgKASQgOAagWAKQgMAGgNAAQgQAAgRgJgEg+YgMtQgZgJghgZIg3gpQgPgKgfgRQgcgPgPgQQgeggABgpQAAgUAJgSQAKgTAQgLQAggWA4AJQBDAKA7AmQA6AnAkA6QAbAtgGAiQgDAVgOARQgOAQgUAJQgTAIgVAAQgUAAgWgIgArPt2QgogbAKhaIAQiVQAEgjAEgRQAFgcALgVQAMgZAUgRQAWgRAZgCQAdgDAZAVQAZAUADAeQABAPgFAUIgJAjQgJAhgGA9QgGBEgFAbQgMA8geAbQgTARgYAFIgOABQgSAAgPgJgEg4MgOOQgbgNgXgcQgSgUgthHQglg6gfgbIgwgsQgYgbADgbQAEgkAwgbQA5geAsAMQAXAHAVASQAOANAVAZQBaBvAbBaQANAugGAgQgEAUgMAQQgNARgSAHQgLAEgLAAQgSAAgTgKgAYTuXQgdgIgegXQiOhphOjbQgVg8AEgjQADgZAPgUQAQgWAXgFQATgFAUAIQATAHAQAPQANANAMATQAHALAOAZQBDB+BZB2QAWAeAIAMQAPAYAFAVQAHAagHAZQgHAbgUANQgQAKgUAAQgMAAgMgDgEgyNgOgQgfgOgbgjQgcglgnhfQglhagigoIgqgwQgWgdACgbQACggAggVQAegTAkACQAmACAkAUQAiASAbAfQAoAsAoBaQAoBaAMBAQAHAngHAgQgIAmgbAQQgQAJgVAAQgTAAgSgIgA1QwkQgJgfAAg8IgCjNQgBg1AKgbQAHgUAPgOQAQgPATgDQAXgDAWAPQAUAOALAXQAJAUADAbQABARAAAgIgCCwQAAAlgCARQgDAegIAXQgKAbgTATQgVAUgaAEQgmgTgPgzgAvRvqQgQgKgJgRQgOgaAAgoIAGhFIACg3QABghAEgWQALhBAqgcQAbgRAXAIQAOAEAKANQAKAMADAQQAEANAAASIgCAeQABBRgDAoQgEBFgYAsQgWAngeADIgGABQgOAAgOgJgA52v1QgcgOgLgnQgIgcAAgtIADknQAAgmAFgXQAIghAVgRQAQgNAXgDQAWgCAUAJQAmASARAsQAJAaADAyQAFBYAAAtQACBKgHA6QgHBIgcAkQgTAYgdAJQgNAEgMAAQgQAAgOgHgEgtIgQAQgpgKgigrQgkgsgNg1QgDgJgIguQgGgigJgUQgFgOgNgVIgTghQgVgtAPgeQAMgZAggJQAcgIAfAHQBDAQApA2QAYAeAOAuQAJAbALA5IAPBRQAHAgAAAUQgBAdgNATQgOASgYAHQgMAEgNAAQgKAAgLgDgEgi1gRTQgggNgSgdQgPgZgGgjQgEgXgCgqIgHiEQgCgvAEgUQAKgnAbgQQAagOAfALQAeALASAbQAPAWAHAhQAFAWADAnIANCEIACAmQABAVgEARQgJAsggARQgOAHgQAAQgPAAgQgGgA9wx0QgRgHgMgOQgQgTgGgfQgDgVAAgkIABi1QAAgbACgNQACgXAGgRQAIgUAPgOQAQgOAUgCQAYgCAVARQAUARAJAZQAHAVABAdIABAyIAEBsQACA/gLAqQgHAbgOASQgQAWgWAFIgMACQgLAAgMgFgEgoggR6QgUgFgRgPQgYgVgYgvQgVgpgMgkQgRgsgKg0QgLhAAYgeQAPgSAagFQAagEAXAKQAqARAaA1QAJASAKAcIAQAvQAPAlAHATQALAgACAZQACAggPAbQgPAegbAIQgKADgJAAQgLAAgLgEgAac0tQgxgHg0goQgTgOhDhBIgjgiQgTgVgLgSQgNgXgCgYQgDgaALgVQAPgbAjgKQAhgJAgAKQAcAJAdAXQARANAgAfIBeBdQAaAaAMATQARAbgEAaQgCARgLAOQgLAOgQAIQgVALgaAAQgKAAgKgCgAeS5IQgagIgcgXQgtglghgpIgVgZQgMgOgLgJQgJgHgRgKIgcgQQgUgOgMgSQgOgUgBgWQgBggAagaQAYgZAigEQA5gJBDAsQAzAiAnAxQAnAxAUA6QAOAngDAcQgCASgIAPQgJAPgPAIQgNAIgQAAQgNAAgOgFgEAjSgb5QgjgIgtgmQhnhZhAh+QgrhSAhgsQAUgZAkgBQAigBAeASQAXAQAYAdQANASAZAkQARAYAoAxQAmAuASAaQA2BOgfAvQgNASgYAHQgNAEgNAAQgKAAgLgCgEBHQgcvQADgMgCgEQgCgFgGgDIgLgGQgXgNgLgsQgKglgGgwQgDgcgFg6IgalPQgEg2ABgcQABguAOgiQAJgWAOgPQARgRAUgBQAQgCAQAJQAOAIALAPQAOATAKArQARBHAJBUIARC8QAJBlgFAyQgDAjgLA7QgJAwgKAUQgTAmghAJQADASgOAMQgEgEACgLgEAzggc9QgRgGgOgNQgUgSgMgfQgJgWgJglQgYhtgMhkIgKhGIgKgzIgKg0QgFgkgDgMQgEgNgLgZQgJglARggQAMgWATgJQASgIAWADQATAEATALQAeASASAfQANAUAIAcQAFAQAHAjIAOBGIAaCbQAMBOAEAoQAEAugBAuQgBAogIAVQgNAhgdAJQgIADgIAAQgJAAgLgEgEBCRgdNQgUgNgNgYQgVgmgIhEQgIhCACiwQACiagUhXQgMgzgBgTQgBgqAXgXQAMgMASgFQARgEASADQAhAFAbAdQAUAUAPAfQAKAVAOAlQAQAwAEAcQAFAcAAAwQACC9gFBeQgDA4gGAjQgKAxgWAiQgUAggYAFIgMABQgQAAgQgLgEAnVge9QgegZgZgyQgbg7gPgcIgegzQgSgegHgXQgKgdAFgeQAFggAVgSQAVgSAfAAQAdAAAZAPQAnAXAlA9QAqBHAcBOQAUA8gFAlQgEAagQAUQgQAVgYAGQgHACgIAAQgeAAgfgbgEAtjgesQgWgMgOgYQgOgWgLggIgQg5Qgfh4gthyQgMghgEgPQgGgcAGgVQAIgcAdgPQAcgQAcAJQAaAJAUAbQAPATAPAjQAaA/AXA+QAZBGAKAzQAOBDgHA5QgHA+glANQgIACgIAAQgPAAgQgJgEA40gewQgYgFgRgRQgdgbgPg4QgLglgShtQgPhcgVgzIgSgoQgLgYgEgRQgGgXACgVQADgXAMgRQAPgTAagHQAZgHAZAHQAsANAhAxQASAaAOAkQAKAZALApQAXBSAKA+QANBPgDBEQgDBPgsAVQgNAGgPAAQgIAAgJgCgEA8uge+QgTgKgLgWQgKgUgCgYQgCgSACgbIACguQABgngKgzIgUhZIgLg2QgLg4gJg7QgEgjAEgSQAHgjAegSQAfgSAhALQASAGAUASQAZAWAQAhQALAYAKAoIAiCFQAMAyAHAsQAKA5gDAlQgDAdgMAmQgUA6ggAaQgVARgZAEIgOABQgTAAgPgJg");

    this.timeline.addTween(cjs.Tween.get(mask).to({
      graphics: mask_graphics_0,
      x: 168.3458,
      y: 216.6731
    }).wait(3).to({graphics: mask_graphics_3, x: 149.044, y: 216.6731}).wait(2).to({
      graphics: mask_graphics_5,
      x: 127.179,
      y: 216.6731
    }).wait(2).to({graphics: mask_graphics_7, x: 107.9247, y: 216.6731}).wait(3).to({
      graphics: mask_graphics_10,
      x: 88.8179,
      y: 216.6731
    }).wait(3).to({graphics: mask_graphics_13, x: 67.2054, y: 216.6731}).wait(3).to({
      graphics: mask_graphics_16,
      x: 45.7579,
      y: 216.6731
    }).wait(3).to({graphics: mask_graphics_19, x: 27.5179, y: 216.6731}).wait(3).to({
      graphics: mask_graphics_22,
      x: 5.3565,
      y: 216.6731
    }).wait(3).to({graphics: mask_graphics_25, x: -15.8579, y: 216.6731}).wait(3).to({
      graphics: mask_graphics_28,
      x: -46.4408,
      y: 214.2046
    }).wait(4).to({graphics: mask_graphics_32, x: -57.8221, y: 203.8683}).wait(2).to({
      graphics: mask_graphics_34,
      x: -70.0177,
      y: 187.862
    }).wait(3).to({graphics: mask_graphics_37, x: -78.6211, y: 172.931}).wait(3).to({
      graphics: mask_graphics_40,
      x: -94.0825,
      y: 159.65
    }).wait(3).to({graphics: mask_graphics_43, x: -108.1169, y: 149.2535}).wait(3).to({
      graphics: mask_graphics_46,
      x: -123.0619,
      y: 142.5439
    }).wait(2).to({graphics: mask_graphics_48, x: -136.983, y: 135.7319}).wait(3).to({
      graphics: mask_graphics_51,
      x: -138.4847,
      y: 121.7673
    }).wait(3).to({graphics: mask_graphics_54, x: -138.4847, y: 107.2188}).wait(3).to({
      graphics: mask_graphics_57,
      x: -138.4847,
      y: 92.3046
    }).wait(3).to({graphics: mask_graphics_60, x: -138.4847, y: 81.9683}).wait(3).to({
      graphics: mask_graphics_63,
      x: -138.4847,
      y: 76.7695
    }).wait(2).to({graphics: mask_graphics_65, x: -138.4847, y: 74.2117}).wait(2).to({
      graphics: mask_graphics_67,
      x: -138.4847,
      y: 71.1802
    }).wait(3).to({graphics: mask_graphics_70, x: -138.4847, y: 70.6457}).wait(2).to({
      graphics: mask_graphics_72,
      x: -138.4847,
      y: 68.4647
    }).wait(3).to({graphics: mask_graphics_75, x: -138.4847, y: 68.4647}).wait(3).to({
      graphics: mask_graphics_78,
      x: -138.4847,
      y: 68.4647
    }).wait(3).to({graphics: mask_graphics_81, x: -138.4847, y: 68.4647}).wait(3).to({
      graphics: mask_graphics_84,
      x: -138.4847,
      y: 68.4647
    }).wait(3).to({graphics: mask_graphics_87, x: -138.4847, y: 68.4647}).wait(3).to({
      graphics: mask_graphics_90,
      x: -138.4847,
      y: 68.4647
    }).wait(3).to({graphics: mask_graphics_93, x: -138.4847, y: 68.4647}).wait(3).to({
      graphics: mask_graphics_96,
      x: -138.4847,
      y: 68.4647
    }).wait(3).to({graphics: mask_graphics_99, x: -138.4847, y: 68.4647}).wait(3).to({
      graphics: mask_graphics_102,
      x: -138.4847,
      y: 68.4647
    }).wait(3).to({graphics: mask_graphics_105, x: -138.4847, y: 68.4647}).wait(3).to({
      graphics: mask_graphics_108,
      x: -138.4847,
      y: 68.4647
    }).wait(3).to({graphics: mask_graphics_111, x: -138.4847, y: 64.4673}).wait(3).to({
      graphics: mask_graphics_114,
      x: -138.4847,
      y: 49.4062
    }).wait(3).to({graphics: mask_graphics_117, x: -123.6589, y: 37.5224}).wait(3).to({
      graphics: mask_graphics_120,
      x: -108.6182,
      y: 28.7492
    }).wait(2).to({graphics: mask_graphics_122, x: -90.908, y: 22.6847}).wait(2).to({
      graphics: mask_graphics_124,
      x: -72.1676,
      y: 19.749
    }).wait(2).to({graphics: mask_graphics_126, x: -54.8904, y: 19.6185}).wait(2).to({
      graphics: mask_graphics_128,
      x: -38.9429,
      y: 16.7414
    }).wait(2).to({graphics: mask_graphics_130, x: -22.6468, y: 14.965}).wait(2).to({
      graphics: mask_graphics_132,
      x: -8.3276,
      y: 14.9643
    }).wait(3));

    // Layer_3
    this.instance = new lib.Symbol33();

    var maskedShapeInstanceList = [this.instance];

    for (var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(135));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-459.1, -222.4, 916.8, 444.9);


  (lib.Symbol30 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Tween7("synched", 0);
    this.instance.setTransform(-629.3, 0);
    this.instance._off = true;

    this.instance_1 = new lib.Tween8("synched", 0);
    this.instance_1.setTransform(1359.15, 0, 1, 1, 0, 0, 180);
    this.instance_1._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(8).to({_off: false}, 0).to({x: 1204.25}, 620).to({_off: true}, 1).wait(644));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(629).to({_off: false}, 0).to({
      x: -600.5,
      y: 208.8
    }, 643).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-734.5, -79.7, 2198.9, 579.3000000000001);


  (lib.Symbol23 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("ABsEjQAGACAEADIADADgAqBEcQAEgEAFgDQAEgCAHgBIALgBQAHgCAIgEIgxAbQAAgFADgFgAkPD7QgVgNgPgZQgKgRgOghQgLgagPgEQgNgDgVAPQgwAjiWBTQALgGANgKICGhrQAbgWARgHQAdgLAXALQAPAIAMAXQAIAOANAfQAOAbARAKQAVAMAjgGQAcgFAqgTQA1gZAPgFQAdgLASAHQAQAGANAXIAUAnIAIAMIg2ggQgIgOgFgCQgHgEgOAEQgVAHgiAQQgnATgQAGQgiANgcAAQgeAAgXgOgAhUBrQgCgIAMgMQAqgpAVgzQAGgQAJgiIAchdQAIgfAHgQQAEgMAHgGQAKgJAJAGQAHAEACAQIAIBmQACAbAAASQgCAmgVA3IgJAZQgDgbgFglQAHgSABgIQAHgdgFg5IgEg0QgEAFgEASQgDAPgFAMIgHATIgNAnIgNAmQgPAshNBUQgEgDgBgFgAlvgCQgmgkgMgvQgQAKgWAVIglAhQgeAWglAJQglAKgkgHQgsgJgrgiQgegXgogwQgagegOgOQgLgMgKgBQgJAAgQALQhWBFhZBBIglAdQgEgCgDgDQgDgFABgFQABgFAJgGIDNifQATgOALAAQALAAAQAMQAUAPA5A+QAtAyAmARQAlARAqgEQArgFAggYQANgJAXgWQAXgWAOgKQAPgLANADQALADAGANIAJAZQAPAsAwAaQAkATA3AIQATADAEAJQACAEgCAGQgBAGgFAEQgDADgHACQh3gogagXgAE9ANQAAgEAFgJQAaglAPgTQAYgfAagRQARgMARgDQAUgEAOAKQAOAJAHAYIAMAnQAFALAIAGQAJAHAKgCQANgCAMgXQAhg8Asg5QASgYARAEQAIABAHAKIALARQAPAaApAfQA3ApAoAPQAyATA0gGQAmgDA2gTQAQgGAIABQAHABAFADIACACIACACIADAGQACANgTANQgPAMgYALIgIAAIhDgCQhTAJhSgtQhFgog5hKQgnAzgdA0QgOAYgJAKQgPARgSACQgSADgRgLQgQgJgKgRQgHgNgHgVIgLgkQgZgBgXASQgNAKhPBaQgJgEABgJgADfAGQgFgGAAgFIAGAMIgBgBgAEoivQAbgoAyguQAbgYAQgIQAXgKALAMQAGAIgEASQgSBWhUBkIgYAeQgJAOgQAdQgIAMg1gFQApgDABgeIAJgRQAIgOAggnQAmgsAVgjQAYgqANgvQgYANgjAjQgkAjgQAYQgJAOgJARIg0ByIAAgBIAmhXQgeA3gHAcIgBAEIgIASIgCAEQA1ioAHgKg");
    this.shape.setTransform(9.811, 70.88);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#30A1DA").s().p("AiiEzQgmgChCgoQhCgoglgCQgbgBgmANIg/AVQgQAFgfAFIhIgJIAxgbIAGgDQCWhUAwgjQAUgPAOADQAPAEAKAaQAOAhALARQAPAZAVANQAtAbBGgaQAQgGAngTQAigQAVgHQANgEAHAEQAFACAIAOIA3AgQATAZAeAJIAOAEIACABIANAIQAEAEACAFQACAFgBAEIgCAEQgHAMgdgJIg7gPQgjgJgSgQIgKgJQgGgFgFABQgGAAgLAKQg7BAg/AAIgHgBgAAmB1IgMgNQgLgKgQgEIgQgEIgQgCQgNgBgGgEQBMhUARgtIANgmIAMgnIAIgTQAFgMADgPQADgSAFgFIAEA0QAFA5gHAdQgCAIgGATQAFAkACAcQgTA0gEAPIgCAIQgEAMgHACIgFABQgFAAgHgGgAo+BGQgqgCgngSQhHghgxhLQgOgXgNgGQgTgJgmATQg0AZgjANQggAMgdAFQgRADgPABQgLACgJgDIgBgBIAlgeQBahBBWhFQAPgLAKAAQAKABALAMQAOAOAZAeQApAwAeAXQArAjAsAJQAjAHAmgKQAlgJAdgXIAlghQAXgVAPgKQANAvAlAkQAbAYB2AoIgKADIgDABQg3AMgugHQg4gJgjgkQgbAYgRANQgaAUgYAKQggAOglAAIgLAAgAIMAvQgRgEgVgVQgYgVgVgaQgIgKgGgCQgJgDgMAHIgQAMQgJAJgHADQgIAEgJgDIgCgBIgDgBQBPhbANgKQAYgSAZABIALAkQAGAVAIANQAKARAQAKQARALARgDQASgCAQgSQAJgKAOgYQAdg0AngzQA4BKBGAoQBSAuBSgJIBEACIgLALIgXALQguAWghAJQgtAOgmgEQgqgEgsgaQgigUgoglQgKgIgGAAQgHAAgIAJIgrAtQgZAZgWANQgZAQgVAAQgHAAgHgCgAEbgoIgEgGIgFgHIgBgBIA1hzQAJgRAJgOQAPgYAlgjQAjgjAXgNQgNAvgYAqQgUAjgmAsQggAngJAOIgJARIgCAFQgIANgHAIQgEADgDAAIgCAAgAESg8QAIgcAdg3IgmBYIABgFg");
    this.shape_1.setTransform(7.05, 74.5588);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#30A1DA").s().p("AnaFRQgegCgVgNQgLgHgYgWQgUgUgPgHQgXgLgfACQgPACgRAEIgpANIggAJQAHgQAeghQAwgzA6g3QAZAtAQAWQAYAjAbAVQAZATAZAEQAYAEAbgLQAVgIAagRQA2gmArguQAZAtAqAfQAgAXB5AnQAvAPA8ARIh6gbIgTAFQg2AMgngCQg0gEglgbIgOgKQgIgFgHAAQgJAAgQAJQg6AogmAKQgWAHgVAAIgLgBgAj7BPQg6gCgsgkIgQgMQgJgHgIgBQgKgCgUAFIg7AOQggAGgbAAQhGAAgqglIgjglQgUgWgUgGQgUgGgkAHIhGAQQgqAJgcAEQgJABgGgBQgGgCgDgEIgBgGIA4gfQB7gzBfhdIAUA4QATAvAPATQAVAcAjAOQAhAOAlAAQA9AABLgnQABAiAcAaQAaAVAlAIQAqAJAlgKQAqgMAWgfQAGASAPAOQALALA0AbIAqAVIgrgTIgCAFQgFAJgGAEQgRALghgTQgQgIgGABQgJAAgPAJQgxAagzAAIgIAAgAJjA2Qg5gGg2ggQgLgHgJgEQgIgEgGAAQgQgBgUAMIgiAWIgKAEQASgbAhgsQAWgqAcgqQAEgGAEABQACAAADAGQAGASAeAWIAiAbQAfAVANAGQAbALAfgDQAdgDAcgNQAYgMAagUQAQgMAegaQAMAoAKATQANAWBTAkQgUALgLADQgWAHgPADQg/AMgqgUIgNgFQgHgDgGABQgFAAgNAHQgsAWgyAAIgVgBgAE0jTQAHgKAEgFQAGgHAMgJQAJgGARgJIAPgFQAPgDALALIACAEIgCADIgIAHQgIAHgWALIhIAigAgckPQgJgWgBgNQgCgKADgLQABgHAEgCQAFgCAEAEQADACACAHQAOAmgNAsgAKckTQAFgFAEgDIAHgDIAEAAIADACQADADgBAEQAAAFgDADQgEAFgLADQgNAEgLAAQAHgIAKgKg");
    this.shape_2.setTransform(14.45, 75.1254);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#FFFFFF").s().p("AjwEPQgqgfgZgtQgrAug2AmQgbARgUAIQgcALgYgEQgYgEgZgTQgbgVgZgjQgPgWgZgtQg6A3gwAzQgeAhgHAQIgLACQgWAEgEgMQgDgIAHgIIAOgLQAJgGATgSIBnhrQALgMAIgEQAOgHAKAFQAIAEAIAPQATAjAWAeQAqA5AqACQAVAAAcgOQAVgMAlgfQAsgkAQgXQAJgMAEgEQAKgHAJAEQAGADADAJIADAQQAFAWAVAVQAeAfApARQApARArAAQAWAAAHADQAIADAEAGQAFAGgBAHQgCAKgOAIIgRAIQh6gngfgXgAFTBKQgGgBgFgFQgFgFAAgGQAAgIAJgHIAQgLQAIgGAIgLIALgUQATgkAagrQAMgSALgIQARgMAQAIQAGADAFAGIAIANQAOASAhAaQApAgAbAFQAnAHAygfQAtgdAngrQAOgOAFgCQAPgHAJAJQAGAGgCANIgDAWQgCAOALAZQALAaAMALQALAHAUAGQAfALASgBIAQgCQAJAAAGAEIAAAAQAGAEABAIQABAJgGAFQgFAGgSACIgaAEIgNgGQhTglgNgVQgLgTgMgoQgdAagQAMQgaAUgYALQgcANgdADQggADgagLQgOgGgegUIgigbQgegWgHgSQgCgGgDAAQgDgBgEAGQgdAqgVApQghAsgSAcIgJABIgFAAgAiBAkQgPgPgHgSQgVAggrAMQgkAKgqgJQgmgIgZgWQgdgZgBgiQhKAng9AAQglAAghgOQgjgOgVgcQgPgTgTgvIgVg4QheBdh7AzIg4AfIAAgIQABgKgCgEQAEAGAHgDQAEgBAGgGQARgPAcgPIAxgZQBFglA3g5QAOgOAHgEQAOgIAMAGQALAFAHAWQASA4APAZQAZArAnAOQASAGAdAAQBMgBAygkQAWgQAMAGQAHAEADAMIACAVQADAWAUAPQARALAZAHQAnAJAegIQARgGAOgLQAOgLAGgQIAHgTQAFgJAIgBQAJgCAIALIAHATQAFAXAIAKQAMASATABIARAAQAJABAFAFQAGAHgFAMIgLARIgEAIQg0gbgLgLgAhChiQgFgFgBgHQgBgGACgIIACgNQAEgWgHgaQgDgJgOgkQgFgJgCgJIgDgTQgEgiAMgQQAJgNASgBQAQgBAMALQAGAFAGANQAHAOACANQACAIAAAPIgBAeQgCASgKAhQgLAigCARIgDARQgFAKgIABIgBAAQgFAAgFgFgAhBktQgEACgBAHQgDALACAKQABANAJAWIALAcQANgsgPgmQgCgHgCgCQgDgDgDAAIgDABgADYhoQgGgHAFgJIAJgNQAEgHAEgMQAGgPAPgZIAPgVQAIgKAZgPQAggUAVABQAMAAAOAHQAYANABAUQACARgUASIgOAKIiGBDQgIAEgEAAIgCAAQgGAAgDgDgAFYjjIgOAFQgSAJgIAGQgMAJgGAHQgFAFgGAKIgOAXIBHgiQAXgLAIgHIAIgHIACgDIgCgEQgIgJgMAAIgHABgAJEi9QgGgFAAgGQAAgHAFgFIAGgFQADgCAEgIQAQgZATgPQAUgOAPADQALACAHAKIAEAIIAEANQAAAFgDAKIgGAMQgFAHgDADQgMAKgeAFIgaAFIgLABQgIAAgEgCgAKLj7IgHADQgEADgFAFQgKAKgHAIQALAAANgEQALgDADgFQADgDABgFQAAgEgCgDIgDgCIgBgBIgDABg");
    this.shape_3.setTransform(17.7577, 71.646);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#FFFFFF").s().p("ArTFSIBEhLQAngsAYgkQAIgNAHgBQALgDAOAPIAfAiIAiAnQAQASAVAUQAXAWAUADQAUACAagQQAcgTAigkQAkgpATgUQARgQAMADQAHABAGAIIAKAPQAOAVAoAWQBKAmA5gDIAWgBQANgBAJACQALACAJAIQAJAIACAKIAkAPQgJAKgLAJQgTAOgOgGQgMgFgDgQQgCgKgEgDQgEgDgKACQg0AIhJgcQhPgegjgsQgOANgVAYIgjAlQgjAihSAdQgmgTgNgKQgKgJgWgWIgfgmQgWgegZgWIgDgCIgDACQggArhNBjIgLgIQgQARgUAOQgHgLARgUgAB/DWQAIgJAKADQAGABAEAGQAEAFAAAHQABAEgEAVIAAABQgggjADgEgAD7BnIghgGIgQgCIA8AIIgGAAIgFAAgADqApIgGgOQAAAAgBAAQAAAAgBABQAAAAAAABQgBAAAAABQgGANgtAsIgGgGQgFgIgBgDQAAgFAEgFQADgEAGgCIAGgCQADgCACgEQAOgcAQgaQAFgIAEgDQAIgFAFACQAGACADAJIADANQAEAcAXAPQAUAOAiABQANABADADQAGAEABAHQACAHgEAFQgFAGgMACQhggtgFgJgAGFBVQgEgGAHgJQALgRAQgQIAgghIAngzQAYgfAXgMQAQgJAJAHQAFAEABAJIABAQQABAbAZARQAUAOAeAAQAoABAngZQAdgSAigkQALgMAGgDQANgHAJAIQAGAGgBANQhXBPgdAMQg7AZgygQQgbgIgTgVQgTgWgCgbQgNAIgJALIgUAbQgRAXhCA/IgEADIgHAGQgDACgEABQgFAAgCgDgAnmAuQgfgGgZgQQgbgRgOgZQgJgRgHghQgIglgHgPQgcAcgpAcQgaARgzAdIhDAnIgCgEQgEgNAWgOIApgZQAzghAagSQApgeAcgdQAIgIAFgDQAJgEAHAFQADADACAJIAPA8QAGAaAFAOQAJAVAOANQAQANAhAIQAbAHAPgEQARgFATgRQAQgPAigoQAJgMAIgFQAMgIAKAGQAGAEADAIIAFAPQAPAsA/ASQAZAHAQgFQATgGAPgWQAcgpABg4QAAgeAOgFQAKgEAKAJQAGAFAFANQAFAPABAVIABAlQACAwAVAUIAEAEQg7gfgBgQQgQAdgNAOQgTAWgWAHQgYAIgigJQgigJgcgVQgcgVgOgeQgYAggaAXQgaAYgVAGQgKADgMAAQgLAAgOgDgADzi9QgHAAgDgGQgEgGAEgGIADgGIABgGQACgKANgPIAJgJQAPgNAXgFQAagFAVAHQARAGAGALQADAFACAHQADAQgEAKQgEAIgKAHQgGAEgFACQgKAFgQgCIgagEQgMgCgLABIgQACIgHADIgFABIgCAAgAE7jdIAQAEQAHAAAFgDQAFgDABgDQAEgIgIgHQgGgFgKgBQgRgCgRAGQgSAGgLANIAPgBQASAAAQAEgAhzkKQgXgDgPgWQgFgGABgGIAAgGIgFgGQgFgEgBgLQgBgEABgEIACgGQAFgLAJgGQAKgHAKAAQAFgBAFACQAEACADAEIALAOIAEAGQAFAHABAVIAAAMQABAJgBAIQgBALgGAEQgEADgGAAIgEAAgAiPlSQgDADAAADQgCAFADAGQAAABAAABQABAAAAABQAAAAABAAQAAABABAAIAEABQAEABAFAFIAHAJIABgMQAAgKgDgFIgFgHQgEgHgBAAIgCgBQgDAAgEAFg");
    this.shape_4.setTransform(24.5806, 72.17);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#30A1DA").s().p("AgNF8QgggDgbgPQgYgPgMgGQgVgLgSAEQgJACgKAGIgSAKQgSAIgUAAQgUgBgRgKIgNgKQgJgFgHgCQgJgDgLADIgVAFQg+AShDgKQhBgKg5giQgRgKgHgBQgPgFgKAJQgDADgDAGIgGAJQgKAKgSgCQgSgBgQgJQAegdAIAAQAHgNAMgIQANgJAOgBQASgCATAKQAHADAaAPQAkATBHAaIAKgEQBSgeAjgiIAjglQAWgYAOgNQAjAtBPAeQBJAbA1gIQAIgCAFAEQAEADACAJQADARAMAFQANAGAUgPQALgJAIgKIALgOQgBAJgCAIIAhANQgHAWgRASQgWAZgdALQgWAIgYAAIgPgBgAmzFDIA4ATIgtgVIgLACgAn6BhQglgEgxgrIglghQgVgRgUgIQgagKgsgBIgBAAQgpgBgcAIQgSAFgEgBQgMAAgEgIIBCgoQA0gdAagRQApgbAcgcQAHAPAHAkQAIAiAJARQANAZAcASQAZAPAfAGQAbAFAUgFQAVgHAagXQAagXAYghQAOAeAcAXQAcAUAhAIQAjAJAYgHQAWgHATgWQANgPAQgdQABAQA6AgIAJAJQAGAHgBAGIAAACQgCAJgLADQgJADgLgDIgOAAQgEABgHAIQgvA3gxAAQgaAAgkgSQgWgLgagQQgRgLgKABQgIABgMAMQgfAagkARQgiAPgbAAIgJAAgAC0A5IgGgBIgLgEIgIgCIgCgBQAtgrAGgMQAAgBABgBQAAAAAAgBQABAAAAgBQABAAAAAAIAGAOQAFAJBgAtIgGABQgQACglADIgOABgAKQA2QhIgCgognQgLgKgFgBQgHgBgMAHIgTAOIgUAMQgYAOgVgGIgPgFIgEgBQgGgBgFACIgCABQBCg9ARgYIATgcQAKgKANgJQABAbAUAXQATAVAbAJQAxAPA7gYQAdgNBXhQIApglIgqAuIgBADQgNAqgHATQgOAigSAVQgNATgQAJQgXAOgmAAIgJAAgAEmkCQgXgGgaACQALgNASgGQARgGARADQAKABAFAEQAJAHgEAIQgCADgEADQgFADgHAAIgQgDgAiXlbQgEgFgFgCIgDgBQgBAAAAAAQgBAAAAgBQgBAAAAgBQAAgBAAAAQgEgHACgFQABgDADgDQAEgFAFABQABABAEAGIAFAIQADAEgBALIgBALg");
    this.shape_5.setTransform(26.7, 75.9262);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).to({state: [{t: this.shape_3}, {t: this.shape_2}]}, 6).to({state: [{t: this.shape_5}, {t: this.shape_4}]}, 6).to({state: [{t: this.shape_1}, {t: this.shape}]}, 5).to({state: [{t: this.shape_3}, {t: this.shape_2}]}, 6).to({state: [{t: this.shape_5}, {t: this.shape_4}]}, 6).to({state: [{t: this.shape_1}, {t: this.shape}]}, 5).to({state: [{t: this.shape_1}, {t: this.shape}]}, 6).to({state: [{t: this.shape_3}, {t: this.shape_2}]}, 6).to({state: [{t: this.shape_5}, {t: this.shape_4}]}, 6).wait(5));

    // Layer_2
    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#0D325B").s().p("AAbAxQAKgoAHgmQAPAHAMAJQAYARgEASQgEASgeAHQgLACgNAAIgGAAgAhFAQQgYgTADgUQAEgTAdgFQALgBAMAAQgHAVgJAfIgFASIgBADIgNgJg");
    this.shape_6.setTransform(12.4812, -64.1199);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#0D325B").s().p("AgIAsQgmgKgXgSQgYgTADgUQAEgTAdgFQAegEAlAKQAlAJAYATQAYARgEASQgEASgeAHQgLACgNAAQgUAAgVgFg");
    this.shape_7.setTransform(12.4812, -64.1175);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#0D325B").s().p("AgEAtIAXg9IAGgVQAbAJATAPQAYARgEASQgEASgeAHQgLACgNAAQgSAAgTgEgAhFAQQgYgTADgUQAEgTAdgFQAZgDAfAGIgFAOIgXBDQgXgJgRgMg");
    this.shape_8.setTransform(12.4812, -64.1156);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_6}]}).to({state: [{t: this.shape_7}]}, 2).to({state: [{t: this.shape_8}]}, 1).to({state: []}, 1).to({state: [{t: this.shape_6}]}, 26).to({state: [{t: this.shape_7}]}, 2).to({state: [{t: this.shape_8}]}, 1).to({state: []}, 1).to({state: []}, 1).wait(22));

    // Layer_1
    this.instance = new lib.Symbol4("synched", 0);
    this.instance.setTransform(-0.1, -10.05, 1.1395, 1.1395);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(12).to({startPosition: 0}, 0).wait(45));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-149.6, -123, 299.1, 240.4);


  (lib.Symbol17 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib.Symbol18("synched", 0);
    this.instance.setTransform(3.25, -13.55, 1.8516, 2.3159, 0, 0, 0, 33.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["#0066FF", "#00CCFF"], [0, 1], 13.1, -719, -13, 719.1).s().p("EhwXBF+MAAAiHxQB+g9BGhoQBFhnFQCVQFRCVCagQQBJgIBngWICugmQDVgrCKAQQBvAMDVBRQCLA0BhAXIFeAAQBSgQCAgkQCqgxBAgOQCEgdBqABQB/ACDzBBQD1BBB8ADQCeAEDlhFQA/gTB5hQQB4hQCFg5QCEg6DCBLQDCBMC4A1QBEAUCHAtQB3AlBXAGQBoAHB5geQBVgUCFgzQCvhDAsgOQB8gpBkgJQBqgLCCAUQBUAMCVAlQCoApBCAMQCEAZBpgDQBmgCB/gdQA7gNCmguQCOgnBUgPQB8gWBoAHQBxAICpA3QDsBMAqALQDDAvDogLQDAgJDrgzIDLgtQB4gbBVgMQDpghCrAuQBLAVCcBHQCUBEBVATQCAAeCagVQBvgPCrgyQD/hKAdgIQCjgpB9gBQB7gCD+A0QD3AyCCgFQBVgDCDgZQC2gkA+gXQA9gWA2g8QA2g8CwBLQCvBMAcABMAAACKEg");
    this.shape.setTransform(0.025, -2.2313);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-719.2, -503.8, 1445.1, 949.4000000000001);


  (lib.Symbol14 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Symbol8_1();
    this.instance.setTransform(0, 0.1, 0.8365, 0.8365, 0, 0, 0, 0.1, 0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-62.2, -143.5, 124.7, 286.9);


  (lib.Symbol12_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance_1 = new lib.CachedBmp_312();
    this.instance_1.setTransform(-76.25, -52.95, 0.5, 0.5);

    this.instance_2 = new lib.Symbol13("synched", 0);
    this.instance_2.setTransform(-29.75, -69.7, 0.6536, 0.6536, 0, -23.2047, 156.7953, -0.1, -0.1);

    this.instance_3 = new lib.Symbol13("synched", 0);
    this.instance_3.setTransform(34.1, -83.35, 1.2042, 1.2042, 0, 16.7699, -163.2301);

    this.instance_4 = new lib.CachedBmp_311();
    this.instance_4.setTransform(-51.7, -32.65, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}]}).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AhfHaQgQgKgDgRIgTAIIgTAIQgYAHgPgKQgFgDgKgMQgJgLgIgEQgFgCgPgCQgNgBgFgFQAOgIAQACQARACAMALIANAOQAJAIAHACQALACAYgMQAVgKAKAGQAFADAGAKQAGAJAFADQAJAFANgGIAXgLQAJgDAXABIAvAEQgFAOgTAAQgHABgRgDQgPgBgJACQgFABgKAGQgKAFgFACQgGACgFAAQgLAAgKgHgABPHXQgDgFADgFQAEgBAMgBQAKgBAFgEQADgCAFgJQAFgIAFgBQAEgBAGADIALACQAHAAAJgMQAUgaAGgPQABAAAAAAQABAAAAAAQAAgBABAAQAAgBABAAIAKAFQgMAbgPAVQgIANgIACQgEABgNgBQgKgBgEAEIgHAIQgJAKgVAAIgPgBgAlxHFIgNgBQgLgBgFgDQgEgCgCgFIgBgGIgDgDQgFgEACgDQABgGAFAAQADAAAEAEIAGAJQAFAFADABIATgBIAGgBIAKgEQAGgDADAEQABACgFAEQgDABgDAEIgEAGQgDADgIAAIgEAAgABMGrQANgLAKgCIARgHIAKAAQAFAAAEgDIABAFQgEAIgSAJIgHACIgIACIgJAFQgFACgOAAQgBgEAGgGgAmbGcQABgDgCgEIgEgHQgFgLAHgJQACgFAEgBIAFAAQABAAAAAAQABABAAAAQAAAAABABQAAAAAAABQADAFgIAGIAGAKQADAAACgDIAHgFQAFgCAGAFQAHAFAEABQAGABAKgJQANgKALAAQALABABAJIgCAHQgCAFABADIgOAFIAEgKQgLgBgIAGIgGAFIgHADQgEACgIgBQgKgBgCgFQgHABgEAGgAERFzQAAgDADgEQAHgJAQgIQASgIAPgFQABACgCADIgFAEIgRAOIgMAGQgGADgEADQgBABAAAAQAAABgBAAQAAABAAAAQAAABAAAAgAE+FvQgBgHAIgFQAFgCALgEQALgEAVgUQATgTAOgCIAPgCQAIAAAFgCQAIgDAPgPQAcghAngWQAogWArgIQgDAIgSAGQhKAZgyA6QgOAQgIADQgEACgJABIgOABQgLADgNAKIgVASQgXAUgTAAIgIgBgAp+FiIAAgEQgQgIgEgJQgBgFAAgKIABgsIAHAAQAEAOgBAQQAAATADAFQAEAGAKAEQAPAGAPACQAAAHgLABIgKABIgQgBgAhOFVQgWgDgcgJIgZgJQAqgiAzgXQA8gbBAgGIA5gFQAhgEAXgIQAKgEAagNQAWgMAOgEQAMgEARgCIAcgDQAhgFAggLIAGADIAHAGIAEAHIgKACQgUAHgGAAIgSAAQgLgBgHACQgGACgSANQgOAKgKgDIgJgDQgFgBgDABQgEAPgNALQgMAKgQABIgLABQgDABgKAJQgLAIgYAGQgYAHgRACIgiAEQgbADgqAMQgiAMgOAKIgRAPQgKAJgJABQBXgGAtgHIgGACQgmAKgXADQgUADgSAAIgYgBgAkWEbIAEgHIAPAHQATgKAQAFQADgNAOgFQAOgFALAIQgEALgJAHQgJAIgLAAIgDAJQgegIgegHgAn0D9IACAAQAOACATgGQAVgGAWgBIgJADQgeAKgSAEQgMgCgJgEgAoKDTIAGgmQAKgxgQgxQgQgxgjgiIgggeQgHgIgFgIQAlAdAhAeQA1AyAQAqQAKAdgFAdQgFAfgTATIgQAPQgGAHAAAHQgEgKABgNgAqQDAIgIgJQgDgFAAgHQAAgOAKgMQAEgHAAgDQAAgDgGgGQgGgEgDgFQgEgHADgFQAHADAJAHQAIAHACAFQAGAPgLAPQgFAGgBADQAAAFAGAGQAKALACAKQADANgJAJQgDgQgLgMgAGFCvIAAgBIAFAKIABADIgXAMIARgYgArkBdQgNgEgCgKQAAgEABgIQABgJgBgEQgLACgKgIQgLgHgBgLQgBgHACgJIAEgOQABgIACgEQAEgHAGACQgGAHAAAKQAAAJgBACIgEAGQgEAFACAHQACAGAFAFQAJAHAQgBQABAFAAAJQAAAKACAEQABAFAHAEQAJAHAFgBIALgEQAGgCADADQACAGgHADQgCABgIAAQgQgBgEgCgAsTBJQgcgTgJglQgIgjARgeQAEgGAAgDIgDgGIgFgFQgGgKAIgTQAJgUALgTQAMgUAKgIQAQgNAQAEQgYALgQAUQgNAQgPAlQgFAKADAGQABACAEAEQAFAEABACQACAFgDAGIgHAKQgQAWAHAcQAEAdAWASQAIAIACAEQACAEgBAEQgBAEgDACQgEgFADgFgAMzARIgBgBQAAAAAAgBQAAAAAAgBQABAAAAAAQAAgBAAAAIADgBIAIgBQAKgCAAgSIAAgZQAAgJgDgDQgCgEgKgDQgJgDgCgFQgCgDABgFQABgHgBgCIgEgGIgGgGQgBgDAAgGIgBgIQgDgIgSgFQgTgFgDgJQADgGAQAGIAVAGQAMAFACAHQACAFACAKIAEAGIAFAGQABADABAHQAAAIACADQADADAHADIAKAFQAGAFAAAJIgBAQIABAQQAAAKgBAFQgCAJgIAHIgFAAQgKAAgKgDgAtUgcQgCgEAAgLIAAgfIABgKIAFgIQAFgFABgEIADgIIAEgGQACgFgBgCIAGAAQAFAIgGAOIgHAMIgHALQgDAHAAATQAAAPAGAGQAGAFgCADQgBADgDAAQgIAAgEgJgAr6hEQgCgdAGgWIAFgTIAHgOQAIgPAWgfQAIgJAFAAQgEAPgMANIgMAOQgGAHgJAVQgJAXgBANIAAAUQABANgCAIQgFAAAAgIgAnClJQgDgJACgGIADgGIAEgHIABgHIADgEQAGgFAEAAQADgBAGADQARAHAGAHQgCAFgHAAIgNAAQACAGgHAIQgGAGgGACQgEACgDAAIgGgBgAnNnUIgBgCIgBgCQgBgDABgCQAAAAABAAQAAAAABAAQAAgBABAAQABAAAAAAIAAgCIAJACIABABIABABIACADIgBAFIgFADg");
    this.shape.setTransform(-4.45, 12.7726);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#217DAC").s().p("AiEFcQgbgDgjgKIg9gUQhcgehKgIIgvgEQgqAKgegBQgoAAghgQQgkgSgOghQgJgVAAgdQAAgRAFgiIAHgsQADgSgCgJQgFgTgWgNIgTgKIgUgJQgfgUgQgmQgPgkAFgoQAHgxAigxIAEgJQAWguAigbQAoggApAGQAKACASAGQASAGAJACQAPACANgDIAFgCIgLAMIgKAMIgQARQgMANgZAQIgnAaQgVARgKATQgNAXAFAWQACAKAHALQAFAIAHAIIAgAeQAkAjAPAxQAQAwgKAxIgGAmQAAANAEAKQACAGAEAEQAEAFAJAEQAIAEAMACQASgEAfgKIAJgDQAdgJASgDQAagEAkACQAvABAUgEQAbgGAtgaQAOgHADAIIg1AXQgfANgMAMQgJAIgNAZIgDAIQAeAHAdAIIAqAOIAXAIIAZAJQAcAJAWADQAdADAigFQAXgDAlgKIAGgCQCngtCahQIAXgNIgBgCIgEgKIAAAAIgBgDIgFgHIgHgGIgFgEIgDgBQgSgMgBgWQgPACgOgHQgNgHgJgMQgIgMAAgPQAAgPAIgNQAGgJAAgEIgBgPQABgQAcgNQAagLAVgHQAXgJATgEQAygNAqADQAoAEAUATQAMALAEARQAEARgJANIgLAOQgHAJgCAGQgBADABADQABACAGAAQAaABARAUIAHgCIgGgCQABgDACgCQADgCADABQADgcAlgSIAdgNQARgIAKgIIAGgHQgEgIgNgKIglgfIgbgLQgegKgOgHQgegOgIgCIgWgDQgNgDgIgLQgJgLABgMQACgLAJgKQAOgOASgEQAQgEAVAFQAOADAWAJIBbAkQAeAMAOAIQAXANAOAQQAKANAFARQAEAOABAYQACASgDANQgBAAAAABQAAAAAAAAQgBABAAAAQAAABABAAQgFAMgLAPIgCADQgFAOgHANQgYAng6AbIhpAoQgnAPg3AgIhcAzQgsAXg5AXQgiAOhGAaQh4Atg/ANQgyALgpAAQgQAAgPgCg");
    this.shape_1.setTransform(-0.6784, 21.0808);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#30A1DA").s().p("AhyGjQhZgMhLgmQgNgGgKgHIgHADIgXANQgwAYgogUQgVgKgLgVQgMgWAHgWQgXgJgigCIg7gBQhLgDgbgqQgPgXAEgeQADgdARgZQgSgJgLgTQgLgSAAgUQgxgDgrgvQgQgSgPgZIgagwQgQgigGgUQgIgfAHgZQAJgbApgiQAugmA+goQAngZBLgtQAbgQAPgGQAYgLAUAAQAZgBAUAOQAVAPABAXQACAUgQAUQgIAJgNALIAKgLIAMgNIgFACQgNAEgQgDQgIgBgTgHQgRgGgKgBQgqgHgnAgQgjAcgVAuIgEAJQgjAxgGAxQgFAnAPAlQAPAmAgATIATAJIATAKQAXAOAEASQACAJgDATIgGArQgFAiAAASQgBAdAJAVQAPAgAkASQAgARAoAAQAfAAAqgJIAvADQBJAIBcAfIA+ATQAjALAbADQA0AGBEgPQBAgOB5gtQBFgZAjgOQA5gXArgXIBcg0QA4gfAmgQIBpgoQA6gaAYgoQAIgNAEgOIACgCQAMgPAEgMIAAABQANAEANgBQAHgIACgJQACgGgBgJIAAgRIABgQQgBgJgFgEIgKgFQgHgDgDgEQgCgDAAgHQgBgIgBgDIgFgGIgEgGQgCgKgCgEQgDgIgMgFIgUgFQgQgGgDAGQADAIASAFQATAFADAIIABAJQAAAFABADIAFAGIAFAGQABADgBAGQgBAGACADQACAFAJADQAKADACADQADAEAAAJIAAAZQAAATgKABIgIABIgDABQAEgMgCgTQgBgXgEgOQgGgRgKgNQgNgRgXgNQgOgIgegMIhcgjQgWgJgOgEQgUgEgRAEQgSAEgNAOQgJAJgCAMQgCAMAJALQAJAKAMAEIAXADQAIACAdAOQAOAGAeAKIAbAMIAlAeQANAKAEAJIgGAGQgJAJgRAIIgeANQglARgCAcQgDAAgDACQgDACAAADIAFACIgHACQgQgUgagBQgGAAgCgDQgBgCABgDQACgHAIgIIALgOQAIgOgEgRQgEgQgMgMQgUgTgngDQgrgEgyANQgSAFgXAIQAFgHAIgHIAZgRQAPgLAIgKQAFgHAGgNIAKgWQAZgtBMgRQBHgPBDAOQBJAOAwAtQAYAXANAbQAPAegCAdQgDAlggAsQg0BGhNApQgSAKgoASQgmARgUALQgVAMggAWIgzAkQgsAdg2AYIgaASQibBqihAbQgxAIgwAAQglAAglgEgAhWGIQAQAKAQgGQAFgBAKgGQAJgFAGgCQAJgCAPACQARACAGAAQAUAAAFgPIgwgEQgWgBgJADIgXALQgNAGgJgEQgFgDgGgKQgGgJgFgDQgKgHgWALQgYAMgKgDQgIgCgIgIIgNgOQgMgKgRgCQgRgCgOAIQAGAEANACQAPABAFADQAIADAJALQAKAMAFAEQAPAKAYgIIATgHIATgIQACARARAKgACJFgQgFABgFAHQgGAKgCACQgFAEgKAAQgMABgEACQgDAFADAEQAhAFALgOIAHgIQAFgDAKAAQANABAEgBQAIgCAIgMQAPgWAMgbIgKgFQgBABAAAAQgBABAAAAQgBAAAAAAQgBAAAAAAQgGAQgUAaQgJAMgIAAIgKgDIgIgCIgCABgAmNFRQgEAAgBAFQgCAEAEAEIADACIACAHQABAEAFADQAFACALABIAMABQAMABAEgDIAEgGQADgEACgCQAFgDAAgCQgDgFgGADIgKAFIgGABIgTAAQgEgBgEgFIgGgIQgDgEgEAAIgBAAgABsFLQgKADgNALQgHAFACAEQAOAAAFgCIAJgEIAIgCIAGgDQASgIAFgIIgCgFQgDADgFAAIgKAAgAmPEhQgEACgDAEQgGAKAFALIAEAHQACAEgBADIASABQAEgFAHgBQABAFALABQAHABAFgCIAGgEIAGgEQAJgHAKACIgDAKIANgFQAAgDACgFIACgIQgCgIgKgBQgLgBgNAKQgKAJgGgBQgEAAgHgFQgGgFgGACIgGAEQgDAEgDgBIgFgJQAIgHgDgFQAAAAAAgBQgBAAAAgBQAAAAgBAAQAAgBgBAAIgDAAIgCAAgAE0EIQgQAJgHAJQgDADAAAEIAMACQAAgBAAAAQAAgBAAAAQABgBAAAAQAAgBABAAQADgEAHgDIAMgFIARgOIAFgEQACgDgBgDQgQAFgRAIgAIVCQQgnAWgdAhQgOAQgIADQgFABgJABIgOABQgOADgTASQgVAUgLAEQgMAEgEADQgIAFABAHQAWAFAcgZIAVgSQANgKALgCIAOgCQAIgBAFgCQAHgCAPgQQAyg7BKgYQARgGAEgJQgsAIgnAWgAqLDsQAAAKACAEQAEAKAQAIIAAADQAPACAKgBQAMgBAAgHQgPgCgPgHQgLgEgDgFQgEgFABgTQABgQgEgPIgIAAgAqUAiQAEAGAFAEQAHAFAAAEQAAACgFAHQgJAMAAAOQAAAIADAFIAIAJQALAMACAQQAKgJgDgNQgCgKgKgLQgHgHABgEQAAgDAFgGQAMgQgGgOQgDgGgHgGQgJgIgHgDQgDAGADAGgArpgPQgCAJABAEQACAIAMAFQAFACAQABQAHAAADgCQAHgCgCgHQgDgCgHACIgKAEQgFAAgKgHQgGgDgCgFQgCgEABgJQAAgKgCgEQgPABgJgIQgFgEgCgHQgCgHAEgFIAEgFQABgDAAgJQAAgKAGgIQgGgBgEAHQgCADgBAIIgEAQQgCAJABAGQABALAKAIQAKAHAMgCQABAFgBAIgAr/j2QgJAHgMAUQgLATgJAUQgJAUAHAKIAFAFIACAFQABAEgEAGQgRAdAIAlQAIAkAdAUQgDAEADAEQAEAAABgFQABgEgCgDQgCgFgJgHQgVgTgFgcQgGgeAQgVIAHgLQADgGgDgEQgBgDgEgDQgEgEgBgDQgDgGAFgKQAPgkANgRQAPgTAZgMIgHgBQgNAAgNALgAs6jGIgFAHIgCAIQgCADgFAGIgEAHIgBAKIAAAgQAAAKACAFQAEAIAHAAQAEAAABgCQABgDgGgFQgFgGAAgPQAAgTADgIIAHgKIAGgNQAHgOgFgIIgGAAQABADgCAEgArDkZQgXAfgHAPIgHAPIgGATQgFAVACAdQAAAIAFAAQACgHgBgNIAAgVQABgMAJgXQAJgVAFgHIANgPQALgMAFgPQgFAAgIAIg");
    this.shape_2.setTransform(-5.3387, 21.034);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-93.4, -188.5, 214.9, 251.9);


  (lib.Symbol11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Symbol30();
    this.instance.setTransform(-30.9, -7.7);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(0, 0, 0, 0);


  (lib.Symbol10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol13("synched", 0);
    this.instance.setTransform(65.2, 97.55, 0.5083, 0.5083, 0, 0, 180, -0.1, 0.2);

    this.instance_1 = new lib.Symbol13("synched", 0);
    this.instance_1.setTransform(174.25, -0.45, 0.5865, 0.5865, 0, 0, 0, 0.6, -0.8);

    this.instance_2 = new lib.Symbol13("synched", 0);
    this.instance_2.setTransform(205.15, 17.55, 0.3877, 0.3877, 0, 30.9483, -149.0517, -2.9, 4.5);

    this.instance_3 = new lib.Symbol13("synched", 0);
    this.instance_3.setTransform(-180.85, 2.9, 0.7552, 0.7552, 0, 0, 180);

    this.instance_4 = new lib.Symbol16("synched", 0);
    this.instance_4.setTransform(99.35, -64, 0.7124, 0.7124, -63.7209, 0, 0, -0.1, -0.3);

    this.instance_5 = new lib.Symbol16("synched", 0);
    this.instance_5.setTransform(110.85, -61.5, 0.7124, 0.7124, 108.811, 0, 0, -0.1, -0.4);

    this.instance_6 = new lib.Symbol15("synched", 0);
    this.instance_6.setTransform(26.65, -110.75, 0.8669, 0.8669, -58.5032);

    this.instance_7 = new lib.Symbol16("synched", 0);
    this.instance_7.setTransform(174.45, -109.35, 0.7124, 0.7124, 43.6957, 0, 0, 0, -0.3);

    this.instance_8 = new lib.Symbol15("synched", 0);
    this.instance_8.setTransform(29, -102.75, 0.8669, 0.8669, 139.2356);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_8}, {t: this.instance_7}, {t: this.instance_6}, {t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    // Layer_1
    this.instance_9 = new lib.CachedBmp_309();
    this.instance_9.setTransform(-248.95, -156.45, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(1));

    // Layer_3
    this.instance_10 = new lib.CachedBmp_310();
    this.instance_10.setTransform(-259.6, -92.45, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(1));

    // Layer_4
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("ALIUqQhPgIiXgfIoLhrQiugkhYgYIhqgfQg+gSgtgHQhHgLhUADQhEADhXAOIiyAaQg7AHgzgCIAFAHQAOARAJAGQALAHAPAEIAcAFIBQAJQgbAdg+gIQgggEgTgJQgTgKgOgRIgMgRQgIgJgIgEQgIgEgSAAQgZgBgPAEIgUAFQgNAEgHABQgaAEgNgPQgIgNgFgFQgJgMgTgBQgMgBgXADQhxAPhCguQgYgRgaghIgcgjQgdAFgfAAQgeABgTgIQgigPgUg3QgGgTgFgJQgLgRgbgLIgxgPQgdgKg4goQg1gmghgKIgXgFQgOgDgIgDQgbgLgGgVQgDgMAHgFQADARAJARQAgABAQACIABAAQgXgPghgXIhdhFQAKASAAAUQgPgFgSgQIgegcQgVgSgygYQhXgthIgvQgMgHAAgIQgBgGAGgEQAGgDAHABQAGABAPALQAgAbA4AbIBOAkQhwhXhrhcQgkgfgRgVIgOgRIgBADIgFAPQgEAJABAGQABAOARAHQAFACAJACIAOAEQAQAFAMANQAMANAEAQQgtgMgrgTQgPgHgFgGQgLgNAFgaQAGgjgBgIQgBgLgIgNIgOgWQgYgoAEgxQACgTAGgYQAFgUAKgEQgQAmALAwQAHAgASAiQgJg/AshSQAohKA0guQAbgYAogYQAWgNAzgbIFWivQB7hAA3giQAugdBIg1IB0hTQBMg0Bkg3QA+gjB4g/QCNhJBGgcQA7gXBMgXQAsgNBdgYII3iVQgLAOgTALQgNAHgXAJQhRAfiuAxQiqAwhUAhQg3AWhqAyQjrBvhxA8Qi/BliMBlIhWBAQg0AmgkAXQg8AnhzA6IkYCOQhtA4g2AoQhVA+giBMQgUAtAJAhQAFATAQATQAKALAWATQC9CiDDCSQBGA1AuATQApARA8AKIBpANQCBAQA7AyQAUARAdAkQAgApAPAOQAeAeAvAZQAeAQA6AXQA8AZAfAKQAzASArAFQBMALBzgVQCyggANgCQCHgOC1AlQBoAVDQA3QBgAXC/AmQBqAWDTAwQC4AlCIAGQCuAHCVgpQCogvBzhlQBMhEBChqQAvhLA7h/QAZg0ATgZQAQgSAggZQAmgdAMgMQAagZAOgeQAQghgDggQgBgQgJgeQgJgegCgPQgCgZAHgZQAIgZAQgTQARgWAfgQQATgLAngPICshDQA3gWAcgNQAugVAhgXQA9grBFhYQAXgeAMgVQARgeAFgcQAJg5gmhBQgTggg4hEIgTAXQgOAQgFAOIgGAZQgEAPgGAIQgIAKgOAGIgbAKQgZAKgbAWQgLAJgjAhQg0AwhsBWIjgCxQh9Bjg8AtQhpBNhaAzQEci/ENjgQB5hkDRi8IARhkQAIgwAIgYIAAgCQAAgmAkggQAZgXAxgYICJhAIABAAQAQgZABgiQABgjgQgiQgOgcgagdQgSgUghgdQixiXjPhnQgpgWgagCIgYgBQgOgCgIgGQgPgKABgVQACgUAOgMQAagVAtAIQAoAHBBAiQDNBuCvCVQAnAhAWAZQAfAhAQAiQAZAzAABBQgBBbgvAuQgbAZhOAdIADACQAHAGgCAIQgBAGgMAIQgPALgIAOQgKAQADAQQADARAVATQAcAaAFAHQASAagFAvQgBARgEARQAWBDgOA/QgKAtghAxQgVAfgtAyQgkAmgTATQgfAfgdAUQgdAUgmATIhIAfIhgAoIABABQgMAJgHALIgQAiIAHgEQAHgGAIgLIAMgTQARgYAUAAQAGAEgBAIQAAAHgFAIQgRAfgYAYQgPAPgNADIgBABQAEAEAAAJQAAAHgEAPQgCAKACAPIACAaQACAggSAOQgMAKgcgBQgPAAgLABQgGAMACAKIAFAQQAFAWgXAaIgNANIggAZQgTAPgKANQgGAJgHAOIgKAYQgOAegSAMQgIAEgcAJQgXAHgKAKIgMAUQgQAYgcAIIgDgDIAAgBIhLCOQgyBYgyA8QhHBWhjBAQheA+hvAkQiVAwilAAQhDAAhGgIgA0HQtQAEAGAEACQAJAHAOgBQAGAAAKgDQgZgHgagJIAEAFgA4/O5IAUAaQAhAqAhAPQAeAOAtgBIBOgGIAPgBIg0gWQg5gZgdgQQgkgVgZgXQgbALgcAHgA+cLzQAdASAfALQArANAUAJQAkAQAOAaQADAFAOAmQAJAaAPAJQAPAJAhgDIAdgEIgXgaQgTgVgJgSQgLgVgKgqQgCgMAAgHQghgJgpgFIhbgJQgkgEgcgHIAMAIgA6iMjQgCAcAXAeIAVAXIAWAXIALAPQAZgGAZgIIACAAIgcgkQgZgggTgQQgWgTgggNgAc1JIIgrAmQgXAUgNAQQAJgGALgFIAcgKQAPgHAHgJQAHgIAEgPIAIgYIABgBIgLALgAeWHDQgOASgLARQgFAKgGAEQgKAUgMASIASgOIAlgdQAMgJADgHQAEgIgCgSIAAgQQgHAFgHAJgAedD9QgQAHgHAGQgNAIgEALQgFAOAHATIAPAhQAMAdgFAmIABgBQAQgNARgGIADgDQAJgIAJgCQAHgCAEAFQACACgBAEIABAAIAMACIAMAAQAQgBADgNIgBgPQgDgNAIgdIACgHIgBAAQgCgDgBgMIgEgSQgQgBgHABQgXAFgXAeIgSAYQgLAOgLAHQAIgoAJgRQAPggAZgLQAKgFAhgEIAGgBIAAgDQACgIAFgIQAEgIAHgJQAJgKAJgIgEAnZgGQIgDgIQgDgKgLgKIARAcgEAlygIYIApAvIgCgFQgEgSADgQQADgXAWgcIABgBQgtAUgTAYgAvBRqQgHgCgCgEQgCgEADgFQACgEAFgDQBHAOBHAAQAbgBAIgCQAbgFAOgCQARgCApACQAlACAUgFQANgCAWgJIAjgNQASgEAXgCIAqAAQAeABAPAEQAQAFASANIAdAZQAHAGgBAFQgIgFgKgJIgSgPQgWgTgUADQgJABgRAIQgLAEgSABIgeACQgTACgmAPQglAOgVACQgUADgqgEQgQABghAEQggAEghAAQhIAAhJgUg");
    this.shape.setTransform(1.6219, 40.0735);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-265.7, -156.4, 534.7, 329.5);


  (lib.Symbol9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Symbol13("synched", 0);
    this.instance.setTransform(-79.2, -105.15, 0.2921, 0.3382, 0, 16.9689, -163.0322, 0.4, -1);

    this.instance_1 = new lib.Symbol13("synched", 0);
    this.instance_1.setTransform(-97.35, -110.15, 0.2921, 0.3382, -17.2069, 0, 0, 0.4, -1);

    this.instance_2 = new lib.Symbol13("synched", 0);
    this.instance_2.setTransform(152, -8, 0.4253, 0.4923, 0, 16.9796, -163.0202, 0.1, -0.1);

    this.instance_3 = new lib.Symbol13("synched", 0);
    this.instance_3.setTransform(125.7, -15.25, 0.4253, 0.4923, -17.2172, 0, 0, 0.1, -0.1);

    this.instance_4 = new lib.Symbol13("synched", 0);
    this.instance_4.setTransform(-58.9, 108.55, 0.7121, 0.8243, 0, 0, 180);

    this.instance_5 = new lib.Symbol15("synched", 0);
    this.instance_5.setTransform(-1.85, 85.7, 0.6702, 0.6275, 0, 52.3329, 44.9244, 0.4, -1.1);

    this.instance_6 = new lib.Symbol15("synched", 0);
    this.instance_6.setTransform(-8.85, 85, 0.6422, 0.6561, 0, -109.6837, -117.8966, 0.5, 0.8);

    this.instance_7 = new lib.Symbol15("synched", 0);
    this.instance_7.setTransform(82.85, -83.8, 0.6702, 0.6275, 0, 52.3315, 44.9253, 0.5, -1.8);

    this.instance_8 = new lib.Symbol15("synched", 0);
    this.instance_8.setTransform(75.9, -84.35, 0.6423, 0.6561, 0, -109.6853, -117.8971, 0.6, 1.5);

    this.instance_9 = new lib.Symbol15("synched", 0);
    this.instance_9.setTransform(70.75, -142.75, 0.6702, 0.6276, 0, 52.3297, 44.9262, 0.1, -1.7);

    this.instance_10 = new lib.Symbol15("synched", 0);
    this.instance_10.setTransform(64, -143, 0.6423, 0.6562, 0, -109.686, -117.8982, 0.3, 1.3);

    this.instance_11 = new lib.Symbol15("synched", 0);
    this.instance_11.setTransform(35.75, -114.85, 0.6702, 0.6276, 0, 52.3302, 44.9253, 0.2, -1.7);

    this.instance_12 = new lib.Symbol15("synched", 0);
    this.instance_12.setTransform(28.95, -115.2, 0.6423, 0.6561, 0, -109.6856, -117.8983, 0.4, 1.4);

    this.instance_13 = new lib.Symbol15("synched", 0);
    this.instance_13.setTransform(-109.55, 58.35, 0.6704, 0.6278, 0, -49.7349, -57.1382, 0.1, -0.6);

    this.instance_14 = new lib.Symbol15("synched", 0);
    this.instance_14.setTransform(-108.45, 64.9, 0.6425, 0.6563, 0, 148.244, 140.0359, 0, 0.5);

    this.instance_15 = new lib.Symbol15("synched", 0);
    this.instance_15.setTransform(99.7, -123.95, 0.6703, 0.6276, 0, 52.3303, 44.9262, 0.3, -1.4);

    this.instance_16 = new lib.Symbol15("synched", 0);
    this.instance_16.setTransform(92.95, -124.2, 0.6424, 0.6562, 0, -109.6876, -117.8988, 0.1, 1.2);

    this.instance_17 = new lib.Symbol15("synched", 0);
    this.instance_17.setTransform(42, -169.6, 0.6703, 0.6277, 0, 52.3309, 44.9262, 0.3, -1.5);

    this.instance_18 = new lib.Symbol15("synched", 0);
    this.instance_18.setTransform(34.95, -169.95, 0.6424, 0.6562, 0, -109.6892, -117.8999, 0.4, 0.9);

    this.instance_19 = new lib.Symbol15("synched", 0);
    this.instance_19.setTransform(99.4, 49.75, 0.6703, 0.6277, 0, 52.3304, 44.9253, 0.8, -0.6);

    this.instance_20 = new lib.Symbol15("synched", 0);
    this.instance_20.setTransform(92.75, 49.4, 0.6424, 0.6563, 0, -109.69, -117.8998, -0.6, 0.7);

    this.instance_21 = new lib.Symbol15("synched", 0);
    this.instance_21.setTransform(91.15, -170.3, 0.5255, 0.5255, -58.5027, 0, 0, -0.1, -0.1);

    this.instance_22 = new lib.Symbol15("synched", 0);
    this.instance_22.setTransform(92.65, -165.45, 0.5255, 0.5255, 139.2354);

    this.instance_23 = new lib.CachedBmp_306();
    this.instance_23.setTransform(95.05, 107.95, 0.5, 0.5);

    this.instance_24 = new lib.Symbol13("synched", 0);
    this.instance_24.setTransform(-157.15, 16.25, 0.7121, 0.8243);

    this.instance_25 = new lib.CachedBmp_305();
    this.instance_25.setTransform(-218.35, -183.9, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_25}, {t: this.instance_24}, {t: this.instance_23}, {t: this.instance_22}, {t: this.instance_21}, {t: this.instance_20}, {t: this.instance_19}, {t: this.instance_18}, {t: this.instance_17}, {t: this.instance_16}, {t: this.instance_15}, {t: this.instance_14}, {t: this.instance_13}, {t: this.instance_12}, {t: this.instance_11}, {t: this.instance_10}, {t: this.instance_9}, {t: this.instance_8}, {t: this.instance_7}, {t: this.instance_6}, {t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    // Layer_2
    this.instance_26 = new lib.CachedBmp_307();
    this.instance_26.setTransform(-229.1, -169.9, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_26).wait(1));

    // Layer_3
    this.instance_27 = new lib.CachedBmp_383();
    this.instance_27.setTransform(-258.4, -187.95, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-258.4, -207.6, 519.5, 446.7);


  (lib.Symbol7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance = new lib.Symbol13("synched", 0);
    this.instance.setTransform(185.15, -65.35, 0.8639, 0.8639, 0, 16.4478, -163.5522, 0.1, -0.1);

    this.instance_1 = new lib.Symbol13("synched", 0);
    this.instance_1.setTransform(60.4, -173.8, 0.7355, 0.7355);

    this.instance_2 = new lib.Symbol14("synched", 0);
    this.instance_2.setTransform(-15.9, -9.8, 0.5215, 0.5215, 0, 0, 180, 0.1, -0.2);

    this.instance_3 = new lib.Symbol13("synched", 0);
    this.instance_3.setTransform(-49.15, -91.2, 0.5911, 0.5911, -19.2067, 0, 0, -0.7, 0.1);

    this.instance_4 = new lib.Symbol13("synched", 0);
    this.instance_4.setTransform(-70.05, -111.9, 0.5911, 0.5911, -19.2067, 0, 0, -0.7, 0.1);

    this.instance_5 = new lib.Symbol13("synched", 0);
    this.instance_5.setTransform(-0.45, -100.1, 0.5911, 0.5911, 0, 0, 180, 0.4, -0.1);

    this.instance_6 = new lib.Symbol13("synched", 0);
    this.instance_6.setTransform(-28.05, -115.5, 0.5911, 0.5911, -19.2067, 0, 0, -0.8, 0.5);

    this.instance_7 = new lib.Symbol14("synched", 0);
    this.instance_7.setTransform(152.9, -111.4, 0.6986, 0.6986);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_7}, {t: this.instance_6}, {t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(91,54,30,0.439)").s().p("AqfJVQgKgFgHgIQgMgOgDgpQgDgmgPgOQgMgMgXgBQgPgBgZAFIgHACIAEgOIA0gIQAFgdAZgqQAegzAHgTQgLgMAEgSQADgTAPgHQAPgHAQAJQARAJACAQQABAJACACQADAFAIgBQALgBAKgQQANgUAFgEQAEgDAKgFIAPgHQAEgCAIgIQAHgHAFgCQALgFAPAEIAZAJIAYAFQAOADAHAHQAHAIACAYQAFBXgYA8QgSAwghAKQgTAGgTgJQgTgJgIgRQgMAKgMAaQgOAdgIAJIgLALQgGAGgCAFQgDAFgCAKIgDAOQgEAKgJAIQgJAHgLABIgEAAQgJAAgJgDgAHdJAQgGgOgFgYQgMg3gMhMQgEgZgBgSQAAgYAIgnIAhi2QADgRgEgIQgEgIgOgHQhTgrgTgLQg6gegogdQgVgQgGgKQgDgFgBgGIgLALQgeAbgRAGIgHADQgCgQgDgGQgFgJgMgIQgGgEgPgIQgKgFgQgMIgSh5QgFgjgGgZQgNg2ghhHQgTgqgQgTIgRgSIgRgTQgTgZAIgXIAEgKQACgHgCgEQgBgEgKgKQgJgJAFgIQACgDAFAAQAEgBAEACIAMAKQAHAGAaALQAuAUAjA1QAWAhAbBDQARAqAFASQAHAXAHAjQANBHAHBBQAQgEAVgZQASgUACgOQABgHgCgLIgDgSQgBgUAPgTQAMgQAVgMIAmgTQAXgLANgLIATgTQAMgMAJgEQANgHAOADQAPADAFAMQAEAKgHATQgZA7g3AcIgSAKQgKAGgFAHQgFAKAAARIABAdQgCAUgPAUQgKAOgVAUIAAAAQALACARAJICgBTQAbAMAMABQgCgLgHgQIgNgaQgQghAKgXQAIgTAXgEIAMgCIAHgDQgFgdAIgdQAJgcATgWQgKgMgdgIQgegKgKgKQgPgPAGgZQAFgYAVgIQASgGAgAKQAiAKAlAPQAdAMAHAPQACAEADANQADALAEAFQAEAFAIAEIANAIQAIAHADAKQAEAKgCAJQgCAJgHAIQgHAIgKADQgJACgZgDQgWgCgLAHQARABAMANQANAMACARQAIgIAMgCQAMgCALADQALAEAIAJQAIAJACALQAIgLAIgTIAMggQAIgRAMgLQAOgNAPgBQAOgBANAIQAMAIAHANQALAVgBAhQAPgIARAEQASAEAKANQALAQgBAcQgDAhABAQQAAAMAHAfQAGAcgCAQQAIgNAQgEQAPgEAOAGQAcALAJAiQAGAYgGAkQgEAWAEAKIAHAMIAIAMQAKARgGAkQgHArgOAoQgLAegMAPQgUAXgYgCQgYgBgSgZQgLgQgJggQgKASgRAKQgTALgSgGQgPgFgRgYQgTgagNgGQgIAMgFAVIgHAiQgMAugfAHQgdAHgmgnQALgDAHgTQAghaAOgvQAWhNAHhAQgDAUgWAJQgOAFgMgCIAAANQADAngGAVQgFAPgLAKQgLALgPACQgPACgMgMQgNgLAEgPIgSAAQACAvgKBxQgJBlAEA5QgJAAgJADQgSAGgJARQgJATAIAQQgRgCgSACgAvxHuIAFAEIghADIAcgHgAhWCuQgfgJgggRQglgTgQgYQgPgXgDgjQgDgiAKgfQAEgNAHgQIgFADQgSAKgWAQIgnAcQgVAQgOAGQgPAHgaAGQg6AOgeABIgoAAQgXAAgPAGIgLAEQgHACgFAAQgFgBgHgDIgLgGQgPgHgQABQgOABAAgHIAjgFQgFgLADgNQADgMAKgJQAQgPAdgCQAKgBAnABQAnAAA5gLQAbgGANgHIARgLIAQgNQAKgHAcgPIBNgnQAPgHAIAAQAIAAAHAGIAJgRQAqhXgPhfQgVgDgRgIQgVAfgSANQgcAUgcgKQgOgFgIgNQgIgPAEgNIg2gRQgYgIgMgIQgTgMgDgRIgRALQADgDABgEQACgGgBgGQgCgIgFgQQgDgTAOgRQANgQAVgGQAagHATAHQALAEAIAJQAHAJACALQALgCARgLQATgMAJgCQARgGARAHQASAGAJAPQAAgeASgSQAJgJAOgEQANgDAMAEQASAIAQAiQAQAjAHAQQAMAeAFAYQAHAjgBBCQAAAqgEAZQgEAZgKAcQgTAAgUADQggAGgUALIgMAHIgTANIgGAFQgIAHAEAEIACgCQAQgOANgJIALgGQAWgLAwgFIARgBIgbA5QgVApgJANQgSAXgDAIQgFAKAAAUQAAAOAEAFQAGAGANACQA5AMBFAFQAXABAKgFQAIgEAJgLIAQgRIAIgGIgBAIQgEAMgRARQgRATgNAFQgIACgQACQgWACgLAAQgLgBgegHQgogKgRgLIgNgKIgNgJQgRgJgNAHQgIAEgDAJQgDAIACAJQACAOAPAQQAnAoBWAWQAUAFALgBQAJAAASgDIAggBQAcgDAkgdQAjgdANgeQgDAggcAbQgaAZgmAKQgeAJggAAQgpAAgugOgABghOIACABQAFAGAAADQAAADgDADIAAAAIgEgQg");
    this.shape.setTransform(67.4625, -132.5209);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(145,124,63,0.549)").s().p("ApRRIQgMgFgQgLIgZgTQgdgQgNgKIgBgBQgUgBgSgQQgPgOgOgeIgOgdQgUgdgCgsIgBgQQgBgXAFgUIADgVQAEgbAFgPQAJgWAQgKQAPgJAUADQASADAQALQAaAUAIAiQAHAjgQAdQATAMAXgFQAYgFAMgSQgKgnAJgcQAFgQAMgNQAMgMAQgEQgCgGgBgNQAAgNgDgGQgBgFgOgUQgMgRgOgrQg0ipgVhiIBYElQAHAZAKAJQAFAGAMAJQAKAIAAAJQAMgIARgBQAPgBAPAGQAVAJAdAaQAlAiAXAdQAdAkANAmQAOArgKArQgKAtgiAYQgRANgjALQgoANgOAIQgPAIgbAUQgWAOgXAFQgKACgJAAQgPAAgNgFgARsGWQg8gIgegCQgugEguAFIguAEQgeAAgigMQgXgIglgTQhXgug3gtQhIg6gihFQgPgegDgcQgDghAQgWIAOgPIANgPQAIgKAMgZQASgbAhgHQAPgDAUACIAkAEQAYADAxAAQArADAbALIAcAOQARAJAMACQAHACAKAAQgPgrgPgiQg3hxhWgxQABAagcAOQgaAMgdgFQgJgCgqgPQgggLgVAAQgSABgWAIQgLAEgcAOQi9BakkgLIg5gCQggAAgYADIgsAIQgcAFgQABQhOAHhTgsIgdgPQgQgIgNgCQgNgDgTABIggACQhDAChBgpQg6glgsg/QgLgQgDgPQgDgSALgKIABAAQgQAEgWAAQghgBgQgEQgagHghgYIgPgLIgFgEQgqgfglgmQgUAJgMAYQgIAPgIAfIgGASQALgDALAAQAWABANANQAEAFAFAJIAIAPQAOAPAGAJQAQAUgEAdQgDAcgVARQAKAQACAUQABAUgIARQgGANACAFQADAHAKAAIARgCQAMgCAMAGQAMAFAHAKQAHALAAANQAAANgHAKQA0APBCgIQArgFBMgVQBKgUAkgLQA8gUAugWQgeAYgxAQIhXAYIiTAmQhVARg+gSQgQAggfATQgfATgjAAQgkAAgegTQgggVgMghQgLgcADgmQADgaALgqQAMgtgQgRQgLgMgXgCIgpgBQgRgDgfgQQgfgQgRgCQgIgBgXABQgUABgLgDQgUgHgMgVQgLgTgBgYQgBgzAmgrQAkgoA1gQQArgOAfAMQAUAIANASQANASACAVQgCAaACAMQABAHAFALIAHARQAGAXgMALQARADAWgHIAlgPIAEgCIAIghQALgvAkghQg2g2gPhJQgJgpAPgVQAMgRAXgEQAVgEAVAHQAQAGAVANIAkAWQgJgTAEgXQAEgWAPgPQAQgPAXgEQAWgDATAKQAAgQAHgOQAIgPANgIQAHgFAagLQAVgIAJgLQAGgHAFgPIAIgXQAKgZAggZIA1grQAJgJANgRQAPgUAGgHQAbgeAmgRQAsgSAXATQALAJADAQQADAQgGAPQgJAYggAUIgQALQgKAHgEAHQgEAIgCAOIgBAWQgEAbgXAZQgQASgfAUQhBArhHAiQACAXgLAVQgMAVgUALQAMAJAFAPQAFAPgCAOQgGATABAKQAAALAKAQQAUAbAHAPQAOAZgDAUIgEARIgBAAIgDAOIgBACIgBAXQgBAOgDAIQgFALgOAHQALgBAOAGQAXAJAcAXQAeAbAQAMQA+AxAygQIAUgJQAMgGAJgBQAPgCASAJQAKAFAVAPQBKA1BEACQATAAAcgFIAwgHQArgGBfADQBvADBTgIQBogLBTggQAogPBQgjQAxgSApgDQASgCARACQgIgQAJgTQAJgRASgGQAJgDAJAAQAJAAAJAEQARAFAOAOQADgXAXgMQAWgNAVAJQgCgYAagMQAagNAXAKQAVAJARAWQALAOAPAdQgIgWAKgYQAKgYAVgKQgIgRAEgSQAEgTAPgIQAVgMAZAQQAXAPAKAaIAPAxQAKAeAOAPQgHgNAGgPQAGgPANgIQAWgMAlAEQAgAFAPAMQASAOAMAkQApBvgHB5IgFBFQgCAoAHAcQADANALAbQAKAbADAPQADAQgBAQIANALQAiAdAMAtQAMAtgQAoQgOAlgsAnQguApgqAMQgaAHgjAAIgcgBgAOmCLQATAXATAMIgLgYQgHgPgGgHQgGgIgMgKIgVgRQADAVAWAZgAL/BIIAoAkQgBgEgFgGQgMgOgSgOIgGgDQgBACADADgABHrpQhVgWgngoQgPgQgCgOQgCgJADgIQADgJAIgEQANgHARAJIANAJIANAKQAQALAoAKQAeAHALABQALAAAXgCQAQgCAIgCQANgFARgTQARgRAEgMIABgIIAAgLIgDgTQgCgQABgVQABgWAIgHIAFgEIABAAQADgDAAgDQAAgDgFgGIgCgBQgZgYgQgHQgIgEgMgDIgVgHIgUgHQgMgEgJgCQgKgCgQABIgQABQgvAFgWALIgLAGQgNAJgRAOIgBACQgEgEAIgHIAFgFIAUgNIAMgHQAUgLAfgGQAUgDASAAIAMABQAOABAjAKQAkALASAKQAJAFASAOIACABQAQAMAKAFQAPAIAGAEQAMAIAFAJQADAGACAQIAAAEIAEArQACAagEARQgCAKgFAKQgNAegjAdQgkAdgcADIggABQgTADgJAAIgCAAQgLAAgSgEg");
    this.shape_1.setTransform(55.8793, -40.3167);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    // Layer_1
    this.instance_8 = new lib.CachedBmp_302();
    this.instance_8.setTransform(-200.4, -199.2, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(1));

    // Layer_4
    this.instance_9 = new lib.CachedBmp_303();
    this.instance_9.setTransform(-213.75, -123.6, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(1));

    // Layer_5
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#FFFFFF").s().p("A0nbPIg6gDQgbgCgKgDIgRgIIgQgJQgSgIgGgHQgFgGAAgHQAAgIAFgEQAIgHAMAFQAGADAMAIQAOAIAcAEQBPAKAkgCIAiAAQATABAMAHQgOATglAGQgWADgcAAIgHAAgAxHaeIgSgHIgagLIgqgQQgQgHgCgIQgCgGAEgGQAEgGAHgCQAKgDAPAFQAXAGAnATQAJAFAHABIAhABQATAAAIgEQAHgDALgMQAKgLAHgDQAOgEARAJQASALAKAEQAMAFAbABQBEADA/AOQAHABAAAEQgeAJgnABQgYABgvgDQgUgBgOgDQgVgEgfgOQgSAagPAFQgIADgPAAIgwAAIgGABIgHgBgA8DaMQgSgIgWgSQgdgYgWgdIgZgiQgOgUgOgLQgPgMgXgKIgqgRIgygVQgTgIgFgGQgEgGgCgHQgBgHAEgGQAHgKAOACQAGABAPAIQAgAQAdAMIAmAQQAWAJAOALQATAOAVAbIAiAvQAtA7AxACQAOABAhgIQAfgHAQADQAhAFAQAcQgOAKgUgDQgOgCgVgIQgoATgiAAQgXAAgVgIgA1wZzQgSgDgZgQIgvgeQgcgPgYgDQgRgBgjAHQgjAHgRgBQgTgCgbgMIgtgTIgqgLQgZgGgOgKQgNgJgDgKQgCgGACgHQADgHAGgCQAHgDARAIQAQAIAdAIQAhAIANAFIAgAQQAUAJAOACQARACAcgGQAjgHALAAQAcgBAgAPQASAJAkAXQAaARAOAEQAYAJATgIIAUgIQAFgBAGABQAFACACAFQADAIgGAHQgDAEgLAFQghAQgYAAIgIgBgAsTZMIgbgIIg/gVQAHgLARAAQAOgBAOAHIAZANQAPAHAMAAQATgBAYgUQAfgbAIgEQAbgOAtAIIA0AKQAeADATgMQAJgFALgLIATgSQAXgTBDgIQA/gIAXgYQAIgIANgXQAMgWAKgJQAKgIAZgLQAZgKAKgJQAGgGAHgJQiGBSiYA3QkBBdkMgCQhmgBjKgWQhegLgugHQhOgMg8gRQijguiGhsQiHhthOiUQg2hogniaQgKgkgHgjQgCAbgMAgQgJAVAAALQAAAIAFANQAKAbAHANIAaAnQAOAXAHAeQgVgBgTgSQgOgMgRgYQgXgjgJgZQgOgjAIgeQACgJALgZQAIgVABgNQABgNgJgcIgchZQgQg4ADg8IAFgrIAHgpQAGggAGgRQAJgbAOgTIAMgPQAGgJABgIIAJADQABAOgEARIgIAeQgIAdgLBHQgGAxABAUQAAATAGAdQAKA3AVA3IAAACQgZiaAOh7QANhtArheQAuhlBMhEQBIhABugrQBRggB+gbQAxgKAXAGQATAFAMAPQANAPgCASQgDAnhGANIiVAfQhUAUg5AhQhMAtg1BPQgzBKgXBbQgnCaAmDOQArDmBqCiQA7BcBOBEQBSBJBfAnQBEAdBZARQA3AKBsANQDJAaBtABQDcAEDXhAQDWg/C3h7QCVhjBJhtIA+hlQAmg7AkggQAngiBHgdIB4gvQBBgdAqgsQAwg0AAg8IgFg1QgDgfAIgTQATgtBUgRQAcgHCPgbQBogTBBgTQDFg2BrhrQAUgTAHgGQAPgMAQgDQAOgDAUAGIAiALQAlALAuAAQAkgBAxgJQB8gXBeg1QBsg9AzhfQAnhJAGhiQAGhMgOhnQgOhmgpi5QgsjFgOhZIgWiYQgNhYgTg9QgwiZhohZQglgigqgRQgbgLgjgGQgWgDgqgEQhRgHgsAGQgyAGg4AWQgrARg4AgQhtA9hoBQQArgvBHguQARgLBwhAQA/glAYgMQAygZArgJQAhgHApAAQAbgBAxAEQBPAFAwAKQBEAOAxAdQA8AjAtBBQAnA5AYBKQATA7AOBTIAWCQIAEAVIAEAGQAgA1AKAVQAMAbATA2QATA3ALAZQALAYACAGQADARAEAIQACAFAFAIIAIANQADAHAEAKIAaBFQAIAigJAgIgIAAQgCgegJgeQgFgQgQgmIgehEQgphegOgxQgIgegGgPIgSgnIgJgaIBHFaQAyD0AACjQAAB0gcBMIAIgEQARgLAIgNQAGgJAEgRQAFgTABgNQACgRgDgYIgGgpQgEgcAAghQAIgCAEAFQAJAmAGAhIAFAgQAFAngFAbQgHAngWAUQgLALgZANIgWANQgXArglApQgUArgRAQQgNAOgiATQgyAcguAgQgNAIgHABQgGABgFgCQgGgCgCgFQgeANgfALQh+AqiBgIIgrgCQgYAAgSAFQgUAFgYAOIgpAcQh3BRinAxQh7Aki9AbQgQADgJADQgNAEgHAJQgJAKgBARIABAgQABAqgPArIAXgJQAPgHAGgJQAGgJACgRIAPhsQAbAcgQA7QgKApgWASQgJAIgaALIgRAIQgRAngdAhQgVA5gnAtQAlgUAlgPQATgHAIgGQAJgIAMgRQAagnACgdQADgCACAEQACADAAAEQgBANgEAPQgIAbgRAWQgSAXgYAOQgNAHgbANQgSAKgbAUQggAXgRAQIgdAeQgwAvg4ALQgVAEghABIg3AAIgVgYQARgKAcABIAwABQAdAAAbgLQAlgOAdgdQAVgYAMgLIATgPIgDgDQAvghAagyIAFgNQg3AwhUAiQhAAXggANQg2AWghAcQggAcggA1Ig2BaQguBEhWBJQgxApg0AlQgWATgMARIgQAcQgKAQgJAIQgKAKggANQgdAMgLANQgGAHgHAOQgIARgEAFQgYAihGAJQgnAGgPAEQgdAHgSAPIgPAQQgJAJgHAFQgZAQgsgJIg4gOQgggGgWAJQgMAFgZAWQgXATgQAEQgGACgHAAIgPgCgA/zV4QgPgFgSgMQgQgLgGgHQgJgJgLgWIgbgzQgDgHABgEQABgGAJAAQAGAAAGAFQAQALAUAeQAWAhANALIAZATQAPALAGALQgHAEgKABIgFABQgHAAgGgDgA9wVxIgbgSIgXgPQgOgIgIgHQgVgPgggnIgTgVIgWgRQgYgSgUgoQgTglgLgtQgEgSAJgFQAHgEAIAGQAEAEAFAKIAfBBQAJAUAIALQALASAQAMIASAPQAFAEAEAIIAIANQAFAIAOARQAQATAHAGQAHAGAXANQAZAPAYAUQAIAHAAAHQgBAEgEACQgDADgFAAQgGAAgJgGgAACUiQAcgTAegZQAugmAdgeQAmgnAXgoIAWgmQAOgVAPgLQAfgUA6AJQAeAEALgBQAYgCALgPQAEgFADgIIAFgNQAHgQAMgCQgNAggMAQQgSAYgXAHQgOAEgfgDQgegCgPAFQgYAHgTAcQgLAQgRAiQgRAegeAgQgSATgnAiQguApgYAOQgaAOgkALQgoAMgiAGQBCgfAegUgAMsIYQgZgGgNgMQACgIANABQANACAZAGQAaAHAMACQAcAEArgGIARgDQgRABgOgHQADgLANgIIAYgLQARgIAWgRQArggAogkIAXgUQANgKAMgFQAhgPBIANIAQACQABgIAAgJQgBgMABgEQABgEAEgCQAEgCADABQAFADABAKQABAQgCAOQAvAEAZgKQAdgMAZgjQAJgMAKgSIARggQAHgMAFgFQAKgIAKAEQAHADAAAGQAAADgDAFQgZAugNAWQgYAmgaAUQgtAig2gDIgXgDQgOAVgdARIghARIghARIgzAjQgeAUgZAFQgIACgMABIgUABQgMABgSAEIgeAGQgZAEgYAAQgqAAgpgMgAR6GIQgPAGgbAYQgxArgsAgQgaATgSAFIAngGQAjgEAIgCQAUgFAXgRIAogcQAQgKAcgOQAbgNALgGQAQgKAJgMIgZgGQgRgCgNAAQgWAAgQAGgAXsGzQAQgPAcgKQAQgGAigJQA7gRAzgjQAzgiAngwIApg5QAXgjAQgRQAZgaAbgKQAJgDAXgEQAUgEALgFQAfgOAVgoQAXgqgSgYQADgGAIABQAHABAFAFQAGAJgCAQQgEAmgYAfQgXAggiAPIgiAMQgVAHgMAHQgSALgRAXQgIALgTAfQgNAWgLAQQglAyhGAzQgeAWgWAKQgYAMg3ASQggAKgQACQgMACgKAAQgPAAgMgEgEgkRAFsQgBgTAFgjIAOhbQAEgfAGgRQAIgaAQgPQAGgGAUgNQARgLAHgJQAMgPAEghQAFgnAFgMQANgdAcgKQAEAAADAEQAEADgBAEQgBAEgHAIQgSAPgGAUQgDAMAAAYQAAAlgNATQgJAPgXAQQgUAOgGAGQgNAOgFAYIgGAsIgNBOIgJAxQgGAagJAbQgMgVAAgfgAW5FbQALgCAXgBQAYAAALgCQASgEAagPQAfgRANgEQAKgFAHADQAFABACAEQADAFgCAEQgCAEgKADQgOAFgWAQQgZASgMAFQgPAGgmAEIhjAJQAUgdAjgIgEgh9ABiQgFgEACgMQAGgjAKgQQAQgZApgQIAigNQATgIANgJQANgJAFgLQAEgHABgSQADgvARgTQAKgNATgHQALgFAXgFIBOgSQAhgIAWgDIAfgCQASgCAMgEQASgGACAKQACAHgOAGQgNAEghAIQgeAIgRAGIgkANIguALIgiAKQgQAHgFAEQgNAMgDAaIgBAVQgBANgDAIQgIAcggAUQgMAGgRAHIgeANQgnAPgMAXIgKAWQgHALgNAEIgHACQgEAAgCgDgEAihAA4QABgNAHgcQAVhaAChZQABhCADgUQAGgwAVgeIAKgNQAFgHADgGQAHgRgFgbQgFgfgTgvQgYg9gFgQQgCgHAAgEQACgGAHgBQAGgCAGAEQAHAFAGAPQAYBBATA/QAHAXACAOQACAVgHAPQgDAHgIAKIgMAQQgTAZgFAoQgDAUAAA0QABBRgKA9QgNBLggA5QgHgRADgXgA1kh5QgHgLAIgRQAghICEgdQA9gNBsgNQB6gPAwgJQC0ggBzhIQBOgwBNhUQAxg0BShqQAYgeAKgWQANgfgIgaQguAJgrgYQgsgYgRgrQgYAXgjAKQghAJgjgDQg9gFhCgqQhHgtgRg4QgLgiAIgrQAFgbASgxQhWARhKgNQhWgPg0g1QgxgzgUhZQgPhAAIg6QAIhCAjguQAcgkAxgcQAggTA8gYQBgglA2gKQAbgGAUAEQAaAFAKATQAKASgLAXQgKAVgUALQgQAIgaAGIgrAIQgrAKglAaQglAagYAlQgQAaABATQABAPAOAZQAPAaACANQADAXgRATQgSAUgXgBQgXgBgQgVQgGgHgDgHQAFAaANAZQAUAmAiAYQAiAXArAFQArAFAngPQANgFAhgTQAcgPATgFQAagIAaAJQAbAJAIAYQAEAOgEARQgDAMgIAUIgmBZQgJAWgEAOQgFAUAEARQAIAiAsAZQAfASAeAEQAiAFAbgNQAKgFAwgrQAigeAcAEQASADAOAOQANAOAGASQAFAQAAAUIgBAlQAtgMAtAXQAtAYAOAsQALAegGAlQgGAggRAgQgOAagYAeIgtA0IhlB7Qg8BHg2AnQghAYgtAXQgfAPgzAXQhmArhBARQhHATjKASQitAQhfAmQgPAHgKAAQgLAAgGgIg");
    this.shape_2.setTransform(1.5949, 66.4186);

    this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-230.6, -229.1, 478.29999999999995, 469.9);


  (lib.Stageon = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol28("synched", 0);
    this.instance.setTransform(8.4, 5.7, 5.5976, 4.688, 0, 0, 0, 1.3, 3.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-397.9, -213.2, 812.0999999999999, 440.2);


  (lib.Light = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_14 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(14).call(this.frame_14).wait(1));

    // Layer_1
    this.instance = new lib.Stageon("synched", 0);
    this.instance.setTransform(6.05, 821);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(15));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AC5QaQg2g8AAh3IAA1DQl2EhiCAAQg+AAgxgxQgxgxABhDQgBhKAxglQAugjB6g4QCyhUBrhdQBqhdBTh1QBUhzAZgbQAZgbBFAAQBPABAvA9QAvA9gBBrIAAadQABEpjKABQhagBg5g8g");
    this.shape.setTransform(-22.65, 353.8);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(15));

    // Layer_3
    this.instance_1 = new lib.C_ON("synched", 0);
    this.instance_1.setTransform(0, 350.85, 1.0374, 1.0374);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(15));

    // Layer_5
    this.instance_2 = new lib._3_Stars("synched", 0);
    this.instance_2.setTransform(2.95, 50.95, 0.2172, 0.2172);
    this.instance_2._off = true;

    // this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({_off: false}, 0).to({
    //   scaleX: 0.7133,
    //   scaleY: 1.4347,
    //   x: 3,
    //   y: 30.95
    // }, 2).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
    //   regX: 0,
    //   regY: 0,
    //   scaleX: 1,
    //   scaleY: 1.1333,
    //   x: 2.95,
    //   y: 30.95
    // }, 2).to({scaleY: 1}, 1).wait(7));

    // Layer_6
    this.instance_3 = new lib._3_stae_L("synched", 0);
    this.instance_3.setTransform(0, -0.05);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol519("synched", 0);
    this.instance_4.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);


    // Layer_7
    this.instance_5 = new lib._3_Star_R("synched", 0);
    this.instance_5.setTransform(-0.05, 0);
    this.instance_5._off = true;

    this.instance_6 = new lib.Symbol520("synched", 0);
    this.instance_6.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    // this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_5}]}, 5).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_6}]}, 2).wait(2));
    // this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(5).to({_off: false}, 0).to({
    //   regX: -0.3,
    //   regY: -0.1,
    //   scaleX: 0.7989,
    //   scaleY: 0.7989,
    //   rotation: -28.602,
    //   x: -88.15,
    //   y: 20
    // }, 3).to({rotation: -126.6131, x: -125.7, y: 28.9}, 3).to({
    //   _off: true,
    //   regX: -165.8,
    //   regY: 48.6,
    //   scaleX: 1,
    //   scaleY: 1,
    //   rotation: 0,
    //   x: -165.8,
    //   y: 48.6
    // }, 2).wait(2));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-391.9, -173.4, 812.2, 1221.4);


  (lib.Tween6copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.text = new cjs.Text("4", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-3.95, -40.25, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,87,255,0.659)").s().p("AnXHuQjBjEAAkkQAAkiDJjKQDKjJEPAAQERABC7DJQC7DJAIEuQAIEvjIC3QjHC2kUACIgEAAQkRAAjAjCg");
    this.shape.setTransform(-0.0207, -0.312);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_1
    this.instance = new lib.CachedBmp_382();
    this.instance.setTransform(-80.85, -81.7, 0.5, 0.5);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.5);


  (lib.Tween6copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween6copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ON("synched", 0);
    this.instance.setTransform(0, 7.55, 0.3601, 0.3601);

    this.instance_1 = new lib.C_1_ON("synched", 0);
    this.instance_1.setTransform(0, 7.55, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-80.8, -81.7, 161.7, 163.4);


  (lib.Tween5copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween5copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ON("synched", 0);
    this.instance.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ON("synched", 0);
    this.instance_1.setTransform(0, 6.85, 0.4491, 0.3281, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.8, -74.4, 201.7, 148.9);


  (lib.Tween4copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween4copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ON("synched", 0);
    this.instance.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.instance_1 = new lib.C_1_ON("synched", 0);
    this.instance_1.setTransform(0.15, 9.15, 0.1152, 0.4372, 0, 0, 0, 1.3, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-25.8, -99.1, 51.7, 198.3);


  (lib.Tween3copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween3copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ON("synched", 0);
    this.instance.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.instance_1 = new lib.C_1_ON("synched", 0);
    this.instance_1.setTransform(0.1, 3.85, 0.7563, 0.1881, 0, 0, 0, 0.1, -0.6);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-169.8, -42.7, 339.70000000000005, 85.4);


  (lib.Tween2copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween2copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ON("synched", 0);
    this.instance.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.instance_1 = new lib.C_1_ON("synched", 0);
    this.instance_1.setTransform(0, 13.45, 0.1552, 0.6422, 0, 0, 0, 0, -0.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-34.8, -145.7, 69.69999999999999, 291.4);


  (lib.Tween1copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy5("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy4("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy4("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy3("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy2("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy2("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ONcopy("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ONcopy("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Tween1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.C_1_ON("synched", 0);
    this.instance.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.instance_1 = new lib.C_1_ON("synched", 0);
    this.instance_1.setTransform(0.05, 7.6, 0.1821, 0.3601, 0, 0, 0, 0.3, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.8, -81.6, 81.69999999999999, 163.3);


  (lib.Symbol28_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.instance_1 = new lib.CachedBmp_381();
    this.instance_1.setTransform(-64.4, -39.45, 0.0778, 0.0778);

    this.instance_2 = new lib.Path_0();
    this.instance_2.setTransform(-23.9, -5.65, 0.9999, 0.9999);
    this.instance_2.alpha = 0.4883;
    this.instance_2.compositeOperation = "multiply";

    this.instance_3 = new lib.Path_1_1();
    this.instance_3.setTransform(0.75, -12, 0.9999, 0.9999, 0, 0, 0, 24.3, 6.1);
    this.instance_3.alpha = 0.4883;
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_279();
    this.instance_4.setTransform(-64.35, -42.9, 0.0778, 0.0778);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}]}).wait(1));

    // Layer_2
    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#3860B1").s().p("Ah7CdQgNgCgKgHQgLgIgEgMQgmADgSgDQgggEgSgUQgOgPgCgWQgDgWAKgTIAGgMQAEgGABgGQADgIgBgSQACgYAYgXQAKgJATgLIARgKIA6ggQAWgMAMgEQAUgIAQAAQAUAAAQALQAQAMACASQBXgdAkgIQAjgHAQAJQAZANAGAvQAVgGAWAJQAVAJALATQAHALABANQABAJgBAJQgDAWgQAQQAPAGAHAQQAHAQgIAMQgIANgYAHQhDAVhdAQQg2AKhsARQgWAEgPAAIgNgBg");
    this.shape_3.setTransform(0.6373, -4.725);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#5387EE").s().p("AjAG3QhwgOhPgwQgbgQglgeQhphThMhXQgkgqgMgdQgYg2AMhSQARhpA5hJQAaggAuglQBRhBBYgnQBfgqBggGQAxgEA7AGQAqAEBCALQCAAVBXAZQBzAgBWAyQAAgKAJgGQAIgGAKAAQAOAAAWANQAgAUA9AxQAYAUAKAKQASARALARQANAUAEAVQAFAYgIAUIgFANQgCAHAAAGQAAAGAJAUQAJAUgCAbQgBASgHAfQgJAogKARQgIAQgSAUIgfAgIgWAdIgVAdQghAng1AiQgiAVhDAhQhQAogoAMQg2ARhFAGQgqAEhUABIgrAAQhkAAg6gIgAoihCQgdAdgCAiQAPAIAKAYQAIAdAGANQAMAdAmAdQAsAeATASQAKAJAUAWQASAVALAKQArAmBQAUQBKATBYACQBJACBagJQBBgGAwgLQBHgPBVgmQBNgjAmgiQA5g1AFhAQAEgogUgtQgOghggguQgjg0gbgVQgYgSgqgSQhNghhlgXQhFgQhygSQgzgIgigDQhkgGhmAlQhhAjhTBDQg/AygSAxQAqgEApgSQgrAbgPAQgAJThzQAAABABAAQAAABAAAAQABABAAAAQAAAAABAAQACgCgBgEIgDgEIgEgEIgCgDIgDgBgAkjBSQhpgtAAg/QAAhABpgtQAlgPApgLQgTAMgKAJQgYAWgCAZQABASgDAJQgBAFgEAHIgGALQgKATADAWQACAWAOAPQASATAgAFQASADAmgDQAEALALAIQAKAHANADQARADAhgGQBsgRA2gKQBdgRBDgVQAYgHAIgMQAIgMgHgQQgHgPgPgHQAQgPADgXQABgJgBgJQAzAjAAAsQAAA/hqAtQhqAtiVAAQiVAAhqgtg");
    this.shape_4.setTransform(1.1041, 2.2029);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_4}, {t: this.shape_3}]}).wait(1));

    // Layer_3
    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#FFFFFF").s().p("AjfG4QgngKg7gWQhEgagjgQQg4gbgngfQgwgng3hNQgkgxgVgoQgbgzgLgvQgKgtADgnQAEg1AehFQAYg4AbgfQAWgZAlgZQB6hSCtgYQCCgRC4AQQBJAGApAKQAcAGAwAQQA1AQAXAGQAqALBXANQBMAOAuAZQA9AgAyBEQBGBggCBiQAAAhgNA2QgNA0gSAdQgLATgSATIgiAjQhEBAgoAdQhQA6hxAhQhXAZh9AQIhLAJQhKAHg1AAQhFAAg5gOgAiqlKQhlAChRATQhOAThIAnQgxAagZAdQgSAVgQAjQgWAzACAoQACAnAeA3QBLCQBoA7QAbAQA8AYIBPAfQAtAQAlAHQBCAOBTgCQA3gCBegMQBfgLAzgOQgZgOhFgOQg+gMgcgXIA9gJQArgHAbgHIApgNIAogNQATgFAjgHIA2gMQA7gQAygiQAmgaAZggQAcglAGgnQAJgxgYgyQgWgvgrgiQghgZgngOQgdgKgtgHIhLgNQgigHhOgYQhIgXgpgHQgZgEgygEQhggHhBAAIgXAAg");
    this.shape_5.setTransform(1.2562, 5.5929);

    this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-71.3, -42.9, 145.1, 93.9);


  (lib.Symbol21 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Tween5("synched", 0);

    this.instance_1 = new lib.Tween6("synched", 0);
    this.instance_1.setTransform(75.8, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.instance_1}]}, 779).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true, x: 75.8}, 779).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-177.1, -59.6, 430.1, 119.2);


  (lib.Symbol18_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance_2 = new lib.Tween3("synched", 0);

    this.instance_3 = new lib.Tween4("synched", 0);
    this.instance_3.setTransform(43.6, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_2}]}).to({state: [{t: this.instance_3}]}, 739).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true, x: 43.6}, 739).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-138.9, -58, 321.5, 116.1);


  (lib.Symbol17_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance_1 = new lib.Tween1("synched", 0);

    this.instance_2 = new lib.Tween2("synched", 0);
    this.instance_2.setTransform(117.35, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}]}).to({state: [{t: this.instance_2}]}, 699).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off: true, x: 117.35}, 699).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-152.1, -80.2, 421.6, 160.4);


  (lib.Symbol15_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Symbol12("synched", 0);
    this.instance.setTransform(0, 0.05, 1, 1, 0, 0, 0, 0.7, -0.8);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#FFFFFF").s().p("Ehv7BXZMAAAiuxMDf2AAAMAAACuxgEhhpBI/MDB1gDXMAAAiSVMjD9AAAg");
    this.shape_5.setTransform(-2.1, 1.525);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_5}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-718.4, -557.7, 1432.6999999999998, 1118.5);


  (lib.Symbol9_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance_28 = new lib.Symbol21();
    this.instance_28.setTransform(-1228.45, -301.5, 1.5356, 1.5356, 0, 0, 0, 0.1, -0.1);

    this.instance_29 = new lib.Symbol21();
    this.instance_29.setTransform(321.65, -272.85, 1.5356, 1.5356, 0, 0, 0, 0.1, -0.1);

    this.instance_30 = new lib.Symbol18_1();
    this.instance_30.setTransform(-274.2, -303.8, 1.5356, 1.5356);

    this.instance_31 = new lib.Symbol17_1();
    this.instance_31.setTransform(-822.55, -304.5, 1.5356, 1.5356, 0, 0, 0, -0.1, -0.1);

    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["#FFFFFF", "#00CCFF"], [0, 1], 9.5, -128.9, 9.7, -733.2).s().p("EirDBwTMAAAjglMFWHAAAMAAADglg");
    this.shape.setTransform(-428.775, 148.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape}, {t: this.instance_31}, {t: this.instance_30}, {t: this.instance_29}, {t: this.instance_28}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol9_1, new cjs.Rectangle(-1523.5, -569.9, 2189.5, 1437.4), null);


  (lib.Symbol7copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFC000").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACIgEgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape.setTransform(5.8, -5.3833);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#542000").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBAAAAgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape_1.setTransform(5.8, -3.8833);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f().s("#F0EDED").ss(0.1, 1, 1).p("AEbABQAAB0hUBTQhSBTh1AAQh0AAhUhTQhShTAAh0QAAh1BShTQBUhTB0AAQB1AABSBTQBUBTAAB1g");
    this.shape_2.setTransform(6.125, -3.425);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("rgba(0,0,0,0.329)").s().p("AjIDIQhShTAAh0QAAh1BShTQBUhTB0AAQB1AABSBTQBUBTAAB1QAAB0hUBTQhSBTh1AAQh0AAhUhTg");
    this.shape_3.setTransform(6.125, -3.425);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#F3F3E8").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACIgEgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape_4.setTransform(5.8, -5.3833);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("rgba(52,100,196,0.769)").s().p("AhhBuQgKgKgGgKIgBgEQAAgBAAAAQABgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAlAAQABAAAAAAQABAAAAAAQABAAAAABQABAAAAAAQAYAYAjAAQAhAAAZgaQAZgZAAglQAAgkgZgZQgMgOgPgGIABAiQAAAAAAABQAAABAAAAQgBABAAAAQAAAAgBABIgDACQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBAAAAgBIhkg6IgDgDIAAgEIADgDIBgg2IAEAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAIABAEIACAjQAgAIAZAaQAkAmAAA0QAAA1gkAnQglAlgyAAQg1AAgkglg");
    this.shape_5.setTransform(5.8, -3.8833);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).to({state: [{t: this.shape_3}, {t: this.shape_2}]}, 1).to({state: [{t: this.shape_5}, {t: this.shape_4}]}, 1).wait(2));

    // Layer 1
    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#FFFFFF").s().p("AgbBfQgEgEAAgFQAAgJALgFQApgTAMgZQgWAAgKgGQgMgGAAgUQAAgPAJgOQAJgPAOAAQARAAALAOQAKANACATIAoAAQgHgQAAgKQAAgOAMgNQANgPAUgBQALAAALAHQAOAGAAALQAAALgJAAQgEAAgIgEQgIgDgFAAQgVABAAAQQAAAKAHAGQAHAFAKAAQAKAAAVgFIAGAAQAOAAAAAKQAAALgMAFQgfANg1gBIgoAAQgNAvg5AXIgHABQgGgBgDgDgAAMgRQgDADAAAEQAAAHAKACIAPABQAAgIgEgFQgEgHgHAAQgEAAgDADgAh0A3QABgMATgCIAegDQAWgFAAgGQACgOgggaQgLgKABgHQAAgGAFgEQAEgDAGAAQAPACARAaQARAZAAAQQAAATgOAKQgIAGgUAGQgNADgPABQgcgBACgPgAilAoQgKgIAAgPQAAgLAFgMQAEgJAFgGQgLgFAAgJQAAgGAFgFQAEgGAGAAQAUABAVARQAVATAAATQAAAVgPALQgMAMgSgBQgQAAgJgHgAiXAHQAAALAMAAQAFgBAFgCQAFgDAAgEQAAgLgTgIQgIAKAAAIgAiQhAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGgAi8hAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGg");
    this.shape_6.setTransform(5.6825, -3.8402, 0.8692, 0.8692);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#330000").s().p("AgbBfQgEgEAAgFQAAgJALgFQApgTAMgZQgWAAgKgGQgMgGAAgUQAAgPAJgOQAJgPAOAAQARAAALAOQAKANACATIAoAAQgHgQAAgKQAAgOAMgNQANgPAUgBQALAAALAHQAOAGAAALQAAALgJAAQgEAAgIgEQgIgDgFAAQgVABAAAQQAAAKAHAGQAHAFAKAAQAKAAAVgFIAGAAQAOAAAAAKQAAALgMAFQgfANg1gBIgoAAQgNAvg5AXIgHABQgGgBgDgDgAAMgRQgDADAAAEQAAAHAKACIAPABQAAgIgEgFQgEgHgHAAQgEAAgDADgAh0A3QABgMATgCIAegDQAWgFAAgGQACgOgggaQgLgKABgHQAAgGAFgEQAEgDAGAAQAPACARAaQARAZAAAQQAAATgOAKQgIAGgUAGQgNADgPABQgcgBACgPgAilAoQgKgIAAgPQAAgLAFgMQAEgJAFgGQgLgFAAgJQAAgGAFgFQAEgGAGAAQAUABAVARQAVATAAATQAAAVgPALQgMAMgSgBQgQAAgJgHgAiXAHQAAALAMAAQAFgBAFgCQAFgDAAgEQAAgLgTgIQgIAKAAAIgAiQhAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGgAi8hAQgGgFAAgJQAAgIAGgFQAGgHAIAAQAIAAAGAHQAGAFAAAIQAAAJgGAFQgGAGgIAAQgIAAgGgGg");
    this.shape_7.setTransform(5.6825, -2.8902, 0.8692, 0.8692);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape_7}, {t: this.shape_6}]}, 1).to({state: []}, 1).wait(2));

    // Layer 1
    this.instance = new lib.CachedBmp_380();
    this.instance.setTransform(-25.1, -35.05, 0.2582, 0.2582);

    this.instance_1 = new lib.Path();
    this.instance_1.setTransform(6.2, -3.4, 1.3907, 1.3907, 0, 0, 0, 19.7, 19.8);
    this.instance_1.compositeOperation = "screen";

    this.instance_2 = new lib.CachedBmp_275();
    this.instance_2.setTransform(-25.4, -35.1, 0.2582, 0.2582);

    this.instance_3 = new lib.Path_1();
    this.instance_3.setTransform(6.1, -3.8, 1.4695, 1.4695, 0, 0, 0, 19.7, 19.7);
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_379();
    this.instance_4.setTransform(-25.3, -35.2, 0.2582, 0.2582);

    this.instance_5 = new lib.CachedBmp_378();
    this.instance_5.setTransform(-30.05, -39.9, 0.2582, 0.2582);

    this.instance_6 = new lib.Blend();
    this.instance_6.setTransform(-0.45, 1.55, 1, 1, 0, 0, 0, 41.5, 38.5);
    this.instance_6.compositeOperation = "multiply";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_6}, {t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-41.9, -39.9, 83.9, 80);


  (lib.Stageoncopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol28copy9("synched", 0);
    this.instance.setTransform(8.4, 5.7, 5.5976, 4.688, 0, 0, 0, 1.3, 3.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-397.9, -213.2, 812.0999999999999, 440.2);


  (lib.Stageoncopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol28copy8("synched", 0);
    this.instance.setTransform(8.4, 5.7, 5.5976, 4.688, 0, 0, 0, 1.3, 3.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-397.9, -213.2, 812.0999999999999, 440.2);


  (lib.Stageoncopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol28copy7("synched", 0);
    this.instance.setTransform(8.4, 5.7, 5.5976, 4.688, 0, 0, 0, 1.3, 3.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-397.9, -213.2, 812.0999999999999, 440.2);


  (lib.Stageoncopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol28copy6("synched", 0);
    this.instance.setTransform(8.4, 5.7, 5.5976, 4.688, 0, 0, 0, 1.3, 3.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-397.9, -213.2, 812.0999999999999, 440.2);


  (lib.Stageon_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance_1 = new lib.Symbol28("synched", 0);
    this.instance_1.setTransform(8.4, 5.7, 5.5976, 4.688, 0, 0, 0, 1.3, 3.8);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-397.9, -213.2, 812.0999999999999, 440.2);


  (lib.Lightcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_14 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(14).call(this.frame_14).wait(1));

    // Layer_1
    this.instance = new lib.Stageoncopy4("synched", 0);
    this.instance.setTransform(6.05, 821);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(15));

    // Layer_2
    this.text = new cjs.Text("6", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-7.6, 238.65);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(15));

    // Layer_3
    this.instance_1 = new lib.C_ONcopy5("synched", 0);
    this.instance_1.setTransform(0, 350.85, 1.0374, 1.0374);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(15));

    // Layer_5
    this.instance_2 = new lib._3_Starscopy5("synched", 0);
    this.instance_2.setTransform(2.95, 50.95, 0.2172, 0.2172);
    this.instance_2._off = true;

    // this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({_off: false}, 0).to({
    //   scaleX: 0.7133,
    //   scaleY: 1.4347,
    //   x: 3,
    //   y: 30.95
    // }, 2).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
    //   regX: 0,
    //   regY: 0,
    //   scaleX: 1,
    //   scaleY: 1.1333,
    //   x: 2.95,
    //   y: 30.95
    // }, 2).to({scaleY: 1}, 1).wait(7));

    // Layer_6
    this.instance_3 = new lib._3_stae_Lcopy5("synched", 0);
    this.instance_3.setTransform(0, -0.05);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol517("synched", 0);
    this.instance_4.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    // this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).wait(2));
    // this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5).to({_off: false}, 0).to({
    //   scaleX: 0.7987,
    //   scaleY: 0.7987,
    //   rotation: 36.6297,
    //   x: 92,
    //   y: 29.9
    // }, 3).to({regX: -0.1, regY: -0.1, rotation: 195.5884, x: 124.15, y: 30.9}, 3).to({
    //   _off: true,
    //   regX: 179.1,
    //   regY: 40.8,
    //   scaleX: 1,
    //   scaleY: 1,
    //   rotation: 360,
    //   x: 179.1,
    //   y: 40.8
    // }, 2).wait(2));

    // Layer_7
    this.instance_5 = new lib._3_Star_Rcopy5("synched", 0);
    this.instance_5.setTransform(-0.05, 0);
    this.instance_5._off = true;

    this.instance_6 = new lib.Symbol518("synched", 0);
    this.instance_6.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    // this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_5}]}, 5).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_6}]}, 2).wait(2));
    // this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(5).to({_off: false}, 0).to({
    //   regX: -0.3,
    //   regY: -0.1,
    //   scaleX: 0.7989,
    //   scaleY: 0.7989,
    //   rotation: -28.602,
    //   x: -88.15,
    //   y: 20
    // }, 3).to({rotation: -126.6131, x: -125.7, y: 28.9}, 3).to({
    //   _off: true,
    //   regX: -165.8,
    //   regY: 48.6,
    //   scaleX: 1,
    //   scaleY: 1,
    //   rotation: 0,
    //   x: -165.8,
    //   y: 48.6
    // }, 2).wait(2));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-391.9, -173.4, 812.2, 1221.4);


  (lib.Lightcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_14 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(14).call(this.frame_14).wait(1));

    // Layer_1
    this.instance = new lib.Stageoncopy3("synched", 0);
    this.instance.setTransform(6.05, 821);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(15));

    // Layer_2
    this.text = new cjs.Text("5", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-7.6, 238.65);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(15));

    // Layer_3
    this.instance_1 = new lib.C_ONcopy4("synched", 0);
    this.instance_1.setTransform(0, 350.85, 1.0374, 1.0374);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(15));

    // Layer_5
    this.instance_2 = new lib._3_Starscopy4("synched", 0);
    this.instance_2.setTransform(2.95, 50.95, 0.2172, 0.2172);
    this.instance_2._off = true;

    // this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({_off: false}, 0).to({
    //   scaleX: 0.7133,
    //   scaleY: 1.4347,
    //   x: 3,
    //   y: 30.95
    // }, 2).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
    //   regX: 0,
    //   regY: 0,
    //   scaleX: 1,
    //   scaleY: 1.1333,
    //   x: 2.95,
    //   y: 30.95
    // }, 2).to({scaleY: 1}, 1).wait(7));

    // Layer_6
    this.instance_3 = new lib._3_stae_Lcopy4("synched", 0);
    this.instance_3.setTransform(0, -0.05);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol515("synched", 0);
    this.instance_4.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    // this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).wait(2));
    // this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5).to({_off: false}, 0).to({
    //   scaleX: 0.7987,
    //   scaleY: 0.7987,
    //   rotation: 36.6297,
    //   x: 92,
    //   y: 29.9
    // }, 3).to({regX: -0.1, regY: -0.1, rotation: 195.5884, x: 124.15, y: 30.9}, 3).to({
    //   _off: true,
    //   regX: 179.1,
    //   regY: 40.8,
    //   scaleX: 1,
    //   scaleY: 1,
    //   rotation: 360,
    //   x: 179.1,
    //   y: 40.8
    // }, 2).wait(2));

    // Layer_7
    this.instance_5 = new lib._3_Star_Rcopy4("synched", 0);
    this.instance_5.setTransform(-0.05, 0);
    this.instance_5._off = true;

    this.instance_6 = new lib.Symbol516("synched", 0);
    this.instance_6.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    // this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_5}]}, 5).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_6}]}, 2).wait(2));
    // this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(5).to({_off: false}, 0).to({
    //   regX: -0.3,
    //   regY: -0.1,
    //   scaleX: 0.7989,
    //   scaleY: 0.7989,
    //   rotation: -28.602,
    //   x: -88.15,
    //   y: 20
    // }, 3).to({rotation: -126.6131, x: -125.7, y: 28.9}, 3).to({
    //   _off: true,
    //   regX: -165.8,
    //   regY: 48.6,
    //   scaleX: 1,
    //   scaleY: 1,
    //   rotation: 0,
    //   x: -165.8,
    //   y: 48.6
    // }, 2).wait(2));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-391.9, -173.4, 812.2, 1221.4);


  (lib.Lightcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_14 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(14).call(this.frame_14).wait(1));

    // Layer_1
    this.instance = new lib.Stageoncopy2("synched", 0);
    this.instance.setTransform(6.05, 821);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(15));

    // Layer_2
    this.text = new cjs.Text("4", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-7.6, 238.65);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(15));

    // Layer_3
    this.instance_1 = new lib.C_ONcopy3("synched", 0);
    this.instance_1.setTransform(0, 350.85, 1.0374, 1.0374);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(15));

    // Layer_5
    this.instance_2 = new lib._3_Starscopy3("synched", 0);
    this.instance_2.setTransform(2.95, 50.95, 0.2172, 0.2172);
    this.instance_2._off = true;

    // this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({_off: false}, 0).to({
    //   scaleX: 0.7133,
    //   scaleY: 1.4347,
    //   x: 3,
    //   y: 30.95
    // }, 2).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
    //   regX: 0,
    //   regY: 0,
    //   scaleX: 1,
    //   scaleY: 1.1333,
    //   x: 2.95,
    //   y: 30.95
    // }, 2).to({scaleY: 1}, 1).wait(7));

    // Layer_6
    this.instance_3 = new lib._3_stae_Lcopy3("synched", 0);
    this.instance_3.setTransform(0, -0.05);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol513("synched", 0);
    this.instance_4.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    // this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).wait(2));
    // this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5).to({_off: false}, 0).to({
    //   scaleX: 0.7987,
    //   scaleY: 0.7987,
    //   rotation: 36.6297,
    //   x: 92,
    //   y: 29.9
    // }, 3).to({regX: -0.1, regY: -0.1, rotation: 195.5884, x: 124.15, y: 30.9}, 3).to({
    //   _off: true,
    //   regX: 179.1,
    //   regY: 40.8,
    //   scaleX: 1,
    //   scaleY: 1,
    //   rotation: 360,
    //   x: 179.1,
    //   y: 40.8
    // }, 2).wait(2));

    // Layer_7
    this.instance_5 = new lib._3_Star_Rcopy3("synched", 0);
    this.instance_5.setTransform(-0.05, 0);
    this.instance_5._off = true;

    this.instance_6 = new lib.Symbol514("synched", 0);
    this.instance_6.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    // this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_5}]}, 5).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_6}]}, 2).wait(2));
    // this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(5).to({_off: false}, 0).to({
    //   regX: -0.3,
    //   regY: -0.1,
    //   scaleX: 0.7989,
    //   scaleY: 0.7989,
    //   rotation: -28.602,
    //   x: -88.15,
    //   y: 20
    // }, 3).to({rotation: -126.6131, x: -125.7, y: 28.9}, 3).to({
    //   _off: true,
    //   regX: -165.8,
    //   regY: 48.6,
    //   scaleX: 1,
    //   scaleY: 1,
    //   rotation: 0,
    //   x: -165.8,
    //   y: 48.6
    // }, 2).wait(2));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-391.9, -173.4, 812.2, 1221.4);


  (lib.Lightcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_14 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(14).call(this.frame_14).wait(1));

    // Layer_1
    this.instance = new lib.Stageoncopy("synched", 0);
    this.instance.setTransform(6.05, 821);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(15));

    // Layer_2
    this.text = new cjs.Text("3", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-7.6, 238.65);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(15));

    // Layer_3
    this.instance_1 = new lib.C_ONcopy2("synched", 0);
    this.instance_1.setTransform(0, 350.85, 1.0374, 1.0374);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(15));

    // Layer_5
    this.instance_2 = new lib._3_Starscopy("synched", 0);
    this.instance_2.setTransform(2.95, 50.95, 0.2172, 0.2172);
    this.instance_2._off = true;

    // this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({_off: false}, 0).to({
    //   scaleX: 0.7133,
    //   scaleY: 1.4347,
    //   x: 3,
    //   y: 30.95
    // }, 2).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
    //   regX: 0,
    //   regY: 0,
    //   scaleX: 1,
    //   scaleY: 1.1333,
    //   x: 2.95,
    //   y: 30.95
    // }, 2).to({scaleY: 1}, 1).wait(7));

    // Layer_6
    this.instance_3 = new lib._3_stae_Lcopy("synched", 0);
    this.instance_3.setTransform(0, -0.05);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol511("synched", 0);
    this.instance_4.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    // this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 5).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_3}]}, 3).to({state: [{t: this.instance_4}]}, 2).wait(2));
    // this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5).to({_off: false}, 0).to({
    //   scaleX: 0.7987,
    //   scaleY: 0.7987,
    //   rotation: 36.6297,
    //   x: 92,
    //   y: 29.9
    // }, 3).to({regX: -0.1, regY: -0.1, rotation: 195.5884, x: 124.15, y: 30.9}, 3).to({
    //   _off: true,
    //   regX: 179.1,
    //   regY: 40.8,
    //   scaleX: 1,
    //   scaleY: 1,
    //   rotation: 360,
    //   x: 179.1,
    //   y: 40.8
    // }, 2).wait(2));

    // Layer_7
    this.instance_5 = new lib._3_Star_Rcopy("synched", 0);
    this.instance_5.setTransform(-0.05, 0);
    this.instance_5._off = true;

    this.instance_6 = new lib.Symbol512("synched", 0);
    this.instance_6.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    // this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_5}]}, 5).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_5}]}, 3).to({state: [{t: this.instance_6}]}, 2).wait(2));
    // this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(5).to({_off: false}, 0).to({
    //   regX: -0.3,
    //   regY: -0.1,
    //   scaleX: 0.7989,
    //   scaleY: 0.7989,
    //   rotation: -28.602,
    //   x: -88.15,
    //   y: 20
    // }, 3).to({rotation: -126.6131, x: -125.7, y: 28.9}, 3).to({
    //   _off: true,
    //   regX: -165.8,
    //   regY: 48.6,
    //   scaleX: 1,
    //   scaleY: 1,
    //   rotation: 0,
    //   x: -165.8,
    //   y: 48.6
    // }, 2).wait(2));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-391.9, -173.4, 812.2, 1221.4);


  (lib.Light_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_14 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(14).call(this.frame_14).wait(1));

    // Layer_1
    this.instance_7 = new lib.Stageon_1("synched", 0);
    this.instance_7.setTransform(6.05, 821);

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(15));

    // Layer_2
    this.text = new cjs.Text("2", "bold 200px 'Calibri'", "#FFFFFF");
    this.text.textAlign = "center";
    this.text.lineHeight = 225;
    this.text.lineWidth = 172;
    this.text.alpha = 0.98823529;
    this.text.parent = this;
    this.text.setTransform(-7.6, 238.65);

    this.timeline.addTween(cjs.Tween.get(this.text).wait(15));

    // Layer_3
    this.instance_8 = new lib.C_ONcopy("synched", 0);
    this.instance_8.setTransform(0, 350.85, 1.0374, 1.0374);

    this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(15));

    // Layer_5
    this.instance_2 = new lib._3_Starscopy2("synched", 0);
    this.instance_2.setTransform(2.95, 50.95, 0.2172, 0.2172);
    this.instance_2._off = true;

    // this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({_off: false}, 0).to({
    //   scaleX: 0.7133,
    //   scaleY: 1.4347,
    //   x: 3,
    //   y: 30.95
    // }, 2).to({regX: 0.1, regY: 0.2, scaleX: 0.7868, scaleY: 0.2561, x: 3.05, y: 31}, 2).to({
    //   regX: 0,
    //   regY: 0,
    //   scaleX: 1,
    //   scaleY: 1.1333,
    //   x: 2.95,
    //   y: 30.95
    // }, 2).to({scaleY: 1}, 1).wait(7));

    // Layer_6
    this.instance_3 = new lib._3_stae_Lcopy2("synched", 0);
    this.instance_3.setTransform(0, -0.05);
    this.instance_3._off = true;

    this.instance_4 = new lib.Symbol509("synched", 0);
    this.instance_4.setTransform(179.1, 40.8, 1, 1, 0, 0, 0, 179.1, 40.8);

    // this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_10}]}, 5).to({state: [{t: this.instance_10}]}, 3).to({state: [{t: this.instance_10}]}, 3).to({state: [{t: this.instance_11}]}, 2).wait(2));
    // this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(5).to({_off: false}, 0).to({
    //   scaleX: 0.7987,
    //   scaleY: 0.7987,
    //   rotation: 36.6297,
    //   x: 92,
    //   y: 29.9
    // }, 3).to({regX: -0.1, regY: -0.1, rotation: 195.5884, x: 124.15, y: 30.9}, 3).to({
    //   _off: true,
    //   regX: 179.1,
    //   regY: 40.8,
    //   scaleX: 1,
    //   scaleY: 1,
    //   rotation: 360,
    //   x: 179.1,
    //   y: 40.8
    // }, 2).wait(2));

    // Layer_7
    this.instance_5 = new lib._3_Star_Rcopy2("synched", 0);
    this.instance_5.setTransform(-0.05, 0);
    this.instance_5._off = true;

    this.instance_6 = new lib.Symbol510("synched", 0);
    this.instance_6.setTransform(-165.8, 48.6, 1, 1, 0, 0, 0, -165.8, 48.6);

    // this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_12}]}, 5).to({state: [{t: this.instance_12}]}, 3).to({state: [{t: this.instance_12}]}, 3).to({state: [{t: this.instance_13}]}, 2).wait(2));
    // this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(5).to({_off: false}, 0).to({
    //   regX: -0.3,
    //   regY: -0.1,
    //   scaleX: 0.7989,
    //   scaleY: 0.7989,
    //   rotation: -28.602,
    //   x: -88.15,
    //   y: 20
    // }, 3).to({rotation: -126.6131, x: -125.7, y: 28.9}, 3).to({
    //   _off: true,
    //   regX: -165.8,
    //   regY: 48.6,
    //   scaleX: 1,
    //   scaleY: 1,
    //   rotation: 0,
    //   x: -165.8,
    //   y: 48.6
    // }, 2).wait(2));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-391.9, -173.4, 812.2, 1221.4);


  (lib.Less_1_offcopy5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol28_1("synched", 0);
    this.instance.setTransform(-1328.35, 176.75, 6.4285, 5.3111);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1786.6, -51, 932.6999999999999, 498.6);


  (lib.Less_1_offcopy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol28("synched", 0);
    this.instance.setTransform(-1329.05, 160, 6.2587, 5.2647, 0, 0, 0, 0.9, 4.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1780.8, -89.5, 908, 494.3);


  (lib.Less_1_offcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol28("synched", 0);
    this.instance.setTransform(-1329.1, 219.55, 6.0055, 5.0517, 0, 0, 0, 0.9, 4.2);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1762.6, -18.3, 871.3, 474.3);


  (lib.Less_1_offcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol28("synched", 0);
    this.instance.setTransform(-1336.05, 183.15, 5.631, 4.7367, 0, 0, 0, 1.2, 4.2);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1744.2, -39.9, 817, 444.7);


  (lib.Less_1_offcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol28("synched", 0);
    this.instance.setTransform(-1326.15, 190.75, 5.2476, 4.4129, 0, 0, 0, 1.3, 4.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1707, -18.4, 761.3, 414.4);


  (lib.Less_1_off = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.Symbol28("synched", 0);
    this.instance.setTransform(-1318.15, 193.2, 5.6351, 4.4919, 0, 0, 0, 1.1, 4.2);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-1726, -18.3, 817.5, 421.8);


  (lib.All_Space = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_22
    this.instance = new lib.Symbol7copy();
    this.instance.setTransform(-0.05, 615.3, 1.9367, 1.9367);
    new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib.Symbol7copy(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_18
    this.instance_1 = new lib.Symbol15_1("synched", 0);
    this.instance_1.setTransform(1.75, -4.85, 1.2652, 1.3156, 0, 0, 0, -0.1, -0.2);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.All_Space, new cjs.Rectangle(-907.1, -738.3, 1812.7, 1471.5), null);


  (lib.Symbol44 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Lightcopy4();
    this.instance.setTransform(63.4, 5.95, 0.1341, 0.1341, 0, 0, 0, 486.9, 627.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-54.4, -62.3, 108.9, 124.69999999999999);


  (lib.Symbol43 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Lightcopy3();
    this.instance.setTransform(63.4, 5.95, 0.1341, 0.1341, 0, 0, 0, 486.9, 627.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-54.4, -62.3, 108.9, 124.69999999999999);


  (lib.Symbol42 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Lightcopy2();
    this.instance.setTransform(63.4, 5.95, 0.1341, 0.1341, 0, 0, 0, 486.9, 627.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-54.4, -62.3, 108.9, 124.69999999999999);


  (lib.Symbol41 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Lightcopy();
    this.instance.setTransform(63.4, 5.95, 0.1341, 0.1341, 0, 0, 0, 486.9, 627.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-54.4, -62.3, 108.9, 124.69999999999999);


  (lib.Symbol40 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Light_1();
    this.instance.setTransform(63.4, 5.95, 0.1341, 0.1341, 0, 0, 0, 486.9, 627.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-54.4, -62.3, 108.9, 124.69999999999999);


  (lib.Symbol39 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Light();
    this.instance.setTransform(63.4, 5.95, 0.1341, 0.1341, 0, 0, 0, 486.9, 627.1);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-54.4, -62.3, 108.9, 124.69999999999999);


  (lib.Symbol21_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance_2 = new lib.Symbol9_1();
    this.instance_2.setTransform(11.35, -235.25, 1, 1, 0, 0, 0, -417.4, -86.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol21_1, new cjs.Rectangle(-1094.8, -718.7, 2189.6, 1437.4), null);


  (lib.Symbol1copy6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy5("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy5("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.instance_2 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy5("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy6, new cjs.Rectangle(-171.4, -172.2, 355.20000000000005, 349.6), null);


  (lib.Symbol1copy4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy3("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy3("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.instance_2 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy3("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy4, new cjs.Rectangle(-162.3, -172.2, 331.8, 352.7), null);


  (lib.Symbol1copy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1copy("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_offcopy("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.instance_2 = new lib.C_1_ONcopy("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ONcopy("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy2, new cjs.Rectangle(-141.1, -172.2, 289.9, 344.6), null);


  (lib.Symbol1copy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.Locked_1("synched", 0);
    this.instance.setTransform(-12.35, 81.7, 0.2338, 0.2338);

    this.instance_1 = new lib.Less_1_off("synched", 0);
    this.instance_1.setTransform(509.1, 7.45, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.instance_2 = new lib.C_1_ON("synched", 0);
    this.instance_2.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.instance_3 = new lib.C_1_ON("synched", 0);
    this.instance_3.setTransform(-0.3, -83, 0.3601, 0.3601);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol1copy, new cjs.Rectangle(-148.4, -172.2, 311.4, 344.6), null);


  (lib.OFFcopy3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy5("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy6();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy5("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy6("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy6("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy6("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy6("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy6("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy6("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_4}]}, 1).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-92.4, -221.9, 355.20000000000005, 418.20000000000005);


  (lib.OFFcopy2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy4("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).wait(2));

    // Layer_3
    this.instance_1 = new lib.Less_1_offcopy4("synched", 0);
    this.instance_1.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(20));

    // C
    this.instance_2 = new lib.Tween1copy5("synched", 0);
    this.instance_2.setTransform(78.7, 76.2);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween2copy5("synched", 0);
    this.instance_3.setTransform(78.7, -24.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween3copy5("synched", 0);
    this.instance_4.setTransform(78.7, -127.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween4copy5("synched", 0);
    this.instance_5.setTransform(78.6, -71.2);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween5copy5("synched", 0);
    this.instance_6.setTransform(78.7, -96);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween6copy5("synched", 0);
    this.instance_7.setTransform(78.7, -120.65);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).wait(8));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -120.65
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 346.7, 418.20000000000005);


  (lib.OFFcopy = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy3("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy4();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy3("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy4("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy4("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy4("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy4("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy4("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy4("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_4}]}, 2).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({
      _off: true,
      y: -24.7
    }, 2).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(2).to({_off: false}, 2).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 418.20000000000005);


  (lib.OFF = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy2("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).wait(2));

    // Layer_3
    this.instance_1 = new lib.Less_1_offcopy2("synched", 0);
    this.instance_1.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(20));

    // C
    this.instance_2 = new lib.Tween1copy3("synched", 0);
    this.instance_2.setTransform(78.7, 76.2);
    this.instance_2._off = true;

    this.instance_3 = new lib.Tween2copy3("synched", 0);
    this.instance_3.setTransform(78.7, -24.7);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween3copy3("synched", 0);
    this.instance_4.setTransform(78.7, -127.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween4copy3("synched", 0);
    this.instance_5.setTransform(78.6, -71.2);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween5copy3("synched", 0);
    this.instance_6.setTransform(78.7, -96);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween6copy3("synched", 0);
    this.instance_7.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_2}]}, 2).to({state: [{t: this.instance_2}]}, 1).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_4}]}, 1).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_6}]}, 2).to({state: [{t: this.instance_7}]}, 2).wait(8));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(3).to({_off: false}, 1).to({y: -76.2}, 1).to({
      _off: true,
      y: -127.7
    }, 1).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(5).to({_off: false}, 1).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 418.20000000000005);


  (lib.B_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance = new lib.Locked_1copy("synched", 0);
    this.instance.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance._off = true;

    this.instance_1 = new lib.Symbol1copy2();
    this.instance_1.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance}]}, 11).to({state: [{t: this.instance}]}, 3).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance}]}, 1).to({state: [{t: this.instance_1}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_3
    this.instance_2 = new lib.Less_1_offcopy("synched", 0);
    this.instance_2.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 19).wait(1));

    // C
    this.instance_3 = new lib.Tween1copy2("synched", 0);
    this.instance_3.setTransform(78.7, 76.2);
    this.instance_3._off = true;

    this.instance_4 = new lib.Tween2copy2("synched", 0);
    this.instance_4.setTransform(78.7, -24.7);
    this.instance_4._off = true;

    this.instance_5 = new lib.Tween3copy2("synched", 0);
    this.instance_5.setTransform(78.7, -127.7);
    this.instance_5._off = true;

    this.instance_6 = new lib.Tween4copy2("synched", 0);
    this.instance_6.setTransform(78.6, -71.2);
    this.instance_6._off = true;

    this.instance_7 = new lib.Tween5copy2("synched", 0);
    this.instance_7.setTransform(78.7, -96);
    this.instance_7._off = true;

    this.instance_8 = new lib.Tween6copy2("synched", 0);
    this.instance_8.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_3}]}, 2).to({state: [{t: this.instance_3}]}, 1).to({state: [{t: this.instance_4}]}, 1).to({state: [{t: this.instance_5}]}, 2).to({state: [{t: this.instance_5}]}, 1).to({state: [{t: this.instance_6}]}, 1).to({state: [{t: this.instance_7}]}, 2).to({state: [{t: this.instance_8}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(2).to({_off: false}, 0).to({y: 25.75}, 1).to({
      _off: true,
      y: -24.7
    }, 1).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(3).to({_off: false}, 1).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off: false}, 2).to({
      x: 78.65,
      y: -99.45
    }, 1).to({_off: true, x: 78.6, y: -71.2}, 1).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(7).to({_off: false}, 1).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 418.20000000000005);


  (lib.B_1_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_19 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(1));

    // K
    this.instance_9 = new lib.Locked_1("synched", 0);
    this.instance_9.setTransform(66.65, 83.5, 0.0222, 0.0222);
    this.instance_9._off = true;

    this.instance_10 = new lib.Symbol1copy();
    this.instance_10.setTransform(268.7, 1.8, 1, 1, 0, 0, 0, 189.7, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_9}]}, 11).to({state: [{t: this.instance_9}]}, 3).to({state: [{t: this.instance_9}]}, 1).to({state: [{t: this.instance_9}]}, 1).to({state: [{t: this.instance_9}]}, 1).to({state: [{t: this.instance_9}]}, 1).to({state: [{t: this.instance_10}]}, 1).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(11).to({_off: false}, 0).to({
      regX: 0.2,
      regY: -0.1,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 18.2114,
      y: 83.55
    }, 3).to({regX: 0.4, regY: 0.1, rotation: -6.9886, x: 66.7}, 1).to({
      regX: 0.2,
      rotation: -23.972,
      y: 83.5
    }, 1).to({regX: 0.4, scaleX: 0.2758, scaleY: 0.2758, rotation: -5.511, x: 66.75, y: 83.55}, 1).to({
      regX: 0,
      regY: 0,
      scaleX: 0.2338,
      scaleY: 0.2338,
      rotation: 0,
      x: 66.65,
      y: 83.5
    }, 1).to({_off: true, regX: 189.7, scaleX: 1, scaleY: 1, x: 268.7, y: 1.8, mode: "independent"}, 1).wait(1));

    // Layer_3
    this.instance_11 = new lib.Less_1_off("synched", 0);
    this.instance_11.setTransform(588.1, 9.25, 0.3808, 0.3808, 0, 0, 0, 0.4, 1.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_11).to({_off: true}, 19).wait(1));

    // C
    this.instance_12 = new lib.Tween1copy("synched", 0);
    this.instance_12.setTransform(78.7, 76.2);
    this.instance_12._off = true;

    this.instance_13 = new lib.Tween2copy("synched", 0);
    this.instance_13.setTransform(78.7, -24.7);
    this.instance_13._off = true;

    this.instance_14 = new lib.Tween3copy("synched", 0);
    this.instance_14.setTransform(78.7, -127.7);
    this.instance_14._off = true;

    this.instance_15 = new lib.Tween4copy("synched", 0);
    this.instance_15.setTransform(78.6, -71.2);
    this.instance_15._off = true;

    this.instance_16 = new lib.Tween5copy("synched", 0);
    this.instance_16.setTransform(78.7, -96);
    this.instance_16._off = true;

    this.instance_17 = new lib.Tween6copy("synched", 0);
    this.instance_17.setTransform(78.7, -88.75);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.instance_12}]}, 2).to({state: [{t: this.instance_13}]}, 2).to({state: [{t: this.instance_14}]}, 2).to({state: [{t: this.instance_15}]}, 2).to({state: [{t: this.instance_16}]}, 2).to({state: [{t: this.instance_17}]}, 2).to({state: []}, 7).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(2).to({_off: false}, 0).to({
      _off: true,
      y: -24.7
    }, 2).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(2).to({_off: false}, 2).to({
      _off: true,
      y: -127.7
    }, 2).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(4).to({_off: false}, 2).to({
      _off: true,
      x: 78.6,
      y: -71.2
    }, 2).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(6).to({_off: false}, 2).to({
      _off: true,
      x: 78.7,
      y: -96
    }, 2).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(8).to({_off: false}, 2).to({
      _off: true,
      y: -88.75
    }, 2).wait(8));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-91.1, -221.9, 339.7, 418.20000000000005);


  (lib.Symbol31 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AlVG7QiOhOAAhvQgBhwCKhNQCKhMC/AAIASAAQDJACCMBPQCNBQABBrQABBsiOBOQiNBOjJgBQjIABiOhOgAgjgPIABgBIgBAAIALAAIABAAIACAAIABAAIAFAAQhjgDhGhIQhKhJAAhoQAAhpBKhJQApgpAzgTQAngOAsABQArgBAoAOQAzATAqApQBIBJAABpQAABohIBJQhIBIhiADIAAAAIAEABgADWknIAAABIABABIgBgDgADQlHIAAABIAAABIAEAIIgFgRIABAHgADJlfIACACIgCgDg");
    this.shape.setTransform(1.0501, -27.35);

    this.instance = new lib.OFFcopy3();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy3();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-52.5, -79.4, 108.9, 109.2);


  (lib.Symbol26 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.OFFcopy2();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off: false}, 0).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AABIPQjGgCiOhNQiOhOgChsQgDhtB0hCQB1hCCTgQQBtgPBuAPQCDAQBlA3QCOBNgDBvQgDBwiNBNQiJBKjAAAIgKAAgAgegZQhcgGhDhDQhJhKAAhnQAAhpBJhJQApgpAzgSQAngOAsAAQArAAAoAOQAzASApApQBIBJAABpQAABnhIBKQhDBDhcAGgADRkuIAAABIABAAIgBgDgADLlOIAAABIAAABIACAEIgDgNIABAHgADDlmIACABIgCgCg");
    this.shape.setTransform(1.3616, -32.7224);
    this.shape._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1).to({_off: false}, 0).to({_off: true}, 1).wait(2));

    // Layer_3
    this.instance_1 = new lib.OFFcopy2();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-51.9, -85.4, 106.1, 110.2);


  (lib.Symbol25 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AAGD9Qi6gCiGhKQiFhKgEhjQgFhiBwg+QBxg9CJgcQBngQBnAQQB7APBrBCQBsBCADBnQACBpiDBJQiABHi0AAIgKgBg");
    this.shape.setTransform(-0.0559, 0.5527);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(0,0,0,0.247)").s().p("AgfD5QhSgKg9g9QhJhKAAhnQAAhoBJhIQApgqAzgSQAngNArAAQAsAAAoANQAyASAqAqQBIBIAABoQAABnhIBKQg9A9hTAKgADfgZIAAABIABAAIgBgDgADag4IAAAAIAAABIACAEIgEgNIACAIgADShRIABACIgBgDg");
    this.shape_1.setTransform(-0.1, -51.95);

    this.instance = new lib.OFFcopy();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape_1}, {t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFFcopy();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-49.7, -76.9, 101.7, 107.60000000000001);


  (lib.Symbol24 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AABHoQitgDh8hFQh8hFgDhjQgEhjBsg7QBrg6BOgKQBPgJAlgBIgBAAIALAAIABAAIACAAIABAAIAEAAQhhgDhHhGQhIhJAAhnQAAhoBIhJQApgpAzgSQAmgOAsAAQAsAAAnAOQAzASApApQBIBJAABoQAABnhIBJQhHBGhjADQAsAAAsAHQBzAOBYAyQB9BFgDBkQgCBkh8BFQh4BDipAAIgIAAgADkkIIAAABIABABIgBgDgADfknIAAABIAAABIACAEIgDgMIABAGg");
    this.shape.setTransform(-0.7898, -28.3234);

    this.instance = new lib.OFF();
    this.instance.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.OFF();
    this.instance_1.setTransform(58.3, 0.1, 0.3067, 0.3067, 0, 0, 0, 269, 82.7);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-47.6, -77.1, 95.5, 101.89999999999999);


  (lib.Symbol22 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AAAJHQjSgDiWhOQiWhQAAhvQAAhvCWhPQBtg5CPgQIAggDIgIgCICXAAIgEABIAwAEQCPAPBtA6QCWBPgDByQgDByiVBPQiQBMjKAAIgMAAgAgtAMIANAAIABAAIACAAIABAAIAHAAQh1gDhUhUQhXhXAAh7QAAh8BXhWQAwgyA9gVQAugQA1AAQAzAAAvAQQA8AVAxAyQBWBWAAB8QAAB7hWBXQhSBShyAFIgpAAIgBAAgAD6k9IAAABIABABIgBgDgADzliIAAABIAAABIADAHIgEgRIABAIgADql/IACACIgCgDg");
    this.shape.setTransform(1.702, -33.6461);
    this.shape._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1).to({_off: false}, 0).to({_off: true}, 1).wait(2));

    // Layer_1
    this.instance = new lib.B_1_1();
    this.instance.setTransform(69.55, 0.3, 0.3655, 0.3655, 0, 0, 0, 269.3, 83);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off: false}, 0).wait(1));

    // Layer_2
    this.instance_1 = new lib.B_1_1();
    this.instance_1.setTransform(69.55, 0.3, 0.3655, 0.3655, 0, 0, 0, 269.3, 83);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-54.2, -91.9, 113.80000000000001, 121.30000000000001);


  (lib.N3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.B_1();
    this.instance.setTransform(128.7, 10.1, 1, 1, 0, 0, 0, 268.7, 82.4);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.N3, new cjs.Rectangle(-202.1, -70.6, 290, 157.8), null);


  (lib.Symbol29 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(0,0,0,0.239)").s().p("AAAJCQjGgDiPhNQiNhOAAhtQAAhuCNhOQBmg3CEgQQBtgRBtARQCDAQBlA3QCOBOgCBwQgDBwiNBOQiKBLjAAAIgJAAgAgZARQhzgEhUhSQhWhYAAh6QAAh8BWhXQAxgxA8gVQAugRA1AAQAzAAAvARQA9AVAxAxQBVBXAAB8QAAB6hVBYQhUBTh1ADIAAABIgQgBgAD4k3IAAABIABAAIgBgDgADxldIAAACIAAABIADAGIgEgRIABAIgADol6IACADIgCgDg");
    this.shape.setTransform(1.7522, -34.2225);

    this.instance = new lib.N3();
    this.instance.setTransform(69.35, 0.05, 0.3654, 0.3654, 0, 0, 0, 128.8, 10.1);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape}]}, 1).to({state: []}, 1).to({state: [{t: this.instance}]}, 1).wait(1));

    // Layer_2
    this.instance_1 = new lib.N3();
    this.instance_1.setTransform(69.35, 0.05, 0.3654, 0.3654, 0, 0, 0, 128.8, 10.1);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-51.5, -92, 105.9, 120.3);


  (lib.All = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // timeline functions:
    this.frame_281 = function () {
      this.stop();
    }

    // actions tween:
    this.timeline.addTween(cjs.Tween.get(this).wait(281).call(this.frame_281).wait(1));

    // Layer_23
    this.instance = new lib.Symbol44();
    this.instance.setTransform(496.65, -185.45, 0.974, 0.974);
    this.instance._off = true;
    this.instance.name = '6';
    this.instance.visible = false;
    new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib.Symbol44(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(251).to({_off: false}, 0).wait(31));

    // Layer_22
    this.instance_1 = new lib.Symbol43();
    this.instance_1.setTransform(103.45, -18.6, 0.9515, 0.9515, 0, 0, 0, 0.1, -0.1);
    this.instance_1._off = true;
    this.instance_1.name = '5';
    this.instance_1.visible = false;
    new cjs.ButtonHelper(this.instance_1, 0, 1, 2, false, new lib.Symbol43(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(237).to({_off: false}, 0).wait(45));

    // Layer_21
    this.instance_2 = new lib.Symbol42();
    this.instance_2.setTransform(-198.6, -67.2, 0.8822, 0.8822);
    this.instance_2._off = true;
    this.instance_2.name = '4';
    this.instance_2.visible = false;
    new cjs.ButtonHelper(this.instance_2, 0, 1, 2, false, new lib.Symbol42(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(220).to({_off: false}, 0).wait(62));

    // Layer_20
    this.instance_3 = new lib.Symbol41();
    this.instance_3.setTransform(-389.45, 32.5, 0.9695, 0.9695);
    this.instance_3._off = true;
    this.instance_3.name = '3';
    this.instance_3.visible = false;

    new cjs.ButtonHelper(this.instance_3, 0, 1, 2, false, new lib.Symbol41(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(201).to({_off: false}, 0).wait(81));

    // Layer_19
    this.instance_4 = new lib.Symbol40();
    this.instance_4.setTransform(-199.2, 238.55, 1.0397, 1.0397);
    this.instance_4._off = true;
    this.instance_4.name = '2';
    this.instance_4.visible = false;
    new cjs.ButtonHelper(this.instance_4, 0, 1, 2, false, new lib.Symbol40(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(178).to({_off: false}, 0).wait(104));

    // Layer_2
    this.instance_5 = new lib.Symbol39();
    this.instance_5.setTransform(249.5, 229.05, 1, 1, 0, 0, 0, 4.5, 0);
    this.instance_5._off = true;
    this.instance_5.name = '1';
    this.instance_5.visible = false;
    new cjs.ButtonHelper(this.instance_5, 0, 1, 2, false, new lib.Symbol39(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(160).to({_off: false}, 0).wait(122));

    // Layer_3
    this.instance_6 = new lib.Symbol26();
    this.instance_6.setTransform(495.55, -149.4);
    this.instance_6._off = true;
    new cjs.ButtonHelper(this.instance_6, 0, 1, 2, false, new lib.Symbol26(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(149).to({_off: false}, 0).wait(31));

    // Layer_4
    this.instance_7 = new lib.Symbol25();
    this.instance_7.setTransform(103.1, 10.2);
    this.instance_7._off = true;
    new cjs.ButtonHelper(this.instance_7, 0, 1, 2, false, new lib.Symbol25(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(118).to({_off: false}, 0).wait(45));

    // Layer_5
    this.instance_8 = new lib.Symbol24();
    this.instance_8.setTransform(-199, -37.5);
    this.instance_8._off = true;
    new cjs.ButtonHelper(this.instance_8, 0, 1, 2, false, new lib.Symbol24(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(89).to({_off: false}, 0).wait(62));

    // Layer_6
    this.instance_9 = new lib.Symbol29();
    this.instance_9.setTransform(-390.65, 64.75);
    this.instance_9._off = true;
    new cjs.ButtonHelper(this.instance_9, 0, 1, 2, false, new lib.Symbol29(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(60).to({_off: false}, 0).wait(81));

    // Layer_7
    this.instance_10 = new lib.Symbol22();
    this.instance_10.setTransform(-201.6, 274.15);
    this.instance_10._off = true;
    new cjs.ButtonHelper(this.instance_10, 0, 1, 2, false, new lib.Symbol22(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(32).to({_off: false}, 0).wait(104));

    // Layer_8
    this.instance_11 = new lib.Symbol31();
    this.instance_11.setTransform(243.2, 260.95);
    this.instance_11._off = true;
    new cjs.ButtonHelper(this.instance_11, 0, 1, 2, false, new lib.Symbol31(), 3);

    this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(6).to({_off: false}, 0).wait(122));

    // Layer_16
    this.instance_12 = new lib.Symbol27();
    this.instance_12.setTransform(0.5, -233.8, 0.789, 0.789, 0, 9.4729, -170.5271, -1.2, 14.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(282));

    // Layer_10
    this.instance_13 = new lib.Symbol23();
    this.instance_13.setTransform(-81.1, 124.2, 1, 1, 0, 0, 0, 0, -0.1);

    this.instance_14 = new lib.Symbol11("synched", 0);
    this.instance_14.setTransform(-244.95, -346.4, 0.8233, 0.8233);

    this.instance_15 = new lib.Symbol5("synched", 0);
    this.instance_15.setTransform(-415.45, 223.15, 0.6461, 0.6461, 0, 0, 0, 0.1, 0.4);

    this.instance_16 = new lib.Symbol9("synched", 0);
    this.instance_16.setTransform(-428.85, 287.5, 0.5585, 0.5585, 0, 0, 0, 0.2, 0.4);

    this.instance_17 = new lib.Symbol8("synched", 0);
    this.instance_17.setTransform(-392.05, -180.25, 0.6583, 0.6583, 0, 7.6927, -172.3073, -0.5, 0.6);

    this.instance_18 = new lib.Symbol6("synched", 0);
    this.instance_18.setTransform(-488.2, -197.85, 0.5125, 0.6089, 0, 7.6919, -172.3085, -0.4, 0.6);

    this.instance_19 = new lib.Symbol7("synched", 0);
    this.instance_19.setTransform(-427.6, -189.5, 0.6585, 0.6585, 0, 7.6928, -172.3072, -0.2, 0.2);

    this.instance_20 = new lib.Symbol3("synched", 0);
    this.instance_20.setTransform(77.65, 302.35, 0.8233, 0.8233);

    this.instance_21 = new lib.Symbol12_1("synched", 0);
    this.instance_21.setTransform(348.75, -226.65, 0.9838, 0.9838, 0, 0, 0, -0.2, 0.1);

    this.instance_22 = new lib.Symbol10("synched", 0);
    this.instance_22.setTransform(351.7, 17.35, 0.7527, 0.7527);

    this.instance_23 = new lib.Symbol2("synched", 0);
    this.instance_23.setTransform(452.75, 272.45, 1.1588, 1.1588, 0, 11.2117, -168.7883, -0.2, 0.4);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_23}, {t: this.instance_22}, {t: this.instance_21}, {t: this.instance_20}, {t: this.instance_19}, {t: this.instance_18}, {t: this.instance_17}, {t: this.instance_16}, {t: this.instance_15}, {t: this.instance_14}, {t: this.instance_13}]}).wait(282));

    // Layer_24
    this.instance_24 = new lib.Symbol45();
    this.instance_24.setTransform(39.25, 98.25, 1, 1, 0, 0, 0, 0, 28.8);
    this.instance_24._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(18).to({_off: false}, 0).wait(264));

    // Layer_12
    this.instance_25 = new lib.Symbol17("synched", 0);
    this.instance_25.setTransform(0.1, 103.7, 0.8628, 0.8372);

    this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(282));

    // Layer_13
    this.instance_26 = new lib.Symbol19("synched", 0);
    this.instance_26.setTransform(-251.65, -355, 1.0497, 1.0497);

    this.timeline.addTween(cjs.Tween.get(this.instance_26).wait(282));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-620.4, -435.9, 1246.8, 912.5999999999999);


// stage content:
  (lib.Sea = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.All_Space();
    this.instance.setTransform(675.3, 585.85, 0.7944, 0.7654, 0, 0, 0, -57.5, 20.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_1
    this.instance_1 = new lib.All();
    this.instance_1.setTransform(726.7, 571.7, 0.9892, 0.9892, 0, 0, 0, 8.4, 4.2);
    this.instance_1.name = 'stages';
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    // Layer_3
    this.instance_2 = new lib.Symbol21_1();
    this.instance_2.setTransform(710.65, 470.9, 0.573, 0.573, 0, 0, 0, 0, 0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

    // stageBackground
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("rgba(0,0,0,0)").ss(1, 1, 1, 3, true).p("EhyDhaEMDkHAAAMAAAC0JMjkHAAAg");
    this.shape.setTransform(720, 566.5);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("EhyDBaFMAAAi0JMDkHAAAMAAAC0Jg");
    this.shape_1.setTransform(720, 566.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new lib.AnMovieClip();
  p.nominalBounds = new cjs.Rectangle(709, 555.5, 742, 588.5);
// library properties:
  lib.properties = {
    id: '72A66984C87C0E4B9630B2DA350D76E1',
    width: 1440,
    height: 1133,
    fps: 24,
    color: "#FFFFFF",
    opacity: 1.00,
    manifest: [
      {src: "assets/images/seas/CachedBmp_314.png", id: "CachedBmp_314"},
      {src: "assets/images/shared/1.png", id: "CachedBmp_278"},
      {src: "assets/images/seas/SEA_atlas_1.png", id: "Sea_atlas_1"},
      {src: "assets/images/seas/SEA_atlas_1.png", id: "Sea_atlas_2"},
      {src: "assets/images/seas/SEA_atlas_3.png", id: "Sea_atlas_3"},
      {src: "assets/images/shared/2.png", id: "Sea_atlas_4"},
      {src: "assets/images/shared/2.png", id: "Sea_atlas_5"},
      {src: "assets/images/shared/2.png", id: "Sea_atlas_6"},
      {src: "assets/images/seas/SEA_atlas_7.png", id: "Sea_atlas_7"},
      {src: "assets/images/seas/SEA_atlas_7.png", id: "Sea_atlas_8"},
      {src: "assets/images/seas/SEA_atlas_9.png", id: "Sea_atlas_9"},
      {src: "assets/images/shared/3.png", id: "Sea_atlas_10"},
      {src: "assets/images/shared/3.png", id: "Sea_atlas_11"},
      {src: "assets/images/seas/SEA_atlas_12.png", id: "Sea_atlas_12"},
      {src: "assets/images/seas/SEA_atlas_13.png", id: "Sea_atlas_13"},
      {src: "assets/images/seas/SEA_atlas_14.png", id: "Sea_atlas_14"},
      {src: "assets/images/seas/SEA_atlas_15.png", id: "Sea_atlas_15"},
      {src: "assets/images/seas/SEA_atlas_16.png", id: "Sea_atlas_16"},
      {src: "assets/images/seas/SEA_atlas_17.png", id: "Sea_atlas_17"}
    ],
    preloads: []
  };


// bootstrap callback support:

  (lib.Stage = function (canvas) {
    createjs.Stage.call(this, canvas);
  }).prototype = p = new createjs.Stage();

  p.setAutoPlay = function (autoPlay) {
    this.tickEnabled = autoPlay;
  }
  p.play = function () {
    this.tickEnabled = true;
    this.getChildAt(0).gotoAndPlay(this.getTimelinePosition())
  }
  p.stop = function (ms) {
    if (ms) this.seek(ms);
    this.tickEnabled = false;
  }
  p.seek = function (ms) {
    this.tickEnabled = true;
    this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000);
  }
  p.getDuration = function () {
    return this.getChildAt(0).totalFrames / lib.properties.fps * 1000;
  }

  p.getTimelinePosition = function () {
    return this.getChildAt(0).currentFrame / lib.properties.fps * 1000;
  }

  an.bootcompsLoaded = an.bootcompsLoaded || [];
  if (!an.bootstrapListeners) {
    an.bootstrapListeners = [];
  }

  an.bootstrapCallback = function (fnCallback) {
    an.bootstrapListeners.push(fnCallback);
    if (an.bootcompsLoaded.length > 0) {
      for (var i = 0; i < an.bootcompsLoaded.length; ++i) {
        fnCallback(an.bootcompsLoaded[i]);
      }
    }
  };

  an.compositions = an.compositions || {};
  an.compositions['72A66984C87C0E4B9630B2DA350D76E1'] = {
    getStage: function () {
      return exportRoot.stage;
    },
    getLibrary: function () {
      return lib;
    },
    getSpriteSheet: function () {
      return ss;
    },
    getImages: function () {
      return img;
    }
  };

  an.compositionLoaded = function (id) {
    an.bootcompsLoaded.push(id);
    for (var j = 0; j < an.bootstrapListeners.length; j++) {
      an.bootstrapListeners[j](id);
    }
  }

  an.getComposition = function (id) {
    return an.compositions[id];
  }


  an.makeResponsive = function (isResp, respDim, isScale, scaleType, domContainers) {
    var lastW, lastH, lastS = 1;
    window.addEventListener('resize', resizeCanvas);
    resizeCanvas();

    function resizeCanvas() {
      var w = lib.properties.width, h = lib.properties.height;
      var iw = window.innerWidth, ih = window.innerHeight;
      var pRatio = window.devicePixelRatio || 1, xRatio = iw / w, yRatio = ih / h, sRatio = 1;
      if (isResp) {
        if ((respDim == 'width' && lastW == iw) || (respDim == 'height' && lastH == ih)) {
          sRatio = lastS;
        } else if (!isScale) {
          if (iw < w || ih < h)
            sRatio = Math.min(xRatio, yRatio);
        } else if (scaleType == 1) {
          sRatio = Math.min(xRatio, yRatio);
        } else if (scaleType == 2) {
          sRatio = Math.max(xRatio, yRatio);
        }
      }
      domContainers[0].width = w * pRatio * sRatio;
      domContainers[0].height = h * pRatio * sRatio;
      domContainers.forEach(function (container) {
        container.style.width = w * sRatio + 'px';
        container.style.height = h * sRatio + 'px';
      });
      stage.scaleX = pRatio * sRatio;
      stage.scaleY = pRatio * sRatio;
      lastW = iw;
      lastH = ih;
      lastS = sRatio;
      stage.tickOnUpdate = false;
      stage.update();
      stage.tickOnUpdate = true;
    }
  }
  an.handleSoundStreamOnTick = function (event) {
    if (!event.paused) {
      var stageChild = stage.getChildAt(0);
      if (!stageChild.paused) {
        stageChild.syncStreamSounds();
      }
    }
  }


})(createjs = createjs || {}, AdobeAn = AdobeAn || {});
var createjs, AdobeAn, instance;
var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation, instance;

function initSeas() {
  return new Promise((resolve, reject) => {
    canvas = document.getElementById("canvas");
    anim_container = document.getElementById("animation_container");
    dom_overlay_container = document.getElementById("dom_overlay_container");
    var comp = AdobeAn.getComposition("72A66984C87C0E4B9630B2DA350D76E1");
    var lib = comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function (evt) {
      handleFileLoadSeas(evt, comp)
    });
    loader.addEventListener("complete", function (evt) {
      handleCompleteSeas(evt, comp).then(data => {
        resolve(true)
      });
    });
    var lib = comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  });
}

function handleFileLoadSeas(evt, comp) {
  var images = comp.getImages();
  if (evt && (evt.item.type == "image")) {
    images[evt.item.id] = evt.result;
  }
}

function handleCompleteSeas(evt, comp) {
  return new Promise((resolve, reject) => {
    var lib = comp.getLibrary();
    var ss = comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for (i = 0; i < ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet({
        "images": [queue.getResult(ssMetadata[i].name)],
        "frames": ssMetadata[i].frames
      })
    }
    var preloaderDiv = document.getElementById("_preload_div_");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.Sea();
    stage = new lib.Stage(canvas);
    stage.enableMouseOver();
    createjs.Ticker.removeAllEventListeners(); // Note the function name
    //Registers the "tick" event listener.
    fnStartAnimation = function () {
      stage.addChild(exportRoot);
      createjs.Ticker.framerate = lib.properties.fps;
      createjs.Ticker.addEventListener("tick", stage);
    }
    //Code to support hidpi screens and responsive scaling.
    AdobeAn.makeResponsive(true, 'both', false, 1, [canvas, preloaderDiv, anim_container, dom_overlay_container]);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
    seaClickEvents();
    seaHoverEvents();
    resolve(true)
  });
}

function seaClickEvents() {
  //stage 1
  exportRoot.instance_1.instance_5.on("click", function (evt) {
    playAudioOnClick();
    instance = 1;
  });
  //stage 2
  exportRoot.instance_1.instance_4.on("click", function (evt) {
    playAudioOnClick();
    instance = 2;
  });
  //stage 3
  exportRoot.instance_1.instance_3.on("click", function (evt) {
    playAudioOnClick();
    instance = 3;
  });
  //stage 4
  exportRoot.instance_1.instance_2.on("click", function (evt) {
    playAudioOnClick();
    instance = 4;
  });
  //stage 5
  exportRoot.instance_1.instance_1.on("click", function (evt) {
    playAudioOnClick();
    instance = 5;
  });
  //stage 6
  exportRoot.instance_1.instance.on("click", function (evt) {
    playAudioOnClick();
    instance = 6;
  });
  //back
  exportRoot.instance.instance.on("click", function (evt) {
    playAudioOnClick();
    instance = 7;
  });
}

function seaHoverEvents() {
  //stage 1
  exportRoot.instance_1.instance_5.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 2
  exportRoot.instance_1.instance_4.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 3
  exportRoot.instance_1.instance_3.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 4
  exportRoot.instance_1.instance_2.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 5
  exportRoot.instance_1.instance_1.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //stage 6
  exportRoot.instance_1.instance.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //back
  exportRoot.instance.instance.on("mouseover", function (evt) {
    playAudioOnHover();
  });
}
