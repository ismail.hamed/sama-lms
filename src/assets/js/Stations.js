(function (cjs, an) {

  var p; // shortcut to reference prototypes
  var lib = {};
  var ss = {};
  var img = {};
  lib.ssMetadata = [
    {name: "Home_atlas_1", frames: [[0, 0, 1049, 1692]]},
    {name: "Home_atlas_2", frames: [[0, 0, 1049, 1692]]},
    {name: "Home_atlas_3", frames: [[0, 0, 1281, 1376]]},
    {name: "Home_atlas_4", frames: [[0, 0, 1049, 1644], [1051, 0, 944, 1796]]},
    {name: "Home_atlas_5", frames: [[0, 0, 1640, 712]]},
    {name: "Home_atlas_6", frames: [[0, 0, 577, 1828], [579, 1304, 1442, 670], [579, 0, 774, 1302]]},
    {name: "Home_atlas_7", frames: [[896, 0, 714, 1232], [0, 0, 894, 1014]]},
    {
      name: "Home_atlas_8",
      frames: [[1521, 542, 498, 119], [318, 560, 498, 119], [821, 535, 498, 140], [716, 830, 338, 108], [1879, 1025, 139, 76], [140, 1062, 139, 39], [1575, 330, 427, 210], [393, 1039, 139, 76], [534, 1039, 139, 76], [1056, 855, 335, 105], [675, 1044, 139, 76], [816, 1044, 139, 76], [1005, 962, 286, 82], [510, 681, 204, 204], [821, 484, 250, 26], [1392, 1072, 139, 26], [379, 887, 335, 105], [1031, 1046, 250, 26], [1031, 1074, 139, 26], [1293, 1033, 309, 37], [0, 906, 335, 105], [1107, 330, 466, 203], [1917, 663, 100, 191], [1321, 535, 198, 318], [281, 1062, 100, 51], [716, 681, 88, 135], [1653, 1033, 51, 174], [1393, 855, 115, 50], [379, 790, 113, 72], [382, 363, 437, 195], [1521, 663, 365, 152], [818, 677, 338, 151], [1706, 1025, 171, 69], [1283, 1072, 107, 44], [763, 0, 342, 361], [1604, 1033, 47, 215], [0, 409, 316, 248], [1852, 195, 158, 106], [1158, 677, 157, 105], [382, 0, 379, 359], [957, 1046, 72, 112], [1852, 0, 164, 193], [0, 790, 377, 114], [1706, 923, 334, 100], [1487, 0, 363, 328], [0, 1013, 391, 47], [0, 681, 508, 107], [393, 994, 314, 43], [1521, 817, 394, 104], [1107, 0, 378, 319], [0, 0, 380, 407], [1917, 856, 121, 45], [821, 363, 271, 119], [0, 1062, 138, 43], [1393, 923, 311, 108], [1158, 784, 126, 48], [716, 940, 287, 102]]
    },
    {
      name: "Home_atlas_9",
      frames: [[0, 1803, 681, 235], [1079, 1021, 427, 470], [0, 1395, 538, 320], [1014, 0, 607, 495], [0, 900, 1077, 277], [0, 1179, 809, 214], [0, 0, 606, 606], [1079, 497, 482, 522], [608, 0, 404, 898], [955, 1493, 427, 386], [1623, 0, 417, 579], [1563, 581, 448, 452], [540, 1395, 413, 406], [1384, 1493, 400, 408], [1508, 1035, 387, 456]]
    },
    {
      name: "Home_atlas_10",
      frames: [[540, 553, 507, 790], [0, 1486, 1318, 461], [1049, 838, 608, 608], [0, 0, 538, 1484], [1431, 0, 526, 836], [540, 0, 889, 551]]
    }
  ];


  (lib.AnMovieClip = function () {
    this.actionFrames = [];
    this.gotoAndPlay = function (positionOrLabel) {
      cjs.MovieClip.prototype.gotoAndPlay.call(this, positionOrLabel);
    }
    this.play = function () {
      cjs.MovieClip.prototype.play.call(this);
    }
    this.gotoAndStop = function (positionOrLabel) {
      cjs.MovieClip.prototype.gotoAndStop.call(this, positionOrLabel);
    }
    this.stop = function () {
      cjs.MovieClip.prototype.stop.call(this);
    }
  }).prototype = p = new cjs.MovieClip();
// symbols:


  (lib.CachedBmp_169 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_168 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_167 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_88 = function () {
    this.initialize(ss["Home_atlas_1"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_87 = function () {
    this.initialize(ss["Home_atlas_2"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_86 = function () {
    this.initialize(ss["Home_atlas_4"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_85 = function () {
    this.initialize(ss["Home_atlas_4"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_84 = function () {
    this.initialize(ss["Home_atlas_3"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_83 = function () {
    this.initialize(ss["Home_atlas_6"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_82 = function () {
    this.initialize(ss["Home_atlas_9"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_81 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_80 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(4);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_79 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(5);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_166 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(6);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_77 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(7);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_76 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(8);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_165 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(9);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_74 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(10);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_73 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(11);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_164 = function () {
    this.initialize(ss["Home_atlas_9"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_163 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(12);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_162 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(13);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_69 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(14);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_68 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(15);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_161 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(16);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_66 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(17);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_65 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(18);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_64 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(19);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_160 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(20);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_62 = function () {
    this.initialize(ss["Home_atlas_10"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_61 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(21);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_159 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(22);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_158 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(23);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_157 = function () {
    this.initialize(ss["Home_atlas_9"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_156 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(24);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_56 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(25);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_55 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(26);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_155 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(27);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_53 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(28);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_154 = function () {
    this.initialize(ss["Home_atlas_9"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_51 = function () {
    this.initialize(ss["Home_atlas_9"]);
    this.gotoAndStop(4);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_50 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(29);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_49 = function () {
    this.initialize(ss["Home_atlas_5"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_48 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(30);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_47 = function () {
    this.initialize(ss["Home_atlas_9"]);
    this.gotoAndStop(5);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_46 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(31);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_45 = function () {
    this.initialize(ss["Home_atlas_6"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_44 = function () {
    this.initialize(ss["Home_atlas_10"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_43 = function () {
    this.initialize(ss["Home_atlas_9"]);
    this.gotoAndStop(6);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_42 = function () {
    this.initialize(ss["Home_atlas_10"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_41 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(32);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_40 = function () {
    this.initialize(img.CachedBmp_40);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 3493, 2165);


  (lib.CachedBmp_39 = function () {
    this.initialize(img.CachedBmp_39);
  }).prototype = p = new cjs.Bitmap();
  p.nominalBounds = new cjs.Rectangle(0, 0, 2982, 2236);


  (lib.CachedBmp_38 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(33);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_153 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(34);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_36 = function () {
    this.initialize(ss["Home_atlas_7"]);
    this.gotoAndStop(0);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_35 = function () {
    this.initialize(ss["Home_atlas_6"]);
    this.gotoAndStop(2);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_34 = function () {
    this.initialize(ss["Home_atlas_7"]);
    this.gotoAndStop(1);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_33 = function () {
    this.initialize(ss["Home_atlas_10"]);
    this.gotoAndStop(3);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_32 = function () {
    this.initialize(ss["Home_atlas_9"]);
    this.gotoAndStop(7);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_152 = function () {
    this.initialize(ss["Home_atlas_10"]);
    this.gotoAndStop(4);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_151 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(35);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_150 = function () {
    this.initialize(ss["Home_atlas_9"]);
    this.gotoAndStop(8);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_149 = function () {
    this.initialize(ss["Home_atlas_9"]);
    this.gotoAndStop(9);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_148 = function () {
    this.initialize(ss["Home_atlas_9"]);
    this.gotoAndStop(10);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_147 = function () {
    this.initialize(ss["Home_atlas_10"]);
    this.gotoAndStop(5);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_25 = function () {
    this.initialize(ss["Home_atlas_9"]);
    this.gotoAndStop(11);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_146 = function () {
    this.initialize(ss["Home_atlas_9"]);
    this.gotoAndStop(12);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_23 = function () {
    this.initialize(ss["Home_atlas_9"]);
    this.gotoAndStop(13);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_145 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(36);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_21 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(37);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_20 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(38);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_19 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(39);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_144 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(40);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_17 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(41);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_16 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(42);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_15 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(43);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_14 = function () {
    this.initialize(ss["Home_atlas_9"]);
    this.gotoAndStop(14);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_13 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(44);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_12 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(45);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_11 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(46);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_10 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(47);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_9 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(48);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_8 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(49);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_7 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(50);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_6 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(51);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_5 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(52);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_4 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(53);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_3 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(54);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_2 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(55);
  }).prototype = p = new cjs.Sprite();


  (lib.CachedBmp_1 = function () {
    this.initialize(ss["Home_atlas_8"]);
    this.gotoAndStop(56);
  }).prototype = p = new cjs.Sprite();

// helper functions:

  function mc_symbol_clone() {
    var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop, this.reversed));
    clone.gotoAndStop(this.currentFrame);
    clone.paused = this.paused;
    clone.framerate = this.framerate;
    return clone;
  }

  function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
    var prototype = cjs.extend(symbol, cjs.MovieClip);
    prototype.clone = mc_symbol_clone;
    prototype.nominalBounds = nominalBounds;
    prototype.frameBounds = frameBounds;
    return prototype;
  }


  (lib.Tween15 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_169();
    this.instance.setTransform(-124.45, -29.85, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-124.4, -29.8, 249, 59.5);


  (lib.Tween14 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_168();
    this.instance.setTransform(-124.45, -29.85, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-124.4, -29.8, 249, 59.5);


  (lib.Tween13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_167();
    this.instance.setTransform(-124.45, -35.1, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-124.4, -35.1, 249, 70);


  (lib.Tween12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_88();
    this.instance.setTransform(-262.35, -422.9, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-262.3, -422.9, 524.5, 846);


  (lib.Tween11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_87();
    this.instance.setTransform(-262.35, -422.9, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-262.3, -422.9, 524.5, 846);


  (lib.Tween10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_86();
    this.instance.setTransform(-262.35, -410.9, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-262.3, -410.9, 524.5, 822);


  (lib.Tween9 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_85();
    this.instance.setTransform(-236.25, -448.95, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-236.2, -448.9, 472, 898);


  (lib.Tween8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_84();
    this.instance.setTransform(-320.1, -343.9, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-320.1, -343.9, 640.5, 688);


  (lib.Tween7 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_83();
    this.instance.setTransform(-144.3, -456.9, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-144.3, -456.9, 288.5, 914);


  (lib.Tween6 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_82();
    this.instance.setTransform(-170.35, -58.8, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-170.3, -58.8, 340.5, 117.5);


  (lib.Tween1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#000000").s().p("AA5AZQgDgBgHgGIgagUIgJAMIgDADIAAAAIABADQAAAGgGABQgEABgEgDIgDgEIgCgFIgBgFIgCgDIgEgJQgLAGgZAEQgJABgDgDQgCgDACgDQABgEAEgCQAFgEANgCQANgDAFgCQAJgFADABQAFACADALIADAKIABABIAAAAQAEgEAEgDQAGgGABgCIADgFQADgEAFAEQAFAEACAGQAWASAKALQAAADgDABQgDACgCAAIgBAAg");
    this.shape.setTransform(0.0179, -0.0291);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-6.5, -2.5, 13.1, 5);


  (lib.Symbolm = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.instance = new lib.CachedBmp_81();
    this.instance.setTransform(-84.45, -27, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-84.4, -27, 169, 54);


  (lib.Symbol27 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // OBJECTS
    this.instance = new lib.CachedBmp_80();
    this.instance.setTransform(-36.75, -25, 0.5, 0.5);

    this.instance_1 = new lib.CachedBmp_79();
    this.instance_1.setTransform(-36.75, 5, 0.5, 0.5);

    this.instance_2 = new lib.CachedBmp_166();
    this.instance_2.setTransform(-106.7, -52.45, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-106.7, -52.4, 213.5, 105);


  (lib.Symbol26 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // OBJECTS
    this.instance = new lib.CachedBmp_77();
    this.instance.setTransform(-36.75, 58, 0.5, 0.5);

    this.instance_1 = new lib.CachedBmp_76();
    this.instance_1.setTransform(-36.75, 27.45, 0.5, 0.5);

    this.instance_2 = new lib.CachedBmp_165();
    this.instance_2.setTransform(-84.6, 26.95, 0.5, 0.5);

    this.instance_3 = new lib.CachedBmp_74();
    this.instance_3.setTransform(-36.75, -3.15, 0.5, 0.5);

    this.instance_4 = new lib.CachedBmp_73();
    this.instance_4.setTransform(-36.75, -33.15, 0.5, 0.5);

    this.instance_5 = new lib.CachedBmp_164();
    this.instance_5.setTransform(-106.7, -117.45, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-106.7, -117.4, 213.5, 235);


  (lib.Symbol22 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.instance = new lib.CachedBmp_49();
    this.instance.setTransform(-439.75, -135.6, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-439.7, -135.6, 820, 356);


  (lib.Symbol20 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.instance = new lib.CachedBmp_45();
    this.instance.setTransform(-426.25, -248.35, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-426.2, -248.3, 721, 335);


  (lib.Symbol19 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.instance = new lib.CachedBmp_44();
    this.instance.setTransform(-338.6, -58.65, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-338.6, -58.6, 659, 230.5);


  (lib.Symbol16 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFF500").s().p("AgFApIgtASIANgtIgggjIAxgDIAYgoIASArIAwALIgnAcIAFAvg");
    this.shape.setTransform(-33.35, 27.8);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    // Layer_2
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.lf(["#2D3383", "#7519BB", "#0094FF", "rgba(254,223,0,0.706)", "rgba(255,255,255,0.188)", "rgba(3,143,248,0)"], [0, 0.094, 0.208, 0.443, 0.635, 1], -71, 0, 71, 0).s().p("AqcJnQgRgFgKgMQgTgWAHgiQAFgXAVgfQA5hYB2huQBohjC0ibQBUhIIpl/IgOAIQgPAJD6irQD7iqpdHtQk5EshYBpQhYBpiQCFIiiCVQgjAdgeAUQgiAVgYAEIgNACQgKAAgJgDg");
    this.shape_1.setTransform(33.802, -28.9813);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol16, new cjs.Rectangle(-40.3, -90.7, 145.1, 125), null);


  (lib.Symbol15MM = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#004C5E").s().p("AhYDqQgGgHgBgTQAAgSAKgJQAJgKASABQAlABACAiIAAABQAAARgLAKQgKAJgSAAQgUAAgKgKgAg/C0QgMABgGAJQgFAIAAAMQAAAQAGAFQALAIATgEQAVgEABgaQgBgOgKgGQgIgFgLAAIgFAAgAAoCkQgzgJgPg4QgpAhgsAGQg1AJgfglQgSAbgWAOQgbAQgggFQgggGgcgWQgWgSgQgZQgNASgUgBQgSAAgRgPQgSAegqgLQgmgKAEgeQgLAEgOABQgWAAgKgOQgLgRANgQQALgOAUgFIAVAAIAAAAIAAAAIADgPQAHgPAXgJQArgTAfAaQAKggAugNQAtgNAeASQATghAigWQAlgYAngCQBEgDAxBEQAVgXAggNQAigOAhAEQAzAGAVBEQAVgGAUAFQAMADAGAFIACACQgBgEADgLQAFgOAGgKQAKgSAQgMQAngaAnARQAjAQAEAoQASgKAZACQAIABAIACQApgrAtg0IAngzIAMgQQAHgLAGgDQAJgEAdANQAQAHANAOQAbAeAAArQAOgIATAFQAaAFAJAVQAMAZgGAcQgGAdgXAPQgMAIgGAFQgGAHgFAMQgFAPgLAQQgRAcgcANQgdAOgggGQgagFgYgSQgOAhgeAWQggAXgmAAQg6AAgRgsQgGAPgNANQgVATgYACQgcACgTgQQgOgMgMgWQgUAfgaAQQgZAPgbAAQgJAAgKgCgAgEBJQAEAEgDACIgQAOQAOA9A5ADQA7ADAphCIgKgXQgBgEAEgBQAEgBACAEQADAKAIAPIAAAAQAMAaAOALQASAPAZgEQAUgCASgRQARgRAEgVIAAgDIgBgEQAAgEAFABQAEABAAAEIAAAAIABABIgBAAIABABIAAAFIAAABIAAAAQAGAfAaAMQAYAMAigHQAdgGAYgVQAWgTAKgbQgFgEgDgFQgDgEAEgCQADgCADAEQAEAGAFADIAAAAIABABQAwAmA0gVQAzgVAQg7IACgHIAAgDQAEgcgKgPQgDgEADgCQAEgCACAEQALAPgBAaIAKgFQAWgOAHgQQAFgNAAgSQgBgTgGgLQgHgPgVgEQgVgFgNALIgDAMQAAAEgFgCQgEgDABgDIACgLIAAgBQADgcgKgYQgLgbgYgNQgbgPgRAPQgGAEgHALIgMARQg1BDg6A9QAJAEAIAGQASgfAggQQAkgSAjAOQAEABAAAFQAAAEgEgCQgRgGgRABQgQABgQAIQggAQgRAgIAAAAIAAABQgKAUAEAUQABAEgEAAQgEAAgBgEQgGgVAKgWQgQgMgXgCQgbgCgRAMIgCALQgBAEgEgDQgFgCABgEIACgKQAAgjgagRQgagPggAMQgZAJgPAXQgOAWgBAcQAAABAAAAQAAABgBAAQAAABgBAAQgBAAgBAAIAAAAQgDAAgCgDQgGgOgWgBQgQgBgQAFIgCAAIAAAAQgDABgFAFQgCACgCAGQgBAEgEgCQgEgCAAgEQABgHAGgFQAEgEAEgBQgVhGg3ABQgeABgbANQgaAMgUAVIAOAYQACAEgFABQgEABgCgEQgFgLgJgPIgBgBQgzhKhIALQgjAFgeAXQgeAVgRAeIAAAAIAAABQgFALgCALQAAAEgFgBQgFgCABgEQACgLAEgKQgZgQgqAMQgvAOgFAhQAAABAAAAQAAABgBAAQAAAAgBABQAAAAgBAAQAAAAgBAAQgBAAAAAAQgBAAAAAAQAAgBgBAAQgZgagjALIgIADQgfANAAAXIABALQAAAEgFgBQgDgBgBgFIgBgFQgNgBgMAEQgRAHgFANQgJAXAaACQAWACAOgKIAAAAIADgEQACgDAEADQADADgCADIgDAEQgKAiAlAHQAmAHANgcQADgKAAgOQAAgFAFACQAFABAAAEQAAANgEALQAPAPARgCQARgBALgRQgGgMgDgNQgBgFAFABQAEAAABAEQADAOAGALQAQAdAaAUQAbAVAgADQAyAFAjg5QgJgQACgSQAAgFAFACQAFABgBAEQgBARAJAOIAAABQAeAmA0gKQAqgIAogjIABAAIAQgOIADgBIAEABgArKAvQADgRAQgJQAOgIATAAQAPAAAJAJQAIAIABAOIAAACIAAABQgFAjgrADIgFAAQgmAAAGgmgAqiAXQgYAFgFARQgFAOAGAJQAHAJAPgBQAQgBALgIQANgJABgPQAAgVgXAAIgMABgAtTA4QABgSASgHQANgGAUAAQAoAAACAcIAAABIAAABQgCAYgqAFIgNABQglAAAAgdgAskAhQglADAAAWQAAARAVABQAOACASgGQAVgFABgPQgBgTgdAAIgIAAgALuhRQgBgSAKgNQAKgNARgBQAVgCATAJQAVAKAEARQAAABAAAAQABABAAAAQAAABAAAAQAAAAAAABQgFApgwAFIgJABQgnAAgBgogAMLhzQgTAHgBAbQAAALADAHQAHANAaAAQARgBAOgLQAOgMACgQQgCgRgXgHQgMgEgKAAQgJAAgHADgAiLitQgegFAGgkQACgNAIgGQAIgGAMACQAPADAKAJQALAJAAAOIAAAAIAAABQgBAPgNAIQgJAGgLAAIgIgBgAiZjWQgEAOADAIQACAJAPACQANABAIgGQAJgGABgMQgBgKgIgHQgHgGgLgBIgFAAQgMAAgDAOg");
    this.shape.setTransform(0.0313, 0.0015);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#84DDEC").s().p("AhQDlQgGgFAAgQQAAgMAFgIQAGgJAMgBQAPgBAJAGQAKAGABAOQgBAagVAEIgMABQgLAAgHgFgAA0CbQg5gEgOg8IAQgOQADgCgEgEQgEgDgDADIgQAOIgBAAQgoAigqAJQg0AKgegmIAAgBQgJgOABgRQABgEgFgBQgFgCAAAFQgCASAJAQQgjA5gygFQgggDgbgVQgagUgQgdQgGgLgDgOQgBgEgEAAQgFgBABAFQADANAGAMQgLAQgRACQgRACgPgPQAEgLAAgNQAAgEgFgBQgFgCAAAFQAAAOgDAKQgNAcgmgHQglgHALgiIACgEQACgDgDgDQgEgDgCADIgDAEIAAAAQgOAKgWgCQgagCAJgXQAFgNARgHQAMgEANABIABAFQABAFADABQAFABAAgEIgBgLQAAgYAfgMQAWADAPARQAMAPAAAQQATgTAkAAQARAAANAMQAHAGADAGQAJgIANgEQAMgEARAAQALAAAGADQAHgdAXgbQAlgqA6AAQBNAAAhA+QAJATADANQAHgRAQgMQAVgSAlAAQAnAAARAfQAGAKABAIQAWgPAUgGQAQgFAWAAQAPAAAPAFQADgXATgPQAUgRAiAAQAeAAAOAfIAIAQIACAAQALgFAWAAQAhAAATAYQALAPAEAKQAEhFAfgeQAbgYAWgOQARgBARAGQAEACAAgEQAAgFgEgBQgjgOgkASQggAQgSAfQgIgGgJgEQA6g9A1hDIAMgRQAHgLAGgEQARgPAbAPQAYANALAbQAKAYgDAcIAAABIgCALQgBADAEADQAFACAAgEIADgMQANgLAVAFQAVAEAHAPQAGALABATQAAASgFANQgHAQgWAOIgJAFQAAgagLgPQgCgEgEACQgDACADAEQAKAPgEAcIABADIgDAHQgQA7gzAVQg0AVgwgmIgBgBIAAAAQgFgDgEgGQgDgEgDACQgEACADAEQADAFAFAEQgKAagWAUQgYAVgdAGQgiAHgYgMQgagMgGgfIAAAAIAAgBIAAgFIgBgBIABAAIgBgBIAAAAQAAgEgEgBQgFgBAAAEIABAEIAAADQgEAVgRARQgSARgUACQgZAEgSgPQgOgLgMgaIAAAAQgIgPgDgKQgCgEgEABQgEABABAEIAKAXQgnA/g3AAIgGAAgAs0BKQgVgBAAgRQAAgWAlgDQAlgDABAWQgBAPgVAFQgOAFgMAAIgGgBgAq+BCQgGgJAFgOQAFgRAYgFQAjgGAAAaQgBAPgNAJQgLAIgQABIgBAAQgOAAgHgIgAL6hBQgDgHAAgLQABgbATgHQAQgGAWAHQAXAHACARQgCAQgOAMQgOALgRABQgaAAgHgNg");
    this.shape_1.setTransform(0.025, 0.2215);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#D0FAF6").s().p("AGfBrQgTgZghAAQgWAAgLAFIgCAAIgIgQQgOgfgeAAQgiAAgUARQgTAPgDAXQgPgFgPAAQgWAAgQAFQgUAHgWAPQgBgIgGgKQgRgggnAAQglAAgVASQgQAMgHASQgDgNgJgUQghg+hNAAQg6AAglAqQgXAbgHAeQgGgDgLAAQgRAAgMAEQgNAEgJAIQgDgGgHgGQgNgMgRAAQgkAAgTATQAAgQgMgQQgPgRgWgDIAIgDQAjgLAZAaQABAAAAABQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAABgBQAAAAAAgBQAFghAvgOQAqgMAZAQQgEAKgCALQgBAEAFACQAFABAAgEQACgLAFgLIAAgBIAAAAQARgeAegUQAegXAjgFQBIgLAzBJIABABQAJAPAFALQACAEAEgBQAFgBgCgEIgOgYQATgVAbgLQAbgNAegBQA3gBAVBFQgEABgEAEQgGAFgBAHQAAAEAEACQAEACABgEQACgGACgCQAFgFADgBIAAAAIACAAQAQgFAQABQAWABAGAOQACADADAAIAAAAQABAAABAAQABAAAAgBQABAAAAgBQAAAAAAgBQABgcAOgWQAPgWAZgJQAggMAaAPQAaAQAAAjIgCAKQgBAEAFACQAEADABgEIACgLQARgMAbACQAXACAQAMQgKAWAGAVQABAEAEAAQAEABgBgFQgEgUAKgUIAAgBIAAAAQARggAggQQAQgHAQgBQgWAOgbAXQgfAegEBGQgEgKgLgPgAiFhUQgPgCgCgJQgDgIAEgOQAEgRAQADQALABAHAGQAIAHABAKQgBAMgJAGQgHAGgKAAIgEgBg");
    this.shape_2.setTransform(-0.3625, -9.6975);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_2}, {t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-85.1, -24.4, 170.3, 48.9);


  (lib.Symbol15 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#FF7D00").ss(1, 1, 1).p("ACrAAQAABHgyAyQgyAyhHAAQhGAAgygyQgygyAAhHQAAhGAygyQAygyBGAAQBHAAAyAyQAyAyAABGg");
    this.shape.setTransform(8.3419, 8.34, 0.4886, 0.4886);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFF00").s().p("Ah4B5QgygyAAhHQAAhGAygyQAygyBGAAQBHAAAyAyQAyAyAABGQAABHgyAyQgyAyhHAAQhGAAgygyg");
    this.shape_1.setTransform(8.3419, 8.34, 0.4886, 0.4886);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol15, new cjs.Rectangle(-1, -1, 18.7, 18.7), null);


  (lib.Symbol14 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.instance = new lib.CachedBmp_42();
    this.instance.setTransform(-151.45, -151.95, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // Layer_1
    this.instance_1 = new lib.CachedBmp_43();
    this.instance_1.setTransform(-151.5, -151.55, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol14, new cjs.Rectangle(-151.5, -151.9, 304.1, 304), null);


  (lib.Symbol12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(27,78,168,0.329)").s().p("AtJFqQl3l2AAoTQAAhjAOhdQBPBiBeBdQKRKROiAAQFcAAE3hcQhcC4icCdQl3F2oTAAQoRAAl3l2g");
    this.shape.setTransform(0.025, 0);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(-121.6, -73.6, 243.3, 147.3), null);


  (lib.Symbol11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(255,255,255,0.047)").s().p("AmoEtQivh9AAiwQAAivCvh9QCxh8D3AAQD4AACwB8QCwB9AACvQAACwiwB9QiwB8j4AAQj3AAixh8g");

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(-60, -42.5, 120, 85), null);

  (lib.Symbol5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // OBJECT
    this.instance = new lib.CachedBmp_40();
    this.instance.setTransform(-252.8, -192.75, 0.1786, 0.1786);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-252.8, -192.7, 623.7, 386.6);


  p.nominalBounds = new cjs.Rectangle(-361, -15.9, 722.1, 31.8);


  (lib.Symbol3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#0066FF").s().p("EhSjA97MAAAh71MClHAAAMAAAB71g");

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-528.3, -396.2, 1056.6999999999998, 792.5);


  (lib.Symbol2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_39();
    this.instance.setTransform(-511.95, -383.95, 0.3434, 0.3434);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-511.9, -383.9, 1024.1, 767.9);


  (lib.LIPHT = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AtaNbQljlkAAn3QAAn2FjlkQFkljH2AAQDFAACuA2QEPBVDZDYQFkFkgBH2QABH3lkFkQgrArguAmQlIETm6gBQn2ABlklkgArprpQk2E1AAG0QAAG1E2E1QE1E1G0ABQELAADch0QCLhJB4h5QE1k1ABm1QgBm0k1k1QkCkDlcgqQhEgIhIgBQm0AAk1E2g");

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.LIPHT, new cjs.Rectangle(-121.4, -121.4, 242.9, 242.9), null);


  (lib.Path_4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FDBF0F").s().p("AzyKpIAAx7QAAhYA+g/QA/g/BZAAMAg5AAAQBZAAA/A/QA+A/AABYIAAR7g");
    this.shape.setTransform(126.725, 68.1);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_4, new cjs.Rectangle(0, 0, 253.5, 136.2), null);


  (lib.Path_0 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#9F69F4").s().p("Ak4E5QiCiCAAi3QAAi3CCiBQCCiCC2AAQC4AACBCCQCCCBAAC3QAAC3iCCCQiBCBi4AAQi2AAiCiBg");
    this.shape.setTransform(44.275, 44.25);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_0, new cjs.Rectangle(0, 0, 88.6, 88.5), null);


  (lib.Path = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FDBF0F").s().p("AjlBkQgqAAgdgdQgegeAAgpQAAgoAegeQAdgdAqAAIHMAAQApAAAdAdQAeAeAAAoQAAAqgeAdQgdAdgpAAg");
    this.shape.setTransform(33.075, 10.025);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path, new cjs.Rectangle(0, 0, 66.2, 20.1), null);


  (lib.Path_10 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#5B746D").s().p("AhSBEQAEgegHhTQgHhmAAgPQA/gBA9ARQAVAFAoAVIgLASQgQAagKAZQgIAYgGAhIgEA+QgBAsgJAPQgKASgdAJQgRAFgpAEQgDgxgKgug");
    this.shape.setTransform(9.325, 16.2939);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_10, new cjs.Rectangle(0, 0, 18.7, 32.6), null);


  (lib.Path_8 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#415B53").s().p("Ag1ABQgCgIgGgGIgGgNIgFgJQAbgIAjgGIBAgKIATAaQgMABgTAEIgfAFQgQADgKAGQgMAHgFAOQgFAQAOARIAJAHIgKADQgKADgGAHIgNg7g");
    this.shape.setTransform(7.3, 6);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_8, new cjs.Rectangle(0, 0, 14.6, 12), null);


  (lib.Path_5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#415B53").s().p("AgMBGQgPhGgGgiQgEgXAFgMQADgMAOgOQAcgdAKgSIABgCIAGACIAGAOIgEAGQgDAIgMAVQgKASgDANQgEAPABAYIABAlQgBBSAOAnIgJAKQgGgegMgtg");
    this.shape.setTransform(3.5809, 14.5);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_5, new cjs.Rectangle(0, 0, 7.2, 29), null);


  (lib.Path_4_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#415B53").s().p("AgiBPQA1hgAShTIgLBPQgGAwgGAeIgZAYQgPANgKAHg");
    this.shape_1.setTransform(3.7, 10.125);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_4_1, new cjs.Rectangle(0, 0, 7.4, 20.3), null);


  (lib.Path_3_0 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#415B53").s().p("AgxgFIAqgiQAXgTAVgJIAGgEIAFACIACAEQgMAAgKAJQgdAXgNAMQgWAVAAAcQAAAVgEAXQgBgqgIgjg");
    this.shape.setTransform(4.975, 7.175);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_3_0, new cjs.Rectangle(0, 0, 10, 14.4), null);


  (lib.Path_3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#5B746D").s().p("AgGBQIg8ipIADAAQAxgDA/AUIAEBBQAEAuAKAmIgLAMIgMABQgTAAgfgKg");
    this.shape.setTransform(6.725, 9.0133);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_3, new cjs.Rectangle(0, 0, 13.5, 18), null);


  (lib.Path_2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#5B746D").s().p("AAMBpQhDi9gFgQQAGgCA2gFQAdgEAfADIABAEQgjgCgZAQQgbASAFAiQAFAgAPApQAVA3ARAVg");
    this.shape.setTransform(6.1, 11.0433);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_2, new cjs.Rectangle(0, 0, 12.2, 22.1), null);


  (lib.Path_44 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["#9579A7", "#FFFFFF"], [0, 1], -8.1, -3.7, 11.1, 3.4).s().p("AgUAtQgCAPgFAVIguhRIAciEIANABIABABQAeAHAaAPQgZAYgFArQgNBUBcBaQg1gOgphKg");
    this.shape.setTransform(7.3625, 13.3125);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_44, new cjs.Rectangle(0, 0, 14.8, 26.7), null);


  (lib.Path_26 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["#FFFFFF", "#9579A7"], [0, 1], -30.4, -11.3, 30.7, 11.3).s().p("ABzG9QAEimgWhSIg1gHIg9A4QhCA6gSASQgWAWgyAYQgzAZggAEIgDAAIg3grIALhEIAFgBIAkAhQAtAhApgDQAtgDBFhRQBAhMgCgbQgChGgmhDQgaguglgmIAjgxQAJgKAVgdQg5AbgwgPQgwgPgignQgUgXgMgbQAOAeAcAXQAvAoBHAEQAhACAfgQQA5geAuhWQAjhBAFgoIABgNQgBgRgKgMIgJgHIAIgFQAMgKARgEIAJADIgBALQgFBFgSBBQgPA3gaAqQg9BjgSARIAbBWQAhBdAcAZQAeAcBMgcQBBgXAfggIAggfIgHAjIACAlQgtAwgmAtQg/BJgNAfQgPAigJA7QgFAegCAWg");
    this.shape.setTransform(31.475, 44.7);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_26, new cjs.Rectangle(0, 0, 63, 89.4), null);


  (lib.Path_25 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#53345F").s().p("AgCBGQg2gkgig9QgQgfgGgYIC5AiIAoAOQgHAogSAhQgYAsgfAAQgRAAgSgNg");
    this.shape.setTransform(11.275, 8.2791);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_25, new cjs.Rectangle(0, 0, 22.6, 16.6), null);


  (lib.Path_22 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["#FFFFFF", "#9579A7"], [0, 1], -32, -7.6, 31, 15.7).s().p("AgdDnQhmgTg/gwQhAgwgbh1QgahtAXg0QAQgjAQgbQAXgWAbAGQAuAKAZB8QAMA+ADA7IAkgCIAFAWQAIAcAOAZQAtBSBaAfQBQAcBIgQQAggIAmgVQhIAthYAKQgdADgdAAQg4AAg3gLg");
    this.shape.setTransform(29.8672, 24.2082);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_22, new cjs.Rectangle(-0.1, 0, 60, 48.4), null);


  (lib.Path_16 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["#FFFFFF", "#9579A7"], [0, 1], -7.6, -1.8, 6.7, 3.5).s().p("AhPAVIAKgQIAAgpQAWALAdAKQA7ASAngDQgIAHgOAIQgcARgfAFQg1gQgZAAg");
    this.shape.setTransform(8, 3.7);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_16, new cjs.Rectangle(0, 0, 16, 7.4), null);


  (lib.Path_15 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["#FFFFFF", "#9579A7"], [0, 1], -7, -1.2, 6.5, 3.8).s().p("AhBAiIALhaQADAKAGAMQAMAZAUASQASASAfAFQASADAMgBIhUAXQgcgHgTgQg");
    this.shape.setTransform(6.625, 5.65);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_15, new cjs.Rectangle(0, 0, 13.3, 11.3), null);


  (lib.Path_12 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.rf(["#003CEE", "#0036D6", "#00289E", "#001C6D", "#001246", "#000A27", "#000412", "#000104", "#000000"], [0, 0.055, 0.2, 0.341, 0.482, 0.62, 0.753, 0.882, 1], 0, 0, 0, 0, 0, 8.2).s().p("Ag5AbQgYgKAAgQQgBgOAYgLQAYgLAigBQAhgBAZALQAYAKAAAQQABAOgYALQgYALgiABIgFAAQgfAAgWgKg");
    this.shape.setTransform(8.225, 3.7032);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_12, new cjs.Rectangle(0, 0, 16.5, 7.4), null);


  (lib.Path_11 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.lf(["#FFFFFF", "#9579A7"], [0, 1], -4.9, -3, 4.4, 0.5).s().p("AgEAPQgggTgNgZQAHAJAMAIQAWAQATABQATABAMgPQAGgHACgIQABAHgDAJQgFATgRATQgOgGgQgJg");
    this.shape.setTransform(5.0143, 2.95);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_11, new cjs.Rectangle(0, 0, 10.1, 5.9), null);


  (lib.Path_10_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.lf(["#9579A7", "#FFFFFF"], [0, 1], -2.9, -0.6, 2.7, 1.5).s().p("AgSATQgIgTADgVIAGgOQAAAJAFAKQAKAWAbANQgTALgPAGQgFgHgEgKg");
    this.shape_1.setTransform(2.4702, 3.55);

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Path_10_1, new cjs.Rectangle(0, 0, 5, 7.1), null);


  (lib.ClipGroup = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2 (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    mask.graphics.p("AglC2QhlgZgzhQQgQgZgJgcIgGgWIgggFQACgegCgbQBEhABhgmQDBhMCSCAQABBFgRBHQgQA+gVAlIgCADQgdAdgvAQQgoAOgsAAQgkAAgmgJg");
    mask.setTransform(25.2507, 38.3094);

    // Layer_3
    this.instance = new lib.CachedBmp_153();
    this.instance.setTransform(0, 0, 0.1653, 0.1653);

    var maskedShapeInstanceList = [this.instance];

    for (var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.ClipGroup, new cjs.Rectangle(0, 19.2, 50.5, 38.2), null);


  (lib.Usre_Cr = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#457CF5").s().p("A6TaUQq5q6AAvaQAAvZK5q6QK6q5PZAAQPaAAK6K5QK5K6AAPZQAAPaq5K6Qq6K5vaAAQvZAAq6q5g");

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-238.1, -238.1, 476.2, 476.2);


  (lib.Tween5 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_36();
    this.instance.setTransform(-178.5, -308, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-178.5, -308, 357, 616);


  (lib.Tween4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_35();
    this.instance.setTransform(-193.5, -325.55, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-193.5, -325.5, 387, 651);


  (lib.Tween3 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_34();
    this.instance.setTransform(-223.55, -253.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-223.5, -253.5, 447, 507);


  (lib.Tween2 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_33();
    this.instance.setTransform(-134.5, -371, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-134.5, -371, 269, 742);


  (lib.Tween1_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_32();
    this.instance.setTransform(-120.5, -130.5, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-120.5, -130.5, 241, 261);


  (lib.جبال1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_25();
    this.instance.setTransform(-38.75, -38.7, 0.174, 0.174);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.جبال1, new cjs.Rectangle(-38.7, -38.7, 77.9, 78.7), null);


  (lib.قةوي = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#000000").s().p("AAVAkQgEgEACgKIAEgWQACgMgCgHIgRAXQgDAEgCAAQgEABgEgIIgDgFIgBACIgFARQgCAGgEABQgEADgEgEQgEgDACgFIACgFIAAgGQABgDADgFIAEgIIACgJQACgGAEgBQAEAAACADIAAABQACAAADACIADAHQAAABAAABQAAABAAAAQABABAAAAQAAAAABAAIADgBQALgIACgGIADgGQADgEADACQACAAABAGQAEARgBALQgEAOACAHIACAIQABAEgCADQgBACgEABIgBABQgBAAAAgBQgBAAAAAAQgBAAgBAAQAAgBgBAAg");
    this.shape.setTransform(0.0088, 0.0095);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-3.3, -3.7, 6.699999999999999, 7.5);


  (lib.fhvr = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.fhvr, new cjs.Rectangle(0, 0, 0, 0), null);


  (lib.صاروخ = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // OBJECT
    this.instance = new lib.CachedBmp_20();
    this.instance.setTransform(-40.4, -27, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-40.4, -27, 78.5, 52.5);


  (lib.سهول1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_19();
    this.instance.setTransform(-63.05, -59.8, 0.3333, 0.3333);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.سهول1, new cjs.Rectangle(-63, -59.8, 126.3, 119.69999999999999), null);


  (lib.سفينة = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // flash0_ai
    this.instance = new lib.CachedBmp_144();
    this.instance.setTransform(-3.85, -21.65, 0.2941, 0.2941);

    this.instance_1 = new lib.CachedBmp_17();
    this.instance_1.setTransform(-24.05, -28.35, 0.2941, 0.2941);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-24, -28.3, 48.2, 56.7);


  (lib._02 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // OBJECTS_copy
    this.instance = new lib.CachedBmp_16();
    this.instance.setTransform(-93.5, -34.35, 0.4957, 0.4957);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    // OBJECTS
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(6,98,180,0.329)").s().p("AouESQgGgFgCgGIgBgOQgBgSAKgLQAJgKAQgCIACAAIACAAQAlABACAdQACARgLANQgLANgSABIgEABQgQAAgKgJgAkFC8QgkgNgSgfQgLALgGAEQgMAIgPgCQgbgCgXgXQgTAWgRAIQgXAKgbgMQgTgIgOgSQgKgMgBgLQgGAFgMABQgRABgMgOIgIgQIgFgKIgLAAQgWACgOgOQgOgPABgXQACgXAQgMQARgLAfABIAPACQAJABAEgDQADgCAEgJIAIgLIABgBQAQgNAVADQANACAJAHQgDgQABgKQAAgWALgQQAMgSAXgNQAUgLAYgEQAogHAbARQAYAQALAhQAYgNAfgCQAggDAbALQAOgyAxgeQA1ghA0AVQAoASANA4QACAKADACQACACAKACQAVAEAGACQARAGARAQQAFhVBDgOQAqgJAeARQAcAQAMAkQANgGAQAAQATAAAPAKQAMAIAKAPQAHAKACAJQAPgMAKgFQAVgKAQAFQATAGAKAWQAGAOACATIAugPQAcgHAaAFIAAAAIACAAQARAGANALQASAQgHAOQgFAKgXAGQgXAFgYgDQgGAagXAMQgcAQgbgXQgLApgjgEQgTgCgOgLQgGgFgEgFQgBAWgCAIQgHAYgaAIQg3AQgngtQgOAbgVAQQgaATgggCQg2gBgZgzQgLAGgHACQgLACgOAAQgXgBgNgLIgFAKQgPAWgbAJQgXAIgYgGQgVgEgOgNIgFANQgQAggkAIQgRAEgRAAQgXAAgWgIgArtBWQgUgBgJgLQgLgLAFgUQAHgjAygCQAxgCACAnIAAABIAAABQgDAXgYALQgRAIgUAAIgJgBgAuVBOQgTgEACgXQABgKAHgGQAIgGAKABQAKAAAGAHQAFAGABALIAAABQgBAMgJAHQgHAFgIAAIgGgBgAKnACQAAgSATgKQAOgIATgBIACAAIACAAQATABAMAHQAQAJgDASQgEAWgpADIgQABQgmAAgBgYgANuAQQg1gBAIggQAGgYA2AAQAnABADAWIAAACIAAABIAAACQgGAdgyAAIgBAAgAgOj2QABgRALgKQAJgJARAAQARABAKAHQAMAJABAQIAAABQgBAQgNAJQgLAHgRAAQgnAAADgeg");
    this.shape.setTransform(-4.8095, 3.4535);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib._02, new cjs.Rectangle(-98.3, -34.3, 191.7, 66), null);


  (lib._01 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // OBJECTS
    this.instance = new lib.CachedBmp_15();
    this.instance.setTransform(-83.45, -25.05, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib._01, new cjs.Rectangle(-83.4, -25, 167, 50), null);

  (lib.جبال1_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance_1 = new lib.CachedBmp_14();
    this.instance_1.setTransform(-60.45, -71.25, 0.3125, 0.3125);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.جبال1_1, new cjs.Rectangle(-60.4, -71.2, 120.9, 142.5), null);


  (lib.تنناخت = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#000000").s().p("AgVAkIgDgCIgIgBQgFAAgDgCQgFgDAAgJQAAgEACgDIAFgDIgEgKQgFgMgCgKQgEgJAEgDQADgCAEAAQADABACADIAGANIAHAQIAFAPIABABIACABIACAAQAbgLAagGQAIgDADAEQAEAEgFAGQgBACgJAEIguAPIAAABQgCAFgEACIgEACIgEgCg");
    this.shape.setTransform(0, 0.0075);

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-5.2, -3.7, 10.4, 7.5);


  (lib.بحلر = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_13();
    this.instance.setTransform(-60.45, -54.35, 0.3335, 0.3335);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(70));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-60.4, -54.3, 121.1, 109.4);


  (lib.الصحراء = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_8();
    this.instance.setTransform(-62.95, -53.1, 0.3333, 0.3333);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.الصحراء, new cjs.Rectangle(-62.9, -53.1, 126, 106.4), null);

  (lib.Symbol23 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.instance = new lib.CachedBmp_61();
    this.instance.setTransform(-25.25, 51.55, 0.5, 0.5);

    this.instance_1 = new lib.CachedBmp_159();
    this.instance_1.setTransform(-153.8, 75.4, 0.5, 0.5);

    this.instance_2 = new lib.Path_3_0();
    this.instance_2.setTransform(-144.8, 42.1, 2.9973, 2.9973, 0, 0, 0, 5.1, 7.4);
    this.instance_2.alpha = 0.75;

    this.instance_3 = new lib.Path_4_1();
    this.instance_3.setTransform(-145.85, 58.7, 2.9973, 2.9973, 0, 0, 0, 4, 10.2);
    this.instance_3.alpha = 0.75;

    this.instance_4 = new lib.CachedBmp_158();
    this.instance_4.setTransform(-202.7, 18.15, 0.5, 0.5);

    this.instance_5 = new lib.CachedBmp_157();
    this.instance_5.setTransform(-301.3, 7.95, 0.5, 0.5);

    this.instance_6 = new lib.CachedBmp_156();
    this.instance_6.setTransform(-293.35, 37.1, 0.5, 0.5);

    this.instance_7 = new lib.Path_2();
    this.instance_7.setTransform(-275.7, 91.85, 2.9973, 2.9973, 0, 0, 0, 6.2, 11.2);
    this.instance_7.alpha = 0.75;

    this.instance_8 = new lib.Path_3();
    this.instance_8.setTransform(-270.05, 99.1, 2.9973, 2.9973, 0, 0, 0, 6.8, 9.1);
    this.instance_8.alpha = 0.5898;

    this.instance_9 = new lib.CachedBmp_56();
    this.instance_9.setTransform(-293.35, 59.75, 0.5, 0.5);

    this.instance_10 = new lib.Path_5();
    this.instance_10.setTransform(-253.15, 80.9, 2.9973, 2.9973, 0, 0, 0, 3.9, 14.7);
    this.instance_10.alpha = 0.75;

    this.instance_11 = new lib.CachedBmp_55();
    this.instance_11.setTransform(-263.5, 38.4, 0.5, 0.5);

    this.instance_12 = new lib.CachedBmp_155();
    this.instance_12.setTransform(-253.75, -14.25, 0.5, 0.5);

    this.instance_13 = new lib.Path_8();
    this.instance_13.setTransform(-216.8, 13.55, 2.9977, 2.9977, 0, 0, 0, 7.3, 6.1);
    this.instance_13.alpha = 0.75;

    this.instance_14 = new lib.CachedBmp_53();
    this.instance_14.setTransform(-238.2, -4, 0.5, 0.5);

    this.instance_15 = new lib.Path_10();
    this.instance_15.setTransform(-230.8, 82.9, 2.9977, 2.9977, 0, 0, 0, 9.4, 16.4);
    this.instance_15.alpha = 0.5898;

    this.instance_16 = new lib.CachedBmp_154();
    this.instance_16.setTransform(-364.05, -14.25, 0.5, 0.5);

    this.instance_17 = new lib.CachedBmp_51();
    this.instance_17.setTransform(-242.55, 38.1, 0.5, 0.5);

    this.instance_18 = new lib.CachedBmp_50();
    this.instance_18.setTransform(-17.05, 62.05, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_18}, {t: this.instance_17}, {t: this.instance_16}, {t: this.instance_15}, {t: this.instance_14}, {t: this.instance_13}, {t: this.instance_12}, {t: this.instance_11}, {t: this.instance_10}, {t: this.instance_9}, {t: this.instance_8}, {t: this.instance_7}, {t: this.instance_6}, {t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-364, -14.2, 660, 247.5);


  (lib.Symbol13 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // OBJECTS
    this.instance = new lib.Symbol14();

    this.timeline.addTween(cjs.Tween.get(this.instance).to({
      scaleX: 0.9998,
      scaleY: 0.9998,
      rotation: 3.2932
    }, 44).to({scaleX: 1, scaleY: 1, rotation: 44.9994}, 535).to({
      scaleX: 0.9994,
      scaleY: 0.9994,
      rotation: 79.7314
    }, 367).to({scaleX: 1, scaleY: 1, rotation: 89.9991}, 109).to({
      scaleX: 0.9999,
      scaleY: 0.9999,
      rotation: 134.9982
    }, 455).to({rotation: 179.9983}, 499).to({rotation: 224.9982}, 490).to({rotation: 269.9983}, 500).to({rotation: 314.9975}, 500).to({
      scaleX: 0.9998,
      scaleY: 0.9998,
      rotation: 359.9091
    }, 499).to({_off: true}, 1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-215.4, -215.4, 430.8, 430.8);


  (lib.Group_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.CachedBmp_38();
    this.instance.setTransform(5.75, -2.55, 0.5, 0.5);

    this.instance_1 = new lib.Path();
    this.instance_1.setTransform(33, 10, 1, 1, 0, 0, 0, 33, 10);
    this.instance_1.compositeOperation = "multiply";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Group_1, new cjs.Rectangle(0, -2.5, 66.2, 22.6), null);


  (lib.Group_4 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.ClipGroup();
    this.instance.setTransform(28.3, 29.8, 1, 1, 0, 0, 0, 28.3, 29.8);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

    this._renderFirstFrame();

  }).prototype = getMCSymbolPrototype(lib.Group_4, new cjs.Rectangle(0, 0, 56.6, 59.7), null);


  (lib.Symbol5_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance_1 = new lib.جبال1();

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-38.7, -38.7, 77.9, 78.7);


  (lib.Symbol4_1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.instance = new lib.CachedBmp_152();
    this.instance.setTransform(-80.85, -247.5, 0.5, 0.5);

    this.instance_1 = new lib.Path_10_1();
    this.instance_1.setTransform(192.8, -28.65, 3.024, 3.024, 0, 0, 0, 2.6, 3.7);
    this.instance_1.compositeOperation = "multiply";

    this.instance_2 = new lib.Path_11();
    this.instance_2.setTransform(-99, -23.7, 3.024, 3.024, 0, 0, 0, 5, 3);
    this.instance_2.compositeOperation = "multiply";

    this.instance_3 = new lib.Path_12();
    this.instance_3.setTransform(107.1, -185.05, 3.024, 3.024, 0, 0, 0, 8.2, 3.7);
    this.instance_3.compositeOperation = "screen";

    this.instance_4 = new lib.Group_4();
    this.instance_4.setTransform(113.45, -155.3, 3.024, 3.024, 0, 0, 0, 28.4, 29.8);
    this.instance_4.compositeOperation = "screen";

    this.instance_5 = new lib.CachedBmp_151();
    this.instance_5.setTransform(39.9, -215.6, 0.5, 0.5);

    this.instance_6 = new lib.Path_15();
    this.instance_6.setTransform(-90.05, 176.05, 3.024, 3.024, 0, 0, 0, 6.8, 5.7);
    this.instance_6.compositeOperation = "multiply";

    this.instance_7 = new lib.Path_16();
    this.instance_7.setTransform(41.9, 213.1, 3.024, 3.024, 0, 0, 0, 8.1, 3.8);
    this.instance_7.compositeOperation = "multiply";

    this.instance_8 = new lib.CachedBmp_150();
    this.instance_8.setTransform(-135.6, -198.35, 0.5, 0.5);

    this.instance_9 = new lib.Path_22();
    this.instance_9.setTransform(57.5, -131.2, 3.024, 3.024, 0, 0, 0, 29.9, 24.2);
    this.instance_9.compositeOperation = "multiply";

    this.instance_10 = new lib.CachedBmp_149();
    this.instance_10.setTransform(-32.7, -250.6, 0.5, 0.5);

    this.instance_11 = new lib.Path_25();
    this.instance_11.setTransform(60.35, -41.7, 3.024, 3.024, 0, 0, 0, 11.3, 8.2);
    this.instance_11.alpha = 0.5;
    this.instance_11.compositeOperation = "multiply";

    this.instance_12 = new lib.Path_26();
    this.instance_12.setTransform(-12.8, 68.65, 3.024, 3.024, 0, 0, 0, 31.4, 44.7);
    this.instance_12.compositeOperation = "multiply";

    this.instance_13 = new lib.CachedBmp_148();
    this.instance_13.setTransform(-118.5, -67.3, 0.5, 0.5);

    this.instance_14 = new lib.Path_44();
    this.instance_14.setTransform(92.45, -16.45, 3.024, 3.024, 0, 0, 0, 7.4, 13.3);
    this.instance_14.compositeOperation = "multiply";

    this.instance_15 = new lib.CachedBmp_147();
    this.instance_15.setTransform(-222.1, -59.3, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_15}, {t: this.instance_14}, {t: this.instance_13}, {t: this.instance_12}, {t: this.instance_11}, {t: this.instance_10}, {t: this.instance_9}, {t: this.instance_8}, {t: this.instance_7}, {t: this.instance_6}, {t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-222.1, -250.6, 444.5, 501.29999999999995);


  (lib.كوكب = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // flash0_ai
    this.instance = new lib.fhvr();
    this.instance.setTransform(1.45, -5.25, 1, 1, 0, 0, 0, 1.4, -5.3);

    this.shape = new cjs.Shape();
    this.shape.graphics.rf(["#AFD9F7", "#2D3383"], [0.396, 1], 20.8, 33.6, 0, 20.8, 33.6, 0).s().p("AiaXBQiRgPkJg6IgKgEIhHghQgEgPAMgNQALgNAQgDQAOgCASAEIAfAJQA0AQBBAAQAxAABGgKQDRgeDphUQC5hDDthzQCJhEERiMQD0h8CshGQBPgfA0gNIAVgFIgBADIgRAkQhrDXiyC5IgaAaQgjAjgkAhQh9AdiaBGQi4BZhdAqQiiBKh8AdQh7AciQAAQhUAAhcgKgA1GKpQiLj+gjkpIAAAAIgCgQIgCgPIgCgdIAGgDQD0h0EMg1QEMg2EPAOQArACC2AQQCKAMBYgBQEJgDCjhzQA9grBKhTIB8iKQA1g2B0hlQBlhcAvhOQAgg1AdALQAQAGAFAVQADARgDAWQguEMinDiQimDjjxB7QiPBKi4AsQiQAjjIAWQjEAWhMASQiXAjhcBNQgoAhhIBVQhEBRgtAiQgkAcgwAYQgmATg2AUQhTAehUAVQgVgigTgjgA1SsyQARghATggIABAAQBfijCMiQIgBAAIAGgHIASgSIAIgIIAygvIACgCQHzhRH1h9QAYgHAVAEQAZAEAEAUQAFAWggAaQjqDHlNCMQkNBwlxBYQhtAZgoAMIgwAPg");
    this.shape.setTransform(20.7625, 33.5548);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.rf(["#AFD4EE", "#2D3383"], [0.396, 1], 22.8, -32.4, 0, 22.8, -32.4, 0).s().p("ARKDWQABgJAGgHQAFgGAIgGIAPgLQAjgYAgg3QAzhWAZheQAIgaAFgMQAGgNAhgvIgCgJQgDgNACgGQADgIAKgCQAJgDAIAEIAIgLIgFAMQALAGAKATQANAcAHAlQAFAbACApQAEA3gJAaQgKAegmArIgiAlQgUAUgTAMQgSAMgZAKQgNAGghALIhYAdQgGgHABgKgA1hAsQgcgEgBgPQgBgHAHgKQAQgSAogOQA2gSBSgNQBcgMAugJQBRgOBngfQAngMCNgvQAXgIAJAIQAIAGgBAKQgBALgHAHQgGAGgKAFIgSAHQgaAJgnAXQgvAbgQAHIghAQQgNAGgYAPQgYAPgOAHQgPAGgjAMQiBAmiBAAQg+AAg+gJg");
    this.shape_1.setTransform(22.8348, -32.425);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.rf(["#8CC6EE", "#2D3383"], [0.396, 1], 29.6, 8.5, 0, 29.6, 8.5, 0).s().p("Ax8UhQAXgKAZgIQBpghCKgKQBRgGCoABQE3AACtgQQEJgXDMhHQBTgdBkgwQA5gaB5g+QBFgjAigWQA3giAjgoQiBCZi/BtQiuBjjVA4Qi3AwjhAWQilAPj8AEIiFADQhMADg6AEQheAJhIAXQgqglgogngAzbS7IgcghIgDgDIgIgKIgHgJQAjgXAWgNQA1geAvgQQA4gUBNgIICJgLQBSgHA2gMQBJgQA1ghQAWgNAHgBQAJgCAIADQAIADAEAHQAGALgJAMQgGALgNAIQgqAcg6AVQgkAMhHAUIiJAmQhCARgiAQQgaANgwAdQgWALg1AVIgxAVIglgqgAkwODQiogRjWhJQgPgEgEgHQgHgNARgKQAPgKATACQD7AKEJg9QD0g6D2h2QDdhpDkieQDDiGDai3QCQh5BDgsQA4gkA2gXIAIAgQAKAmAHAmIAOBSIAAAEIhWBgQh/CMhyBhIFYhlIAAAIIgBBCIgBAYIgCAfIgDAmIgBAMIAAAAQh7BPhYAvQkSCTj2AgIi7AQQhxAKhIAUQhKAVh8A+QiIBEg9AVQh6AqiQAAQg4AAg8gHgA4ZCAQgRgMgEgSQgGgbAVgrQAMgaADgGQAHgUAAgPIgBgZQgBgOAFgIQAJgRAYgBQANgBAZAHQAmAKAPAOIAKAKQAGAHAFACQAHADALACQAgACAegIQAdgJAHANQAEAIgGAMQgMAYghAPQgTAKgqAQQghAPg4AtIgyAnQgGACgGAAQgKAAgLgGgAlsg2QgbgEg6gOQgMgDgDgHQgCgGAHgFQAGgEAJgCQArgHBPgfIDRhOQBlglAvgZQBxg6C5isQC7isBug7QAfgQAOANQANANgLAgQgmBlgqBJQg0BahDA7QggAcg8AnQj3CgkYBMQhSAXg3ABIgIAAQgjAAgrgIgAuginQjBgDiqg8QgWgIgOgOQgQgRAIgQQAJgTAmgBQDDgHC9gxQC+gyCshYQB2g+BKhCQAugpA0hCQAegmA6hOQBJhfCdixQBGhPAlglQA/g/A5gnQAlgZAsgXQAsALDYBOIBSAdIgagDQBzA6BqBNIA3AqQhcAWhdAxQhdAyhxBZQiABrhBAzQiGBsiNBeQlgDpk7BRQixAtisAAIgTAAg");
    this.shape_2.setTransform(29.6071, 8.45);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.rf(["#5EA3D2", "#2D3383"], [0.396, 1], 0, 0, 0, 0, 0, 162.6).s().p("AqCXEIALAEQEIA6CRAQQD5AbDDguQB7gcCjhKQBdgrC3hZQCbhFB9geQnAGTpoAAQlbAAkniBgAwVTCQBIgXBegIQA7gFBLgCICFgDQD8gEClgQQDigVC3gwQDUg4CvhkQC+htCBiZQgjAog3AjQghAWhFAjQh6A9g5AbQhjAvhTAdQjMBHkKAYQitAPk2AAQipAAhRAFQiJAKhqAiQgZAIgXAJIgHgHIgzg1IAygUQA0gVAWgMQAxgcAZgNQAigQBCgRICJgmQBHgUAkgNQA6gUAqgcQANgIAHgLQAIgNgGgLQgDgHgJgCQgIgDgIACQgIABgWAMQg1AhhJARQg2AMhSAHIiJAKQhNAJg4ATQguAQg2AeQgWAOgjAWIAIAKQhBhQg0hVQBUgVBSgeQA3gTAmgTQAwgYAkgcQAsgiBFhRQBHhWAoghQBdhMCWgjQBNgSDEgWQDIgXCQgjQC3gsCQhJQDxh9CmjhQCmjiAvkMQADgWgDgRQgFgVgQgGQgdgLggA1QgvBNhmBdQhzBkg1A3Ih8CKQhKBTg9AqQijB0kKACQhXACiKgMQi2gQgrgCQkPgOkMA1QkMA1j1B0IgGAEQgGhKAAhNQAAmUCvlNIAvgPQApgMBtgaQFxhXENhwQFMiNDsjGQAegagEgWQgDgUgZgFQgVgDgZAGQn1B+nzBRQHEmdJuAAQFjAAEsCGQjYhOgsgKQgsAXgkAZQg6Ang+A+QgmAlhGBPQidCxhJBfQg6BOgeAmQg0BCguApQhJBDh3A9QisBZi9AyQi9AwjEAIQgmAAgJATQgIARARARQANAOAXAHQCpA8DBADQC2ACC6gvQE7hQFgjqQCNhdCGhsQBBg0CAhrQBxhZBdgxQBdgyBcgWQBMA9BIBIQE0E0BqGHQg2AYg4AjQhDAsiPB5QjbC5jCCFQjlCejdBpQj2B2j0A5QkIA+j8gLQgSgBgQAJQgRALAHANQAEAHAPAEQDWBICpASQDUAWCpg5QA9gWCIhEQB9g+BJgUQBIgVBxgJIC7gQQD2ghESiSQBYgvB7hPQgaEKhrDpIgWAFQg0ANhOAfQisBGj0B7QkRCNiKBDQjsB0i6BDQjoBUjRAdQhGAKgxAAQhBAAg0gPIgggJQgRgFgOACQgRAEgKAMQgMAOAEAOQixhYiciJgA3QkTQgYACgJAQQgEAIAAAPIACAYQAAAQgIATQgCAHgNAaQgVAsAHAbQADASARALQARALARgHIAxgnQA4gsAhgPQAqgQATgKQAigQAMgZQAGgLgFgJQgGgMgeAIQgeAJgggDQgKgBgIgDQgFgCgFgHIgKgKQgQgOgmgKQgXgHgNAAIgCAAgAKLuMQhuA6i7CtQi5CshxA5QgwAZhkAmIjQBOQhQAegrAIQgJACgGAEQgGAFACAGQACAHAMADQA6AOAbAEQAwAJAmgBQA4gBBSgXQEXhND3ifQA8goAhgcQBCg7A1hZQAphJAmhmQALgfgNgNQgGgGgKAAQgMAAgRAJgAULsfQgKACgDAIQgBAGACANIADAJQghAvgHANQgFALgHAaQgaBggzBWQggA3giAYIgQAKQgIAHgFAFQgGAIgBAJQgBAKAGAGIBYgcQAigMAMgFQAagKARgMQATgNAUgTIAiglQAngrAKgeQAIgcgEg2QgCgpgFgbQgHglgNgcQgKgTgKgGIAEgMIgHAKQgFgCgFAAIgIACgArxr4QiMAwgoAMQhnAehRAPQgtAJhdAMQhSANg2ASQgoANgQAUQgHAKABAGQABAQAcAEQDAAbC+g5QAjgLAQgIQANgGAYgPQAZgPAMgHIAigPQAPgIAwgaQAngXAZgKIASgHQAKgEAHgGQAGgIACgKQAAgKgHgHQgFgDgIAAQgIAAgMADgAXeiPIBXhhIgBgEQAQBzABB6IlYBjQByhgB/iLg");
    this.shape_3.setTransform(27.5, 25.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_3}, {t: this.shape_2}, {t: this.shape_1}, {t: this.shape}, {t: this.instance}]}).wait(1));

    // Layer_2
    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#0099FF").s().p("Au/PAQmOmOAAoyQAAoxGOmOQGOmOIxAAQIyAAGOGOQGOGOAAIxQAAIymOGOQmOGOoyAAQoxAAmOmOg");
    this.shape_4.setTransform(27.4668, 26.3296, 1.182, 1.1875);

    this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-133, -135, 321, 322.6);

  (lib.فضاء = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_7
    this.instance = new lib.CachedBmp_145();
    this.instance.setTransform(-306.7, -36.45, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(239));

    // Layer_6
    this.instance_1 = new lib.كوكب("synched", 0);
    this.instance_1.setTransform(-116.3, 183.05, 0.5523, 0.5523, 0, 0, 0, 27.5, 25.6);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(239));

    // flash0_ai_copy
    this.instance_2 = new lib.Symbol16();
    this.instance_2.setTransform(128.75, -210.55, 0.5918, 0.5918, 0, 0, 0, 0.1, -0.1);
    this.instance_2.alpha = 0;
    this.instance_2._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5).to({_off: false}, 0).to({
      x: 111.75,
      y: -183.9,
      alpha: 1
    }, 25).to({x: 77.85, y: -130.65}, 50).to({_off: true}, 1).wait(67).to({
      _off: false,
      x: 128.75,
      y: -210.55,
      alpha: 0
    }, 0).to({x: 111.75, y: -183.9, alpha: 1}, 25).to({x: 77.85, y: -130.65}, 50).to({_off: true}, 1).wait(15));

    // flash0_ai_copy
    this.instance_3 = new lib.Symbol16();
    this.instance_3.setTransform(63.05, -194.55, 1, 1, 0, 0, 0, 0.1, -0.1);
    this.instance_3.alpha = 0;
    this.instance_3._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(36).to({_off: false}, 0).to({
      x: 0.45,
      y: -144.55,
      alpha: 0.9805
    }, 20).to({x: -101.95, y: -52.45, alpha: 0}, 39).to({_off: true}, 1).wait(83).to({
      _off: false,
      x: 63.05,
      y: -194.55
    }, 0).to({x: 0.45, y: -144.55, alpha: 0.9805}, 20).to({x: -101.95, y: -52.45, alpha: 0}, 39).wait(1));

    // flash0_ai
    this.instance_4 = new lib.Symbol16();
    this.instance_4.setTransform(67.55, -251.55, 1, 1, 0, 0, 0, 0.1, -0.1);
    this.instance_4.alpha = 0;

    this.timeline.addTween(cjs.Tween.get(this.instance_4).to({x: 25.4, y: -220.05, alpha: 0.75}, 15).to({
      x: -56.15,
      y: -159.1,
      alpha: 0.2695
    }, 29).to({x: -101.15, y: -125.5, alpha: 0}, 16).to({_off: true}, 1).wait(82).to({
      _off: false,
      x: 67.55,
      y: -251.55
    }, 0).to({x: 25.4, y: -220.05, alpha: 0.75}, 15).to({
      x: -101.15,
      y: -125.5,
      alpha: 0
    }, 45).to({_off: true}, 1).wait(35));

    // Layer_4
    this.instance_5 = new lib.CachedBmp_146();
    this.instance_5.setTransform(-100.6, -105.5, 0.5, 0.5);

    this.instance_6 = new lib.CachedBmp_23();
    this.instance_6.setTransform(-108.7, -99.8, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_6}, {t: this.instance_5}]}).wait(239));

    // flash0_ai
    this.shape = new cjs.Shape();
    this.shape.graphics.rf(["#FF0077", "#FF0079", "#FF0380", "#E8118B", "#C1269A", "#8C40AD", "#8244B0"], [0, 0.141, 0.306, 0.486, 0.675, 0.867, 0.898], 25.1, -97.8, 0, 25.1, -97.8, 225.9).s().p("AiiRwQjnghjHh4Qi/hziHi0QiGi0g5jYQg7jgAhjmQAhjnB4jHQB0i/CziHQC0iGDYg5QDgg7DmAhQDnAhDGB4QDAB0CHCzQCGC0A5DYQA7DgghDmQghDnh4DGQh0DAizCHQi0CGjYA5QiPAmiSAAQhSAAhTgMg");
    this.shape.setTransform(-0.025, 0.025);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.rf(["#FF0077", "rgba(255,0,120,0.984)", "rgba(255,0,123,0.929)", "rgba(255,3,129,0.835)", "rgba(239,13,136,0.702)", "rgba(213,27,146,0.525)", "rgba(181,44,159,0.31)", "rgba(141,63,173,0.063)", "rgba(130,68,176,0)"], [0, 0.427, 0.584, 0.694, 0.784, 0.859, 0.925, 0.988, 1], -1, -0.7, 0, -1, -0.7, 129.6).s().p("Ai1T1QkCgljeiGQjWiBiWjJQiXjJg/jxQhCj6AlkBQAlkCCGjeQCBjWDJiWQDJiWDxhAQD6hCEBAlQECAlDeCGQDWCBCWDJQCWDJBADxQBCD6glECQglECiGDdQiBDWjJCWQjJCWjxBAQigAqijAAQhbAAhdgNgAkjxVQjYA5i0CGQizCHh0C/Qh4DHghDnQghDmA7DgQA5DYCGC0QCHC0C/BzQDHB4DnAhQDmAhDgg7QDYg5C0iGQCziHB0jAQB4jGAhjnQAhjmg7jgQg5jYiGi0QiHizjAh0QjGh4jnghQhTgMhSAAQiSAAiPAmg");
    this.shape_1.setTransform(-0.0218, 0.0282);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(239));

    // flash0_ai
    this.instance_7 = new lib.صاروخ("synched", 0);
    this.instance_7.setTransform(-132.05, 88.8, 0.4401, 0.4401, -153.2022, 0, 0, -2.1, -4.2);
    this.instance_7.alpha = 0;

    this.timeline.addTween(cjs.Tween.get(this.instance_7).to({
      regX: -2,
      regY: -4.4,
      scaleX: 0.5344,
      scaleY: 0.5344,
      rotation: -161.9805,
      x: -106.55,
      y: 117.35,
      alpha: 0.5
    }, 8).to({
      regX: -2.1,
      regY: -4.2,
      scaleX: 0.6295,
      scaleY: 0.6295,
      rotation: -180.1153,
      x: -76.1,
      y: 139.25,
      alpha: 1
    }, 8).to({rotation: -180.0292, x: -76.15, y: 139.3, alpha: 0.4609}, 7).to({
      rotation: -180.1153,
      x: -76.1,
      y: 139.25,
      alpha: 1
    }, 8).to({regX: -2, scaleX: 0.6291, scaleY: 0.6291, rotation: -190.0316, x: -52.75, y: 149.3}, 10).to({
      regX: -1.9,
      scaleX: 0.6295,
      scaleY: 0.6295,
      rotation: -202.089,
      x: -24.55,
      y: 154.1
    }, 12).to({
      regX: -1.8,
      regY: -4.3,
      scaleX: 0.6285,
      scaleY: 0.6285,
      rotation: -211.8576,
      x: 0.5,
      y: 154.85
    }, 9).to({
      regX: -1.9,
      scaleX: 0.6294,
      scaleY: 0.6294,
      rotation: -224.0561,
      x: 31.15,
      y: 149.35
    }, 11).to({scaleX: 0.6285, scaleY: 0.6285, rotation: -237.9443, x: 64.05, y: 134.05}, 8).to({
      scaleX: 0.6294,
      scaleY: 0.6294,
      rotation: -261.9593,
      x: 104.15,
      y: 98.6
    }, 10).to({regY: -4.4, scaleX: 0.629, scaleY: 0.629, rotation: -279.0663, x: 125.55, y: 55.35}, 8).to({
      regY: -4.2,
      scaleX: 0.6294,
      scaleY: 0.6294,
      rotation: -299.1364,
      x: 132.35,
      y: 12
    }, 8).to({regX: -1.8, scaleX: 0.5566, scaleY: 0.5566, rotation: -317.1008, x: 127.2, y: -29.65}, 8).to({
      regX: -1.9,
      regY: -4.4,
      scaleX: 0.4766,
      scaleY: 0.4766,
      rotation: -337.1051,
      x: 104.3,
      y: -74.2
    }, 9).to({
      regX: -1.5,
      regY: -4.7,
      scaleX: 0.4113,
      scaleY: 0.4113,
      rotation: -349.2871,
      x: 82,
      y: -98.15
    }, 14).to({
      regX: -1.2,
      regY: -4.8,
      scaleX: 0.3324,
      scaleY: 0.3324,
      rotation: -359.7659,
      x: 55.85,
      y: -115.75,
      alpha: 0
    }, 14).to({_off: true}, 1).wait(86));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-306.7, -342.2, 497.5, 614.7);


  (lib.طير = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.قةوي("synched", 0);
    this.instance.setTransform(23.1, -67.35);
    this.instance.alpha = 0;

    this.shape = new cjs.Shape();
    this.shape.graphics.f("#000000").s().p("AAyAfQgDgBgDgEQgEgFgEgKQgFgKgDgDQgGABgEABIgFAEQgIAFgHgJIAAAAQgIgDgDABQgCAAgEADQgIAGgNAHQgFADgGAAQgHAAgCgFQgCgGAIgFIAMgEIAOgKIAEgCIAAgBIALgMIAFgDQAEgBADADQACAEgCADIgBACQACgDAEAAIAHADIAHACQADAAAGgDQAHgCAEAEQADABAEALQAEAKAHALQAEAFAAADQACAFgDAEQgBAAAAABQgBAAAAAAQgBAAgBAAQAAAAgBAAIgEgBgAgFgRIAAABIABgBIgBAAg");
    this.shape.setTransform(20.8153, -68.0293);

    this.instance_1 = new lib.Tween1("synched", 0);
    this.instance_1.setTransform(21.8, -69.25);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#000000").s().p("AAAAcQgDgBgDgEIgCgIQgCgFgCgCQgHAEgKgDQgFgCgKgHIgOgLQgHgEAAgEQAAgEADgCQACgCAEAAQAFgBAGAGIAJAJQAHAGAIABIAHAAIAHABQADABAEAEIAEAFIADACIAAAAQAJAAAKgGIAXgMIAHgDQAFgCADAEQADAEgEAGQgDAEgRAJQgTAIgEABIgIABQgBAEgDABQgDACgDAAIgCAAg");
    this.shape_1.setTransform(18.8643, -72.0878);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#000000").s().p("AgKAlQgCADgFgBQgEgCgBgEQgCgEgBgKIgFgLQgEgJgCgSIgCgRQgBgFACgDQAEgDAEAEQADAEABAFIAKAvQAGgFAFAEQAGADgCAHQARgeAKgPIAFgGQAEgCADAEQABACgCAFIggA8QgCAFgDABIgCAAQgFAAgEgJg");
    this.shape_2.setTransform(18.3138, -75.1542);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#000000").s().p("AABAkQgIgBgDgDIgCgFIgFAAQgHgCgJgGQgVgOgTgSQgGgFgCgFQgCgIAGgDQAGgDAJAJQAZAaAaAQIABAAIABgBQADgDAGACIAJAEIACADIAAABIA4gOIAIgBQAFAAABAEQADAGgGADQgDADgGABIgYAGQgWAFgMgBIgBACIgDACg");
    this.shape_3.setTransform(15.8141, -76.361);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#000000").s().p("AAnAaIgFgGQgEgGgIgIIgIgGIgFgBIgGgCIgDgEIgBgDIgEABQgYgBgRgDQgEAAgDgCQgEgEADgGQADgEAFgBQAFgBAFACIAWAEIAJAAQAEACABACIABABQACgCADgBQABAAABAAQAAAAABAAQABAAAAAAQABAAAAAAIADACIAFABQAFABABAGIAAADIAIAIIATATQAEAEAAADQABAFgEACIgDAAQgGAAgFgFg");
    this.shape_4.setTransform(11.6957, -73.7681);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#000000").s().p("AAcAiIgBgHIABgXIABgHIABgCIgIgBQgHgCAAgIIABgDIgFgCQgDgBgEADQgEAEgFAHIgLAKIgKAIIgGAFQgEADgEgDQgFgCADgHQABgDAGgDIAIgGIAIgJQAEgGAOgKQAEgEADAAQAGgCAHAHIABABIACgBQAHgDAEAAQADABAEADQADACABADQACAEgEAIIgBABQACAEAAAFIgCAGIAAAVQAAAGgBADQgCADgEAAQgDAAgDgDg");
    this.shape_5.setTransform(7.8917, -72.5722);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#000000").s().p("AA7AfQgJgDgPgNQgPgOgJgDIgHgDIgBAAQgGAAgDgCQgEABgGAAIguAAQgJAAgDgEQgDgDADgFQADgFAKAAQAQABANgBIAKgBIALABIADAAIAFgFIAFgDQACgBAEABQADABACADQADACACAEIAAADIAHADIAPAMQANAKAOAIQAJAFABAFQAAAGgHABIgDABIgIgCg");
    this.shape_6.setTransform(8.2308, -73.1833);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#000000").s().p("AgTAgQgDgBgCgEQgDgCgFgFIgbggQgFgGgBgCQgCgGAEgEQADgDAEABQAEABAEADQADAEAEAGIAGALIAKALIAFAHIADgCIAGgCQAGgBAFAEQACADABAEIAWgHQAQgFALgBIAJAAQAFADABAEQABAFgGAEIgKADIg0AIIgEABIgDABIgHAAIgFgBg");
    this.shape_7.setTransform(8.3963, -77.5625);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#000000").s().p("AgPAtIgEAAQgHgCgBgHQgFADgEgDQgFgEACgJIAEgOIABgIIADgMIAKgYQADgLAGACQAFABAAAFQABAEgDAFQgMAigBANQABAAAAAAQABABAAAAQABAAABAAQAAABABAAIAAABIAGgEQAHgDAEAFIACACQANgVAHgQQADgHADgDQAEgEAGADQADABAAAGQAAADgCAFQgKAYgOAYIgFAGQgDADgFAAIgCAAIgDAAg");
    this.shape_8.setTransform(5.8964, -79.3344);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.f("#000000").s().p("AgYAcIgBgBIgBgBQgDgDgDgFQgEgEgDgIQgIgRgGgFQgFADgEAAIgBAAQgJAHgGgBQAAAAAAAAQgBAAAAgBQgBAAAAAAQgBAAAAgBQAAAAgBgBQAAAAAAgBQAAAAAAgBQgBAAAAgBQAAgBABgBQAAgBAAAAQAAgBAAAAQABAAAAgBIAEgCIAFgFQADgCACAAQAGgKAKADQAIABALANIAGAJIADAKIAHAJIAAAAIABgBIAHAAIAGACIATgPQAOgLAKAAQAHABAKAIIAPAKQALAHgBAIQgIAFgMgFIgRgMQgEgDgDAAIgIAFQgLAKgTAKIgCACQgDAGgIABQgJAAgDgIg");
    this.shape_9.setTransform(5.8011, -78.6751);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.f("#000000").s().p("AAtAeQgDgDAAgDIgHgaQgGAEgIACIgFADQgGADgMgBIgEgBQgFgCAAgIIABgHIgBgBQgFgFgFgBIgIALQgGAGgCAEQgDAHgCADQgFAGgFgEQgDgCABgHQACgIAOgSIAIgKQAHgGAFACQAEABAEAEQAGAEAEAFIAAABIABgCQAKAAAEACQAFACADADIAFgDIAOgDQADgBACAEIACAHIADALQAEARgBAFIgEAJQgCgCgEgCg");
    this.shape_10.setTransform(1.9063, -76.7161);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.f("#000000").s().p("AAJAsQgDgEACgHIAJgiIACgOQgEgBgEgDIgBgCIgGAAQgGAGgGAMIgIAVQgEAKgHAAQgGgCgBgGQAAgEABgGQAGgRAJgPIAKgOQAFgJAFABQAEAAAEAFIAAAAIACgDQAFgCADAAQADAAAEACIAGADQACAEgBAEIgBAFQACAEgBAHIgEAUIgGAaQgCAJgDADQgCACgDABQgDAAgCgCg");
    this.shape_11.setTransform(-1.1396, -73.6);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.f("#000000").s().p("AAtAcQgEgEgEgMQgDgMgEgEQgGAGgKADIgDADIgFACIgGgBQgDgBgCgDQgEACgGgBQgGgBgGgFIgHgEQgDgDgCABQAAAAAAAAQgBAAAAAAQAAABgBAAQAAABAAABIgDAKQgCAGgDADQgDADgDgBQgGAAgBgKQAAgSAMgNQAGgGAFAAQAGgBAIAHQAJAJAEABIAEABQADgGAFgBQAFgBADACQAEABACAEIAAAAIAIgGQAEgEAEAAQAFAAAFADQAHAEAFAKIAIASQAEAKgDAFQgCADgFABQgFAAgEgDg");
    this.shape_12.setTransform(-1.64, -76.3517);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.f("#000000").s().p("AgYAhQgDgCgCgFIgCgEQgMABgQgPIgIgJQgHgKgCgOIABgGQADgGAHAEQADACAFAIIALARQAHAHAIACIAAgBQAEgDAFgBQAGAAAFACQAFAEACAFIADAAQAZADAVgPQAQgKAFgBQAIAAACAGQADAHgLAGQgYARgPAEQgKADgQAAQgHAAgEgBIgDAEQgCADgEAAIgCAAQgDAAgCgCg");
    this.shape_13.setTransform(-0.6769, -79.3758);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.f("#000000").s().p("AgZAsIgDAAQgCgBgDgDIgFgJQgEgHABgEQABgEAEgDQAAgJADgTIACgIIAEgGIAEgKQAAgBABAAQAAAAAAgBQAAAAABAAQAAAAABAAQAAAAAAAAQABAAAAAAQAAAAAAABQABAAAAABIgCAEQgCAEABAJQABAMgCAHIgBAGIgBAJIAFACIAGAFIACABQAIgEAKgQQANgUAHgPIAEgGQADgDAFABQACACABAEQABAKgJAOIgPAXQgGAKgHAHQgGAHgIACIgFAEIgDAEIgDAAg");
    this.shape_14.setTransform(-2.6061, -81.025);

    this.instance_2 = new lib.تنناخت("synched", 0);
    this.instance_2.setTransform(-5.15, -81.15);
    this.instance_2._off = true;

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance}]}).to({state: [{t: this.shape}]}, 4).to({state: [{t: this.instance_1}]}, 1).to({state: [{t: this.shape_1}]}, 4).to({state: [{t: this.shape_2}]}, 5).to({state: [{t: this.shape_3}]}, 4).to({state: [{t: this.shape_4}]}, 5).to({state: [{t: this.shape_5}]}, 5).to({state: [{t: this.shape_6}]}, 5).to({state: [{t: this.shape_7}]}, 4).to({state: [{t: this.shape_8}]}, 5).to({state: [{t: this.shape_9}]}, 5).to({state: [{t: this.shape_10}]}, 4).to({state: [{t: this.shape_11}]}, 5).to({state: [{t: this.shape_12}]}, 5).to({state: [{t: this.shape_13}]}, 4).to({state: [{t: this.shape_14}]}, 4).to({state: [{t: this.instance_2}]}, 4).to({state: [{t: this.instance_2}]}, 5).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(73).to({_off: false}, 0).to({
      x: -5.4,
      y: -80.7,
      alpha: 0
    }, 5).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.6, -85.4, 39, 21.800000000000004);


  (lib.صحراء = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // OBJECTS
    this.instance = new lib.CachedBmp_21();
    this.instance.setTransform(-14.9, -24.3, 0.3333, 0.3333);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(311));

    // flash0_ai
    this.instance_1 = new lib.الصحراء();
    this.instance_1.setTransform(-1, 5.05);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(311));

    // Layer_2
    this.instance_2 = new lib.Symbol15();
    this.instance_2.setTransform(51.95, -12.65, 1.1198, 1.1198, 0, 0, 0, 8.4, 8.3);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x: 49.8, y: -23.65}, 6).to({
      x: 44.15,
      y: -41.1
    }, 10).to({x: 35.75, y: -57.65}, 8).to({scaleX: 1.2773, scaleY: 1.2773, x: 23, y: -73.1}, 8).to({
      scaleX: 1.5871,
      scaleY: 1.5871,
      x: 0.6,
      y: -86.25
    }, 9).wait(52).to({regY: 8.2, scaleX: 1.2782, scaleY: 1.2782, x: -18.9, y: -77.25}, 36).to({
      regX: 8.3,
      scaleX: 1.0907,
      scaleY: 1.0907,
      x: -33.65,
      y: -65.25
    }, 16).to({scaleX: 0.9444, scaleY: 0.9444, x: -41.8, y: -54.15}, 19).to({x: -44.7, y: -41.85}, 20).to({
      x: -48.55,
      y: -12.85
    }, 20).wait(107));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-63.9, -100.2, 126, 158.5);


  (lib.سهول = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AiKFCQgDgEgBgGQgIgZABgeQgCACgDgBQgBgBAAAAQgBAAAAAAQgBgBAAAAQgBgBAAAAQgCgDAAgIIAAgNIABgFQABAAAAgBQAAAAABAAQAAAAABAAQABAAAAAAQABAAAAAAQABABABAAQAAAAAAABQABAAAAABQACADgBAHQgCAFACAEQAHACACANIANBAIgBAAQgFAAgEgEgADgDwQgEgCgBgCIAAgEIAAgKQACgDADgBQAEgBADACQgBAAAAABQAAAAgBABQAAAAAAABQAAABAAABIABAEIACAEQABAEAAADQAAADgEABIgFgDgADtDWIgCgBIgDgCQgFgBgCgBQgDgDAAgGIABgGQAAgBABAAQAAgBABAAQAAAAABAAQABAAAAAAIAAgBQAFAAACADIADAFIADACIAGAFIACADIAAACQgCACgDABgAizDLQAAAAgBgBQAAAAAAgBQAAAAAAAAQAAgBAAAAQAAgEACgEQADgDAEgBIABgEIADgCIAFgCIABgBIADABIgBACQACABACAEIAAAGQgBAEgCABQgCABgDAAQgEABgFAEIAAABgADoCMIgCgDIgDgDIgCgEIgDgNQgHgKAAgFQgBgHAEgCIAHAEQgBAAAAAAQAAAAAAABQgBAAAAABQAAAAABABQAAAAAAABQAAAAAAABQAAAAAAAAQABAAAAABQABACACADQAFAIABAFIABAGIADAFQAAACgDAEIgCACIgBAAgAl+BwQgCgBgCgFQgDgMADgXIgGgBIgBgFQgFACgDgGQgBgDABgGIACgJQACgFABgBQAAAAABAAQAAAAABgBQAAAAABAAQAAAAABABQAAAAABAAQAAAAAAAAQABAAAAABQAAAAAAABQAAAAAAABQAAAAgBABQAAAAAAAAQgBABAAAAIABAJIADAAIACADIABAHIAEAGIABAIIAAAUQACALADAFQAAAAgBABQAAAAAAAAQgBABAAAAQgBAAAAAAIgEgBgAirBdIAAgIIABgEQAAAAAAgBQAAAAAAgBQAAAAAAgBQgBAAAAgBIgCgEQAAAAAAgBQAAAAABgBQAAAAAAgBQABAAAAgBIAEgDIgHACQgFACgDgCIAAgCQAEgCACgCIARAAQACAFgDACIgDACQgCACAAAEQABAFgBACIgBAEIgBAFgADcBdQgBgBAAAAQAAgBAAAAQgBgBAAgBQAAAAAAgBIACgFIACgDIAGgDQAAAAAAgBQAAAAAAgBQAAAAABgBQAAAAABgBQAAAAABgBQAAAAABAAQAAAAABAAQAAAAABAAIAEACQACAAADgEQADgCADgBQAAAAABAAQAAAAAAAAQABAAAAABQABAAAAABQABAAAAABQAAAAABABQAAAAAAABQAAAAAAABQAAADgDAFQgHAHgEABIgHgBQgEAAgCACIgCACIgDABIgCAAgAl1AaQgBgEAAgDQgEAAgCgDQgCgDAEgEIAKgHIAEgCIAFgCQAIgCADABIAEADQABAAAAAAQAAABABAAQAAAAAAABQAAAAAAABIgCACQgBAAAAAAQAAAAAAAAQgBAAAAABQAAAAABAAQgHgBgIADIABAGQAEABACABQAAABAAAAQABABAAAAQAAABAAAAQAAABAAAAIgDAEQgDgBgFACIgGABIgEAAgAkKgXIgFgBQgBgBgBAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBgCABgDQAAgFACgCIADgCQABgBAAAAQAAgBABAAQAAAAAAgBQAAAAAAgBQgCAAgFADQgDADgDgCQgBgBAAAAQgBAAAAgBQAAAAAAgBQAAAAABgBIACgDQAHgGAKABQAHAAAAAFIgBAFQgDAEAAAEIAHABQAEACgCAFQAAADgFACQgDACgCAAQgFgBABgDgACNg9QgCgFACgDIADgDIADgEQAAAAAAgBQABAAAAgBQABAAAAAAQABAAAAAAQABAAAAgBQABAAAAAAQAAAAABAAQAAgBAAAAQAGgCAEAFIgCAEQgEABgEAEIgFAGIgEABIgDAAgAkNhiIgEAEQgFABgCgDQgCgCABgEIACgFIAIgIQAGgDAEAFIACAGQAAAAABAAQAAAAAAAAQABgBAAAAQAAgBABAAQAAgBAAAAQAAAAABgBQAAAAABAAQAAAAABAAQAEgBAEACIAKAIIgCAEQgGABgGgCIgCgBIgEAFIgFABQgFAAgEgEgAHjhjQgDgCAAgDQgBgCABgDIACgDIACgFQgCgDAAgDIABgGQAFgDAFADQACADgDADQADAIgDAEIgCADIAAAEQAAAFgCABIgCAAIgDgBgADChmIgHgCQgBABAAAAQAAAAgBAAQAAAAgBAAQAAgBgBAAIgBgEQAAgBAAgBQAAAAAAgBQABAAAAgBQAAAAAAAAIACgBQAAAAABgBQAAAAABAAQAAAAABAAQAAAAABABQABAAAAAAQABAAAAABQABAAAAABQAAAAAAABIABgDQABgFAFgBIADABIABACIABACIAAADIgDADQgCAFgCABgAnsiAQAAAAAAgBQgBAAAAAAQAAgBABAAQAAgBAAAAIADgCQAEgHAEgBIAAgBQABAAAAAAQABAAAAAAQABAAAAAAQABABABAAQAAAAABABQAAAAAAABQABAAAAABQAAAAAAABIABAAQAAgBAAAAQAAgBAAAAQAAAAABgBQAAAAABAAQAEgCACAFQgEABgBAFIgDAAIgEgBQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAABAAIgCABIgFAEIgCADIgCAAIgEgBgAHfiWQgFgCACgDIgGAAIgGgBQgFgCACgFQgBACgEABIgGgBQgEgCABgGIAFgIQgCgBgFAAQgFAAgBgDIAAgFIAAgBQgDAFgCACIgKACQgPABgIgBIgGgCQgDgBAAgEQABgEAEgBIAIAAQAMABALgDQADgHAEgDQAGgEAJADQAEACAIAHQAEADAAACIgBADQADACgBADIAAAEQAAAAgBABQAAAAABAAQAAABAAAAQAAAAAAABIAEACIABADQAAAAABAAQAAAAABAAQAAABABgBQAAAAABAAQAHgBACAEIACAFQAGADAAACQACAEgEACIgGABIgDAAIgDgBgAG9i8IAFAAIAAAAIgDgCQAAAAAAAAQgBABAAAAQAAAAAAAAQAAAAgBABgAB8ikQAAAAAAAAQABgBAAAAQAAAAgBAAQAAgBAAAAIgBgCQAAAAAAgBQAAAAAAAAQAAgBAAAAQABAAAAAAIADgCIADADQAAAAAAAAQABAAAAAAQABAAAAgBQABAAAAAAQABgCAAgEIABAAIALABIAAADIgGACIAAADIgIADIgEABIgEgBgACWizIgCgCIgCgDIAAgCIAGAAIgBAEIAAAAIABADIgBAAgAiAi7QgFgCAAgDQgEABgCAEIgEAAQAAAAgBAAQAAAAgBAAQAAAAAAgBQgBAAAAAAIgBgCQgBgHAGgEQAFgFAHACQAFACABgBIAEgBIAEABQAAAAABAAQAAAAABABQAAAAABAAQAAABAAAAQABACgBAEIgDAEIgBADIgCACIgEAAIgFgBgACkjCIAAgBIgBAAIgBgCIgBgBIAAgCIABgBIABAAIACgBIADACIAAACIgBADIgCAEQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAAAAAgBgAibjjIgOgEQgOgFgEgGQAHgFATADQACgFAGgEIAEgCIADgGQACgDAJADQALACAFACQAKAFACAKQABAJgEACQgFACgHgFQgGgFgEAAQgDAHgIgCQABACgCADQAAAAAAABQgBAAAAABQgBAAAAAAQgBABAAAAIgBAAIgHgBgAgejlIAEgIQAFgGACgCQAGgEAEAFQABgJAJgBQAFAAAKADIAFABQAAAAAAABQABAAAAABQAAAAAAABQABAAAAABQgBADgGAAIgIgBQgIgBAAABIgDAIQgCACgEgBIgGgDQgDAGgDACQgDACgCAAIgEgBgAgKkfQgEAEgGgCQAAgDgFgBIgGgCQgFgEACgHQABgEAGgGIAKgKQAFgDACABQADAAABAEIACAIIADAEQADADAIgDQAIgCACAEQADADgDAFIgFAIQgEAHgEgDIgEgDIgDACQgCADgCAAQgDAAgDgDg");
    this.shape.setTransform(0.7375, 3.1414);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#FFFFFF").s().p("ADWEaQAAgTgCgOIgCgFIgCgDQAAAAgBgBQAAAAAAgBQAAAAAAgBQAAAAABgBQAAgBABAAQAAgBAAAAQABAAABgBQAAAAABAAIAGABIADgBQAAAAAAABQABAAAAAAQAAAAAAABQAAAAAAABIAAADQgFgBgBAGQAAADACAFQACAHAAANIAAAUQACAKgCADQgGgFAAgTgAidDjIgDgCIgBgGIgBgHIgDgIIgCgCQgBgCACgEIAEgHQAAgBAAAAQABgBAAAAQAAAAABgBQAAAAABAAIACAAIABABQAAABAAAAQAAABAAAAQAAAAAAABQAAAAgBAAIgDAGIABACIABAMIACAGIABAJQABAEgCABQAAAAAAAAQAAgBAAAAQAAgBAAAAQgBAAAAgBgADlCyIgBgEIABgJIgBgCIgCgGIAAgEIgCgCQAAgBAAAAQAAgBAAAAQAAgBgBAAQAAAAAAgBIABAAQAAAAABAAQAAAAAAAAQAAABAAAAQAAAAAAAAIABACIABACIACAGIACACIAAACQACALgBAFIgCABIgBgBgAmBCXQgDgGgBgGIgDgOIgCAAIgCgCIABgEIABgBQAAAAAAABQAAAAAAABQAAAAABABQAAAAAAAAIABgDQAAAAAAAAQABgBAAAAQAAAAABAAQAAgBABAAIACABQAAAFABAFIAEASQAAAEgCADgADjCGIAAgGQABgIgHgGIgDgEIgBgFIABgEQABAAAAAAQAAgBABAAQAAAAAAABQABAAAAAAIAFgIQAFgEADAAQABAAAAABQABAAAAAAQABAAAAAAQABABAAAAQABABAAAAQAAABAAAAQAAABAAAAQAAABgBAAQgBgCgEABIgEADIgDAGIAAADQAAABgBAAQAAABAAAAQgBAAAAAAQAAAAgBAAIAHAJQADADAAADIAAADIgCALIgBAAQgBAAAAAAQgBAAAAgBQAAAAgBgBQAAAAAAgBgAilB7IgEgBQgCgBgBgGIgBgBIgDAAQgEAAAAgEQAAAAgBAAQgBAAAAAAQgBAAAAgBQAAAAgBgBIgCgEIgBgIQAAgBAAAAQABgBAAgBQAAAAABAAQAAgBABAAQACAAAAAFIABAGQADgDACADQADACgCADQAEAAACACQABAGADABIAGABQAEADgBADgAmOA2QgCgOABgGQAAgBABAAQAAAAAAgBQAAAAgBAAQAAgBAAAAIgDgBQgDgBABgHQgGAAgDgEQgDgEACgFIADgCQAAAAABAAQAAABAAAAQAAABABAAQAAABAAABQAAAEABABIAGgBQABAAAAAAQABAAAAABQABAAAAAAQAAAAABABQAAAAAAABQABAAAAABQAAAAAAAAQgBABAAAAQAFAAABAFIgBAIQgBAFACAQQACAMgEAIgAD1APQgCgCABgEIABgGIAAgDIgCgEQAAAAgBAAQAAgBAAAAQAAgBAAAAQABgBAAgBQABgDADgDIACACIgCAGIADAGIAAABIAAAEIgCAKgAkkAAQAAAAAAAAQAAAAAAAAQAAgBAAAAQAAAAAAAAIgCgBIgEABQgDAAgBgEIABgGQgFAEgGgCQgDgBACgEIAFgEIgHACQgEACgCgCQgBgFADgDIADgDIABgDIAGgEQAAAAABAAQABgBAAAAQABAAAAAAQAAABABAAQADABgDAEIgJAHQAGgCAGAAQAAAAABABQAAAAABAAQAAAAAAAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAgBAAIgGAGIAFgCIAFgCQAEAAAAAEQAAAEgDACQAFgCACACQABAAAAABQAAAAAAABQABAAAAAAQAAABAAAAQAAAFgEAAIgBAAgAFqguQAAAAABAAQAAgBABAAQAAgBABAAQAAAAABgBIAFgCIgCgCQAAAAgBAAQAAgBAAAAQAAgBABAAQAAAAAAgBIACgBIAHgBQAAAAAAAAQAAgBAAAAQgBgBAAgBQAAAAgBgBQAAgBgBAAQAAgBAAAAQgBgBAAgBQAAAAAAgBIACABIAEAFIADADIgCACIgGADIgBAEQAAABgFADIgDAAIgEgBgADFg3QgCgBgBgFIgGAAIgFgBQgDgCACgDIgIABQgFACgDgBQAAAAgBAAQAAAAgBgBQAAAAAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQABAAAAAAQABAAAAAAIAKgDIAHAAQAEABAAADIAKABQACABADAFQACACgBADQAAABAAAAQgBAAAAABQAAAAgBAAQAAAAAAAAIgEgBgAG0hJQAAgEAFgCIAIgCQAAAAABAAQAAAAABAAQAAAAABAAQAAgBAAAAIABgCIADgEIAEgCIACgEQABgEADgCQAFgCACAEIgCAHIAAAFIgDADIgFAHIgJAHQgHADgDAAQgIgBAAgGgADChGIgHgEIgEgCQAAAAgBgBQAAAAAAAAQAAgBgBAAQAAgBAAAAIAAgDIgDgCQgCgCgBgDQAAgFAHgHIAEgDQAEgBACACQACADgDAEIgFAGQAFABABACIAAACIADADIAHAFQAEAFgEAEIgBAAIgHgCgAjrhaQAAgDACgEQgGAEgEABQgHABgCgFQgHAGgFgBIgEgBIgIgBQgGgCACgEQAAgBABAAQAAAAABgBQAAAAABAAQAAAAABAAIAFgBIAHgBIAEAAIAHgBQAFgBACACIADABIAHgFIAEgCIAEACQAFAFAAAGQAAAHgGADIgCABQgDAAgBgFgAnYh+IABgFQAAAAAAgBQAAgBAAAAQAAgBABAAQAAAAAAgBQACgCADABIAGABQgBgFACgCIAFAAIADgCQADgCAEABQABABAAAAQABAAAAABQAAAAAAABQAAAAAAABQAAAAAAAAQAAABgBAAQAAAAgBAAQgBAAAAAAQgFABgBADIgFAAQABAEgEADIgIgBQAAABAAAAQAAABgBAAQAAABAAAAQAAABgBAAIgCABIgCgBgAjLiAQgDgBgBgFQAAgDADgDQAGgIAKADQABgKAIgCQAEgBABgCIABgEQADgHAIAAQAHAAAFAFQADADAAACQABAEgFACQgDACgFAAQAAADgEADQgEACgEAAQgBAAAAABQgBAAgBAAQAAAAAAAAQgBABAAAAIgCAGQgCADgGABQgHAAgCABIgEACIgEACIgBAAgACEidQgDAAgBgDQgCgDABgCIAHgMQACgDAEAAQAEAAADADIADgHQAEgCADAAQACABADADIACAEQAAABAAAAQAAABAAAAQAAABAAAAQAAAAgBAAQgFAAgCACIgCAGQgCAEgEABIgHABQgCAFgEAAIgDgBgAGNiwQgDgEgBgEIgIADIgGAAQgEgBgBgDQAAgFgBgDQgEADgDAAQgDgBgFgEQgFADgMAAIgOAAIgFgBQgDgBgBgDQAAgDABgDQADgDAIAAIAPAAIAIAAIAFgBIAGgBQAGABAAAFQAGgEAEADQACABABAEIAAAGQAIgDAEABQAEABACAEIADAHIAGgEQAEgCACAAIAEADIAEAEQABAEgCACQgDACgDgCIgEgFQgFAGgEABIgBAAQgDAAgDgDgACliyIABgCIABgCIABgCIAEgCIAGAAIAEABQABABAAAAQABAAAAABQAAAAABABQAAAAAAAAIgBADQAAAAgBABQAAAAAAAAQAAAAgBAAQAAAAgBAAIgHgBIgHACIgCgBgAgOjXQgCgDADgDQABgBAEACQAEABACgBQgCgCAAgEQAAgEADgDQADgEAJACQAAgGACgCQADgDAFABIAIgBIAGgDIAFgCQAEAAACADQACADgBADQAAADgEADQgFAEgHABQACAGgHADQgDABgHgBIgLAAQABAHgDABQAAABgBAAQAAAAgBAAQAAAAgBAAQAAAAAAgBIgKgCQACACgCADIgEgEgAiajZQgBgBgBAAQAAgBgBAAQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAABgBQAAAAABAAQABAAABAAIANAAIAAgDIACgBIAFABIAEABQABABAAAAQABABAAAAQAAABAAAAQAAABgBAAQAAABAAAAQgBAAAAAAQgBAAAAAAQgBAAAAAAIgEAAIgBACQgBABAAABQgBAAAAAAQgBAAAAAAQgBAAAAgBIAAAAIgIAAIgCABIgFgBgAgxkUQgCAEgEgDIgFgHIgCgJQAAgHAFgFQAEgEAEACQAEABAAAGQAAADgCACQAAAAABAAQAAAAABAAQAAAAAAAAQABABAAAAQAEABgBAEQADAAACgFIADgHQACgEAFACQAGACgCAFQAAAAAAAAQABgBAAAAQABAAAAAAQABgBABAAIAFAAIAHgEQAEgDACgBQADAAADABQACACABADQABAEgCAEQgCAHgCADQgEAGgEAAQgIABgBgHQgCABgDgCQgCAFgFAAQgFgBgBgFQgEAGgEACIgEACQgEAAgCgEg");
    this.shape_1.setTransform(1.725, 4.0545);
    this.shape_1._off = true;

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#FFFFFF").s().p("ADHEwIgBgBIAAgBIAAgDIAAgBIABgBIABgCIACgBIAAAAIADADIgCACIAAACIgBADIgCAAIgBAAgADDEjQgDgCAAgFIgBgPIgBgEQgCgFABgLQAAgFADAAQAAAAAAAAQABABAAAAQABAAAAABQAAAAAAABIAAALIAEAOIAAAGIABAFIACADQAAABABAAQAAAAAAABQAAAAAAAAQAAABAAAAQAAABAAAAQAAABgBAAQAAAAgBABQAAAAgBAAIgBAAIgDgBgAiwDXQgCgBgBgDIgBgGIABgRQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAAAABAAIABgBIACAAQACAKgCANIAAADIABADIgCACIAAAAgAmTC6IAAgJIgBgGQgDgJACgFIACAAIAEAUIAAAJQABAKgDAGgAi3CpIgCgHQgDgGABgBQgCABgDgCIgCgEIAAgEIAAgEQAAAAABgBQAAAAAAAAQABgBAAAAQABAAAAAAIABACIAAACQAAADACACIAEAAQAAAAABAAQAAAAAAAAQAAABAAAAQAAABAAAAIAAADIACAFIABADIABAEQAAAAAAABQAAAAAAAAQAAABAAAAQgBAAAAABgAmXCOQAAAAAAAAQAAgBgBAAQAAgBAAAAQAAgBAAgBIAAgTQgEABgDgFQgBgCAAgGIAAgHIADgFQAAgBAAAAQAAAAABAAQAAAAABAAQAAAAABAAQAAAAAAAAQAAABABAAQAAAAAAAAQAAABAAAAIgBADIAAAEIAAAHIABACIAFACQABABABAEIAAASIgBAEQgBAAAAAAQAAABAAAAQgBAAAAAAQAAAAAAAAIgCAAgAC7B3QAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQACgDAEgBQADAAAGADIAFACQgBAAAAAAQAAABAAAAQgBAAAAABQgBAAgBAAIgDAAQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAgBgBAAIgCABQgBAAAAAAQgBAAAAABQAAAAAAAAQAAAAAAABgAjMBvIgCgCIAAgDQAAgEABgBIACgCIAAgDIACgDIAEAAIABAAIACABIgCACIgDADIAAACQAAABAAAAQAAAAgBABQAAAAAAAAQgBAAAAAAIAAAEIABACQAAAAAAABQAAAAAAAAQgBABAAAAQAAAAgBAAIAAAAIgCAAgAjCBVIAAgIIgDgCQgBgBAAgBQAAAAgBgBQAAAAAAgBQAAAAAAgBIADgDQAAgBAAgBQAAAAAAgBQAAAAAAAAQgBgBAAAAIgCAAQgCgCABgDQABgBAAgBQABAAAAgBQAAAAAAgBQABAAAAgBIgCgHIAAgFQgDgBgBgCIAAgCIABgBIAIADQgBAFABADIACACQACADgEAFIABAFQACAEAAACIgCAFIADABQACABAAAEIgCALIgBAAQgDAAAAgEgAmsATIAAgHIACgGQgCgHABgEIABgBIACABIACAFQACAEgBAGIgBAEIABAHQABALADAJQACAIgDACQgGgJgEgXgACvAHQgCgHAEgEQABgBAAAAQAAAAAAAAQAAgBAAAAQAAAAAAgBIADgBQADgBAEABQABAAAAABQABAAAAAAQABABAAAAQAAAAAAABIgBABIgDABIgDAAIgCABIgCAEIgBAEIgCABIgCAAgAmtgUQgBAAAAAAQgBgBAAAAQAAAAAAgBQAAAAAAgBIAAgDQgBgJAAgEIADgKIgHgBQgCgCAAgEQAAgBAAAAQAAgBAAAAQABgBAAAAQABgBAAAAIACAAIABACIgBABQgBADAEAAQABAAABAAQABAAAAAAQABABAAAAQAAAAAAAAQACADgBAFIgCAGIAAAFIABAJIAAADIgCACIAAAAgADAgjIADADQACgBACgEQAAAAAAAAQABgBAAAAQABAAAAAAQABAAABAAIAFACQAAgBAAAAQAAAAAAgBQAAgBAAAAQABgBAAgBQAAgBAAAAQAAgBABAAQAAgBABAAQAAAAAAAAIADAAIABgCQABgGgBgDQAAAAAAgBQAAAAAAgBQAAAAAAAAQAAgBAAAAQAAAAABAAQAAAAABAAQAAAAAAAAQABABAAAAIABAEQACAHgCAGIgCADIgCAAIgCADQgBADgDABQgFAAgBgCIgEAGQgEADgCgFQgBAFgGAAQgCgIAIgEgAmBhMIgBgCQAAgBAAAAQAAAAAAAAQAAgBABAAQAAAAABAAIAGAAQgBgFACgCIgFgBQACgHADgEQABgBAAAAQABAAAAABQAAAAAAAAQABABAAABIgCADIgCADIAEgBQABAAAAAAQABABAAAAQAAAAAAABQAAABAAABQAAAAAAABQAAAAAAABQgBABAAAAQgBABAAAAQADADgCACIAFAAQAAAAABAAQABAAAAABQABAAAAAAQABAAAAABIACAAQAAAAAAABQAAABAAAAQAAAAgBABQAAAAAAAAQAAAAgBABQAAAAgBAAQAAAAgBAAQgBgBAAAAIgHABIgKACIgBgFgACyh/IAGgBQgBgFACgBQACgCAGAAIgBgEQgBAAAAgBQAAgBAAAAQAAgBAAAAQABAAAAgBQAAAAABAAQAAgBABAAQAAAAABAAQABAAAAAAQABAAABAAQABgBABAAQAAAAABAAQAAgBAAAAIgBgFQgBgBAAgBQAAAAgBgBQAAAAAAgBQABAAAAgBQAAAAABgBQAAAAABAAQABABAAAAQABAAAAABIAAADIAEADIgBADQgCAEgDACIgDABIAAAEQgBAEgDABQgCACgDgBIgBAGQgGAAgFACQgDgDAEgCgAkPiEQgEgBgBgEQgFAFgDgBQgBAAgBAAQAAAAgBAAQAAgBgBAAQAAAAgBgBQAAAAgBgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQgFADgHAAIgBgFQgDABgDgDQgBgCAAgFQAAgHACgGQAAAAAAgBQAAAAABgBQAAAAAAAAQABAAAAAAQAAAAABAAQAAAAAAABQAAAAAAABQABAAAAABIgDAJQgBAFACADQAAAAABAAQAAAAABgBQAAAAABgBQAAAAABgBQABgBAAAAQABgBAAAAQABAAAAAAQABAAAAABIAAAHQABAAAAAAQABAAAAAAQABAAAAAAQABgBABAAIADgCQAEAAACAIQACAAACgDQABAAAAgBQABAAAAgBQABAAAAAAQABAAAAAAIADACIAEACIAFAAIAHAAIADABQAAAAAAABQABAAAAAAQAAABgBAAQAAAAAAABIgLABIgEABIgCAAgAFiiGIABgCIACgBIAJgHQABAAAAgBQABAAAAAAQABAAABAAQAAAAABAAIADABIAIABIAIABIgCACIgPABIgDAAIgEADIgDADIgHABQAAgBgBAAQAAAAgBAAQAAgBAAAAQAAAAAAAAgAEaiHIgBgDIABgEQAAgBAAAAQABgBAAAAQABAAAAAAQABAAAAAAIgBgHQAAgBABAAQAAAAAAgBQAAAAABAAQAAgBAAAAIAEAAQgBgFACgCQAAAAABAAQAAgBAAAAQAAAAAAAAQAAgBAAAAIABAAIgBAFQACAGgCACIgDAAIAAAHIgBADIgDABQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAAAAAIAAADIgBAAIgCgBgAjmivQAAAAgBAAQAAgBgBAAQAAgBAAAAQAAgBAAgBQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAgBgBQgBgCABgDQAAAAgBAAQAAABgBgBQgBAAAAAAQgBAAAAgBQAAAAAAgBQgBgBAAAAQAAAAABgBQAAAAAAAAIACgEQgEABgDgCQAAgBAAAAQAAAAAAgBQAAgBAAAAQgBgBAAgBIACgGQADgFACgBQADgBAFAFIgGADQgCACABADQACgCADABQACAAACAEIAAAFIABADIAAAEIACABQABAAAAAAQAAABABAAQAAAAAAABQAAABgBAAIAEACQAAABABAAQAAABAAAAQAAAAAAABQAAAAAAABIgDABIgGgBgAlejIIgEgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBIgCACQgBABgBAAQAAABgBAAQAAAAgBAAQAAAAgBgBQgBAAAAgBQgBAAAAgBQAAgBAAAAQABgBAAAAIACgCIACgDQACgDADAAQABAAAAAAQAAAAABABQAAAAAAAAQABABAAAAIABADQAAAAAAABQAAAAABAAQAAAAAAAAQAAAAABgBQAAgBAAAAQABgBAAAAQABAAAAAAQABAAABAAIAFADQAHABACACQgEAEgDABIgEgCIgEAAQAAAAgBAAQAAABgBAAQAAAAAAABQAAAAgBABIgDgBgAGxjMQgDgIAEgHQgFABgCgCQgFgDAEgDIADgDIgCgDQgEgDAEgGQAAAAAAgBQAAAAAAAAQABgBAAAAQAAAAgBgBIgHgCIgDgDQAAAAAAgBQAAAAAAAAQAAgBAAAAQABAAAAAAIACAAIANAEQACACgBADQgEAEACACIACACIABAFQgBAAAAABQAAABAAAAQAAABABAAQAAAAAAABIADABIABAEIgCAMIgBADIgCABIgBAAgACSjaIgFgBQAAAAAAAAQgBgBAAAAQAAAAAAgBQgBAAAAAAQAAgEgBgBIgCgBQgBAAAAAAQgBgBAAAAQgBAAAAgBQAAAAAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQABgBAAAAQgBgCgEgBQAAgBAAAAQAAAAAAgBQAAAAAAAAQABgBAAAAQAAAAABAAQAAgBABAAQAAAAAAAAQABAAAAABIAEABIACADIAAACIADACQADACAAADQAAAAAAAAQgBABAAAAQAAAAABABQAAAAAAAAIAEACIAAACQAAABAAAAQAAAAgBABQAAAAAAAAQgBAAAAAAIgBAAgAiykBQgBAAgBAAQAAgBgBAAQAAAAAAAAQgBAAAAgBIgEABQgCgCAAgDIAAgEIADgCQAAgBABAAQAAAAAAAAQABABAAAAQAAAAAAABIADgBIADgCQAAAAAAAAQABAAAAAAQABAAAAAAQABAAAAAAQAAAAABABQAAAAAAAAQAAABAAAAQAAABAAAAIABADIACABIAHgBQAGgBABABQACADgDADIgFACIgIAAIgIAAgAAFkUQAAAAABgBQAAAAAAgBQAAAAABgBQAAAAABAAIAEgDIAEgEQABAAAAgBQABAAAAgBQABAAAAAAQABAAABAAIADADQABAAAAABQAAAAABAAQAAAAABAAQAAAAABAAQgCgGAHgCIAGAAQACAAADgDIAGgEIASgEQAFgBABACQABADgGADQgFABAAACQgHAAgGADQgGABgCAEQgGAAgDACIgCAEQgDADgEgBQgEgBgBgDIgFAEIgEAEIgCAAQgBAAAAAAQgBAAAAAAQgBgBAAAAQAAgBgBgBg");
    this.shape_2.setTransform(4.6, 12.4833);
    this.shape_2._off = true;

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#FFFFFF").s().p("ADFFuQgCgNgFgQIgCALIgDgBQgEgMAAgXIAAgBIACAAQgBADABAIIADAWQACgBABgGIACgQQAAAAAAAAQABAAAAABQAAAAAAAAQAAABAAAAIAAALIADANQADAIAAAFIACALQgCAAgBgFgAimFYIgCgMIgBgKIgBgGQAAAAABAAQAAAAAAAAQAAABABAAQAAABAAAAIAFAlIgBABIgCgMgADDEjQgCgJAAgSQAAgBAAAAQAAgBABAAQAAAAAAgBQAAAAABAAQAAAAAAAAQAAAAABAAQAAAAAAAAQABAAAAABIABADIgBAVIgBAFgAi1DvIgBgEQAAAAgBAAQAAgBAAAAQgBAAAAAAQgBABAAAAIgCgIIAAgEIACgEIACgBIABAEIgBACIACABIABADIAEAXQABANgCAGgAmXC8IgBAEIgFAAQACgPgDgQIAAgEQAAgBABAAQAAgBABAAQAAAAAAAAQABAAABAAQABAQgBAPQAAAAAAgBQABAAAAAAQAAgBAAgBQAAAAAAgBQADgCACAEIAAAGIACAMQABAIgBAFQgFgPAAgMgADDDFIgCgEIgBgFIgCgFQAAgBAAAAQAAgBAAAAQAAgBAAgBQABAAAAgBIADACIAGARIABAIQgEgBgCgHgAjEC3IgBgHIgDgKIgFgMIgBgHIADgIQACgFAFgFIgDgBIAAgFIADgBQAAAAABABQAAAAAAABQAAAAgBABQAAAAgBAAQABAAAAABQABAAAAAAQABABAAABQAAAAAAABIgCAFIgFAIIACABQAAABAAAAQAAAAAAABQAAAAAAAAQgBABAAAAQAAAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAQABAFAEAGQAEAIABALIADANQgEgDgBgFgAC3CkIgBgGIgBgHQAAgJACgGQABgDADgBQADgBADADIAFAFQABAAABAAQAAAAABAAQABAAAAAAQAAABABAAQABABgBAEIABAAIACgCIACAAQAAAGgFAAQgBAAgBAAQAAAAgBAAQAAAAAAAAQgBgBAAAAQAAgBAAAAQgBgBAAgBQAAAAAAgBQAAAAABgBQgGgBgEgFIgDAHQgBAHACAJIAAAGQAAABAAABQABAAgBABQAAAAAAAAQAAABgBAAQgCgBgBgFgAmgCHIgCgIIgBgoIAAgCQAAAAAAgBQABAAAAAAQAAAAAAAAQABAAAAAAQADANAAAbQABAPgBAEQAAgDgCgFgAjTBwIACgJIABgRIAEAAIAAAPIgBAJIgEAMIgCgKgAjHBSQgBgEgCgBQAAAAgBAAQAAAAgBAAQAAAAAAABQgBAAAAAAQgBABgBAAQAAAAgBAAQAAAAAAAAQgBAAAAAAQgCAAAAgFIABgGQgEACgCgCQgBgBABgFQAAABABAAQAAAAAAAAQAAAAAAgBQAAAAAAAAIACABIAAABIADgBQABAAAAAAQABAAAAAAQAAAAABAAQAAAAAAABIABACIABAGQAFgCADAFIACAEQACACAEABIgCACIgEAAQgDAAgBgCgADNBPQgFgGAEgFQgCgBgCgCIgEgFIgHgFQgEgDAAgFQAAgEADgEIgFgEQgEgDABgCIADgEIAFgFQACgDADAAIAFABQADABACAAQADAAAGgEIgFAGQgCABgEgBIgGAAQgEABgEAGIgBADQAAAAAAAAQAAABABAAQAAAAABABQAAAAABAAQADACgBAFIgBAHQAAADAEACIAFADIAGAGIAEADIgBAFIgBAJgADUBLIgBgHIgBgDIgBgDIgBgDIAAgKQAAAAAAgBQAAAAAAgBQABAAAAAAQAAAAABAAQAAAAABgBQAAAAAAABQAAAAAAAAQABAAAAABIAAADQgBADACADIABACIAAAEIABADIABAIIgCACgAmmA8IgEgGIgCgIIgBgTIgDACQgBgBABgIQACgFgBgGIgDADQAAABgBAAQAAAAAAABQgBAAAAAAQgBAAAAAAIgBgCIgCgLQgBgBAAAAQAAgBABAAQAAgBAAAAQABAAABAAQgCAEAEAKQADgGAEABIAAAOQACAAACAFIABAMIACAMIACAGQADAFgEAGQgCgCABgFgAmlgSIgCgFIgBgGQABgEAEABQgBgCACgDQABgCADgBIgEgCQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAgBABAAQACgFAFgEIACgBQABAAAAAAQAAAAAAABQABAAAAAAQAAAAAAABIgCACQgFADgCAGIADAAQAAABAAAAQABAAAAAAQAAABAAAAQAAAAAAABIgCAGIgBAGQgFAAAAACIABADQADALgIAFQACgHAAgEgADYglIgBgCIgHAAIgBAAIgBgDIAAgFIgBABQgEADgDgBIgCgBQgBAAAAAAQAAgBAAAAQAAAAAAgBQAAAAABAAIABgDQAAABABAAQAAAAABAAQABAAAAAAQABAAABgBQAEgCAEABIABAHIADgBQAAAAABgBQABAAAAAAQABAAAAABQABAAAAAAIACACIACABIABACQgDADgCAAIgCAAgAlDg6IgBgEIACgIIgGACQgFABAAgEIABgDIADgJIgEACIgFAAQgBAAAAAAQAAgBgBAAQAAgBAAAAQAAgBAAgBIABgEIACgGIABgDQABAAAAAAQABAAAAAAQAAAAABAAQAAABAAAAQgDAFgCAJIALgDQAAAAABAAQAAAAABAAQAAAAAAAAQAAABABAAIgBACIgEALIAFgCQABAAAAAAQABAAAAAAQABABABAAQAAABABAAQABADgDAHQAEgDAGAAQABAAABAAQAAABABAAQAAAAAAABQABAAAAABQAAAAgBAAQAAABAAAAQgBAAgBAAQAAABgBAAQgFAAgEADIgBABQgBAAAAgBQAAAAgBAAQAAAAAAAAQAAgBAAAAgAE0hqIgDgBQAAgBAAAAQgBAAAAgBQAAAAABgBQAAAAAAgBIADgDIAAgCQgBAAAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAgFACgCQABAAABAAQAAAAABAAQAAAAABABQAAAAABABIAEgGQAAgBABAAQAAAAAAAAQABgBAAAAQAAABABAAIgCAEQgDAEAAAFIgFAAIAAgBQAAAAAAAAQAAABAAAAQAAAAAAAAQAAABAAAAIABACQABACgCAFQAEACgCADIgBAAIgEgCgAkJhtQgDgDABgGIgEADQgCABgDgBIAAgFQgGABgCgEQgBgCABgEIACgIQABAAAAAAQAAAAABABQAAAAAAAAQAAAAABABIgDAIIABACIACABIAHgBIADABIACgBQABgBAAAAQABAAAAAAQABABAAAAQABABAAABIABAFQgBAAAAABQAAABAAAAQAAABAAABQAAAAABABQAHgFAJgDQAAAAABAAQAAAAAAAAQABAAAAAAQABAAAAAAQAAABAAAAQABAAAAABQAAAAgBABQAAAAAAAAIgMAIQgEACgDAAIgDgBgAichuIABgFIAKgNQABAAAAAAQABAAAAAAQAAAAABAAQAAAAABAAIAAAHIADgHIACgDIACgCIACAAIABABIgBAEIgEAEQgBAEgEAFQgBAAAAAAQAAAAgBAAQAAAAgBAAQAAAAAAgBIgBgDIgBAAIgEAGIgDACIgCACIgBgBgADmhvQgEAAgBgCIgBgDIgEgCIgDgBQgBgBAAAAQAAgBAAAAQAAgBABgBQAAAAABgBIADgEIgFABQgEgBAAgCQgBgCADgEIAEgFQAAAAAAAAQAAgBABAAQAAAAAAABQABAAAAABIgBADIgEAGIAGgBQAEABAAADIgDAHQADgCACAEIADADIAEABQAFgCAHACQgCADgFABIgFABIgEgBgABsh9QAAgBAAAAQAAgBABAAQAAAAABgBQAAAAABAAQABgBAAAAQABAAAAgBQABAAAAgBQAAAAAAgBIAAgDQAAAAAAgBQAAAAAAAAQAAgBAAAAQAAAAABAAQABgCAFABIACgIQAAAAABgBQAAAAAAABQABAAAAAAQABABABAAIABAEQABADAEAAQADAAADgCIAEgDQABACgCADIgFAEIgGAAIgDgBQgBgBAAAAQAAgBgBAAQAAgBgBAAQAAAAAAAAQAAAAAAABQAAABgBAAQAAABAAAAQgBABAAAAQgDACgCAAQAAAEgBACIgCABIgFADIgBAAQAAAAAAAAQgBAAAAgBQAAAAAAgBQAAAAAAgBgAk0iIIAAgRIABgCQAAAAAAgBQAAAAABAAQAAAAAAAAQABABAAAAIABACIAAAOIgBAEIgDACgAjki6QAAgBgBAAQAAAAAAgBQgBAAAAgBQAAgBAAAAIACgEQgDACgDgBQgDgCAAgDQAAgBAAAAQAAgBABAAQAAgBAAAAQAAgBABAAQAAAAAAgBQABAAAAABQAAAAABAAQAAAAAAABIABADIAEgCQAAgBABAAQAAAAABAAQABAAAAAAQABAAAAABQgBAFACAEIAMgGQABAAAAAAQABAAAAABQABAAAAAAQAAAAABABQAAAAAAABQAAAAAAAAQAAABAAAAQAAAAgBAAQAAABAAAAQAAAAgBAAQAAABAAAAQgBAAAAAAIgHADIgHADIgBAAIgDgBgAA8jwIADgFQABAAAAgBQABAAAAAAQABAAAAABQABAAAAABIAJgHQADgDACAAQAEAAADAHQAFAAAHgGQAIgGAFgBQAAAFgIAFIgMAHQgJACgDgHIgGAFIgHAFIgEABQgCACgCAEQgDAFgDAAQAAgFAGgJgAFmjnIgGgDIgEgCQgDgCACgHIgGgBQgBgEADgFIgGAAIABgHQACgFADAAIgDAKIAFgCQABABAAAAQABAAAAABQAAAAAAABQAAABgBABIgCAFIAEAAQABABAAAAQABAAAAABQAAAAAAABQABAAAAABQgCADABACIACABQAHACAGAFIgCADQgCAAgDgCgAGhj1QgDgDAAgHIgHgCQgBAAAAAAQAAAAAAAAQgBAAAAAAQAAAAAAgBIABgCIAEgFIABACIgCAEQAFgBACADIABAEQAAABAAABQABAAAAABQAAAAAAABQABAAAAAAQAAABABAAQAAAAABAAQAAAAABAAQABAAABAAQAJgBAIADQAAAAABAAQAAABgBAAQAAAAAAAAQgBAAAAAAIgQABQgFAAgCgBgAigkSIgDgBIgCgCIgCgGQgFgBgEADIgDAAQgBAAgBAAQAAAAAAAAQgBAAAAAAQAAAAAAgBIAAgDIAPgCIAEACIABADQABADACACIACABQAAABAAAAQgBAAAAAAQAAABAAAAQgBAAAAAAgAkOk8QgFgCADgDQABABAAABQABAAAAAAQABAAAAAAQABgBABgBIAFgEQADgDADAFIAGgDQAAAAAAAAQABAAAAAAQAAAAABAAQAAAAABAAIACACIACgDIAMgGIAEgBQABAAABAAQAAAAABABQAAAAAAAAQABABAAABQgPAHgGAGQgFACgCgFQgBADgEAAQgDAAgBgDIgFAEIgEABIgBAAgAhFlDIgBgBIgBgCQAAAAAAAAQABgBAAAAQAAAAABAAQAAABABAAIACABIACABIAEgBQAAABAAAAQABAAgBAAQAAABAAAAQAAAAAAABIgBAAIgDABQgBAAgBAAQAAgBgBAAQAAAAgBAAQAAAAgBgBgAhSleQgBgDACgHIADgFQACgCADABQADABABAFQACgCADgGQAGgEADADIACADIADAAQAAAAABABQAAAAABABQAAAAAAABQAAAAABABQABAEABABIADgDQAAgBABAAQAAgBAAAAQABgBAAAAQAAAAABAAQADAAABAFIADgEIADgBIACABIgEAFQgCAEgDAAQgCgBgBgFQgCAGgGABIgDgJQgDABgCgCQgDgCABgDQgDAAgDAEIgGAJQgBAAAAAAQgBABAAgBQgBAAAAgBQAAAAgBgBIgCgGQgEAIgBAGIgCgCg");
    this.shape_3.setTransform(5.0375, 9.5734);
    this.shape_3._off = true;

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#FFFFFF").s().p("AhjGXIgBgCIAAAAIAAgFIABAAIAAADIAAAAIABACIAAACgAhvF1QABgDgCgCIgBgCIAAgCIgBgCQAAAAAAgBQgBAAAAAAQAAgBAAAAQABAAAAgBIAAAAIACACIAAACIAAACIABABIAAABIABACIABAEQAAABAAABQAAABAAAAQgBABAAAAQAAAAAAABIgBgFgAD4FwIgCgBIAAgCIAAgGIAAgEQAAAAAAAAQgBAAAAAAQAAAAAAgBQAAAAABAAQAAgBAAAAQAAAAAAAAQAAgBABAAQAAAAAAAAQABAAAAAAQAAAAAAABQABAAAAAAQAAAAAAABIABABQABAFgBAEQAEABgBADIgCABIgDgBgADuFiQgCAAgCgDIgBgGQAAgTACgQQAAAAAAAAQABAAAAAAQAAAAAAABQAAAAAAABQACANgBASIABAFIAEADQAAAAABABQAAAAAAABQAAAAAAABQgBAAAAAAgAlcEPIgBgEIgCgEQgBgCABgEIgBgGQAAABgBAAQAAABAAAAQAAABgBAAQAAABgBAAQAAABgBAAQgBAAAAAAQgBAAAAAAQgBAAAAAAIgCgUIgCgOIABgFIACARIADAJQACAFgCADIAEgDQAAgBABAAQAAgBABAAQAAAAABAAQABAAAAABIABAEIACAMQACAHgCADIgBAAQAAAAgBAAQAAAAAAgBQAAAAAAAAQAAgBAAAAgAiKD8IAAgJIAAgDQgCgHACgEIAAAAIACAEIABAHIADABIACAFIABACIgBACIgCAAQAAgBAAAAQAAAAAAAAQAAAAAAgBQAAAAgBAAIgCgBQABACgBADgAlcDwIgBgCIAAAAIgBgCIABgJIgBgCIAAgCIACAAIABADIABAEIgBAEIABABIABACQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQgBAAAAAAQgBAAAAgBQgBAAAAAAQAAAAAAAAgADvDCQABgLgDgFIgDgFQgCgDAAgCQABgFgCgBIgCgCIAAgDIABgHQAAAAAAgBQABAAAAgBQAAAAAAgBQABAAAAAAIAFAAIADgCIAEgDQACgBACAEIAEAEQADADACgDQAAAAABAAQAAABAAAAQABABAAAAQAAABgBABQAAAAgBABQAAAAAAAAQgBAAgBABQAAAAgBAAQgHABgDgIQgCAEgEAAIgDABQgBABAAAAQAAAAAAABQAAAAAAABQAAABAAAAIAEALIAEAJQABAFAAAKIAAAQQAAAGgDABQgCgLABgKgAiPC4QACgQgDgGIgFACQgDACgCAAQgFAAABgIQAAgHgBgCQgBAAAAAAQAAAAgBAAQAAAAAAAAQgBAAAAAAQgBgBAAAAQgBAAAAAAQAAgBAAAAQAAAAABgBIACgBIABgCIgCAAIgBgCIACgEIAEgEQAAgBAAAAQABgBAAAAQAAAAABAAQAAAAAAABQABAAAAAAQAAAAAAABQAAAAgBAAQAAAAAAABIgDAGQAEAKAAAMQAIgGAEADQACABABADQACAHgCAKIgCARIABAIQgEgHABgOgAiWB2QgBAAAAAAQAAgBAAAAQAAgBAAAAQABgBAAAAIAEgCQACgCgDgGIgEgHIACgHIAAgCIADAAIACACIAAgLQgBAAAAAAQgBgBAAAAQAAAAAAgBQAAAAAAgBQAAAAABgBQAAAAABAAQAAAAABAAQABABAAAAIACAFQABAKgEAFIgDAAIADAHQAEAHgFAHgAlrBZIgBgEQAAAAgBABQAAAAAAAAQgBAAAAAAQgBAAAAAAQgDgGABgGQAAAAgBAAQAAAAAAABQAAAAgBABQAAAAAAABIgEAAQgEgJAAgNIABgEQAAAAABgBQAAAAABAAQAAAAABABQAAAAAAABQgCADABAFIABAIIABAEQAAAAAAABQABAAAAAAQAAAAABAAQAAAAABAAIABgBIAAgBIACgBIACACIAAAFQgBADACACQAAgBABAAQAAAAABAAQAAABABAAQAAAAABABIAAAEQABAKgBACIgCgKgADnAuIAAgJQAAgFgCgCIgEgDIAGgFQADgCABgDQAAAAAAgBQAAAAABgBQAAAAAAgBQAAAAABAAQABgBAEAAIAFgDIADADQAEACAIgDIAFgRQAAAAABAAQAAAAAAAAQAAAAABAAQAAABAAAAIAAADIgDAMQgBAFgDABQgBABgHgBQgHgBgIACQAAAFgFACIgCACIAAACQADADAAAHIAAALIACAGQABAEgCACQgGgHABgJgAiXA3IgBgCIgBgEQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAgBgBAAIgFgBQgEgCACgEIACAAIABAAIABACIAGACQACABABAEIABACIACADIABAGIAAAAQgDAAgCgDgAl1gGIgFgIQgDgGACgGQgEACgDAAQgEgBgBgGIAAgJIABAAIACAGQAAAFADABIAEgBQABAAABAAQAAAAABAAQAAAAABAAQAAAAABAAIAAAHQAAAFAHAJQADAGgDAFQgCgDgCgGgAiwgCIgCgBIgDAAQgBgBAAAAQAAAAgBAAQAAgBAAAAQAAAAAAgBQgCABgBAEIgDAAIgCgBIAAgCIgBgGIACAAIACAEIADgDQACgCADADIABABIACAAIADAAQAFgCACAAQABABAAAAQAAABAAABQAAAAgBABQAAAAAAABQgBAAAAAAQAAAAgBAAQAAAAgBABQgBAAAAABIgCABgAk1gyIgFgDIgCgBIgBgEIgDAAQgBAAAAAAQAAAAgBAAQAAAAAAAAQAAAAgBAAIgBgCIACgHIAAgEIACgBQAEgBACADQgBAAAAABQgBAAAAAAQgBABAAAAQgBAAAAAAIAAAEQAAgBABAAQABAAAAAAQABAAAAAAQABAAAAAAIABAEIABADIADACIAEAFIgCACIgCgBgAEDg7QgDgDABgDIgGAEIgDAAIgCgBQgBAAAAgBQAAAAAAAAQgBgBAAAAQAAgBAAAAIABgDQABAAAAAAQABAAAAAAQABAAAAABQABAAAAABIAGgEIADgBIABABIACAGIABAAIACgFIACgCIACgBIADAAQAAAAABABQAAAAAAAAQAAABAAAAQAAAAAAABIgEABIgBACIgBACIgCAEIgDABIgCAAgAGBhiIgCAAQAAgBAAAAQgBAAAAAAQAAgBAAAAQAAAAABgBIABgCIgEAAIgBgCIAAgDIADgEIgDgBIgDgBQAAgBAAAAQAAAAAAgBQAAAAAAAAQABgBAAAAIADgGIABgCIABABQACADgEAEIADAAQAAAAAAAAQABABAAAAQAAAAAAAAQAAABAAAAQABAEgCAEQABgBAAAAQABAAAAAAQABAAAAABQABAAABABIABACQAAABAAAAQAAAAgBABQAAAAAAAAQAAAAgBAAIAIABQAAABABAAQAAAAAAABQAAAAAAAAQAAABAAAAgAhQhkIABgCIABgCIACgEIAEgBQAAAAABAAQAAAAABAAQAAAAAAgBQABAAAAAAIAAgDIADgBIABgCIABAAIABABIAAADIgCACIgCAFIgEACQAAAAgBAAQAAAAgBAAQAAAAAAABQgBAAAAAAIgDADIgBABQAAAAgBgBQAAAAAAAAQAAAAAAAAQgBAAAAgBgADChsQAAgEADgDIAHgDQABAAABAAQAAgBAAAAQABAAAAAAQAAgBgBAAIAAgCQgCgEADgCIAFgCQABAAAAAAQABgBAAAAQAAgBABAAQAAgBAAAAIADABQAAAAABABQAAAAAAAAQAAABAAAAQAAAAAAABIgBABIgHAEIABADQAAAAAAAAQAAABAAAAQAAABAAAAQgBAAAAABIgCADIgGADQgBAAAAAAQgBAAAAAAQgBAAAAABQAAAAgBAAIgCACIgCABIgBAAgAjNiLIAAgLQgCAEgDAAQgDAAgBgEIgBgLIABgDQAAAAAAAAQABgBAAAAQAAAAAAABQABAAAAAAIABAPIACgBQACgCADgCQABAAAAAAQABAAAAAAQAAABAAAAQAAABAAAAIAAAHIAAACIADABQACABAFgCQAGgDABABQAGABgCAEIgHgCQgEADgJAAIgEAAgAHvidIgFgEIAAABIgKAAIgBgGIABgBIABAAQABAAAAABQAAAAAAAAQAAABAAAAQAAAAAAABIAIAAIABgBIADACIACACIABABIADAAQAAABABAAQAAAAAAABQAAAAAAABQAAAAgBABIgCAAIgDgBgAkZimIgBgBIgCgBIgCgCQgBAAAAAAQAAgBgBAAQAAAAAAAAQAAAAgBAAIAAgDIABAAQADAFAGAAQAAgFABgBQABgBAAAAQAAAAABAAQAAAAAAAAQABAAAAAAIAEAAIAEgEQAEgBACABQAAABAAAAQAAABAAAAQgBAAAAABQgBAAAAAAIgDADIgCACQAAABAAAAQgBAAAAAAQgBAAAAAAQgBAAAAAAIgEAAIgCAEIgCABIgCAAgAHwiwQAAAAgBAAQAAgBgBAAQAAAAgBAAQAAgBAAAAQAAgBgBgBQAAAAAAgBQAAAAAAgBQABAAAAgBIgFABQAAAAgBAAQAAgBAAAAQgBAAAAAAQAAgBgBAAIgBAAIAAABIgHAAQgCgHACgGQABAAAAAAQAAAAABAAQAAAAAAAAQABABAAAAIAAAEIABADQACAAACgCQADABACACQADgBACACQACABAAAEIAFgEIAFgCQABgBABAAQAAAAABAAQAAABABAAQAAABAAABQAAABgHAEIgEADIgDABIgBAAgAn8jAQgBAAAAAAQAAgBgBAAQAAAAAAgBQAAAAAAAAQAAgBAAAAQABAAAAAAQAAAAABAAQAAAAABAAIAIABQgDgEADgBIACgBIAJgBQgBAAAAAAQAAAAAAgBQAAAAAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAAAABgBQAAAAAAAAQACgBAFAAQgBgBAAAAQAAAAAAgBQAAAAAAgBQAAAAAAgBIACgCQAEgEAIgCQgBAFgFACIgDACQgBACABAEQAAAAgBAAQAAABgBAAQAAAAgBAAQAAAAgBAAIgEABIACACIAAADIgDABIgKAAIAEAEIgBACIgEAAQgGAAgFgCgACojRIAGAAQAAAAABgBQAAAAAAAAQABAAAAgBQABAAABAAIAFgDIAEgCQAAABAAAAQAAAAAAABQAAAAAAABQAAAAgBABIgFAEIgDAAIgMABgACWjfIgBgBIABgCIAEgBQAEAAAFgDIADgCQADAAABAFQACgBAEABQAAAAAAgBQAAAAABAAQAAAAAAgBQAAAAAAgBIABgHQAEgCAEADQADAAACgCQADgCAAgDIAAgCQABgBAAAAQAAgBAAAAQABAAAAAAQAAAAAAAAIAAAHQAAAEgFADQgFABgEgCQAAAAAAAAQgBAAAAAAQAAAAAAAAQAAABAAABIAAACQAAAFgFACIgGAAQgDgBgCgDQgGAEgGAAIgDAAgAHajmIgDgCQgBgBAAAAQAAgBAAAAQAAAAAAgBQABAAAAAAIABgBQABAAAAABQABAAAAAAQAAAAABABQAAAAAAABIASACQAAAAAAABQAAAAgBAAQAAAAAAAAQAAAAgBAAIgHABIgKgBgAh/jzIgDADQAAAAgBAAQAAABgBAAQAAAAAAAAQgBAAAAgBQgBAAAAAAQAAAAAAgBQgBAAAAgBQAAgBAAAAIAAgLQAAgBAAAAQAAgBABAAQAAAAAAgBQAAAAAAAAIACABIABACIgBAKIADgCQABgBABAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABABQAAAAAAABQAAAAAAABQAAABgBAAQAKADAIgFIgBADIgDACIgFACIgFAAIgHABQgBgEABgDgAgIksIgOgCIAAgCIAAgBIAEgBIAIABIAGABIAEgDQAAgBABAAQAAgBABAAQAAgBABAAQAAAAABAAIAFgBIACgEQAEgGADACIABADQAIgBAFgFIABACIgBACQgHAKgLgFQABAEgFACQgFABgCACIgEAEIgDABIgEgBgAiOkyQgGAAgCgDQABAAAAAAQAAAAAAAAQABAAAAAAQAAAAABAAIABAAIAJAAIAAgCIACAAIADgDIACgBIACABIgFAFIgDADgAjak5QgIgDgLgBQACgDAEABIAGABQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAgBAAAAIALgCQgBAAAAAAQgBAAAAAAQAAAAAAgBQAAAAABAAIABgCQAHgCADgCIAEgCQAAgBABAAQAAAAABAAQAAABABAAQAAAAAAABQgEAGgGACIgDACIAAACQAAAAAAAAQAAAAAAAAQAAABgBAAQAAAAAAAAIgJABIAEADIAAACgAAqmMIgBgDIgCABIgJABQgBgEgCgCQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIACgCQABAAABAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAAAABQABABAAAAQAAABABAAQAAAAAAAAQABAAAAABQABAAAAAAQABAAAAAAQABgBAAAAIAEgCQABgBABAAQAAAAABABQAAAAABAAQAAABAAABQAAAAAAAAQAAABgBAAQAAAAAAAAQAAABgBAAIABABIAHgCQABAAAAAAQABAAAAAAQAAAAAAABQAAAAAAABIgHADIgCAAIgDAAg");
    this.shape_4.setTransform(-0.08, 9.53);
    this.shape_4._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape).to({_off: true}, 14).wait(49).to({_off: false}, 0).to({_off: true}, 13).wait(53).to({_off: false}, 0).to({_off: true}, 15).wait(53).to({_off: false}, 0).to({_off: true}, 12).wait(51).to({_off: false}, 0).to({_off: true}, 13).wait(53).to({_off: false}, 0).to({_off: true}, 12).wait(42));
    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(14).to({_off: false}, 0).to({_off: true}, 15).wait(47).to({_off: false}, 0).to({_off: true}, 15).wait(53).to({_off: false}, 0).to({_off: true}, 13).wait(52).to({_off: false}, 0).to({_off: true}, 13).wait(51).to({_off: false}, 0).to({_off: true}, 12).wait(53).to({_off: false}, 0).to({_off: true}, 11).wait(31));
    this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(29).to({_off: false}, 0).to({_off: true}, 12).wait(50).to({_off: false}, 0).to({_off: true}, 14).wait(52).to({_off: false}, 0).to({_off: true}, 13).wait(52).to({_off: false}, 0).to({_off: true}, 14).wait(49).to({_off: false}, 0).to({_off: true}, 12).wait(52).to({_off: false}, 0).to({_off: true}, 10).wait(21));
    this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(41).to({_off: false}, 0).to({_off: true}, 11).wait(53).to({_off: false}, 0).to({_off: true}, 12).wait(53).to({_off: false}, 0).to({_off: true}, 14).wait(52).to({_off: false}, 0).to({_off: true}, 12).wait(49).to({_off: false}, 0).to({_off: true}, 15).wait(47).to({_off: false}, 0).to({_off: true}, 10).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(52).to({_off: false}, 0).to({_off: true}, 11).wait(54).to({_off: false}, 0).to({_off: true}, 12).wait(55).to({_off: false}, 0).to({_off: true}, 13).wait(51).to({_off: false}, 0).to({_off: true}, 12).wait(52).to({_off: false}, 0).to({_off: true}, 14).wait(43).to({_off: false}, 0).wait(11));

    // flash0_ai
    this.instance = new lib.سهول1();

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#0066FF").s().p("AgBAEIgBgBQgBAAAAgBQAAAAgBgBQAAAAAAgBQAAAAAAAAIABgBQAAgBABgBQAAAAABAAQAAAAABAAQAAAAAAAAIAAgBQABAAAAAAQABAAABABQAAAAABAAQAAABABAAIAAACIAAAAQAAABAAABQAAAAgBABQAAAAAAAAQgBABAAAAIgBABg");
    this.shape_5.setTransform(-25.95, -63.025);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_5}, {t: this.instance}]}).wait(380));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-63, -63.5, 126.3, 123.4);


  (lib.زرالسهول = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#000000").s().p("AgsPtQgjgCgdgRQgfgTgMgeQgdAPgkgBQgigCgggPQgcgNgdgXQgSgOghgfIhvhnQg/g6gfgXIhBgvQgngcgWgYQglgqgYhAQgRgrgRhNQghiRgciOQgEgWAEgRQAGgUAQgBQgPgOgVABQACgOAKgLQALgKAOgCIg6gmIAQgpQBogUBrAIQgNgnABgrQBMAfBjhnQA+hBAjgZQA7gqA5AAIAgABQATgCAKgIQAKgJAFgTIAHgiQAIgcAagSQAZgSAdACQAEgTgbgTQgkgYgGgLQgJgPAGgTQAFgSAPgMQAUgRAugKQgWgUgJgLQgQgSgIgSQgJgVADgWQADgYAQgNQgYAAgPgXQgPgXAHgXQAHgXAVgPQAVgOAZgCQAngEA3AYQAZALAMACQAVAFAPgHQAIgEANgNQANgNAIgEQAYgLAaASQAYAQAMAdQAHARANAkQAMAeATAPQARgjAtAGQAuAGAKAlQAFASgEAWQgDAPgJAZQgNAmgLAUQgRAfgXAPQgVAOhLAOQg9ALgUAfQgJANABAQQABARALAIQAMAJAUgFQAMgCAXgHQAogJAiAdQAiAegCAoQBzAUB0AnQCFAtAOBOIAEAeQADASAHAKQAHALAPAHIAbALQBbAkA6BlQAbAuAXBGQASA4ABAmQAAAfgKAmQgGAXgRArQglBmgaA0QgqBTgzA2QgZAbglAcQgUAQgwAhIiiBwQhZA8gnAVQguAYg9AXQglAOhKAXQgpAOgXAFQgcAGgaAAIgMAAg");
    this.shape.setTransform(0.0261, -0.0042);
    this.shape._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off: false}, 0).wait(1));

    // Layer_3
    this.instance = new lib.Symbol22("synched", 0);
    this.instance.setTransform(-3, 98, 0.2151, 0.2151, 0, 0, 0, 0, -45.8);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off: false}, 0).to({_off: true}, 2).wait(1));

    // space
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(10,0,0,0.22)").s().p("AgzPJIgMgMQgegdgSgMIgKgFIgTADIgHAGIgBACIgPAOIgLAJIgOAMIgEAAIgngDQgQghgsgUQgogSgDgNIgKgkQgoglgIgEIAAgBIgmgYQgUgNgTgRIABAAQgbgZgdggIgbgfIgOgDIgEgBQgbgIgYgpQgogKgQgGQgKgEgJgHIgDgDIgXgdQgXgtgahjQgahigIgNQggg8gMhXQgIg7AAhfQgVAHgKABQATghBAgMIAXgOIgjgCQg9gBgyALQAhgcBEgDQAWgBA7AEIADgCIhYgPQhDgKguAGQAxgYBIAIQAnAEA+ATIADgCQglgUgwgOQhHgUgpAKQArgRA+AMQAiAIBEATIhAgcQgogTgbgFQBqAJA1ApIANgJIAKgHQAAgVgCgHQgJghgDgTQgJgjAOgMQASAtAIBDIAAgCQATgyAdghQAaggAvgfIAngYIAXgPQAsghAEgCQAUgLAogIQANgDApgDQAlgEAKgLIADgEQAGgHACgLQABgKgBgOQgEgeAHgMQATgnA0gKQAbgFAeADQgbgLgKgRIgIAAIgOAAIgBAAIgDAAIgCgBIgBAAQgjgFgPgLQgUgOADgfIAAgDIAEgNQAEgIAGgIQANgPARgGQAMgGAUAAQAXAAAGAJIACAAQAngKAPAgQAHALAAALQAFAggnAAIgEAAIAIAYQAGAUAOAIQAXANA2AMIARAHQAGACAFAAIAHgDQAGgGAAgNQAAgNgEgGQgGgLgSgRQgTgTgGgJQgOgVgGgRQgGgSAEgJIgJgDQgXgEgOgFIAAAAIgCgBQgqgRAIgjQgbADgEgXIgBgCQgDgMADgKQAEgZAZgDIABgGIABABIACgIQgEgIAAgJIgEgIIgPAFIgJABQgMABgJgHIgGgGQgGgGgDgHQgEgLACgMQAAg2A3gGIADgBQADgNAbgLQAZgJATABQAXABAVANQAMAFAHAJQAEAEAEAGIASgDQAUgHAigPQAdgKAdANQAvAUALAiIACAUQAAAMgEALQAkgCALAkQAHgGANgEQAYgKASABQAYAAAWANQALAGAIAIQAIAIAEAJIACACQARAkgTAbQgNAUgpAVQgNAHgLAFIACAHIACAGQAGAfgjAAIgDAAQgBAWgQAHQgMAEgZgEQgegDgNgKQgIgGgEgJIgPAOQgBARgJALQgKALgYANQgYAPgJAJQgVAUACAvIAMAHIAdgIQA1gMAmAGQAmAGAsArQAtAsAgAIQAaAHBJACQA8ABAlAUQBUAsAcBnIAFAXIADASQACASAAARIAFACIABgEQANgiAcgdQgQAdgMAtIAIAEIABgCQABgRADgNQAFADADAIIAIgPQAXghAbgMQgXANgYArIgKATIAAAEIAAAJIABABQAMgXAMgNQAdglAjgKQgZASgaAjIgaAkIADABQATgWAIgGQAhgeAcgCQgpAZgiAqIAGADIAPgLQAkgYAXAHIgfARQgTAJgKAJIABABIANgIQAkgRAngGQgZAKgxAcIAEADIAfgPQAmgOAdAFQgbgCgsAWIgRAJIAIAFQANgFAKgDQAtgOAiALQgeABgpANIgPAGIAIAEIAFAAQAsgFAYAOQgcgCgbAFQA0AiAZAbQgLAggHBbIAOAFQAFACAEAEIABABIARAxQAOA0gNARQgHAKgSAQQgOAMgIAPQgGAMgHAbQgHAWgKAPIgIAKIgPANIABAMIABAMIgCACQgCAqgQAmIAAAAQgPAJg6BDIgSAVQgRAVgSAPQgwAtgxAOQgdAIgeAPQgXALgWAPQgMA1gLAIIgKAHIgPAIIgNAGIgCAAQgUAGgUgIIhCA6IABgBIgJAHQgOANgOAIQgbAOgbgBIgigKIgDACIgEABQgfANgzArIgFAEQg0AqgeAHQgqgPghgcgAgDoMIAHgEIgKgGIgQgEQAKAGAJAIgAhjo+QgBgGgJgXIgDgHIgCAGIgOAMIgEACIACADQAOAMARABIAAAAgAgeq3QAeB2BKgnIAmgQQAYgLAHgNQACgEAFgpIACgJQgeADgTgSIgCgCQggAUgXAGQgcAHgggEIgCAAIgHgBIgIgCIABAGgACrrKIgDABIgKADIgBADIAAAiQACgDAKgHQgBgIAAgIIABgEIAAgDQABgEADgFIgCABg");
    this.shape_1.setTransform(-2.5802, 1.345);
    this.shape_1._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1).to({_off: false}, 0).to({_off: true}, 1).wait(2));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-97.5, -100.5, 189.4, 255.7);


  (lib.زرالصحراء = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#000000").s().p("AgUMJQgZAAgmgZQgrgdgTgFQgVgGg4AGQgxAFgZgNQgVgLgYghQgbgogNgLQgJgHgegRQgagOgLgNQgIgKgMgZQgLgYgJgKQgOgRgcgJIgygNQgfgJgkgSQgVgLgpgYQgZgOgMgJQgTgOgMgQQgPgWgMgxQgyjOgTjSQgEgmAHgTQAGgTASgTIAhggQAkghA2hGQA7hMAdgeQBThUBtgoIA/gXQAbgMB2hMQBXg4BAgDQAogCAuAPQAhALAvAZQAlATCcBcQB2BFBQAhQAdAMAKAFQAWALAOANQAQAQAGAUQAHAXgJASQAlgOAuAWQAdAPAuAoQAoAjAnANQAxAQAggaQAKAVgDAYQgDAZgQARIgPAQQgJAJgBAJQgCALAHAMIANAWQAbAqgGA+QgFArgYBCQgpBzgmA8QgoA+hFA/QgqAlhZBEIhhBJQg0AngZANQgWALggALIg4ATQhhAghaA5QgbASgMAFQgVAKgSAAIgCAAg");
    this.shape.setTransform(-0.0074, 0.0193);
    this.shape._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off: false}, 0).wait(1));

    // space
    this.instance = new lib.Symbol19("synched", 0);
    this.instance.setTransform(4.05, 85.9, 0.2966, 0.2966, 0, 0, 0, 0, -7.8);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off: false}, 0).to({_off: true}, 2).wait(1));

    // Layer_3
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f().s("#FF7D00").ss(1, 1, 1).p("Ag7hvQAsAHAhAgQAqAqAAA7QAAAvgbAk");
    this.shape_1.setTransform(84.9625, -27.1875);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("rgba(6,0,0,0.22)").s().p("Ag4L2IgMgLQgggfgTgMIgJgDIgUADIgGAGIgpAlIgFAAIgmgDQgRgigsgUQgogSgEgOIgHgZIgEgNIgBAAIgFgGQghgdgIgFIgBAAIgngZQgUgOgRgQIABAAQgdgZgcghIgLgLIgJgKIgJgKIgNgDIgEAAQgbgJgYgpIgzgOIgCgBIgEgCIgDAAIgCgCQgJgEgGgEQgGgGgHgIIgNgUQgXgtgbhjQgahkgIgOQghg8gLhXQgJg7AAhkIADAAIgFgEIAFgDQgQgHgSgEIgwgIQAtAAAsAQIALgHQgfgegWgnQAUAdAoAkIAMgHIgKgbQgMgggLgTQAOARATAiIALAVQAEgDAGgCIgGgaQgHgqAIgSQgCAZAKApIAFAPIASgLIAEgFQAHgRAMgRIgDAMQBBhcAyg1QBKhPBCgcQBKhDAYgHQAVgIAoAoQAhgZAtgjQCVh3BKgDQAngJBPA0QAtAeAlAhIAHADQBsA7AYATQAhAdB0AwQBrAsA2A7IgBAmIA3gKIA5AcQAlgdAyAAQAZAAAVAHQAeAKAYAXQApAqAAA7QAAAugaAjQgEAdgEAoIALAEIAFADQAKAGAAALIAMAkQAPA1gMAQQgIAJgRAPIgCACQgOAMgHAQQgHAMgHAbQgHAXgKAPIgDAEQgGAIgNALIAAARIAAAIIAAACQgEApgQAnIgBABQgQAKhLBYIgNAPIgWAWQgxAtgyAOQgcAHgfAQQgUAKgaARQgMA0gLAIIgKAHIgSALIgHADIgDABQgXAFgUgGIhKBBQgpAkgrgDIghgKIgEACIgRAHQgcAQgqAjIgFAEQg0AqgfAHQgrgPghgdg");
    this.shape_2.setTransform(-5.4533, 1.6336);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape_2}, {t: this.shape_1}]}, 1).to({state: []}, 1).wait(2));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-100.5, -78.6, 199.6, 217.79999999999998);


  (lib.زرالبحار = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#000000").s().p("AgtPHQgKgDgbgOQgXgLgPgBQgMgBgTAFIgfAHQgcACgogTQhSgog8hDIgoguQgYgagXgMQgOgHgngMQgigLgQgMQgTgOgZgnQgagogSgOQgOgLgjgPQgigOgPgMQgWgSgNgiQgJgWgIgpIg9kuQgLg2gBgdQgCguAPgiQAKgWAUgWQAKgMAbgaIBmhfQAYgWAPAJQAGAFAIAOQAJAMARAAQAQABANgJQASgNARgkIAhhJQAUgpAWgZQAbghAlgPQAngSAlAKQgWgqAdg/IAZgvQAQgeAHgUQAFgRAIg1QAHgsAMgYQARgiAmgLQApgLASAdQAIAMACAVIABAkQACATALAPQAMAQAQgBIAKgCQAGAAAEACQAIAEgCASIgQB6QgCASAIAGQAHAFAKgGQAIgGAFgKIBGh5QASggAHgPQAMgbACgYIAAgyQAAgdAMgQQAEgFAQgPQANgMAEgKQACgHAAgMIAAgTQABgLAHgIQAHgIAJACIARAWQgSAZAYAdQAZAdAcgNQAOAWgSA0QgTA1ANAWQAGALASAKIAbASQATAQAHAbQAGAZgGAbQgEAWgOAZQgHAOgUAfQA2ATAoAsQAoAsANA3QASAHAagiQAbgjASAEQANADAOAWIBxC4QANgUAcABQAbAAAVASQANAKAVAbQAVAaANALQAFAEA0AdQAiATALAWQAIASgCAfQgCAqABALQACALARA0QANAlgIAYQgGAPgSATQgYAZgFAIQgNATgFAeIgFA1QgMBrhKBWQhJBVhnAdIggAKQgSAHgJAKQgIAKgHAVQgIAYgGAJQgPAUgnAMQg0AQgLAGQgLAHgTASQgUASgLAHQgXAPguAHQg4AIgQAFQgLAEhVAyQgrAZggAAQgLAAgKgDg");
    this.shape.setTransform(-0.0053, 0.0279);
    this.shape._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off: false}, 0).wait(1));

    // space
    this.instance = new lib.Symbol20("synched", 0);
    this.instance.setTransform(8, 126.75, 0.2754, 0.2754, 0, 0, 0, 0, -21.6);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off: false}, 0).to({_off: true}, 2).wait(1));

    // Layer_3
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(6,0,0,0.22)").s().p("Ag4POQgUgLgSgPIgBgBIgLgKQgegegSgLQgGgEgEgCIgUADIgGAGIgRAQIgZAVIgEAAIgmgDQgRgggsgUQgogTgEgOIgKgkQgogkgHgFIgBgBIglgYQgVgNgSgRIAAAAQgbgZgdggQgPgSgNgNIgNgDIgEgBQgcgIgYgpQgngLgRgGIgLgFIgIgFQgHgFgHgJIgMgTQgXgtgahjQgahjgIgOQghg8gLhWQgIg8AAhiIgDgEIAAAAIAEAEIAEABIABABIACAAIACABIgNgOIAHgEQgDgLAIgLIAEgEIC+iVQAHgGAJABQAKAAAGAHIAcAcQAJAJAMgCQAMgBAGgLIB1jHQAGgLANgBQAMgCAJAJIAmAoIAAAAIAIg1IgEgCQgHAAACgGQgFgDgDgIQgDgJADgHQAHgRAVAGIAeAJIAFACIAFABIAGABQADABADADQAEgBAGACIAyAPIABgDQAGgIAKgBQAFgBAFACQAMgHAUAOIACgNIh7gYQgBgBAAAAQAAAAgBAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQABAAAAAAQAAAAABgBQAAAAABABIAJACQgCgGAAgTQAAggAGgiQAShkA9goIgBAAQgBAAAAAAQAAgBgBAAQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAAAABgBQAAAAAAAAQABAAABAAIBAAJIACgOIg1gHIgBgBIACgBIAGABQgNhSAtghIgBAAIgBAAIABgBIAfACIADgVQABgFAFAAQAEABgBAFIgDAVIA4ADIABACIgCABIgCAAQgTAqACAtQAAAVACANIAFABIABACIgBAAIg7gHIgCANIB5AQQABAAAAAAQABABAAAAQAAAAAAABQABAAAAABQgBABAAAAQAAABAAAAQgBAAAAAAQgBAAAAAAIgEAAQgtBTgBBeIAAA4IAUgiQAUghAOgUQAehXA+gpIgCgBIgCgCIAAgDQAAgBAAAAQAAAAABgBQAAAAAAAAQABAAABAAIBZAMIACgSIhHgKIgBgCIABgBIAIABQgRhxA/gtIgBAAIgCgCIACgBIApADIAPhxQABgEAFAAQAFABgBAFIgPBwIBRAHQAAAAAAAAQAAAAABAAQAAAAAAAAQAAABAAAAIgCACIgCAAQgbA5ADA/IACAwIAGAAIACACIgCABIhVgLIgCATICtAXQAAAAABAAQAAAAAAABQABAAAAAAQAAABAAABIAAADIgDABIgGgBQgqBPgPBWQAFgBAFAAQANAAAHAMIBQCIQAGAKANACQAMABAIgJIBAhEQAJgKAMACQANACAGAMIBQCxQADAGAEADQAEAFAIABIACAAQAOACAIgMIADgFQAIgJAMAAQAMAAAIAJIAuA9QADAFABAFQCMBEAOAOIgNAEIACAAIALADQgMAhgHBcQAHABAHADQAQAIgCAMIANAkQAOA1gMAQQgIALgSAPQgOAMgIAQQgGAMgHAbQgHAXgKAOIgDAEIAAABQgIAJgMAKIABAMIABANIgCABQgCApgRAnIAAAAQgQALhLBXIgNAOIgVAWQgxAtgyAOQgcAHgfAQIgIAFIgfASIgGADQgMA1gLAIIgKAHQgJAGgKAEIgJAEQgWAHgUgIIhBA4IgBABIABAAIgYATQgiAYgigCIgigKIgDACIgDAAIgDACIAAABQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAABAAAAQgdAOgwAnIgEAEIAAABQgOALgMAIIgSAMgAjOm3IABABQAoAjAegMQAIgDAPgVIhcgSgAjMnOIBfATIABgBQgXgKgggJQgUgGgTgEgAk3njIBiAUIACgMQgsgIgrAAIgNAAgACUqwIBIAKQgQgGgWgFQgQgEgRgCgABRq5IA5AIIABgIQgQgCgPAAIgbACgAiorXIAwAHQgXgKgYgDgAjYrdIAnAFIABgFIgUgBIgUABg");
    this.shape_1.setTransform(-0.9552, 0.6475);
    this.shape_1._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1).to({_off: false}, 0).to({_off: true}, 1).wait(2));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-109.3, -96.9, 199, 253.5);


  (lib.زرالانهار = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#000000").s().p("AAINxQgRAAgVgLQgMgGgXgQIhHgxQgKgHgGACQgFACgHAOQgOAZg1gKQgdgFgHgPQgCgFAAgLQAAgLgCgFQgFgLgQgGQgUgFgJgEQgSgHgRgYQgRgcgKgNQgPgTgcgUIgyggQgggVg0gqIiFhsQgYgTgLgLQgSgSgKgSQgKgTgFgdIgIgyQgKg6gZgzQgPgfgTgLQgIgFgMgEIgVgGQglgLgNgZQgIgPAFgQQAFgSAOgFQAGgCAOgBQANgBAHgDQAMgGAEgPQADgOgFgQIgKgcQgGgQAAgMQABghAtgkQBIg6BUguQA1gdAOgKQAkgZARgcQgFgKgTgDQgYgDgGgEQgNgJADgTQADgTAOgLQAJgHAVgKQAVgJAJgHQAVgSgGgTQgDgGgNgMQgcgagEgmQgDgaAMgXQANgYAXgFQAXgFAoATQArAUAUgBIAUgEQAMgDAHABQAVABAVAXQAdAhABAsQABAPADAEQAGAJAMAAQAOABALgKQALgJACgNQABgGgCgNQgBgMAAgGQABgTASgPQAPgNAWgDQAQgDAYACIApADQA0ABAUgcQAJgMAGgfQAGgeAKgNQAJgNASgEQASgEAPAIQAQAIAHASQAHARgGAQIgFAOQgDAIABAFQACAHAGAGIAMAJQASALBpBZQBKA/A8ARQAKgSgggUQgogagdgmQgegngPguQgVhAAWgvQANgZAogiQAhgbAVgFQASgEAkAGQAkAHASgEIAtgSQAbgLARAJQAOAHAFATQAFARgGARQgEAOgLARIgTAdQgWAmANAcQAGAMAUAUQAHAJACAKQACALgGAIQAKALATAFQAHACAbADQATACAOAIQARAJACAPQADAUgWAWQgcAag1ATQg9ATgdAMQgMAFgDAHQgCAHAGAHQAFAFAIAEQAIADAmAIQAcAGANALQAJAIAMASQAMATAIAHQAKAKARAGIAfAJQAzANA7AaQAqATBAAhQATAKAKAHQAOALAHAOQAKAVgIAnQgKAwACAQQADASAaAtQAVAngFAZQgDARgSAVQgXAcgEAHQgHAMgHAUIgKAhQgEAKgKASIgOAbQgGANgHAZQgHAbgFALQgKAZgVAaQgOATgZAaQgtAtglAdQgvAkgvASIgdAMQgRAHgKAJQgMAKgPAaQgPAagLAKQgOANgcALQggAKgQAHQgRAHgoAbQgkAYgXAHQgPAFgjAFQggAEgRAHQgTAHg3AsQgqAhghAAIgCAAg");
    this.shape.setTransform(-0.0122, -0.0088);
    this.shape._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off: false}, 0).wait(1));

    // space
    this.instance = new lib.Symbol23("synched", 0);
    this.instance.setTransform(9.1, 90.7, 0.3155, 0.3155, 0, 0, 0, 0, 1.4);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off: false}, 0).to({_off: true}, 2).wait(1));

    // Layer_3
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(10,0,0,0.22)").s().p("AhDNmQghgggTgLIgGgDIgUADIgDADIgQAQIgEADIgZAVIgqgDQgRghgsgUQgogSgEgOIgKglQgkghgLgGIgBgBQgmgVgmgiIgigiIAAABIgggkIgLgMIgHgHQgJgDgEAAIgEgBQgbgJgXgoIg2gQIgDgBIgGgCQgIgEgGgFQgHgFgGgIIgMgUQgYgsgbhkQgahjgHgNQgHgMgFgNQgdgIgMgOIgHABIgBAAIgIAAIgCAAIgCAAIgBAAIgBAAQgegDgMgHQgSgKAAgZIAAgBIAAgCQAAgEACgCIABgFIAAAAQADgHAEgGQAJgOANgGQAKgFARgCQASgBAFAGIACAAQADgBAEAAIgBgGQgIg6AAhkIAAgBIgBgBIgBgBIAAgBIABABIA1gfIAEgBIArgbIAUgKIAGgFIAAAAIAGgDIAagRIAhgUQAlgYBOg1QgMgHgGgIIgVgCQgZgEgLgHQgPgKADgWIAAgEIABgEQADgJAGgHQAJgKAMgFQAJgEAPAAQARAAADAGIACAAQAcgGAMAXQADAHAAAHQAFAYgcAAIgDAAIAFARQACAHAEAFIAIgEIAAgBIADgBIABgBIARgIIgBAAIAJgEIAYgJIACgBIgIgIQgJgOgFgOQgEgMADgIQgYgEgLgFQgegMAGgZIgDAAIgBAAIgFAAQgPgCgBgVQAAgZAWgFIADgJQgDgFAAgHIgCgFQgGADgGABIgCAAQgaAGgEgbIgBgFIAAgDIAAgEQgBgnApgGIACAAQACgJATgIQASgGANABQARgBAQAKQAKAFAGAHIABABIACACIABACIANgDQAOgEAZgLQAVgHAVAJIAIAEQAtAYgNAqQAdgDAHAkQAFAdgLAbIgGAMQAhgNAWgQQgEgEgCgJIAAgBQAAgGABgFQACgPAPgCIABgDIABABIABgFQgHgOAKgWQAJgVAOgKQANgIAVAAQAaAAAHgCQAMgEAVgKQASgFASAHQALAFAHAHIADACQApgSAXgNIgDgNIgBABIgEACIgBAAIgBACQgZAKgHgcIgBgCIAAgBIAAgDIAAAAIgBgDQgEggAigJIABgBQABgJAPgJQAOgHAMgCQAPgBAOAFQAPAHAIALQAOAUgJAUQgGANgXARIgDACIgRAKIgBALIBuA7QAtAZAeARIABAAQAfASAPAMIAGAFIACACIAFADIgBAAQARAOAkASQANgEAOADQgFgKgHgHQgJgJgXgOQgWgNgKgKQgIgJgDgRQgXgYgEgIQgMgRAFgMQgjgRgMgfQgPgkAHgmQAIguAoACQgQg4A7ggIAKgFQAcgMAdAJQAQAFAjAQQAGACAMABIADgEIAAAAIAFgFQAIgIALgHQAUgLAWAAQASgBAYAJQAaAJADANIACABQA1AGABAxQACARgIAOIAAABIgBAAIgBABIgCACIAAABIgBACQgNAOgVgDIgLgEIgBAAIgBgBIgDgBIgCAGQAAAJgEAIIADAMQAaAEADAaIAAALQgBAZgUAEIgLAAQAHAhgoAQQgOAFgfAHQAIARgdAuQgGAIgTASQgSAQgGALQgEAHAAAMIABAIQAGAFAEAHIAAABQAFACAHgDIASgIIAlgKQAYgGAMgIQAOgHAGgTQAEgQAEgHIgFAAQgkgBAFgcIAAgDIAAAAIAAgBIABgCQAAgGADgHQAPgjAnAKIACAAQAGgJAVABQATAAAMAEQARAIAMANQAIAKADAKIACAKIAAABQADAdgTAOQgOALgiAEQgQACgKAAQgOATgjALQhAANgVAIQgHADgFAFIAGACQBrArA2A7IAAABQABAOgBAWIA2gJIAnASIAeARIAHADIADABIAMAHIAeARQBpA5AnAqQgLAhgIBbQAIABAHADQAPAIgBANIANAkQAOA0gMAQQgIALgSAQQgPAMgHAQQgGANgIAaQgGAWgKAPIgDAEQgHAIgNAKIAAASIABAIIgBABQgDApgQAnIgBABQgQAKhLBYIgMAOIgVAWIgBAAQgxAtgxAOQgdAHgeAQQgWALgYAPQgMA0gLAJQgXARgRAEQgPAFgPgDIgLgDIg5AzIgBAAIgWAUQgmAdgmgCIgigKIgDABIgMAHQgdAOguAmIgEAEIgXASQgKAIgIADQgWAPgUAEQg0gRgkgmgAtTCNIgDABIACADQALAIAMAAIgIgXIgBgCIgBgDQgCAKgKAGgAo2lFIgDABIABACQAIAHAKACIAAgBIAAgBIADgCIgGgRIgBgEQgDAJgJAEgAnwmQQAKAhAOAPQAYgHAUgBIABgOQAEgRABgJIABACQAHgFAcgLQgIgDgGgGQgWAOgSAEQgUAGgZgDIgLgBgAIenfIgJAbQAQAAAMgMIADgDIgEgCQgLgEgFgNIgCAHgAEko9QAFAnACAEQAHAMAWALIAkAQQBGAkAghxIAAgFIgQACQggAFgagHQgXgHgegSQgUATgcgDIABAJgAECpJQADALADAGQAEAIARAOQABgWgBgKIAAgFQgLgDgMgEIgFgCIABAHgAAnpvIABgDIgDABIgBAAIADACgAmHuQIgCgBQgDgBAAgFIABgCQADgEAEACIAAgBQAEAAACACIABADIAAACQAAAEgDABIgBACg");
    this.shape_1.setTransform(0.2012, -3.6);
    this.shape_1._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1).to({_off: false}, 0).to({_off: true}, 1).wait(2));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-105.7, -96.1, 208.2, 260);


  (lib._1 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1_copy
    this.instance = new lib.LIPHT();
    this.instance.alpha = 0.3086;
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(35).to({_off: false}, 0).to({
      scaleX: 1.4623,
      scaleY: 1.4623
    }, 3).to({scaleX: 2.6952, scaleY: 2.6952, alpha: 0.1211}, 8).to({
      scaleX: 4.6788,
      scaleY: 4.6788,
      alpha: 0.0586
    }, 17).to({scaleX: 5.8458, scaleY: 5.8458, alpha: 0}, 10).wait(1));

    // Layer_1_copy
    this.instance_1 = new lib.LIPHT();
    this.instance_1.alpha = 0.3086;
    this.instance_1._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(12).to({_off: false}, 0).to({
      scaleX: 1.4623,
      scaleY: 1.4623
    }, 3).to({scaleX: 2.6952, scaleY: 2.6952, alpha: 0.1211}, 8).to({
      scaleX: 5.8458,
      scaleY: 5.8458,
      alpha: 0
    }, 40).to({_off: true}, 1).wait(10));

    // Layer_1
    this.instance_2 = new lib.LIPHT();
    this.instance_2.alpha = 0.3086;

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({scaleX: 1.4623, scaleY: 1.4623}, 3).to({
      scaleX: 2.6952,
      scaleY: 2.6952,
      alpha: 0.1211
    }, 8).to({scaleX: 5.8458, scaleY: 5.8458, alpha: 0}, 37).to({_off: true}, 1).wait(25));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-710, -710, 1420, 1420);


  (lib.جبال = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#996424").s().p("AgEAtIgEgDQgBgCAAgEIAAgEQABgFADgDIADgCIAEAAQAFABACADIAAAAQACADgBAFIgBAEIgCAFQgDADgEAAIgEgBgAgZALQgFgEABgFQACgDAFgBIABAAQAEgBADADIABAEIgBAEIgDADIgBABIgHgBgAgCgOQgGgIAFgJIADgFQAGgHAHgBQAKgCAEAHQAFAFgDAHQgBAHgGAFQgDAEgFACIgGABQgGAAgEgGgAgBgdIAAABIAAAGIACAAQAAAAAAgBQABAAAAAAQAAgBgBAAQAAgBAAAAIAEAAQAAgBAAAAQAAgBAAAAQAAAAgBgBQAAAAAAAAIgDgCIgBAAIAAABIgBAAIAAAAg");
    this.shape.setTransform(-17.1727, -52.9276);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#4E2404").s().p("AgOAvQgFgDAAgIQAAgEACgEIAAAEQAAAFABACIAEACQAHADAFgFIABgFIABgDQABgFgCgDIAAgBQAEACACAFQADAGgDAGIgFAHQgCADgGAAQgFgBgDgDgAgdAXIgBgBIgDgEIAAgBIABgDIACAAQAAAAAAAAQABABAAAAQAAAAAAAAQAAAAAAAAIACADIAAACIAAAAIAAACIgBABgAAZAVQgBAAAAAAQAAgBAAAAQAAAAAAgBQAAAAAAgBIACgCIADgBQAAAAAAgBQABAAAAAAQAAAAAAgBQAAAAgBAAIgBAAQAAAAgBAAQAAAAgBAAQAAAAAAABQgBAAAAAAIgCABIgCAAQAAAAgBAAQAAgBAAAAQAAAAABgBQAAAAAAgBIACgCIADgCIAGABQAEADgDAFIgDAEIgDABIgCgBgAgNgIQgEgHABgKQABgIAHgHQAGgHAIgCQAHgCAIADQAHADADAGQAEAIgFAKQgEAIgIAGQgJAHgJAAQgIAAgFgIgAAFgwQgGACgHAGIgDAFQgFAKAGAIQAHAIAJgEQAFgBADgFQAGgEABgHQADgHgFgGQgDgFgHAAIgEAAgAgJgZIAAgHIAAgBIABAAIABAAIABAAIADABQAAABAAAAQABAAAAABQAAAAAAAAQAAABAAAAIgEAAQAAABABAAQAAABAAAAQAAABAAAAQgBABAAAAg");
    this.shape_1.setTransform(-16.3769, -52.59);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#4E2404").s().p("AgJBOIgBgBIAAAAQAAgBABAAQAAAAAAAAQAAAAAAAAQAAAAAAABIAAgBIADAAIABABIABAAIgBACgAgCBJIgGgIIgCgDQgCgDAAgHIgBgHIACgEIAEgBIACgCIADgEIACgBQAGgHAJAAQAGAAACADQACACgBADQAAALgCAIQgCAIgCACQgCAEgGACQgGAEgEAAIgCAAgAgHA2IAEAIQADAFAAAAIAEAAIAHgEQADgBACgGIAEgOIgDgBQAAAFgDACIgEACIgEAAIgHAAQgEgCgBgEQgDAGACAEgAANAgIgBABIgFACIgFADIgBACIgDACIACACIAEABIACAAIAEgBQAAAAAAAAQABAAAAgBQAAAAAAAAQABgBAAAAIAAgBIABgBIACgEIABgCIgBgCIgBAAIgBAAgAgcA4IAAgBIACgBIADAAIABAAIAAACIgBADIgCABIAAABIgBABQgCgEAAgCgAgWARQgBAAAAgBQgBAAAAAAQgBgBAAAAQAAgBAAAAQgDABgCgDQgBgEAAgDIABgFIADgEQADgDADAAQAEAAAEAFIADAEQACAEgBACIgCABIAAAEIgEAAIAAAAQACgCAAgFIAAgBIAAAAIAAgBIgBgCIAAgBIgCgBIgEgCQAAAAgBAAQAAAAgBAAQAAABAAAAQgBABAAAAIgBAAIgDAFIgBABIABABQADAEAEACIADABIABACIgCABgAALgPQgBAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBIABAAIAAgBIACAAIABABIAAABIAAAAIgBAEgAgBgcIAAgBQgHgJADgUIADgJIAAgBIADgDQADgCAEACIAEAEQAGAEAEgFQgBgEgEAAIgGAAQgBAAgBgBQAAAAgBAAQAAgBAAAAQAAgBAAAAIAAgCIAFgBQAGABAGADQAHAFAAAHIAAADIgBAEQgGATgMAJIgCABIgDACIgBAAQgEAAgEgEgAAChBIgCADIgBAEQgCAFAAAIQgBAFACADIAEAEIAEAEIADgBQAJgHAEgJIAEgMIACgEQgBgBAAAAQgBgBAAAAQgBAAAAAAQgBAAgBAAIgCACQgLAAgDgDIgDgBgAAKgoIgBgFIABgCIACgBIAAAAQAEAAABABIABAEQAAAAgBABQAAAAAAAAQAAABgBAAQAAAAgBAAIgBAAIgBABIgCAAIgBAAg");
    this.shape_2.setTransform(-21.15, -46);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#996424").s().p("AgBBHQgBAAgDgEIgEgIQgCgFADgGQABAEAFACIAHABIADAAIAFgDQACgCABgEIACABIgEANQgCAGgDABIgHAEIgCABIgBgBgAgNBAQgCgEABgDQABAGACAEIABACIgDgFgAACAxIgDAAIgCgDIADgCIAAgCIAGgCIAEgDIABAAIADABIAAACIgDAEIgBABIAAABQAAABAAAAQgBAAAAABQAAAAAAAAQgBABAAAAIgDAAgAgDAmIABgBIgBACIgEAEIAEgFgAgVARQgFgBgDgFIAAAAIAAgCIADgFIADAAIACgBQADABACACIABACIAAABIABABQgBAGgCABIAAAAIgCACIgCgCgAALgWQAMgKAGgTIABgEQAAAGgDAIQgEAJgFAFQgEAFgFABgAAAgdIgDgEQgCgDABgFQAAgHACgGIABgDIABgEIADgBIACABQAEADAKAAIADgBQAAgBABAAQABAAAAAAQABAAAAABQAAABABAAIgBAEIgFAMQgEAJgJAHIgCABIgFgEgAAIgrIgBACIABAFQAAAAABAAQAAAAAAAAQABAAAAAAQABAAAAAAIACgBIABAAQAAAAAAAAQABAAAAAAQAAAAABgBQAAAAAAgBIgBgDQgBgCgEAAIAAABIgBAAIgBAAgAgIgfQgDgGABgJQABgKAFgHIgDAJQgDAVAHAIQgDgCgCgEgAAKg/IgEgDQgEgCgCACIADgDIADgCQAAABAAAAQAAABABAAQAAAAAAABQABAAABAAIAGABQAFAAAAADQgDADgCAAQgBAAAAAAQgBAAgBAAQAAAAgBgBQAAAAgBgBg");
    this.shape_3.setTransform(-20.9625, -46.4417);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#4E2404").s().p("AAjA7IAAgEIACgCIACACIACABIgCACIgCACgAAcAvIgNAAIgEgBIgEgCQgBgDACgGIACgFIACgDIAAgBIAAgDQACgEAFgCIAFgCIAEgBIAGgBIABgBIAEgCQADgBACAAIACAFQADADAAAEIgCAFQgBAHAAAGIAAAEIgCACQgEADgIAAIgEgBgAAmAhIgOAKQAHADAJgDIABgBIAAAAIAAgEIACgDIAAgFIAAgCIgFAFgAATAVQgDADAAACIAAADIgFAIIgBADIAAABIABAAQAAAAAAABQABAAAAAAQAAABABAAQAAAAABAAIAFgBIAGgGIAHgDIAEgEIAFgCQACgEABgGIAAAAIgBgEQgBAAAAgBQgBAAAAAAQAAAAgBAAQAAAAgBAAIgBABQAAABgBAAQAAABAAAAQgBABAAAAQAAABAAAAIgCAAQgIAAgHAEgAgRATIgGgBQgEgCgBgEQgBgBAAgEIAAgCIAAgDQABgDAEgEQAGgDAIABQABAAAAAAQABAAAAABQABAAAAAAQABABAAAAIAAADIACACIAAAIQgBAEgCAEIgDADgAgPgCIgDACIgDAAIgCACIgBACIgBAEQAAADACACIACACIAIAAIAEgIQAAgBABAAQAAgBAAAAQAAgBAAAAQgBAAAAgBIgBgBQgBAAAAgBQAAAAAAgBQAAAAAAAAQAAgBAAAAIgDgBIgBAAgAAEAAIgCAAIAAgEIACgBIABAAIAAAAIABABIABACQAAAAABAAQAAAAAAABQAAAAAAAAQgBAAAAABIAAAAgAgjgPQgEAAgCgDQgDgCgCgEIgCgFIABgFQAAgEAEgCIACgBIAAgDQAAgFAEgEQAFgGAHAAQAEAAAFACIAHACQACACABADIAAACIACAIQAAAAABAAQAAAAAAAAQABAAAAAAQAAgBABgBIAAgDIAAgBQABAAAAAAQAAAAAAAAQABAAAAAAQABABABAAQAAAJgHAMIgEAEIgFABIgIABIgHACIgFABIgCAAgAgkgmIgCACIgDACQgCADACAEQABADACADQADACAGgCIAPgEQAGAAACgGQgGAAgCgDQAAAAAAAAQgBgBAAAAQAAgBgBAAQgBAAAAAAQgDABgDgBIgFAAIgCgBIgFgCIgBABgAgKgnQgBgBAAAAQAAgBAAAAQAAgBABAAQAAgBAAgBIAAgCIAAgEQgBAAAAAAQAAgBgBAAQAAAAAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAAAAAAAQgBgBgEgBIgCAAIgBAAIgHADQgBAAgCAFIgCACIAGAAIAGADIAGgBIAJADgAAKgjIgBgBIgCgBQgDgFAAgDIABgCIADgBIAAAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAAAABIABACIACACQACACgBAEIgCABg");
    this.shape_4.setTransform(-25.1646, -38.65);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#996424").s().p("AAbAwIAOgKIAEgFIABADIgBAEIgBAEIAAADIgCABQgEACgEAAIgHgCgAANAuIAAAAIAAAAIAAgDIAFgIIAAgEQABgCADgCQAIgFAJABQAAgBAAAAQAAgBAAgBQABAAAAgBQAAAAABAAQADABACADQAAAFgDAEIgEACIgEAEIgIAEIgFAFIgFABQgBAAAAAAQgBAAAAAAQgBAAAAgBQAAAAgBgBgAAOAgIAAAAIgBAEIABgEgAAjASIADAAIgBAAIgHABIgDABQAFgCADAAgAgVASQgCgCAAgDIACgEIABgCIACgDIADAAIADgCIAEABQAAABgBAAQAAABAAAAQAAABABABQAAAAAAAAIACABQAAABAAAAQAAABAAAAQAAABAAAAQAAAAAAABIgEAIIgJAAgAgbAMIABgEIAAACIAAADIgBgBgAgtgUIAAgEIACAGQACAEACACQgFgCgBgGgAgjgQQgCgCgBgEQgCgDABgDIADgDIADgCQABgBAEACIADACIAFAAQADAAACgBQABAAABAAQAAABABAAQAAAAAAABQABAAAAABQACACAFAAQgBAGgGAAIgQAEIgEABQgBAAAAAAQgBAAAAAAQgBAAAAgBQgBAAAAAAgAgQglIgGABIgHgDIgFABIABgDQACgEACgBIAGgCIABgBIACAAQAFABAAACQABAAAAgBQABAAAAAAQABAAAAAAQABABAAAAQABAAAAAAQABAAAAAAQAAABABAAQAAAAAAABIABADIAAADQgBAAAAABQAAABAAAAQAAABAAAAQAAAAAAABIgIgDgAgBgjIgCgIIgBgBIACABIADADIgBAAIAAAEIgBACIAAgBgAgngnIAAADIgCABIACgEg");
    this.shape_5.setTransform(-25.4287, -39.187);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#4E2404").s().p("AAyBEIgBgBIAAAAIgBgBIAAgCIAAgBIABAAQABAAAAAAQAAgBAAAAQAAAAAAABQAAAAAAAAIABgBIACABIAAADIgBAAIAAACgAANAvIgCAAIAAgBIAAAAQAAAAAAAAQAAgBAAAAQAAAAAAgBQAAAAAAAAIADgBIAAAAIACABIgBACIgBABgAAYApIgEgBIgFgEQgFgFAAgDQAAgEADgIIABgFQAAAAAAgBQAAgBAAAAQAAgBABAAQAAgBAAAAQACgDAGABIAIACIAHAAIADAEIACAFIAAADQAAABAAAAQAAABABAAQAAABABAAQAAAAABAAIAFACQAEADgBAGQAAAEgCACIgEACIgKABIgOgBgAAVAiQgBABAAAAQAAABAAAAQAAAAABABQAAAAAAAAIADAAIALABIABAAIADAAIAMgDIgBgGIgDgCIgGgBIAAgBIgBAAQgDgBgCACIAAADQAAACgDADIgFgBIgGABgAASALIAAABIAAADQAAADgBACIgBABIgBADIgBAEQABABAAAAQAAABABAAQAAAAAAABQAAAAAAAAIABAAIAGgCQAEgCADACIACgFQABgDAEgBIABgBQABAAAAgBQAAAAAAAAQAAgBAAAAQAAgBgBgBIgDgEIgJgBIgDgBIgBAAQgBAAAAAAQgBAAAAAAQgBABAAAAQgBAAAAABgAA9AXIgBgDIAAgCIAAgBIACgBIABABIABABIABABIgBADIgBACgAAvAMIgBgDIgBgGIAAgBIABgBIACgBIAEABIgBABIgCACIgBADIAAADIACACIgCABIgBgBgAgIAEIgCgBIgCgBIgBgCIAAgCIAAgFIABgDQAAgEACgCIACgBQADgBACADIABABIABABIABABIgCABIgCgBIgDAAIgBABIgCAEQgBAEACACQACACADgBQADAAABAAQgBgDAAgEIABgEIABAAQABAAAAAAQABAAAAAAQAAAAABAAQAAABAAAAIgBAEIABADQACADgDACIgCABIgDABgAgkgSIgMgBIgDAAQgEgCgDgDIgFgJQgBgDABgHIADgKQACgEAJgHIAGgDQAIgCAJAKQAEADABAEQACADAAACIgBADIgBACQgBAAAAABQAAAAAAABQABAAAAABQAAAAAAAAIABACIAAACIABABIABAAIABAEIgBAEIgCABIgEAEIgCAAIgCAAIgCgCQgBAAAAAAQAAAAAAgBQABAAAAAAQAAAAABgBIACgBIAEgCQAAAAAAAAQABAAAAAAQAAgBAAAAQAAAAAAgBIgBgCIgCgDIAAgEIgBgBQgDACgGAAIgGgBIgEgCIgLgKIgCAFIgBAGIgBAEIABADQADAGAJAEIAQADQAAABAAAAQAAAAAAABQAAAAAAAAQgBABAAAAgAgpg/IgHAFQgEACAAACQAAACAEADIAKAGIAKgBIADgBIABgDIABgBIgDgEIgGgFIgCgCIgCgDIgCAAg");
    this.shape_6.setTransform(-26.725, -30.2924);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#996424").s().p("AAlAzIgLgBIgDAAQAAAAAAAAQgBgBAAAAQAAAAAAgBQAAAAABAAQAFgCAGABQADgCAAgDIAAgDQACgCADABIAAAAIABAAIAAACIAGAAIADACIABAHIgMACIgBgCIgBABIgBABIgBAAgAASAkIAAgCIABgBQABgBAAgDIAAgEQACgCADgBIADABIAJABIADAFQABAAAAABQAAAAAAABQAAAAAAAAQAAABgBAAIgBABQgEABgBADIgCAFQgDgCgEACIgGACIgBgIgAAyAXIAAgCIABgEIACgCIABAAIAAAAQAAAAABAAQAAAAABABQAAAAABAAQAAABABAAIABABIAAACQAAACgDACIgCABQAAAAgBAAQAAAAgBgBQAAAAgBAAQAAAAgBgBgAgIAQIACABIAGAAIADgBIgBAAIgDACIgCABQgCAAgDgDgAgHAMQgCgCABgFIACgDIABgCIADAAIACACIABgCIgBAAIAAgBIABAAIACACIgBABIgCADQAAAEACADQgCABgCABIgCAAQAAAAgBAAQAAgBgBAAQAAAAAAgBQgBAAAAAAgAgMAJQgBgEACgDIABAAIgBADIAAAFIAAADIgBgEgAgsgDIgFgDIADABIAMABIAEAAQAAAAABgBQAAAAAAAAQAAAAAAgBQAAAAAAAAIgQgEQgJgEgDgGIgBgDIABgEIABgFIACgGIALAKIAEACIAGABQAGAAADgCIABABIAAAEIACADIABACQAAABAAAAQAAABAAAAQAAAAgBAAQAAAAAAABIgEACIgCAAQgBABAAAAQAAAAgBAAQAAABAAAAQAAAAABAAIACACIACAAQgDAEgDABIgGABQgEAAgEgBgAgSgWIAAgCIgBgCQAAAAAAAAQAAgBgBAAQAAgBAAAAQAAgBABAAIABgBIABgDQABAGgBAGgAgugkQgEgDAAgCQAAgCAEgCIAHgEIADgBIACABIACACIACACIAGAFIADAEIgBABIgBADIgDACIgKAAg");
    this.shape_7.setTransform(-26.925, -31.6);

    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#592B06").s().p("AAbA/IgCgBIgBgDIABgCIACAAIAAgBIACABIABAAIAAACQAAAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAAAIgBACIAAAAIAAABgAA4A4IgBgBIgHAAIgCAAIgDAAIgCgDQAAAAAAgBQAAAAAAgBQAAAAAAAAQAAgBAAAAIABgDIAAgDIACgDIAEgEIAFABIADACQADABABADIAAAAQgDACgDgCQgEgCgBAAQgDABAAAEIAAAFIAKAAIABgDQAAgBAAAAQABAAAAgBQAAAAABAAQAAAAAAAAIACABIABABIgBABIgBABIAAACIAAACQgBAAAAABQAAAAgBAAQAAAAAAAAQgBABAAAAIgBAAgAA9AhIAAgCIgBgBIAAgBIgCgCIAAgHQAAAAAAAAQABgBAAAAQAAAAAAgBQABAAAAAAQAHgEAJACIAEADIABAEIAAADIgCADIAAADQAAABgBAAQAAABAAAAQgBABAAAAQAAABgBAAIgDABIgCABIgBAAIgCAAQgFAAgCgFgAA+AYIAAAAIAEAEIABABIAAACQAAAAAAAAQAAABAAAAQAAAAABAAQAAAAAAAAIADAAIADgCIABgBIABgCIAAAAIACgFQAAAAgBAAQAAgBAAAAQAAAAgBAAQAAAAgBAAIgDgBIAAgBIgDAAQgFAAgCAFgAAgAjQgBAAAAAAQgBAAAAgBQAAAAgBgBQAAAAAAgBQgEgIAEgHIAFgJQAFgFAHAEQAGAHAEABQABAFgCAEQgBAFgDACIgBAAIgJADIgDABIgDABIgDgBgAAmALIgBABIgBACIgBACIgCACIgCADIAAAEQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAAAIABABIABACQAAAAABABQAAAAABAAQAAAAABAAQAAgBABAAIAKgEIADgCIAAgEIABgCQAAAAAAAAQAAgBAAAAQAAAAgBgBQAAAAAAAAIgDgBQAAgDgEgBIgCAAIAAgCIgDACgAgQAKIgDgBIgDgDIgCgDIABgDIAEgDIADgCIADgBIABgBIAAAAIACgBIAGAAIACABQAAAAABAAQAAABAAAAQAAABAAAAQAAABAAABIAAAFQAAAEgGADQAAABgBAAQAAAAgBABQAAAAgBAAQAAAAgBAAIgBAAIgEgBgAgMAAQAAAAAAAAQgBAAAAAAQAAAAAAAAQAAAAAAAAIgDABIgCACIAGACIACAAIADgCIACgDIAAgEgAgzgSIgHgBIgIgBQgFgBgDgFIgBgCQgDgFAAgLIgCgFIAAAAIABgIIADgEQAAAAAAAAQABAAAAAAQABgBABAAQAAAAABAAIAAAAIAKAAIANABQAIACADAFQADAEABAGQAAAGgCAKIgBAEQAAAAAAABQABAAgBABQAAAAAAABQgBABAAAAQgEADgEAAgAhKgmQACAKAIADIAHABIAHABQAFABACgBIABgBIACgEIACgJQABgEgBgCIgBgDQgEgBgCABIgEACQgLAFgKABIgEgBIAAAAIAAABgAhIg2IgBAHIAAABQAAAAAAAAQABABAAAAQAAAAABAAQAAAAABAAIAGAAIAFAAIADgCIAGgCIAHgDQgCgCgFgBIgGgBIgPgBIgBADgAgSgSIgCAAIgBgDIABgBIAAgCIACgBIABgBIAAAAQAAgBAAAAQAAAAAAAAQAAAAABAAQAAAAAAAAIABADIgBAFIgCABgAgdgaIgBgBIAAgDIAAAAIACgCIABAAIACAAIABABIABABIAAABIgBADIgCAAIgDAAgAAKgbQAAgBAAAAQgBgBAAAAQAAAAAAgBQABgBAAAAIAAAAIABgBIACAAIABABIAAABIAAAAIgBADgAgQg2IACgCIABgBIABAAIABABIACABIAAABIABACIgFAAIgDgCg");
    this.shape_8.setTransform(-29.775, -25.325);

    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.f("#996424").s().p("AAzA8IAIAAIAAAAIgBABIgCAAIgFgBgABAA2IAAgBIgBADgAAyAyQAAgDACgBQACgBADADQAEABADgBIAAACIAAABIgBgBQgBAAAAAAQgBAAAAABQAAAAAAAAQgBABAAAAIAAADIgKABgAAsAnIAKgEIgFADIgDABgABIAlQgBAAAAAAQAAgBgBAAQAAAAAAAAQAAAAAAgBIAAgBIgBgCIgDgDIADgCIABAAIABgBQAFAAADAEIAAABIAAACIgBACIgDABIgDABIAAAAgAAlAjIgBgCIgBgBQgBgBAAAAQAAAAAAAAQAAgBAAAAQAAAAABAAIAAgEIABgDIACgDIACgBIABgCIAAgCIAFAAQAEACABADIACAAQAAAAABABQAAAAAAAAQAAABAAAAQAAAAAAABIAAACIgBADIgCADIgLAEIgCAAIgBAAgAgIAJIgHgBIACgDIADAAQAAgBAAAAQAAAAAAgBQABAAAAAAQAAAAABAAIAGgDIAAAFIgBACIgDACgAgJgDIABAAIgBABIgBAAIABgBgAgvgTIgHAAIgHgBQgHgDgCgKIgBgBIABAAIAEAAQAJAAAMgGIADgBQADgCADABIACAEQABABgBAEIgDAKIgCADIgBABIgDABIgEgBgAhHgWQgJgIADgLIAAgDIACAFQAAAKADAFIABACgAhDgoQAAAAgBAAQAAgBgBAAQAAAAAAAAQgBgBAAAAIAAgBIACgHIAAgCIAQAAIAGABQAEABACACIgGADIgHADIgDABIgEABgAg7g5IgJAAIADgCQAEgCAHACIAJADIgOgBg");
    this.shape_9.setTransform(-30.1022, -25.7625);

    this.shape_10 = new cjs.Shape();
    this.shape_10.graphics.f("#592B06").s().p("AA8BaIgCgBIgBgCIACgCQADgDAEgBIADACIgGAGIgBABgAAHBZIgCgFQAAgBAAgBQAAgBABAAQAAgBAAAAQAAgBAAAAQABAAAAAAQABgBAAAAQAAAAABAAQAAAAABAAIAAgBIACABQABAAAAABQAAAAAAAAQAAAAAAABQAAAAgBAAIAAACIgBACIgBABQAAAAAAABQAAAAAAABQAAAAgBAAQAAABAAAAIgBABIgBAAgAAuBIIABgBQAAAAAAgBQABAAAAgBQAAAAABAAQAAAAABAAIABAAIABABIgDAGIgBACgAA3BJIAAgFIgBgCIgBgDIAAgKIABgEIAAgFQAAgGAEgCQAAgBABAAQAAAAABAAQABAAAAAAQABAAAAAAIgCgDIgBgBIABAAQADAAAEAEQAFAEADAAIADABQAAABABAAQAAAAAAABQAAAAAAABQAAABAAAAQAAADgCADIAAABIgCACIgFADIgDADQgBABAAAAQgBABAAAAQgBAAAAgBQgBAAAAAAQgBgBAAAAQAAAAAAAAQAAgBAAAAQAAAAABgBIAFgFIAEgCIACgEIACgDIgHgDIgEgCIgBgBIABACQgFgCgCAFIAAAHQgDAHABAEIACAGIABAEQAAAAAAABQAAAAABAAQAAAAAAAAQABAAAAAAIADgCIACgGQACgDAFgCIABACIAAAAIAAABIgBACIgDAFIgEAFIgCACIgEABQgDAAgCgDgAAtAdIgCgDIgFgFQgDgGACgJQADgEADAAQABACgCAFQgBAFAAACIADAHQABABAAAAQAAABABAAQAAAAABAAQAAAAABAAIACgBIAHgGIAAAAIAAgBQgDgCAAgEIgBgHIgDgCQAAAAAAgBQgBAAAAAAQAAgBAAAAQAAAAAAAAIAAgBQADAAADABQADADABAEIAAAHIADACQABAEgDACIgGAFIgGAEgAgEgMIgBgCIAAgBIACgBIACABIABAAIABAAIABgBIABgBIABgBIAAACIAAAAIgCADIgDABIgDAAgAgfgQIAAgBIgDAAIgDgCIgBgCIAAgEQAAgFACgBIAGgEQABAAAAAAQABAAAAAAQABAAAAAAQAAAAABAAIACADIgHADIgCACIAAAEQADACADAAIAEgCIAEgDIABgDIABgBIACACIABADIgDAEQAAAAAAABQAAAAgBAAQAAABgBAAQAAAAgBABIgEABIgCABIgCAAIgDAAgAAHgVIgGgCIgIAAIAAgCQAAAAAAAAQABgBAAAAQAAAAAAAAQABAAAAAAIAAgBIAHABIADAAIACABIABACQABAAAAABQAAAAAAAAQAAAAgBABQAAAAAAAAgAgxgkQgFAAgDgCIgCgCIgCgDIgGgFQgDgCgBgDIAAgBIgBAAIgDgBQgBAAAAAAQgBgBAAAAQgBgBAAAAQAAgBAAgBQAAgBAFgFIADgFIADgFIAGgGIADgCQAHgFAEAAQAFgBAGABQAGACADAEIAEAKIABAOQgBAIgEAFIAAAAIAAABQAAAAAAABQAAAAgBABQAAAAAAABQgBAAAAABQgFAEgLAAIgEAAgAhCg1IAFAFQAFACAAADQADADAIgBIAIgBIgDgDIAJgDQAAAAAAgBQAAAAAAgBQAAAAABgBQAAAAABAAIADgEQAAAAAAgBQAAAAAAgBQAAAAAAAAQAAgBgBAAQAAAAAAAAQgBgBAAAAQgBAAAAAAQgBAAAAAAIgHADQgFACgKAAIgOAAgAgxg7QAEAAAIgDQAGgEAFACIgCgJQAAgEgDgDQgCgCgGgBIgEAAQgDAAgEACQgFACgEAEIgGAIQgEAFgDACQACgBAEABIAGABIAGgBg");
    this.shape_10.setTransform(-32.6833, -18.52);

    this.shape_11 = new cjs.Shape();
    this.shape_11.graphics.f("#996424").s().p("ABFBJIADgEIAAgCIAAACQgBAIgGACIAEgGgAA5BMIgBgEIgCgHQAAgEACgHIABgHQACgFAEACIAAgCIABABIAEADIAAABIADADIABABIgCAEIgEACIgFAGQgBAAAAAAQAAABAAAAQAAAAAAABQAAAAABAAQABABAAAAQABAAAAAAQABAAAAAAQABgBAAAAIAEgDIAFgEIABgBIAAABIgCAFIgBACIAAgCQgGADgBADIgDAGIgCABIgCAAIgBAAgAA1BJQgDgEAAgDIAAgEIAAgEIgBgJIABgGQACgFAGAAIADADQgBgBgBAAQAAAAgBAAQAAAAgBABQAAAAgBAAQgDACAAAGIAAAGIgCADIAAAKIACAEIAAABgAAlAeQgDgCgBgCIAAgGIACgJQACgHAKABIAAAAQAAAAAAABQAAAAAAAAQAAABABAAQAAAAABAAIADADIAAAHQAAAEADACIAAABIgGAGIgDABQgBAAAAAAQgBAAAAAAQAAgBgBAAQAAgBAAAAIgDgIQgBgCACgFQABgEgBgDQgDAAgCAFQgDAIAEAHIAEAFIgEgCgAgHgJIgCgBIgCgDIAAgHQAAgDAEgCIADAAQADAAAEAEIABABIgDgBIgIAAIABAAQgBAAAAAAQgBAAAAABQAAAAAAAAQAAAAAAABIAAABIAIABIAGABIgBABQgBADgCACIABgBIAAgBIgCAAIgBACIAAAAIAAABIgDAAIgCgCIgCACIAAAAgAgigRIAAgFIACgBIAGgEIgCgCIABAAQAFABACACIABADIgCADIgDADIgFACQgCAAgDgCgAhCgmQgGgDgCgHIABAAIABAAQAAADAEADIAFAFIACACIgFgDgAg6gmQAAgDgFgCIgFgFIAAAAIAPAAQAKAAAEgCIAHgDQABAAAAAAQABAAAAAAQAAAAABAAQAAAAABABQAAAAAAAAQAAABABAAQAAAAAAABQAAAAAAABIgDADQgBAAgBABQAAAAgBABQAAAAAAABQAAAAABABIgKADIADACIgIACIgDAAQgGAAgCgDgAgyg2IgFgBIgHABIgGgBQgEgBgCABQAEgCADgEIAGgIQAFgFAEgCQAEgCAEAAIADABQAGABADACQADADAAAEIABAIQgEgCgHAEQgHADgDAAIgBAAgAg+hLIgGAGIgEAFQAEgHAGgEg");
    this.shape_11.setTransform(-32.5123, -19.075);

    this.shape_12 = new cjs.Shape();
    this.shape_12.graphics.f("#592B06").s().p("ABCBTIgBgCIgFgHIgDgEIAAgBIgBgGIAAgEIACgJQAAAAAAgBQABAAAAgBQABAAAAAAQABAAAAAAIAFABIAGgCQAEgCAEACQAAAAABAAQAAAAAAABQABAAAAAAQAAABABAAIABAEIACAFIABACIAAAGQAAAEgGAHIgDADIgFAFIgBAAQgDAAgDgCgAA9A6IgBAEQAAADADAEIAIALIAEgDIAEgFIADgGQABgEgFgHIAAgCQgCgCgEACQgEACgHgBIAAAEgAA7AWIgCgDIAAgDIAAgJIACgGIAEgCQAGgFAGAEIADADQAGAFABACIgDAIIAAADQgEACAAABIgFABIgCABIgBABIgBAAIgCABQgEAAgEgEgABDACIgEAFIAAABIgBAGIACAEIACABIAJAAIACgBIACgCIAAgCIABgDQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAAAgBAAIgCgBIgDgDIgBgCIgCgBIgBAAIgCABgAAXAJQgEgBgFgGIgBgBIgBgCQgCgFACgFQACgGAEgCIAKgCIAGAAQAEABAGAFQAEADABADQACAEgDADIgEAFIgDADIgEABIAAAAIgHACIgEAAIgDAAgAAUgOIgDACQgCADABADIABADIABACIADADQABABAAAAQAAABABAAQAAABABAAQAAAAABAAIAFgBIAIgDIAHgFIAAgBIABgCIgBgCIAAgBIgGgDQAAACgCAGIgDAEQAAAAgBAAQAAAAgBgBQAAAAAAgBQAAAAAAgBIACgJQAAAAgBAAQAAAAgBgBQAAAAgBAAQAAgBgBAAIgDAAQgEAAgDABgAgmgTIAAgEQAAgFABgCIAFgDIABgBIACgBIACAAIAAAAIACAAIAEABIADABIABABIABADIAAAGIgBAEQgBADgHAAIgKABQgBAAAAgBQAAAAgBAAQAAgBAAAAQgBgBAAgBgAgdgdIgCABIgCABIgBADIABADIAKAAIACgBIAAgCIABgEIgBgBIgBgBIgEAAgAhIgkIgBAAIgBgCIgFgDQgDgDgCAAIAAgBIgBAAIgBAAIAAgBIAAgLQgBgEACgDQABgFAHgHIAFgEIAFgDQAEgCACABIAFACIADADIAHAHIABAAIACACIABADIgBAJIgDAGIAAABIACgBIABACIgJAKQgEABgGADIgEABIgGgBgAg4hFIgGAIIgJAIQgFAEgCADIAFAEQAJAFAJgHIACgCQAJgJgBgNIgIgGgAhAhPIgHAEIgHAHQgFAGABAMIAGgGQAKgFACgFIACgEIAFgEQAAAAAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAIgCgCIgCAAIgDAAg");
    this.shape_12.setTransform(-33.764, -7.4812);

    this.shape_13 = new cjs.Shape();
    this.shape_13.graphics.f("#996424").s().p("ABJBUIAFgFIADgDQAFgHABgEIgBgGIABABQACADAAAFIgDAIIgEAGQgDACgGAAgABBBSQgCgBgDgGIgDgGIACAEIAFAHIACACIgBAAgAA+BEQgDgEAAgDIACgEIAAgEQAHABAEgCQAEgCABACIABACQAEAHAAAEIgDAGIgEAFIgFADgABHAXIACgBIgEACIACgBgABCASIgCgBIgDgEIACgGIABgCIAEgDIADgBIACACIADADIACABQABAAAAAAQABABAAAAQAAABAAAAQAAABAAAAIgBADIgBACIgBACIgCABgAAVAIQgGgEgCgEIABABQAFAGADABQADABAEgBIAHgCIABAAIgCABIgGADIgBAAQgDAAgEgCgAAVABIgDgDIgCgCIgBgDQAAgDACgDIADgCQADgCAHABQAAAAABABQAAAAABAAQAAAAABABQAAAAABAAIgDAJQAAABAAAAQABABAAAAQAAABABAAQAAAAABAAIADgEQACgGgBgCIAGADIABABIAAAEIgBABIgGAFIgJADIgFABQAAAAgBAAQAAAAgBgBQAAAAAAgBQgBAAAAgBgAghgWIgBgDIABgDIABgBIACgBIAEgBIAEAAIABABIAAABIAAAEIgBACIgBABgAhUgrIgBgDIABAAIgBABQACAAADADIAFADIACACQgGgBgFgFgAhJgrIgFgEQACgDAEgEIAJgIIAHgIIACgFIAJAGQABAMgJAKIgDACQgFAEgFAAQgEAAgDgCgAhYg9QAAgFADgFIAFgFIAIgFIgFAEQgIAHgBAFQgBADABAEIAAALQgCgGAAgIgAhPhFIAHgHIAIgEIAEAAIACACQABAAAAABQAAAAAAABQAAAAAAAAQAAABgBAAIgEAEIgCAEQgDAFgJAFIgHAGQgBgMAFgGgAguhJIgHgHIgDgDQAGACADAGIABACg");
    this.shape_13.setTransform(-33.7335, -7.375);

    this.shape_14 = new cjs.Shape();
    this.shape_14.graphics.f("#592B06").s().p("ABgBNIgFgBIgEgDIgFgFQgCgDABgGIABgHIACgFQADgEAGAAIALABIAHAAQAEAAACACQADADAAAIQgBAMgEADQgCACgFABQgFACgFAAIgCAAgABbAyQgBAAAAAAQgBABAAAAQgBAAAAABQAAAAAAAAIgBADIgCACIAAAEQAAADABACIAEACIABABIACADIAEgBIALgBQAEgBABgDIAAgDIACgDIAAgDIAAgCIgBgBIgGgBQgCAGgGABQgBgEACgDIACgCIAAgCIgDAAIgJABgAAzApQgCgEABgEIACgFIADgGQABAAAAgBQABAAAAAAQABAAAAAAQABAAABAAQABAAAAABQABAAAAAAQAAABAAAAQAAABgBAAIgCABIgCAEIgCADIAAAAIgBADQgBACAEAEIAEADQAAABABAAQABAAAAAAQABgBAAAAQABAAAAgBQACgDgBgCIgDgBQgBgBAAAAQgBgBAAAAQAAgBAAgBQAAAAAAgBQABgDADgBIAEAFQADADAAACIgCAFIgCACIgBACIgEADQgIgBgEgIgAAUACIgBgCIAAAAIAAgGIgBgFIgBgDQgBgEADgDIAGgBIAHgBIAFABIABAAIAFAAIADACQADABABADIABAFIgDAJIgBACQgCADgCABQgCACgEAAIgHAAQgIAAgCgEgAAdgPIgBAHQABAAAAAAQAAAAABABQAAAAAAAAQABAAAAABIAAADIgBADQgBAAAAAAQgBAAAAABQgBAAAAAAQAAABAAAAIAHAAIADgBIAEgCIACgDIADgHIgCgEIgBgBIgBgBIgHAAQgBAAAAAAQgBAAAAgBQAAAAAAAAQAAAAAAgBQgGADgFAAQgBACACADQAAAHABADIACgCQgCgCAAgFQAAgBAAgBQAAgBAAAAQABgBAAAAQAAgBAAAAIACgBIACABgAhCAAIgBAAQgFAAgCgCIAAgIIAAgEIACgDIAKgIQAEgBADAAQAEAAACAEIAAAEIAAAEIgBAFIgBACIAAACIgCABIgBADIgCAAIgDgBQABgDACgCIADgEIAAgEQAAgBAAAAQAAgBAAgBQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAAAgBAAQgBAAAAAAQgBAAgBABIgFABIgBABIgBABIgCADQgDAEABACQABAAAAABQAAAAABAAQAAAAABABQABAAAAAAIAFABQAAAAABABQAAAAABAAQAAABAAAAQAAAAgBABQAAAAAAAAQAAAAgBAAQAAABgBAAQAAAAgBAAgAhhgUIgFgBIgCgBIgHgCIAAgBIgCgBQgFgFgBgLQgBgIADgJIACgGQABgFAEgDQAGgFAPACIABABIAQACIAGABIABAGIACAJQgCAJABAFIAAAAIABAFQAAADgEADQgHAFgJAEQgIADgEAAIgCAAgAhIg4IgEACIAAAEQAAAAAAABQgBABAAAAQAAAAAAABQgBAAAAAAQAAAAAAABQAAAAABAAQAAABAAAAQgBABAAAAIgDADIgJAGQgDACgDAHQALAAAQgMIABgBIAAgDQgCgHADgHIgCgCQAAAAAAAAQgBABAAAAQAAAAgBABQgBAAAAAAgAhmhGIgFAEQgEAFAAADIgBAEIgCAKQAAADADAGIADAGQAEACADgDIADgFQADgFAKgFQAAAAABAAQABgBAAAAQAAAAABgBQAAAAAAgBQABgCgBgGIADgDQAEgEAAgEQgGgEgQAAg");
    this.shape_14.setTransform(-35.9168, -2.1248);

    this.shape_15 = new cjs.Shape();
    this.shape_15.graphics.f("#996424").s().p("ABiBJIgEgBIgBgBIgDgDQgCgCAAgDIABgDIABgCIABgDQAAgBAAAAQABAAAAgBQABAAAAAAQABAAAAgBIAMAAIABACIgDACQgCACABAFQAGgBACgHIAGACIABABIAAACIAAADIgBACIgBAEQgBACgEABIgLACIgCgBgABKAuIABgFQAAgCgDgDIgEgFQgDABAAADQgBAAAAABQAAABABAAQAAABAAAAQABABAAAAIADACQABABgCAEQAAAAAAABQgBAAAAAAQgBAAgBAAQAAAAgBAAIgEgDQgDgFAAgCIABgCIAAAAQADgEAGgBIABAAQAEAAADACQADACABAEQAAAEgCACQgCADgBAAIgCABgAAhACIACgCIgBgEQAAAAAAAAQgBgBAAAAQAAAAgBAAQAAAAgBAAIABgIQAAAAgBAAQAAgBAAAAQgBAAAAABQgBAAAAABQgBAAAAAAQAAABAAAAQAAABAAABQAAAAAAABQAAAFABACIgCACQgBgCABgHQgDgEABgCQAFABAHgDQgBAAAAAAQAAABABAAQAAAAAAAAQAAAAABAAIAHABIACAAIAAACIACAEIgDAGIgBADIgFACIgDABIgHABQAAgBAAAAQABAAAAgBQABAAAAAAQABgBAAAAgAg/ABIADAAQABAAABAAQAAAAAAAAQABAAAAAAQAAAAAAgBQABAAAAAAQAAAAAAAAQAAgBgBAAQAAAAgBgBIgEgBQgBAAgBAAQAAAAgBAAQAAAAgBgBQAAAAgBgBQgBgCADgEIADgCIAAgBIAAAAIACgCIAEgBQABAAABAAQABAAAAAAQABAAAAAAQABAAAAAAQAAABABAAQAAAAAAABQAAAAAAABQAAABAAABIAAADIgCAEQgCACgCADIADABIACgBIABgCIACgBIAAgCIABgCIABgFIAAgDIABAAQACADgBAFQgBAEgDADIgCACIgDACIgBAAQgEAAgFgDgAAVgFIAAgDIABAFIAAAFIgBgHgAAzAAIADgJIgBgFQADAFgBAFQgCAEgDABIABgBgAhrgWIAGADIACABQgFgBgDgDgAApgTIgBAAIACgBIAEABIgFAAgAhXghIAJgFIADgDQABgBAAAAQAAgBAAAAQAAgBAAAAQgBAAAAgBQABAAAAAAQAAAAABAAQAAgBAAAAQAAgBAAgBIABgDIADgCQABAAAAgBQABAAAAAAQABgBAAAAQAAAAAAgBIADADQgEAHACAHIAAADIgBABQgQAMgLAAQAEgHACgDgAhzggQgDgGABgHQAAgGADgFQgDAJACAIQAAAKAGAGQgEgEgCgFgAhpgaIgDgGQgDgGAAgDIACgKIABgFQABgCADgFIAFgEIAFgBQAQAAAHAEQgBAEgEADIgCADQAAAGAAADQgBAAAAABQAAAAgBABQAAAAgBAAQAAABgBAAQgKAFgDAEIgDAFQgBABAAAAQgBABAAAAQgBAAAAAAQgBAAAAAAIgDAAgAg8gpQgBgEABgJIgBgJIABADQACAEgBAHIgBAJgAhVhJIgBAAIAGAAQAGABAFACg");
    this.shape_15.setTransform(-36.25, -2.3757);

    this.shape_16 = new cjs.Shape();
    this.shape_16.graphics.f("#592B06").s().p("ABKBEIgCgCIABgCIAAAAIAAgBIACAAIABACIAAACIgBAAIAAABgABqA7IgFgEQgGgFgBgEIgBgCQgBgEACgCIAEgCIAGgFQAJgHALAHQADACAAACIAAACQAAAAAAABQAAABAAAAQAAABAAAAQAAABABAAIABACIACABIgBACIAAADIgBABIgCADIgCADIgEACIgKACIgFgBgABqAkIgHAGQABAEAGAFIAEADQAEACAFgCQAFgDACgDQgBAAgBAAQAAAAgBAAQAAAAAAgBQgBgBAAAAIAAgFIAAgDIgCgCIgFgCIgCgBQgDAAgEADgABPArIAAgBIAAAAIgBgBIAAgCIABgBIABAAIABAAIABABIgBADIAAAAIAAABgAA3gOQgEAAgBgBIgCgEIgEgEQgBAAAAgBQAAAAgBgBQAAAAAAAAQAAgBAAAAIABgEQACgGAGgDIAJgBQAIAAADADQAAAAAAABQABAAAAABQAAABAAAAQABABAAAAIAAABIAAAFIABACIAAAEIAAACQgBADgDABIgGABgAA9gTIAEAAIADAAQABgBAAAAQABAAAAgBQAAAAAAgBQAAAAAAAAQAAgBgBAAQAAAAAAAAQAAgBAAAAQAAgBAAAAIgBgFIgCgDIgBgBIgCAAIgEgBIgGABIgDADIgCAEQAAABADADQABAAAAABQABAAAAABQAAAAAAABQAAAAAAABIAFgBgAh0gTIgBgEIAAgBIAAgFIAAgBIAAgDIAAAAIgFAAIgDgBQgBAAAAgBQgBAAAAgBQgBAAAAgBQAAAAgBgBIgBgDIAAgEIABgFIABgIIACgFQACgDAFAAIADAAIAKAAIABAAQAIgBADAFIABAFIADADQAAABAAAAQABAAAAABQAAAAAAABQgBAAAAABIAHACIAEAEIAAAAQAFAFgBAHQAAAAgBABQAAABAAAAQAAABgBAAQAAAAgBABQgCABgDgBIgKgDIgDgDIABACIAEAGIgDAEIgEACIgDABIgFAAIgFABIgBABIgCAAQAAAAAAAAQgBAAAAAAQAAgBgBAAQAAAAAAgBgAhtgjIgCACQgDAGAAADIABACIACABIAQgDIABgBIgBgDIgBAAIgBgCIgCgBIgCgEIgHgBgAhZgsIgCADIgBACIABACIACACIADABIADAAIAEAAIAAgDIgCgDIgCAAIgCgCIgEgBIABgBgAhpgnIAFABIABABIABABIAAgBIAAgDIACgEQAAgBABgBQAAgBAAAAQAAgBAAAAQAAgBAAAAIgCgEIgDgFQgDgFgHACIgMACQABACgCAEIgBAGQAAAFACADIAIACIABAAIABgBIACgBgAg7gVIgEgBIgCgCQgCgBAAgFIAAgEIADgDQAEgEADgBIAEAAQAAAAABAAQABAAAAAAQAAAAABABQAAAAAAABIABACIABAIQAAABAAAAQAAAAAAABQAAAAgBABQAAAAAAABQgBAAAAAAQgBABAAAAQgBAAAAAAQAAAAgBAAIAAAAQAAgBgBAAQAAAAAAAAQAAgBAAAAQAAAAABgBIABgCQAAgBAAAAQAAgBAAAAQAAgBgBAAQAAAAAAgBIgBgDIAAgBIgCAAIgBABIgGAGIgBACIABABIACACIAEABIAEABIADACQAAAAgBAAQAAAAgBABQAAAAgBAAQgBAAAAAAg");
    this.shape_16.setTransform(-36.8065, 2.546);

    this.shape_17 = new cjs.Shape();
    this.shape_17.graphics.f("#996424").s().p("ABnBAQgDgCgCgFIgDgDQgBgEABgEQACAEAFAGIAFADQAFADAKgDIAEgCIACgEIACgDIAAABQgDALgKAEIgGABQgFAAgDgDgABvA3IgEgDQgGgFgBgEIAHgHQAGgDADABIAFADIACABIAAADIAAAFQABABAAABQAAAAABABQAAAAABAAQAAAAABAAQgCADgFACIgFABIgEAAgACBAqQgBgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAgBIACAGIAAAAIgBgBgAhogQIgGgBIAFgBIAFgBIADgBIgDADIgCABgAA+gTIgDgBIgFABQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAAAgBgBQgDgCAAgCIACgEIADgCIAGgBIAEAAIACAAIABABIACAEIABAEQAAABAAAAQAAABAAAAQAAAAABABQAAAAAAAAQAAABAAAAQAAAAAAABQAAAAgBABQAAAAgBAAIgDABgABLgYIgBgDIAAgEIAAgBQAEADgBAFIgCADIAAgDgAhwgVIgBgCQAAgDADgGIACgDIABAAIAHAAIACAEIACABIABACIABABIABACIgBABIgQADgAh0gcIAAAFIAAABQgBgEABgCgAg4gYIgEgCIgBAAIgBgCIAAAAIAAgDIAGgGIACAAIABAAIABAEQAAAAABABQAAAAAAAAQAAABAAAAQAAABAAAAIgBADQAAAAgBABQAAAAAAAAQAAABABAAQAAAAAAAAIABAAIgBABIgEAAgAh5ggIAFAAIAAAAIAAAAIgCAAIgDAAgAhVghIgDgBIgCgCIgBgDIABgBIACgDIABgBIgBABIAEABIACACIACAAIACADIAAADIgEABgAhjglIgFgBIgFAAIgCAAIgBABIgBAAIgIgCQgCgCAAgGIABgGQACgDgBgDIAMgBQAHgCADAEIADAGIACAEQAAAAAAAAQAAABAAABQAAAAAAABQAAAAgBABIgCAFIAAACIgBAAIgBAAgAiBgrIAAgCIAAAEIABAEQgBgCAAgEgAhOgrIgEgEQADAAABAEgAhvhCIAHAAIgBAAIgKABIAEgBg");
    this.shape_17.setTransform(-36.925, 2.4875);

    this.shape_18 = new cjs.Shape();
    this.shape_18.graphics.f("#592B06").s().p("ABxB+IgEgBIgBgBQAAgBAAAAQAAgBAAAAQABgBAAAAQABAAABgBIAEgBIABAAIACgDIAAgCIgBgCIgCgDIAAgBIgDgDIgBAAQgBABAAAAQgBAAAAAAQAAAAAAABQAAAAAAAAIgDAAIAAgCQgBgDAEgCIAEAAIAEABIACADQAGAKgBAGIgBADIgDABIgCACgABuBKIgLgDIgBAAIgBgBQgBAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQgBgNACgHQABgIAIgEIAFgCQAEgCAGACQAFABACADIABACIABAEQACAKgBAGQgCAGgCADQgDAFgEABIgGABIgEAAgABlAvIgBACIAAACIAAAPIAPACIADgBIACgDIADgGIAAgLIgBgDIgBgBIAAgCIABgBQgDABgEgDIgCABIgCABQgBAAAAABQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAADgFADgAhlgPIAAgCIAAgBIAGAAQABAAAAAAQABgBAAAAQABAAAAgBQAAAAAAgBQACgDABgHIABgGQAAgBAAAAQAAgBgBgBQAAAAAAgBQAAAAgBgBIgFgCIAAAAQAAgBAAgBQABAAAAgBQAAAAAAAAQABAAAAgBIAEAAQAAAAABABQABAAAAAAQABAAAAAAQABABAAAAQACACAAAEQAAAJgCAFIgDAIIgEAFIgCABQgEAAgDgDgAhTgzIgCgCIgBgBIgDgBQgCgCACgEQABAAAAAAQABAAAAABQABAAAAAAQABAAABABQAEABABgBQAAAAAAAAQABAAAAgBQAAAAABgBQAAgBAAgBIACgEIAAgDIgBgCQAAgBAAAAQABgBAAAAQAAgBABAAQAAAAABAAIABAAIAEACQABABAAAAQAAABAAAAQAAABAAABQAAAAAAABIgBACIgBACIAAABIAAACIgCACIgBAEIgCABIgCADIgEABgAgmg1QgBAAAAgBQAAAAAAgBQAAAAABgBQAAAAAAgBQAGgCAAgDIAAgDIgDgFIAAAAIABgCQAAAAAAAAQAAAAABAAQAAAAAAAAQAAAAABAAIABgBIAEACIAAACIACADIABAEIgBAFIgCACIgDACIgEACIgBAAIgDgCgAhjhQQAAAAAAgBQABAAAAAAQABgBAAAAQABAAABAAIAFgDQACgCgBgEIgDgGIgDgHQgCgEgEgBIgEABIgBgBQAAgDADgDIACgBQADAAADABIAGAFIAEAAQAEgFgCgFIgBgEQAAgBABAAQAAAAABAAQAAAAABAAQAAAAABAAIACADIABACQABAHgDAEQgCAFgEABIgEAAIAAAAIAFAKQACAEgBAFQgCAFgDABIgDABIgEAAQgFAAABgDgAiAhYIgCgCQAAAAAAgBQAAAAAAAAQAAgBAAAAQAAgBABAAIABgBIACgBIACgCIABgHIgCAAIgBgDIAAAAQAAAAAAAAQAAgBABAAQAAAAAAAAQABAAAAAAQAFgBABAEIABAEIgBAJIgBACQgEACgDAAIgCAAg");
    this.shape_18.setTransform(-42.4741, 7.1056);

    this.shape_19 = new cjs.Shape();
    this.shape_19.graphics.f("#996424").s().p("ABnB+IgDgDQgCgCAAgFIABgEIACgCIAEgDIABADIADAAQAAAAAAgBQAAAAAAAAQAAgBAAAAQABAAAAAAQAEABABACIACAEIAAADIgBADIgCABIgEABQgBAAAAABQgBAAAAAAQAAABAAAAQAAABAAAAIABACIgBAAIgFgCgABiBKIAMACIgFABQgFgBgCgCgABjBFIABgPIAAgDQAEgEAFgCIAHgBIAFABIAAABIACADIAAAMIgDAGIgDADIgDAAgAB0ArIgDgCIACAAQAEACACAAIAAABIgFgBgAB/AnQgCgEgFgBQgFgBgFABIAAgBIACgIQACgFADgBQAEgCAEABQAHAAACAFIAAAGIgCAHIgEAEIgBgBgAB9AeIgCABIgEACQAEAAAEgDIAAAAIgCAAgAhqgXIACgLQABgEACgCQADgCADAAIAAAAIAFADQAAAAAAABQABAAAAABQAAAAAAABQAAABAAAAIAAAHQgBAHgCACQAAABgBABQAAAAAAAAQgBABAAAAQgBAAAAAAIgGAAIAAACQgEgCgBgHgAgkguQgBAAAAAAQgBAAgBAAQAAAAgBgBQAAAAAAAAQgDgCgBgDIgBgGQAAgEACgCIADgDIAFgBIAAAAIACAFIABAEQgBACgFADQgBAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQABAAAAABQABAAAAAAQABABAAAAQABAAAAgBIAEgCIADgCIAAACIgDADIgCACgAhag7QAAgDADgEIADgEQADgEADAAIABAAQABAAABAAQAAAAABAAQAAABABAAQAAAAABABIgBgBQgBAAAAABQgBAAAAAAQgBABAAAAQAAABAAAAIABADIgBACIgBAFQAAABgBABQAAAAAAABQgBAAAAABQAAAAgBAAQgBABgEgCQAAAAgBAAQgBgBAAAAQgBAAAAAAQgBAAAAAAQgCADACACIACACIABABQgGgCACgHgAhFg+IABgCIAAAAIgBADgAhkhKQgGgCgDgHIgCgHQgBgHABgDQABgFAEgBIAAAAIAFAAQADAAACAEIADAIIADAFQABAFgCACIgFACQAAABgBAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAAEAFAAIAEAAIgFABIgEgBgAiBhcIgDgCIgBgCQgBgCACgDIACgCIADgBIAAgBIAAABIABACIADABIgBAHIgDACIgBABIgBgBgAhZhkIAAgBIADABIgDAAgAhghwQgDgCgCABQgCgDACgEQADgDADgCQADgCADAAQAEAAADACQADADABADIgCgDQgBgBgBAAQAAAAgBAAQAAAAgBABQAAAAgBAAIABAFQADAFgFAEIgEABg");
    this.shape_19.setTransform(-42.4312, 6.8522);

    this.shape_20 = new cjs.Shape();
    this.shape_20.graphics.f("#592B06").s().p("ACRCdIgGAAIAAgEIACgEIABACQABAAADgEIACgCIABgFIADAAIAAAAIAAABIAAACIABACIAAACIgCACQAAAAgBAAQAAABAAAAQgBAAAAABQAAABgBAAIAAABIgCADIgBABgABnBKIgDgBQgBAAAAgBQAAAAAAgBQAAAAAAAAQABgBAAAAIAFgBIABgCQAAAAABgBQAAAAAAgBQAAAAAAgBQAAAAAAgBIgBgDIgBgCIgEgCQgBAAAAAAQAAgBAAAAQAAAAAAAAQAAAAABAAIABAAIABgBIAFABQACACABADIgBAFIABADIgBADIgCACIgEACIAAAAIgBgBgAB0AqIgBAAQAAAAgBgBQAAAAAAAAQAAgBAAAAQAAgBAAAAIACgDQABABAAAAQAAAAABABQAAAAABAAQAAAAABgBQgBAAAAAAQAAgBgBAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAAAAAAAQAAAAgBgBQAAAAgBAAIgBAAIgCAAIAAAAIgDAAIAAgBIAAgCQABAAAAgBQABAAAAAAQABAAAAAAQABABAAAAIABgBIAFABQAAAAABAAQAAAAABAAQAAABAAAAQABAAAAABQAAAAAAAAQABABAAAAQAAABAAAAQAAABgBAAIABADIABADQABACgDAEgAiNgSIgGgCIgBgFQgBgCABgFIgCgDQgBgDACgGIABgCQAAAAABAAQAAgBAAAAQABAAAAAAQABAAAAAAQAFAAAHAHIAAACIgCAAIgEgBIAAgCIgCAAIgCgBIgCAAQAAABgBAAQAAABAAAAQgBABAAAAQAAABAAAAIABAEIABAAIAAACIAAADIAAACIAEADIADADIABABIAAADIAAAAIgEgBgAiRhvIABgBQgEAAgCgCQgBgCAAgDIgBgEIAAgDQABgEAFgBIAGAAIACABIABACIACAHIgBAGQgBADgDABIgCAAIgDAAgAiTh9IgBAAIAAABQgCADACACIABACIAEAAQABAAAAAAQABAAAAAAQAAAAABAAQAAAAAAgBIABgBQAAgDgBgDIgBgBIgBAAgAhqiAIgBgCIgCgDIgBgGIABgGIABgCIAAAAIAAgFQABgBAFgCIAGgBIAEADQAHADAAAFIgCAHIAAADIgBACIgGAGIgBACIgCAAQgFAAgEgDgAhgiYIgBABIgFACIgCAAIAAAEIgBACQgCACABAEIACAFIAEADIACABQAEgDACgDIAAgBIAAgBIABgBIABgCIAAgDIACgCQAAAAAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBIgCgCIgDgDIgBAAIgCAAg");
    this.shape_20.setTransform(-48.15, 15.3667);

    this.shape_21 = new cjs.Shape();
    this.shape_21.graphics.f("#996424").s().p("ABaC0IgEgFQgBgCABgDIABgDIACgCIAEgCIABAAIABgBIAFADQACACAAADIgBAEIgEAFIgDACIgCAAIgCgBgACMCBQgEAAgBgBQgCgCABgEQAAgEABgDIAEgFQACgCACAAQAAAAABAAQAAAAAAAAQAAAAAAAAQABAAAAgBQADABACADIABABIgCAAIgBAEIgDADQgCAEgBAAIgCgCIgBADIAAAEIAFAAIgCABgABgAsIgFgFQgBgDABgCQACgDADAAQAAAAAAAAQAAAAAAAAQABAAgBAAQAAgBAAAAQAFABABACIACAFIAAACIgFABQAAAAgBABQAAAAAAABQAAAAAAABQAAAAABABIADABIACAAQgBAFgBACIgDACgABqAJQgBAAAAgBQAAAAgBgBQAAAAAAAAQAAgBAAAAQAAgDABgCQAAgBABAAQAAAAAAAAQABAAAAAAQABgBAAAAIAAABIADAAIAAAAIADAAIACADQABAFgFACIAAABgAiKgwIgBgCIgDgCIgDgEIgBgEIAAgCIAAgBIABgEQAAAAAAgBQABAAAAgBQAAAAABAAQAAgBABAAIADABIgBABIAFABIgCAFQgBAEABAFIAAACIgBADgAhfg9QgHgGAEgHIAGgHIAEgFQACgDADABQADABABADQABADgCAGIACALQgBACgDADQgDACgCAAQgFAAgDgEgAiTiRIgCgDQgBgCABgDIABAAIAAgBIAFAAIABAAIABAAQABAEAAACIgBACQAAAAAAAAQgBAAAAAAQAAAAgBABQAAAAgBAAgAhwiiIAAgFIAAgFIABgBIACgDIAAAAIgBADIgBAFIABAGIACAEIABABQgEgBgBgEgAhpigIgCgGQgBgDACgCIABgCIAAgEIACgBIAFgCIABAAIABAAQAEACABADQAAAAAAABQABAAAAABQAAABgBAAQAAABAAAAIAAAFIgBABIgBABIAAABQgBADgFABIgCABg");
    this.shape_21.setTransform(-48.0575, 18.2417);

    this.timeline.addTween(cjs.Tween.get({}).to({state: []}).to({state: [{t: this.shape_1}, {t: this.shape}]}, 3).to({state: [{t: this.shape_3}, {t: this.shape_2}]}, 5).to({state: [{t: this.shape_5}, {t: this.shape_4}]}, 4).to({state: [{t: this.shape_7}, {t: this.shape_6}]}, 3).to({state: [{t: this.shape_9}, {t: this.shape_8}]}, 4).to({state: [{t: this.shape_11}, {t: this.shape_10}]}, 3).to({state: [{t: this.shape_13}, {t: this.shape_12}]}, 4).to({state: [{t: this.shape_15}, {t: this.shape_14}]}, 3).to({state: [{t: this.shape_17}, {t: this.shape_16}]}, 4).to({state: [{t: this.shape_19}, {t: this.shape_18}]}, 4).to({state: [{t: this.shape_21}, {t: this.shape_20}]}, 4).to({state: []}, 5).wait(28));

    // flash0_ai
    this.instance = new lib.جبال1_1();

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(74));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-63.4, -71.2, 123.9, 142.5);


  (lib.بحارصورة = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#34D0E0").s().p("AhiBTQgKgGgFAAQgFAAgJADIgWAEQggAFgNgMQgOgNAGgXQACgGAEgGQgDgHABgIQAAgIAGgJQAOgWAbgBQAHgBAGACQAFgJAIgFQgDgMADgMQADgNAJgIQADgEADABIAEAEQACAEAAAEQAAAFgDAGQgHAOAAANIAGAAQgBgFADgFQADgFAFgBQAEgBADADQADAFgEADIgEABQgBAAAAAAQgBAAAAAAQAAAAgBABQAAAAAAABIACADQAFAEgFAGQgCACgCAAQgDABgCgCQAEAJAKAFQAEACACgBIAHgGQAEgEAGAAQAGgBAFADQAJAGADANQAFAAAKgJQAKgJAGAAQADAAAHAEQAGADADAAQAEAAAHgFQAHgEAEABQAFAAAEAFIAGAIIAGAHIACAAQADgBAKgFQATgKAUAIIAPAFIANgDQAVgIATAJIAJAEQgBAAAAAAQAAgBAAAAQAAAAAAgBQABAAAAAAQABgDAEAAIAGAAQAAgCACgCIABgEQAAAAAAgBQABAAAAAAQAAAAABAAQAAAAAAAAIADgBQABgBAAAAQABAAAAABQABAAAAABQABAAAAABQABACgBADQgBAEgGAFIgFAEIgHgBQAKAJABAOQACAKgFAKQgFAKgJAEIgLAEIgMADQgNAFgHABQgIABgKgDIgRgGQgJgCgEABIgKAFQgSANgVgIIgLgDQgDABgIAGQgLAIgTgDQgcgDgEABIgOADIgOAEIgBAAQgMAAgMgGgAAagHIABABIABABIABACQAAAAAAAAQAAAAAAAAQAAABAAAAQAAAAAAAAIABAAIAAgFIAAgBIgBAAIgCgCQAAAAAAABQgBAAAAAAQAAABAAAAQAAAAAAABg");
    this.shape.setTransform(10.167, -20.1582);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#34D0E0").s().p("AAtA6QgLgGgDAAIgKAFQgLAHgNgCQgNgCgMgHQgIgFgFgBQgEAAgLAFQgQAIgTgBQgSgCgPgJQgGgEgDgBIgNADQgNAEgNgIQgNgIgDgNQgEgPALgNQAJgOAQgFQAMgEAMABQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAgBABAAIgOAAQgIgCgCgEQgEgHADgHQACgGAFgDQAFgDAFAAQAIACAEAJIADAFIABAGIABAHIAGAGIABAFIAGACIAAgFIABgHQAAgBAFgCIAHgCQAFAAAAAEQAGgBADACQAFADgDAFIgFABIgKABIgCAAIgDAEIgDAAIAIAEQAHAEAEAAQAFABAMgIQAOgIAQABQAQABANAJQAGAEACAAQACAAAEgEQAQgNAUAHIAGADQABgDAEgDQAGgFAGgBQAIgBAEAHQACADgCADQAAAAgBAAQAAABgBAAQAAAAgBAAQAAAAgBAAQAAAAgBAAQAAAAgBAAQAAAAgBgBQAAAAAAAAIgCgBIgCgBIgEABIgFAFIAAABIAIAFQAPAJALgFQAGgDAJgJQALgJASADQANACAPAJIAGAEIACgGIAAgDQABgDAEAAIABAAIAFACIADAAQAAAAABAAQAAAAABAAQAAABABAAQAAAAABABIABADQAAAFgEACQgDACgDAAIgCAAIgCABIgBABQALAKACANQADARgNAMQgNALgQgCQgIgCgDAAIgNAEQgMAFgUABIgLAAQgZAAgMgIgAiTgwIgBACIABABIADABIAAgGIAAgBIgBgBgACOgaIgBgBIgCgBQgDgDADgCQAAgBABAAQAAAAAAgBQABAAABAAQAAAAABAAIgBAAIAHAAQACABABAEIgBACQAAAAAAABQAAAAgBAAQAAAAgBABQAAAAgBAAg");
    this.shape_1.setTransform(10.9313, -19.5607);

    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#34D0E0").s().p("ABhBFIgNgCIgOABQgIABgFgCQgJgHgGgBIgLABQgaAGgZgJIgSgFIgNAAQgIAAgFgCQgIgEgEgBQgCAAgMACQgMADgMgGQgNgHgFgLQgGAHgJADQgJAEgKgCQgJgCgHgHQgIgGgCgJQgFgOAHgRQAFgLAIgIQgBAAAAAAQgBgBAAAAQAAAAgBgBQAAAAAAAAQgBgDACgCIADgDIAPgHQAHgCAEACIAFADIAAAAQAIABAOAEIAAgJIAAgDIACgDQACgCAEgBQAAAAAAAAQABAAAAABQABAAAAAAQABABAAAAQAAABABAAQAAAAAAABQAAAAAAABQAAABAAAAIgCADQgCACABAEQAAAAAAABQAAABAAAAQgBABAAAAQAAABgBAAIgBAAIASAHQAIAFAFAAQAEAAAHgDQANgFANAGIAJAEIAJADIASADQAJACAFAIQABAEADAAQACABAEgEQALgKAPgBQAPgCAMAIQAMAIAEABIAPACQAHABAEgBQADgBAGgHQAHgIAKgEQAKgEAKACQAUADAMAWQAJARgDAQQgEAVgTAFIgWADQgFABgNAHQgLAFgHAAIgCAAIgKgCgABPgRIgCgCIgDgEQgBAAAAgBQgBAAAAgBQAAAAAAgBQAAAAAAgBQAAgBAAAAQAAgBAAAAQABgBAAAAQABAAABAAQAAAAAAAAQABAAAAgBQAAAAAAAAQAAAAAAAAIAGACIAEADIABAEQAAAEgCABIgEABgACxgWQAAgBAAAAQgBgBAAAAQAAgBAAAAQAAgBABAAIAIgLQAAAAABAAQAAgBABAAQAAAAABAAQAAAAABAAIABgBQAEgBABAFQABADgEAEIgEADIgFAEIgDABQgBAAAAgBQAAAAgBAAQAAAAAAgBQgBAAAAAAgAAXgeQAAgBgBAAQAAAAAAgBQAAAAAAgBQAAgBAAAAIABgDIACgDQAAAAABAAQAAgBABAAQAAAAAAAAQABAAAAAAIAAAAQABAAAAAAQABAAABAAQAAABAAAAQABABAAAAQAAABABAAQAAAAAAABQAAAAAAABQAAABAAAAIgDAEIgBABIgBABQgBAAgBAAQgBAAgBgBQAAAAAAAAQgBAAAAAAgACpgiIgBgDIAAgHIAAgDQAAAAABgBQAAAAAAAAQABgBAAAAQABAAAAAAIACAAIADABIACADQACAEgBABIgDAGQAAAAgBAAQAAABAAAAQgBAAgBAAQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBAAAAgBQAAAAAAAAgAiqg6IgBgCIgBgDIABgCIABgCIACgCIADgBIAEADQAAAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABIgBADIgEADIgBABg");
    this.shape_2.setTransform(13.2897, -19.42);

    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#34D0E0").s().p("Ai9BEQgTgGgEgUQgCgUAZgcQAOgRAMgIQAIgEAQgGQASgIANAAQAQAAAMAMIANAKQADABAOgBQANgCANAIQALgQATgEQAVgEAQAKQAJAFACAAIAHgDQAKgGALACIAKACIAJABQAFAAAPgFQAMgEAHADQACgLAJgIQAIgIALgBQALgCALAFQAKAGAFAJQAJAOgFAUQgDATgMALQgJAIgRAFIgfAIIgSAJQgLAGgIABQgRACgOgLQgUAMgVgJQgLgEgDAAQgEAAgMAHQgNAHgPgDQgNgCgDAAIgNAGQgLAGgNgCQgNgDgJgIIgGgHQgEgEgDgBQgLgEgLAKQgLANgGAGQgHAFgJACIgIABQgFAAgFgCg");
    this.shape_3.setTransform(13.1667, -16.9523);

    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#34D0E0").s().p("AAsBDQgTgCgVgQQgEgFgEABQgDAAgGAGQgHAHgLADQgJACgJgCQgKAEgLgEQgIgDgFgFIgIADIgKAFQgNAFgOgHQgNgGgKgNQgMAIgPgFQgPgGgGgNIgBgEIgDgCQgHgFgHgLQgFgHgBgFIgBgMIACgKQABgEAGgIQAIgIAFgDQALgHAPADQgBgEACgCIACAAIAAgBIAEACQAAABABAAQAAAAABABQAAAAAAAAQABABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABgBAAIgBABQAIADAGAHIAEAFIAHAAQASABANAKIABgBQAHgDAJAAQAOgBAIADQAJADAKAJIADAEIABAAQAIgBAPgGQAYgGAVAIQAIAEAFgBQADAAAIgEQAagQAcAMIAQAIQAKAFAIAAQAGgBAJgEIADgBIgBgFIAAgCIACgBIADgEQAAAAAAAAQABAAAAgBQAAAAABAAQAAAAABAAIAAAAQABAAAAAAQABAAAAABQABAAAAAAQAAAAABABQAAAAAAABQABAAAAABQAAAAAAABQAAAAAAABQAOgHAQADQAQADAKAMQAKAMgBAQQgCARgMAJQgGAFgRAFIgbAJQgYAHgMgCQgJgBgNgFQgQgHgHgCQgEAKgLAFQgJAEgJAAIgDAAgAhAgcIgEgEIAAgEIAAgDIAAgCQAAAAABAAQAAgBABAAQAAAAABAAQAAAAABAAIAAgBQAAAAABABQABAAAAAAQABAAAAAAQAAABAAAAIACADIABACIABACIgBADIgBADIgDAAgABQgiIgCgCIgBgDQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAAAgBQABAAAAAAQABAAAAAAQAAAAABAAIAAgBQADgBACADQACACgBADIgEAEgAhpgkQgBAAAAAAQgBgBAAAAQAAgBAAgBQAAAAAAgBIABgDQAAAAAAgBQABAAAAAAQAAAAABAAQAAAAABAAIAAgBQABABAAAAQABAAAAABQABAAAAAAQABABAAAAIABACQAAABAAABQAAAAgBAAQAAABAAAAQAAAAgBAAIgBACgACigpIACgFIAJgJQADgCADAAQADgBACACQADADgCADIgFACIgIAIQgBABAAABQgBAAgBAAQAAABAAAAQgBAAAAAAIgCAAQgDAAgBgEg");
    this.shape_4.setTransform(12.3295, -18.2417);

    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#34D0E0").s().p("ACUBEQgNgGgEgMQgHAGgNgCQgQgDgEABIgMADQgUAGgQgOQgEAGgKABIgSgBIgNABIgOACQgIABgGgDQgGgEgCAAQgDgBgIAFQgVAMgXgKIgJgEQgFgDgEgBIgMABQgHAAgEgBIgPgGQgGgBgNACQgMgBgLgJQgKgJgCgNQgCgLAGgNQAFgLALgIQAWgOAbAIIAWAHQARAAAHAEQAKAFACAAQACAAAEgDQAPgKASADQADgLAJgHQAIgHALgBQAWgDAPARIALALQAGAGAGAAQAEAAAHgDIAKgDQAFAAAHACIAMACQAEABAHgCIAKgCQAFgBAIAEIANAFIATAGQAMAEAGAEQAKAGAFALQAEALgBALQAAAOgIALQgIALgMAEQgFACgGAAQgHAAgHgDgACkgkQAAAAgBgBQAAAAAAgBQgBAAAAgBQAAAAAAgBQAAgEACgEQADgDAEAAIABgBIADACIACADQAAABAAAAQAAABAAAAQAAABgBAAQAAABgBAAIgDACIgCADQAAABAAAAQgBAAAAABQgBAAAAAAQgBAAgBAAgAhMgpQAAAAAAAAQgBgBAAAAQAAAAAAgBQAAAAAAgBQAAgBAAAAQgBAAAAgBQAAAAABAAQAAgBAAAAQAAAAABgBQAAAAAAAAQABAAAAAAQABgBAAAAIABAAQAAAAABAAQAAgBABAAQAAAAABABQAAAAABAAQABAAAAABQABAAAAAAQABAAAAABQABAAAAAAIAAACQAAABAAABQAAAAAAABQAAAAgBABQAAAAgBAAIgCAAIgEABIgDgBgAAtgtIgDgBIAAgCQgBgCACgDIAEgCIAAgBIAFAEIADADQABABAAABQAAAAAAABQAAAAAAABQAAAAgBABIgDACIgHgDgAiog0IgEgDIgBgCIABgCQAAgBABAAQAAgBAAAAQABAAABAAQAAgBABAAIAAgBIADACIACADIABAEIgBABIgEABgAC9g4IgCgBIgCgBQgCgCACgEQACgDAEgBIAGgBIABgBQAEABABACQACAEgEACIgCABIgEACIgCAAIgCACgAAVg8IgCgDIABgDQABAAAAAAQAAgBAAAAQABAAAAAAQABAAAAAAIAAgBQABAAABABQAAAAABAAQAAAAABABQAAAAABAAIAAADQAAABAAAAQAAABAAAAQAAABAAAAQgBABAAAAIgDAAIgDgBg");
    this.shape_5.setTransform(9.5541, -16.7987);

    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#34D0E0").s().p("AhxAuQgJgEAAgFIACgIIgJgFQgEgBgIACQgNAAgQgUQgIgKgBgIQgCgNAKgEIgBgBIgEgEIgBgEIABgGQAAgFABgBIADgDIABgBIACAAQAAAAAAABQAAABAAAAQAAABAAAAQAAABAAAAIgBACIgBADIAAAGIABACIABABIABADIACACIAGABIAAAAIABgBIACgCQAEgDACAAQABADgDADIgDACIAEADQAKAKABANIAZgBQgGgKAAgJQAAgLAIgFIABAAQAEgDAFACQAFABACAFIABALQgCALADALIACgJQANAAANAEQAGACADgBQAEgBACgFIADgKQACgIAFgGQAKgLAJACQAFABADAFQADAFgCAEIgGAHQgDAEABADQACADADgEIAEgEQAEgCAIAFQAHAFADgDQgCgDACgEQACgFAEgBQAHgDAJAGQAEACABADIAAAKQABAEAFACQAFACAFgBIARgIQALgCASAKIACgLQADgHAFAAQADAAAIAGQAJAIAMgCIAIAAIACAAIABgDIACgCIADgCIACgCQABgBAAAAQABAAAAAAQABgBAAAAQAAAAABABIAAgCIADACIABADIgBAEIgEAGIgBACIgCABIgDABIADAJQALADAGAEQAFACAAADQAAACgDADQgOAMgLADQgEACgRABQgYABg+AHQg1AGggABIgHAAQgoAAgagLgACNgKIgEgBQAAgBABAAQAAAAAAAAQAAgBAAAAQgBAAAAAAIAAgBQgBgBAAAAQAAAAAAAAQAAgBAAAAQAAAAABAAIAAgEQAAAAAAgBQAAAAAAAAQABgBAAAAQAAAAABAAIAAAAIADAAIACABIAAACQABADgBADQAAABAAAAQAAABgBAAQAAAAAAAAQAAABgBAAgAhOgQIgCgGIABgEIACgEIADgBIAIABQAAABAAAAQgBABAAAAQAAABgBAAQgBAAAAABIgCAAQAAAAAAAAQgBABAAAAQAAABgBAAQAAABAAABIgCAGg");
    this.shape_6.setTransform(10.575, -18.2085);

    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#34D0E0").s().p("AhqA1IgOgEIgTAAQgMABgHgEQgFgDgGgKIgLgQQgFgHgBgFQAAgFADgEQAEgEAFgCQAJgDANAEQgBgNASgNQAIgGAFACQAFADAAAGQABAFgDAFIgGAJQgGAMAHAHQADACAGAAQAOAAAKgHIAOgJQAFgBAIAAIAMgBIAJgDQAGgCAEABQAGAAAEAEQAFAEABAFQABAEgDADQgDADgIgCQgJgCgDACQABADAFABQAKADAOgFIAWgJQAGgCALAAQAMAAAGADIAMAHQAHADAFgCQAGgCABgIIABgJIgBACIgDgBIgBgEIAAgEIACgCQAAgBAAAAQABAAAAAAQAAgBAAAAQABABAAAAIABgBQABAAAAAAQABAAABAAQAAAAABAAQAAAAAAABIABgBQADgBADADQAEAEABAEIACAJQAEAGALgCIARgDQAQgTAIgZQAFgLAFAAQAFAAADAHQADAKgGANQgKANgDAHQAFgCADAGQABADgCAGQgDAHgGADQgEADgTADIglAJQgVAFggACQgbADgiABQggABgPAEQgPAFgHAAQgFAAgJgCgAhzgFQAAgBABAAQAAgBAAAAQAAgBAAAAQABAAAAgBIAHgEIACAAQADAAADACIgFAEIgCABQgEACAAAEIgEAAIgCgFgACpgMIABgDIABgCIABgBIgBgFIACgCIADABQAAAAABABQAAAAABAAQAAABABAAQAAABAAAAQABABAAAAQAAABAAABQAAAAAAABQAAABgBAAIgDAEIgBADIgBAAIgEAAQgBAAAAAAQAAgBAAAAQAAAAAAgBQAAAAAAgBgAidgNQAAgDgDgCIgDgDIgBgCIgCgBIgBgDIABgDQAAAAABAAQAAAAAAAAQABAAAAAAQAAgBAAAAQAGABADAEQADAGAAAEQAAABAAAAQAAABgBABQAAAAAAAAQgBABAAAAIgBABIgCgCgACygxIAEgEQAAAAABAAQAAAAABAAQAAAAABAAQAAABAAAAIABABIAAACIAAADQABADgCAEIgFAAIgCgKg");
    this.shape_7.setTransform(12.8531, -19.975);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape}]}).to({state: [{t: this.shape_1}]}, 4).to({state: [{t: this.shape_2}]}, 7).to({state: [{t: this.shape_3}]}, 8).to({state: [{t: this.shape_4}]}, 6).to({state: [{t: this.shape_5}]}, 10).to({state: [{t: this.shape_6}]}, 7).to({state: [{t: this.shape_7}]}, 5).wait(7));

    // Layer_2
    this.instance = new lib.سفينة("synched", 0);
    this.instance.setTransform(17.1, -47.75, 0.9999, 0.9999, 2.0246);

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({rotation: 3.0056}, 0).wait(1).to({rotation: 3.9856}, 0).wait(1).to({rotation: 4.9655}, 0).wait(1).to({rotation: 5.9454}, 0).wait(1).to({rotation: 6.3822}, 0).wait(1).to({rotation: 6.8189}, 0).wait(1).to({rotation: 7.2557}, 0).wait(1).to({rotation: 7.6924}, 0).wait(1).to({rotation: 8.1292}, 0).wait(1).to({rotation: 8.5659}, 0).wait(1).to({rotation: 9.0027}, 0).wait(1).to({rotation: 8.4209}, 0).wait(1).to({rotation: 7.8392}, 0).wait(1).to({rotation: 7.2575}, 0).wait(1).to({rotation: 6.6758}, 0).wait(1).to({rotation: 6.0941}, 0).wait(1).to({rotation: 5.5124}, 0).wait(1).to({rotation: 4.9307}, 0).wait(1).to({rotation: 4.349}, 0).wait(1).to({rotation: 3.6241}, 0).wait(1).to({rotation: 2.8993}, 0).wait(1).to({rotation: 2.1745}, 0).wait(1).to({rotation: 1.4497}, 0).wait(1).to({rotation: 0.7248}, 0).wait(1).to({rotation: 0}, 0).wait(1).to({rotation: 0.0723}, 0).wait(1).to({rotation: 0.1447}, 0).wait(1).to({rotation: 0.217}, 0).wait(1).to({rotation: 0.2894}, 0).wait(1).to({rotation: 0.3617}, 0).wait(1).to({rotation: 0.4341}, 0).wait(1).to({rotation: 0.5064}, 0).wait(1).to({rotation: 0.5788}, 0).wait(1).to({rotation: 0.6511}, 0).wait(1).to({rotation: 0.7235}, 0).wait(1).to({rotation: 0.7958}, 0).wait(1).to({rotation: 0.8682}, 0).wait(1).to({rotation: 0.9405}, 0).wait(1).to({rotation: 1.0129}, 0).wait(1).to({rotation: 1.0852}, 0).wait(1).to({rotation: 1.1575}, 0).wait(1).to({rotation: 1.2299}, 0).wait(1).to({rotation: 1.3022}, 0).wait(1).to({rotation: 1.3746}, 0).wait(1).to({rotation: 1.4469}, 0).wait(1).to({rotation: 1.5193}, 0).wait(1).to({rotation: 1.5916}, 0).wait(1).to({rotation: 1.664}, 0).wait(1).to({rotation: 1.7363}, 0).wait(1).to({rotation: 1.8087}, 0).wait(1).to({rotation: 1.881}, 0).wait(1).to({rotation: 1.9534}, 0).wait(1).to({rotation: 2.0257}, 0).wait(1));

    // flash0_ai
    this.instance_1 = new lib.بحلر();
    this.instance_1.setTransform(16.95, -7.85, 0.8818, 0.8818);

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(54));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-36.3, -79.5, 106.8, 120.2);


  (lib.السهول = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3_copy_copy_copy
    this.instance = new lib.طير();
    this.instance.setTransform(30, -86.25, 0.6119, 0.6119, 0, 0, 0, 23.1, -67.3);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(7).to({_off: false}, 0).to({_off: true}, 78).wait(55));

    // Layer_3_copy_copy
    this.instance_1 = new lib.طير();
    this.instance_1.setTransform(21.9, -79.35, 1, 1, 0, 0, 0, 23.1, -67.3);
    this.instance_1._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(16).to({_off: false}, 0).to({_off: true}, 78).wait(46));

    // Layer_3_copy
    this.instance_2 = new lib.طير();
    this.instance_2.setTransform(34.2, -71.8, 0.6269, 0.6269, 0, 0, 0, 23.2, -67.3);

    this.timeline.addTween(cjs.Tween.get(this.instance_2).to({_off: true}, 78).wait(62));

    // flash0_ai
    this.instance_3 = new lib.CachedBmp_7();
    this.instance_3.setTransform(-63.2, -67.75, 0.3333, 0.3333);

    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(140));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-63.2, -88.5, 126.7, 156.4);

  (lib.Symbol25 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // OBJECTS
    this.instance = new lib.CachedBmp_163();
    this.instance.setTransform(-67.25, -160.55, 0.5, 0.5);

    this.instance_1 = new lib.Path_0();
    this.instance_1.setTransform(-3.7, -166.75, 1, 1, 0, 0, 0, 44.2, 44.2);
    this.instance_1.alpha = 0.5313;
    this.instance_1.compositeOperation = "multiply";

    this.instance_2 = new lib.CachedBmp_162();
    this.instance_2.setTransform(-54.7, -217.8, 0.5, 0.5);

    this.instance_3 = new lib.Group_1();
    this.instance_3.setTransform(-3.7, 196.85, 1, 1, 0, 0, 0, 33, 8.8);
    this.instance_3.compositeOperation = "multiply";

    this.instance_4 = new lib.CachedBmp_69();
    this.instance_4.setTransform(-51.65, 102.55, 0.5, 0.5);

    this.instance_5 = new lib.CachedBmp_68();
    this.instance_5.setTransform(-13.85, 71.45, 0.5, 0.5);

    this.instance_6 = new lib.CachedBmp_161();
    this.instance_6.setTransform(-84.6, 67.3, 0.5, 0.5);

    this.instance_7 = new lib.CachedBmp_66();
    this.instance_7.setTransform(-51.65, 41.4, 0.5, 0.5);

    this.instance_8 = new lib.CachedBmp_65();
    this.instance_8.setTransform(-13.85, 10.3, 0.5, 0.5);

    this.instance_9 = new lib.CachedBmp_64();
    this.instance_9.setTransform(-76.05, -108.35, 0.5, 0.5);

    this.instance_10 = new lib.CachedBmp_160();
    this.instance_10.setTransform(-84.6, 6.15, 0.5, 0.5);

    this.instance_11 = new lib.Path_4();
    this.instance_11.setTransform(0, -109, 1, 1, 0, 0, 0, 126.7, 68.1);
    this.instance_11.compositeOperation = "multiply";

    this.instance_12 = new lib.CachedBmp_62();
    this.instance_12.setTransform(-126.7, -177.1, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_12}, {t: this.instance_11}, {t: this.instance_10}, {t: this.instance_9}, {t: this.instance_8}, {t: this.instance_7}, {t: this.instance_6}, {t: this.instance_5}, {t: this.instance_4}, {t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-126.7, -217.8, 253.5, 435.70000000000005);


  p.nominalBounds = new cjs.Rectangle(-105.8, -10.3, 253.5, 466.3);


  (lib.Symbol21 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.instance = new lib.CachedBmp_48();
    this.instance.setTransform(124.35, -35.55, 0.5, 0.5);

    this.instance_1 = new lib.Symbol5_1("synched", 0);
    this.instance_1.setTransform(-19.4, 10.1, 2.8742, 2.8742, 0, 0, 0, 0.3, 0.6);

    this.instance_2 = new lib.CachedBmp_47();
    this.instance_2.setTransform(-33.75, -54.15, 0.5, 0.5);

    this.instance_3 = new lib.CachedBmp_46();
    this.instance_3.setTransform(128.65, -35.6, 0.5, 0.5);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_3}, {t: this.instance_2}, {t: this.instance_1}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-131.6, -102.8, 502.4, 226);


  (lib.Symbol18 = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Isolation_Mode
    this.instance = new lib.Symbol4_1("synched", 0);
    this.instance.setTransform(-79.25, 189.25, 0.1986, 0.1986, -30.9358, 0, 0, -1.8, -1.9);

    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("EhjrAhFIxYAAQl4ALAcmUIAAmKIOoAAIrFr2QhmiHghijQgfijAqiSQAriTB1hxQB0hyDHgmIRZAAQF0gLggGUIAAGEIt9AAQBfByBmBpIDSDYQBqBrBkBvQBiBtBTByQBcB9AACiQAABwgrBzQgqBxhKBbQhMBbhoA0QhcAvhoAAIgdgBgEBZDAg+MircAAAQAAnlgCnTQgCmeAAoAQAAnPgBnRQgCmdAAoVQA5nCGJgZIG0AAMAAAA0NIPEAAIAEyxQA4nCGGgcIG0AAIgEMOQCckJCNiWQCMiXCGhOQCHhOCKgZQCFgXChgMMAkmAAAIAAaPIN9AAIAHy1QAym+GQgcMAjHAAAIAAaPIPrAAMAAAgsyQAcjgBsh4QBsh4DKgLIG0AAMAAAAyOQAEDxg0C/QgzC/hfCDQhiCFiDBCQh5A9iMAAIgfgBgEAqaATIIOhAAIAAsrIuhAAgA67GyQicAYigBWQigBWiRCiQiSChiBEPMApsAAAIAAsrI26AAIglgBQiEAAiJAWgEBstAg+MAAAg6oQAjjgBqh4QBrh4DKgLIGzAAMAAAA6vQg0G8mOAYgEAu2gMtQhUgig9g/Qg9g+gjhTQgkhUAAhcQAAhfAkhWQAihUA+g/QA8g9BVgkQBWgkBeAAQBgAABVAkQBWAkA8A9QA7A9AlBWQAiBWAABfQAABcgiBUQglBUg7A9Qg9A/hVAiQhVAkhgAAQheAAhWgkgEgkogMtQhSgig/g/Qg/g/gjhSQgjhUAAhcQAAhfAjhWQAjhTA/hAQA+g9BTgkQBUgkBjAAQBfAABUAkQBTAkA+A9QA/BAAjBTQAjBWAABfQAABcgjBUQgjBSg/A/Qg/A/hSAiQhUAkhfAAQhjAAhUgkg");
    this.shape.setTransform(1.9585, 187.3151, 0.0485, 0.0485);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#A1356A").s().p("AE9BtIgDAAIrQAAIAFjZILLAAIAAABIADgBIADAAQASAAAQAJQAPAIAMAPQANAPAGAUQAIAUgBAVQABAWgIAUQgGAUgNAPQgMAPgPAIQgQAJgSAAIgDAAg");
    this.shape_1.setTransform(-9.1473, 188.9626, 1.7218, 1.7218);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}, {t: this.instance}]}).wait(1));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-122.6, 128.2, 183.6, 120.70000000000002);


  (lib.زرصحراء = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.instance = new lib.جبال1_1();
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off: false}, 0).wait(1));

    // space
    this.instance_1 = new lib.Symbol21("synched", 0);
    this.instance_1.setTransform(-38.5, 97.85, 0.2718, 0.2718, 0, 0, 0, -25.4, 11.1);
    this.instance_1._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({_off: false}, 0).to({_off: true}, 2).wait(1));

    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f("rgba(20,2,2,0.22)").s().p("Ag/KsIgIgIQgTgTgNgIIgHgEIgNACIgEAFIgWAUIgBAAIgEAEIgBAAIgDAAIAAAAIgZgCQgLgWgegNQgagMgDgJIgHgZQgZgXgGgEIgBAAIgagRQgNgJgLgLIAAAAQgSgQgTgWIgHgHIAAAAIgMgNIgJgDIAAAAIgDAAQgSgFgPgcQgbgHgLgEQgHgCgGgFIgCgCIgPgUQgQgegShCQgRhCgFgJQgWgogIg6QgFgoAAhCIABABIgDgDQBRgsAvgfIAogcIAhg3IAKgWQAEgJANgxQAKgmALgNQARgTAWgFQAIgTACgMQAGgfA4hUQA5hWAGgdQACgGAFgHIAAgBQAGgdA5gyQA1gvAWgFQAUgEApgDIA7gEIABAAIACAAIAAAHQANAOAKAGIAFACQAYAHAMAIQgIASAbA2QAgA+ABAMIAEAYQADAQAFAEQAJAIAJAjQAKAlgFAUIgMAoIgGAZQAAAIAMAMQAGAGAOAGQAPAHAFAFQAHAHAFANIAJAWIAUAvQALAaAIAQQASAMAOANQAWALAVAQIABAAQCGBAApApIAFABQgIAWgGA+IAHADQAFACACADIALAhQAKAjgIALQgFAGgNALQgJAIgFALQgEAIgFASQgFAPgGAJQgFAHgKAJIAAAKIAAAGIAAABQgCAcgLAaIAAAAQgLAHgyA6QgMAOgLAKQggAegiAKQgSAFgVAKIAAAAQgPAIgPAKQgIAjgIAFIgGAFIgKAFIgGADIgBABIgCAAIAAAAIgBAAQgNAEgOgFIgsAnIAAgBIgGAFIgFAFQgYATgagBIgWgHIgDABIgBAAIAAABIgBAAIgJAEQgSAKgcAYIgDACQgiAcgVAFQgdgKgWgTg");
    this.shape.setTransform(0.0079, 0.025);
    this.shape._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1).to({_off: false}, 0).to({_off: true}, 1).wait(2));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-67.3, -71.3, 136.5, 199.60000000000002);


  (lib.زرالفضاء = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#000000").s().p("AlCNbQiCiDAAi4QAAi5CCiCQCDiCC4gBQC3ABCDCCQCCCCAAC5QAAC4iCCDQiDCBi3AAQi4AAiDiBgAtYBeQhNhOAAhsQAAhvBNhNQBOhOBuAAQBuAABNBOQBOBNAABvQAABshOBOQhNBNhuAAQhuAAhOhNgAgMgoQijijAAjmQAAjmCjiiQChijDmAAQDmAACjCjQCiCiAADmQAADmiiCjQijChjmABQjmgBihihg");
    this.shape.setTransform(0.025, 0);
    this.shape._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off: false}, 0).wait(1));

    // Layer_3
    this.instance = new lib.Symbol18("synched", 0);
    this.instance.setTransform(-41.2, -92.9, 1.0475, 1.0475, -36.968);
    this.instance._off = true;

    this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off: false}, 0).to({_off: true}, 2).wait(1));

    // space
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("rgba(0,0,0,0.22)").s().p("AlBNQQh+h9AAizQAAixB+h9QB+h9CxAAQCwAAB+B9QB9B9AACxQAACzh9B9Qh+B8iwABQixgBh+h8gAtLBPQhIhIAAhmQAAhmBIhIQBJhJBmABQBmgBBIBJQBJBIAABmQAABmhJBIQhIBIhmABQhmgBhJhIgAgegYQijijAAjmQAAjmCjiiQChiiDmAAQDmAACjCiQCiCiAADmQAADmiiCjQijChjmAAQjmAAihihg");
    this.shape_1.setTransform(1.825, -1.6);
    this.shape_1._off = true;

    this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1).to({_off: false}, 0).to({_off: true}, 1).wait(2));

    this._renderFirstFrame();

  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-93.4, -98.8, 227, 273.7);


// stage content:
  (lib.Home = function (mode, startPosition, loop, reversed) {
    if (loop == null) {
      loop = true;
    }
    if (reversed == null) {
      reversed = false;
    }
    var props = new Object();
    props.mode = mode;
    props.startPosition = startPosition;
    props.labels = {};
    props.loop = loop;
    props.reversed = reversed;
    cjs.MovieClip.apply(this, [props]);

    // Layer_3
    this.instance_7 = new lib.زرالصحراء();
    this.instance_7.setTransform(1043.8, 443.4);
    new cjs.ButtonHelper(this.instance_7, 0, 1, 2, false, new lib.زرالصحراء(), 3);

    this.instance_8 = new lib.زرالبحار();
    this.instance_8.setTransform(738.75, 289.65);
    new cjs.ButtonHelper(this.instance_8, 0, 1, 2, false, new lib.زرالبحار(), 3);

    this.instance_9 = new lib.زرالفضاء();
    this.instance_9.setTransform(395, 462.35, 1, 1, 36.4989);
    new cjs.ButtonHelper(this.instance_9, 0, 1, 2, false, new lib.زرالفضاء(), 3);

    this.instance_10 = new lib.زرالسهول();
    this.instance_10.setTransform(380, 730.6);
    new cjs.ButtonHelper(this.instance_10, 0, 1, 2, false, new lib.زرالسهول(), 3);

    this.instance_11 = new lib.زرالانهار();
    this.instance_11.setTransform(1120.2, 716.45);
    new cjs.ButtonHelper(this.instance_11, 0, 1, 2, false, new lib.زرالانهار(), 3);

    this.instance_12 = new lib.زرصحراء();
    this.instance_12.setTransform(744.05, 890.3, 1.5946, 1.5946, 0, 0, 0, 0.1, 0.1);
    new cjs.ButtonHelper(this.instance_12, 0, 1, 2, false, new lib.زرصحراء(), 3);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_12}, {t: this.instance_11}, {t: this.instance_10}, {t: this.instance_9}, {t: this.instance_8}, {t: this.instance_7}]}).wait(3999));

    // Layer_4
    this.instance_13 = new lib.جبال("synched", 0);
    this.instance_13.setTransform(741.4, 890.75, 1.6, 1.6, 0, 0, 0, -1.4, 0.1);

    this.instance_14 = new lib.سهول("synched", 0);
    this.instance_14.setTransform(1120.65, 715.75, 1.5, 1.5, 0, 0, 0, 0.1, 0);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_14}, {t: this.instance_13}]}).wait(3999));

    // Layer_5
    this.instance_15 = new lib.فضاء("synched", 1);
    this.instance_15.setTransform(451.55, 449.7, 0.4796, 0.4796, 25.3072, 0, 0, 0.2, 0.2);

    this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(3999));

    // Layer_6
    this.instance_16 = new lib.صحراء("synched", 0);
    this.instance_16.setTransform(1041.3, 402.1, 1.5, 1.5, 0, 0, 0, 0.2, -23.4);

    this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(3999));

    // Layer_7
    this.instance_17 = new lib.السهول("synched", 0);
    this.instance_17.setTransform(377.75, 719.2, 1.5, 1.5, 0, 0, 0, 0.1, -8.8);

    this.instance_18 = new lib.بحارصورة("synched", 0);
    this.instance_18.setTransform(738.3, 286.7, 1.7, 1.7, 0, 0, 0, 17, -19.8);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_18}, {t: this.instance_17}]}).wait(3999));

    // Layer_12
    this.instance_29 = new lib.Symbol15MM("synched", 0);
    this.instance_29.setTransform(788.15, 677.8, 0.9536, 0.9536);

    this.timeline.addTween(cjs.Tween.get(this.instance_29).wait(3999));

    // Layer_13
    this.instance_30 = new lib.Symbolm("synched", 0);
    this.instance_30.setTransform(671.7, 501.95, 0.9536, 0.9536);

    this.timeline.addTween(cjs.Tween.get(this.instance_30).wait(3999));

    // Layer_14
    this.instance_31 = new lib._02();
    this.instance_31.setTransform(787.2, 559.6, 1.0086, 1.0086, 0, 0, 0, 0, -6);

    this.timeline.addTween(cjs.Tween.get(this.instance_31).to({x: 715.2}, 2893).to({x: 787.2}, 1105).wait(1));

    // Layer_15
    this.instance_32 = new lib.Symbol12();
    this.instance_32.setTransform(733.15, 637.75, 0.9536, 0.9536);

    this.instance_33 = new lib.Symbol11();
    this.instance_33.setTransform(739.75, 509.75, 0.9536, 0.9536);
    this.instance_33.compositeOperation = "lighter";

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_33}, {t: this.instance_32}]}).wait(3999));

    // Layer_16
    this.instance_34 = new lib.Symbol13("synched", 0);
    this.instance_34.setTransform(738.8, 586.5, 0.8041, 0.8041);

    this.timeline.addTween(cjs.Tween.get(this.instance_34).wait(3999));

    // OBJECT
    this.instance_35 = new lib.Symbol5("synched", 0);
    this.instance_35.setTransform(746.95, 587.7, 2.1925, 2.8001, 0, 0, 0, 59.1, 0.6);

    this.timeline.addTween(cjs.Tween.get(this.instance_35).wait(3999));

    // Layer_18
    this.instance_36 = new lib._1();
    this.instance_36.setTransform(738.5, 586.55, 0.6076, 0.6076, 0, 0, 0, 0.6, 0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance_36).wait(3999));

    // Layer_20
    this.instance_37 = new lib._01();
    this.instance_37.setTransform(845.5, 604.6, 0.9536, 0.9536, 0, 0, 180);

    this.instance_38 = new lib._01();
    this.instance_38.setTransform(653.65, 655.05, 0.9536, 0.9536);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_38}, {t: this.instance_37}]}).wait(3999));

    // Layer_21
    this.instance_39 = new lib.Symbol2("synched", 0);
    this.instance_39.setTransform(738.5, 586.55, 1.3872, 1.4558, 0, 0, 0, 0.1, 0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance_39).wait(3999));

    // OBJECT
    this.instance_40 = new lib.Symbol3("synched", 0);
    this.instance_40.setTransform(738.5, 586.5, 1.4056, 1.4751, 0, 0, 0, 0.1, 0.1);

    this.timeline.addTween(cjs.Tween.get(this.instance_40).wait(3998).to({
      regX: 0.2,
      scaleX: 1.3934,
      scaleY: 1.4839,
      x: 736.8,
      y: 587.75
    }, 0).wait(1));

    // stageBackground
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("rgba(0,0,0,0)").ss(1, 1, 1, 3, true).p("Eh0ohdXMDpRAAAMAAAC6vMjpRAAAg");
    this.shape.setTransform(736.45, 587.55);

    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#0066FF").s().p("Eh0nBdYMAAAi6uMDpQAAAMAAAC6ug");
    this.shape_1.setTransform(736.45, 587.55);

    this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(3999));

    this._renderFirstFrame();

  }).prototype = p = new lib.AnMovieClip();
  p.nominalBounds = new cjs.Rectangle(725, 576.5, 758.9000000000001, 609.5999999999999);
// library properties:
  lib.properties = {
    id: '486263FE0B77434DAD402AD387BAAC3A',
    width: 1472,
    height: 1175,
    fps: 24,
    color: "#0066FF",
    opacity: 1.00,
    manifest: [
      {src: "assets/images/stations/CachedBmp_40.png", id: "CachedBmp_40"},
      {src: "assets/images/stations/CachedBmp_39.png", id: "CachedBmp_39"},
      {src: "assets/images/stations/Home_atlas_1.png", id: "Home_atlas_1"},
      {src: "assets/images/stations/Home_atlas_2.png", id: "Home_atlas_2"},
      {src: "assets/images/stations/Home_atlas_3.png", id: "Home_atlas_3"},
      {src: "assets/images/stations/Home_atlas_4.png", id: "Home_atlas_4"},
      {src: "assets/images/stations/Home_atlas_5.png", id: "Home_atlas_5"},
      {src: "assets/images/stations/Home_atlas_6.png", id: "Home_atlas_6"},
      {src: "assets/images/stations/Home_atlas_7.png", id: "Home_atlas_7"},
      {src: "assets/images/stations/Home_atlas_8.png", id: "Home_atlas_8"},
      {src: "assets/images/stations/Home_atlas_9.png", id: "Home_atlas_9"},
      {src: "assets/images/stations/Home_atlas_10.png", id: "Home_atlas_10"}
    ],
    preloads: []
  };


// bootstrap callback support:

  (lib.Stage = function (canvas) {
    createjs.Stage.call(this, canvas);
  }).prototype = p = new createjs.Stage();

  p.setAutoPlay = function (autoPlay) {
    this.tickEnabled = autoPlay;
  }
  p.play = function () {
    this.tickEnabled = true;
    this.getChildAt(0).gotoAndPlay(this.getTimelinePosition())
  }
  p.stop = function (ms) {
    if (ms) this.seek(ms);
    this.tickEnabled = false;
  }
  p.seek = function (ms) {
    this.tickEnabled = true;
    this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000);
  }
  p.getDuration = function () {
    return this.getChildAt(0).totalFrames / lib.properties.fps * 1000;
  }

  p.getTimelinePosition = function () {
    return this.getChildAt(0).currentFrame / lib.properties.fps * 1000;
  }

  an.bootcompsLoaded = an.bootcompsLoaded || [];
  if (!an.bootstrapListeners) {
    an.bootstrapListeners = [];
  }

  an.bootstrapCallback = function (fnCallback) {
    an.bootstrapListeners.push(fnCallback);
    if (an.bootcompsLoaded.length > 0) {
      for (var i = 0; i < an.bootcompsLoaded.length; ++i) {
        fnCallback(an.bootcompsLoaded[i]);
      }
    }
  };

  an.compositions = an.compositions || {};
  an.compositions['486263FE0B77434DAD402AD387BAAC3A'] = {
    getStage: function () {
      return exportRoot.stage;
    },
    getLibrary: function () {
      return lib;
    },
    getSpriteSheet: function () {
      return ss;
    },
    getImages: function () {
      return img;
    }
  };

  an.compositionLoaded = function (id) {
    an.bootcompsLoaded.push(id);
    for (var j = 0; j < an.bootstrapListeners.length; j++) {
      an.bootstrapListeners[j](id);
    }
  }

  an.getComposition = function (id) {
    return an.compositions[id];
  }


  an.makeResponsive = function (isResp, respDim, isScale, scaleType, domContainers) {
    var lastW, lastH, lastS = 1;
    window.addEventListener('resize', resizeCanvas);
    resizeCanvas();

    function resizeCanvas() {
      var w = lib.properties.width, h = lib.properties.height;
      var iw = window.innerWidth, ih = window.innerHeight;
      var pRatio = window.devicePixelRatio || 1, xRatio = iw / w, yRatio = ih / h, sRatio = 1;
      if (isResp) {
        if ((respDim == 'width' && lastW == iw) || (respDim == 'height' && lastH == ih)) {
          sRatio = lastS;
        } else if (!isScale) {
          if (iw < w || ih < h)
            sRatio = Math.min(xRatio, yRatio);
        } else if (scaleType == 1) {
          sRatio = Math.min(xRatio, yRatio);
        } else if (scaleType == 2) {
          sRatio = Math.max(xRatio, yRatio);
        }
      }
      domContainers[0].width = w * pRatio * sRatio;
      domContainers[0].height = h * pRatio * sRatio;
      domContainers.forEach(function (container) {
        container.style.width = w * sRatio + 'px';
        container.style.height = h * sRatio + 'px';
      });
      stage.scaleX = pRatio * sRatio;
      stage.scaleY = pRatio * sRatio;
      lastW = iw;
      lastH = ih;
      lastS = sRatio;
      stage.tickOnUpdate = false;
      stage.update();
      stage.tickOnUpdate = true;
    }
  }
  an.handleSoundStreamOnTick = function (event) {
    if (!event.paused) {
      var stageChild = stage.getChildAt(0);
      if (!stageChild.paused) {
        stageChild.syncStreamSounds();
      }
    }
  }


})(createjs = createjs || {}, AdobeAn = AdobeAn || {});
var createjs, AdobeAn;
var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
var instance = '';

function initStations() {
  return new Promise((resolve, reject) => {
    canvas = document.getElementById("canvas");
    anim_container = document.getElementById("animation_container");
    dom_overlay_container = document.getElementById("dom_overlay_container");
    var comp = AdobeAn.getComposition("486263FE0B77434DAD402AD387BAAC3A");
    var lib = comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function (evt) {
      handleFileLoadStations(evt, comp)
    });
    loader.addEventListener("complete", function (evt) {
      handleCompleteStations(evt, comp).then(data => {
        resolve(true)
      });
    });
    var lib = comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  });
}

function handleFileLoadStations(evt, comp) {
  var images = comp.getImages();
  if (evt && (evt.item.type == "image")) {
    images[evt.item.id] = evt.result;
  }
}

function handleCompleteStations(evt, comp) {
  return new Promise((resolve, reject) => {
    var lib = comp.getLibrary();
    var ss = comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for (i = 0; i < ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet({
        "images": [queue.getResult(ssMetadata[i].name)],
        "frames": ssMetadata[i].frames
      })
    }
    var preloaderDiv = document.getElementById("_preload_div_");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.Home();
    stage = new lib.Stage(canvas);
    stage.enableMouseOver();
    createjs.Ticker.removeAllEventListeners(); // Note the function name
    //Registers the "tick" event listener.
    fnStartAnimation = function () {
      stage.addChild(exportRoot);
      createjs.Ticker.framerate = lib.properties.fps;
      createjs.Ticker.addEventListener("tick", stage);
    }
    //Code to support hidpi screens and responsive scaling.
    AdobeAn.makeResponsive(true, 'both', false, 1, [canvas, preloaderDiv, anim_container, dom_overlay_container]);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
    stationClickEvents();
    stationHoverEvents();
    resolve(true)
  });
}

function stationClickEvents() {
  //rivers
  exportRoot.instance_11.on("click", function (evt) {
    playAudioOnClick();
    instance = 1;
  });
  //mountains
  exportRoot.instance_12.on("click", function (evt) {
    playAudioOnClick();
    instance = 2;
  });
  //plains
  exportRoot.instance_10.on("click", function (evt) {
    playAudioOnClick();
    instance = 3;
  });
  //spaces
  exportRoot.instance_9.on("click", function (evt) {
    playAudioOnClick();
    instance = 4;
  });
  //Seas
  exportRoot.instance_8.on("click", function (evt) {
    playAudioOnClick();
    instance = 5;
  });
  //Deserts
  exportRoot.instance_7.on("click", function (evt) {
    playAudioOnClick();
    instance = 6;
  });

}

function stationHoverEvents() {
  //rivers
  exportRoot.instance_11.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //rivers
  exportRoot.instance_11.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //mountains
  exportRoot.instance_12.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //plains
  exportRoot.instance_10.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //spaces
  exportRoot.instance_9.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //Seas
  exportRoot.instance_8.on("mouseover", function (evt) {
    playAudioOnHover();
  });
  //Deserts
  exportRoot.instance_7.on("mouseover", function (evt) {
    playAudioOnHover();
  });
}
