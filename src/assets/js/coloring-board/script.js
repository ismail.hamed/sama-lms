function initial(image) {
  $(document).ready(() => {
    $.fn.capitalize = function () {
      $.each(this, function () {
        var caps = this.value;
        caps = caps.charAt(0).toUpperCase() + caps.slice(1);
        this.value = caps;
      });
      return this;
    };
    $.fn.wPaint.defaults = {
      path: '../assets/js/coloring-board/', // set absolute path for images and cursors
      theme: 'standard classic', // set theme
      autoScaleImage: true, // auto scale images to size of canvas (fg and bg)
      autoCenterImage: true, // auto center images (fg and bg, default is left/top corner)
      menuHandle: true, // setting to false will means menus cannot be dragged around
      menuOrientation: 'horizontal', // menu alignment (horizontal,vertical)
      menuOffsetLeft: 5, // left offset of primary menu
      menuOffsetTop: 5, // top offset of primary menu
      bg: image, // set bg on init
      image: null, // set image on init
      imageStretch: false, // stretch smaller images to full canvans dimensions
      onShapeDown: null, // callback for draw down event
      onShapeMove: null, // callback for draw move event
      onShapeUp: null // callback for draw up event
      /*, image: 'https://i.ibb.co/4Rm3rZy/test.jpg' // set background image*/
    };

    $.extend($.fn.wPaint.defaults, {
      mode: 'pencil', // set mode
      lineWidth: '4', // starting line width
      fillStyle: '#000000', // starting fill style
      strokeStyle: '#000000', // start stroke style
    });


    $.extend($.fn.wPaint.defaults, {
      fontSize: '12', // current font size for text input
      fontFamily: 'Arial', // active font family for text input
      fontBold: false, // text input bold enable/disable
      fontItalic: false, // text input italic enable/disable
      fontUnderline: false, // text input italic enable/disable
    });

    $.extend($.fn.wPaint.defaults, {
      saveImg: null, // callback triggerd on image save
      loadImgFg: null, // callback triggered on image fg
      loadImgBg: null // callback triggerd on image bg
    });
    setDrowPaintwidth();
    $('#wPaint').wPaint({
      menuOffsetLeft: -35,
      menuOffsetTop: -50,
    });
  });
}

function wUndo() {
  $('#wPaint').wPaint('undo');
}

function wRedo() {
  $('#wPaint').wPaint('redo');
}

function wClear() {
  $('#wPaint').wPaint('clear');
  hidePopup('popup-template-1');
}

function wSetMode(mode) {
  $('#wPaint').wPaint('setMode', mode);
}


function wSetStrokeWidth(sender, value) {

  let root = document.documentElement;
  root.style.setProperty('--stroke-width', value * .2);

  $('.circle-width.active').removeClass('active');
  $(sender).addClass('active');
  $('#wPaint').wPaint('setLineWidth', value);
  $('#wPaint').wPaint('fontSize', value);
}

function wSetColorValue(color, colorFor) {
  let root = document.documentElement;
  if (colorFor == 'fill') {
    $('#wPaint').wPaint('setFillStyle', color);
    root.style.setProperty('--fill-color', color);
  } else {
    $('#wPaint').wPaint('setStrokeStyle', color);
    root.style.setProperty('--stroke-color', color);
  }

}

function wSetPencil(sender, color) {
  $('.tool').removeClass('active');
  $(sender).addClass('active');
  wSetMode('pencil');
  wSetColorValue(color, 'stroke');
}

function updateColorListForAttribute(colorFor) {
  let colorsListDIv = document.querySelector('#colorsList');
  colorsListDIv.setAttribute("data-for", colorFor);
}

function setDrowPaintwidth() {
  let div = document.querySelector('#wPaint');
  let rect = div.getBoundingClientRect();
  document.querySelector('#wPaint').style.width = rect.width + 'px';
  document.querySelector('#wPaint').style.height = rect.height + 'px';
}

function wSetToolMode(sender, mode) {
  $('.tool').removeClass('active');
  sender.classList.add('active');
  wSetMode(mode);
}


function showPopup(popupId) {
  let popup = document.querySelector(`#${popupId}`);
  if (popup) {
    let popupBody = document.querySelector(`#${popupId} .popup-body`);
    if (popupBody) {
      popupBody.classList.add('animate__bounceIn');
      setTimeout(() => {
        popupBody.classList.remove('animate__bounceIn')
      }, 750);
    }
    document.querySelector(`#${popupId}`).style.display = "block";
  }
}

function hidePopup(popupId) {
  let popupBody = document.querySelector(`#${popupId} .popup-body`);
  setTimeout(() => {
    let popup = document.querySelector(`#${popupId}`);
    if (popup) {
      document.querySelector(`#${popupId}`).style.display = "none";
      if (popupBody) {
        popupBody.classList.remove('animate__bounceOut')
      }
    }
  }, 750);
  if (popupBody) {
    popupBody.classList.add('animate__bounceOut')
  }
}

function download() {
  var imageData = $("#wPaint").wPaint("image");
  var download = document.getElementById("download");
  var image = imageData.replace("image/png", "image/octet-stream"); // it will save locally
  download.setAttribute("href", image);
}
