export const environment = {
  production: true,
  baseUrl: 'https://teacher.samaeducation.com/webservice/rest/server.php?',
  loginUrl: 'https://teacher.samaeducation.com/login/token.php?',
  uploadFileURL: 'https://teacher.samaeducation.com/webservice/upload.php?',
};
